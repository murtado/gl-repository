xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CambiaEstadoFinalizadoExitosamente/Send/v1";
(:: import schema at "../CSC/CambiaEstadoFinalizadoExitosamente_v1_EBM.xsd" ::)
declare namespace ns2="http://www.esa.com/Provision/OrderingServ/T/InformarTerminoTrabajoOK";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OTC-OTC/WSDL/TerminarOKWSService/terminarOKWSService1.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:CambiaEstadoFinalizadoExitosamente_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:CambiaEstadoFinalizadoExitosamente_REQ) ::)) as element() (:: schema-element(ns2:getTerminarOK) ::) {
    <ns2:getTerminarOK>
        <arg0>
            <notificacionOFSC>Terminar_OK</notificacionOFSC>
            <idWO>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:ID)}</idWO>
            <pNumeroOXT>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</pNumeroOXT>
            <codigoPrestacion1>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</codigoPrestacion1>
            <cantidadPrestacion1>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</cantidadPrestacion1>
            <lugarPrestacion1>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:plcPrestation)}</lugarPrestacion1>
            <contratistaPrestacion1>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</contratistaPrestacion1>
            <codigoPrestacion2>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</codigoPrestacion2>
            <cantidadPrestacion2>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</cantidadPrestacion2>
            <lugarPrestacion2>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:plcPrestation)}</lugarPrestacion2>
            <contratistaPrestacion2>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</contratistaPrestacion2>
            <codigoPrestacion3>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</codigoPrestacion3>
            <cantidadPrestacion3>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</cantidadPrestacion3>
            <lugarPrestacion3>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:plcPrestation)}</lugarPrestacion3>
            <contratistaPrestacion3>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</contratistaPrestacion3>
            <codigoPrestacion4>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</codigoPrestacion4>
            <cantidadPrestacion4>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</cantidadPrestacion4>
            <lugarPrestacion4>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:plcPrestation)}</lugarPrestacion4>
            <contratistaPrestacion4>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</contratistaPrestacion4>
            <codigoPrestacion5>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</codigoPrestacion5>
            <cantidadPrestacion5>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</cantidadPrestacion5>
            <lugarPrestacion5>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:plcPrestation)}</lugarPrestacion5>
            <contratistaPrestacion5>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</contratistaPrestacion5>
            <tecnologiaAccesoServicio>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:networkType)}</tecnologiaAccesoServicio>
            <metraje>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</metraje>
            <recibeCliente>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</recibeCliente>
            <EPC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:EPC)}</EPC>
            <atiendeCheckList>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:checkList)}</atiendeCheckList>
            <ticketCheckList>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ticketCheckList>
            <zonaKilometraje>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</zonaKilometraje>
            <cumpleEficacia>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)}</cumpleEficacia>
            <motivoNoCumple>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)}</motivoNoCumple>
            <observacionesTecnico>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</observacionesTecnico>
            <celularContacto>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</celularContacto>
            <fumeres>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:fume)}</fumeres>
            <estadoWO>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:status)}</estadoWO>
            <tipoTermino>Terminado</tipoTermino>
            <causaTermino>Normal</causaTermino>
            <loginOFSC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</loginOFSC>
            <fume>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:fume)}</fume>
        </arg0>
    </ns2:getTerminarOK>
};

local:func($Request)