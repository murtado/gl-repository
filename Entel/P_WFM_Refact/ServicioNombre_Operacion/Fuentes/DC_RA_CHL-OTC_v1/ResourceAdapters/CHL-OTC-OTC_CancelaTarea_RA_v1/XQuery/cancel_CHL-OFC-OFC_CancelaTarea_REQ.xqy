xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CancelWorkOrderLegacy/Cancel/v1";
(:: import schema at "../CSC/CHL-OTC-OTC_CancelaTarea_v1_EBM.xsd" ::)
declare namespace ns2="http://www.esa.com/Provision/OrderingServ/T/cancelarTarea";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OTC-OTC/WSDL/CancelaTareaWSService/cancelaTareaWSService1.xsd" ::)

declare variable $reqiuest as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::) external;

declare function local:func($reqiuest as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::)) as element() (:: element(*, ns2:cancelaTareaRequest) ::) {
    <ns2:cancelaTareaRequest>
        <notificacionOFSC>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:notificacionOFSC)}</notificacionOFSC>
        <idWO>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:idWO)}</idWO>
        <numeroOXT>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:numeroOXT)}</numeroOXT>
        <tipoTermino>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:tipoTermino)}</tipoTermino>
        <causaTermino>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:causaTermino)}</causaTermino>
        <observacionesTecnico>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:observacionesTecnico)}</observacionesTecnico>
        <estadoWO>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:estadoWO)}</estadoWO>
        <loginOFSC>{fn:data($reqiuest/ns1:Body/ns1:WorkOrder/ns1:loginOFSC)}</loginOFSC>
    </ns2:cancelaTareaRequest>
};

local:func($reqiuest)