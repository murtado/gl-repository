xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/ComenzarActividad/Send/v1";
(:: import schema at "../CSC/ComenzarActividad_v1_EBM.xsd" ::)
declare namespace ns2="http://www.esa.com/Provision/OrderingServ/T/comenzarTrabajo";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OTC-OTC/WSDL/ComenzarTrabajoWSService/comenzarTrabajoWSService1.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:ComenzarActividad_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:ComenzarActividad_REQ) ::)) as element() (:: schema-element(ns2:getComenzarTrabajo) ::) {
    <ns2:getComenzarTrabajo>
        <arg0>
            <notificacionOFSC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</notificacionOFSC>
            <idWO>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:ID)}</idWO>
            <numeroOXT>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</numeroOXT>
            <loginOFSC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</loginOFSC>
            <recibeCliente>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</recibeCliente>
            <celularContacto>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</celularContacto>
            <observacionesTecnico>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</observacionesTecnico>
            <nombreTecnico>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</nombreTecnico>
            <rutTecnico>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</rutTecnico>
        </arg0>
    </ns2:getComenzarTrabajo>
};

local:func($Request)