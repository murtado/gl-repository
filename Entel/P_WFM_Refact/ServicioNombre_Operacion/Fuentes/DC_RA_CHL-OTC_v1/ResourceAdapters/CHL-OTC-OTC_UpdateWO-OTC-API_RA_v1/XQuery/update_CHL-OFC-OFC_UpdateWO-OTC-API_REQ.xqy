xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/update_CHL-OTC-OTC_UpdateWO-OTC-API_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/UpdateWO-OTC-API/Update/v1";
(:: import schema at "../CSC/CHL-OTC-OTC_UpdateWO-OTC-API_v1_EBM.xsd" ::)

declare variable $updateWoReq as element() (:: schema-element(ns1:UpdateWO-OTC-API_REQ) ::) external;

declare function local:func($updateWoReq as element() (:: schema-element(ns1:UpdateWO-OTC-API_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:idWO>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:idWO>
        <ns2:numeroOXT>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:numeroOXT>
        <ns2:loginOFSC>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:loginOFSC>
        <ns2:fechaAgendamiento>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns2:fechaAgendamiento>
        <ns2:bloqueAgendamiento>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:applicableDuring)}</ns2:bloqueAgendamiento>
        <ns2:observaciones>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:observaciones>
    </ns2:Root-Element>
};

local:func($updateWoReq)