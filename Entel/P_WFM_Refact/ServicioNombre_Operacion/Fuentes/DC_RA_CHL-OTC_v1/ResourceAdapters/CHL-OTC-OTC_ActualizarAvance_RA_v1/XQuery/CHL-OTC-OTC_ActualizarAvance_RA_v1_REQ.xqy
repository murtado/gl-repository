xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/ActualizarAvance/Send/v1";
(:: import schema at "../CSC/ActualizarAvance_v1_EBM.xsd" ::)
declare namespace ns2="http://www.esa.com/Provision/OrderingServ/T/actualizarActividad";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OTC-OTC/WSDL/ActualizarActividadWSService/actualizarActividadWSService1.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:ActualizarAvance_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:ActualizarAvance_REQ) ::)) as element() (:: schema-element(ns2:getActualizarActividad) ::) {
    <ns2:getActualizarActividad>
        <arg0>
            <notificacionOFSC>{fn:data($Request/ns1:Body/ns1:getActualizarActividad/ns1:arg0/ns1:notificacionOFSC)}</notificacionOFSC>
            <idWO>{fn:data($Request/ns1:Body/ns1:getActualizarActividad/ns1:arg0/ns1:idWO)}</idWO>
            <numeroOXT>{fn:data($Request/ns1:Body/ns1:getActualizarActividad/ns1:arg0/ns1:numeroOXT)}</numeroOXT>
            <tiempoEstimadoTermino>{fn:data($Request/ns1:Body/ns1:getActualizarActividad/ns1:arg0/ns1:tiempoEstimadoTermino)}</tiempoEstimadoTermino>
            <observacionesTecnico>{fn:data($Request/ns1:Body/ns1:getActualizarActividad/ns1:arg0/ns1:observacionesTecnico)}</observacionesTecnico>
            <tareaDeEjecucion>{fn:data($Request/ns1:Body/ns1:getActualizarActividad/ns1:arg0/ns1:tareaDeEjecucion)}</tareaDeEjecucion>
        </arg0>
    </ns2:getActualizarActividad>
};

local:func($Request)