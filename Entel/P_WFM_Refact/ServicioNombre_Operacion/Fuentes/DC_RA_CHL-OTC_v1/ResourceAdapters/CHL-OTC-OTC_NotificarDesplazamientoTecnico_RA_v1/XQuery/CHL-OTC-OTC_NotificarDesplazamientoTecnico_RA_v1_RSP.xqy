xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/NotificarDesplazamientoTecnico/Send/v1";
(:: import schema at "../CSC/NotificarDesplazamientoTecnico_v1_EBM.xsd" ::)
declare namespace ns2="http://www.esa.com/Provision/OrderingServ/T/enCamino";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OTC-OTC/WSDL/EnCaminoWSService/enCaminoWSService1.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Response as element() (:: schema-element(ns2:getEnCaminoResponse) ::) external;
declare variable $Result as element() (:: schema-element(ns3:Result) ::) external;

declare function local:func($Response as element() (:: schema-element(ns2:getEnCaminoResponse) ::),
                            $Result as element() (:: schema-element(ns3:Result) ::)) as element() (:: schema-element(ns1:NotificarDesplazamientoTecnico_RSP) ::) {
    <ns1:NotificarDesplazamientoTecnico_RSP>
        <ns4:ResponseHeader>
            <ns4:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns4:Consumer>
            <ns4:Trace clientReqTimestamp="" eventID="">
                <ns4:Service>
                </ns4:Service>
            </ns4:Trace>
            <ns4:Channel>
            </ns4:Channel>
            <ns3:Result status="{fn:data($Result/@status)}">
                {
                    if ($Result/@description)
                    then attribute description {fn:data($Result/@description)}
                    else ()
                }
                {
                    if ($Result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($Result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($Result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($Result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($Result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($Result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($Result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($Result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($Result/ns5:SourceError/@code)
                                then attribute code {fn:data($Result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($Result/ns5:SourceError/@description)
                                then attribute description {fn:data($Result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($Result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($Result/ns3:CorrelativeErrors)
                    then 
                        <ns3:CorrelativeErrors>
                            {
                                for $SourceError in $Result/ns3:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns3:CorrelativeErrors>
                    else ()
                }
            </ns3:Result>
        </ns4:ResponseHeader>
        <ns1:Body></ns1:Body>
    </ns1:NotificarDesplazamientoTecnico_RSP>
};

local:func($Response, $Result)