xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/CHL-AGE-AGE_UpdateActivity_AGE_JSON_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/UpdateActivity_AGE/Update/v1";
(:: import schema at "../CSC/CHL-AGE-AGE_UpdateActivity_AGE_v1_EBM.xsd" ::)

declare variable $request as element() (:: schema-element(ns1:UpdateActivity_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:UpdateActivity_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
            then <ns2:A_APPOINTMENTTIMES>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:A_APPOINTMENTTIMES>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:calculationDate)
            then <ns2:A_CALCULATION_DATE>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:calculationDate)}</ns2:A_CALCULATION_DATE>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)
            then <ns2:A_CANCELATION_REASON>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)}</ns2:A_CANCELATION_REASON>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
            then <ns2:A_ENGINEER_NOTES>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:A_ENGINEER_NOTES>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
            then <ns2:A_ESTIMATED_END_DATE>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:A_ESTIMATED_END_DATE>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:externalID)
            then <ns2:apptNumber>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:apptNumber>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
            then <ns2:A_NOTES>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:A_NOTES>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)
            then <ns2:A_FORCED_APPOINTMENT>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)}</ns2:A_FORCED_APPOINTMENT>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)
            then <ns2:A_FUTURE_CONTACT_DATE>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)}</ns2:A_FUTURE_CONTACT_DATE>
            else ()
        }
        <ns2:activityId>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:activityId>
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
            then <ns2:A_NETWORK_RESOURCES>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:A_NETWORK_RESOURCES>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)
            then <ns2:A_PROGRESSTASK>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:A_PROGRESSTASK>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
            then <ns2:A_SCHEDULER_USERNAME>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:A_SCHEDULER_USERNAME>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)
            then <ns2:A_SIGLA_ACTIVIDAD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)}</ns2:A_SIGLA_ACTIVIDAD>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:accessTech)
            then <ns2:A_ACCESS_TECHNOLOGY>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:accessTech)}</ns2:A_ACCESS_TECHNOLOGY>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
            then <ns2:A_ACCESS_TECH_QTY>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:A_ACCESS_TECH_QTY>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
            then <ns2:longitude>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:longitude>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
            then <ns2:latitude>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:latitude>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
            then <ns2:timeSlot>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:timeSlot>
            else ()
        }
        {
            if ($request/ns1:Body/ns1:WorkOrder/ns1:status)
            then <ns2:status>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
            else ()
        }
    </ns2:Root-Element>
};

local:func($request)