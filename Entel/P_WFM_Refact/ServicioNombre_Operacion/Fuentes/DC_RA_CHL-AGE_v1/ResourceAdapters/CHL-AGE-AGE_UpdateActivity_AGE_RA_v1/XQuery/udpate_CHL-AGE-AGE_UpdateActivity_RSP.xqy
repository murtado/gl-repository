xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/CHL-AGE-AGE_UpdateActivity_AGE_JSON_RSP.xsd" ::)
declare namespace ns3="http://www.entel.cl/EBM/UpdateActivity_AGE/Update/v1";
(:: import schema at "../CSC/CHL-AGE-AGE_UpdateActivity_AGE_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $updateActivityRs as element() (:: schema-element(ns1:Root-Element) ::) external;
declare variable $result as element() (:: schema-element(ns2:Result) ::) external;

declare function local:func($updateActivityRs as element() (:: schema-element(ns1:Root-Element) ::), 
                            $result as element() (:: schema-element(ns2:Result) ::)) 
                            as element() (:: schema-element(ns3:UpdateActivity_RSP) ::) {
    <ns3:UpdateActivity_RSP>
        <ns4:ResponseHeader>
            <ns2:Result status="{fn:data($result/@status)}">
                {
                    if ($result/@description)
                    then attribute description {fn:data($result/@description)}
                    else ()
                }
                {
                    if ($result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($result/ns5:SourceError/@code)
                                then attribute code {fn:data($result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($result/ns5:SourceError/@description)
                                then attribute description {fn:data($result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($result/ns2:CorrelativeErrors)
                    then 
                        <ns2:CorrelativeErrors>
                            {
                                for $SourceError in $result/ns2:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns2:CorrelativeErrors>
                    else ()
                }
            </ns2:Result>
        </ns4:ResponseHeader>
        <ns3:Body>
                  <ns3:WorkOrder>
                      {if($updateActivityRs/ns1:activityId) then
                        <ns3:ID>{fn:data($updateActivityRs/ns1:activityId)}</ns3:ID>
                      else()}
                <ns3:PartyResource>
                </ns3:PartyResource>
            </ns3:WorkOrder>
        </ns3:Body>
    </ns3:UpdateActivity_RSP>
};

local:func($updateActivityRs, $result)