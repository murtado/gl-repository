xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/Error/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/Error_v1_ESO.xsd" ::)

declare variable $LegacyResponse as element() external;
declare variable $LegacyResult as element() external;

declare function local:CHL-BRI-STA-DELIVERY_HTTPSOAP12_SEB($LegacyResponse as element(),$LegacyResult as element()?) as element() (:: schema-element(ns1:SourceError) ::) {
    
    if(exists($LegacyResponse/*:businessResult))then     
    let $SourceErrorCode	          := local:getSourceErrorCode2()
    let $SourceErrorDescription 	  := local:getSourceErrorDescription2()
    let $ErrorSourceCode                  := "CHL-BRI-STA"
    let $ErrorSourceDetails               := "CHL-BRI-STA-DELIVERY_HTTPSOAP12_TW"
    
    return 
    <ns1:SourceError code="{$SourceErrorCode}" description="{$SourceErrorDescription}">
        <ns1:ErrorSourceDetails source="{$ErrorSourceCode}" details="{$ErrorSourceDetails}"/>
         {
            if (data($LegacyResponse/*[1]/*[1])!='200') then
                <ns1:SourceFault>{$LegacyResponse}</ns1:SourceFault>
            else
                <ns1:SourceFault>
                </ns1:SourceFault>
        }
    </ns1:SourceError>
    else
          let $SourceErrorCode	          := local:getSourceErrorCode()
    let $SourceErrorDescription 	  := local:getSourceErrorDescription()
    let $ErrorSourceCode                  := "CHL-BRI-STA"
    let $ErrorSourceDetails               := "CHL-BRI-STA-DELIVERY_HTTPSOAP12_TW"
    
    return 
    <ns1:SourceError code="{$SourceErrorCode}" description="{$SourceErrorDescription}">
        <ns1:ErrorSourceDetails source="{$ErrorSourceCode}" details="{$ErrorSourceDetails}"/>
         {
            if (exists($LegacyResponse/*:faultcode)) then
                <ns1:SourceFault>{$LegacyResponse}</ns1:SourceFault>
            else
                <ns1:SourceFault>
                </ns1:SourceFault>
        }
        </ns1:SourceError>
      
};

declare function local:getSourceErrorCode() as xs:string
{
	
		let $FaultErrorCode := 
          if(exists($LegacyResponse/*:faultcode)) then
            data($LegacyResponse/*[1])
          else
              ('0')
              
	return $FaultErrorCode
};

declare function local:getSourceErrorDescription() as xs:string
{
	  let $FaultErrorDescription := 
            if(exists($LegacyResponse/*:faultcode)) then
            data($LegacyResponse/*[2])
              else
            ('OK')
	
	return $FaultErrorDescription
};
declare function local:getSourceErrorCode2() as xs:string
{
	
		let $FaultErrorCode := 
          if(exists($LegacyResponse/*[1]/*[1])) then
            data($LegacyResponse/*[1]/*[1])    
          else
              ('FRW-Default')
              
	return $FaultErrorCode
};

declare function local:getSourceErrorDescription2() as xs:string
{
	  let $FaultErrorDescription := 
            if(exists($LegacyResponse/*[1]/*[1])) then
              data($LegacyResponse/*[1]/*[2])          
              else
                ('No Legacy Result Information was found. This is a default Legacy Result.')
	
	return $FaultErrorDescription
};
local:CHL-BRI-STA-DELIVERY_HTTPSOAP12_SEB($LegacyResponse,$LegacyResult)