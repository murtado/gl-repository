xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/ComenzarActividad/Send/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OTC_v1/ResourceAdapters/CHL-OTC-OTC_ComenzarActividad_RA_v1/CSC/ComenzarActividad_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/StartActivity/Send/v1";
(:: import schema at "../../../../../ESC/Primary/SendMessage_StartActivity_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $comenzarActividadRq as element() (:: schema-element(ns1:StartActivity_REQ) ::) external;

declare function local:func($comenzarActividadRq as element() (:: schema-element(ns1:StartActivity_REQ) ::)) as element() (:: schema-element(ns2:ComenzarActividad_REQ) ::) {
    <ns2:ComenzarActividad_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}"
                countryCode="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Consumer/@countryCode)}">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" reqTimestamp="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}" rspTimestamp="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}" processID="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@processID)}" eventID="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@eventID)}" sourceID="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@sourceID)}" correlationEventID="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)}" conversationID="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@conversationID)}" correlationID="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/@correlationID)}">
                <ns3:Service code="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}" name="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}" operation="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}">
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel name="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Channel/@name)}" mode="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns3:Channel/@mode)}">
            </ns3:Channel>
            <ns4:Result status="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/@status)}" description="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/@description)}">
                <ns5:CanonicalError code="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}" description="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}" type="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}">
                </ns5:CanonicalError>
                <ns5:SourceError code="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}" description="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}">
                    <ns5:ErrorSourceDetails source="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                    </ns5:ErrorSourceDetails>
                    {
                        if ($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                        then <ns5:SourceFault>{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                        else ()
                    }
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError code="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@code)}" description="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@description)}">
                        <ns5:ErrorSourceDetails source="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                        </ns5:ErrorSourceDetails>
                        {
                            if ($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)
                            then <ns5:SourceFault>{fn:data($comenzarActividadRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                            else ()
                        }
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns2:engineerNotes>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:engineerNotes>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)
                    then <ns2:customerHostCell>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns2:customerHostCell>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:customerHostName)
                    then <ns2:customerHostName>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</ns2:customerHostName>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:BOUserName)
                    then <ns2:BOUserName>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:BOUserName>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:subject)
                    then <ns2:subject>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:subject)}</ns2:subject>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:userNotification)
                    then <ns2:userNotification>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:userNotification)}</ns2:userNotification>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:dateNotification)
                    then <ns2:dateNotification>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:dateNotification)}</ns2:dateNotification>
                    else ()
                }
                <ns2:PartyResource>
                    <ns2:pname>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</ns2:pname>
                    <ns2:IndividualIdentification>
                        <ns2:number>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                    </ns2:IndividualIdentification>
                </ns2:PartyResource>
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)
                    then <ns2:startCoordinates>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)}</ns2:startCoordinates>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns2:status>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:details)
                    then <ns2:details>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:details)}</ns2:details>
                    else ()
                }
                <ns2:CustomerAccount>
                    <ns2:customerGroup>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:customerGroup)}</ns2:customerGroup>
                </ns2:CustomerAccount>
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:actionSD)
                    then <ns2:actionSD>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns2:actionSD>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns2:requestType>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:requestType>
                    else ()
                }
                {
                    if ($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:statusReason)
                    then <ns2:statusReason>{fn:data($comenzarActividadRq/ns1:Body/ns1:WorkOrder/ns1:statusReason)}</ns2:statusReason>
                    else ()
                }
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:ComenzarActividad_REQ>
};

local:func($comenzarActividadRq)