xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/OFC-OFC/CreateActivity/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v1/ResourceAdapters/CHL-OFC-OFC_CreateActivity_RA_v1/CSC/CHL-OFC-OFC_CreateActivity_v1_CSM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/CreateWorkOrder/Create/v1";
(:: import schema at "../../../../../ESC/Primary/CreateWorkOrder_v1_EBM.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare variable $createNewActivity__RA_RSP as element() (:: schema-element(ns1:CreateActivity_RSP) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns3:ResponseHeader) ::) external;

declare function local:func($createNewActivity__RA_RSP as element() (:: schema-element(ns1:CreateActivity_RSP) ::), 
                            $RequestHeader as element() (:: schema-element(ns3:ResponseHeader) ::)) 
                            as element() (:: schema-element(ns2:CreateWorkOrder_RSP) ::) {
    <ns2:CreateWorkOrder_RSP>

        <ns3:ResponseHeader>
            <ns3:Consumer sysCode="{fn:data($RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($RequestHeader/ns3:Consumer/@countryCode)}"> </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            <ns4:Result status="{fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/@status)}">
                {
                    if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/@description)
                    then attribute description {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/@description)}
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@code)
                                then attribute code {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@description)
                                then attribute description {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns4:CorrelativeErrors)
                    then 
                        <ns4:CorrelativeErrors>
                            {
                                for $SourceError in $createNewActivity__RA_RSP/ns3:ResponseHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns4:CorrelativeErrors>
                    else ()
                }
            </ns4:Result>
        </ns3:ResponseHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns2:annulmentTime>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns2:annulmentTime>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)
                    then <ns2:appointmentDate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns2:appointmentDate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns2:appointmentTimes>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:appointmentTimes>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
                    then <ns2:assignmentTime>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns2:assignmentTime>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
                    then <ns2:broadbandIndicator>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns2:broadbandIndicator>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
                    then <ns2:cellComercial>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns2:cellComercial>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:contactName)
                    then <ns2:contactName>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns2:contactName>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns2:contractor>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:contractor>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:createdDate)
                    then <ns2:createdDate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns2:createdDate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
                    then <ns2:deliveryImplementacionDate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns2:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:depReg)
                    then <ns2:depReg>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns2:depReg>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
                    then <ns2:emailComercial>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns2:emailComercial>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
                    then <ns2:emissionDate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns2:emissionDate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:endTime)
                    then <ns2:endTime>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:endTime)}</ns2:endTime>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:entryDate)
                    then <ns2:entryDate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns2:entryDate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns2:estimatedFinishDate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:estimatedFinishDate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
                    then <ns2:externalStatus>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns2:externalStatus>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalType)
                    then <ns2:externalType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns2:externalType>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalZone)
                    then <ns2:externalZone>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns2:externalZone>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:finishTime)
                    then <ns2:finishTime>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns2:finishTime>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:ID)
                    then <ns2:ID>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:installationCost)
                    then <ns2:installationCost>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns2:installationCost>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
                    then <ns2:legacyBOUserName>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns2:legacyBOUserName>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
                    then <ns2:nameComercial>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns2:nameComercial>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns2:networkResourceID>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:networkResourceID>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
                    then <ns2:networkSpeed>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns2:networkSpeed>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns2:networkType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:networkType>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns2:OFTcloseReason>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns2:OFTcloseType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)
                    then <ns2:OFTsummary>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns2:OFTsummary>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
                    then <ns2:phoneComercial>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns2:phoneComercial>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:recordType)
                    then <ns2:recordType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:recordType)}</ns2:recordType>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns2:requestType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:requestType>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
                    then <ns2:rescheduleRequester>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns2:rescheduleRequester>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
                    then <ns2:salesChannelID>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns2:salesChannelID>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
                    then <ns2:salesChannelType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns2:salesChannelType>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns2:scheduleUserName>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:scheduleUserName>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:segment)
                    then <ns2:segment>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:segment>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceID)
                    then <ns2:serviceID>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns2:serviceID>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceName)
                    then <ns2:serviceName>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns2:serviceName>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
                    then <ns2:serviceTemplate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns2:serviceTemplate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceType)
                    then <ns2:serviceType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns2:serviceType>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
                    then <ns2:serviceTypeSIAC>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns2:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)
                    then <ns2:serviceWindowEnd>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)}</ns2:serviceWindowEnd>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)
                    then <ns2:serviceWindowStart>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)}</ns2:serviceWindowStart>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
                    then <ns2:severityProblem>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns2:severityProblem>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
                    then <ns2:signatureDate>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns2:signatureDate>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
                    then <ns2:slaWindowEnd>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
                    then <ns2:slaWindowStart>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns2:slaWindowStart>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)
                    then <ns2:standardDrivingTime>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)}</ns2:standardDrivingTime>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDuration)
                    then <ns2:standardDuration>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDuration)}</ns2:standardDuration>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:startTime)
                    then <ns2:startTime>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:startTime)}</ns2:startTime>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns2:status>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)
                    then <ns2:timeOfAssignment>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)}</ns2:timeOfAssignment>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)
                    then <ns2:timeOfBooking>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)}</ns2:timeOfBooking>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
                    then <ns2:troubleAction>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns2:troubleAction>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
                    then <ns2:troubleDescription>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns2:troubleDescription>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
                    then <ns2:troubleDiagnosis>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns2:troubleDiagnosis>
                    else ()
                }
                {
                    if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:type)
                    then <ns2:type>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:type)}</ns2:type>
                    else ()
                }
                <ns2:absoluteLocalLocation>
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
                        then <ns2:X>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:X>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
                        then <ns2:Y>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:Y>
                        else ()
                    }
                    <ns2:timezone>
                        {
                            if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)
                            then <ns2:name>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)}</ns2:name>
                            else ()
                        }
                        {
                            if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)
                            then <ns2:timeZoneIANA>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)}</ns2:timeZoneIANA>
                            else ()
                        }
                    </ns2:timezone>
                </ns2:absoluteLocalLocation>
                <ns2:address>
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)
                        then <ns2:commune>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)}</ns2:commune>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)
                        then <ns2:city>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)}</ns2:city>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)
                        then <ns2:region>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)}</ns2:region>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)
                        then <ns2:streetName>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)}</ns2:streetName>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)
                        then <ns2:addressReference>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)}</ns2:addressReference>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)
                        then <ns2:buildingType>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)}</ns2:buildingType>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)
                        then <ns2:complentaryAddress>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)}</ns2:complentaryAddress>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)
                        then <ns2:department>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)}</ns2:department>
                        else ()
                    }
                </ns2:address>
                <ns2:customerAccount>
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)
                        then <ns2:alias>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns2:alias>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)
                        then <ns2:customerGroup>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns2:customerGroup>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)
                        then <ns2:portabilityIndicator>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns2:portabilityIndicator>
                        else ()
                    }
                    <ns2:contact>
                    {
                    if($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)then
                        <ns2:alternativePhoneNumber>
                            <ns2:number>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns2:number>
                        </ns2:alternativePhoneNumber>
                        else()
                        }
                         {
                    if($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)then
                        <ns2:cellPhone>
                            <ns2:number>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns2:number>
                        </ns2:cellPhone>
                        else()
                         }
                         {
                    if($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)then
                        <ns2:email>
                            <ns2:eMailAddress>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns2:eMailAddress>
                        </ns2:email>
                        else()
                         }
                         {
                    if($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification)then
                        <ns2:IndividualIdentification>
                            <ns2:number>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                            {
                                if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)
                                then <ns2:type>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)}</ns2:type>
                                else ()
                            }
                        </ns2:IndividualIdentification>
                        else()
                         }
                         {
                    if($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)then
                        <ns2:IndividualName>
                            <ns2:formatedName>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns2:formatedName>
                        </ns2:IndividualName>
                        else()
                        }
                    </ns2:contact>
                </ns2:customerAccount>
                <ns2:Language>
                    <ns2:alphabetName>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Language/ns1:alphabetName)}</ns2:alphabetName>
                </ns2:Language>
                <ns2:PartyResource>
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)
                        then <ns2:ID>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns2:ID>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)
                        then <ns2:internalID>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)}</ns2:internalID>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)
                        then <ns2:resourceTimeZone>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)}</ns2:resourceTimeZone>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)
                        then <ns2:UTCOffset>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)}</ns2:UTCOffset>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ResourceTimeZoneIANA)
                        then <ns2:ResourceTimeZoneIANA>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ResourceTimeZoneIANA)}</ns2:ResourceTimeZoneIANA>
                        else ()
                    }
                    <ns2:IndividualIdentification>
                        <ns2:number>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                        {
                            if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)
                            then <ns2:type>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)}</ns2:type>
                            else ()
                        }
                    </ns2:IndividualIdentification>
                </ns2:PartyResource>
                <ns2:Product>
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                        then <ns2:ANISQuantity>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:ANISQuantity>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                        then <ns2:ANISQuantityRequired>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns2:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)
                        then <ns2:Category>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)}</ns2:Category>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                        then <ns2:DECOQuantity>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns2:DECOQuantity>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                        then <ns2:DECOQuantityRequired>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)
                        then <ns2:Equipment>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)}</ns2:Equipment>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                        then <ns2:layerMPLS>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns2:layerMPLS>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                        then <ns2:ngnModel>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns2:ngnModel>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                        then <ns2:PHONEQuantity>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns2:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                        then <ns2:externalProductCode>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns2:externalProductCode>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                        then <ns2:externalProductDescription>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns2:externalProductDescription>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                        then <ns2:STBQuantity>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:STBQuantity>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)
                        then <ns2:VoiceIndicator>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)}</ns2:VoiceIndicator>
                        else ()
                    }
                    {
                        if ($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                        then <ns2:WIFIExtensorQuantity>{fn:data($createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns2:WIFIExtensorQuantity>
                        else ()
                    }
                </ns2:Product>
                <ns2:linkedActivities>
                    {
                        for $links in $createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:linkedActivities/ns1:links
                        return 
                        <ns2:links>
                            <ns2:rel>{fn:data($links/ns1:rel)}</ns2:rel>
                            <ns2:href>{fn:data($links/ns1:href)}</ns2:href></ns2:links>
                    }
                </ns2:linkedActivities>
                <ns2:linkList>
                    {
                        for $links1 in $createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:linkList/ns1:links
                        return 
                        <ns2:links>
                            <ns2:rel>{fn:data($links1/ns1:rel)}</ns2:rel>
                            <ns2:href>{fn:data($links1/ns1:href)}</ns2:href></ns2:links>
                    }
                </ns2:linkList>
                <ns2:RequiredInventories>
                    {
                        for $links3 in $createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:RequiredInventories/ns1:links
                        return 
                        <ns2:links>
                            <ns2:rel>{fn:data($links3/ns1:rel)}</ns2:rel>
                            <ns2:href>{fn:data($links3/ns1:href)}</ns2:href></ns2:links>
                    }
                </ns2:RequiredInventories>
                <ns2:ResourcePreferences>
                    {
                        for $links2 in $createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:ResourcePreferences/ns1:links
                        return 
                        <ns2:links>
                            <ns2:rel>{fn:data($links2/ns1:rel)}</ns2:rel>
                            <ns2:href>{fn:data($links2/ns1:href)}</ns2:href></ns2:links>
                    }
                </ns2:ResourcePreferences>
                <ns2:workSkill>
                    {
                        for $links4 in $createNewActivity__RA_RSP/ns1:Body/ns1:WorkOrder/ns1:workSkill/ns1:links
                        return 
                        <ns2:links>
                            <ns2:rel>{fn:data($links4/ns1:rel)}</ns2:rel>
                            <ns2:href>{fn:data($links4/ns1:href)}</ns2:href></ns2:links>
                    }
                </ns2:workSkill>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CreateWorkOrder_RSP>
};

local:func($createNewActivity__RA_RSP, $RequestHeader)