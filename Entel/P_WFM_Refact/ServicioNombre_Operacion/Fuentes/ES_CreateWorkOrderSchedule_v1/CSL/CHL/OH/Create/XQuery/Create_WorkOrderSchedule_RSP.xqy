xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CreateWorkOrder/Create/v1";
(:: import schema at "../../../../../../ES_CreateWorkOrder_v1/ESC/Primary/CreateWorkOrder_v1_EBM.xsd" ::)
declare namespace ns3="http://www.entel.cl/EBM/CreateWorkOrderSchedule/Create/v1";
(:: import schema at "../../../../../ESC/Primary/CreateWorkOrderSchedule_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/GetWorkOrderSchedule/Get/v1";
(:: import schema at "../../../../../../ES_GetWorkOrderSchedule_v1/ESC/Primary/GetWorkOrderSchedule_v1_EBM.xsd" ::)
declare namespace ns4="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns6 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/Result/v2";

declare variable $Response_CreateWOrkOrder_AGE_RSP as element() (:: schema-element(ns1:CreateWorkOrder_RSP) ::) external;
declare variable $Response_CreateWorkOrder_RSP as element() (:: schema-element(ns1:CreateWorkOrder_RSP) ::) external;
declare variable $Response_GetWorkOrderSchedule_RSP as element() (:: schema-element(ns2:GetWorkOrderSchedule_RSP) ::) external;
declare variable $Request_header as element() (:: schema-element(ns4:ResponseHeader) ::) external;


declare function local:func($Response_CreateWOrkOrder_AGE_RSP as element() (:: schema-element(ns1:CreateWorkOrder_RSP) ::), 
                            $Response_CreateWorkOrder_RSP as element() (:: schema-element(ns1:CreateWorkOrder_RSP) ::), 
                            $Response_GetWorkOrderSchedule_RSP as element() (:: schema-element(ns2:GetWorkOrderSchedule_RSP) ::),
                            $Request_header as element() (:: schema-element(ns4:ResponseHeader) ::)) 
                            as element() (:: schema-element(ns3:CreateWorkOrderSchedule_RSP) ::) {
    <ns3:CreateWorkOrderSchedule_RSP>
        <ns4:ResponseHeader>
            <ns4:Consumer sysCode="{fn:data($Request_header/ns4:Consumer/@sysCode)}" enterpriseCode="{fn:data($Request_header/ns4:Consumer/@enterpriseCode)}" countryCode="{fn:data($Request_header/ns4:Consumer/@countryCode)}"> </ns4:Consumer>
            <ns4:Trace clientReqTimestamp="{fn:data($Request_header/ns4:Trace/@clientReqTimestamp)}" eventID="{fn:data($Request_header/ns4:Trace/@eventID)}">
                {
                    if ($Request_header/ns4:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($Request_header/ns4:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($Request_header/ns4:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($Request_header/ns4:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($Request_header/ns4:Trace/@processID)
                    then attribute processID {fn:data($Request_header/ns4:Trace/@processID)}
                    else ()
                }
                {
                    if ($Request_header/ns4:Trace/@sourceID)
                    then attribute sourceID {fn:data($Request_header/ns4:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($Request_header/ns4:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($Request_header/ns4:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($Request_header/ns4:Trace/@conversationID)
                    then attribute conversationID {fn:data($Request_header/ns4:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($Request_header/ns4:Trace/@correlationID)
                    then attribute correlationID {fn:data($Request_header/ns4:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($Request_header/ns4:Trace/ns4:Service)
                    then 
                        <ns4:Service>
                            {
                                if ($Request_header/ns4:Trace/ns4:Service/@code)
                                then attribute code {fn:data($Request_header/ns4:Trace/ns4:Service/@code)}
                                else ()
                            }
                            {
                                if ($Request_header/ns4:Trace/ns4:Service/@name)
                                then attribute name {fn:data($Request_header/ns4:Trace/ns4:Service/@name)}
                                else ()
                            }
                            {
                                if ($Request_header/ns4:Trace/ns4:Service/@operation)
                                then attribute operation {fn:data($Request_header/ns4:Trace/ns4:Service/@operation)}
                                else ()
                            }
                        </ns4:Service>
                    else ()
                }
            </ns4:Trace>
            {
                if ($Request_header/ns4:Channel)
                then 
                    <ns4:Channel>
                        {
                            if ($Request_header/ns4:Channel/@name)
                            then attribute name {fn:data($Request_header/ns4:Channel/@name)}
                            else ()
                        }
                        {
                            if ($Request_header/ns4:Channel/@mode)
                            then attribute mode {fn:data($Request_header/ns4:Channel/@mode)}
                            else ()
                        }
                    </ns4:Channel>
                else ()
            }
           
            
            <ns5:Result status="{fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/@status)}">
                {
                    if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/@description)
                    then attribute description {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/@description)}
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:CanonicalError)
                    then 
                        <ns6:CanonicalError>
                            {
                                if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:CanonicalError/@code)
                                then attribute code {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:CanonicalError/@description)
                                then attribute description {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:CanonicalError/@type)
                                then attribute type {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:CanonicalError/@type)}
                                else ()
                            }
                        </ns6:CanonicalError>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError)
                    then 
                        <ns6:SourceError>
                            {
                                if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/@code)
                                then attribute code {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/@description)
                                then attribute description {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/@description)}
                                else ()
                            }
                            <ns6:ErrorSourceDetails>
                                {
                                    if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/ns6:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/ns6:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/ns6:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/ns6:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns6:ErrorSourceDetails>
                            {
                                if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/ns6:SourceFault)
                                then <ns6:SourceFault>{fn:data($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns6:SourceError/ns6:SourceFault)}</ns6:SourceFault>
                                else ()
                            }
                        </ns6:SourceError>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns5:CorrelativeErrors)
                    then 
                        <ns5:CorrelativeErrors>
                            {
                                for $SourceError in $Response_CreateWorkOrder_RSP/ns4:ResponseHeader/ns5:Result/ns5:CorrelativeErrors/ns6:SourceError
                                return 
                                <ns6:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns6:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns6:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns6:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns6:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns6:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns6:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns6:SourceFault)
                                        then <ns6:SourceFault>{fn:data($SourceError/ns6:SourceFault)}</ns6:SourceFault>
                                        else ()
                                    }
                                </ns6:SourceError>
                            }
                        </ns5:CorrelativeErrors>
                    else ()
                }
            </ns5:Result>
        </ns4:ResponseHeader>
        <ns3:Body>
            <ns3:WorkOrder>
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns3:annulmentTime>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns3:annulmentTime>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)
                    then <ns3:appointmentDate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns3:appointmentDate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns3:appointmentTimes>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns3:appointmentTimes>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
                    then <ns3:assignmentTime>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns3:assignmentTime>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
                    then <ns3:broadbandIndicator>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns3:broadbandIndicator>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
                    then <ns3:cellComercial>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns3:cellComercial>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:contactName)
                    then <ns3:contactName>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns3:contactName>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns3:contractor>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns3:contractor>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:createdDate)
                    then <ns3:createdDate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns3:createdDate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
                    then <ns3:deliveryImplementacionDate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns3:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:depReg)
                    then <ns3:depReg>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns3:depReg>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
                    then <ns3:emailComercial>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns3:emailComercial>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
                    then <ns3:emissionDate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns3:emissionDate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:endTime)
                    then <ns3:endTime>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:endTime)}</ns3:endTime>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:entryDate)
                    then <ns3:entryDate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns3:entryDate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns3:estimatedFinishDate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns3:estimatedFinishDate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns3:externalID>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns3:externalID>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns3:externalNotes>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns3:externalNotes>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
                    then <ns3:externalStatus>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns3:externalStatus>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalType)
                    then <ns3:externalType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns3:externalType>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalZone)
                    then <ns3:externalZone>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns3:externalZone>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:finishTime)
                    then <ns3:finishTime>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns3:finishTime>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:ID)
                    then <ns3:ID>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns3:ID>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:installationCost)
                    then <ns3:installationCost>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns3:installationCost>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
                    then <ns3:legacyBOUserName>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns3:legacyBOUserName>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
                    then <ns3:nameComercial>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns3:nameComercial>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns3:networkResourceID>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns3:networkResourceID>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
                    then <ns3:networkSpeed>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns3:networkSpeed>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns3:networkType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns3:networkType>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns3:OFTcloseReason>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns3:OFTcloseReason>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns3:OFTcloseType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns3:OFTcloseType>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)
                    then <ns3:OFTsummary>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns3:OFTsummary>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
                    then <ns3:phoneComercial>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns3:phoneComercial>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:recordType)
                    then <ns3:recordType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:recordType)}</ns3:recordType>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns3:requestType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns3:requestType>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
                    then <ns3:rescheduleRequester>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns3:rescheduleRequester>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
                    then <ns3:salesChannelID>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns3:salesChannelID>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
                    then <ns3:salesChannelType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns3:salesChannelType>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns3:scheduleUserName>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns3:scheduleUserName>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:segment)
                    then <ns3:segment>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns3:segment>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceID)
                    then <ns3:serviceID>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns3:serviceID>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceName)
                    then <ns3:serviceName>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns3:serviceName>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
                    then <ns3:serviceTemplate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns3:serviceTemplate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceType)
                    then <ns3:serviceType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns3:serviceType>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
                    then <ns3:serviceTypeSIAC>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns3:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)
                    then <ns3:serviceWindowEnd>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)}</ns3:serviceWindowEnd>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)
                    then <ns3:serviceWindowStart>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)}</ns3:serviceWindowStart>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
                    then <ns3:severityProblem>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns3:severityProblem>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
                    then <ns3:signatureDate>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns3:signatureDate>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
                    then <ns3:slaWindowEnd>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns3:slaWindowEnd>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
                    then <ns3:slaWindowStart>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns3:slaWindowStart>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns3:sourceSystem>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns3:sourceSystem>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)
                    then <ns3:standardDrivingTime>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)}</ns3:standardDrivingTime>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDuration)
                    then <ns3:standardDuration>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:standardDuration)}</ns3:standardDuration>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:startTime)
                    then <ns3:startTime>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:startTime)}</ns3:startTime>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns3:status>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:status)}</ns3:status>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)
                    then <ns3:timeOfAssignment>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)}</ns3:timeOfAssignment>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)
                    then <ns3:timeOfBooking>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)}</ns3:timeOfBooking>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
                    then <ns3:troubleAction>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns3:troubleAction>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
                    then <ns3:troubleDescription>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns3:troubleDescription>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
                    then <ns3:troubleDiagnosis>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns3:troubleDiagnosis>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:type)
                    then <ns3:type>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:type)}</ns3:type>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation)
                    then <ns3:absoluteLocalLocation>
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
                            then <ns3:X>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns3:X>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
                            then <ns3:Y>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns3:Y>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone)
                            then <ns3:timezone>
                                {
                                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)
                                    then <ns3:name>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)}</ns3:name>
                                    else ()
                                }</ns3:timezone>
                            else ()
                        }</ns3:absoluteLocalLocation>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address)
                    then <ns3:address>
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)
                            then <ns3:commune>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)}</ns3:commune>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)
                            then <ns3:city>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)}</ns3:city>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)
                            then <ns3:region>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)}</ns3:region>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)
                            then <ns3:streetName>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)}</ns3:streetName>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)
                            then <ns3:addressReference>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)}</ns3:addressReference>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)
                            then <ns3:buildingType>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)}</ns3:buildingType>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)
                            then <ns3:complentaryAddress>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)}</ns3:complentaryAddress>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)
                            then <ns3:department>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)}</ns3:department>
                            else ()
                        }</ns3:address>
                    else ()
                }
                <ns3:customerAccount>
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)
                        then <ns3:alias>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns3:alias>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)
                        then <ns3:customerGroup>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns3:customerGroup>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)
                        then <ns3:portabilityIndicator>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns3:portabilityIndicator>
                        else ()
                    }
                    <ns3:contact>
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber)
                            then <ns3:alternativePhoneNumber>
                                <ns3:number>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns3:number></ns3:alternativePhoneNumber>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone)
                            then <ns3:cellPhone>
                                <ns3:number>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns3:number></ns3:cellPhone>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email)
                            then <ns3:email>
                                <ns3:eMailAddress>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns3:eMailAddress></ns3:email>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification)
                            then <ns3:IndividualIdentification>
                                <ns3:number>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns3:number></ns3:IndividualIdentification>
                            else ()
                        }
                        {
                            if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName)
                            then <ns3:IndividualName>
                                <ns3:formatedName>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns3:formatedName></ns3:IndividualName>
                            else ()
                        }
                    </ns3:contact>
                </ns3:customerAccount>
                <ns3:Language>
                    <ns3:alphabetName>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Language/ns1:alphabetName)}</ns3:alphabetName>
                </ns3:Language>
                <ns3:PartyResource>
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)
                        then <ns3:ID>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns3:ID>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)
                        then <ns3:internalID>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)}</ns3:internalID>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)
                        then <ns3:resourceTimeZone>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)}</ns3:resourceTimeZone>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)
                        then <ns3:UTCOffset>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)}</ns3:UTCOffset>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ResourceTimeZoneIANA)
                        then <ns3:ResourceTimeZoneIANA>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ResourceTimeZoneIANA)}</ns3:ResourceTimeZoneIANA>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification)
                        then <ns3:IndividualIdentification>
                            <ns3:number>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns3:number>
                            {
                                if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)
                                then <ns3:type>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)}</ns3:type>
                                else ()
                            }</ns3:IndividualIdentification>
                        else ()
                    }





                </ns3:PartyResource>
                <ns3:Product>
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                        then <ns3:ANISQuantity>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns3:ANISQuantity>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                        then <ns3:ANISQuantityRequired>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns3:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)
                        then <ns3:Category>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)}</ns3:Category>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                        then <ns3:DECOQuantity>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns3:DECOQuantity>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                        then <ns3:DECOQuantityRequired>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns3:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)
                        then <ns3:Equipment>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)}</ns3:Equipment>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                        then <ns3:layerMPLS>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns3:layerMPLS>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                        then <ns3:ngnModel>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns3:ngnModel>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                        then <ns3:PHONEQuantity>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns3:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                        then <ns3:externalProductCode>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns3:externalProductCode>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                        then <ns3:externalProductDescription>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns3:externalProductDescription>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                        then <ns3:STBQuantity>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns3:STBQuantity>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)
                        then <ns3:VoiceIndicator>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)}</ns3:VoiceIndicator>
                        else ()
                    }
                    {
                        if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                        then <ns3:WIFIExtensorQuantity>{fn:data($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns3:WIFIExtensorQuantity>
                        else ()
                    }
                </ns3:Product>
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:linkedActivities)
                    then <ns3:linkedActivities>
                        {
                            for $links in $Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:linkedActivities/ns1:links
                            
                                 return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:linkedActivities>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:linkList)
                    then <ns3:linkList>
                        {
                            for $links1 in $Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:linkList/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links1/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links1/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:linkList>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:RequiredInventories)
                    then <ns3:RequiredInventories>
                        {
                            for $links2 in $Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:RequiredInventories/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links2/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links2/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:RequiredInventories>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:ResourcePreferences)
                    then <ns3:ResourcePreferences>
                        {
                            for $links3 in $Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:ResourcePreferences/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links3/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links3/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:ResourcePreferences>
                    else ()
                }
                {
                    if ($Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:workSkill)
                    then <ns3:workSkill>
                        {
                            for $links4 in $Response_CreateWorkOrder_RSP/ns1:Body/ns1:WorkOrder/ns1:workSkill/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links4/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links4/ns1:href)}</ns3:href>
                            </ns3:links>
                        }</ns3:workSkill>
                    else ()
                }
            </ns3:WorkOrder>
        </ns3:Body>
    </ns3:CreateWorkOrderSchedule_RSP>
};

local:func($Response_CreateWOrkOrder_AGE_RSP, $Response_CreateWorkOrder_RSP, $Response_GetWorkOrderSchedule_RSP, $Request_header)