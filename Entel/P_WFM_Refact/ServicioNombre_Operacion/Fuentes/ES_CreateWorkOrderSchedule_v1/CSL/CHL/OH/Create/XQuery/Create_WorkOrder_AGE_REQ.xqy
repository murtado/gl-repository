xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CreateWorkOrder/Create/v1";
(:: import schema at "../../../../../../ES_CreateWorkOrder_AGE_v1/ESC/Primary/CreateWorkOrder_AGE_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CreateWorkOrderSchedule/Create/v1";
(:: import schema at "../../../../../ESC/Primary/CreateWorkOrderSchedule_v1_EBM.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:CreateWorkOrderSchedule_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:CreateWorkOrderSchedule_REQ) ::)) as element() (:: schema-element(ns2:CreateWorkOrder_REQ) ::) {
    <ns2:CreateWorkOrder_REQ/>
};

local:func($Request)