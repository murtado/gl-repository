xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/RequestProgress/Send/v1";
(:: import schema at "../../../../../ESC/Primary/RequestProgress_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $Request as element() (:: schema-element(ns1:RequestProgress_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:RequestProgress_REQ) ::)) as element() (:: schema-element(ns1:RequestProgress_REQ) ::) {
    <ns1:RequestProgress_REQ>
        <ns2:RequestHeader>
            <ns2:Consumer sysCode="{fn:data($Request/ns2:RequestHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($Request/ns2:RequestHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($Request/ns2:RequestHeader/ns2:Consumer/@countryCode)}"> sysCode="{fn:data($Request/ns2:RequestHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($Request/ns2:RequestHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($Request/ns2:RequestHeader/ns2:Consumer/@countryCode)}"</ns2:Consumer>
            <ns2:Trace clientReqTimestamp="{fn:data($Request/ns2:RequestHeader/ns2:Trace/@clientReqTimestamp)}" eventID="{fn:data($Request/ns2:RequestHeader/ns2:Trace/@eventID)}">
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($Request/ns2:RequestHeader/ns2:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($Request/ns2:RequestHeader/ns2:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/@processID)
                    then attribute processID {fn:data($Request/ns2:RequestHeader/ns2:Trace/@processID)}
                    else ()
                }
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/@sourceID)
                    then attribute sourceID {fn:data($Request/ns2:RequestHeader/ns2:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($Request/ns2:RequestHeader/ns2:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/@conversationID)
                    then attribute conversationID {fn:data($Request/ns2:RequestHeader/ns2:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/@correlationID)
                    then attribute correlationID {fn:data($Request/ns2:RequestHeader/ns2:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($Request/ns2:RequestHeader/ns2:Trace/ns2:Service)
                    then 
                        <ns2:Service>
                            {
                                if ($Request/ns2:RequestHeader/ns2:Trace/ns2:Service/@code)
                                then attribute code {fn:data($Request/ns2:RequestHeader/ns2:Trace/ns2:Service/@code)}
                                else ()
                            }
                            {
                                if ($Request/ns2:RequestHeader/ns2:Trace/ns2:Service/@name)
                                then attribute name {fn:data($Request/ns2:RequestHeader/ns2:Trace/ns2:Service/@name)}
                                else ()
                            }
                            {
                                if ($Request/ns2:RequestHeader/ns2:Trace/ns2:Service/@operation)
                                then attribute operation {fn:data($Request/ns2:RequestHeader/ns2:Trace/ns2:Service/@operation)}
                                else ()
                            }
                        </ns2:Service>
                    else ()
                }
            </ns2:Trace>
            {
                if ($Request/ns2:RequestHeader/ns2:Channel)
                then 
                    <ns2:Channel>
                        {
                            if ($Request/ns2:RequestHeader/ns2:Channel/@name)
                            then attribute name {fn:data($Request/ns2:RequestHeader/ns2:Channel/@name)}
                            else ()
                        }
                        {
                            if ($Request/ns2:RequestHeader/ns2:Channel/@mode)
                            then attribute mode {fn:data($Request/ns2:RequestHeader/ns2:Channel/@mode)}
                            else ()
                        }
                    </ns2:Channel>
                else ()
            }
            {
                if ($Request/ns2:RequestHeader/ns3:Result)
                then 
                    <ns3:Result status="{fn:data($Request/ns2:RequestHeader/ns3:Result/@status)}">
                        {
                            if ($Request/ns2:RequestHeader/ns3:Result/@description)
                            then attribute description {fn:data($Request/ns2:RequestHeader/ns3:Result/@description)}
                            else ()
                        }
                        {
                            if ($Request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError)
                            then 
                                <ns4:CanonicalError>
                                    {
                                        if ($Request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@code)
                                        then attribute code {fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@description)
                                        then attribute description {fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@type)
                                        then attribute type {fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns4:CanonicalError>
                            else ()
                        }
                        {
                            if ($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError)
                            then 
                                <ns4:SourceError>
                                    {
                                        if ($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@code)
                                        then attribute code {fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@description)
                                        then attribute description {fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($Request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            else ()
                        }
                        {
                            if ($Request/ns2:RequestHeader/ns3:Result/ns3:CorrelativeErrors)
                            then 
                                <ns3:CorrelativeErrors>
                                    {
                                        for $SourceError in $Request/ns2:RequestHeader/ns3:Result/ns3:CorrelativeErrors/ns4:SourceError
                                        return 
                                        <ns4:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns4:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns4:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns4:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns4:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns4:SourceFault)
                                                then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                                else ()
                                            }
                                        </ns4:SourceError>
                                    }
                                </ns3:CorrelativeErrors>
                            else ()
                        }
                    </ns3:Result>
                else ()
            }
        </ns2:RequestHeader>
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:messageID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:messageID)}</ns1:messageID>
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns1:externalID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns1:externalID>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns1:engineerNotes>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns1:engineerNotes>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource)
                    then 
                        <ns1:PartyResource>
                            <ns1:pname>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</ns1:pname>
                        </ns1:PartyResource>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns1:progressTask>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns1:progressTask>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns1:estimatedFinishDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns1:estimatedFinishDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:userNotification)
                    then <ns1:userNotification>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:userNotification)}</ns1:userNotification>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)
                    then <ns1:dateNotification>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)}</ns1:dateNotification>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns1:sourceSystem>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns1:sourceSystem>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:REQDuration)
                    then <ns1:REQDuration>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:REQDuration)}</ns1:REQDuration>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:details)
                    then <ns1:details>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:details)}</ns1:details>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:actionSD)
                    then <ns1:actionSD>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns1:actionSD>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns1:requestType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns1:requestType>
                    else ()
                }
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:RequestProgress_REQ>
};

local:func($Request)