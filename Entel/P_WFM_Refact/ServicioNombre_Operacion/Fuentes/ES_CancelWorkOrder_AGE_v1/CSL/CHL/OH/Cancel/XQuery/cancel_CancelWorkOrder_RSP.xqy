xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CancelActivity_AGE/Cancel/v1";
(:: import schema at "../../../../../../DC_RA_CHL-AGE_v1/ResourceAdapters/CHL-AGE-AGE_CancelActivity_AGE_RA_v1/CSC/CHL-AGE-AGE_CancelActivity_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/CancelWorkOrder_AGE/Cancel/v1";
(:: import schema at "../../../../../ESC/Primary/CancelWorkOrder_AGE_v1_EBM.xsd" ::)


declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $CancelWorkOrderRaRsp as element() (:: schema-element(ns1:CancelActivity_RSP) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::) external;

declare function local:func($CancelWorkOrderRaRsp as element() (:: schema-element(ns1:CancelActivity_RSP) ::)) as element() (:: schema-element(ns2:CancelWorkOrder_RSP) ::) {
    <ns2:CancelWorkOrder_RSP>
        <ns3:ResponseHeader>
            {$CancelWorkOrderRaRsp/*[1]/*}
        </ns3:ResponseHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)
                    then <ns2:affectedNetwork>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)}</ns2:affectedNetwork>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns2:annulmentTime>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns2:annulmentTime>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)
                    then <ns2:appointmentDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns2:appointmentDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns2:appointmentTimes>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:appointmentTimes>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
                    then <ns2:assignmentTime>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns2:assignmentTime>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:BOAuthotization)
                    then <ns2:BOAuthotization>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:BOAuthotization)}</ns2:BOAuthotization>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:BOUserName)
                    then <ns2:BOUserName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:BOUserName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:BOValidation)
                    then <ns2:BOValidation>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:BOValidation)}</ns2:BOValidation>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
                    then <ns2:broadbandIndicator>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns2:broadbandIndicator>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cabeceraFO)
                    then <ns2:cabeceraFO>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cabeceraFO)}</ns2:cabeceraFO>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cableName)
                    then <ns2:cableName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cableName)}</ns2:cableName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:calculationDate)
                    then <ns2:calculationDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:calculationDate)}</ns2:calculationDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)
                    then <ns2:cancelationReason>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)}</ns2:cancelationReason>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
                    then <ns2:cellComercial>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns2:cellComercial>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:changeRange)
                    then <ns2:changeRange>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:changeRange)}</ns2:changeRange>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:checkList)
                    then <ns2:checkList>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:checkList)}</ns2:checkList>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:closeReason)
                    then <ns2:closeReason>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:closeReason)}</ns2:closeReason>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:contactName)
                    then <ns2:contactName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns2:contactName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns2:contractor>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:contractor>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:copperParameter)
                    then <ns2:copperParameter>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:copperParameter)}</ns2:copperParameter>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:createdDate)
                    then <ns2:createdDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns2:createdDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:curveType)
                    then <ns2:curveType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:curveType)}</ns2:curveType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)
                    then <ns2:customerHostCell>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns2:customerHostCell>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerHostName)
                    then <ns2:customerHostName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</ns2:customerHostName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerValidator)
                    then <ns2:customerValidator>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerValidator)}</ns2:customerValidator>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:decoParameter1)
                    then <ns2:decoParameter1>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:decoParameter1)}</ns2:decoParameter1>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:decoParameter2)
                    then <ns2:decoParameter2>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:decoParameter2)}</ns2:decoParameter2>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:decoParameter3)
                    then <ns2:decoParameter3>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:decoParameter3)}</ns2:decoParameter3>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
                    then <ns2:deliveryImplementacionDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns2:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:depReg)
                    then <ns2:depReg>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns2:depReg>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:derivationReason)
                    then <ns2:derivationReason>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:derivationReason)}</ns2:derivationReason>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:derivationType)
                    then <ns2:derivationType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:derivationType)}</ns2:derivationType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:dispatchNotes)
                    then <ns2:dispatchNotes>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:dispatchNotes)}</ns2:dispatchNotes>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)
                    then <ns2:efficiencyCumplied>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)}</ns2:efficiencyCumplied>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)
                    then <ns2:efficiencyCumpliedReason>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)}</ns2:efficiencyCumpliedReason>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
                    then <ns2:emailComercial>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns2:emailComercial>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
                    then <ns2:emissionDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns2:emissionDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:endTime)
                    then <ns2:endTime>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:endTime)}</ns2:endTime>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns2:engineerNotes>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:engineerNotes>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:entryDate)
                    then <ns2:entryDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns2:entryDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:EPC)
                    then <ns2:EPC>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:EPC)}</ns2:EPC>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns2:estimatedFinishDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:estimatedFinishDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:executedAction)
                    then <ns2:executedAction>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:executedAction)}</ns2:executedAction>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
                    then <ns2:externalStatus>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns2:externalStatus>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalType)
                    then <ns2:externalType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns2:externalType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalZone)
                    then <ns2:externalZone>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns2:externalZone>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:fiberParameter)
                    then <ns2:fiberParameter>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:fiberParameter)}</ns2:fiberParameter>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:finishCoordinates)
                    then <ns2:finishCoordinates>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:finishCoordinates)}</ns2:finishCoordinates>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:finishTime)
                    then <ns2:finishTime>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns2:finishTime>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)
                    then <ns2:forcedAppointment>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)}</ns2:forcedAppointment>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:fume)
                    then <ns2:fume>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:fume)}</ns2:fume>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)
                    then <ns2:futureContactDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)}</ns2:futureContactDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:ID)
                    then <ns2:ID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:InstallationCost)
                    then <ns2:InstallationCost>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:InstallationCost)}</ns2:InstallationCost>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
                    then <ns2:legacyBOUserName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns2:legacyBOUserName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:n2Resp)
                    then <ns2:n2Resp>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:n2Resp)}</ns2:n2Resp>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
                    then <ns2:nameComercial>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns2:nameComercial>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkOrderID)
                    then <ns2:networkOrderID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkOrderID)}</ns2:networkOrderID>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns2:networkResourceID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:networkResourceID>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkRoute)
                    then <ns2:networkRoute>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkRoute)}</ns2:networkRoute>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
                    then <ns2:networkSpeed>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns2:networkSpeed>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns2:networkType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:networkType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:notDoneType)
                    then <ns2:notDoneType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:notDoneType)}</ns2:notDoneType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns2:OFTcloseReason>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns2:OFTcloseType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)
                    then <ns2:OFTsummary>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns2:OFTsummary>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
                    then <ns2:phoneComercial>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns2:phoneComercial>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)
                    then <ns2:prestacionQTY1>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</ns2:prestacionQTY1>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)
                    then <ns2:prestacionQTY2>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</ns2:prestacionQTY2>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)
                    then <ns2:prestacionQTY3>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</ns2:prestacionQTY3>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)
                    then <ns2:prestacionQTY4>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</ns2:prestacionQTY4>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)
                    then <ns2:prestacionQTY5>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</ns2:prestacionQTY5>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion1)
                    then <ns2:prestacion1>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</ns2:prestacion1>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion2)
                    then <ns2:prestacion2>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</ns2:prestacion2>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion3)
                    then <ns2:prestacion3>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</ns2:prestacion3>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion4)
                    then <ns2:prestacion4>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</ns2:prestacion4>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion5)
                    then <ns2:prestacion5>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</ns2:prestacion5>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns2:progressTask>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:progressTask>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:radioParameter)
                    then <ns2:radioParameter>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:radioParameter)}</ns2:radioParameter>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:recordType)
                    then <ns2:recordType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:recordType)}</ns2:recordType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns2:requestType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:requestType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
                    then <ns2:rescheduleRequester>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns2:rescheduleRequester>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:restartTime)
                    then <ns2:restartTime>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:restartTime)}</ns2:restartTime>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
                    then <ns2:salesChannelID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns2:salesChannelID>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
                    then <ns2:salesChannelType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns2:salesChannelType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedID)
                    then <ns2:sapReplacedID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedID)}</ns2:sapReplacedID>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedName)
                    then <ns2:sapReplacedName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedName)}</ns2:sapReplacedName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedQTY)
                    then <ns2:sapReplacedQTY>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedQTY)}</ns2:sapReplacedQTY>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedResourceName)
                    then <ns2:sapReplacedResourceName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sapReplacedResourceName)}</ns2:sapReplacedResourceName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns2:scheduleUserName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:scheduleUserName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:segment)
                    then <ns2:segment>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:segment>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceData)
                    then <ns2:serviceData>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceData)}</ns2:serviceData>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceID)
                    then <ns2:serviceID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns2:serviceID>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceName)
                    then <ns2:serviceName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns2:serviceName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
                    then <ns2:serviceTemplate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns2:serviceTemplate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceType)
                    then <ns2:serviceType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns2:serviceType>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
                    then <ns2:serviceTypeSIAC>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns2:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceValidate)
                    then <ns2:serviceValidate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceValidate)}</ns2:serviceValidate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)
                    then <ns2:serviceWindowEnd>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)}</ns2:serviceWindowEnd>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)
                    then <ns2:serviceWindowStart>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)}</ns2:serviceWindowStart>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
                    then <ns2:severityProblem>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns2:severityProblem>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
                    then <ns2:signatureDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns2:signatureDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:signedDate)
                    then <ns2:signedDate>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:signedDate)}</ns2:signedDate>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
                    then <ns2:slaWindowEnd>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
                    then <ns2:slaWindowStart>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns2:slaWindowStart>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)
                    then <ns2:standardDrivingTime>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)}</ns2:standardDrivingTime>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:standardDuration)
                    then <ns2:standardDuration>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:standardDuration)}</ns2:standardDuration>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)
                    then <ns2:startCoordinates>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)}</ns2:startCoordinates>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:startedTravel)
                    then <ns2:startedTravel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:startedTravel)}</ns2:startedTravel>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:startTime)
                    then <ns2:startTime>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:startTime)}</ns2:startTime>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns2:status>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)
                    then <ns2:ticketCheckList>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ns2:ticketCheckList>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)
                    then <ns2:timeOfAssignment>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)}</ns2:timeOfAssignment>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)
                    then <ns2:timeOfBooking>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)}</ns2:timeOfBooking>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
                    then <ns2:troubleAction>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns2:troubleAction>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
                    then <ns2:troubleDescription>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns2:troubleDescription>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
                    then <ns2:troubleDiagnosis>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns2:troubleDiagnosis>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)
                    then <ns2:troubleTicketReason>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:troubleTicketReason>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleTicketResponsable)
                    then <ns2:troubleTicketResponsable>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:troubleTicketResponsable)}</ns2:troubleTicketResponsable>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:type)
                    then <ns2:type>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:type)}</ns2:type>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:warehouseName)
                    then <ns2:warehouseName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:warehouseName)}</ns2:warehouseName>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:zoneKM)
                    then <ns2:zoneKM>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</ns2:zoneKM>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:accessTech)
                    then <ns2:accessTech>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:accessTech)}</ns2:accessTech>
                    else ()
                }
                {
                    if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
                    then <ns2:accessTechQTY>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:accessTechQTY>
                    else ()
                }
                <ns2:signature>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:signature/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:signature/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:signature>
                <ns2:files>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:files/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:files/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:files>
                <ns2:photo1>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo1/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo1/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:photo1>
                <ns2:photo2>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo2/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo2/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:photo2>
                <ns2:photo3>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo3/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo3/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:photo3>
                <ns2:photo4>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo4/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:photo4/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:photo4>
                <ns2:linkedActivities>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:linkedActivities/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:linkedActivities/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:linkedActivities>
                <ns2:linkList>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:linkList/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:linkList/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:linkList>
                <ns2:RequiredInventories>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:RequiredInventories/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:RequiredInventories/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:RequiredInventories>
                <ns2:ResourcePreferences>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:ResourcePreferences/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:ResourcePreferences/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:ResourcePreferences>
                <ns2:workSkill>
                    <ns2:links>
                        <ns2:rel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:workSkill/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:workSkill/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:workSkill>
                <ns2:WorkSchedule>
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
                        then <ns2:applicableDuring>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:applicableDuring>
                        else ()
                    }
                </ns2:WorkSchedule>
                <ns2:absoluteLocalLocation>
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
                        then <ns2:X>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:X>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
                        then <ns2:Y>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:Y>
                        else ()
                    }
                    <ns2:timezone>
                        {
                            if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)
                            then <ns2:name>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)}</ns2:name>
                            else ()
                        }
                        {
                            if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)
                            then <ns2:timeZoneIANA>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)}</ns2:timeZoneIANA>
                            else ()
                        }
                    </ns2:timezone>
                </ns2:absoluteLocalLocation>
                <ns2:address>
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)
                        then <ns2:commune>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)}</ns2:commune>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)
                        then <ns2:city>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)}</ns2:city>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)
                        then <ns2:region>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)}</ns2:region>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)
                        then <ns2:streetName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)}</ns2:streetName>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)
                        then <ns2:addressReference>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)}</ns2:addressReference>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)
                        then <ns2:buildingType>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)}</ns2:buildingType>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)
                        then <ns2:complentaryAddress>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)}</ns2:complentaryAddress>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)
                        then <ns2:department>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)}</ns2:department>
                        else ()
                    }
                </ns2:address>
                <ns2:customerAccount>
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)
                        then <ns2:alias>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns2:alias>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)
                        then <ns2:customerGroup>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns2:customerGroup>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)
                        then <ns2:portabilityIndicator>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns2:portabilityIndicator>
                        else ()
                    }
                    <ns2:contact>
                        <ns2:alternativePhoneNumber>
                            <ns2:number>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns2:number>
                        </ns2:alternativePhoneNumber>
                        <ns2:cellPhone>
                            <ns2:number>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns2:number>
                        </ns2:cellPhone>
                        <ns2:email>
                            <ns2:eMailAddress>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns2:eMailAddress>
                        </ns2:email>
                        <ns2:IndividualIdentification>
                            <ns2:number>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                            {
                                if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)
                                then <ns2:type>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)}</ns2:type>
                                else ()
                            }
                        </ns2:IndividualIdentification>
                        <ns2:IndividualName>
                            <ns2:formatedName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns2:formatedName>
                        </ns2:IndividualName>
                    </ns2:contact>
                </ns2:customerAccount>
                <ns2:Language>
                    <ns2:alphabetName>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Language/ns1:alphabetName)}</ns2:alphabetName>
                </ns2:Language>
                <ns2:PartyResource>
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)
                        then <ns2:ID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns2:ID>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)
                        then <ns2:internalID>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)}</ns2:internalID>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)
                        then <ns2:resourceTimeZone>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)}</ns2:resourceTimeZone>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)
                        then <ns2:UTCOffset>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)}</ns2:UTCOffset>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZoneIANA)
                        then <ns2:resourceTimeZoneIANA>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZoneIANA)}</ns2:resourceTimeZoneIANA>
                        else ()
                    }
                    <ns2:individualIdentification>
                        <ns2:number>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:number)}</ns2:number>
                        {
                            if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:type)
                            then <ns2:type>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:type)}</ns2:type>
                            else ()
                        }
                    </ns2:individualIdentification>
                </ns2:PartyResource>
                <ns2:Product>
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                        then <ns2:ANISQuantity>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:ANISQuantity>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                        then <ns2:ANISQuantityRequired>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns2:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)
                        then <ns2:category>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)}</ns2:category>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                        then <ns2:DECOQuantity>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns2:DECOQuantity>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                        then <ns2:DECOQuantityRequired>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)
                        then <ns2:equipment>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)}</ns2:equipment>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                        then <ns2:layerMPLS>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns2:layerMPLS>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                        then <ns2:ngnModel>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns2:ngnModel>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                        then <ns2:PHONEQuantity>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns2:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                        then <ns2:externalProductCode>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns2:externalProductCode>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                        then <ns2:externalProductDescription>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns2:externalProductDescription>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                        then <ns2:STBQuantity>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:STBQuantity>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)
                        then <ns2:voiceIndicator>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)}</ns2:voiceIndicator>
                        else ()
                    }
                    {
                        if ($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                        then <ns2:WIFIExtensorQuantity>{fn:data($CancelWorkOrderRaRsp/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns2:WIFIExtensorQuantity>
                        else ()
                    }
                </ns2:Product>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CancelWorkOrder_RSP>
};

local:func($CancelWorkOrderRaRsp)