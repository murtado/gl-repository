xquery version "2004-draft";
(:: pragma bea:schema-type-parameter parameter="$message" type="ns1:message_t" location="../WSDL/outboundAPI_AdvancedWorkflow.xsd" ::)
(:: pragma  parameter="$data" type="anyType" ::)
(:: pragma bea:global-element-return element="ns0:set_message_status" location="../../OutboundGateway/WSDL/outboundAPI.xsd" ::)

declare namespace ns1 = "urn:toatech:agent";
declare namespace ns0 = "urn:toa:outbound";
declare namespace xf = "http://tempuri.org/OFSC/OutboundGateway/XQuery/message_To_setMessageStatus/";

declare function xf:message_To_setMessageStatus($message as element(),
    $status as xs:string,
    $description as xs:string,
    $data as xs:string)
    as element(ns0:set_message_status) {
        <ns0:set_message_status>
            <messages>
                {
                    let $message_t := $message
                    return
                        <message>
                            <message_id>{ data($message/ns1:message_id) }</message_id>
                            <status>{ $status }</status>
                            <description>{ $description }</description>                            
                            <data>{ 
                            if ($data != "") then                            
	                            let $dataParameter := fn:concat('#params?',$data) return
	                            if (fn:string-length($dataParameter) > 255) then 
	                            	fn:substring($dataParameter, 0 , 255) 
	                            else ($dataParameter)
	                         else ""
	                         }</data>
                        </message>
                }
            </messages>
        </ns0:set_message_status>
};

declare variable $message as element() external;
declare variable $status as xs:string external;
declare variable $description as xs:string external;
declare variable $data as xs:string external;

xf:message_To_setMessageStatus($message,
    $status,
    $description,
    $data)