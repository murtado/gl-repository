xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../WSDL/SendMessage_Handler_RequestProgress.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/RequestProgress/Send/v1";
(:: import schema at "../../../ES_SendMessage_RequestProgress_v1/ESC/Primary/RequestProgress_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $cdata as element() (:: schema-element(envelope) ::) external;
declare variable $message_id as xs:string external;

declare function local:func($cdata as element() (:: schema-element(envelope) ::), 
                            $message_id as xs:string) 
                            as element() (:: schema-element(ns1:RequestProgress_REQ) ::) {
    <ns1:RequestProgress_REQ>
        <ns2:RequestHeader>
            <ns2:Consumer sysCode="{fn:data($cdata/A_SOURCE_SYSTEM)}" enterpriseCode="ENTEL-CHL" countryCode="CHL">
            </ns2:Consumer>
            <ns2:Trace clientReqTimestamp="2008-09-28T21:49:45-04:00" reqTimestamp="2014-09-18T20:18:33-04:00" rspTimestamp="2006-08-19T13:27:14-04:00" processID="string" eventID="string" sourceID="string" correlationEventID="string" conversationID="string" correlationID="string">
                <ns2:Service code="WFM_0011" name="SendMessage" operation="RequestProgress">
                </ns2:Service>
            </ns2:Trace>
            <ns2:Channel>
            </ns2:Channel>
            <ns3:Result status="OK">
                <ns4:CanonicalError>
                </ns4:CanonicalError>
                <ns4:SourceError>
                    <ns4:ErrorSourceDetails>
                    </ns4:ErrorSourceDetails>
                    <ns4:SourceFault></ns4:SourceFault>
                </ns4:SourceError>
                <ns3:CorrelativeErrors>
                    <ns4:SourceError>
                        <ns4:ErrorSourceDetails>
                        </ns4:ErrorSourceDetails>
                        <ns4:SourceFault></ns4:SourceFault>
                    </ns4:SourceError>
                </ns3:CorrelativeErrors>
            </ns3:Result>
        </ns2:RequestHeader>
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:messageID>{fn:data($message_id)}</ns1:messageID>
                <ns1:ID>{fn:data($cdata/activityId)}</ns1:ID>
                <ns1:externalID>{fn:data($cdata/apptNumber)}</ns1:externalID>
                <ns1:engineerNotes>{fn:data($cdata/A_ENGINEER_NOTES)}</ns1:engineerNotes>
                <ns1:PartyResource>
                    <ns1:pname>{fn:data($cdata/pname)}</ns1:pname>
                </ns1:PartyResource>
                <ns1:progressTask>{fn:data($cdata/A_PROGRESSTASK)}</ns1:progressTask>
                <ns1:estimatedFinishDate>{fn:data($cdata/A_ESTIMATED_END_DATE)}</ns1:estimatedFinishDate>
                <ns1:userNotification>{fn:data($cdata/sr_uid)}</ns1:userNotification>
                <ns1:dateNotification>{fn:data($cdata/srdate)}</ns1:dateNotification>
                <ns1:sourceSystem>{fn:data($cdata/A_SOURCE_SYSTEM)}</ns1:sourceSystem>
                <ns1:REQDuration>{fn:data($cdata/REQ_Duration)}</ns1:REQDuration>
                <ns1:details>{fn:data($cdata/A_DETAILS)}</ns1:details>
                <ns1:actionSD>{fn:data($cdata/A_ACTION)}</ns1:actionSD>
                <ns1:requestType>{fn:data($cdata/Request_Type)}</ns1:requestType>
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:RequestProgress_REQ>
};

local:func($cdata, $message_id)