xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/Error/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/Error_v1_ESO.xsd" ::)

declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace http="http://www.bea.com/wli/sb/transports/http";
declare namespace tp="http://www.bea.com/wli/sb/transports";
declare namespace json="json";
declare namespace con="http://www.bea.com/wli/sb/context";
declare namespace tran="http://www.bea.com/wli/sb/transports";

declare variable $ErrorCode as xs:string external;
declare variable $ErrorDetails as xs:string external;
declare variable $response-code as xs:string external;
declare variable $response-message as xs:string external;
declare variable $LegacyResponse as element() external;


declare function local:CHL-EOC-EOC_HTTPJSON_Get_SEB($ErrorCode as xs:string, $ErrorDetails as xs:string, $response-code as xs:string,$response-message as xs:string, $LegacyResponse as element()) as element() (:: schema-element(ns1:SourceError) ::) {
    
    
    let $SourceErrorCode	          := $response-code
    let $SourceErrorDescription 	  := $response-message
    let $ErrorSourceCode                  := $ErrorCode
    let $ErrorSourceDetails               := $ErrorDetails
    
    return 
    <ns1:SourceError code="{$SourceErrorCode}" description="{$SourceErrorDescription}">
        <ns1:ErrorSourceDetails source="{$ErrorSourceCode}" details="{$ErrorSourceDetails}"/>
        <ns1:SourceFault>{$LegacyResponse}</ns1:SourceFault>
    </ns1:SourceError>
};


local:CHL-EOC-EOC_HTTPJSON_Get_SEB($ErrorCode , $ErrorDetails ,$response-code ,$response-message ,$LegacyResponse)