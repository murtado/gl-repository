xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/ENTEL_WS_AgregarInfoTrabajo/Update/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_ENTEL_WS_AgregarInfoTrabajo_v1_EBM.xsd" ::)
declare namespace ns2="urn:ENTEL_WS_AgregaInfoTrabajo";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SVC-DSK-SVC-DSK/wsdl/entel_ws_agregainfotrabajo/ENTEL_WS_AgregaInfoTrabajoService.wsdl" ::)

declare variable $Accion_Glosa as xs:string external;
declare variable $Request as element() (:: schema-element(ns1:ENTEL_WS_AgregarInfoTrabajo_REQ) ::) external;

declare function local:func($Accion_Glosa as xs:string, $Request as element() (:: schema-element(ns1:ENTEL_WS_AgregarInfoTrabajo_REQ) ::)) as element() (:: schema-element(ns2:AgregaInfoTrabajo) ::) {
    <ns2:AgregaInfoTrabajo>
        <ns2:Accion>{fn:data($Accion_Glosa)}</ns2:Accion>
        <ns2:Numero_Incidencia>{fn:data($Request/ns1:Body/ns1:AgregaInfoTrabajo/ns1:Numero_Incidencia)}</ns2:Numero_Incidencia>
        <ns2:z1D_Details>{fn:data($Request/ns1:Body/ns1:AgregaInfoTrabajo/ns1:z1D_Details)}</ns2:z1D_Details>
        <ns2:z1D_Summary>{fn:data($Request/ns1:Body/ns1:AgregaInfoTrabajo/ns1:z1D_Summary)}</ns2:z1D_Summary>
    </ns2:AgregaInfoTrabajo>
};

local:func($Accion_Glosa, $Request)