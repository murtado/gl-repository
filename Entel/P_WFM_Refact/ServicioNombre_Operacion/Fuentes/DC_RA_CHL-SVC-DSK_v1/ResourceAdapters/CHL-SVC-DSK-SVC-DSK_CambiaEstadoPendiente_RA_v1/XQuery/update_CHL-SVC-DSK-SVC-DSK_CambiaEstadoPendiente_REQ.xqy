xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/update_CHL-SVC-DSK-SVC-DSK_CambiaEstadoPendiente_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/UpdateWorkOrderLegacy/Update/v1";
(:: import schema at "../../../../ES_UpdateWorkOrderLegacy_v1/ESC/Primary/UpdateWorkOrderLegacy_v1_EBM.xsd" ::)

declare variable $updateWoReq as element() (:: schema-element(ns1:UpdateWorkOrderLegacy_REQ) ::) external;

declare function local:func($updateWoReq as element() (:: schema-element(ns1:UpdateWorkOrderLegacy_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:Accion>18000</ns2:Accion>
        <ns2:MotivoEstado>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:statusReason)}</ns2:MotivoEstado>
        <ns2:NumeroIncidencia>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:NumeroIncidencia>
    </ns2:Root-Element>
};

local:func($updateWoReq)