xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CambiaEstadoPendiente/Update/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoPendiente_v1_EBM.xsd" ::)
declare namespace ns2="urn:ENTEL_WS_CambiaEstadoIncidente";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SVC-DSK-SVC-DSK/wsdl/entel_ws_cambiaestadoincidente/ENTEL_WS_CambiaEstadoIncidenteService.wsdl" ::)

declare variable $Accion_Glosa as  xs:string external;
declare variable $Request as element() (:: schema-element(ns1:CambiaEstadoPendiente_REQ) ::) external;

declare function local:func($Accion_Glosa as  xs:string, 
                            $Request as element() (:: schema-element(ns1:CambiaEstadoPendiente_REQ) ::)) 
                            as element() (:: schema-element(ns2:CambiaEstadoPendiente) ::) {
    <ns2:CambiaEstadoPendiente>
        <ns2:Accion>{fn:data($Accion_Glosa)}</ns2:Accion>
        <ns2:MotivoEstado>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:statusReason)}</ns2:MotivoEstado>
        <ns2:Numero_Incidencia>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:Numero_Incidencia>
    </ns2:CambiaEstadoPendiente>
};

local:func($Accion_Glosa, $Request)