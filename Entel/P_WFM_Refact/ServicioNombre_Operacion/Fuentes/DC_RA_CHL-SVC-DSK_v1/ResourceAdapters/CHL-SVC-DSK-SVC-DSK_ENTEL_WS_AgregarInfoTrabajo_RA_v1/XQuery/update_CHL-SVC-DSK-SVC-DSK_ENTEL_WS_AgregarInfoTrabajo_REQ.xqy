xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/update_CHL-SVC-DSK-SVC-DSK_ENTEL_WS_AgregarInfoTrabajo_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/ENTEL_WS_AgregarInfoTrabajo/Update/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_ENTEL_WS_AgregarInfoTrabajo_v1_EBM.xsd" ::)

declare variable $updateWoReq as element() (:: schema-element(ns1:ENTEL_WS_AgregarInfoTrabajo_REQ) ::) external;

declare function local:func($updateWoReq as element() (:: schema-element(ns1:ENTEL_WS_AgregarInfoTrabajo_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:Accion>16000</ns2:Accion>
        <ns2:Numero_Incidencia>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:Numero_Incidencia>
        <ns2:zlD_Details>Fecha de  Re Agendamiento</ns2:zlD_Details>
        <ns2:zlD_Summary>{fn:data($updateWoReq/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns2:zlD_Summary>
    </ns2:Root-Element>
};

local:func($updateWoReq)