xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CambiaEstadoResuelto/Cancel/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoResuelto_v1_EBM.xsd" ::)
declare namespace ns2="urn:ENTEL_WS_CambiaEstadoIncidente";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SVC-DSK-SVC-DSK/wsdl/entel_ws_cambiaestadoincidente/ENTEL_WS_CambiaEstadoIncidenteService.wsdl" ::)

declare variable $Accion_Glosa as  xs:string external;
declare variable $Request as element() (:: schema-element(ns1:CambiaEstadoResuelto_REQ) ::) external;

declare function local:func($Accion_Glosa as xs:string, 
                            $Request as element() (:: schema-element(ns1:CambiaEstadoResuelto_REQ) ::)) 
                            as element() (:: schema-element(ns2:CambiaEstadoResuelto) ::) {
    <ns2:CambiaEstadoResuelto>
        <ns2:Accion>{fn:data($Accion_Glosa)}</ns2:Accion>
        <ns2:Cierre_Causa>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:closeReasonSD)}</ns2:Cierre_Causa>
        <ns2:Cierre_Metodo_Resolucion>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)}</ns2:Cierre_Metodo_Resolucion>
        <ns2:Cierre_Origen_Cierre>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:originClosing)}</ns2:Cierre_Origen_Cierre>
        <ns2:Cierre_Resolucion>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:Cierre_Resolucion>
        <ns2:Cierre_Resolucion_Tier_1>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)}</ns2:Cierre_Resolucion_Tier_1>
        <ns2:Cierre_Resolucion_Tier_2>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)}</ns2:Cierre_Resolucion_Tier_2>
        <ns2:Cierre_Resolucion_Tier_3>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)}</ns2:Cierre_Resolucion_Tier_3>
        <ns2:Numero_Incidencia>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:Numero_Incidencia>
        <ns2:Responsabilidad_Falla>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:responsability)}</ns2:Responsabilidad_Falla>
    </ns2:CambiaEstadoResuelto>
};

local:func($Accion_Glosa, $Request)