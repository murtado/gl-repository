xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/cancel_CHL-SVC-DSK-SVC-DSK_CambiaEstadoResuelto_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CambiaEstadoResuelto/Cancel/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoResuelto_v1_EBM.xsd" ::)

declare variable $cancelWoReq as element() (:: schema-element(ns1:CambiaEstadoResuelto_REQ) ::) external;

declare function local:func($cancelWoReq as element() (:: schema-element(ns1:CambiaEstadoResuelto_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:Accion>17000</ns2:Accion>
        <ns2:Cierre_causa>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:closeReasonSD)}</ns2:Cierre_causa>
        <ns2:Cierre_Metodo_Resolucion>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)}</ns2:Cierre_Metodo_Resolucion>
        <ns2:Cierre_Origen_Cierre>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:originClosing)}</ns2:Cierre_Origen_Cierre>
        <ns2:Cierre_Resolucion>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:Cierre_Resolucion>
        <ns2:Cierre_Resolucion_Tier_1>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)}</ns2:Cierre_Resolucion_Tier_1>
        <ns2:Cierre_Resolucion_Tier_2>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)}</ns2:Cierre_Resolucion_Tier_2>
        <ns2:Cierre_Resolucion_Tier_3>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)}</ns2:Cierre_Resolucion_Tier_3>
        <ns2:Numero_Incidencia>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:Numero_Incidencia>
        <ns2:Responsabilidad_Falla>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:responsability)}</ns2:Responsabilidad_Falla>
    </ns2:Root-Element>
};

local:func($cancelWoReq)