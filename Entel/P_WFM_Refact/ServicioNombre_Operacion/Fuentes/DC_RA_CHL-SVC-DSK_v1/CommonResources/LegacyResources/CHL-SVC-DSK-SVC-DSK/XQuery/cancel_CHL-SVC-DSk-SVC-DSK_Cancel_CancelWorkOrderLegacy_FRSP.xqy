xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CancelWorkOrderLegacy/Cancel/v1";
(:: import schema at "../XSD/CancelWorkOrderLegacy_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $headerResponseError as element() (:: schema-element(ns1:ResponseHeader) ::) external;

declare function local:func($headerResponseError as element() (:: schema-element(ns1:ResponseHeader) ::)) as element() (:: schema-element(ns2:CancelWorkOrderLegacy_FRSP) ::) {
    <ns2:CancelWorkOrderLegacy_FRSP>
      {$headerResponseError}
        <ns2:Body></ns2:Body>
    </ns2:CancelWorkOrderLegacy_FRSP>
};

local:func($headerResponseError)