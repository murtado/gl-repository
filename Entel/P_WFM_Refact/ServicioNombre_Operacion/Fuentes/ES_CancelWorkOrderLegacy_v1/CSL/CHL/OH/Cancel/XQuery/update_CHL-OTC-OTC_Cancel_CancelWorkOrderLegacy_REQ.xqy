xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CancelWorkOrderLegacy/Cancel/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OTC_v1/ResourceAdapters/CHL-OTC-OTC_CancelaTarea_RA_v1/CSC/CHL-OTC-OTC_CancelaTarea_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $request as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::)) as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::) {
    <ns1:CancelWorkOrderLegacy_REQ>
        <ns2:RequestHeader>
            <ns2:Consumer sysCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@countryCode)}"> sysCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@countryCode)}"</ns2:Consumer>
            <ns2:Trace clientReqTimestamp="{fn:data($request/ns2:RequestHeader/ns2:Trace/@clientReqTimestamp)}" eventID="{fn:data($request/ns2:RequestHeader/ns2:Trace/@eventID)}">
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($request/ns2:RequestHeader/ns2:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($request/ns2:RequestHeader/ns2:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@processID)
                    then attribute processID {fn:data($request/ns2:RequestHeader/ns2:Trace/@processID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@sourceID)
                    then attribute sourceID {fn:data($request/ns2:RequestHeader/ns2:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($request/ns2:RequestHeader/ns2:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@conversationID)
                    then attribute conversationID {fn:data($request/ns2:RequestHeader/ns2:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@correlationID)
                    then attribute correlationID {fn:data($request/ns2:RequestHeader/ns2:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service)
                    then 
                        <ns2:Service>
                            {
                                if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@code)
                                then attribute code {fn:data($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@code)}
                                else ()
                            }
                            {
                                if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@name)
                                then attribute name {fn:data($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@name)}
                                else ()
                            }
                            {
                                if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@operation)
                                then attribute operation {fn:data($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@operation)}
                                else ()
                            }
                        </ns2:Service>
                    else ()
                }
            </ns2:Trace>
            {
                if ($request/ns2:RequestHeader/ns2:Channel)
                then 
                    <ns2:Channel>
                        {
                            if ($request/ns2:RequestHeader/ns2:Channel/@name)
                            then attribute name {fn:data($request/ns2:RequestHeader/ns2:Channel/@name)}
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns2:Channel/@mode)
                            then attribute mode {fn:data($request/ns2:RequestHeader/ns2:Channel/@mode)}
                            else ()
                        }
                    </ns2:Channel>
                else ()
            }
            {
                if ($request/ns2:RequestHeader/ns3:Result)
                then 
                    <ns3:Result status="{fn:data($request/ns2:RequestHeader/ns3:Result/@status)}">
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/@description)
                            then attribute description {fn:data($request/ns2:RequestHeader/ns3:Result/@description)}
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError)
                            then 
                                <ns4:CanonicalError>
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@code)
                                        then attribute code {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@description)
                                        then attribute description {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@type)
                                        then attribute type {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns4:CanonicalError>
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError)
                            then 
                                <ns4:SourceError>
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@code)
                                        then attribute code {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@description)
                                        then attribute description {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/ns3:CorrelativeErrors)
                            then 
                                <ns3:CorrelativeErrors>
                                    {
                                        for $SourceError in $request/ns2:RequestHeader/ns3:Result/ns3:CorrelativeErrors/ns4:SourceError
                                        return 
                                        <ns4:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns4:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns4:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns4:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns4:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns4:SourceFault)
                                                then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                                else ()
                                            }
                                        </ns4:SourceError>
                                    }
                                </ns3:CorrelativeErrors>
                            else ()
                        }
                    </ns3:Result>
                else ()
            }
        </ns2:RequestHeader>
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:notificacionOFSC>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:notificacionOFSC)}</ns1:notificacionOFSC>
                <ns1:idWO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:idWO)}</ns1:idWO>
                <ns1:numeroOXT>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:numeroOXT)}</ns1:numeroOXT>
                <ns1:tipoTermino>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:tipoTermino)}</ns1:tipoTermino>
                <ns1:causaTermino>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:causaTermino)}</ns1:causaTermino>
                <ns1:observacionesTecnico>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:observacionesTecnico)}</ns1:observacionesTecnico>
                <ns1:estadoWO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:estadoWO)}</ns1:estadoWO>
                <ns1:loginOFSC>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:loginOFSC)}</ns1:loginOFSC>
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:CancelWorkOrderLegacy_REQ>
};

local:func($request)