xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CambiaEstadoResuelto/Cancel/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SVC-DSK_v1/ResourceAdapters/CHL-SVC-DSK-SVC-DSK_CambiaEstadoResuelto_RA_v1/CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoResuelto_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CancelWorkOrderLegacy/Cancel/v1";
(:: import schema at "../../../../../ESC/Primary/CancelWorkOrderLegacy_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $request as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::)) as element() (:: schema-element(ns2:CambiaEstadoResuelto_REQ) ::) {
    <ns2:CambiaEstadoResuelto_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@countryCode)}"> sysCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@countryCode)}"</ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($request/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($request/ns3:RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($request/ns3:RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($request/ns3:RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($request/ns3:RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($request/ns3:RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($request/ns3:RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($request/ns3:RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($request/ns3:RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($request/ns3:RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($request/ns3:RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($request/ns3:RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            {
                if ($request/ns3:RequestHeader/ns4:Result)
                then 
                    <ns4:Result status="{fn:data($request/ns3:RequestHeader/ns4:Result/@status)}">
                        {
                            if ($request/ns3:RequestHeader/ns4:Result/@description)
                            then attribute description {fn:data($request/ns3:RequestHeader/ns4:Result/@description)}
                            else ()
                        }
                        {
                            if ($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError)
                            then 
                                <ns5:CanonicalError>
                                    {
                                        if ($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)
                                        then attribute code {fn:data($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)
                                        then attribute description {fn:data($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)
                                        then attribute type {fn:data($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns5:CanonicalError>
                            else ()
                        }
                        {
                            if ($request/ns3:RequestHeader/ns4:Result/ns5:SourceError)
                            then 
                                <ns5:SourceError>
                                    {
                                        if ($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)
                                        then attribute code {fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)
                                        then attribute description {fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            else ()
                        }
                        {
                            if ($request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors)
                            then 
                                <ns4:CorrelativeErrors>
                                    {
                                        for $SourceError in $request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                        return 
                                        <ns5:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns5:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns5:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns5:SourceFault)
                                                then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                                else ()
                                            }
                                        </ns5:SourceError>
                                    }
                                </ns4:CorrelativeErrors>
                            else ()
                        }
                    </ns4:Result>
                else ()
            }
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                <ns2:externalID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                <ns2:sourceSystem>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                <ns2:BOUserName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:BOUserName>
                <ns2:engineerNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:engineerNotes>
                <ns2:status>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
                <ns2:closeReasonSD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closeReasonSD)}</ns2:closeReasonSD>
                <ns2:closingMethodResolution>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)}</ns2:closingMethodResolution>
                <ns2:originClosing>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:originClosing)}</ns2:originClosing>
                <ns2:troubleTicketReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:troubleTicketReason>
                <ns2:troubleTicketReasonTier1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)}</ns2:troubleTicketReasonTier1>
                <ns2:troubleTicketReasonTier2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)}</ns2:troubleTicketReasonTier2>
                <ns2:troubleTicketReasonTier3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)}</ns2:troubleTicketReasonTier3>
                <ns2:responsability>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:responsability)}</ns2:responsability>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CambiaEstadoResuelto_REQ>
};

local:func($request)