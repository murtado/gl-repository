xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/move_CHL-OFC-OFC_MoveActivity_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/MoveActivity/Update/v1";
(:: import schema at "../CSC/CHL-OFC-OFC_MoveActivity_v1_EBM.xsd" ::)

declare variable $moveActivityReq as element() (:: schema-element(ns1:MoveActivity_REQ) ::) external;

declare function local:func($moveActivityReq as element() (:: schema-element(ns1:MoveActivity_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:setDate>
            <ns2:date>{fn:data($moveActivityReq/ns1:Body/ns1:WorkOrder/ns1:SetDate/ns1:appointmentDate)}</ns2:date>
        </ns2:setDate>
        <ns2:setResource>
            <ns2:resourceId>{fn:data($moveActivityReq/ns1:Body/ns1:WorkOrder/ns1:SetResource/ns1:ID)}</ns2:resourceId>
        </ns2:setResource>
    </ns2:Root-Element>
};

local:func($moveActivityReq)