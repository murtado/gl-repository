xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/CHL-OFC-OFC_UpdateActivity_JSON_RSP.xsd" ::)
declare namespace ns3="http://www.entel.cl/EBM/UpdateActivity/Update/v1";
(:: import schema at "../CSC/CHL-OFC-OFC_UpdateActivity_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $updateActivityRs as element() (:: schema-element(ns1:Root-Element) ::) external;
declare variable $result as element() (:: schema-element(ns2:Result) ::) external;

declare function local:func($updateActivityRs as element() (:: schema-element(ns1:Root-Element) ::), 
                            $result as element() (:: schema-element(ns2:Result) ::)) 
                            as element() (:: schema-element(ns3:UpdateActivity_RSP) ::) {
    <ns3:UpdateActivity_RSP>
        <ns4:ResponseHeader>
            <ns2:Result status="{fn:data($result/@status)}">
                {
                    if ($result/@description)
                    then attribute description {fn:data($result/@description)}
                    else ()
                }
                {
                    if ($result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($result/ns5:SourceError/@code)
                                then attribute code {fn:data($result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($result/ns5:SourceError/@description)
                                then attribute description {fn:data($result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($result/ns2:CorrelativeErrors)
                    then 
                        <ns2:CorrelativeErrors>
                            {
                                for $SourceError in $result/ns2:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns2:CorrelativeErrors>
                    else ()
                }
            </ns2:Result>
        </ns4:ResponseHeader>
        <ns3:Body>
            <ns3:WorkOrder>
                <ns3:affectedNetwork>{fn:data($updateActivityRs/ns1:A_AFFECTED_NETWORK)}</ns3:affectedNetwork>
                <ns3:annulmentTime>{fn:data($updateActivityRs/ns1:A_ANNULMENTTIME)}</ns3:annulmentTime>
                <ns3:appointmentDate>{fn:data($updateActivityRs/ns1:date)}</ns3:appointmentDate>
                <ns3:appointmentTimes>{fn:data($updateActivityRs/ns1:A_APPOINTMENTTIMES)}</ns3:appointmentTimes>
                <ns3:assignmentTime>{fn:data($updateActivityRs/ns1:A_ASSIGNMENTTIME)}</ns3:assignmentTime>
                <ns3:BOAuthotization>{fn:data($updateActivityRs/ns1:A_BO_AUTH)}</ns3:BOAuthotization>
                <ns3:BOUserName>{fn:data($updateActivityRs/ns1:A_BO_USERNAME)}</ns3:BOUserName>
                <ns3:BOValidation>{fn:data($updateActivityRs/ns1:A_BACKOFFICEVALIDATION)}</ns3:BOValidation>
                <ns3:broadbandIndicator>{fn:data($updateActivityRs/ns1:A_BROADBAND_IND)}</ns3:broadbandIndicator>
                <ns3:cabeceraFO>{fn:data($updateActivityRs/ns1:A_CABECERA_FO)}</ns3:cabeceraFO>
                <ns3:cableName>{fn:data($updateActivityRs/ns1:A_CABLE_NAME)}</ns3:cableName>
                <ns3:calculationDate>{fn:data($updateActivityRs/ns1:A_CALCULATION_DATE)}</ns3:calculationDate>
                <ns3:cancelationReason>{fn:data($updateActivityRs/ns1:A_CANCELATION_REASON)}</ns3:cancelationReason>
                <ns3:cellComercial>{fn:data($updateActivityRs/ns1:A_COMMERCIAL_CONTACT_CELL)}</ns3:cellComercial>
                <ns3:changeRange>{fn:data($updateActivityRs/ns1:A_CHANGE_RANGE)}</ns3:changeRange>
                <ns3:checkList>{fn:data($updateActivityRs/ns1:A_CHECKLIST)}</ns3:checkList>
                <ns3:closeReason>{fn:data($updateActivityRs/ns1:A_CLOSING_REASON)}</ns3:closeReason>
                <ns3:contactName>{fn:data($updateActivityRs/ns1:A_CONTACT_NAME)}</ns3:contactName>
                <ns3:contractor>{fn:data($updateActivityRs/ns1:A_CONTRACTOR)}</ns3:contractor>
                <ns3:copperParameter>{fn:data($updateActivityRs/ns1:A_COPPER_PARAMETER)}</ns3:copperParameter>
                <ns3:createdDate>{fn:data($updateActivityRs/ns1:A_CREATEDDATE)}</ns3:createdDate>
                <ns3:curveType>{fn:data($updateActivityRs/ns1:A_CURVE_TYPE)}</ns3:curveType>
                <ns3:customerHostCell>{fn:data($updateActivityRs/ns1:A_CUSTOMER_HOST_CELL)}</ns3:customerHostCell>
                <ns3:customerHostName>{fn:data($updateActivityRs/ns1:A_CUSTOMER_HOST_NAME)}</ns3:customerHostName>
                <ns3:customerValidator>{fn:data($updateActivityRs/ns1:A_CUSTOMERVALIDATOR)}</ns3:customerValidator>
                <ns3:decoParameter1>{fn:data($updateActivityRs/ns1:A_DECOPARAMETER_1)}</ns3:decoParameter1>
                <ns3:decoParameter2>{fn:data($updateActivityRs/ns1:A_DECOPARAMETER_2)}</ns3:decoParameter2>
                <ns3:decoParameter3>{fn:data($updateActivityRs/ns1:A_DECOPARAMETER_3)}</ns3:decoParameter3>
                <ns3:deliveryImplementacionDate>{fn:data($updateActivityRs/ns1:A_DELIVERY_DATE_IMPLEMENTATION)}</ns3:deliveryImplementacionDate>
                <ns3:depReg>{fn:data($updateActivityRs/ns1:A_DEP_REG)}</ns3:depReg>
                <ns3:derivationReason>{fn:data($updateActivityRs/ns1:A_DERIVATION_REASON)}</ns3:derivationReason>
                <ns3:derivationType>{fn:data($updateActivityRs/ns1:A_DERIVATION_TYPE)}</ns3:derivationType>
                <ns3:dispatchNotes>{fn:data($updateActivityRs/ns1:A_DISPATCH_NOTES)}</ns3:dispatchNotes>
                <ns3:efficiencyCumplied>{fn:data($updateActivityRs/ns1:A_EFFICIENCY_CUMPLIED)}</ns3:efficiencyCumplied>
                <ns3:efficiencyCumpliedReason>{fn:data($updateActivityRs/ns1:A_EFFICIENCY_CUMPLIED_REASON)}</ns3:efficiencyCumpliedReason>
                <ns3:emailComercial>{fn:data($updateActivityRs/ns1:A_COMMERCIAL_CONTACT_EMAIL)}</ns3:emailComercial>
                <ns3:emissionDate>{fn:data($updateActivityRs/ns1:A_EMISSION_DATE)}</ns3:emissionDate>
                <ns3:endTime>{fn:data($updateActivityRs/ns1:endTime)}</ns3:endTime>
                <ns3:engineerNotes>{fn:data($updateActivityRs/ns1:A_ENGINEER_NOTES)}</ns3:engineerNotes>
                <ns3:entryDate>{fn:data($updateActivityRs/ns1:A_ENTRY_DATE)}</ns3:entryDate>
                <ns3:EPC>{fn:data($updateActivityRs/ns1:A_EPC)}</ns3:EPC>
                <ns3:estimatedFinishDate>{fn:data($updateActivityRs/ns1:A_ESTIMATED_END_DATE)}</ns3:estimatedFinishDate>
                <ns3:executedAction>{fn:data($updateActivityRs/ns1:A_EXECUTED_ACTION)}</ns3:executedAction>
                <ns3:externalID>{fn:data($updateActivityRs/ns1:apptNumber)}</ns3:externalID>
                <ns3:externalNotes>{fn:data($updateActivityRs/ns1:A_NOTES)}</ns3:externalNotes>
                <ns3:externalStatus>{fn:data($updateActivityRs/ns1:A_LEGACY_STATUS)}</ns3:externalStatus>
                <ns3:externalType>{fn:data($updateActivityRs/ns1:A_LEGACY_WORKTYPE)}</ns3:externalType>
                <ns3:externalZone>{fn:data($updateActivityRs/ns1:A_ZONE)}</ns3:externalZone>
                <ns3:fiberParameter>{fn:data($updateActivityRs/ns1:A_FIBER_PARAMETER)}</ns3:fiberParameter>
                <ns3:finishCoordinates>{fn:data($updateActivityRs/ns1:A_FINISH_COORDINATES)}</ns3:finishCoordinates>
                <ns3:finishTime>{fn:data($updateActivityRs/ns1:A_FINISHTIME)}</ns3:finishTime>
                <ns3:forcedAppointment>{fn:data($updateActivityRs/ns1:A_FORCED_APPOINTMENT)}</ns3:forcedAppointment>
                <ns3:fume>{fn:data($updateActivityRs/ns1:A_FUME)}</ns3:fume>
                <ns3:futureContactDate>{fn:data($updateActivityRs/ns1:A_FUTURE_CONTACT_DATE)}</ns3:futureContactDate>
                <ns3:ID>{fn:data($updateActivityRs/ns1:activityId)}</ns3:ID>
                <ns3:InstallationCost>{fn:data($updateActivityRs/ns1:A_INSTALLATIONCOST)}</ns3:InstallationCost>
                <ns3:legacyBOUserName>{fn:data($updateActivityRs/ns1:A_LEGACY_BO_USERNAME)}</ns3:legacyBOUserName>
                <ns3:n2Resp>{fn:data($updateActivityRs/ns1:A_N2_RESP)}</ns3:n2Resp>
                <ns3:nameComercial>{fn:data($updateActivityRs/ns1:A_COMMERCIAL_CONTACT_NAME)}</ns3:nameComercial>
                <ns3:networkOrderID>{fn:data($updateActivityRs/ns1:A_NETWORK_ORDER_ID)}</ns3:networkOrderID>
                <ns3:networkResourceID>{fn:data($updateActivityRs/ns1:A_NETWORK_RESOURCES)}</ns3:networkResourceID>
                <ns3:networkRoute>{fn:data($updateActivityRs/ns1:A_NETWORK_ROUTE)}</ns3:networkRoute>
                <ns3:networkSpeed>{fn:data($updateActivityRs/ns1:A_NETWORK_SPEED)}</ns3:networkSpeed>
                <ns3:networkType>{fn:data($updateActivityRs/ns1:A_NETWORK_TYPE)}</ns3:networkType>
                <ns3:notDoneType>{fn:data($updateActivityRs/ns1:A_NOT_DONE_TYPE)}</ns3:notDoneType>
                <ns3:OFTcloseReason>{fn:data($updateActivityRs/ns1:A_OFT_CLOSE_REASON)}</ns3:OFTcloseReason>
                <ns3:OFTcloseType>{fn:data($updateActivityRs/ns1:A_OFT_CLOSE_TYPE)}</ns3:OFTcloseType>
                <ns3:OFTsummary>{fn:data($updateActivityRs/ns1:A_OFT_SUMMARY)}</ns3:OFTsummary>
                <ns3:phoneComercial>{fn:data($updateActivityRs/ns1:A_COMMERCIAL_CONTACT_PHONE)}</ns3:phoneComercial>
                <ns3:prestacionQTY1>{fn:data($updateActivityRs/ns1:A_Q_PRESTACION_1)}</ns3:prestacionQTY1>
                <ns3:prestacionQTY2>{fn:data($updateActivityRs/ns1:A_Q_PRESTACION_2)}</ns3:prestacionQTY2>
                <ns3:prestacionQTY3>{fn:data($updateActivityRs/ns1:A_Q_PRESTACION_3)}</ns3:prestacionQTY3>
                <ns3:prestacionQTY4>{fn:data($updateActivityRs/ns1:A_Q_PRESTACION_4)}</ns3:prestacionQTY4>
                <ns3:prestacionQTY5>{fn:data($updateActivityRs/ns1:A_Q_PRESTACION_5)}</ns3:prestacionQTY5>
                <ns3:prestacion1>{fn:data($updateActivityRs/ns1:A_PRESTACION_1)}</ns3:prestacion1>
                <ns3:prestacion2>{fn:data($updateActivityRs/ns1:A_PRESTACION_2)}</ns3:prestacion2>
                <ns3:prestacion3>{fn:data($updateActivityRs/ns1:A_PRESTACION_3)}</ns3:prestacion3>
                <ns3:prestacion4>{fn:data($updateActivityRs/ns1:A_PRESTACION_4)}</ns3:prestacion4>
                <ns3:prestacion5>{fn:data($updateActivityRs/ns1:A_PRESTACION_5)}</ns3:prestacion5>
                <ns3:progressTask>{fn:data($updateActivityRs/ns1:A_PROGRESSTASK)}</ns3:progressTask>
                <ns3:radioParameter>{fn:data($updateActivityRs/ns1:A_RADIO_PARAMETER)}</ns3:radioParameter>
                <ns3:recordType>{fn:data($updateActivityRs/ns1:recordType)}</ns3:recordType>
                <ns3:requestType>{fn:data($updateActivityRs/ns1:A_REQUESTED_TYPE)}</ns3:requestType>
                <ns3:rescheduleRequester>{fn:data($updateActivityRs/ns1:A_RESCHEDULE_REQUESTER)}</ns3:rescheduleRequester>
                <ns3:restartTime>{fn:data($updateActivityRs/ns1:A_RESTART_TIME)}</ns3:restartTime>
                <ns3:salesChannelID>{fn:data($updateActivityRs/ns1:A_SALES_CHANNEL_ID)}</ns3:salesChannelID>
                <ns3:salesChannelType>{fn:data($updateActivityRs/ns1:A_SALES_CHANNEL_TYPE)}</ns3:salesChannelType>
                <ns3:sapReplacedID>{fn:data($updateActivityRs/ns1:A_SAP_REPLACED_ID)}</ns3:sapReplacedID>
                <ns3:sapReplacedName>{fn:data($updateActivityRs/ns1:A_SAP_REPLACED_NAME)}</ns3:sapReplacedName>
                <ns3:sapReplacedQTY>{fn:data($updateActivityRs/ns1:A_SAP_REPLACED_QTY)}</ns3:sapReplacedQTY>
                <ns3:sapReplacedResourceName>{fn:data($updateActivityRs/ns1:A_SAP_REPLACED_RESOURCE_NAME)}</ns3:sapReplacedResourceName>
                <ns3:scheduleUserName>{fn:data($updateActivityRs/ns1:A_SCHEDULER_USERNAME)}</ns3:scheduleUserName>
                <ns3:segment>{fn:data($updateActivityRs/ns1:A_SEGMENT)}</ns3:segment>
                <ns3:serviceData>{fn:data($updateActivityRs/ns1:A_SERVICE_DATA)}</ns3:serviceData>
                <ns3:serviceID>{fn:data($updateActivityRs/ns1:A_SERVICE_ID)}</ns3:serviceID>
                <ns3:serviceName>{fn:data($updateActivityRs/ns1:A_SERVICE)}</ns3:serviceName>
                <ns3:serviceTemplate>{fn:data($updateActivityRs/ns1:A_SERVICE_TEMPLATE)}</ns3:serviceTemplate>
                <ns3:serviceType>{fn:data($updateActivityRs/ns1:A_SERVICE_TYPE)}</ns3:serviceType>
                <ns3:serviceTypeSIAC>{fn:data($updateActivityRs/ns1:A_SERVICE_TYPE_SIAC)}</ns3:serviceTypeSIAC>
                <ns3:serviceValidate>{fn:data($updateActivityRs/ns1:A_SERVICE_VALIDATED)}</ns3:serviceValidate>
                <ns3:severityProblem>{fn:data($updateActivityRs/ns1:A_PRIORITY)}</ns3:severityProblem>
                <ns3:signatureDate>{fn:data($updateActivityRs/ns1:A_SIGNATURE_DATE)}</ns3:signatureDate>
                <ns3:siglaActividad>{fn:data($updateActivityRs/ns1:A_SIGLA_ACTIVIDAD)}</ns3:siglaActividad>
                <ns3:slaWindowEnd>{fn:data($updateActivityRs/ns1:slaWindowEnd)}</ns3:slaWindowEnd>
                <ns3:slaWindowStart>{fn:data($updateActivityRs/ns1:slaWindowStart)}</ns3:slaWindowStart>
                <ns3:sourceSystem>{fn:data($updateActivityRs/ns1:A_SOURCE_SYSTEM)}</ns3:sourceSystem>
                <ns3:standardDrivingTime>{fn:data($updateActivityRs/ns1:travelTime)}</ns3:standardDrivingTime>
                <ns3:standardDuration>{fn:data($updateActivityRs/ns1:duration)}</ns3:standardDuration>
                <ns3:startCoordinates>{fn:data($updateActivityRs/ns1:A_START_COORDINATES)}</ns3:startCoordinates>
                <ns3:startedTravel>{fn:data($updateActivityRs/ns1:A_STARTEDTRAVEL)}</ns3:startedTravel>
                <ns3:startTime>{fn:data($updateActivityRs/ns1:startTime)}</ns3:startTime>
                <ns3:status>{fn:data($updateActivityRs/ns1:status)}</ns3:status>
                <ns3:ticketCheckList>{fn:data($updateActivityRs/ns1:A_TICKET_CHECKLIST)}</ns3:ticketCheckList>
                <ns3:timeOfBooking>{fn:data($updateActivityRs/ns1:timeOfBooking)}</ns3:timeOfBooking>
                <ns3:troubleAction>{fn:data($updateActivityRs/ns1:A_TROUBLE_ACTION)}</ns3:troubleAction>
                <ns3:troubleDescription>{fn:data($updateActivityRs/ns1:A_TROUBLE_DESCRIPTION)}</ns3:troubleDescription>
                <ns3:troubleDiagnosis>{fn:data($updateActivityRs/ns1:A_TROUBLE_DIAGNOSIS)}</ns3:troubleDiagnosis>
                <ns3:troubleTicketReason>{fn:data($updateActivityRs/ns1:A_TROUBLETICKET_REASON)}</ns3:troubleTicketReason>
                <ns3:troubleTicketResponsable>{fn:data($updateActivityRs/ns1:A_TROUBLETICKET_RESPONSABLE)}</ns3:troubleTicketResponsable>
                <ns3:type>{fn:data($updateActivityRs/ns1:activityType)}</ns3:type>
                <ns3:warehouseName>{fn:data($updateActivityRs/ns1:A_WAREHOUSE_NAME)}</ns3:warehouseName>
                <ns3:zoneKM>{fn:data($updateActivityRs/ns1:A_ZONE_KM)}</ns3:zoneKM>
                <ns3:accessTech>{fn:data($updateActivityRs/ns1:A_ACCESS_TECHNOLOGY)}</ns3:accessTech>
                <ns3:accessTechQTY>{fn:data($updateActivityRs/ns1:A_ACCESS_TECH_QTY)}</ns3:accessTechQTY>
                <ns3:signature>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:A_SIGNATURE/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:A_SIGNATURE/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:signature>
                <ns3:files>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:A_FILE/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:A_FILE/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:files>
                <ns3:photo1>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:A_PHOTO_1/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:A_PHOTO_1/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:photo1>
                <ns3:photo2>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:A_PHOTO_2/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:A_PHOTO_2/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:photo2>
                <ns3:photo3>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:A_PHOTO_3/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:A_PHOTO_3/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:photo3>
                <ns3:photo4>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:A_PHOTO_4/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:A_PHOTO_4/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:photo4>
                <ns3:linkedActivities>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:linkedActivities/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:linkedActivities/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:linkedActivities>
                <ns3:linkList>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:linkList>
                <ns3:RequiredInventories>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:requiredInventories/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:requiredInventories/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:RequiredInventories>
                <ns3:ResourcePreferences>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:resourcePreferences/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:resourcePreferences/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:ResourcePreferences>
                <ns3:workSkill>
                    <ns3:links>
                        <ns3:rel>{fn:data($updateActivityRs/ns1:workSkills/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($updateActivityRs/ns1:workSkills/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:workSkill>
                <ns3:WorkSchedule>
                    <ns3:applicableDuring>{fn:data($updateActivityRs/ns1:timeSlot)}</ns3:applicableDuring>
                </ns3:WorkSchedule>
                <ns3:absoluteLocalLocation>
                    <ns3:X>{fn:data($updateActivityRs/ns1:latitude)}</ns3:X>
                    <ns3:Y>{fn:data($updateActivityRs/ns1:longitude)}</ns3:Y>
                    <ns3:timezone>
                        <ns3:name>{fn:data($updateActivityRs/ns1:timeZone)}</ns3:name>
                        <ns3:timeZoneIANA>{fn:data($updateActivityRs/ns1:timeZoneIANA)}</ns3:timeZoneIANA>
                    </ns3:timezone>
                </ns3:absoluteLocalLocation>
                <ns3:address>
                    <ns3:commune>{fn:data($updateActivityRs/ns1:A_COMUNA)}</ns3:commune>
                    <ns3:city>{fn:data($updateActivityRs/ns1:city)}</ns3:city>
                    <ns3:region>{fn:data($updateActivityRs/ns1:stateProvince)}</ns3:region>
                    <ns3:streetName>{fn:data($updateActivityRs/ns1:streetAddress)}</ns3:streetName>
                    <ns3:addressReference>{fn:data($updateActivityRs/ns1:A_ADDRESS_REFERENCE)}</ns3:addressReference>
                    <ns3:buildingType>{fn:data($updateActivityRs/ns1:A_BUILDING_TYPE)}</ns3:buildingType>
                    <ns3:complentaryAddress>{fn:data($updateActivityRs/ns1:A_COMPLENTARY_ADDRESS)}</ns3:complentaryAddress>
                    <ns3:department>{fn:data($updateActivityRs/ns1:A_DEPARTAMENT)}</ns3:department>
                </ns3:address>
                <ns3:customerAccount>
                    <ns3:alias>{fn:data($updateActivityRs/ns1:A_ALIAS)}</ns3:alias>
                    <ns3:customerGroup>{fn:data($updateActivityRs/ns1:A_GROUP)}</ns3:customerGroup>
                    <ns3:portabilityIndicator>{fn:data($updateActivityRs/ns1:A_PORTABILITY)}</ns3:portabilityIndicator>
                    <ns3:contact>
                        <ns3:alternativePhoneNumber>
                            <ns3:number>{fn:data($updateActivityRs/ns1:customerPhone)}</ns3:number>
                        </ns3:alternativePhoneNumber>
                        <ns3:cellPhone>
                            <ns3:number>{fn:data($updateActivityRs/ns1:customerCell)}</ns3:number>
                        </ns3:cellPhone>
                        <ns3:email>
                            <ns3:eMailAddress>{fn:data($updateActivityRs/ns1:customerEmail)}</ns3:eMailAddress>
                        </ns3:email>
                        <ns3:IndividualIdentification>
                            <ns3:number>{fn:data($updateActivityRs/ns1:customerNumber)}</ns3:number>
                        </ns3:IndividualIdentification>
                        <ns3:IndividualName>
                            <ns3:formatedName>{fn:data($updateActivityRs/ns1:customerName)}</ns3:formatedName>
                        </ns3:IndividualName>
                    </ns3:contact>
                </ns3:customerAccount>
                <ns3:Language>
                    <ns3:alphabetName>{fn:data($updateActivityRs/ns1:language)}</ns3:alphabetName>
                </ns3:Language>
                <ns3:PartyResource>
                    <ns3:ID>{fn:data($updateActivityRs/ns1:resourceId)}</ns3:ID>
                    <ns3:internalID>{fn:data($updateActivityRs/ns1:resourceInternalId)}</ns3:internalID>
                    <ns3:resourceTimeZone>{fn:data($updateActivityRs/ns1:resourceTimeZone)}</ns3:resourceTimeZone>
                    <ns3:UTCOffset>{fn:data($updateActivityRs/ns1:resourceTimeZoneDiff)}</ns3:UTCOffset>
                    <ns3:resourceTimeZoneIANA>{fn:data($updateActivityRs/ns1:resourceTimeZoneIANA)}</ns3:resourceTimeZoneIANA>
                    <ns3:individualIdentification>
                        <ns3:number>{fn:data($updateActivityRs/ns1:A_INSTALLER_NUMBER)}</ns3:number>
                    </ns3:individualIdentification>
                </ns3:PartyResource>
                <ns3:Product>
                    <ns3:ANISQuantity>{fn:data($updateActivityRs/ns1:A_ANIS_EQUIP_QTY)}</ns3:ANISQuantity>
                    <ns3:ANISQuantityRequired>{fn:data($updateActivityRs/ns1:A_ANIS_EQUIP_QTY_REQ)}</ns3:ANISQuantityRequired>
                    <ns3:category>{fn:data($updateActivityRs/ns1:A_CATEGORY)}</ns3:category>
                    <ns3:DECOQuantity>{fn:data($updateActivityRs/ns1:A_DECO_QTY)}</ns3:DECOQuantity>
                    <ns3:DECOQuantityRequired>{fn:data($updateActivityRs/ns1:A_EQUIP_QTY_REQ)}</ns3:DECOQuantityRequired>
                    <ns3:equipment>{fn:data($updateActivityRs/ns1:A_EQUIPMENTS)}</ns3:equipment>
                    <ns3:layerMPLS>{fn:data($updateActivityRs/ns1:A_LAYER_MPLS)}</ns3:layerMPLS>
                    <ns3:ngnModel>{fn:data($updateActivityRs/ns1:A_NGN_MODEL)}</ns3:ngnModel>
                    <ns3:PHONEQuantity>{fn:data($updateActivityRs/ns1:A_PHONE_EQUIP_QTY)}</ns3:PHONEQuantity>
                    <ns3:externalProductCode>{fn:data($updateActivityRs/ns1:A_PRODUCT_CODE)}</ns3:externalProductCode>
                    <ns3:externalProductDescription>{fn:data($updateActivityRs/ns1:A_PRODUCT_DESC)}</ns3:externalProductDescription>
                    <ns3:STBQuantity>{fn:data($updateActivityRs/ns1:A_STB_EQUIP_QTY)}</ns3:STBQuantity>
                    <ns3:voiceIndicator>{fn:data($updateActivityRs/ns1:A_VOICE_IND)}</ns3:voiceIndicator>
                    <ns3:WIFIExtensorQuantity>{fn:data($updateActivityRs/ns1:A_WIFI_EQUIP_QTY)}</ns3:WIFIExtensorQuantity>
                </ns3:Product>
            </ns3:WorkOrder>
        </ns3:Body>
    </ns3:UpdateActivity_RSP>
};

local:func($updateActivityRs, $result)