xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/SuspendActivity/Send/v1";
(:: import schema at "../../../../../ESC/Primary/SuspendActivity_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/SuspenderActividad/Send/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OTC_v1/ResourceAdapters/CHL-OTC-OTC_SuspenderActividad_RA_v1/CSC/SuspenderActividad_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $request as element() (:: schema-element(ns1:SuspendActivity_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:SuspendActivity_REQ) ::)) as element() (:: schema-element(ns2:SuspenderActividad_REQ) ::) {
    <ns2:SuspenderActividad_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="" eventID="">
                <ns3:Service>
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel>
            </ns3:Channel>
            <ns4:Result status="">
                <ns5:CanonicalError>
                </ns5:CanonicalError>
                <ns5:SourceError>
                    <ns5:ErrorSourceDetails>
                    </ns5:ErrorSourceDetails>
                    <ns5:SourceFault></ns5:SourceFault>
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError>
                        <ns5:ErrorSourceDetails>
                        </ns5:ErrorSourceDetails>
                        <ns5:SourceFault></ns5:SourceFault>
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:getCancelaTarea>
                <ns2:arg0>
                    <ns2:notificacionOFSC>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:notificacionOFSC>
                    <ns2:idWO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:idWO>
                    <ns2:numeroOXT>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:numeroOXT>
                    <ns2:horaEstReinicio>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:restartTime)}</ns2:horaEstReinicio>
                    <ns2:motivo>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closeReason)}</ns2:motivo>
                    <ns2:observacionesTecnico>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:observacionesTecnico>
                    <ns2:ticketBO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ticketBO)}</ns2:ticketBO>
                    <ns2:estadoWO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:estadoWO>
                </ns2:arg0>
            </ns2:getCancelaTarea>
        </ns2:Body>
    </ns2:SuspenderActividad_REQ>
};

local:func($request)