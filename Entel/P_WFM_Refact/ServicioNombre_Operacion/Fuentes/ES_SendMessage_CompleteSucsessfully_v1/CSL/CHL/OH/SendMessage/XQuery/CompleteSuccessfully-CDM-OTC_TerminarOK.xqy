xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CambiaEstadoFinalizadoExitosamente/Send/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OTC_v1/ResourceAdapters/CHL-OTC-OTC_CambiaEstadoFinalizadoExitosamente_RA_v1/CSC/CambiaEstadoFinalizadoExitosamente_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CompleteSuccessfully/Send/v1";
(:: import schema at "../../../../../ESC/Primary/EBM/CompleteSuccessfully_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $requestOTC as element() (:: schema-element(ns1:CompleteSuccessfully_REQ) ::) external;

declare function local:func($requestOTC as element() (:: schema-element(ns1:CompleteSuccessfully_REQ) ::)) as element() (:: schema-element(ns2:CambiaEstadoFinalizadoExitosamente_REQ) ::) {
    <ns2:CambiaEstadoFinalizadoExitosamente_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($requestOTC/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($requestOTC/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}"
                countryCode="{fn:data($requestOTC/ns3:RequestHeader/ns3:Consumer/@countryCode)}">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" reqTimestamp="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}" rspTimestamp="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}" processID="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@processID)}" eventID="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@eventID)}" sourceID="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@sourceID)}" correlationEventID="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@correlationEventID)}" conversationID="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@conversationID)}" correlationID="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/@correlationID)}">
                <ns3:Service code="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}" name="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}" operation="{fn:data($requestOTC/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}">
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel name="{fn:data($requestOTC/ns3:RequestHeader/ns3:Channel/@name)}" mode="{fn:data($requestOTC/ns3:RequestHeader/ns3:Channel/@mode)}">
            </ns3:Channel>
            <ns4:Result status="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/@status)}" description="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/@description)}">
                <ns5:CanonicalError code="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}" description="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}" type="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}">
                </ns5:CanonicalError>
                <ns5:SourceError code="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}" description="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}">
                    <ns5:ErrorSourceDetails source="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                    </ns5:ErrorSourceDetails>
                    {
                        if ($requestOTC/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                        then <ns5:SourceFault>{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                        else ()
                    }
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError code="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@code)}" description="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@description)}">
                        <ns5:ErrorSourceDetails source="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                        </ns5:ErrorSourceDetails>
                        {
                            if ($requestOTC/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)
                            then <ns5:SourceFault>{fn:data($requestOTC/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                            else ()
                        }
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
                    then <ns2:accessTechQTY>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:accessTechQTY>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:actionSD)
                    then <ns2:actionSD>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns2:actionSD>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:BOUserName)
                    then <ns2:BOUserName>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:BOUserName>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:checkList)
                    then <ns2:checkList>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:checkList)}</ns2:checkList>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)
                    then <ns2:closingMethodResolution>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)}</ns2:closingMethodResolution>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:closingReasonSD)
                    then <ns2:closingReasonSD>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:closingReasonSD)}</ns2:closingReasonSD>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns2:contractor>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:contractor>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)
                    then <ns2:customerHostCell>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns2:customerHostCell>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:customerHostName)
                    then <ns2:customerHostName>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</ns2:customerHostName>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:dateNotification)
                    then <ns2:dateNotification>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:dateNotification)}</ns2:dateNotification>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:deliveryPlace)
                    then <ns2:deliveryPlace>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:deliveryPlace)}</ns2:deliveryPlace>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)
                    then <ns2:efficiencyCumplied>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)}</ns2:efficiencyCumplied>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)
                    then <ns2:efficiencyCumpliedReason>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)}</ns2:efficiencyCumpliedReason>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns2:engineerNotes>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:engineerNotes>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:EPC)
                    then <ns2:EPC>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:EPC)}</ns2:EPC>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:fume)
                    then <ns2:fume>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:fume)}</ns2:fume>
                    else ()
                }
                <ns2:ID>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns2:networkType>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:networkType>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns2:OFTcloseReason>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns2:OFTcloseType>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:originClosing)
                    then <ns2:originClosing>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:originClosing)}</ns2:originClosing>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:plcPrestation)
                    then <ns2:plcPrestation>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:plcPrestation)}</ns2:plcPrestation>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion1)
                    then <ns2:prestacion1>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</ns2:prestacion1>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion2)
                    then <ns2:prestacion2>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</ns2:prestacion2>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion3)
                    then <ns2:prestacion3>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</ns2:prestacion3>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion4)
                    then <ns2:prestacion4>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</ns2:prestacion4>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion5)
                    then <ns2:prestacion5>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</ns2:prestacion5>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)
                    then <ns2:prestacionQTY1>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</ns2:prestacionQTY1>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)
                    then <ns2:prestacionQTY2>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</ns2:prestacionQTY2>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)
                    then <ns2:prestacionQTY3>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</ns2:prestacionQTY3>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)
                    then <ns2:prestacionQTY4>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</ns2:prestacionQTY4>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)
                    then <ns2:prestacionQTY5>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</ns2:prestacionQTY5>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns2:progressTask>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:progressTask>
                    else ()
                }
                <ns2:responsability>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:responsability)}</ns2:responsability>
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns2:status>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:subject)
                    then <ns2:subject>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:subject)}</ns2:subject>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)
                    then <ns2:ticketCheckList>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ns2:ticketCheckList>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)
                    then <ns2:troubleTicketReason>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:troubleTicketReason>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)
                    then <ns2:troubleTicketReasonTier1>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)}</ns2:troubleTicketReasonTier1>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)
                    then <ns2:troubleTicketReasonTier2>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)}</ns2:troubleTicketReasonTier2>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)
                    then <ns2:troubleTicketReasonTier3>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)}</ns2:troubleTicketReasonTier3>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:userNotification)
                    then <ns2:userNotification>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:userNotification)}</ns2:userNotification>
                    else ()
                }
                {
                    if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:zoneKM)
                    then <ns2:zoneKM>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</ns2:zoneKM>
                    else ()
                }
                <ns2:PartyResource>
                    {
                        if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)
                        then <ns2:pname>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</ns2:pname>
                        else ()
                    }
                    <ns2:individualIdentification>
                        <ns2:number>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:number)}</ns2:number>
                        {
                            if ($requestOTC/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:type)
                            then <ns2:type>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:type)}</ns2:type>
                            else ()
                        }
                    </ns2:individualIdentification>
                    <ns2:individualName>
                        <ns2:name>{fn:data($requestOTC/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualName/ns1:name)}</ns2:name>
                    </ns2:individualName>
                </ns2:PartyResource>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CambiaEstadoFinalizadoExitosamente_REQ>
};

local:func($requestOTC)