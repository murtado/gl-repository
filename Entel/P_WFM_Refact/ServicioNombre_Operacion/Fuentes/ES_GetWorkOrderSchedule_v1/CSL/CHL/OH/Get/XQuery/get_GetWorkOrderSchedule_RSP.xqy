xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/GetCapacity/Get/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v1/ResourceAdapters/CHL-OFC-OFC_GetCapacity_RA_v1/CSC/CHL-OFC-OFC_GetCapacity_v1_EBM.xsd" ::)
declare namespace ns3="http://www.entel.cl/EBM/GetWorkOrderSchedule/Get/v1";
(:: import schema at "../../../../../ESC/Primary/GetWorkOrderSchedule_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare variable $response as element() (:: schema-element(ns1:GetCapacity_RSP) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::) external;

declare function local:func($response as element() (:: schema-element(ns1:GetCapacity_RSP) ::), 
                            $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::)) 
                            as element() (:: schema-element(ns3:GetWorkOrderSchedule_RSP) ::) {
    <ns3:GetWorkOrderSchedule_RSP>

             
            
        <ns2:ResponseHeader>
            <ns2:Consumer sysCode="{fn:data($RequestHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($RequestHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($RequestHeader/ns2:Consumer/@countryCode)}"/> 
            <ns2:Trace clientReqTimestamp="{fn:data($RequestHeader/ns2:Trace/@clientReqTimestamp)}" eventID="{fn:data($RequestHeader/ns2:Trace/@eventID)}">
                {
                    if ($RequestHeader/ns2:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($RequestHeader/ns2:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($RequestHeader/ns2:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($RequestHeader/ns2:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($RequestHeader/ns2:Trace/@processID)
                    then attribute processID {fn:data($RequestHeader/ns2:Trace/@processID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns2:Trace/@sourceID)
                    then attribute sourceID {fn:data($RequestHeader/ns2:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns2:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($RequestHeader/ns2:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns2:Trace/@conversationID)
                    then attribute conversationID {fn:data($RequestHeader/ns2:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns2:Trace/@correlationID)
                    then attribute correlationID {fn:data($RequestHeader/ns2:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns2:Trace/ns2:Service)
                    then 
                        <ns2:Service>
                            {
                                if ($RequestHeader/ns2:Trace/ns2:Service/@code)
                                then attribute code {fn:data($RequestHeader/ns2:Trace/ns2:Service/@code)}
                                else ()
                            }
                            {
                                if ($RequestHeader/ns2:Trace/ns2:Service/@name)
                                then attribute name {fn:data($RequestHeader/ns2:Trace/ns2:Service/@name)}
                                else ()
                            }
                            {
                                if ($RequestHeader/ns2:Trace/ns2:Service/@operation)
                                then attribute operation {fn:data($RequestHeader/ns2:Trace/ns2:Service/@operation)}
                                else ()
                            }
                        </ns2:Service>
                    else ()
                }
            </ns2:Trace>
            {
                if ($RequestHeader/ns2:Channel)
                then 
                    <ns2:Channel>
                        {
                            if ($RequestHeader/ns2:Channel/@name)
                            then attribute name {fn:data($RequestHeader/ns2:Channel/@name)}
                            else ()
                        }
                        {
                            if ($RequestHeader/ns2:Channel/@mode)
                            then attribute mode {fn:data($RequestHeader/ns2:Channel/@mode)}
                            else ()
                        }
                    </ns2:Channel>
                else ()
            }
            <ns4:Result status="{fn:data($response/ns2:ResponseHeader/ns4:Result/@status)}">
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/@description)
                    then attribute description {fn:data($response/ns2:ResponseHeader/ns4:Result/@description)}
                    else ()
                }
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@code)
                                then attribute code {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@description)
                                then attribute description {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/ns4:CorrelativeErrors)
                    then 
                        <ns4:CorrelativeErrors>
                            {
                                for $SourceError in $response/ns2:ResponseHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns4:CorrelativeErrors>
                    else ()
                }
            </ns4:Result>
        </ns2:ResponseHeader>
        <ns3:Body>
            {
                if ($response/ns1:Body/ns1:Allocation)
                then <ns3:Allocation>
                    {
                        if ($response/ns1:Body/ns1:Allocation/ns1:StandardDuration)
                        then <ns3:StandardDuration>{fn:data($response/ns1:Body/ns1:Allocation/ns1:StandardDuration)}</ns3:StandardDuration>
                        else ()
                    }
                    {
                        if ($response/ns1:Body/ns1:Allocation/ns1:StandardDrivingTime)
                        then <ns3:StandardDrivingTime>{fn:data($response/ns1:Body/ns1:Allocation/ns1:StandardDrivingTime)}</ns3:StandardDrivingTime>
                        else ()
                    }
                    {
                        if ($response/ns1:Body/ns1:Allocation/ns1:Capacity)
                        then <ns3:Capacity>
                            {
                                for $AvailableCapacity in $response/ns1:Body/ns1:Allocation/ns1:Capacity/ns1:AvailableCapacity
                                return 
                                <ns3:AvailableCapacity>
                                    <ns3:AlocationDate>{xs:dateTime(concat(fn:string(fn:data($AvailableCapacity/ns1:AlocationDate)),"T12:00:00-04:00"))}</ns3:AlocationDate>
                                    {
                                        if ($AvailableCapacity/ns1:ApplicableDuring)
                                        then <ns3:ApplicableDuring>{fn:data($AvailableCapacity/ns1:ApplicableDuring)}</ns3:ApplicableDuring>
                                        else ()
                                    }
                                    {
                                        if ($AvailableCapacity/ns1:PartyRoleName)
                                        then <ns3:PartyRoleName>{fn:data($AvailableCapacity/ns1:PartyRoleName)}</ns3:PartyRoleName>
                                        else ()
                                    }
                                    <ns3:Quota>{fn:data($AvailableCapacity/ns1:Quota)}</ns3:Quota>
                                    <ns3:TimeAvailable>{fn:data($AvailableCapacity/ns1:TimeAvailable)}</ns3:TimeAvailable>
                                    {
                                        if ($AvailableCapacity/ns1:PlaceID)
                                        then <ns3:PlaceID>{fn:data($AvailableCapacity/ns1:PlaceID)}</ns3:PlaceID>
                                        else ()
                                    }</ns3:AvailableCapacity>
                            }
                            {
                                for $DuringInfo in $response/ns1:Body/ns1:Allocation/ns1:Capacity/ns1:DuringInfo
                                return 
                                <ns3:DuringInfo>
                                    <ns3:Name>{fn:data($DuringInfo/ns1:Name)}</ns3:Name>
                                    <ns3:Label>{fn:data($DuringInfo/ns1:Label)}</ns3:Label>
                                    {
                                        if ($DuringInfo/ns1:TimeFrom)
                                        then <ns3:TimeFrom>{fn:data($DuringInfo/ns1:TimeFrom)}</ns3:TimeFrom>
                                        else ()
                                    }
                                    {
                                        if ($DuringInfo/ns1:TimeTo)
                                        then <ns3:TimeTo>{fn:data($DuringInfo/ns1:TimeTo)}</ns3:TimeTo>
                                        else ()
                                    }</ns3:DuringInfo>
                            }
                        </ns3:Capacity>
                        else ()
                    }</ns3:Allocation>
                else ()
            }
        </ns3:Body>
    </ns3:GetWorkOrderSchedule_RSP>
};

local:func($response, $RequestHeader)