xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/GetCapacity/Get/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v1/ResourceAdapters/CHL-OFC-OFC_GetCapacity_RA_v1/CSC/CHL-OFC-OFC_GetCapacity_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/GetWorkOrderSchedule/Get/v1";
(:: import schema at "../../../../../ESC/Primary/GetWorkOrderSchedule_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Request as element() (:: schema-element(ns1:GetWorkOrderSchedule_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:GetWorkOrderSchedule_REQ) ::)) as element() (:: schema-element(ns2:GetCapacity_REQ) ::) {
    <ns2:GetCapacity_REQ>
         {$Request/*[1]}
        <ns2:Body>
            <ns2:Allocation>

                <ns2:Datelist>
                    {
                       for $AllocationDate in $Request/ns1:Body/ns1:Allocation/ns1:Datelist/ns1:AllocationDate
                        return 
                        <ns2:AllocationDate>{fn:data($AllocationDate)}</ns2:AllocationDate>
                    }
                </ns2:Datelist>
                
                    {
                        if($Request/ns1:Body/ns1:Allocation/ns1:Place/ns1:ID)then
                        let $ID := $Request/ns1:Body/ns1:Allocation/ns1:Place/ns1:ID
                        return 
                       <ns2:Place><ns2:ID>{fn:data($ID)}</ns2:ID></ns2:Place>
                        else
                        ()
                    }
               
                {
                    if ($Request/ns1:Body/ns1:Allocation/ns1:IsCalculatedDurationRequired)
                    then <ns2:IsCalculatedDurationRequired>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:IsCalculatedDurationRequired)}</ns2:IsCalculatedDurationRequired>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:Allocation/ns1:IsCalculatedTravelRequired)
                    then <ns2:IsCalculatedTravelRequired>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:IsCalculatedTravelRequired)}</ns2:IsCalculatedTravelRequired>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:Allocation/ns1:IsCalculatedSkillRequired)
                    then <ns2:IsCalculatedSkillRequired>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:IsCalculatedSkillRequired)}</ns2:IsCalculatedSkillRequired>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:Allocation/ns1:WorkSchedule)
                    then <ns2:WorkSchedule>
                        {
                            let $ApplicableDuring := $Request/ns1:Body/ns1:Allocation/ns1:WorkSchedule/ns1:ApplicableDuring
                            return 
                            <ns2:ApplicableDuring>{fn:data($ApplicableDuring)}</ns2:ApplicableDuring>
                        }</ns2:WorkSchedule>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:Allocation/ns1:PartyRole)
                    then <ns2:PartyRole>
                        {
                            let $Name := $Request/ns1:Body/ns1:Allocation/ns1:PartyRole/ns1:Name
                            return 
                            <ns2:Name>{fn:data($Name)}</ns2:Name>
                        }</ns2:PartyRole>
                    else ()
                }
                <ns2:IsReturnTimeSlotInfo>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:IsReturnTimeSlotInfo)}</ns2:IsReturnTimeSlotInfo>
                <ns2:IsDetermineLocationByWorkZone>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:IsDetermineLocationByWorkZone)}</ns2:IsDetermineLocationByWorkZone>
                <ns2:IsNotAggregateResults>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:IsNotAggregateResults)}</ns2:IsNotAggregateResults>
                {
                    if ($Request/ns1:Body/ns1:Allocation/ns1:RemainingTime)
                    then <ns2:RemainingTime>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:RemainingTime)}</ns2:RemainingTime>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:Allocation/ns1:ReserveFor)
                    then <ns2:ReserveFor>{fn:data($Request/ns1:Body/ns1:Allocation/ns1:ReserveFor)}</ns2:ReserveFor>
                    else ()
                }
                {
                    for $AllocationSpecChar in $Request/ns1:Body/ns1:Allocation/ns1:AllocationSpecChar
                    return 
                    <ns2:AllocationSpecChar>
                        <ns2:name>{fn:data($AllocationSpecChar/ns1:name)}</ns2:name>
                        <ns2:value>{fn:data($AllocationSpecChar/ns1:value)}</ns2:value>
                    </ns2:AllocationSpecChar>
                }
            </ns2:Allocation>
        </ns2:Body>
    </ns2:GetCapacity_REQ>
};

local:func($Request)