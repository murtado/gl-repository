xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/Error/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/Error_v1_ESO.xsd" ::)
declare namespace http="http://www.bea.com/wli/sb/transports/http";
declare namespace json="json";
declare namespace tran="http://www.bea.com/wli/sb/transports";
declare variable $LegacyResponse as element() external;
declare variable $LegacyResult as element() external;

declare function local:CHL-OFC-OFC_HTTPJSON_SEB($LegacyResponse as element(),$LegacyResult as element() ) as element() (:: schema-element(ns1:SourceError) ::) {
    
    let $SourceErrorCode	          := local:getSourceErrorCode($LegacyResult)
    let $SourceErrorDescription 	  := local:getSourceErrorDescription($LegacyResult)
    let $ErrorSourceCode                  := "CHL-OFC"
    let $ErrorSourceDetails               := "CHL-OFC-OFC_HTTPJSON_TW"
    
    return 
    <ns1:SourceError code="{$SourceErrorCode}" description="{$SourceErrorDescription}">
        <ns1:ErrorSourceDetails source="{$ErrorSourceCode}" details="{$ErrorSourceDetails}"/>
        {
        if(data($LegacyResult/http:http-response-code)!='200') then
        <ns1:SourceFault>{$LegacyResponse/*}</ns1:SourceFault>
        else
        <ns1:SourceFault/>
        }
    </ns1:SourceError>
};

declare function local:getSourceErrorCode($LegacyResult as element()) as xs:string
{
	
	let $FaultErrorCode := if (exists($LegacyResult/http:http-response-code))
                                      then data($LegacyResult/http:http-response-code) 
                                      else data('404')
	
	return $FaultErrorCode
};

declare function local:getSourceErrorDescription($LegacyResult as element()) as xs:string
{
        
        let $FaultErrorDescription := if (exists($LegacyResult/http:http-response-code))
                                      then if(data($LegacyResult/http:http-response-code) = '200') then
                                        ('OK')
                                      else data('Not Found')
                                      else ('No Legacy Response')
	return $FaultErrorDescription
};

local:CHL-OFC-OFC_HTTPJSON_SEB($LegacyResponse,$LegacyResult)