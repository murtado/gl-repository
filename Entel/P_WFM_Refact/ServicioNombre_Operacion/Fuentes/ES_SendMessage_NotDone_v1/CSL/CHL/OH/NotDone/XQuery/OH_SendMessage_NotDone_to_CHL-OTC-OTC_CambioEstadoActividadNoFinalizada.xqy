xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CambiaEstadoActividadNoFinalizada/Send/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OTC_v1/ResourceAdapters/CHL-OTC-OTC_CambiaEstadoActividadNoFinalizada_RA_v1/CSC/CambiaEstadoActividadNoFinalizada_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/NotDone/Send/v1";
(:: import schema at "../../../../../ESC/Primary/NotDone_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $request as element() (:: schema-element(ns1:NotDone_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:NotDone_REQ) ::)) as element() (:: schema-element(ns2:CambiaEstadoActividadNoFinalizada_REQ) ::) {
    <ns2:CambiaEstadoActividadNoFinalizada_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="" eventID="">
                <ns3:Service>
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel>
            </ns3:Channel>
            <ns4:Result status="">
                <ns5:CanonicalError>
                </ns5:CanonicalError>
                <ns5:SourceError>
                    <ns5:ErrorSourceDetails>
                    </ns5:ErrorSourceDetails>
                    <ns5:SourceFault></ns5:SourceFault>
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError>
                        <ns5:ErrorSourceDetails>
                        </ns5:ErrorSourceDetails>
                        <ns5:SourceFault></ns5:SourceFault>
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                <ns2:PartyResource>
                    <ns2:pname></ns2:pname>
                    <ns2:IndividualIdentification>
                        <ns2:number>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                    </ns2:IndividualIdentification>
                </ns2:PartyResource>
                <ns2:number></ns2:number>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)
                    then <ns2:customerHostCell>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns2:customerHostCell>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns2:engineerNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:engineerNotes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns2:progressTask>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:progressTask>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns2:OFTcloseType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns2:OFTcloseReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
                    then <ns2:accessTechQTY>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:accessTechQTY>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:EPC)
                    then <ns2:EPC>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:EPC)}</ns2:EPC>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:checkList)
                    then <ns2:checkList>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:checkList)}</ns2:checkList>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)
                    then <ns2:ticketCheckList>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ns2:ticketCheckList>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:zoneKM)
                    then <ns2:zoneKM>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</ns2:zoneKM>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns2:status>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion1)
                    then <ns2:prestacion1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</ns2:prestacion1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion2)
                    then <ns2:prestacion2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</ns2:prestacion2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion3)
                    then <ns2:prestacion3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</ns2:prestacion3>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion4)
                    then <ns2:prestacion4>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</ns2:prestacion4>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion5)
                    then <ns2:prestacion5>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</ns2:prestacion5>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)
                    then <ns2:prestacionQTY1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</ns2:prestacionQTY1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)
                    then <ns2:prestacionQTY2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</ns2:prestacionQTY2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)
                    then <ns2:prestacionQTY3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</ns2:prestacionQTY3>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)
                    then <ns2:prestacionQTY4>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</ns2:prestacionQTY4>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)
                    then <ns2:prestacionQTY5>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</ns2:prestacionQTY5>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:PLCPrestation1)
                    then <ns2:PLCPrestation1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PLCPrestation1)}</ns2:PLCPrestation1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:deliveryPlace)
                    then <ns2:deliveryPlace>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:deliveryPlace)}</ns2:deliveryPlace>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns2:contractor>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:contractor>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)
                    then <ns2:BOUserName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:BOUserName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:ticketBO)
                    then <ns2:ticketBO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ticketBO)}</ns2:ticketBO>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)
                    then <ns2:troubleTicketReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:troubleTicketReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:actionSD)
                    then <ns2:actionSD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns2:actionSD>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:closingReasonSD)
                    then <ns2:closingReasonSD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closingReasonSD)}</ns2:closingReasonSD>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)
                    then <ns2:troubleTicketReasonTier1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)}</ns2:troubleTicketReasonTier1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)
                    then <ns2:troubleTicketReasonTier2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)}</ns2:troubleTicketReasonTier2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)
                    then <ns2:troubleTicketReasonTier3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)}</ns2:troubleTicketReasonTier3>
                    else ()
                }
                <ns2:responsability>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:responsability)}</ns2:responsability>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)
                    then <ns2:closingMethodResolution>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)}</ns2:closingMethodResolution>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:originClosing)
                    then <ns2:originClosing>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:originClosing)}</ns2:originClosing>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:subject)
                    then <ns2:subject>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:subject)}</ns2:subject>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:userNotification)
                    then <ns2:userNotification>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:userNotification)}</ns2:userNotification>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)
                    then <ns2:dateNotification>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)}</ns2:dateNotification>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CambiaEstadoActividadNoFinalizada_REQ>
};

local:func($request)