xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/UpdateActivity/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v1/ResourceAdapters/CHL-OFC-OFC_UpdateActivity_RA_v1/CSC/CHL-OFC-OFC_UpdateActivity_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/UpdateWorkOrder/Update/v1";
(:: import schema at "../../../../../ESC/Primary/UpdateWorkOrder_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $updateWorkOrderRq as element() (:: schema-element(ns1:UpdateWorkOrder_REQ) ::) external;

declare function local:func($updateWorkOrderRq as element() (:: schema-element(ns1:UpdateWorkOrder_REQ) ::)) as element() (:: schema-element(ns2:UpdateActivity_REQ) ::) {
    <ns2:UpdateActivity_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Consumer/@countryCode)}"></ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($updateWorkOrderRq/ns3:RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            {
                if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result)
                then 
                    <ns4:Result status="{fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/@status)}">
                        {
                            if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/@description)
                            then attribute description {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/@description)}
                            else ()
                        }
                        {
                            if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError)
                            then 
                                <ns5:CanonicalError>
                                    {
                                        if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)
                                        then attribute code {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)
                                        then attribute description {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)
                                        then attribute type {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns5:CanonicalError>
                            else ()
                        }
                        {
                            if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError)
                            then 
                                <ns5:SourceError>
                                    {
                                        if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)
                                        then attribute code {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)
                                        then attribute description {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            else ()
                        }
                        {
                            if ($updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors)
                            then 
                                <ns4:CorrelativeErrors>
                                    {
                                        for $SourceError in $updateWorkOrderRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                        return 
                                        <ns5:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns5:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns5:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns5:SourceFault)
                                                then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                                else ()
                                            }
                                        </ns5:SourceError>
                                    }
                                </ns4:CorrelativeErrors>
                            else ()
                        }
                    </ns4:Result>
                else ()
            }
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns2:appointmentTimes>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:appointmentTimes>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:calculationDate)
                    then <ns2:calculationDate>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:calculationDate)}</ns2:calculationDate>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)
                    then <ns2:cancelationReason>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)}</ns2:cancelationReason>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns2:engineerNotes>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:engineerNotes>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns2:estimatedFinishDate>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:estimatedFinishDate>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)
                    then <ns2:forcedAppointment>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)}</ns2:forcedAppointment>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)
                    then <ns2:futureContactDate>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)}</ns2:futureContactDate>
                    else ()
                }
                <ns2:ID>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns2:networkResourceID>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:networkResourceID>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns2:progressTask>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:progressTask>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns2:scheduleUserName>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:scheduleUserName>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)
                    then <ns2:siglaActividad>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)}</ns2:siglaActividad>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:accessTech)
                    then <ns2:accessTech>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:accessTech)}</ns2:accessTech>
                    else ()
                }
                {
                    if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
                    then <ns2:accessTechQTY>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:accessTechQTY>
                    else ()
                }
                <ns2:absoluteLocalLocation>
                    {
                        if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
                        then <ns2:X>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:X>
                        else ()
                    }
                    {
                        if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
                        then <ns2:Y>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:Y>
                        else ()
                    }
                </ns2:absoluteLocalLocation>
                <ns2:WorkSchedule>
                    {
                        if ($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
                        then <ns2:applicableDuring>{fn:data($updateWorkOrderRq/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:applicableDuring>
                        else ()
                    }
                </ns2:WorkSchedule>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:UpdateActivity_REQ>
};

local:func($updateWorkOrderRq)