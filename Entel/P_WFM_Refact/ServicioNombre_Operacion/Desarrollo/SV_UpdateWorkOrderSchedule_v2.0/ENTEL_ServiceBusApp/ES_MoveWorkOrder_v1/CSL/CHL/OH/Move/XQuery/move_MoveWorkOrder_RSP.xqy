xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/MoveActivity/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v1/ResourceAdapters/CHL-OFC-OFC_MoveActivity_RA_v1/CSC/CHL-OFC-OFC_MoveActivity_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/MoveWorkOrder/Update/v1";
(:: import schema at "../../../../../ESC/Primary/MoveWorkOrder_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)


declare variable $response as element() (:: schema-element(ns1:MoveActivity_RSP) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::) external;



declare function local:func($RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::),
                             $response as element() (:: schema-element(ns1:MoveActivity_RSP) ::)) as element() (:: schema-element(ns2:MoveWorkOrder_RSP) ::) {
    <ns2:MoveWorkOrder_RSP>
        <ns3:ResponseHeader>
            <ns3:Consumer sysCode="{fn:data($RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($RequestHeader/ns3:Consumer/@countryCode)}"> </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            <ns4:Result status="{fn:data($response/ns3:ResponseHeader/ns4:Result/@status)}">
                {
                    if ($response/ns3:ResponseHeader/ns4:Result/@description)
                    then attribute description {fn:data($response/ns3:ResponseHeader/ns4:Result/@description)}
                    else ()
                }
                {
                    if ($response/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($response/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($response/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@code)
                                then attribute code {fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@description)
                                then attribute description {fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($response/ns3:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($response/ns3:ResponseHeader/ns4:Result/ns4:CorrelativeErrors)
                    then 
                        <ns4:CorrelativeErrors>
                            {
                                for $SourceError in $response/ns3:ResponseHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns4:CorrelativeErrors>
                    else ()
                }
            </ns4:Result>
        </ns3:ResponseHeader>
        <ns2:Body></ns2:Body>
    </ns2:MoveWorkOrder_RSP>
};

local:func($RequestHeader, $response)