xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;

declare namespace ns2="http://www.entel.cl/EBM/Consumer/get/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/get_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/get/v1";
(:: import schema at "../../../../../ESC/Primary/get_Consumer_v1_EBM.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/EBO/Consumer/v1";

declare variable $Get_Consumer_RSP as element() (:: schema-element(ns1:Get_Consumer_RSP) ::) external;

declare function local:func($Get_Consumer_RSP as element() (:: schema-element(ns1:Get_Consumer_RSP) ::)) as element() (:: schema-element(ns2:Get_Consumer_RSP) ::) {
    <ns2:Get_Consumer_RSP>
       {nssif_header:get_ResponseHeader($Get_Consumer_RSP/*:ResponseHeader)}
        <ns2:Body>
            <ns2:Consumer>
                <ns2:ConsumerName>{fn:data($Get_Consumer_RSP/*[2]/ns3:Consumer/ns3:ConsumerName)}</ns2:ConsumerName>
                <ns2:ConsumerSurename>{fn:data($Get_Consumer_RSP/*[2]/ns3:Consumer/ns3:ConsumerSurename)}</ns2:ConsumerSurename>
                <ns2:ConsumerAge>{fn:data($Get_Consumer_RSP/*[2]/ns3:Consumer/ns3:ConsumerAge)}</ns2:ConsumerAge>
                <ns2:ConsumerStatus>{fn:data($Get_Consumer_RSP/*[2]/ns3:Consumer/ns3:ConsumerStatus)}</ns2:ConsumerStatus>
            </ns2:Consumer>
        </ns2:Body>
    </ns2:Get_Consumer_RSP>
};

local:func($Get_Consumer_RSP)
