xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/Consumer/del/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/del_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/Consumer/del/v1";
(:: import schema at "../../../../../ESC/Primary/del_Consumer_v1_EBM.xsd" ::)

import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;

declare namespace ns3 = "http://www.entel.cl/EBO/Consumer/v1";


declare variable $delConsumerReq as element() (:: schema-element(ns1:Del_Consumer_REQ) ::) external;

declare function local:func($delConsumerReq as element() (:: schema-element(ns1:Del_Consumer_REQ) ::)) as element() (:: schema-element(ns2:Del_Consumer_REQ) ::) {
    <ns2:Del_Consumer_REQ>
             {nssif_header:get_RequestHeader($delConsumerReq/*:RequestHeader)}
        <ns2:Body>
            <ns3:ConsumerID>{fn:data($delConsumerReq/ns1:Body/ns1:ConsumerID)}</ns3:ConsumerID>
        </ns2:Body>
    </ns2:Del_Consumer_REQ>
};

local:func($delConsumerReq)
