xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/EnterpriseService/v1";

declare function local:get_ESArchitecture() as element(){
    
    (: 
    This function holds the information require to describe this Service's Architecture (ESAS), at the Solution level.
    
    It should be kept updated over each modification made to the service, on which its Solution Architecture is modified.
    :)
    
    <ns1:EnterpriseService name="WorkOrderSchedule" code="WFM_0006" majorVersion="1" minorVersion="0">
	<ns1:ESC>
		<ns1:EBSC>
			<ns1:PrimaryContract name="UpdateScheduleWorkOrder_v2_ESC"/>
		</ns1:EBSC>
	</ns1:ESC>
	<ns1:MPL>
		<ns1:PIF name="UpdateWorkOrderSchedule_PIF"/>
	</ns1:MPL>
	<ns1:CSL>
		<ns1:Country code="CHL">
			<ns1:OH opName="get" opCode="OWFM_99921" name="CHL_update_WorkOrderSchedule"/>
		</ns1:Country>
	</ns1:CSL>
  </ns1:EnterpriseService>
};

local:get_ESArchitecture()