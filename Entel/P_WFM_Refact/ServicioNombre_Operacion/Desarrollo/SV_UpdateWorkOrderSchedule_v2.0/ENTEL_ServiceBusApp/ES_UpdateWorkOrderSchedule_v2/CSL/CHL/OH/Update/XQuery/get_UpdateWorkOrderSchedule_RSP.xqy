xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/EBM/UpdateScheduleWorkOrder/Update/v1";
(:: import schema at "../../../../../ESC/Primary/UpdateScheduleWorkOrder_v2_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/UpdateWorkOrder/Update/v1";
(:: import schema at "../../../../../../ES_UpdateWorkOrder_v1/ESC/Primary/UpdateWorkOrder_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare variable $response as element() (:: schema-element(ns1:UpdateWorkOrder_RSP) ::) external;
declare variable $ResponseHeader as element() (:: schema-element(ns2:ResponseHeader) ::) external;

declare function local:func($response as element() (:: schema-element(ns1:UpdateWorkOrder_RSP) ::), 
                            $ResponseHeader as element() (:: schema-element(ns2:ResponseHeader) ::)) 
                            as element() (:: schema-element(ns3:UpdateScheduleWorkOrder_RSP) ::) {
    <ns3:UpdateScheduleWorkOrder_RSP>
        <ns2:ResponseHeader>
            <ns2:Consumer sysCode="{fn:data($ResponseHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($ResponseHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($ResponseHeader/ns2:Consumer/@countryCode)}"> sysCode="{fn:data($ResponseHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($ResponseHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($ResponseHeader/ns2:Consumer/@countryCode)}"</ns2:Consumer>
            <ns2:Trace clientReqTimestamp="{fn:data($ResponseHeader/ns2:Trace/@clientReqTimestamp)}" eventID="{fn:data($ResponseHeader/ns2:Trace/@eventID)}">
                {
                    if ($ResponseHeader/ns2:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($ResponseHeader/ns2:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns2:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($ResponseHeader/ns2:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns2:Trace/@processID)
                    then attribute processID {fn:data($ResponseHeader/ns2:Trace/@processID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns2:Trace/@sourceID)
                    then attribute sourceID {fn:data($ResponseHeader/ns2:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns2:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($ResponseHeader/ns2:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns2:Trace/@conversationID)
                    then attribute conversationID {fn:data($ResponseHeader/ns2:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns2:Trace/@correlationID)
                    then attribute correlationID {fn:data($ResponseHeader/ns2:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns2:Trace/ns2:Service)
                    then 
                        <ns2:Service>
                            {
                                if ($ResponseHeader/ns2:Trace/ns2:Service/@code)
                                then attribute code {fn:data($ResponseHeader/ns2:Trace/ns2:Service/@code)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns2:Trace/ns2:Service/@name)
                                then attribute name {fn:data($ResponseHeader/ns2:Trace/ns2:Service/@name)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns2:Trace/ns2:Service/@operation)
                                then attribute operation {fn:data($ResponseHeader/ns2:Trace/ns2:Service/@operation)}
                                else ()
                            }
                        </ns2:Service>
                    else ()
                }
            </ns2:Trace>
            {
                if ($ResponseHeader/ns2:Channel)
                then 
                    <ns2:Channel>
                        {
                            if ($ResponseHeader/ns2:Channel/@name)
                            then attribute name {fn:data($ResponseHeader/ns2:Channel/@name)}
                            else ()
                        }
                        {
                            if ($ResponseHeader/ns2:Channel/@mode)
                            then attribute mode {fn:data($ResponseHeader/ns2:Channel/@mode)}
                            else ()
                        }
                    </ns2:Channel>
                else ()
            }
            <ns4:Result status="{fn:data($response/ns2:ResponseHeader/ns4:Result/@status)}">
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/@description)
                    then attribute description {fn:data($response/ns2:ResponseHeader/ns4:Result/@description)}
                    else ()
                }
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@code)
                                then attribute code {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@description)
                                then attribute description {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($response/ns2:ResponseHeader/ns4:Result/ns4:CorrelativeErrors)
                    then 
                        <ns4:CorrelativeErrors>
                            {
                                for $SourceError in $response/ns2:ResponseHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns4:CorrelativeErrors>
                    else ()
                }
            </ns4:Result>
        </ns2:ResponseHeader>
        <ns3:Body>
            <ns3:WorkOrder>
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:accessTech)
                    then <ns3:accessTech>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:accessTech)}</ns3:accessTech>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
                    then <ns3:accessTechQTY>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns3:accessTechQTY>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)
                    then <ns3:affectedNetwork>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)}</ns3:affectedNetwork>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns3:annulmentTime>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns3:annulmentTime>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)
                    then <ns3:appointmentDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns3:appointmentDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns3:appointmentTimes>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns3:appointmentTimes>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
                    then <ns3:assignmentTime>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns3:assignmentTime>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:BOAuthotization)
                    then <ns3:BOAuthotization>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:BOAuthotization)}</ns3:BOAuthotization>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:BOUserName)
                    then <ns3:BOUserName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns3:BOUserName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:BOValidation)
                    then <ns3:BOValidation>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:BOValidation)}</ns3:BOValidation>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
                    then <ns3:broadbandIndicator>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns3:broadbandIndicator>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:cabeceraFO)
                    then <ns3:cabeceraFO>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:cabeceraFO)}</ns3:cabeceraFO>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:cableName)
                    then <ns3:cableName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:cableName)}</ns3:cableName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:calculationDate)
                    then <ns3:calculationDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:calculationDate)}</ns3:calculationDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)
                    then <ns3:cancelationReason>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)}</ns3:cancelationReason>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
                    then <ns3:cellComercial>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns3:cellComercial>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:changeRange)
                    then <ns3:changeRange>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:changeRange)}</ns3:changeRange>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:checkList)
                    then <ns3:checkList>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:checkList)}</ns3:checkList>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:closeReason)
                    then <ns3:closeReason>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:closeReason)}</ns3:closeReason>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:contactName)
                    then <ns3:contactName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns3:contactName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns3:contractor>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns3:contractor>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:copperParameter)
                    then <ns3:copperParameter>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:copperParameter)}</ns3:copperParameter>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:createdDate)
                    then <ns3:createdDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns3:createdDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:curveType)
                    then <ns3:curveType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:curveType)}</ns3:curveType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)
                    then <ns3:customerHostCell>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns3:customerHostCell>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerHostName)
                    then <ns3:customerHostName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</ns3:customerHostName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerValidator)
                    then <ns3:customerValidator>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerValidator)}</ns3:customerValidator>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:decoParameter1)
                    then <ns3:decoParameter1>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:decoParameter1)}</ns3:decoParameter1>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:decoParameter2)
                    then <ns3:decoParameter2>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:decoParameter2)}</ns3:decoParameter2>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:decoParameter3)
                    then <ns3:decoParameter3>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:decoParameter3)}</ns3:decoParameter3>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
                    then <ns3:deliveryImplementacionDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns3:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:depReg)
                    then <ns3:depReg>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns3:depReg>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:derivationReason)
                    then <ns3:derivationReason>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:derivationReason)}</ns3:derivationReason>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:derivationType)
                    then <ns3:derivationType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:derivationType)}</ns3:derivationType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:dispatchNotes)
                    then <ns3:dispatchNotes>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:dispatchNotes)}</ns3:dispatchNotes>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)
                    then <ns3:efficiencyCumplied>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)}</ns3:efficiencyCumplied>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)
                    then <ns3:efficiencyCumpliedReason>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)}</ns3:efficiencyCumpliedReason>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
                    then <ns3:emailComercial>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns3:emailComercial>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
                    then <ns3:emissionDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns3:emissionDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:endTime)
                    then <ns3:endTime>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:endTime)}</ns3:endTime>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns3:engineerNotes>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns3:engineerNotes>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:entryDate)
                    then <ns3:entryDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns3:entryDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:EPC)
                    then <ns3:EPC>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:EPC)}</ns3:EPC>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns3:estimatedFinishDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns3:estimatedFinishDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:executedAction)
                    then <ns3:executedAction>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:executedAction)}</ns3:executedAction>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns3:externalID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns3:externalID>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns3:externalNotes>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns3:externalNotes>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
                    then <ns3:externalStatus>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns3:externalStatus>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:externalType)
                    then <ns3:externalType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns3:externalType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:externalZone)
                    then <ns3:externalZone>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns3:externalZone>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:fiberParameter)
                    then <ns3:fiberParameter>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:fiberParameter)}</ns3:fiberParameter>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:finishCoordinates)
                    then <ns3:finishCoordinates>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:finishCoordinates)}</ns3:finishCoordinates>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:finishTime)
                    then <ns3:finishTime>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns3:finishTime>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)
                    then <ns3:forcedAppointment>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)}</ns3:forcedAppointment>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:fume)
                    then <ns3:fume>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:fume)}</ns3:fume>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)
                    then <ns3:futureContactDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)}</ns3:futureContactDate>
                    else ()
                }
                <ns3:ID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns3:ID>
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:installationCost)
                    then <ns3:installationCost>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns3:installationCost>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
                    then <ns3:legacyBOUserName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns3:legacyBOUserName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:n2Resp)
                    then <ns3:n2Resp>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:n2Resp)}</ns3:n2Resp>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
                    then <ns3:nameComercial>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns3:nameComercial>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:networkOrderID)
                    then <ns3:networkOrderID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:networkOrderID)}</ns3:networkOrderID>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns3:networkResourceID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns3:networkResourceID>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:networkRoute)
                    then <ns3:networkRoute>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:networkRoute)}</ns3:networkRoute>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
                    then <ns3:networkSpeed>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns3:networkSpeed>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns3:networkType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns3:networkType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:notDoneType)
                    then <ns3:notDoneType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:notDoneType)}</ns3:notDoneType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns3:OFTcloseReason>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns3:OFTcloseReason>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns3:OFTcloseType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns3:OFTcloseType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)
                    then <ns3:OFTsummary>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns3:OFTsummary>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
                    then <ns3:phoneComercial>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns3:phoneComercial>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacion1)
                    then <ns3:prestacion1>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</ns3:prestacion1>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacion2)
                    then <ns3:prestacion2>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</ns3:prestacion2>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacion3)
                    then <ns3:prestacion3>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</ns3:prestacion3>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacion4)
                    then <ns3:prestacion4>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</ns3:prestacion4>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacion5)
                    then <ns3:prestacion5>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</ns3:prestacion5>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)
                    then <ns3:prestacionQTY1>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</ns3:prestacionQTY1>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)
                    then <ns3:prestacionQTY2>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</ns3:prestacionQTY2>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)
                    then <ns3:prestacionQTY3>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</ns3:prestacionQTY3>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)
                    then <ns3:prestacionQTY4>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</ns3:prestacionQTY4>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)
                    then <ns3:prestacionQTY5>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</ns3:prestacionQTY5>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns3:progressTask>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns3:progressTask>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:radioParameter)
                    then <ns3:radioParameter>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:radioParameter)}</ns3:radioParameter>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:recordType)
                    then <ns3:recordType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:recordType)}</ns3:recordType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns3:requestType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns3:requestType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
                    then <ns3:rescheduleRequester>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns3:rescheduleRequester>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:restartTime)
                    then <ns3:restartTime>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:restartTime)}</ns3:restartTime>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
                    then <ns3:salesChannelID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns3:salesChannelID>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
                    then <ns3:salesChannelType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns3:salesChannelType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedID)
                    then <ns3:sapReplacedID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedID)}</ns3:sapReplacedID>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedName)
                    then <ns3:sapReplacedName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedName)}</ns3:sapReplacedName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedQTY)
                    then <ns3:sapReplacedQTY>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedQTY)}</ns3:sapReplacedQTY>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedResourceName)
                    then <ns3:sapReplacedResourceName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:sapReplacedResourceName)}</ns3:sapReplacedResourceName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns3:scheduleUserName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns3:scheduleUserName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:segment)
                    then <ns3:segment>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns3:segment>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceData)
                    then <ns3:serviceData>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceData)}</ns3:serviceData>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceID)
                    then <ns3:serviceID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns3:serviceID>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceName)
                    then <ns3:serviceName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns3:serviceName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
                    then <ns3:serviceTemplate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns3:serviceTemplate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceType)
                    then <ns3:serviceType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns3:serviceType>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
                    then <ns3:serviceTypeSIAC>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns3:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceValidate)
                    then <ns3:serviceValidate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceValidate)}</ns3:serviceValidate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)
                    then <ns3:serviceWindowEnd>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceWindowEnd)}</ns3:serviceWindowEnd>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)
                    then <ns3:serviceWindowStart>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:serviceWindowStart)}</ns3:serviceWindowStart>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
                    then <ns3:severityProblem>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns3:severityProblem>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)
                    then <ns3:siglaActividad>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)}</ns3:siglaActividad>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
                    then <ns3:signatureDate>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns3:signatureDate>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
                    then <ns3:slaWindowEnd>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns3:slaWindowEnd>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
                    then <ns3:slaWindowStart>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns3:slaWindowStart>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns3:sourceSystem>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns3:sourceSystem>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)
                    then <ns3:standardDrivingTime>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:standardDrivingTime)}</ns3:standardDrivingTime>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:standardDuration)
                    then <ns3:standardDuration>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:standardDuration)}</ns3:standardDuration>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)
                    then <ns3:startCoordinates>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)}</ns3:startCoordinates>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:startedTravel)
                    then <ns3:startedTravel>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:startedTravel)}</ns3:startedTravel>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:startTime)
                    then <ns3:startTime>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:startTime)}</ns3:startTime>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns3:status>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:status)}</ns3:status>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)
                    then <ns3:ticketCheckList>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ns3:ticketCheckList>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)
                    then <ns3:timeOfAssignment>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:timeOfAssignment)}</ns3:timeOfAssignment>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)
                    then <ns3:timeOfBooking>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:timeOfBooking)}</ns3:timeOfBooking>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
                    then <ns3:troubleAction>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns3:troubleAction>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
                    then <ns3:troubleDescription>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns3:troubleDescription>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
                    then <ns3:troubleDiagnosis>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns3:troubleDiagnosis>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)
                    then <ns3:troubleTicketReason>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns3:troubleTicketReason>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:troubleTicketResponsable)
                    then <ns3:troubleTicketResponsable>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:troubleTicketResponsable)}</ns3:troubleTicketResponsable>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:type)
                    then <ns3:type>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:type)}</ns3:type>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:warehouseName)
                    then <ns3:warehouseName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:warehouseName)}</ns3:warehouseName>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:zoneKM)
                    then <ns3:zoneKM>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</ns3:zoneKM>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:signature)
                    then <ns3:signature>
                        {
                            for $links in $response/ns1:Body/ns1:WorkOrder/ns1:signature/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:signature>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:files)
                    then <ns3:files>
                        {
                            for $links1 in $response/ns1:Body/ns1:WorkOrder/ns1:files/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links1/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links1/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:files>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:photo1)
                    then <ns3:photo1>
                        {
                            for $links2 in $response/ns1:Body/ns1:WorkOrder/ns1:photo1/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links2/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links2/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:photo1>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:photo2)
                    then <ns3:photo2>
                        {
                            for $links3 in $response/ns1:Body/ns1:WorkOrder/ns1:photo2/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links3/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links3/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:photo2>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:photo3)
                    then <ns3:photo3>
                        {
                            for $links4 in $response/ns1:Body/ns1:WorkOrder/ns1:photo3/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links4/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links4/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:photo3>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:photo4)
                    then <ns3:photo4>
                        {
                            for $links5 in $response/ns1:Body/ns1:WorkOrder/ns1:photo4/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links5/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links5/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:photo4>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:linkedActivities)
                    then <ns3:linkedActivities>
                        {
                            for $links6 in $response/ns1:Body/ns1:WorkOrder/ns1:linkedActivities/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links6/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links6/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:linkedActivities>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:linkList)
                    then <ns3:linkList>
                        {
                            for $links7 in $response/ns1:Body/ns1:WorkOrder/ns1:linkList/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links7/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links7/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:linkList>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:RequiredInventories)
                    then <ns3:RequiredInventories>
                        {
                            for $links8 in $response/ns1:Body/ns1:WorkOrder/ns1:RequiredInventories/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links8/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links8/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:RequiredInventories>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:ResourcePreferences)
                    then <ns3:ResourcePreferences>
                        {
                            for $links9 in $response/ns1:Body/ns1:WorkOrder/ns1:ResourcePreferences/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links9/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links9/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:ResourcePreferences>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:workSkill)
                    then <ns3:workSkill>
                        {
                            for $links10 in $response/ns1:Body/ns1:WorkOrder/ns1:workSkill/ns1:links
                            return 
                            <ns3:links>
                                <ns3:rel>{fn:data($links10/ns1:rel)}</ns3:rel>
                                <ns3:href>{fn:data($links10/ns1:href)}</ns3:href></ns3:links>
                        }</ns3:workSkill>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule)
                    then <ns3:WorkSchedule>
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
                            then <ns3:applicableDuring>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns3:applicableDuring>
                            else ()
                        }</ns3:WorkSchedule>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation)
                    then <ns3:AbsoluteLocalLocation>
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
                            then <ns3:X>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns3:X>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
                            then <ns3:Y>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns3:Y>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone)
                            then <ns3:Timezone>
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)
                                    then <ns3:name>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)}</ns3:name>
                                    else ()
                                }
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)
                                    then <ns3:timeZoneIANA>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)}</ns3:timeZoneIANA>
                                    else ()
                                }</ns3:Timezone>
                            else ()
                        }</ns3:AbsoluteLocalLocation>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:Address)
                    then <ns3:Address>
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:commune)
                            then <ns3:commune>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:commune)}</ns3:commune>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:city)
                            then <ns3:city>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:city)}</ns3:city>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:region)
                            then <ns3:region>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:region)}</ns3:region>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:streetName)
                            then <ns3:streetName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:streetName)}</ns3:streetName>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:addressReference)
                            then <ns3:addressReference>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:addressReference)}</ns3:addressReference>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:buildingType)
                            then <ns3:buildingType>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:buildingType)}</ns3:buildingType>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:complentaryAddress)
                            then <ns3:complentaryAddress>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:complentaryAddress)}</ns3:complentaryAddress>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:department)
                            then <ns3:department>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:department)}</ns3:department>
                            else ()
                        }</ns3:Address>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount)
                    then <ns3:CustomerAccount>
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)
                            then <ns3:alias>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns3:alias>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)
                            then <ns3:customerGroup>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns3:customerGroup>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)
                            then <ns3:portabilityIndicator>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns3:portabilityIndicator>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact)
                            then <ns3:Contact>
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber)
                                    then <ns3:alternativePhoneNumber>
                                        <ns3:number>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns3:number></ns3:alternativePhoneNumber>
                                    else ()
                                }
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone)
                                    then <ns3:cellPhone>
                                        <ns3:number>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns3:number></ns3:cellPhone>
                                    else ()
                                }
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email)
                                    then <ns3:email>
                                        <ns3:eMailAddress>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns3:eMailAddress></ns3:email>
                                    else ()
                                }
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification)
                                    then <ns3:IndividualIdentification>
                                        <ns3:number>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns3:number>
                                        {
                                            if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)
                                            then <ns3:type>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)}</ns3:type>
                                            else ()
                                        }</ns3:IndividualIdentification>
                                    else ()
                                }
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName)
                                    then <ns3:IndividualName>
                                        <ns3:formatedName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns3:formatedName></ns3:IndividualName>
                                    else ()
                                }</ns3:Contact>
                            else ()
                        }</ns3:CustomerAccount>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:Language)
                    then <ns3:Language>
                        <ns3:alphabetName>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Language/ns1:alphabetName)}</ns3:alphabetName></ns3:Language>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource)
                    then <ns3:PartyResource>
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)
                            then <ns3:ID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns3:ID>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)
                            then <ns3:internalID>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)}</ns3:internalID>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)
                            then <ns3:resourceTimeZone>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)}</ns3:resourceTimeZone>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)
                            then <ns3:UTCOffset>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)}</ns3:UTCOffset>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZoneIANA)
                            then <ns3:resourceTimeZoneIANA>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZoneIANA)}</ns3:resourceTimeZoneIANA>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification)
                            then <ns3:IndividualIdentification>
                                <ns3:number>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns3:number>
                                {
                                    if ($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)
                                    then <ns3:type>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)}</ns3:type>
                                    else ()
                                }</ns3:IndividualIdentification>
                            else ()
                        }</ns3:PartyResource>
                    else ()
                }
                {
                    if ($response/ns1:Body/ns1:WorkOrder/ns1:Product)
                    then <ns3:Product>
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                            then <ns3:ANISQuantity>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns3:ANISQuantity>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                            then <ns3:ANISQuantityRequired>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns3:ANISQuantityRequired>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)
                            then <ns3:category>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)}</ns3:category>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                            then <ns3:DECOQuantity>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns3:DECOQuantity>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                            then <ns3:DECOQuantityRequired>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns3:DECOQuantityRequired>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)
                            then <ns3:equipment>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)}</ns3:equipment>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                            then <ns3:layerMPLS>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns3:layerMPLS>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                            then <ns3:ngnModel>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns3:ngnModel>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                            then <ns3:PHONEQuantity>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns3:PHONEQuantity>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                            then <ns3:externalProductCode>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns3:externalProductCode>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                            then <ns3:externalProductDescription>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns3:externalProductDescription>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                            then <ns3:STBQuantity>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns3:STBQuantity>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)
                            then <ns3:voiceIndicator>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)}</ns3:voiceIndicator>
                            else ()
                        }
                        {
                            if ($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                            then <ns3:WIFIExtensorQuantity>{fn:data($response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns3:WIFIExtensorQuantity>
                            else ()
                        }</ns3:Product>
                    else ()
                }</ns3:WorkOrder></ns3:Body>
    </ns3:UpdateScheduleWorkOrder_RSP>
};

local:func($response, $ResponseHeader)
