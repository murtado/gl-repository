xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/UpdateScheduleWorkOrder/Update/v1";
(:: import schema at "../../../../../ESC/Primary/UpdateScheduleWorkOrder_v2_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/UpdateWorkOrder/Update/v1";
(:: import schema at "../../../../../../ES_UpdateWorkOrder_v1/ESC/Primary/UpdateWorkOrder_v1_EBM.xsd" ::)

declare variable $request as element() (:: schema-element(ns1:UpdateScheduleWorkOrder_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:UpdateScheduleWorkOrder_REQ) ::)) as element() (:: schema-element(ns2:UpdateWorkOrder_REQ) ::) {
    <ns2:UpdateWorkOrder_REQ>
        {$request/*[1]}
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns2:appointmentTimes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:appointmentTimes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:calculationDate)
                    then <ns2:calculationDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:calculationDate)}</ns2:calculationDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)
                    then <ns2:cancelationReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)}</ns2:cancelationReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns2:engineerNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:engineerNotes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns2:estimatedFinishDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:estimatedFinishDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)
                    then <ns2:forcedAppointment>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)}</ns2:forcedAppointment>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)
                    then <ns2:futureContactDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)}</ns2:futureContactDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns2:networkResourceID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:networkResourceID>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns2:progressTask>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:progressTask>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns2:scheduleUserName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:scheduleUserName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)
                    then <ns2:siglaActividad>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)}</ns2:siglaActividad>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:accessTech)
                    then <ns2:accessTech>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:accessTech)}</ns2:accessTech>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
                    then <ns2:accessTechQTY>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:accessTechQTY>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)
                    then <ns2:affectedNetwork>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)}</ns2:affectedNetwork>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns2:annulmentTime>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns2:annulmentTime>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
                    then <ns2:assignmentTime>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns2:assignmentTime>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:BOValidation)
                    then <ns2:BOValidation>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:BOValidation)}</ns2:BOValidation>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:BOAuthotization)
                    then <ns2:BOAuthotization>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:BOAuthotization)}</ns2:BOAuthotization>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)
                    then <ns2:BOUserName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:BOUserName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
                    then <ns2:broadbandIndicator>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns2:broadbandIndicator>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:cabeceraFO)
                    then <ns2:cabeceraFO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:cabeceraFO)}</ns2:cabeceraFO>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:cableName)
                    then <ns2:cableName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:cableName)}</ns2:cableName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:changeRange)
                    then <ns2:changeRange>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:changeRange)}</ns2:changeRange>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:checkList)
                    then <ns2:checkList>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:checkList)}</ns2:checkList>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:closeReason)
                    then <ns2:closeReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closeReason)}</ns2:closeReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
                    then <ns2:cellComercial>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns2:cellComercial>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
                    then <ns2:emailComercial>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns2:emailComercial>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
                    then <ns2:nameComercial>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns2:nameComercial>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
                    then <ns2:phoneComercial>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns2:phoneComercial>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:contactName)
                    then <ns2:contactName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns2:contactName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns2:contractor>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:contractor>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:copperParameter)
                    then <ns2:copperParameter>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:copperParameter)}</ns2:copperParameter>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:createdDate)
                    then <ns2:createdDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns2:createdDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:curveType)
                    then <ns2:curveType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:curveType)}</ns2:curveType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)
                    then <ns2:customerHostCell>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns2:customerHostCell>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:customerHostName)
                    then <ns2:customerHostName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</ns2:customerHostName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:customerValidator)
                    then <ns2:customerValidator>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:customerValidator)}</ns2:customerValidator>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:decoParameter1)
                    then <ns2:decoParameter1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:decoParameter1)}</ns2:decoParameter1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:decoParameter2)
                    then <ns2:decoParameter2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:decoParameter2)}</ns2:decoParameter2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:decoParameter3)
                    then <ns2:decoParameter3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:decoParameter3)}</ns2:decoParameter3>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
                    then <ns2:deliveryImplementacionDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns2:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:depReg)
                    then <ns2:depReg>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns2:depReg>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:derivationReason)
                    then <ns2:derivationReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:derivationReason)}</ns2:derivationReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:derivationType)
                    then <ns2:derivationType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:derivationType)}</ns2:derivationType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:dispatchNotes)
                    then <ns2:dispatchNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:dispatchNotes)}</ns2:dispatchNotes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)
                    then <ns2:efficiencyCumplied>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)}</ns2:efficiencyCumplied>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)
                    then <ns2:efficiencyCumpliedReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)}</ns2:efficiencyCumpliedReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
                    then <ns2:emissionDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns2:emissionDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:entryDate)
                    then <ns2:entryDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns2:entryDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:EPC)
                    then <ns2:EPC>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:EPC)}</ns2:EPC>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:executedAction)
                    then <ns2:executedAction>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:executedAction)}</ns2:executedAction>
                    else ()
                }
                <ns2:fiberParameter>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:fiberParameter)}</ns2:fiberParameter>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:finishCoordinates)
                    then <ns2:finishCoordinates>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:finishCoordinates)}</ns2:finishCoordinates>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:finishTime)
                    then <ns2:finishTime>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns2:finishTime>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:fume)
                    then <ns2:fume>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:fume)}</ns2:fume>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:installationCost)
                    then <ns2:installationCost>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns2:installationCost>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:partyIndividualNumber)
                    then <ns2:partyIndividualNumber>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:partyIndividualNumber)}</ns2:partyIndividualNumber>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
                    then <ns2:legacyBOUserName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns2:legacyBOUserName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
                    then <ns2:externalStatus>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns2:externalStatus>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalType)
                    then <ns2:externalType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns2:externalType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:n2Resp)
                    then <ns2:n2Resp>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:n2Resp)}</ns2:n2Resp>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:networkOrderID)
                    then <ns2:networkOrderID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:networkOrderID)}</ns2:networkOrderID>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:networkRoute)
                    then <ns2:networkRoute>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:networkRoute)}</ns2:networkRoute>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
                    then <ns2:networkSpeed>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns2:networkSpeed>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns2:networkType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:networkType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:notDoneType)
                    then <ns2:notDoneType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:notDoneType)}</ns2:notDoneType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns2:OFTcloseReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns2:OFTcloseType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)
                    then <ns2:OFTsummary>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns2:OFTsummary>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion1)
                    then <ns2:prestacion1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</ns2:prestacion1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion2)
                    then <ns2:prestacion2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</ns2:prestacion2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion3)
                    then <ns2:prestacion3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</ns2:prestacion3>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion4)
                    then <ns2:prestacion4>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</ns2:prestacion4>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion5)
                    then <ns2:prestacion5>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</ns2:prestacion5>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
                    then <ns2:severityProblem>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns2:severityProblem>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)
                    then <ns2:prestacionQTY1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</ns2:prestacionQTY1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)
                    then <ns2:prestacionQTY2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</ns2:prestacionQTY2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)
                    then <ns2:prestacionQTY3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</ns2:prestacionQTY3>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)
                    then <ns2:prestacionQTY4>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</ns2:prestacionQTY4>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)
                    then <ns2:prestacionQTY5>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</ns2:prestacionQTY5>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:radioParameter)
                    then <ns2:radioParameter>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:radioParameter)}</ns2:radioParameter>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns2:requestType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:requestType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
                    then <ns2:rescheduleRequester>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns2:rescheduleRequester>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:restartTime)
                    then <ns2:restartTime>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:restartTime)}</ns2:restartTime>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
                    then <ns2:salesChannelID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns2:salesChannelID>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
                    then <ns2:salesChannelType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns2:salesChannelType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedID)
                    then <ns2:sapReplacedID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedID)}</ns2:sapReplacedID>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedName)
                    then <ns2:sapReplacedName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedName)}</ns2:sapReplacedName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedQTY)
                    then <ns2:sapReplacedQTY>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedQTY)}</ns2:sapReplacedQTY>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedResourceName)
                    then <ns2:sapReplacedResourceName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sapReplacedResourceName)}</ns2:sapReplacedResourceName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:segment)
                    then <ns2:segment>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:segment>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:serviceName)
                    then <ns2:serviceName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns2:serviceName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:serviceData)
                    then <ns2:serviceData>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:serviceData)}</ns2:serviceData>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:serviceID)
                    then <ns2:serviceID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns2:serviceID>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
                    then <ns2:serviceTemplate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns2:serviceTemplate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:serviceType)
                    then <ns2:serviceType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns2:serviceType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
                    then <ns2:serviceTypeSIAC>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns2:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:serviceValidate)
                    then <ns2:serviceValidate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:serviceValidate)}</ns2:serviceValidate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
                    then <ns2:signatureDate>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns2:signatureDate>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)
                    then <ns2:startCoordinates>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)}</ns2:startCoordinates>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:startedTravel)
                    then <ns2:startedTravel>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:startedTravel)}</ns2:startedTravel>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)
                    then <ns2:ticketCheckList>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ns2:ticketCheckList>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
                    then <ns2:troubleAction>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns2:troubleAction>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
                    then <ns2:troubleDescription>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns2:troubleDescription>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
                    then <ns2:troubleDiagnosis>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns2:troubleDiagnosis>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)
                    then <ns2:troubleTicketReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:troubleTicketReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketResponsable)
                    then <ns2:troubleTicketResponsable>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketResponsable)}</ns2:troubleTicketResponsable>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:warehouseName)
                    then <ns2:warehouseName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:warehouseName)}</ns2:warehouseName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalZone)
                    then <ns2:externalZone>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns2:externalZone>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:zoneKM)
                    then <ns2:zoneKM>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</ns2:zoneKM>
                    else ()
                }
                <ns2:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
                    then <ns2:slaWindowEnd>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
                    then <ns2:slaWindowStart>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns2:slaWindowStart>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:Address)
                    then <ns2:Address>
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:commune)
                            then <ns2:commune>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:commune)}</ns2:commune>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:city)
                            then <ns2:city>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:city)}</ns2:city>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:region)
                            then <ns2:region>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:region)}</ns2:region>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:streetName)
                            then <ns2:streetName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:streetName)}</ns2:streetName>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:addressReference)
                            then <ns2:addressReference>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:addressReference)}</ns2:addressReference>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:buildingType)
                            then <ns2:buildingType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:buildingType)}</ns2:buildingType>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:complentaryAddress)
                            then <ns2:complentaryAddress>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:complentaryAddress)}</ns2:complentaryAddress>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:department)
                            then <ns2:department>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:department)}</ns2:department>
                            else ()
                        }</ns2:Address>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount)
                    then <ns2:customerAccount>
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:alias)
                            then <ns2:alias>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:alias)}</ns2:alias>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:customerGroup)
                            then <ns2:customerGroup>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:customerGroup)}</ns2:customerGroup>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:portabilityIndicator)
                            then <ns2:portabilityIndicator>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:portabilityIndicator)}</ns2:portabilityIndicator>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact)
                            then <ns2:contact>
                                {
                                    if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:alternativePhoneNumber)
                                    then <ns2:alternativePhoneNumber>
                                        <ns2:number>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:alternativePhoneNumber/ns1:number)}</ns2:number></ns2:alternativePhoneNumber>
                                    else ()
                                }
                                {
                                    if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:cellPhone)
                                    then <ns2:cellPhone>
                                        <ns2:number>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:cellPhone/ns1:number)}</ns2:number></ns2:cellPhone>
                                    else ()
                                }
                                {
                                    if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:email)
                                    then <ns2:email>
                                        <ns2:eMailAddress>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:email/ns1:eMailAddress)}</ns2:eMailAddress></ns2:email>
                                    else ()
                                }
                                {
                                    if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:IndividualIdentification)
                                    then <ns2:IndividualIdentification>
                                        <ns2:number>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:IndividualIdentification/ns1:number)}</ns2:number></ns2:IndividualIdentification>
                                    else ()
                                }
                                {
                                    if ($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:IndividualName)
                                    then <ns2:IndividualName>
                                        <ns2:formatedName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:Contact/ns1:IndividualName/ns1:formatedName)}</ns2:formatedName></ns2:IndividualName>
                                    else ()
                                }</ns2:contact>
                            else ()
                        }</ns2:customerAccount>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource)
                    then <ns2:PartyResource>
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)
                            then <ns2:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns2:ID>
                            else ()
                        }</ns2:PartyResource>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:Product)
                    then <ns2:Product>
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                            then <ns2:ANISQuantity>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:ANISQuantity>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                            then <ns2:ANISQuantityRequired>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns2:ANISQuantityRequired>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)
                            then <ns2:category>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)}</ns2:category>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                            then <ns2:DECOQuantity>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns2:DECOQuantity>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                            then <ns2:DECOQuantityRequired>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:DECOQuantityRequired>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)
                            then <ns2:equipment>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)}</ns2:equipment>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                            then <ns2:layerMPLS>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns2:layerMPLS>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                            then <ns2:ngnModel>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns2:ngnModel>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                            then <ns2:PHONEQuantity>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns2:PHONEQuantity>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                            then <ns2:externalProductCode>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns2:externalProductCode>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                            then <ns2:externalProductDescription>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns2:externalProductDescription>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                            then <ns2:STBQuantity>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:STBQuantity>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)
                            then <ns2:voiceIndicator>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)}</ns2:voiceIndicator>
                            else ()
                        }
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                            then <ns2:WIFIExtensorQuantity>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns2:WIFIExtensorQuantity>
                            else ()
                        }</ns2:Product>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:AbsoluteLocalLocation)
                    then <ns2:AbsoluteLocalLocation>
                        <ns2:X>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:AbsoluteLocalLocation/ns1:X)}</ns2:X>
                        <ns2:Y>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:AbsoluteLocalLocation/ns1:Y)}</ns2:Y></ns2:AbsoluteLocalLocation>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule)
                    then <ns2:WorkSchedule>
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
                            then <ns2:applicableDuring>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:applicableDuring>
                            else ()
                        }</ns2:WorkSchedule>
                    else ()
                }</ns2:WorkOrder>
        </ns2:Body>
    </ns2:UpdateWorkOrder_REQ>
};

local:func($request)
