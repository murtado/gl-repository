xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)


declare namespace ns2="http://www.entel.cl/EBM/CancelWorkOrder/Cancel/v1";
(:: import schema at "../../../../../ESC/Primary/Cancel_WorkOrder_v2_EBM.xsd" ::)

declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)


declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $ResponseHeader as element() (:: schema-element(ns1:ResponseHeader) ::) external;

declare function local:cancel_Cancel_WorkOrder_FRSP($ResponseHeader as element() (:: schema-element(ns1:ResponseHeader) ::)) as element() (:: schema-element(ns2:CancelWorkOrder_FRSP) ::) {
    <ns2:CancelWorkOrder_FRSP>
        {$ResponseHeader}
        <ns2:Body></ns2:Body>
    </ns2:CancelWorkOrder_FRSP>
};

local:cancel_Cancel_WorkOrder_FRSP($ResponseHeader)