xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/getCallbackDetail/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getCallbackDetail_ConversationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getCallbackDetail";
(:: import schema at "../JCA/getCallbackDetail/getCallbackDetail_sp.xsd" ::)

declare namespace ns3="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace cal = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare variable $GetCallbackDetailREQ as element()? (:: schema-element(ns1:GetCallbackDetailREQ) ::) external;
declare variable $CallbackDetail as element()? (:: schema-element(ns2:OutputParameters) ::) external;
declare variable $Result as element() (:: schema-element(ns3:Result) ::) external;

declare function local:GetCallbackDetailRSP($GetCallbackDetailREQ as element()? (:: schema-element(ns1:GetCallbackDetailREQ) ::), 
                                            $CallbackDetail as element()? (:: schema-element(ns2:OutputParameters) ::),
                                            $Result as element() (:: schema-element(ns3:Result) ::)
                                            ) 
                                            as element() (:: schema-element(ns1:GetCallbackDetailRSP) ::) {
    <ns1:GetCallbackDetailRSP>
        {$Result}
        {  
          if(boolean($GetCallbackDetailREQ)) then
            <cal:CbDetails>
                {
                    for $P_CB_DETAILS_ITEM in $CallbackDetail/ns2:P_CB_DETAILS/ns2:P_CB_DETAILS_ITEM
                    return 
                    <cal:CbDetail 
                        cbID="{fn:data($P_CB_DETAILS_ITEM/ns2:CB_ID)}" 
                        cnvID="{fn:data($GetCallbackDetailREQ/cal:CbGroupMembers/cal:CbGroupMember/@cnvID)}"
                        cnvStatus="{fn:data($GetCallbackDetailREQ/cal:CbGroupMembers/cal:CbGroupMember[@cbID=fn:data($P_CB_DETAILS_ITEM/ns2:CB_ID)]/@cnvStatus)}"
                        srvName="{fn:data($GetCallbackDetailREQ/cal:CbGroupMembers/cal:CbGroupMember[@cbID=fn:data($P_CB_DETAILS_ITEM/ns2:CB_ID)]/@srvName)}"
                        srvOper="{fn:data($GetCallbackDetailREQ/cal:CbGroupMembers/cal:CbGroupMember[@cbID=fn:data($P_CB_DETAILS_ITEM/ns2:CB_ID)]/@srvOper)}">
                        {
                            if ($GetCallbackDetailREQ/cal:CbGroupMembers/cal:CbGroupMember[@cbID=fn:data($P_CB_DETAILS_ITEM/ns2:CB_ID)]/@provID)
                            then attribute provID {fn:data($GetCallbackDetailREQ/cal:CbGroupMembers/cal:CbGroupMember[@cbID=fn:data($P_CB_DETAILS_ITEM/ns2:CB_ID)]/@provID)}
                            else ()
                        }
                    <cal:CallbackMessage>{fn-bea:inlinedXML(fn:data($P_CB_DETAILS_ITEM/ns2:CB_MSG))}</cal:CallbackMessage></cal:CbDetail>
                }
            </cal:CbDetails>
          else
          ()
        }
    </ns1:GetCallbackDetailRSP>
};

local:GetCallbackDetailRSP($GetCallbackDetailREQ, $CallbackDetail, $Result)
