xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/GetWorkOrder/Get/v1";
(:: import schema at "../../../../../ESC/Primary/GetWorkOrder_v1_EBM.xsd" ::)



declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $ResponseHeader as element() (:: schema-element(ns3:ResponseHeader) ::) external;
declare function local:func($ResponseHeader as element() (:: schema-element(ns3:ResponseHeader) ::)) as element() (:: schema-element(ns2:GetWorkOrder_FRSP) ::) {
    <ns2:GetWorkOrder_FRSP>
        {$ResponseHeader}
        <ns2:Body>
        </ns2:Body>
    </ns2:GetWorkOrder_FRSP>
};

local:func($ResponseHeader)