xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/CSM/RA/CHL-OFC-OFC/GetActivity/v2";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v2/ResourceAdapters/CHL-OFC-OFC_GetActivity_RA_V2/CSC/CHL-OFC-OFC_GetActivity_v2_CSM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/GetWorkOrder/Get/v1";
(:: import schema at "../../../../../ESC/Primary/GetWorkOrder_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $getWorkOrderOhRq as element() (:: schema-element(ns1:GetWorkOrder_REQ) ::) external;

declare function local:func($getWorkOrderOhRq as element() (:: schema-element(ns1:GetWorkOrder_REQ) ::)) as element() (:: schema-element(ns2:CHL-OFC-OFC_GetActivity_REQ) ::) {
    <ns2:CHL-OFC-OFC_GetActivity_REQ>
        {$getWorkOrderOhRq/*[1]}
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($getWorkOrderOhRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CHL-OFC-OFC_GetActivity_REQ>
};

local:func($getWorkOrderOhRq)