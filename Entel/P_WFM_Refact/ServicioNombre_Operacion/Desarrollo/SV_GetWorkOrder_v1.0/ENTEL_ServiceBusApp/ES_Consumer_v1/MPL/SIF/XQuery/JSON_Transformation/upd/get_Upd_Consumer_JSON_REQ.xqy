xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/Consumer/upd/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/upd_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/Consumer/upd/v1";
(:: import schema at "../../../../../ESC/Primary/upd_Consumer_v1_EBM.xsd" ::)


import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;


declare namespace ns6 = "http://www.entel.cl/EBO/Consumer/v1";

declare variable $UpdConsumerREQ as element() (:: schema-element(ns1:Upd_Consumer_REQ) ::) external;

declare function local:func($UpdConsumerREQ as element() (:: schema-element(ns1:Upd_Consumer_REQ) ::)) as element() (:: schema-element(ns2:Upd_Consumer_REQ) ::) {
    <ns2:Upd_Consumer_REQ>
        {nssif_header:get_RequestHeader($UpdConsumerREQ/*:RequestHeader)}
        <ns2:Body>
            <ns6:ConsumerID>{fn:data($UpdConsumerREQ/ns1:Body/ns1:ConsumerID)}</ns6:ConsumerID>
            <ns6:Consumer>
                <ns6:ConsumerName>{fn:data($UpdConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerName)}</ns6:ConsumerName>
                <ns6:ConsumerSurename>{fn:data($UpdConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerSurename)}</ns6:ConsumerSurename>
                <ns6:ConsumerAge>{fn:data($UpdConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerAge)}</ns6:ConsumerAge>
                <ns6:ConsumerStatus>{fn:data($UpdConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerStatus)}</ns6:ConsumerStatus>
            </ns6:Consumer>
        </ns2:Body>
    </ns2:Upd_Consumer_REQ>
};

local:func($UpdConsumerREQ)
