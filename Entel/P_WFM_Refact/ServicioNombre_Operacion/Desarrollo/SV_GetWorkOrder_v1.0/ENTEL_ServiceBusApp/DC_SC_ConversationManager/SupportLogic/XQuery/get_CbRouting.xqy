xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $CbDetails as element() external;

declare function local:get_CbRouting($CbDetails as element()) as element() {
    
    if (data($CbDetails/@sVer) = 'v1') then 
    
      <ctx:route xmlns:ctx='http://www.bea.com/wli/sb/context'>
        <ctx:service isProxy="false">{concat('DC_SC_ConversationManager/SupportLogic/Business/',tokenize(data($CbDetails/@sAddr),'://')[1], '_PublishResult')}</ctx:service>
      </ctx:route>
    
    else(
    
    if (data($CbDetails/@sVer) = 'v2') then 
    
      <ctx:route xmlns:ctx='http://www.bea.com/wli/sb/context'>
        <ctx:service isProxy="false">DC_SC_ConversationManager/SupportLogic/Business/ConversationManager_AP_RSP</ctx:service>
      </ctx:route>
    
    else()
    
    )
    
};

local:get_CbRouting($CbDetails)