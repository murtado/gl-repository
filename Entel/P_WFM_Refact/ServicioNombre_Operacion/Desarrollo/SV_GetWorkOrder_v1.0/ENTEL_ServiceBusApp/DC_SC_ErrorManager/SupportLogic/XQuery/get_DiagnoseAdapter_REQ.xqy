xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ErrorManager/diagnose/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/diagnose_ErrorManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/diagnose";
(:: import schema at "../JCA/diagnoseAdapter/diagnose_sp.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/SC/ErrorManager/ErrorDiagnostics/Aux";

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare variable $DiagnoseREQ as element() (:: schema-element(ns1:DiagnoseREQ) ::) external;

declare function local:get_DiagnoseAdapter_REQ($DiagnoseREQ as element() (:: schema-element(ns1:DiagnoseREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_CMP_NAME>{fn:data($DiagnoseREQ/ns3:IncidentMeta/@cmpName)}</ns2:P_CMP_NAME>
        <ns2:P_CMP_STEP>{fn:data($DiagnoseREQ/ns3:IncidentMeta/@cmpStep)}</ns2:P_CMP_STEP>
        <ns2:P_ERR_CODE>{fn:data($DiagnoseREQ/ns3:IncidentMeta/ns4:Result/ns5:CanonicalError/@code)}</ns2:P_ERR_CODE>
        <ns2:P_ERR_TYPE>{fn:data($DiagnoseREQ/ns3:IncidentMeta/ns4:Result/ns5:CanonicalError/@type)}</ns2:P_ERR_TYPE>
    </ns2:InputParameters>
};

local:get_DiagnoseAdapter_REQ($DiagnoseREQ)
