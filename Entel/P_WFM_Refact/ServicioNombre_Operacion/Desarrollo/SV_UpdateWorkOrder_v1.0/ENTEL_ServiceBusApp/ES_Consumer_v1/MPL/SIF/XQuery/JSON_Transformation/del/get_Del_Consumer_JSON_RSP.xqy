xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/Consumer/del/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/del_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/del/v1";
(:: import schema at "../../../../../ESC/Primary/del_Consumer_v1_EBM.xsd" ::)

import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;


declare variable $delConsumerRsp as element() (:: schema-element(ns1:Del_Consumer_RSP) ::) external;

declare function local:func($delConsumerRsp as element() (:: schema-element(ns1:Del_Consumer_RSP) ::)) as element() (:: schema-element(ns2:Del_Consumer_RSP) ::) {
    <ns2:Del_Consumer_RSP>
       {nssif_header:get_ResponseHeader($delConsumerRsp/*:ResponseHeader)}
        <ns2:Body></ns2:Body>
    </ns2:Del_Consumer_RSP>
};

local:func($delConsumerRsp)
