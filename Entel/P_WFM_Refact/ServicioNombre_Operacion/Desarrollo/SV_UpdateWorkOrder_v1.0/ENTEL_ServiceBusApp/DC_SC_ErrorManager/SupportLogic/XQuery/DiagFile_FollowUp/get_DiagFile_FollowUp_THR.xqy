xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ErrorManager/ErrorDiagnostics/Aux";
(:: import schema at "../../../SupportAPI/XSD/ErrorDiagnostics.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/diagnose";
(:: import schema at "../../JCA/diagnoseAdapter/diagnose_sp.xsd" ::)

declare variable $DiagFile as element()? (:: schema-element(ns1:DiagFile) ::) external;
declare variable $AdapterRSP as element() (:: schema-element(ns2:OutputParameters) ::) external;

declare function local:get_Diagnose_FollowUp_Propagate($DiagFile as element()? (:: schema-element(ns1:DiagFile) ::), 
                                                   $AdapterRSP as element() (:: schema-element(ns2:OutputParameters) ::)) as element() (:: schema-element(ns1:DiagFile) ::) {

let $Aux:=if(boolean($DiagFile)) then data($DiagFile) else ()

  return

      <ns1:DiagFile 
            action="THR"
            >
            {
                  
                  if(boolean($DiagFile)) then
                    attribute pvAction {$DiagFile/@action}
                  else
                    ()
            }
            {
                  for $ActionCFG in $AdapterRSP/*[2]/*[1]/*
                    return  
                      if(data($ActionCFG)!='') then
                        attribute{data($ActionCFG/@name)} {data($ActionCFG)}
                      else
                        ()
            }
      </ns1:DiagFile>
};

local:get_Diagnose_FollowUp_Propagate($DiagFile, $AdapterRSP)