xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CorrelationManager/getCorrelation/v2";
(:: import schema at "../../SupportAPI/XSD/CSM/getCorrelation_CorrelationManager_v2_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getCorrelationMemberDBAdapter_v2";
(:: import schema at "../JCA/getCorrelationMemberAdapter_v2/getCorrelationMemberDBAdapter_v2_sp.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $GetCorrelationREQ as element() (:: schema-element(ns1:GetCorrelationREQ) ::) external;

declare function local:func($GetCorrelationREQ as element() (:: schema-element(ns1:GetCorrelationREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_GRP_NAME>{fn:data($GetCorrelationREQ/cor:GroupName)}</ns2:P_GRP_NAME>
        <ns2:P_GRP_TAG>{fn:data($GetCorrelationREQ/cor:GroupTag)}</ns2:P_GRP_TAG>
        {
            if ($GetCorrelationREQ/cor:GroupStatus)
            then <ns2:P_GRP_STATUS>{fn:data($GetCorrelationREQ/cor:GroupStatus)}</ns2:P_GRP_STATUS>
            else ()
        }
        <ns2:P_GRP_MEM>
            <ns2:MEM_NAME_>{fn:data($GetCorrelationREQ/cor:MemberName)}</ns2:MEM_NAME_>
            <ns2:MEM_LABEL_></ns2:MEM_LABEL_>
            <ns2:MEM_TYPE_>{fn:data($GetCorrelationREQ/cor:MemberType)}</ns2:MEM_TYPE_>
            <ns2:MEM_CORR_ID_>{fn:data($GetCorrelationREQ/cor:MemberCorrID)}</ns2:MEM_CORR_ID_>
            <ns2:MEM_ADDR_></ns2:MEM_ADDR_>
            <ns2:MEM_FLAGS_/>
        </ns2:P_GRP_MEM>
    </ns2:InputParameters>
};

local:func($GetCorrelationREQ)