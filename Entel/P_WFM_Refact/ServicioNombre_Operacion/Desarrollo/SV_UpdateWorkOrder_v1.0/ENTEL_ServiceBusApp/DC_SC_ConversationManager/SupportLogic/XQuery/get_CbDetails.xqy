xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $CorrelationGroup as element() external;
declare variable $SubCorrID as xs:string external;

declare function local:get_CbDetails($CorrelationGroup as element(), $SubCorrID as xs:string) as element() {

    let $GroupMembers:=$CorrelationGroup/*:Group/*:Members
    let $Subscriber:=$GroupMembers/*[@ns2:memberType='GroupOwner']
    let $ProviderInstance:=$GroupMembers/*[@ns2:memberCorrID=$SubCorrID]
    
    return
  
    <CbDetails> 
    {
       (: Made by Subscribe v1  :)
         if ($Subscriber/@ns2:memberName='SUB_v1') then attribute sVer {'v1'} else (),
         
         if(
            exists($Subscriber/@ns2:memberAddress) and data($Subscriber/@ns2:memberAddress) != '') then
              attribute sAddr {data($Subscriber/@ns2:memberAddress)}
         else (),
       
       (: Made by Subscribe v2  :)
         if ($Subscriber/@ns2:memberName='SUB_v2') then attribute sVer {'v2'} else (),
         
         if ($Subscriber/@ns2:memberName='SUB_v2') then attribute jmsCorrID {$ProviderInstance/*[1]/*[@name='jmsCorrID']/@value} else (),
         if ($Subscriber/@ns2:memberName='SUB_v2') then attribute cbMode {$ProviderInstance/*[1]/*[@name='cbMode']/@value} else (),
         
         if(
            exists($ProviderInstance/*[1]/*[@name='jmsType']/@value) and data($ProviderInstance/*[1]/*[@name='jmsType']/@value != '')) then
              attribute jmsType {data($ProviderInstance/*[1]/*[@name='jmsType']/@value)}
         else ()
    }
    </CbDetails>
    
};

local:get_CbDetails($CorrelationGroup, $SubCorrID)
