xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/CSM/RA/CHL-OFC-OFC/MoveActivity/v2";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v2/ResourceAdapters/CHL-OFC-OFC_MoveActivity_RA_v2/CSC/CHL-OFC-OFC_MoveActivity_v2_CSM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/MoveWorkOrder/Update/v1";
(:: import schema at "../../../../../ESC/Primary/MoveWorkOrder_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $moveWorkOrderOhRq as element() (:: schema-element(ns1:MoveWorkOrder_REQ) ::) external;

declare function local:func($moveWorkOrderOhRq as element() (:: schema-element(ns1:MoveWorkOrder_REQ) ::)) as element() (:: schema-element(ns2:CHL-OFC-OFC_MoveActivity_REQ) ::) {
    <ns2:CHL-OFC-OFC_MoveActivity_REQ>
        {$moveWorkOrderOhRq/*[1]}
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($moveWorkOrderOhRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                <ns2:SetDate>
                    <ns2:appointmentDate>{fn:data($moveWorkOrderOhRq/ns1:Body/ns1:WorkOrder/ns1:SetDate/ns1:appointmentDate)}</ns2:appointmentDate>
                </ns2:SetDate>
                <ns2:SetResource>
                    <ns2:ID>{fn:data($moveWorkOrderOhRq/ns1:Body/ns1:WorkOrder/ns1:SetResource/ns1:ID)}</ns2:ID>
                </ns2:SetResource>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CHL-OFC-OFC_MoveActivity_REQ>
};

local:func($moveWorkOrderOhRq)