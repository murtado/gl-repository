xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-OFC-OFC/MoveActivity/v2";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v2/ResourceAdapters/CHL-OFC-OFC_MoveActivity_RA_v2/CSC/CHL-OFC-OFC_MoveActivity_v2_CSM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/MoveWorkOrder/Update/v1";
(:: import schema at "../../../../../ESC/Primary/MoveWorkOrder_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)


declare variable $response as element() (:: schema-element(ns1:CHL-OFC-OFC_MoveActivity_RSP) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::) external;



declare function local:func($RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::),
                             $response as element() (:: schema-element(ns1:CHL-OFC-OFC_MoveActivity_RSP) ::)) as element() (:: schema-element(ns2:MoveWorkOrder_RSP) ::) {
    <ns2:MoveWorkOrder_RSP>
        <ns3:ResponseHeader>
        {$RequestHeader/*}
        {$response/*[1]}
        </ns3:ResponseHeader>
        <ns2:Body></ns2:Body>
    </ns2:MoveWorkOrder_RSP>
};

local:func($RequestHeader, $response)