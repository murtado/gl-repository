xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/subscribe/v2";
(:: import schema at "../../SupportAPI/XSD/CSM/subscribe_ConversationManager_v2_CSM.xsd" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/registerGroup/v2";
(:: import schema at "../../../DC_SC_CorrelationManager/SupportAPI/XSD/CSM/registerGroup_CorrelationManager_v2_CSM.xsd" ::)

declare namespace ns3="http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";
declare namespace ns4="http://www.entel.cl/ESO/MessageHeader/v1";
declare namespace ns5="http://www.entel.cl/SC/ConversationManager/Aux/Conversation";

declare namespace cal = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare variable $SubscribeREQ as element() (:: schema-element(ns1:SubscribeREQ) ::) external;
declare variable $JMSMeta as element() external;

declare function local:get_RegisterGroupREQ_ConversationManager($SubscribeREQ as element() (:: schema-element(ns1:SubscribeREQ) ::), $JMSMeta as element()) 
                                              as element() (:: schema-element(ns2:RegisterGroupREQ) ::) {

let $Prov:=$SubscribeREQ/*[3]
let $Sub:=$SubscribeREQ/*[2]
let $CbOptions:=$Sub/*[1]

return
  <ns2:RegisterGroupREQ>
    {$SubscribeREQ/*[1]}
        <ns3:Group 
              ns3:groupName="{'cbSUB'}" 
              ns3:groupStatus="O">
            {
                if ($CbOptions/@cbAggregationGroup)
                then attribute ns3:groupTag {fn:data($CbOptions/@cbAggregationGroup)}
                else ()
            }
              <ns3:Members>
                  <ns3:Member>
                  {
                      attribute ns3:memberName{'SUB_v2'},
                      
                      if(exists($Prov/@subID)) then
                        attribute ns3:memberLabel{$Sub/@subID}
                      else
                        (),
                      
                      attribute ns3:memberType{'GroupOwner'},
                      attribute ns3:memberCorrID{data(fn:data($Sub/@conversationID))}
                  }
                  </ns3:Member>
                   <ns3:Member>
                  {
                      attribute ns3:memberName{'PRV'},
                      
                      if(exists($Prov/@provID)) then
                        attribute ns3:memberLabel{$Prov/@provID}
                      else
                        (),
                        
                      attribute ns3:memberType{'GroupProvider'},
                      attribute ns3:memberCorrID{data(fn:data($Prov/@conversationID))}
                  }
                  {
                  if(
                    (fn:data($JMSMeta/@jmsMessageID)!='')
                      or
                    (fn:data($JMSMeta/@jmsMessageType)!='')
                  
                  ) then
                      <ns3:Flags>
                          {
                          if(fn:data($JMSMeta/@jmsMessageType)!='') then
                            <ns3:Flag name="jmsType" value="{fn:data($JMSMeta/@jmsMessageType)}"/>
                          else ()
                          }
                          {
                          if(fn:data($JMSMeta/@jmsCorrelationID)!='') then
                            <ns3:Flag name="jmsCorrID" value="{fn:data($JMSMeta/@jmsCorrelationID)}"/>
                          else ()
                          }
                          <ns3:Flag name="cbMode" value="{fn:data($CbOptions/@cbMode)}"/>
                      </ns3:Flags>
                  else 
                    ()
                }
                  </ns3:Member>
              </ns3:Members>
              <ns3:GroupHeader>{fn-bea:serialize($SubscribeREQ/*[1])}</ns3:GroupHeader>
        </ns3:Group>
  </ns2:RegisterGroupREQ>
  
};

local:get_RegisterGroupREQ_ConversationManager($SubscribeREQ,$JMSMeta)
