xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/closeAggrGroup/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/closeAggrGroup_ConversationManager_v1_CSM.xsd" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/createGroupMember/v1";
(:: import schema at "../../../DC_SC_CorrelationManager/SupportAPI/XSD/CSM/createGroupMember_CorrelationManager_v1_CSM.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare namespace cal = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare namespace ns3="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::) external;
declare variable $CbGroupTag as xs:string external;
declare variable $CbMode as xs:string external;
declare variable $JMSMeta as element() external;

declare function local:get_CreateGroupMemberREQ_CloseAggrGroup(
  $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::),
  $CbGroupTag as xs:string, 
  $CbMode as xs:string,
  $JMSMeta as element()) as element() (:: schema-element(ns2:CreateGroupMemberREQ) ::) {

    <ns2:CreateGroupMemberREQ>
        {$RequestHeader}
        <cor:GroupTag>{$CbGroupTag}</cor:GroupTag>
        <cor:Member>
                {
                    attribute cor:memberName{'SUB_CLOSE'},
                    attribute cor:memberType{'GroupController'},
                    attribute cor:memberCorrID{data($RequestHeader/*[2]/@conversationID)}
                }
                {
                  if(
                    (fn:data($JMSMeta/@jmsMessageID)!='')
                      or
                    (fn:data($JMSMeta/@jmsMessageType)!='')
                  
                  ) then
                      <cor:Flags>
                          {
                          if(fn:data($JMSMeta/@jmsMessageType)!='') then
                            <cor:Flag name="jmsType" value="{fn:data($JMSMeta/@jmsMessageType)}"/>
                          else ()
                          }
                          {
                          if(fn:data($JMSMeta/@jmsCorrelationID)!='') then
                            <cor:Flag name="jmsCorrID" value="{fn:data($JMSMeta/@jmsCorrelationID)}"/>
                          else ()
                          }
                          <cor:Flag name="cbMode" value="{$CbMode}"/>
                      </cor:Flags>
                  else 
                    ()
                }
                </cor:Member>
    </ns2:CreateGroupMemberREQ>
};

local:get_CreateGroupMemberREQ_CloseAggrGroup($RequestHeader, $CbGroupTag, $CbMode, $JMSMeta)
