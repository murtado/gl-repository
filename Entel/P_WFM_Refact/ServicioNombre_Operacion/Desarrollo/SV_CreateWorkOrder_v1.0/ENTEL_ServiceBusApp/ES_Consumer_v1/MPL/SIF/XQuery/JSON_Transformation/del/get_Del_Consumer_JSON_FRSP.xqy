xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)


import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;

declare namespace ns2="http://www.entel.cl/EBM/Consumer/del/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/del_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/del/v1";
(:: import schema at "../../../../../ESC/Primary/del_Consumer_v1_EBM.xsd" ::)

declare variable $delConsumerFrsp as element() (:: schema-element(ns1:Del_Consumer_FRSP) ::) external;
declare variable $faultstring as xs:string external;
declare variable $faultcode as xs:string external;

declare function local:func($delConsumerFrsp as element() (:: schema-element(ns1:Del_Consumer_FRSP) ::), 
                            $faultstring as xs:string, 
                            $faultcode as xs:string) 
                            as element() (:: schema-element(ns2:Fault) ::) {
    <ns2:Fault>
        <ns2:faultcode>{fn:data($faultcode)}</ns2:faultcode>
        <ns2:faultstring>{fn:data($faultstring)}</ns2:faultstring>
        <ns2:detail>
            <ns2:Del_Consumer_FRSP>
                {nssif_header:get_ResponseHeader($delConsumerFrsp/*:ResponseHeader)}
                <ns2:Body></ns2:Body>
            </ns2:Del_Consumer_FRSP>
        </ns2:detail>
    </ns2:Fault>
};

local:func($delConsumerFrsp, $faultstring, $faultcode)
