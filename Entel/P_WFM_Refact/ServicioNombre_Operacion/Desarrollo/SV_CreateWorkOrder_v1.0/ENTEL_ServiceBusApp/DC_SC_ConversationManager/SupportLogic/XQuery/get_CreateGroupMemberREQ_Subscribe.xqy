xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/subscribe/v2";
(:: import schema at "../../SupportAPI/XSD/CSM/subscribe_ConversationManager_v2_CSM.xsd" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/createGroupMember/v1";
(:: import schema at "../../../DC_SC_CorrelationManager/SupportAPI/XSD/CSM/createGroupMember_CorrelationManager_v1_CSM.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";
declare namespace ns4 = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare variable $SubscribeREQ as element() (:: schema-element(ns1:SubscribeREQ) ::) external;
declare variable $JMSMeta as element() external;

declare function local:get_CreateGroupMemberREQ_CM($SubscribeREQ as element() (:: schema-element(ns1:SubscribeREQ) ::),$JMSMeta as element()) as element() (:: schema-element(ns2:CreateGroupMemberREQ) ::) {

let $Prov:=$SubscribeREQ/*[3]
let $Sub:=$SubscribeREQ/*[2]
let $CbOptions:=$Sub/*[1]

return
    <ns2:CreateGroupMemberREQ>
        {$SubscribeREQ/*[1]}
        <ns3:GroupTag>{fn:data($CbOptions/@cbAggregationGroup)}</ns3:GroupTag>
        <ns3:Member>
                {
                    attribute ns3:memberName{'PRV'},
                    
                    if(exists($Prov/@provID)) then
                      attribute ns3:memberLabel{$Prov/@provID}
                    else
                      (),
                    
                    attribute ns3:memberType{'GroupProvider'},
                    attribute ns3:memberCorrID{data(fn:data($Prov/@conversationID))}
                }
                {
                  if(
                    (fn:data($JMSMeta/@jmsCorrelationID)!='')
                      or
                    (fn:data($JMSMeta/@jmsMessageType)!='')
                  
                  ) then
                      <ns3:Flags>
                          {
                          if(fn:data($JMSMeta/@jmsMessageType)!='') then
                            <ns3:Flag name="jmsType" value="{fn:data($JMSMeta/@jmsMessageType)}"/>
                          else ()
                          }
                          {
                          if(fn:data($JMSMeta/@jmsCorrelationID)!='') then
                            <ns3:Flag name="jmsCorrID" value="{fn:data($JMSMeta/@jmsCorrelationID)}"/>
                          else ()
                          }
                          <ns3:Flag name="cbMode" value="{fn:data($CbOptions/@cbMode)}"/>
                      </ns3:Flags>
                  else 
                    ()
                }
                </ns3:Member>
    </ns2:CreateGroupMemberREQ>
};

local:get_CreateGroupMemberREQ_CM($SubscribeREQ,$JMSMeta)
