xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/EnterpriseService/v1";

declare function local:get_ESArchitecture() as element(){
    
    (: 
    This function holds the information require to describe this Service's Architecture (ESAS), at the Solution level.
    
    It should be kept updated over each modification made to the service, on which its Solution Architecture is modified.
    :)
    
    <ns1:EnterpriseService name="WorkOrder" code="WFM_0001" majorVersion="2" minorVersion="0">
	<ns1:ESC>
		<ns1:EBSC>
			<ns1:PrimaryContract name="CreateWorkOrder_v1_ESC"/>
		</ns1:EBSC>
	</ns1:ESC>
	<ns1:MPL>
		<ns1:PIF name="CreateWorkOrder_PIF"/>
	</ns1:MPL>
	<ns1:CSL>
		<ns1:Country code="CHL">
			<ns1:OH opName="Create" opCode="OWFM_99907" name="CHL_Create_CreateWorkOrder"/>
		</ns1:Country>
	</ns1:CSL>
  </ns1:EnterpriseService>
};

local:get_ESArchitecture()