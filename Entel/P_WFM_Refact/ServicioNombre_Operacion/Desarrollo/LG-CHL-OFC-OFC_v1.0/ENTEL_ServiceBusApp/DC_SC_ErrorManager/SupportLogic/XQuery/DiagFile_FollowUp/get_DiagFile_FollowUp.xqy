xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)


declare variable $AdapterRSP as element() external;

declare function local:get_Diagnose_FollowUp($AdapterRSP as element()) as xs:string {

  let $Action := 
    if(not(empty($AdapterRSP/*:P_ERR_ACTION)) and data($AdapterRSP/*:P_ERR_ACTION) != '') 
      then data($AdapterRSP/*:P_ERR_ACTION) 
    else 
      'THR'
  
  return
    concat(
    'DC_SC_ErrorManager/SupportLogic/XQuery/DiagFile_FollowUp/get_DiagFile_FollowUp_',
    $Action
    )
};

local:get_Diagnose_FollowUp($AdapterRSP)
