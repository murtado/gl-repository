xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESC/OFC_REQ";
(:: import schema at "../../../../../CommonResources/LegacyResources/CHL-OFC-OFC/HTTPJSON/XSD/OFC_REQ_Schema.xsd" ::)

declare variable $Id as xs:string external;
declare variable $Request as xs:string external;

declare function local:func($Id as xs:string, 
                            $Request as xs:string) 
                            as element() (:: schema-element(ns1:OFC_REQ) ::) {
    <ns1:OFC_REQ>
    { 
      if($Id!='')then
        <Id>{fn:data($Id)}</Id>
      else ()
    }
    {
      if($Request!='')then
        <Request>{fn:data($Request)}</Request>
      else()
    }
    </ns1:OFC_REQ>
};

local:func($Id, $Request)