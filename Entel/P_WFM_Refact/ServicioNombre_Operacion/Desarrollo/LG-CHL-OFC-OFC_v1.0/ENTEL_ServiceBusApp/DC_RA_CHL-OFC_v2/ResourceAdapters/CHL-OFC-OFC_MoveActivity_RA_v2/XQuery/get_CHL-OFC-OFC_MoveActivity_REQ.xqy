xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OFC-OFC/HTTPJSON/XSD/CHL-OFC-OFC_MoveActivity_JSON_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-OFC-OFC/MoveActivity/v2";
(:: import schema at "../CSC/CHL-OFC-OFC_MoveActivity_v2_CSM.xsd" ::)

declare variable $moveActivityReq as element() (:: schema-element(ns1:CHL-OFC-OFC_MoveActivity_REQ) ::) external;

declare function local:func($moveActivityReq as element() (:: schema-element(ns1:CHL-OFC-OFC_MoveActivity_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:setDate>
            <ns2:date>{fn:data($moveActivityReq/ns1:Body/ns1:WorkOrder/ns1:SetDate/ns1:appointmentDate)}</ns2:date>
        </ns2:setDate>
        <ns2:setResource>
            <ns2:resourceId>{fn:data($moveActivityReq/ns1:Body/ns1:WorkOrder/ns1:SetResource/ns1:ID)}</ns2:resourceId>
        </ns2:setResource>
    </ns2:Root-Element>
};

local:func($moveActivityReq)