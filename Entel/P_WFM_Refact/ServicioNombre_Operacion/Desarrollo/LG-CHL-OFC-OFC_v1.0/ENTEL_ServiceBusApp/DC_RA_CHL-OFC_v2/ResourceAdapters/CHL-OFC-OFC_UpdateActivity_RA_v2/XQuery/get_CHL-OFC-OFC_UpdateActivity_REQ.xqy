xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OFC-OFC/HTTPJSON/XSD/CHL-OFC-OFC_UpdateActivity_JSON_REQ_v2.xsd" ::)
declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-OFC-OFC/UpdateActivity/v2";
(:: import schema at "../CSC/CHL-OFC-OFC_UpdateActivity_v2_CSM.xsd" ::)

declare variable $CHL-OFC-OFC_UpdateActivity_REQ as element() (:: schema-element(ns1:CHL-OFC-OFC_UpdateActivity_REQ) ::) external;

declare function local:get_CHL-OFC-OFC_UpdateActivity_REQ($CHL-OFC-OFC_UpdateActivity_REQ as element() (:: schema-element(ns1:CHL-OFC-OFC_UpdateActivity_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
  <ns2:Root-Element>
      <ns2:resourceId>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:resourceId>
      <ns2:timeSlot>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:timeSlot>
      <ns2:customerName>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns2:customerName>
      <ns2:customerNumber>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns2:customerNumber>
      <ns2:customerPhone>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns2:customerPhone>
      <ns2:customerEmail>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns2:customerEmail>
      <ns2:customerCell>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns2:customerCell>
      <ns2:streetAddress>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:streetName)}</ns2:streetAddress>
      <ns2:city>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:city)}</ns2:city>
      <ns2:stateProvince>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:region)}</ns2:stateProvince>
      <ns2:longitude>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:AbsoluteLocalLocation/ns1:Y)}</ns2:longitude>
      <ns2:latitude>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:AbsoluteLocalLocation/ns1:X)}</ns2:latitude>
      <ns2:slaWindowStart>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns2:slaWindowStart>
      <ns2:slaWindowEnd>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
      <ns2:A_NETWORK_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:A_NETWORK_TYPE>
      <ns2:A_TROUBLE_ACTION>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns2:A_TROUBLE_ACTION>
      <ns2:A_CATEGORY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)}</ns2:A_CATEGORY>
      <ns2:A_SERVICE_ID>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns2:A_SERVICE_ID>
      <ns2:A_DEPARTAMENT>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:department)}</ns2:A_DEPARTAMENT>
      <ns2:A_TROUBLE_DIAGNOSIS>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns2:A_TROUBLE_DIAGNOSIS>
      <ns2:A_COMPLENTARY_ADDRESS>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:complentaryAddress)}</ns2:A_COMPLENTARY_ADDRESS>
      <ns2:A_ASSIGNMENTTIME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns2:A_ASSIGNMENTTIME>
      <ns2:A_CREATEDDATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns2:A_CREATEDDATE>
      <ns2:A_GROUP>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns2:A_GROUP>
      <ns2:A_PRIORITY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns2:A_PRIORITY>
      <ns2:A_ADDRESS_REFERENCE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:addressReference)}</ns2:A_ADDRESS_REFERENCE>
      <ns2:A_SEGMENT>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:A_SEGMENT>
      <ns2:A_SERVICE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns2:A_SERVICE>
      <ns2:A_ALIAS>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns2:A_ALIAS>
      <ns2:A_LEGACY_BO_USERNAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns2:A_LEGACY_BO_USERNAME>
      <ns2:A_SERVICE_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns2:A_SERVICE_TYPE>
      <ns2:A_SCHEDULER_USERNAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:A_SCHEDULER_USERNAME>
      <ns2:A_ENGINEER_NOTES>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:A_ENGINEER_NOTES>
      <ns2:A_SERVICE_TEMPLATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns2:A_SERVICE_TEMPLATE>
      <ns2:A_PORTABILITY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns2:A_PORTABILITY>
      <ns2:A_PRODUCT_DESC>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns2:A_PRODUCT_DESC>
      <ns2:A_BUILDING_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:buildingType)}</ns2:A_BUILDING_TYPE>
      <ns2:A_OFT_CLOSE_REASON>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:A_OFT_CLOSE_REASON>
      <ns2:A_CHANGE_RANGE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:changeRange)}</ns2:A_CHANGE_RANGE>
      <ns2:A_EQUIPMENTS>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)}</ns2:A_EQUIPMENTS>
      <ns2:A_CALCULATION_DATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:calculationDate)}</ns2:A_CALCULATION_DATE>
      <ns2:A_LAYER_MPLS>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns2:A_LAYER_MPLS>
      <ns2:A_NGN_MODEL>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns2:A_NGN_MODEL>
      <ns2:A_OFT_SUMMARY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns2:A_OFT_SUMMARY>
      <ns2:A_OFT_CLOSE_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:A_OFT_CLOSE_TYPE>
      <ns2:A_CURVE_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:curveType)}</ns2:A_CURVE_TYPE>
      <ns2:A_LEGACY_WORKTYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns2:A_LEGACY_WORKTYPE>
      <ns2:A_REQUESTED_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:A_REQUESTED_TYPE>
      <ns2:A_NETWORK_SPEED>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns2:A_NETWORK_SPEED>
      <ns2:A_LEGACY_STATUS>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns2:A_LEGACY_STATUS>
      <ns2:A_ANNULMENTTIME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns2:A_ANNULMENTTIME>
      <ns2:A_FINISHTIME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns2:A_FINISHTIME>
      <ns2:A_SALES_CHANNEL_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns2:A_SALES_CHANNEL_TYPE>
      <ns2:A_INSTALLER_NUMBER>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:partyIndividualNumber)}</ns2:A_INSTALLER_NUMBER>
      <ns2:A_BROADBAND_IND>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns2:A_BROADBAND_IND>
      <ns2:A_FORCED_APPOINTMENT>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)}</ns2:A_FORCED_APPOINTMENT>
      <ns2:A_VOICE_IND>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)}</ns2:A_VOICE_IND>
      <ns2:A_TROUBLE_DESCRIPTION>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns2:A_TROUBLE_DESCRIPTION>
      <ns2:A_DISPATCH_NOTES>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:dispatchNotes)}</ns2:A_DISPATCH_NOTES>
      <ns2:A_NETWORK_RESOURCES>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:A_NETWORK_RESOURCES>
      <ns2:A_SOURCE_SYSTEM>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:A_SOURCE_SYSTEM>
      <ns2:A_COMMERCIAL_CONTACT_EMAIL>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns2:A_COMMERCIAL_CONTACT_EMAIL>
      <ns2:A_COMMERCIAL_CONTACT_CELL>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns2:A_COMMERCIAL_CONTACT_CELL>
      <ns2:A_COMMERCIAL_CONTACT_PHONE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns2:A_COMMERCIAL_CONTACT_PHONE>
      <ns2:A_INSTALLATIONCOST>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns2:A_INSTALLATIONCOST>
      <ns2:A_STB_EQUIP_QTY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:A_STB_EQUIP_QTY>
      <ns2:A_PHONE_EQUIP_QTY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns2:A_PHONE_EQUIP_QTY>
      <ns2:A_WIFI_EQUIP_QTY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns2:A_WIFI_EQUIP_QTY>
      <ns2:A_ANIS_EQUIP_QTY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:A_ANIS_EQUIP_QTY>
      <ns2:A_ANIS_EQUIP_QTY_REQ>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns2:A_ANIS_EQUIP_QTY_REQ>
      <ns2:A_EQUIP_QTY_REQ>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:A_EQUIP_QTY_REQ>
      <ns2:A_APPOINTMENTTIMES>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:A_APPOINTMENTTIMES>
      <ns2:A_CUSTOMER_HOST_CELL>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns2:A_CUSTOMER_HOST_CELL>
      <ns2:A_STARTEDTRAVEL>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:startedTravel)}</ns2:A_STARTEDTRAVEL>
      <ns2:A_BACKOFFICEVALIDATION>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:BOValidation)}</ns2:A_BACKOFFICEVALIDATION>
      <ns2:A_SERVICE_VALIDATED>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceValidate)}</ns2:A_SERVICE_VALIDATED>
      <ns2:A_EFFICIENCY_CUMPLIED>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumplied)}</ns2:A_EFFICIENCY_CUMPLIED>
      <ns2:A_SAP_REPLACED_QTY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:sapReplacedQTY)}</ns2:A_SAP_REPLACED_QTY>
      <ns2:A_COPPER_PARAMETER>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:copperParameter)}</ns2:A_COPPER_PARAMETER>
      <ns2:A_FIBER_PARAMETER>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:fiberParameter)}</ns2:A_FIBER_PARAMETER>
      <ns2:A_RADIO_PARAMETER>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:radioParameter)}</ns2:A_RADIO_PARAMETER>
      <ns2:A_Q_PRESTACION_1>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</ns2:A_Q_PRESTACION_1>
      <ns2:A_Q_PRESTACION_2>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</ns2:A_Q_PRESTACION_2>
      <ns2:A_Q_PRESTACION_4>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</ns2:A_Q_PRESTACION_4>
      <ns2:A_Q_PRESTACION_5>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</ns2:A_Q_PRESTACION_5>
      <ns2:A_Q_PRESTACION_3>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</ns2:A_Q_PRESTACION_3>
      <ns2:A_NETWORK_ORDER_ID>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:networkOrderID)}</ns2:A_NETWORK_ORDER_ID>
      <ns2:A_FUME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:fume)}</ns2:A_FUME>
      <ns2:A_DECO_QTY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns2:A_DECO_QTY>
      <ns2:A_DECOPARAMETER_1>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:decoParameter1)}</ns2:A_DECOPARAMETER_1>
      <ns2:A_DECOPARAMETER_2>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:decoParameter2)}</ns2:A_DECOPARAMETER_2>
      <ns2:A_DECOPARAMETER_3>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:decoParameter3)}</ns2:A_DECOPARAMETER_3>
      <ns2:A_CUSTOMER_HOST_NAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerHostName)}</ns2:A_CUSTOMER_HOST_NAME>
      <ns2:A_WAREHOUSE_NAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:warehouseName)}</ns2:A_WAREHOUSE_NAME>
      <ns2:A_CONTRACTOR>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:A_CONTRACTOR>
      <ns2:A_SAP_REPLACED_NAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:sapReplacedName)}</ns2:A_SAP_REPLACED_NAME>
      <ns2:A_SAP_REPLACED_ID>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:sapReplacedID)}</ns2:A_SAP_REPLACED_ID>
      <ns2:A_SAP_REPLACED_RESOURCE_NAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:sapReplacedResourceName)}</ns2:A_SAP_REPLACED_RESOURCE_NAME>
      <ns2:A_CUSTOMERVALIDATOR>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:customerValidator)}</ns2:A_CUSTOMERVALIDATOR>
      <ns2:A_N2_RESP>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:n2Resp)}</ns2:A_N2_RESP>
      <ns2:A_CABECERA_FO>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:cabeceraFO)}</ns2:A_CABECERA_FO>
      <ns2:A_AFFECTED_NETWORK>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)}</ns2:A_AFFECTED_NETWORK>
      <ns2:A_NETWORK_ROUTE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:networkRoute)}</ns2:A_NETWORK_ROUTE>
      <ns2:A_SERVICE_DATA>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceData)}</ns2:A_SERVICE_DATA>
      <ns2:A_CABLE_NAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:cableName)}</ns2:A_CABLE_NAME>
      <ns2:A_CHECKLIST>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:checkList)}</ns2:A_CHECKLIST>
      <ns2:A_EPC>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:EPC)}</ns2:A_EPC>
      <ns2:A_CANCELATION_REASON>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)}</ns2:A_CANCELATION_REASON>
      <ns2:A_FUTURE_CONTACT_DATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)}</ns2:A_FUTURE_CONTACT_DATE>
      <ns2:A_RESCHEDULE_REQUESTER>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns2:A_RESCHEDULE_REQUESTER>
      <ns2:A_START_COORDINATES>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:startCoordinates)}</ns2:A_START_COORDINATES>
      <ns2:A_FINISH_COORDINATES>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:finishCoordinates)}</ns2:A_FINISH_COORDINATES>
      <ns2:A_PROGRESSTASK>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:A_PROGRESSTASK>
      <ns2:A_EXECUTED_ACTION>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:executedAction)}</ns2:A_EXECUTED_ACTION>
      <ns2:A_NOT_DONE_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:notDoneType)}</ns2:A_NOT_DONE_TYPE>
      <ns2:A_DERIVATION_TYPE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:derivationType)}</ns2:A_DERIVATION_TYPE>
      <ns2:A_DERIVATION_REASON>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:derivationReason)}</ns2:A_DERIVATION_REASON>
      <ns2:A_CLOSING_REASON>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:closeReason)}</ns2:A_CLOSING_REASON>
      <ns2:A_RESTART_TIME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:restartTime)}</ns2:A_RESTART_TIME>
      <ns2:A_ACCESS_TECH_QTY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:A_ACCESS_TECH_QTY>
      <ns2:A_ACCESS_TECHNOLOGY>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:accessTech)}</ns2:A_ACCESS_TECHNOLOGY>
      <ns2:A_TROUBLETICKET_RESPONSABLE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleTicketResponsable)}</ns2:A_TROUBLETICKET_RESPONSABLE>
      <ns2:A_TROUBLETICKET_REASON>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:A_TROUBLETICKET_REASON>
      <ns2:A_BO_USERNAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:A_BO_USERNAME>
      <ns2:A_EFFICIENCY_CUMPLIED_REASON>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:efficiencyCumpliedReason)}</ns2:A_EFFICIENCY_CUMPLIED_REASON>
      <ns2:A_PRESTACION_1>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</ns2:A_PRESTACION_1>
      <ns2:A_PRESTACION_2>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</ns2:A_PRESTACION_2>
      <ns2:A_PRESTACION_3>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</ns2:A_PRESTACION_3>
      <ns2:A_PRESTACION_4>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</ns2:A_PRESTACION_4>
      <ns2:A_COMUNA>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Address/ns1:commune)}</ns2:A_COMUNA>
      <ns2:A_PRESTACION_5>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</ns2:A_PRESTACION_5>
      <ns2:A_NOTES>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:A_NOTES>
      <ns2:A_BO_AUTH>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:BOAuthotization)}</ns2:A_BO_AUTH>
      <ns2:A_TICKET_CHECKLIST>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ns2:A_TICKET_CHECKLIST>
      <ns2:A_COMMERCIAL_CONTACT_NAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns2:A_COMMERCIAL_CONTACT_NAME>
      <ns2:A_SERVICE_TYPE_SIAC>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns2:A_SERVICE_TYPE_SIAC>
      <ns2:A_ESTIMATED_END_DATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:A_ESTIMATED_END_DATE>
      <ns2:A_EMISSION_DATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns2:A_EMISSION_DATE>
      <ns2:A_SIGNATURE_DATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns2:A_SIGNATURE_DATE>
      <ns2:A_DELIVERY_DATE_IMPLEMENTATION>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns2:A_DELIVERY_DATE_IMPLEMENTATION>
      <ns2:A_ENTRY_DATE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns2:A_ENTRY_DATE>
      <ns2:A_DEP_REG>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns2:A_DEP_REG>
      <ns2:A_ZONE_KM>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</ns2:A_ZONE_KM>
      <ns2:A_ZONE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns2:A_ZONE>
      <ns2:A_SALES_CHANNEL_ID>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns2:A_SALES_CHANNEL_ID>
      <ns2:A_PRODUCT_CODE>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns2:A_PRODUCT_CODE>
      <ns2:A_SIGLA_ACTIVIDAD>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)}</ns2:A_SIGLA_ACTIVIDAD>
      <ns2:A_CONTACT_NAME>{fn:data($CHL-OFC-OFC_UpdateActivity_REQ/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns2:A_CONTACT_NAME>
  </ns2:Root-Element>
};

local:get_CHL-OFC-OFC_UpdateActivity_REQ($CHL-OFC-OFC_UpdateActivity_REQ)
