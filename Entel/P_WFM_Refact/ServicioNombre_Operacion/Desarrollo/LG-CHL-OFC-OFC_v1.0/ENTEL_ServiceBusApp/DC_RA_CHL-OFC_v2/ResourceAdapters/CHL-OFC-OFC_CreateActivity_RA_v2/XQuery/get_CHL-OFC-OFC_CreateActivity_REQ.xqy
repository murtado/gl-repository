xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OFC-OFC/HTTPJSON/XSD/CHL-OFC-OFC_CreateActivity_JSON_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-OFC-OFC/CreateActivity/v2";
(:: import schema at "../CSC/CHL-OFC-OFC_CreateActivity_v2_CSM.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:CHL-OFC-OFC_CreateActivity_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:CHL-OFC-OFC_CreateActivity_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:language>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Language/ns1:alphabetName)}</ns2:language>
        <ns2:resourceId>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns2:resourceId>
        <ns2:activityType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:type)}</ns2:activityType>
        <ns2:timeZone>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)}</ns2:timeZone>
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
            then <ns2:A_ANNULMENTTIME>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns2:A_ANNULMENTTIME>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)
            then <ns2:date>{xs:date(fn:substring-before(fn:string($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentDate),"T"))}</ns2:date>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
            then <ns2:A_APPOINTMENTTIMES>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:A_APPOINTMENTTIMES>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
            then <ns2:A_ASSIGNMENTTIME>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns2:A_ASSIGNMENTTIME>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
            then <ns2:A_BROADBAND_IND>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns2:A_BROADBAND_IND>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
            then <ns2:A_COMMERCIAL_CONTACT_CELL>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns2:A_COMMERCIAL_CONTACT_CELL>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:contactName)
            then <ns2:A_CONTACT_NAME>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns2:A_CONTACT_NAME>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)
            then <ns2:A_CONTRACTOR>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:A_CONTRACTOR>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:createdDate)
            then <ns2:A_CREATEDDATE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns2:A_CREATEDDATE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
            then <ns2:A_DELIVERY_DATE_IMPLEMENTATION>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns2:A_DELIVERY_DATE_IMPLEMENTATION>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
            then <ns2:A_COMMERCIAL_CONTACT_EMAIL>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns2:A_COMMERCIAL_CONTACT_EMAIL>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
            then <ns2:A_EMISSION_DATE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns2:A_EMISSION_DATE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:entryDate)
            then <ns2:A_ENTRY_DATE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns2:A_ENTRY_DATE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
            then <ns2:A_ESTIMATED_END_DATE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:A_ESTIMATED_END_DATE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)
            then <ns2:apptNumber>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:apptNumber>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
            then <ns2:A_NOTES>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:A_NOTES>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
            then <ns2:A_LEGACY_STATUS>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns2:A_LEGACY_STATUS>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalType)
            then <ns2:A_LEGACY_WORKTYPE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns2:A_LEGACY_WORKTYPE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalZone)
            then <ns2:A_ZONE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns2:A_ZONE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:finishTime)
            then <ns2:A_FINISHTIME>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns2:A_FINISHTIME>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:installationCost)
            then <ns2:A_INSTALLATIONCOST>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns2:A_INSTALLATIONCOST>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
            then <ns2:A_LEGACY_BO_USERNAME>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns2:A_LEGACY_BO_USERNAME>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
            then <ns2:A_COMMERCIAL_CONTACT_NAME>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns2:A_COMMERCIAL_CONTACT_NAME>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
            then <ns2:A_NETWORK_RESOURCES>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:A_NETWORK_RESOURCES>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
            then <ns2:A_NETWORK_SPEED>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns2:A_NETWORK_SPEED>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:networkType)
            then <ns2:A_NETWORK_TYPE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:A_NETWORK_TYPE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
            then <ns2:A_OFT_CLOSE_REASON>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:A_OFT_CLOSE_REASON>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
            then <ns2:A_OFT_CLOSE_TYPE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:A_OFT_CLOSE_TYPE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)
            then <ns2:A_OFT_SUMMARY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns2:A_OFT_SUMMARY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
            then <ns2:A_COMMERCIAL_CONTACT_PHONE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns2:A_COMMERCIAL_CONTACT_PHONE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)
            then <ns2:A_REQUESTED_TYPE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:A_REQUESTED_TYPE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
            then <ns2:A_RESCHEDULE_REQUESTER>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns2:A_RESCHEDULE_REQUESTER>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
            then <ns2:A_SALES_CHANNEL_ID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns2:A_SALES_CHANNEL_ID>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
            then <ns2:A_SALES_CHANNEL_TYPE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns2:A_SALES_CHANNEL_TYPE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
            then <ns2:A_SCHEDULER_USERNAME>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:A_SCHEDULER_USERNAME>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:segment)
            then <ns2:A_SEGMENT>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:A_SEGMENT>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceID)
            then <ns2:A_SERVICE_ID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns2:A_SERVICE_ID>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceName)
            then <ns2:A_SERVICE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns2:A_SERVICE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
            then <ns2:A_SERVICE_TEMPLATE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns2:A_SERVICE_TEMPLATE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceType)
            then <ns2:A_SERVICE_TYPE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns2:A_SERVICE_TYPE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
            then <ns2:A_SERVICE_TYPE_SIAC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns2:A_SERVICE_TYPE_SIAC>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
            then <ns2:A_PRIORITY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns2:A_PRIORITY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
            then <ns2:A_SIGNATURE_DATE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns2:A_SIGNATURE_DATE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
            then <ns2:slaWindowEnd>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
            then <ns2:slaWindowStart>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns2:slaWindowStart>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
            then <ns2:A_SOURCE_SYSTEM>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:A_SOURCE_SYSTEM>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:startTime)
            then <ns2:startTime>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:startTime)}</ns2:startTime>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
            then <ns2:A_TROUBLE_ACTION>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns2:A_TROUBLE_ACTION>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
            then <ns2:A_TROUBLE_DESCRIPTION>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns2:A_TROUBLE_DESCRIPTION>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
            then <ns2:A_TROUBLE_DIAGNOSIS>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns2:A_TROUBLE_DIAGNOSIS>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:depReg)
            then <ns2:A_DEP_REG>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns2:A_DEP_REG>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
            then <ns2:latitude>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:latitude>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
            then <ns2:longitude>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:longitude>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)
            then <ns2:A_ADDRESS_REFERENCE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)}</ns2:A_ADDRESS_REFERENCE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)
            then <ns2:A_BUILDING_TYPE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)}</ns2:A_BUILDING_TYPE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)
            then <ns2:city>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)}</ns2:city>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)
            then <ns2:A_COMUNA>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)}</ns2:A_COMUNA>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)
            then <ns2:A_COMPLENTARY_ADDRESS>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)}</ns2:A_COMPLENTARY_ADDRESS>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)
            then <ns2:A_DEPARTAMENT>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)}</ns2:A_DEPARTAMENT>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)
            then <ns2:stateProvince>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)}</ns2:stateProvince>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)
            then <ns2:streetAddress>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)}</ns2:streetAddress>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)
            then <ns2:A_ALIAS>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns2:A_ALIAS>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)
            then <ns2:A_GROUP>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns2:A_GROUP>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)
            then <ns2:A_PORTABILITY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns2:A_PORTABILITY>
            else ()
        }
        {
         if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)
         then  <ns2:customerPhone>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns2:customerPhone>
         else ()
         }
         {
         if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)
         then  
        <ns2:customerCell>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns2:customerCell>
          else ()
         }
        {
         if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)
         then  
        <ns2:customerEmail>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns2:customerEmail>
         else ()
         }
        {
         if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)
         then  
        <ns2:customerNumber>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns2:customerNumber>
         else ()
         }
         {
         if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)
         then  
        <ns2:customerName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns2:customerName>
         else ()
         }
        {
         if ($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)
         then   <ns2:A_INSTALLER_NUMBER>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns2:A_INSTALLER_NUMBER>
         else ()
         }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
            then <ns2:A_ANIS_EQUIP_QTY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:A_ANIS_EQUIP_QTY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
            then <ns2:A_ANIS_EQUIP_QTY_REQ>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns2:A_ANIS_EQUIP_QTY_REQ>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)
            then <ns2:A_CATEGORY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)}</ns2:A_CATEGORY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
            then <ns2:A_DECO_QTY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns2:A_DECO_QTY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
            then <ns2:A_EQUIP_QTY_REQ>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:A_EQUIP_QTY_REQ>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)
            then <ns2:A_EQUIPMENTS>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)}</ns2:A_EQUIPMENTS>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
            then <ns2:A_PRODUCT_CODE>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns2:A_PRODUCT_CODE>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
            then <ns2:A_PRODUCT_DESC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns2:A_PRODUCT_DESC>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
            then <ns2:A_LAYER_MPLS>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns2:A_LAYER_MPLS>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
            then <ns2:A_NGN_MODEL>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns2:A_NGN_MODEL>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
            then <ns2:A_PHONE_EQUIP_QTY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns2:A_PHONE_EQUIP_QTY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
            then <ns2:A_STB_EQUIP_QTY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:A_STB_EQUIP_QTY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)
            then <ns2:A_VOICE_IND>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)}</ns2:A_VOICE_IND>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
            then <ns2:A_WIFI_EQUIP_QTY>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns2:A_WIFI_EQUIP_QTY>
            else ()
        }
        {
            if ($Request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
            then <ns2:timeSlot>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:timeSlot>
            else ()
        }

    </ns2:Root-Element>
};

local:func($Request)