xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ServiceName";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OFC-OFC/HTTPJSON/XSD/CHL-OFC-OFC_CancelActivity_JSON_RSP.xsd" ::)
declare namespace ns3="http://www.entel.cl/CSM/RA/CHL-OFC-OFC/CancelActivity/v2";
(:: import schema at "../CSC/CHL-OFC-OFC_CancelActivity_v2_CSM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";


declare variable $cancelActivityRsp as element() (:: schema-element(ns1:Root-Element) ::) external;
declare variable $result as element() (:: schema-element(ns2:Result) ::) external;

declare function local:func($cancelActivityRsp as element() (:: schema-element(ns1:Root-Element) ::), 
                            $result as element() (:: schema-element(ns2:Result) ::)) 
                            as element() (:: schema-element(ns3:CHL-OFC-OFC_CancelActivity_RSP) ::) {
    <ns3:CHL-OFC-OFC_CreateActivity_RSP>
        {$result}
        <ns3:Body>
            <ns3:WorkOrder>
                {
                if(fn:data($cancelActivityRsp/ns1:date))then
                <ns3:appointmentDate>{fn:data($cancelActivityRsp/ns1:date)}</ns3:appointmentDate>
                else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:apptNumber))then
                <ns3:externalID>{fn:data($cancelActivityRsp/ns1:apptNumber)}</ns3:externalID>
                else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:activityId))then
                <ns3:ID>{fn:data($cancelActivityRsp/ns1:activityId)}</ns3:ID>
                  else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:recordType))then  
                <ns3:recordType>{fn:data($cancelActivityRsp/ns1:recordType)}</ns3:recordType>
                         else()
                }
                {
                    if ($cancelActivityRsp/ns1:A_SOURCE_SYSTEM)
                    then <ns3:sourceSystem>{fn:data($cancelActivityRsp/ns1:A_SOURCE_SYSTEM)}</ns3:sourceSystem>
                    else ()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:travelTime))then 
                <ns3:standardDrivingTime>{fn:data($cancelActivityRsp/ns1:travelTime)}</ns3:standardDrivingTime>
                else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:duration))then
                <ns3:standardDuration>{fn:data($cancelActivityRsp/ns1:duration)}</ns3:standardDuration>
                else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:startTime))then
                <ns3:startTime>{fn:data($cancelActivityRsp/ns1:startTime)}</ns3:startTime>
                                else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:status))then
                <ns3:status>{fn:data($cancelActivityRsp/ns1:status)}</ns3:status>
                                else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:timeOfBooking))then
                <ns3:timeOfBooking>{fn:data($cancelActivityRsp/ns1:timeOfBooking)}</ns3:timeOfBooking>
                else()
                }
                {
                if(fn:data($cancelActivityRsp/ns1:activityType))then
                <ns3:type>{fn:data($cancelActivityRsp/ns1:activityType)}</ns3:type>
                else()
                }
                {
                if($cancelActivityRsp/ns1:linkedActivities)then
                <ns3:linkedActivities>
                    <ns3:links>
                        <ns3:rel>{fn:data($cancelActivityRsp/ns1:linkedActivities/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($cancelActivityRsp/ns1:linkedActivities/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:linkedActivities>
                else()
                }
                {
                if($cancelActivityRsp/ns1:links)then
                <ns3:linkList>
                    <ns3:links>
                        <ns3:rel>{fn:data($cancelActivityRsp/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($cancelActivityRsp/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:linkList>
                                else()
                }
                {
                if($cancelActivityRsp/ns1:requiredInventories)then
                <ns3:RequiredInventories>
                    <ns3:links>
                        <ns3:rel>{fn:data($cancelActivityRsp/ns1:requiredInventories/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($cancelActivityRsp/ns1:requiredInventories/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:RequiredInventories>
                else()
                }
                {
                if($cancelActivityRsp/ns1:resourcePreferences)then
                <ns3:ResourcePreferences>
                    <ns3:links>
                        <ns3:rel>{fn:data($cancelActivityRsp/ns1:resourcePreferences/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($cancelActivityRsp/ns1:resourcePreferences/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:ResourcePreferences>
                                else()
                }
                {
                if($cancelActivityRsp/ns1:workSkills)then
                <ns3:workSkill>
                    <ns3:links>
                        <ns3:rel>{fn:data($cancelActivityRsp/ns1:workSkills/ns1:links/ns1:rel)}</ns3:rel>
                        <ns3:href>{fn:data($cancelActivityRsp/ns1:workSkills/ns1:links/ns1:href)}</ns3:href>
                    </ns3:links>
                </ns3:workSkill>
                                else()
                }

                <ns3:absoluteLocalLocation>
                    <ns3:timezone>
                        {
                         if(fn:data($cancelActivityRsp/ns1:timeZone))then
                        <ns3:name>{fn:data($cancelActivityRsp/ns1:timeZone)}</ns3:name>
                        else()
                        }
                        {
                         if(fn:data($cancelActivityRsp/ns1:timeZoneIANA))then
                        <ns3:timeZoneIANA>{fn:data($cancelActivityRsp/ns1:timeZoneIANA)}</ns3:timeZoneIANA>
                        else()
                        }
                    </ns3:timezone>
                </ns3:absoluteLocalLocation>
                {
                if($cancelActivityRsp/ns1:customerName)then
                <ns3:customerAccount>
                    <ns3:contact>
                         <ns3:IndividualName>
                            <ns3:formatedName>{fn:data($cancelActivityRsp/ns1:customerName)}</ns3:formatedName>
                        </ns3:IndividualName>
                    </ns3:contact>
                </ns3:customerAccount>
                else()
                }
                {
                if($cancelActivityRsp/ns1:language)then
                <ns3:Language>
                    <ns3:alphabetName>{fn:data($cancelActivityRsp/ns1:language)}</ns3:alphabetName>
                </ns3:Language>
                                else()
                }

                <ns3:PartyResource>
                   {
                   if(fn:data($cancelActivityRsp/ns1:resourceId))then 
                    <ns3:ID>{fn:data($cancelActivityRsp/ns1:resourceId)}</ns3:ID>
                    else()
                    }
                    {
                   if(fn:data($cancelActivityRsp/ns1:resourceInternalId))then 
                    <ns3:internalID>{fn:data($cancelActivityRsp/ns1:resourceInternalId)}</ns3:internalID>
                    else
                    ()
                    }
                    {
                   if(fn:data($cancelActivityRsp/ns1:resourceTimeZone))then 
                    <ns3:resourceTimeZone>{fn:data($cancelActivityRsp/ns1:resourceTimeZone)}</ns3:resourceTimeZone>
                    else()
                    }
                                        {
                   if(fn:data($cancelActivityRsp/ns1:resourceTimeZoneDiff))then 
                    <ns3:UTCOffset>{fn:data($cancelActivityRsp/ns1:resourceTimeZoneDiff)}</ns3:UTCOffset>
                    else()
                    }
                    {
                   if(fn:data($cancelActivityRsp/ns1:resourceTimeZoneIANA))then 
                    <ns3:resourceTimeZoneIANA>{fn:data($cancelActivityRsp/ns1:resourceTimeZoneIANA)}</ns3:resourceTimeZoneIANA>
                    else()
                    }
                </ns3:PartyResource>
                <ns3:Product>
                </ns3:Product>
            </ns3:WorkOrder>
        </ns3:Body>
    </ns3:CHL-OFC-OFC_CreateActivity_RSP>
};

local:func($cancelActivityRsp, $result)