xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CorrelationManager/createGroupMember/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/createGroupMember_CorrelationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/createMember";
(:: import schema at "../JCA/createGroupMember/createGroupMember_sp.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $SC_REQ as element() (:: schema-element(ns1:CreateGroupMemberREQ) ::) external;

declare function local:func($SC_REQ as element() (:: schema-element(ns1:CreateGroupMemberREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    
    let $Member:=$SC_REQ/cor:Member
    
    return
    
      <ns2:InputParameters>
          <ns2:P_GRP_TAG>{fn:data($SC_REQ/cor:GroupTag)}</ns2:P_GRP_TAG>
          <ns2:GRP_MEM>
              <ns2:MEM_NAME_>{fn:data($Member/@cor:memberName)}</ns2:MEM_NAME_>
              {
                  if ($Member/@cor:memberLabel)
                  then <ns2:MEM_LABEL_>{fn:data($Member/@cor:memberLabel)}</ns2:MEM_LABEL_>
                  else ()
              }
              <ns2:MEM_TYPE_>{fn:data($Member/@cor:memberType)}</ns2:MEM_TYPE_>
              <ns2:MEM_CORR_ID_>{fn:data($Member/@cor:memberCorrID)}</ns2:MEM_CORR_ID_>
              {
                  if ($Member/@cor:memberAddress)
                  then <ns2:MEM_ADDR_>{fn:data($Member/@cor:memberAddress)}</ns2:MEM_ADDR_>
                  else ()
              }
              {
              if(exists($Member/cor:Flags/cor:Flag)) then
                <ns2:MEM_FLAGS_>
                    {
                        for $Flag in $Member/cor:Flags/cor:Flag
                        return 
                        <ns2:MEM_FLAGS__ITEM>
                            <ns2:NAME_>{fn:data($Flag/@name)}</ns2:NAME_>
                            <ns2:VALUE_>{fn:data($Flag/@value)}</ns2:VALUE_></ns2:MEM_FLAGS__ITEM>
                    }
                </ns2:MEM_FLAGS_>
              else
                ()
              }
          </ns2:GRP_MEM>
      </ns2:InputParameters>
};

local:func($SC_REQ)
