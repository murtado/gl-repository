xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/GetCapacity/Get/v1";
(:: import schema at "../CSC/CHL-OFC-OFC_GetCapacity_v1_EBM.xsd" ::)
declare namespace ns1="urn:toa:capacity";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OFC-OFC/HTTPSOAP11/WSDL/capacity.wsdl" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Response as element() (:: schema-element(ns1:get_capacity_response) ::) external;
declare variable $Result as element() (:: schema-element(ns4:Result) ::) external;


declare function local:func($Response as element() (:: schema-element(ns1:get_capacity_response) ::),
                            $Result as element() (:: schema-element(ns4:Result) ::)) as element() (:: schema-element(ns2:GetCapacity_RSP) ::) {
    <ns2:GetCapacity_RSP>
        <ns3:ResponseHeader>
            <ns3:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="" eventID="">
                <ns3:Service>
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel>
            </ns3:Channel>
                {
                   $Result}

        </ns3:ResponseHeader>
        <ns2:Body>
            <ns2:Allocation>
                {
                    if ($Response/activity_duration)
                    then <ns2:StandardDuration>{fn:data($Response/activity_duration)}</ns2:StandardDuration>
                    else ()
                }
                {
                    if ($Response/activity_travel_time)
                    then <ns2:StandardDrivingTime>{fn:data($Response/activity_travel_time)}</ns2:StandardDrivingTime>
                    else ()
                }
                {
                if($Response/capacity)then
                <ns2:Capacity>
                {
                    for $capacity in $Response/capacity
                      return 
                    <ns2:AvailableCapacity>
                        <ns2:AlocationDate>{fn:data($capacity/date)}</ns2:AlocationDate>
                        {
                            if ($capacity/time_slot)
                            then <ns2:ApplicableDuring>{fn:data($capacity/time_slot)}</ns2:ApplicableDuring>
                            else ()
                        }
                        {
                            if ($capacity/work_skill)
                            then <ns2:PartyRoleName>{fn:data($capacity/work_skill)}</ns2:PartyRoleName>
                            else ()
                        }
                        <ns2:Quota>{fn:data($capacity/quota)}</ns2:Quota>
                        <ns2:TimeAvailable>{fn:data($capacity/available)}</ns2:TimeAvailable>
                        {
                            if ($capacity/location)
                            then <ns2:PlaceID>{fn:data($capacity/location)}</ns2:PlaceID>
                            else ()
                        }
                    </ns2:AvailableCapacity>
                    }
                    {
                    for $time_slot_info in $Response/time_slot_info
                      return 
                    <ns2:DuringInfo>
                        <ns2:Name>{fn:data($time_slot_info/name)}</ns2:Name>
                        <ns2:Label>{fn:data($time_slot_info/label)}</ns2:Label>
                        {
                            if ($time_slot_info/time_from)
                            then <ns2:TimeFrom>{fn:data($time_slot_info/time_from)}</ns2:TimeFrom>
                            else ()
                        }
                        {
                            if ($time_slot_info/time_to)
                            then <ns2:TimeTo>{fn:data($time_slot_info/time_to)}</ns2:TimeTo>
                            else ()
                        }
                    </ns2:DuringInfo>
                    }
                </ns2:Capacity>
                else
                ()
                }
            </ns2:Allocation>
        </ns2:Body>
    </ns2:GetCapacity_RSP>
};

local:func($Response,$Result)