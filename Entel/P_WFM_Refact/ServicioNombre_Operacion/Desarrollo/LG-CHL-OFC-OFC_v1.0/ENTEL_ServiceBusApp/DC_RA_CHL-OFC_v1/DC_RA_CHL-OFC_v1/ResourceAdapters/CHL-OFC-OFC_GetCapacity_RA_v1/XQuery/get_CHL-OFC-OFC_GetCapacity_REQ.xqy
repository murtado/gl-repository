xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/GetCapacity/Get/v1";
(:: import schema at "../CSC/CHL-OFC-OFC_GetCapacity_v1_EBM.xsd" ::)
declare namespace ns2="urn:toa:capacity";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OFC-OFC/HTTPSOAP11/WSDL/capacity.wsdl" ::)

declare variable $GetCapacity_REQ as element() (:: schema-element(ns1:GetCapacity_REQ) ::) external;

declare function local:func($GetCapacity_REQ as element() (:: schema-element(ns1:GetCapacity_REQ) ::)) as element() (:: schema-element(ns2:get_capacity) ::) {
    <ns2:get_capacity>
        {
            for $AllocationDate in $GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:Datelist/ns1:AllocationDate
            return 
            <date>{xs:date(fn:substring-before(fn:string($AllocationDate),"T"))}</date>
        }
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:Place/ns1:ID)
            then
              let $ID := $GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:Place/ns1:ID
            return 
              <location>{fn:data($ID)}</location>
            else
              ()
        }
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsCalculatedDurationRequired)
            then <calculate_duration>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsCalculatedDurationRequired)}</calculate_duration>
            else ()
        }
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsCalculatedTravelRequired)
            then <calculate_travel_time>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsCalculatedTravelRequired)}</calculate_travel_time>
            else ()
        }
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsCalculatedSkillRequired)
            then <calculate_work_skill>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsCalculatedSkillRequired)}</calculate_work_skill>
            else ()
        }
        {
        if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsReturnTimeSlotInfo)
            then <return_time_slot_info>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsReturnTimeSlotInfo)}</return_time_slot_info>
            else
            ()
        }
        {
        if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsDetermineLocationByWorkZone)
            then  <determine_location_by_work_zone>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsDetermineLocationByWorkZone)}</determine_location_by_work_zone>
            else
            ()
        }
        {
        if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsNotAggregateResults)
            then <dont_aggregate_results>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:IsNotAggregateResults)}</dont_aggregate_results>
            else
            ()
        }       
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:RemainingTime)
            then <min_time_to_end_of_time_slot>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:RemainingTime)}</min_time_to_end_of_time_slot>
            else ()
        }
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:ReserveFor)
            then <default_duration>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:ReserveFor)}</default_duration>
            else ()
        }
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:WorkSchedule/ns1:ApplicableDuring)
            then
            let $ApplicableDuring := $GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:WorkSchedule/ns1:ApplicableDuring
            return 
            <time_slot>{fn:data($ApplicableDuring)}</time_slot>
            else
            ()
        }
        {
            if ($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:PartyRole/ns1:Name)
            then
            let $Name := $GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:PartyRole/ns1:Name
            return 
            <work_skill>{fn:data($GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:PartyRole/ns1:Name)}</work_skill>
            else
            ()
        }
        {
            for $AllocationSpecChar in $GetCapacity_REQ/ns1:Body/ns1:Allocation/ns1:AllocationSpecChar
            return 
            <activity_field>
                <name>{fn:data($AllocationSpecChar/ns1:name)}</name>
                <value>{fn:data($AllocationSpecChar/ns1:value)}</value>
            </activity_field>
        }

    </ns2:get_capacity>
};

local:func($GetCapacity_REQ)