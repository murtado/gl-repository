xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/CHL-OFC-OFC_GetActivity_JSON_RSP.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/GetActivity/Get/v1";
(:: import schema at "../CSC/CHL-OFC-OFC_GetActivity_v1_EBM.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $getLWResponse as element() (:: schema-element(ns1:Root-Element) ::) external;
declare variable $result as element() (:: schema-element(ns3:Result) ::) external;

declare function local:func($getLWResponse as element() (:: schema-element(ns1:Root-Element) ::), 
                            $result as element() (:: schema-element(ns3:Result) ::)) 
                            as element() (:: schema-element(ns2:GetActivity_RSP) ::) {
    <ns2:GetActivity_RSP>
        <ns4:ResponseHeader>
            <ns4:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns4:Consumer>
            <ns4:Trace clientReqTimestamp="" eventID="">
                <ns4:Service>
                </ns4:Service>
            </ns4:Trace>
            <ns4:Channel>
            </ns4:Channel>
            <ns3:Result status="{fn:data($result/@status)}">
                {
                    if ($result/@description)
                    then attribute description {fn:data($result/@description)}
                    else ()
                }
                {
                    if ($result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($result/ns5:SourceError/@code)
                                then attribute code {fn:data($result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($result/ns5:SourceError/@description)
                                then attribute description {fn:data($result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($result/ns3:CorrelativeErrors)
                    then 
                        <ns3:CorrelativeErrors>
                            {
                                for $SourceError in $result/ns3:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns3:CorrelativeErrors>
                    else ()
                }
            </ns3:Result>

        </ns4:ResponseHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($getLWResponse/ns1:A_AFFECTED_NETWORK)
                    then <ns2:affectedNetwork>{fn:data($getLWResponse/ns1:A_AFFECTED_NETWORK)}</ns2:affectedNetwork>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_ANNULMENTTIME)
                    then <ns2:annulmentTime>{fn:data($getLWResponse/ns1:A_ANNULMENTTIME)}</ns2:annulmentTime>
                    else ()
                }
                <ns2:appointmentDate>{fn:data($getLWResponse/ns1:date)}</ns2:appointmentDate>
                {
                    if ($getLWResponse/ns1:A_APPOINTMENTTIMES)
                    then <ns2:appointmentTimes>{fn:data($getLWResponse/ns1:A_APPOINTMENTTIMES)}</ns2:appointmentTimes>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_ASSIGNMENTTIME)
                    then <ns2:assignmentTime>{fn:data($getLWResponse/ns1:A_ASSIGNMENTTIME)}</ns2:assignmentTime>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_BO_AUTH)
                    then <ns2:BOAuthotization>{fn:data($getLWResponse/ns1:A_BO_AUTH)}</ns2:BOAuthotization>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_BO_USERNAME)
                    then <ns2:BOUserName>{fn:data($getLWResponse/ns1:A_BO_USERNAME)}</ns2:BOUserName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_BACKOFFICEVALIDATION)
                    then <ns2:BOValidation>{fn:data($getLWResponse/ns1:A_BACKOFFICEVALIDATION)}</ns2:BOValidation>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_BROADBAND_IND)
                    then <ns2:broadbandIndicator>{fn:data($getLWResponse/ns1:A_BROADBAND_IND)}</ns2:broadbandIndicator>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CABECERA_FO)
                    then <ns2:cabeceraFO>{fn:data($getLWResponse/ns1:A_CABECERA_FO)}</ns2:cabeceraFO>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CABLE_NAME)
                    then <ns2:cableName>{fn:data($getLWResponse/ns1:A_CABLE_NAME)}</ns2:cableName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CALCULATION_DATE)
                    then <ns2:calculationDate>{fn:data($getLWResponse/ns1:A_CALCULATION_DATE)}</ns2:calculationDate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CANCELATION_REASON)
                    then <ns2:cancelationReason>{fn:data($getLWResponse/ns1:A_CANCELATION_REASON)}</ns2:cancelationReason>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_COMMERCIAL_CONTACT_CELL)
                    then <ns2:cellComercial>{fn:data($getLWResponse/ns1:A_COMMERCIAL_CONTACT_CELL)}</ns2:cellComercial>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CHANGE_RANGE)
                    then <ns2:changeRange>{fn:data($getLWResponse/ns1:A_CHANGE_RANGE)}</ns2:changeRange>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CHECKLIST)
                    then <ns2:checkList>{fn:data($getLWResponse/ns1:A_CHECKLIST)}</ns2:checkList>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CLOSING_REASON)
                    then <ns2:closeReason>{fn:data($getLWResponse/ns1:A_CLOSING_REASON)}</ns2:closeReason>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CONTACT_NAME)
                    then <ns2:contactName>{fn:data($getLWResponse/ns1:A_CONTACT_NAME)}</ns2:contactName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CONTRACTOR)
                    then <ns2:contractor>{fn:data($getLWResponse/ns1:A_CONTRACTOR)}</ns2:contractor>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_COPPER_PARAMETER)
                    then <ns2:copperParameter>{fn:data($getLWResponse/ns1:A_COPPER_PARAMETER)}</ns2:copperParameter>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CREATEDDATE)
                    then <ns2:createdDate>{fn:data($getLWResponse/ns1:A_CREATEDDATE)}</ns2:createdDate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CURVE_TYPE)
                    then <ns2:curveType>{fn:data($getLWResponse/ns1:A_CURVE_TYPE)}</ns2:curveType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CUSTOMER_HOST_CELL)
                    then <ns2:customerHostCell>{fn:data($getLWResponse/ns1:A_CUSTOMER_HOST_CELL)}</ns2:customerHostCell>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CUSTOMER_HOST_NAME)
                    then <ns2:customerHostName>{fn:data($getLWResponse/ns1:A_CUSTOMER_HOST_NAME)}</ns2:customerHostName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_CUSTOMERVALIDATOR)
                    then <ns2:customerValidator>{fn:data($getLWResponse/ns1:A_CUSTOMERVALIDATOR)}</ns2:customerValidator>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_DECOPARAMETER_1)
                    then <ns2:decoParameter1>{fn:data($getLWResponse/ns1:A_DECOPARAMETER_1)}</ns2:decoParameter1>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_DECOPARAMETER_2)
                    then <ns2:decoParameter2>{fn:data($getLWResponse/ns1:A_DECOPARAMETER_2)}</ns2:decoParameter2>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_DECOPARAMETER_3)
                    then <ns2:decoParameter3>{fn:data($getLWResponse/ns1:A_DECOPARAMETER_3)}</ns2:decoParameter3>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_DELIVERY_DATE_IMPLEMENTATION)
                    then <ns2:deliveryImplementacionDate>{fn:data($getLWResponse/ns1:A_DELIVERY_DATE_IMPLEMENTATION)}</ns2:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_DERIVATION_REASON)
                    then <ns2:derivationReason>{fn:data($getLWResponse/ns1:A_DERIVATION_REASON)}</ns2:derivationReason>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_DERIVATION_TYPE)
                    then <ns2:derivationType>{fn:data($getLWResponse/ns1:A_DERIVATION_TYPE)}</ns2:derivationType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_DISPATCH_NOTES)
                    then <ns2:dispatchNotes>{fn:data($getLWResponse/ns1:A_DISPATCH_NOTES)}</ns2:dispatchNotes>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_EFFICIENCY_CUMPLIED)
                    then <ns2:efficiencyCumplied>{fn:data($getLWResponse/ns1:A_EFFICIENCY_CUMPLIED)}</ns2:efficiencyCumplied>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_EFFICIENCY_CUMPLIED_REASON)
                    then <ns2:efficiencyCumpliedReason>{fn:data($getLWResponse/ns1:A_EFFICIENCY_CUMPLIED_REASON)}</ns2:efficiencyCumpliedReason>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_COMMERCIAL_CONTACT_EMAIL)
                    then <ns2:emailComercial>{fn:data($getLWResponse/ns1:A_COMMERCIAL_CONTACT_EMAIL)}</ns2:emailComercial>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_EMISSION_DATE)
                    then <ns2:emissionDate>{fn:data($getLWResponse/ns1:A_EMISSION_DATE)}</ns2:emissionDate>
                    else ()
                }
                <ns2:endTime>{fn:data($getLWResponse/ns1:endTime)}</ns2:endTime>
                {
                    if ($getLWResponse/ns1:A_ENGINEER_NOTES)
                    then <ns2:engineerNotes>{fn:data($getLWResponse/ns1:A_ENGINEER_NOTES)}</ns2:engineerNotes>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_ENTRY_DATE)
                    then <ns2:entryDate>{fn:data($getLWResponse/ns1:A_ENTRY_DATE)}</ns2:entryDate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_EPC)
                    then <ns2:EPC>{fn:data($getLWResponse/ns1:A_EPC)}</ns2:EPC>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_ESTIMATED_END_DATE)
                    then <ns2:estimatedFinishDate>{fn:data($getLWResponse/ns1:A_ESTIMATED_END_DATE)}</ns2:estimatedFinishDate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_EXECUTED_ACTION)
                    then <ns2:executedAction>{fn:data($getLWResponse/ns1:A_EXECUTED_ACTION)}</ns2:executedAction>
                    else ()
                }
                <ns2:externalID>{fn:data($getLWResponse/ns1:apptNumber)}</ns2:externalID>
                {
                    if ($getLWResponse/ns1:A_NOTES)
                    then <ns2:externalNotes>{fn:data($getLWResponse/ns1:A_NOTES)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_LEGACY_STATUS)
                    then <ns2:externalStatus>{fn:data($getLWResponse/ns1:A_LEGACY_STATUS)}</ns2:externalStatus>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_LEGACY_WORKTYPE)
                    then <ns2:externalType>{fn:data($getLWResponse/ns1:A_LEGACY_WORKTYPE)}</ns2:externalType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_ZONE)
                    then <ns2:externalZone>{fn:data($getLWResponse/ns1:A_ZONE)}</ns2:externalZone>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_FIBER_PARAMETER)
                    then <ns2:fiberParameter>{fn:data($getLWResponse/ns1:A_FIBER_PARAMETER)}</ns2:fiberParameter>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_FINISH_COORDINATES)
                    then <ns2:finishCoordinates>{fn:data($getLWResponse/ns1:A_FINISH_COORDINATES)}</ns2:finishCoordinates>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_FINISHTIME)
                    then <ns2:finishTime>{fn:data($getLWResponse/ns1:A_FINISHTIME)}</ns2:finishTime>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_FORCED_APPOINTMENT)
                    then <ns2:forcedAppointment>{fn:data($getLWResponse/ns1:A_FORCED_APPOINTMENT)}</ns2:forcedAppointment>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_FUME)
                    then <ns2:fume>{fn:data($getLWResponse/ns1:A_FUME)}</ns2:fume>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_FUTURE_CONTACT_DATE)
                    then <ns2:futureContactDate>{fn:data($getLWResponse/ns1:A_FUTURE_CONTACT_DATE)}</ns2:futureContactDate>
                    else ()
                }
                <ns2:ID>{fn:data($getLWResponse/ns1:activityId)}</ns2:ID>
                {
                    if ($getLWResponse/ns1:A_INSTALLATIONCOST)
                    then <ns2:InstallationCost>{fn:data($getLWResponse/ns1:A_INSTALLATIONCOST)}</ns2:InstallationCost>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_LEGACY_BO_USERNAME)
                    then <ns2:legacyBOUserName>{fn:data($getLWResponse/ns1:A_LEGACY_BO_USERNAME)}</ns2:legacyBOUserName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_N2_RESP)
                    then <ns2:n2Resp>{fn:data($getLWResponse/ns1:A_N2_RESP)}</ns2:n2Resp>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_COMMERCIAL_CONTACT_NAME)
                    then <ns2:nameComercial>{fn:data($getLWResponse/ns1:A_COMMERCIAL_CONTACT_NAME)}</ns2:nameComercial>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_NETWORK_ORDER_ID)
                    then <ns2:networkOrderID>{fn:data($getLWResponse/ns1:A_NETWORK_ORDER_ID)}</ns2:networkOrderID>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_NETWORK_RESOURCES)
                    then <ns2:networkResourceID>{fn:data($getLWResponse/ns1:A_NETWORK_RESOURCES)}</ns2:networkResourceID>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_NETWORK_ROUTE)
                    then <ns2:networkRoute>{fn:data($getLWResponse/ns1:A_NETWORK_ROUTE)}</ns2:networkRoute>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_NETWORK_SPEED)
                    then <ns2:networkSpeed>{fn:data($getLWResponse/ns1:A_NETWORK_SPEED)}</ns2:networkSpeed>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_NETWORK_TYPE)
                    then <ns2:networkType>{fn:data($getLWResponse/ns1:A_NETWORK_TYPE)}</ns2:networkType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_NOT_DONE_TYPE)
                    then <ns2:notDoneType>{fn:data($getLWResponse/ns1:A_NOT_DONE_TYPE)}</ns2:notDoneType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_OFT_CLOSE_REASON)
                    then <ns2:OFTcloseReason>{fn:data($getLWResponse/ns1:A_OFT_CLOSE_REASON)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_OFT_CLOSE_TYPE)
                    then <ns2:OFTcloseType>{fn:data($getLWResponse/ns1:A_OFT_CLOSE_TYPE)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_OFT_SUMMARY)
                    then <ns2:OFTsummary>{fn:data($getLWResponse/ns1:A_OFT_SUMMARY)}</ns2:OFTsummary>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_COMMERCIAL_CONTACT_PHONE)
                    then <ns2:phoneComercial>{fn:data($getLWResponse/ns1:A_COMMERCIAL_CONTACT_PHONE)}</ns2:phoneComercial>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_Q_PRESTACION_1)
                    then <ns2:prestacionQTY1>{fn:data($getLWResponse/ns1:A_Q_PRESTACION_1)}</ns2:prestacionQTY1>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_Q_PRESTACION_2)
                    then <ns2:prestacionQTY2>{fn:data($getLWResponse/ns1:A_Q_PRESTACION_2)}</ns2:prestacionQTY2>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_Q_PRESTACION_3)
                    then <ns2:prestacionQTY3>{fn:data($getLWResponse/ns1:A_Q_PRESTACION_3)}</ns2:prestacionQTY3>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_Q_PRESTACION_4)
                    then <ns2:prestacionQTY4>{fn:data($getLWResponse/ns1:A_Q_PRESTACION_4)}</ns2:prestacionQTY4>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_Q_PRESTACION_5)
                    then <ns2:prestacionQTY5>{fn:data($getLWResponse/ns1:A_Q_PRESTACION_5)}</ns2:prestacionQTY5>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_PRESTACION_1)
                    then <ns2:prestacion1>{fn:data($getLWResponse/ns1:A_PRESTACION_1)}</ns2:prestacion1>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_PRESTACION_2)
                    then <ns2:prestacion2>{fn:data($getLWResponse/ns1:A_PRESTACION_2)}</ns2:prestacion2>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_PRESTACION_3)
                    then <ns2:prestacion3>{fn:data($getLWResponse/ns1:A_PRESTACION_3)}</ns2:prestacion3>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_PRESTACION_4)
                    then <ns2:prestacion4>{fn:data($getLWResponse/ns1:A_PRESTACION_4)}</ns2:prestacion4>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_PRESTACION_5)
                    then <ns2:prestacion5>{fn:data($getLWResponse/ns1:A_PRESTACION_5)}</ns2:prestacion5>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_PROGRESSTASK)
                    then <ns2:progressTask>{fn:data($getLWResponse/ns1:A_PROGRESSTASK)}</ns2:progressTask>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_RADIO_PARAMETER)
                    then <ns2:radioParameter>{fn:data($getLWResponse/ns1:A_RADIO_PARAMETER)}</ns2:radioParameter>
                    else ()
                }
                <ns2:recordType>{fn:data($getLWResponse/ns1:recordType)}</ns2:recordType>
                {
                    if ($getLWResponse/ns1:A_REQUESTED_TYPE)
                    then <ns2:requestType>{fn:data($getLWResponse/ns1:A_REQUESTED_TYPE)}</ns2:requestType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_RESCHEDULE_REQUESTER)
                    then <ns2:rescheduleRequester>{fn:data($getLWResponse/ns1:A_RESCHEDULE_REQUESTER)}</ns2:rescheduleRequester>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_RESTART_TIME)
                    then <ns2:restartTime>{fn:data($getLWResponse/ns1:A_RESTART_TIME)}</ns2:restartTime>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SALES_CHANNEL_ID)
                    then <ns2:salesChannelID>{fn:data($getLWResponse/ns1:A_SALES_CHANNEL_ID)}</ns2:salesChannelID>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SALES_CHANNEL_TYPE)
                    then <ns2:salesChannelType>{fn:data($getLWResponse/ns1:A_SALES_CHANNEL_TYPE)}</ns2:salesChannelType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SAP_REPLACED_ID)
                    then <ns2:sapReplacedID>{fn:data($getLWResponse/ns1:A_SAP_REPLACED_ID)}</ns2:sapReplacedID>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SAP_REPLACED_NAME)
                    then <ns2:sapReplacedName>{fn:data($getLWResponse/ns1:A_SAP_REPLACED_NAME)}</ns2:sapReplacedName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SAP_REPLACED_QTY)
                    then <ns2:sapReplacedQTY>{fn:data($getLWResponse/ns1:A_SAP_REPLACED_QTY)}</ns2:sapReplacedQTY>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SAP_REPLACED_RESOURCE_NAME)
                    then <ns2:sapReplacedResourceName>{fn:data($getLWResponse/ns1:A_SAP_REPLACED_RESOURCE_NAME)}</ns2:sapReplacedResourceName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SCHEDULER_USERNAME)
                    then <ns2:scheduleUserName>{fn:data($getLWResponse/ns1:A_SCHEDULER_USERNAME)}</ns2:scheduleUserName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SEGMENT)
                    then <ns2:segment>{fn:data($getLWResponse/ns1:A_SEGMENT)}</ns2:segment>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SERVICE_DATA)
                    then <ns2:serviceData>{fn:data($getLWResponse/ns1:A_SERVICE_DATA)}</ns2:serviceData>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SERVICE_ID)
                    then <ns2:serviceID>{fn:data($getLWResponse/ns1:A_SERVICE_ID)}</ns2:serviceID>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SERVICE)
                    then <ns2:serviceName>{fn:data($getLWResponse/ns1:A_SERVICE)}</ns2:serviceName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SERVICE_TEMPLATE)
                    then <ns2:serviceTemplate>{fn:data($getLWResponse/ns1:A_SERVICE_TEMPLATE)}</ns2:serviceTemplate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SERVICE_TYPE)
                    then <ns2:serviceType>{fn:data($getLWResponse/ns1:A_SERVICE_TYPE)}</ns2:serviceType>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SERVICE_TYPE_SIAC)
                    then <ns2:serviceTypeSIAC>{fn:data($getLWResponse/ns1:A_SERVICE_TYPE_SIAC)}</ns2:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SERVICE_VALIDATED)
                    then <ns2:serviceValidate>{fn:data($getLWResponse/ns1:A_SERVICE_VALIDATED)}</ns2:serviceValidate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:serviceWindowEnd)
                    then <ns2:serviceWindowEnd>{fn:data($getLWResponse/ns1:serviceWindowEnd)}</ns2:serviceWindowEnd>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:serviceWindowStart)
                    then <ns2:serviceWindowStart>{fn:data($getLWResponse/ns1:serviceWindowStart)}</ns2:serviceWindowStart>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_PRIORITY)
                    then <ns2:severityProblem>{fn:data($getLWResponse/ns1:A_PRIORITY)}</ns2:severityProblem>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SIGNATURE_DATE)
                    then <ns2:signatureDate>{fn:data($getLWResponse/ns1:A_SIGNATURE_DATE)}</ns2:signatureDate>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SIGLA_ACTIVIDAD)
                    then <ns2:siglaActividad>{fn:data($getLWResponse/ns1:A_SIGLA_ACTIVIDAD)}</ns2:siglaActividad>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:slaWindowEnd)
                    then <ns2:slaWindowEnd>{fn:data($getLWResponse/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:slaWindowStart)
                    then <ns2:slaWindowStart>{fn:data($getLWResponse/ns1:slaWindowStart)}</ns2:slaWindowStart>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_SOURCE_SYSTEM)
                    then <ns2:sourceSystem>{fn:data($getLWResponse/ns1:A_SOURCE_SYSTEM)}</ns2:sourceSystem>
                    else ()
                }
                <ns2:standardDrivingTime>{fn:data($getLWResponse/ns1:travelTime)}</ns2:standardDrivingTime>
                <ns2:standardDuration>{fn:data($getLWResponse/ns1:duration)}</ns2:standardDuration>
                {
                    if ($getLWResponse/ns1:A_START_COORDINATES)
                    then <ns2:startCoordinates>{fn:data($getLWResponse/ns1:A_START_COORDINATES)}</ns2:startCoordinates>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_STARTEDTRAVEL)
                    then <ns2:startedTravel>{fn:data($getLWResponse/ns1:A_STARTEDTRAVEL)}</ns2:startedTravel>
                    else ()
                }
                <ns2:startTime>{fn:data($getLWResponse/ns1:startTime)}</ns2:startTime>
                <ns2:status>{fn:data($getLWResponse/ns1:status)}</ns2:status>
                {
                    if ($getLWResponse/ns1:A_TICKET_CHECKLIST)
                    then <ns2:ticketCheckList>{fn:data($getLWResponse/ns1:A_TICKET_CHECKLIST)}</ns2:ticketCheckList>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:timeOfAssignment)
                    then <ns2:timeOfAssignment>{fn:data($getLWResponse/ns1:timeOfAssignment)}</ns2:timeOfAssignment>
                    else ()
                }
                <ns2:timeOfBooking>{fn:data($getLWResponse/ns1:timeOfBooking)}</ns2:timeOfBooking>
                {
                    if ($getLWResponse/ns1:A_TROUBLE_ACTION)
                    then <ns2:troubleAction>{fn:data($getLWResponse/ns1:A_TROUBLE_ACTION)}</ns2:troubleAction>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_TROUBLE_DESCRIPTION)
                    then <ns2:troubleDescription>{fn:data($getLWResponse/ns1:A_TROUBLE_DESCRIPTION)}</ns2:troubleDescription>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_TROUBLE_DIAGNOSIS)
                    then <ns2:troubleDiagnosis>{fn:data($getLWResponse/ns1:A_TROUBLE_DIAGNOSIS)}</ns2:troubleDiagnosis>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_TROUBLETICKET_REASON)
                    then <ns2:troubleTicketReason>{fn:data($getLWResponse/ns1:A_TROUBLETICKET_REASON)}</ns2:troubleTicketReason>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_TROUBLETICKET_RESPONSABLE)
                    then <ns2:troubleTicketResponsable>{fn:data($getLWResponse/ns1:A_TROUBLETICKET_RESPONSABLE)}</ns2:troubleTicketResponsable>
                    else ()
                }
                 <ns2:type>{fn:data($getLWResponse/ns1:activityType)}</ns2:type>
                {
                    if ($getLWResponse/ns1:A_WAREHOUSE_NAME)
                    then <ns2:warehouseName>{fn:data($getLWResponse/ns1:A_WAREHOUSE_NAME)}</ns2:warehouseName>
                    else ()
                }
                {
                    if ($getLWResponse/ns1:A_ZONE_KM)
                    then <ns2:zoneKM>{fn:data($getLWResponse/ns1:A_ZONE_KM)}</ns2:zoneKM>
                    else ()
                }
                <ns2:linkedActivities>
                    <ns2:links>
                        <ns2:rel>{fn:data($getLWResponse/ns1:linkedActivities/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($getLWResponse/ns1:linkedActivities/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:linkedActivities>
                <ns2:linkList>
                    {
                        for $links in $getLWResponse/ns1:links
                        return 
                        <ns2:links>
                            <ns2:rel>{fn:data($links/ns1:rel)}</ns2:rel>
                            <ns2:href>{fn:data($links/ns1:href)}</ns2:href></ns2:links>
                    }
                </ns2:linkList>
                <ns2:RequiredInventories>
                    <ns2:links>
                        <ns2:rel>{fn:data($getLWResponse/ns1:requiredInventories/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($getLWResponse/ns1:requiredInventories/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:RequiredInventories>
                <ns2:ResourcePreferences>
                    <ns2:links>
                        <ns2:rel>{fn:data($getLWResponse/ns1:resourcePreferences/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($getLWResponse/ns1:resourcePreferences/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:ResourcePreferences>
                <ns2:workSkill>
                    <ns2:links>
                        <ns2:rel>{fn:data($getLWResponse/ns1:workSkills/ns1:links/ns1:rel)}</ns2:rel>
                        <ns2:href>{fn:data($getLWResponse/ns1:workSkills/ns1:links/ns1:href)}</ns2:href>
                    </ns2:links>
                </ns2:workSkill>
                <ns2:WorkSchedule>
                    {
                        if ($getLWResponse/ns1:timeSlot)
                        then <ns2:applicableDuring>{fn:data($getLWResponse/ns1:timeSlot)}</ns2:applicableDuring>
                        else ()
                    }
                </ns2:WorkSchedule>
                <ns2:absoluteLocalLocation>
                    <ns2:X>{fn:data($getLWResponse/ns1:latitude)}</ns2:X>
                    {
                        if ($getLWResponse/ns1:longitude)
                        then <ns2:Y>{fn:data($getLWResponse/ns1:longitude)}</ns2:Y>
                        else ()
                    }
                    <ns2:timezone>
                        <ns2:name>{fn:data($getLWResponse/ns1:timeZone)}</ns2:name>
                        <ns2:timeZoneIANA>{fn:data($getLWResponse/ns1:timeZoneIANA)}</ns2:timeZoneIANA>
                    </ns2:timezone>
                </ns2:absoluteLocalLocation>
                <ns2:address>
                    {
                        if ($getLWResponse/ns1:A_COMUNA)
                        then <ns2:commune>{fn:data($getLWResponse/ns1:A_COMUNA)}</ns2:commune>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:city)
                        then <ns2:city>{fn:data($getLWResponse/ns1:city)}</ns2:city>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:stateProvince)
                        then <ns2:region>{fn:data($getLWResponse/ns1:stateProvince)}</ns2:region>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:streetAddress)
                        then <ns2:streetName>{fn:data($getLWResponse/ns1:streetAddress)}</ns2:streetName>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_ADDRESS_REFERENCE)
                        then <ns2:addressReference>{fn:data($getLWResponse/ns1:A_ADDRESS_REFERENCE)}</ns2:addressReference>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_BUILDING_TYPE)
                        then <ns2:buildingType>{fn:data($getLWResponse/ns1:A_BUILDING_TYPE)}</ns2:buildingType>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_COMPLENTARY_ADDRESS)
                        then <ns2:complentaryAddress>{fn:data($getLWResponse/ns1:A_COMPLENTARY_ADDRESS)}</ns2:complentaryAddress>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_DEPARTAMENT)
                        then <ns2:department>{fn:data($getLWResponse/ns1:A_DEPARTAMENT)}</ns2:department>
                        else ()
                    }
                </ns2:address>              
                <ns2:customerAccount>
                    {
                        if ($getLWResponse/ns1:A_ALIAS)
                        then <ns2:alias>{fn:data($getLWResponse/ns1:A_ALIAS)}</ns2:alias>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_GROUP)
                        then <ns2:customerGroup>{fn:data($getLWResponse/ns1:A_GROUP)}</ns2:customerGroup>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_PORTABILITY)
                        then <ns2:portabilityIndicator>{fn:data($getLWResponse/ns1:A_PORTABILITY)}</ns2:portabilityIndicator>
                        else ()
                    }
                    <ns2:contact>
                        <ns2:alternativePhoneNumber>
                            <ns2:number>{fn:data($getLWResponse/ns1:customerPhone)}</ns2:number>
                        </ns2:alternativePhoneNumber>
                        <ns2:cellPhone>
                            <ns2:number>{fn:data($getLWResponse/ns1:customerCell)}</ns2:number>
                        </ns2:cellPhone>
                        <ns2:email>
                            <ns2:eMailAddress>{fn:data($getLWResponse/ns1:customerEmail)}</ns2:eMailAddress>
                        </ns2:email>
                        {
                        if(fn:data($getLWResponse/ns1:customerNumber))
                        then
                                <ns2:IndividualIdentification>
                                    <ns2:number>{fn:data($getLWResponse/ns1:customerNumber)}</ns2:number>
                                    <ns2:type></ns2:type>
                                </ns2:IndividualIdentification>
                        else ()
                          
                        }
                        <ns2:IndividualName>
                            <ns2:formatedName>{fn:data($getLWResponse/ns1:customerName)}</ns2:formatedName>
                        </ns2:IndividualName>
                    </ns2:contact>
                </ns2:customerAccount>
                <ns2:Language>
                    <ns2:alphabetName>{fn:data($getLWResponse/ns1:language)}</ns2:alphabetName>
                </ns2:Language>
                <ns2:PartyResource>
                    <ns2:ID>{fn:data($getLWResponse/ns1:resourceId)}</ns2:ID>
                    <ns2:internalID>{fn:data($getLWResponse/ns1:resourceInternalId)}</ns2:internalID>
                    <ns2:resourceTimeZone>{fn:data($getLWResponse/ns1:resourceTimeZone)}</ns2:resourceTimeZone>
                    <ns2:UTCOffset>{fn:data($getLWResponse/ns1:resourceTimeZoneDiff)}</ns2:UTCOffset>
                    <ns2:resourceTimeZoneIANA>{fn:data($getLWResponse/ns1:resourceTimeZoneIANA)}</ns2:resourceTimeZoneIANA>
                    {
                      if(fn:data($getLWResponse/ns1:A_INSTALLER_NUMBER))
                      then
                        <ns2:individualIdentification>
                            <ns2:number>{fn:data($getLWResponse/ns1:A_INSTALLER_NUMBER)}</ns2:number>
                            <ns2:type></ns2:type>
                        </ns2:individualIdentification>
                      else ()
                      
                    }
                </ns2:PartyResource>
                <ns2:Product>
                    {
                        if ($getLWResponse/ns1:A_ANIS_EQUIP_QTY)
                        then <ns2:ANISQuantity>{fn:data($getLWResponse/ns1:A_ANIS_EQUIP_QTY)}</ns2:ANISQuantity>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_ANIS_EQUIP_QTY_REQ)
                        then <ns2:ANISQuantityRequired>{fn:data($getLWResponse/ns1:A_ANIS_EQUIP_QTY_REQ)}</ns2:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_CATEGORY)
                        then <ns2:category>{fn:data($getLWResponse/ns1:A_CATEGORY)}</ns2:category>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_DECO_QTY)
                        then <ns2:DECOQuantity>{fn:data($getLWResponse/ns1:A_DECO_QTY)}</ns2:DECOQuantity>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_EQUIP_QTY_REQ)
                        then <ns2:DECOQuantityRequired>{fn:data($getLWResponse/ns1:A_EQUIP_QTY_REQ)}</ns2:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_EQUIPMENTS)
                        then <ns2:equipment>{fn:data($getLWResponse/ns1:A_EQUIPMENTS)}</ns2:equipment>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_LAYER_MPLS)
                        then <ns2:layerMPLS>{fn:data($getLWResponse/ns1:A_LAYER_MPLS)}</ns2:layerMPLS>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_NGN_MODEL)
                        then <ns2:ngnModel>{fn:data($getLWResponse/ns1:A_NGN_MODEL)}</ns2:ngnModel>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_PHONE_EQUIP_QTY)
                        then <ns2:PHONEQuantity>{fn:data($getLWResponse/ns1:A_PHONE_EQUIP_QTY)}</ns2:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_PRODUCT_CODE)
                        then <ns2:externalProductCode>{fn:data($getLWResponse/ns1:A_PRODUCT_CODE)}</ns2:externalProductCode>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_PRODUCT_DESC)
                        then <ns2:externalProductDescription>{fn:data($getLWResponse/ns1:A_PRODUCT_DESC)}</ns2:externalProductDescription>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_STB_EQUIP_QTY)
                        then <ns2:STBQuantity>{fn:data($getLWResponse/ns1:A_STB_EQUIP_QTY)}</ns2:STBQuantity>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_VOICE_IND)
                        then <ns2:voiceIndicator>{fn:data($getLWResponse/ns1:A_VOICE_IND)}</ns2:voiceIndicator>
                        else ()
                    }
                    {
                        if ($getLWResponse/ns1:A_WIFI_EQUIP_QTY)
                        then <ns2:WIFIExtensorQuantity>{fn:data($getLWResponse/ns1:A_WIFI_EQUIP_QTY)}</ns2:WIFIExtensorQuantity>
                        else ()
                    }
                </ns2:Product>               
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:GetActivity_RSP>
};

local:func($getLWResponse, $result)