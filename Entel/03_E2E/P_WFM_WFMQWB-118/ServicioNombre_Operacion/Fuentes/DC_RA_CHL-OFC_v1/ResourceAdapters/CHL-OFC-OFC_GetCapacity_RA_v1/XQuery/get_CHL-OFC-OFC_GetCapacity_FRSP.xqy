xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/GetCapacity/Get/v1";
(:: import schema at "../CSC/CHL-OFC-OFC_GetCapacity_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Result as element() (:: schema-element(ns1:Result) ::) external;

declare function local:func($Result as element() (:: schema-element(ns1:Result) ::)) as element() (:: schema-element(ns2:GetCapacity_FRSP) ::) {
    <ns2:GetCapacity_FRSP>
        {$Result}
    </ns2:GetCapacity_FRSP>
};

local:func($Result)