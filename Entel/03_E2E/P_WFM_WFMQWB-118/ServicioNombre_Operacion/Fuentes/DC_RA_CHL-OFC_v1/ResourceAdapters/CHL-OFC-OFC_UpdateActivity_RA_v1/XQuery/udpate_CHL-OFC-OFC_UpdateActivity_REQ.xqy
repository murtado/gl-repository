xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/CHL-OFC-OFC_UpdateActivity_JSON_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/UpdateActivity/Update/v1";
(:: import schema at "../CSC/CHL-OFC-OFC_UpdateActivity_v1_EBM.xsd" ::)

declare variable $updateActivityRq as element() (:: schema-element(ns1:UpdateActivity_REQ) ::) external;

declare function local:func($updateActivityRq as element() (:: schema-element(ns1:UpdateActivity_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:A_APPOINTMENTTIMES>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:A_APPOINTMENTTIMES>
        <ns2:A_CALCULATION_DATE>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:calculationDate)}</ns2:A_CALCULATION_DATE>
        <ns2:A_CANCELATION_REASON>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:cancelationReason)}</ns2:A_CANCELATION_REASON>
        <ns2:A_ENGINEER_NOTES>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:A_ENGINEER_NOTES>
        <ns2:A_ESTIMATED_END_DATE>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:A_ESTIMATED_END_DATE>
        <ns2:apptNumber>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:apptNumber>
        <ns2:A_NOTES>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:A_NOTES>
        <ns2:A_FORCED_APPOINTMENT>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:forcedAppointment)}</ns2:A_FORCED_APPOINTMENT>
        <ns2:A_FUTURE_CONTACT_DATE>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:futureContactDate)}</ns2:A_FUTURE_CONTACT_DATE>
        <ns2:activityId>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:activityId>
        <ns2:A_NETWORK_RESOURCES>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:A_NETWORK_RESOURCES>
        <ns2:A_PROGRESSTASK>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns2:A_PROGRESSTASK>
        <ns2:A_SCHEDULER_USERNAME>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:A_SCHEDULER_USERNAME>
        <ns2:A_SIGLA_ACTIVIDAD>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:siglaActividad)}</ns2:A_SIGLA_ACTIVIDAD>
        <ns2:A_ACCESS_TECHNOLOGY>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:accessTech)}</ns2:A_ACCESS_TECHNOLOGY>
        <ns2:A_ACCESS_TECH_QTY>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns2:A_ACCESS_TECH_QTY>
        <ns2:longitude>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:longitude>
        <ns2:latitude>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:latitude>
        <ns2:timeSlot>{fn:data($updateActivityRq/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:timeSlot>
    </ns2:Root-Element>
};

local:func($updateActivityRq)