xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CancelWorkOrderSchedule/Cancel/v1";
(:: import schema at "../../../../../ESC/Primary/CancelWorkOrderSchedule_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/CancelWorkOrder_AGE/Cancel/v1";
(:: import schema at "../../../../../../ES_CancelWorkOrder_AGE_v1/ESC/Primary/CancelWorkOrder_AGE_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $cancelWorkOrderAgeRq as element() (:: schema-element(ns1:CancelWorkOrderSchedule_REQ) ::) external;

declare function local:func($cancelWorkOrderAgeRq as element() (:: schema-element(ns1:CancelWorkOrderSchedule_REQ) ::)) as element() (:: schema-element(ns2:CancelWorkOrder_REQ) ::) {
    <ns2:CancelWorkOrder_REQ>
        <ns3:RequestHeader>
            
            <ns3:Consumer sysCode="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}"
                countryCode="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Consumer/@countryCode)}">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" reqTimestamp="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}" rspTimestamp="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}" processID="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@processID)}" eventID="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@eventID)}" sourceID="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@sourceID)}" correlationEventID="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)}" conversationID="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@conversationID)}" correlationID="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/@correlationID)}">
                <ns3:Service code="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}" name="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}" operation="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}">
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel name="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Channel/@name)}" mode="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns3:Channel/@mode)}">
            </ns3:Channel>
            <ns4:Result status="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/@status)}" description="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/@description)}">
                <ns5:CanonicalError code="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}" description="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}" type="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}">
                </ns5:CanonicalError>
                <ns5:SourceError code="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}" description="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}">
                    <ns5:ErrorSourceDetails source="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                    </ns5:ErrorSourceDetails>
                    {
                        if ($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                        then <ns5:SourceFault>{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                        else ()
                    }
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError code="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@code)}" description="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@description)}">
                        <ns5:ErrorSourceDetails source="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                        </ns5:ErrorSourceDetails>
                        {
                            if ($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)
                            then <ns5:SourceFault>{fn:data($cancelWorkOrderAgeRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                            else ()
                        }
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($cancelWorkOrderAgeRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CancelWorkOrder_REQ>
};

local:func($cancelWorkOrderAgeRq)