xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/MoveActivity/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v1/ResourceAdapters/CHL-OFC-OFC_MoveActivity_RA_v1/CSC/CHL-OFC-OFC_MoveActivity_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/MoveWorkOrder/Update/v1";
(:: import schema at "../../../../../ESC/Primary/MoveWorkOrder_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $moveWorkOrderOhRq as element() (:: schema-element(ns1:MoveWorkOrder_REQ) ::) external;

declare function local:func($moveWorkOrderOhRq as element() (:: schema-element(ns1:MoveWorkOrder_REQ) ::)) as element() (:: schema-element(ns2:MoveActivity_REQ) ::) {
    <ns2:MoveActivity_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Consumer/@countryCode)}"></ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            {
                if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result)
                then 
                    <ns4:Result status="{fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/@status)}">
                        {
                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/@description)
                            then attribute description {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/@description)}
                            else ()
                        }
                        {
                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError)
                            then 
                                <ns5:CanonicalError>
                                    {
                                        if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)
                                        then attribute code {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)
                                        then attribute description {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)
                                        then attribute type {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns5:CanonicalError>
                            else ()
                        }
                        {
                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError)
                            then 
                                <ns5:SourceError>
                                    {
                                        if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)
                                        then attribute code {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)
                                        then attribute description {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            else ()
                        }
                        {
                            if ($moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors)
                            then 
                                <ns4:CorrelativeErrors>
                                    {
                                        for $SourceError in $moveWorkOrderOhRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                        return 
                                        <ns5:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns5:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns5:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns5:SourceFault)
                                                then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                                else ()
                                            }
                                        </ns5:SourceError>
                                    }
                                </ns4:CorrelativeErrors>
                            else ()
                        }
                    </ns4:Result>
                else ()
            }
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($moveWorkOrderOhRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                <ns2:SetDate>
                    <ns2:appointmentDate>{fn:data($moveWorkOrderOhRq/ns1:Body/ns1:WorkOrder/ns1:SetDate/ns1:appointmentDate)}</ns2:appointmentDate>
                </ns2:SetDate>
                <ns2:SetResource>
                    <ns2:ID>{fn:data($moveWorkOrderOhRq/ns1:Body/ns1:WorkOrder/ns1:SetResource/ns1:ID)}</ns2:ID>
                </ns2:SetResource>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:MoveActivity_REQ>
};

local:func($moveWorkOrderOhRq)