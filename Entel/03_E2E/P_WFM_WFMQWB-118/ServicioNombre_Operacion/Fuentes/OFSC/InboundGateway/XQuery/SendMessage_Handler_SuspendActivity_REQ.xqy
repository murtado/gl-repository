xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../WSDL/SendMessage_Handler_SuspendActivity.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/SuspendActivity/Send/v1";
(:: import schema at "../../../ES_SendMessage_SuspendActivity_v1/ESC/Primary/SuspendActivity_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $request as element() (:: schema-element(envelope) ::) external;
declare variable $message_id as xs:string external;
declare variable $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::) external;

declare function local:func($RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::),$request as element() (:: schema-element(envelope) ::), 
                  $message_id as xs:string) as element() (:: schema-element(ns1:SuspendActivity_REQ) ::) {
    <ns1:SuspendActivity_REQ>
        {$RequestHeader}
        
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:messageID>{fn:data($message_id)}</ns1:messageID>
                <ns1:ID>{fn:data($request/activityId)}</ns1:ID>
                <ns1:externalID>{fn:data($request/apptNumber)}</ns1:externalID>
                <ns1:engineerNotes>{fn:data($request/A_ENGINEER_NOTES)}</ns1:engineerNotes>
                <ns1:PartyResource>
                    <ns1:pname>{fn:data($request/pname)}</ns1:pname>
                </ns1:PartyResource>
                <ns1:status>{fn:data($request/status)}</ns1:status>
                <ns1:contractor>{fn:data($request/A_CONTRACTOR)}</ns1:contractor>
                <ns1:ticketBO>{fn:data($request/A_TICKET_BO)}</ns1:ticketBO>
                <ns1:closeReason>{fn:data($request/A_CLOSING_REASON)}</ns1:closeReason>
                <ns1:restartTime>{fn:data($request/A_RESTART_TIME)}</ns1:restartTime>
                <ns1:actionSD>{fn:data($request/A_ACTION)}</ns1:actionSD>
                <ns1:statusReason>{fn:data($request/A_STATUS_REASON)}</ns1:statusReason>
                <ns1:requestType>{fn:data($request/Request_Type)}</ns1:requestType>
                <ns1:details>{fn:data($request/A_DETAILS)}</ns1:details>
                <ns1:userNotification>{fn:data($request/sr_uid)}</ns1:userNotification>
                <ns1:datenotification>{fn:data($request/srdate)}</ns1:datenotification>
                <ns1:SAPReplacedID>{fn:data($request/A_SAP_REPLACED_ID)}</ns1:SAPReplacedID>
                <ns1:SAPReplacedName>{fn:data($request/A_SAP_REPLACED_NAME)}</ns1:SAPReplacedName>
                <ns1:SAPReplacedQTY>{fn:data($request/A_SAP_REPLACED_QTY)}</ns1:SAPReplacedQTY>
                <ns1:SAPReplacedResource>{fn:data($request/A_SAP_REPLACED_RESOURCE_NAME)}</ns1:SAPReplacedResource>
                <ns1:sourceSystem>{fn:data($request/A_SOURCE_SYSTEM)}</ns1:sourceSystem>
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:SuspendActivity_REQ>
};

local:func($RequestHeader, $request, $message_id)