xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../WSDL/SendMessage_Handler_StartActivity.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/StartActivity/Send/v1";
(:: import schema at "../../../ES_SendMessage_StartActivity_v1/ESC/Primary/SendMessage_StartActivity_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)


declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $cdataRq as element() (:: schema-element(envelope) ::) external;
declare variable $message_id as xs:string external;
declare variable $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::) external;


declare function local:func($RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::),$cdataRq as element() (:: schema-element(envelope) ::), 
                            $message_id as xs:string) as element() (:: schema-element(ns1:StartActivity_REQ) ::) {

    <ns1:StartActivity_REQ>
      {$RequestHeader}
           
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:messageID>{fn:data($message_id)}</ns1:messageID>
                <ns1:ID>{fn:data($cdataRq/activityId)}</ns1:ID>
                <ns1:externalID>{fn:data($cdataRq/apptNumber)}</ns1:externalID>
                <ns1:engineerNotes>{fn:data($cdataRq/A_ENGINEER_NOTES)}</ns1:engineerNotes>
                <ns1:customerHostCell>{fn:data($cdataRq/A_CUSTOMER_HOST_CELL)}</ns1:customerHostCell>
                <ns1:customerHostName>{fn:data($cdataRq/A_CUSTOMER_HOST_NAME)}</ns1:customerHostName>
                <ns1:BOUserName>{fn:data($cdataRq/A_BO_USERNAME)}</ns1:BOUserName>
                <ns1:subject>{fn:data($cdataRq/Subject)}</ns1:subject>
                <ns1:userNotification>{fn:data($cdataRq/sr_uid)}</ns1:userNotification>
                <ns1:dateNotification>{fn:data($cdataRq/srdate)}</ns1:dateNotification>
                <ns1:PartyResource>
                    <ns1:pname>{fn:data($cdataRq/pname)}</ns1:pname>
                    <ns1:IndividualIdentification>
                        <ns1:number>{fn:data($cdataRq/A_INSTALLER_NUMBER)}</ns1:number>
                    </ns1:IndividualIdentification>
                </ns1:PartyResource>
                <ns1:sourceSystem>{fn:data($cdataRq/A_SOURCE_SYSTEM)}</ns1:sourceSystem>
                <ns1:startCoordinates>{fn:data($cdataRq/A_START_COORDINATES)}</ns1:startCoordinates>
                <ns1:status>{fn:data($cdataRq/status)}</ns1:status>
                <ns1:details>{fn:data($cdataRq/A_DETAILS)}</ns1:details>
                <ns1:CustomerAccount>
                    <ns1:customerGroup>{fn:data($cdataRq/A_GROUP)}</ns1:customerGroup>
                </ns1:CustomerAccount>
                <ns1:actionSD>{fn:data($cdataRq/A_ACTION)}</ns1:actionSD>
                <ns1:requestType>{fn:data($cdataRq/Request_Type)}</ns1:requestType>
                <ns1:statusReason>{fn:data($cdataRq/A_STATUS_REASON)}</ns1:statusReason>
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:StartActivity_REQ>
};

local:func($RequestHeader,$cdataRq, $message_id)