xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../WSDL/SendMessage_Handler_NoteDone.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/NotDone/Send/v1";
(:: import schema at "../../../ES_SendMessage_NotDone_v1/ESC/Primary/NotDone_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $cdata as element() (:: schema-element(envelope) ::) external;
declare variable $message_id as xs:string external;
declare variable $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::) external;

declare function local:func($RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::),$cdata as element() (:: schema-element(envelope) ::),
                            $message_id as xs:string) 
                            as element() (:: schema-element(ns1:NotDone_REQ) ::) {
    <ns1:NotDone_REQ>
        {$RequestHeader}
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:messageID>{data($message_id)}</ns1:messageID>
                <ns1:ID>{fn:data($cdata/activityId)}</ns1:ID>
                <ns1:PartyResource>
                    <ns1:pname>{fn:data($cdata/pname)}</ns1:pname>
                    <ns1:IndividualIdentification>
                        <ns1:number></ns1:number>
                    </ns1:IndividualIdentification>
                </ns1:PartyResource>
                <ns1:number>{fn:data($cdata/A_INSTALLER_NUMBER)}</ns1:number>
                <ns1:externalID>{fn:data($cdata/apptNumber)}</ns1:externalID>
                <ns1:externalNotes>{fn:data($cdata/A_NOTES)}</ns1:externalNotes>
                <ns1:customerHostCell>{fn:data($cdata/A_CUSTOMER_HOST_CELL)}</ns1:customerHostCell>
                <ns1:customerHostName>{fn:data($cdata/A_CUSTOMER_HOST_NAME)}</ns1:customerHostName>
                <ns1:engineerNotes>{fn:data($cdata/A_ENGINEER_NOTES)}</ns1:engineerNotes>
                <ns1:progressTask>{fn:data($cdata/A_PROGRESSTASK)}</ns1:progressTask>
                <ns1:OFTcloseType>{fn:data($cdata/A_OFT_CLOSE_TYPE)}</ns1:OFTcloseType>
                <ns1:OFTcloseReason>{fn:data($cdata/A_OFT_CLOSE_REASON)}</ns1:OFTcloseReason>
                <ns1:accessTechQTY>{fn:data($cdata/A_ACCESS_TECH_QTY)}</ns1:accessTechQTY>
                <ns1:EPC>{fn:data($cdata/A_EPC)}</ns1:EPC>
                <ns1:checkList>{fn:data($cdata/A_CHECKLIST)}</ns1:checkList>
                <ns1:ticketCheckList>{fn:data($cdata/A_TICKET_CHECKLIST)}</ns1:ticketCheckList>
                <ns1:zoneKM>{fn:data($cdata/A_ZONE_KM)}</ns1:zoneKM>
                <ns1:status>{fn:data($cdata/status)}</ns1:status>
                <ns1:prestacion1>{fn:data($cdata/A_PRESTACION_1)}</ns1:prestacion1>
                <ns1:prestacion2>{fn:data($cdata/A_PRESTACION_2)}</ns1:prestacion2>
                <ns1:prestacion3>{fn:data($cdata/A_PRESTACION_3)}</ns1:prestacion3>
                <ns1:prestacion4>{fn:data($cdata/A_PRESTACION_4)}</ns1:prestacion4>
                <ns1:prestacion5>{fn:data($cdata/A_PRESTACION_5)}</ns1:prestacion5>
                <ns1:prestacionQTY1>{fn:data($cdata/A_Q_PRESTACION_1)}</ns1:prestacionQTY1>
                <ns1:prestacionQTY2>{fn:data($cdata/A_Q_PRESTACION_2)}</ns1:prestacionQTY2>
                <ns1:prestacionQTY3>{fn:data($cdata/A_Q_PRESTACION_3)}</ns1:prestacionQTY3>
                <ns1:prestacionQTY4>{fn:data($cdata/A_Q_PRESTACION_4)}</ns1:prestacionQTY4>
                <ns1:prestacionQTY5>{fn:data($cdata/A_Q_PRESTACION_5)}</ns1:prestacionQTY5>
                <ns1:PLCPrestation1>{fn:data($cdata/A_PLC_PRESTACION1)}</ns1:PLCPrestation1>
                <ns1:deliveryPlace>{fn:data($cdata/A_DELIVERY_PLACE)}</ns1:deliveryPlace>
                <ns1:contractor>{fn:data($cdata/A_CONTRACTOR)}</ns1:contractor>
                <ns1:BOUserName>{fn:data($cdata/A_BO_USERNAME)}</ns1:BOUserName>
                <ns1:ticketBO>{fn:data($cdata/A_TICKET_BO)}</ns1:ticketBO>
                <ns1:troubleTicketReason>{fn:data($cdata/A_TROUBLETICKET_REASON)}</ns1:troubleTicketReason>
                <ns1:actionSD>{fn:data($cdata/A_ACTION)}</ns1:actionSD>
                <ns1:closingReasonSD>{fn:data($cdata/A_CLOSING_REASON_SD)}</ns1:closingReasonSD>
                <ns1:troubleTicketReasonTier1>{fn:data($cdata/A_TROUBLETICKET_REASON_TIER1)}</ns1:troubleTicketReasonTier1>
                <ns1:troubleTicketReasonTier2>{fn:data($cdata/A_TROUBLETICKET_REASON_TIER2)}</ns1:troubleTicketReasonTier2>
                <ns1:troubleTicketReasonTier3>{fn:data($cdata/A_TROUBLETICKET_REASON_TIER3)}</ns1:troubleTicketReasonTier3>
                <ns1:responsability>{fn:data($cdata/A_RESPONSABILITY)}</ns1:responsability>
                <ns1:closingMethodResolution>{fn:data($cdata/A_CLOSING_METHOD_RESOLUTION)}</ns1:closingMethodResolution>
                <ns1:originClosing>{fn:data($cdata/A_ORIGIN_CLOSING)}</ns1:originClosing>
                <ns1:subject>{fn:data($cdata/Subject)}</ns1:subject>
                <ns1:userNotification>{fn:data($cdata/sr_uid)}</ns1:userNotification>
                <ns1:dateNotification>{fn:data($cdata/srdate)}</ns1:dateNotification>
                <ns1:sourceSystem>{fn:data($cdata/A_SOURCE_SYSTEM)}</ns1:sourceSystem>
                <ns1:requestType>{fn:data($cdata/Request_Type)}</ns1:requestType>
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:NotDone_REQ>
};

local:func($RequestHeader,$cdata, $message_id)