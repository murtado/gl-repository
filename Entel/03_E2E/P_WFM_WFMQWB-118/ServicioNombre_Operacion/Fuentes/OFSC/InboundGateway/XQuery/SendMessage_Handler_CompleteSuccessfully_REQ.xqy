xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../WSDL/SendMessage_Handler_CompleteSuccesfully.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CompleteSuccessfully/Send/v1";
(:: import schema at "../../../ES_SendMessage_CompleteSucsessfully_v1/ESC/Primary/EBM/CompleteSuccessfully_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $request as element() (:: schema-element(envelope) ::) external;
declare variable $message_id as xs:string external;
declare variable $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::) external;

declare function local:func($RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::),$request as element() (:: schema-element(envelope) ::), 
                            $message_id as xs:string)
                            as element() (:: schema-element(ns1:CompleteSuccessfully_REQ) ::) {
    <ns1:CompleteSuccessfully_REQ>
        {$RequestHeader}
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:messageID>{fn:data($message_id)}</ns1:messageID>
                <ns1:accessTechQTY>{fn:data($request/A_ACCESS_TECH_QTY)}</ns1:accessTechQTY>
                <ns1:actionSD>{fn:data($request/A_ACTION)}</ns1:actionSD>
                <ns1:BOUserName>{fn:data($request/A_BO_USERNAME)}</ns1:BOUserName>
                <ns1:checkList>{fn:data($request/A_CHECKLIST)}</ns1:checkList>
                <ns1:closingMethodResolution>{fn:data($request/A_CLOSING_METHOD_RESOLUTION)}</ns1:closingMethodResolution>
                <ns1:closingReasonSD>{fn:data($request/A_CLOSING_REASON_SD)}</ns1:closingReasonSD>
                <ns1:contractor>{fn:data($request/A_CONTRACTOR)}</ns1:contractor>
                <ns1:customerHostCell>{fn:data($request/A_CUSTOMER_HOST_CELL)}</ns1:customerHostCell>
                <ns1:customerHostName>{fn:data($request/A_CUSTOMER_HOST_NAME)}</ns1:customerHostName>
                <ns1:dateNotification>{fn:data($request/srdate)}</ns1:dateNotification>
                <ns1:deliveryPlace>{fn:data($request/A_DELIVERY_PLACE)}</ns1:deliveryPlace>
                <ns1:efficiencyCumplied>{fn:data($request/A_EFFICIENCY_CUMPLIED)}</ns1:efficiencyCumplied>
                <ns1:efficiencyCumpliedReason>{fn:data($request/A_EFFICIENCY_CUMPLIED_REASON)}</ns1:efficiencyCumpliedReason>
                <ns1:engineerNotes>{fn:data($request/A_ENGINEER_NOTES)}</ns1:engineerNotes>
                <ns1:EPC>{fn:data($request/A_EPC)}</ns1:EPC>
                <ns1:externalID>{fn:data($request/apptNumber)}</ns1:externalID>
                <ns1:externalNotes>{fn:data($request/A_NOTES)}</ns1:externalNotes>
                <ns1:fume>{fn:data($request/A_FUME)}</ns1:fume>
                <ns1:ID>{fn:data($request/activityId)}</ns1:ID>
                <ns1:networkType>{fn:data($request/A_NETWORK_TYPE)}</ns1:networkType>
                <ns1:OFTcloseReason>{fn:data($request/A_OFT_CLOSE_REASON)}</ns1:OFTcloseReason>
                <ns1:OFTcloseType>{fn:data($request/A_OFT_CLOSE_TYPE)}</ns1:OFTcloseType>
                <ns1:originClosing>{fn:data($request/A_ORIGIN_CLOSING)}</ns1:originClosing>
                <ns1:plcPrestation>{fn:data($request/A_PLC_PRESTATION)}</ns1:plcPrestation>
                <ns1:prestacion1>{fn:data($request/A_PRESTACION_1)}</ns1:prestacion1>
                <ns1:prestacion2>{fn:data($request/A_PRESTACION_2)}</ns1:prestacion2>
                <ns1:prestacion3>{fn:data($request/A_PRESTACION_3)}</ns1:prestacion3>
                <ns1:prestacion4>{fn:data($request/A_PRESTACION_4)}</ns1:prestacion4>
                <ns1:prestacion5>{fn:data($request/A_PRESTACION_5)}</ns1:prestacion5>
                <ns1:prestacionQTY1>{fn:data($request/A_Q_PRESTACION_1)}</ns1:prestacionQTY1>
                <ns1:prestacionQTY2>{fn:data($request/A_Q_PRESTACION_2)}</ns1:prestacionQTY2>
                <ns1:prestacionQTY3>{fn:data($request/A_Q_PRESTACION_3)}</ns1:prestacionQTY3>
                <ns1:prestacionQTY4>{fn:data($request/A_Q_PRESTACION_4)}</ns1:prestacionQTY4>
                <ns1:prestacionQTY5>{fn:data($request/A_Q_PRESTACION_5)}</ns1:prestacionQTY5>
                <ns1:progressTask>{fn:data($request/A_PROGRESSTASK)}</ns1:progressTask>
                <ns1:responsability>{fn:data($request/A_RESPONSABILITY)}</ns1:responsability>
                <ns1:sourceSystem>{fn:data($request/A_SOURCE_SYSTEM)}</ns1:sourceSystem>
                <ns1:status>{fn:data($request/status)}</ns1:status>
                <ns1:subject></ns1:subject>
                <ns1:ticketCheckList>{fn:data($request/A_TICKET_CHECKLIST)}</ns1:ticketCheckList>
                <ns1:troubleTicketReason>{fn:data($request/A_TROUBLETICKET_REASON)}</ns1:troubleTicketReason>
                <ns1:troubleTicketReasonTier1>{fn:data($request/A_TROUBLETICKET_REASON_TIER1)}</ns1:troubleTicketReasonTier1>
                <ns1:troubleTicketReasonTier2>{fn:data($request/A_TROUBLETICKET_REASON_TIER2)}</ns1:troubleTicketReasonTier2>
                <ns1:troubleTicketReasonTier3>{fn:data($request/A_TROUBLETICKET_REASON_TIER3)}</ns1:troubleTicketReasonTier3>
                <ns1:userNotification>{fn:data($request/sr_uid)}</ns1:userNotification>
                <ns1:zoneKM>{fn:data($request/A_ZONE_KM)}</ns1:zoneKM>
                <ns1:PartyResource>
                    <ns1:pname>{fn:data($request/pname)}</ns1:pname>
                    <ns1:individualIdentification>
                        <ns1:number>{fn:data($request/A_INSTALLER_NUMBER)}</ns1:number>
                        <ns1:type></ns1:type>
                    </ns1:individualIdentification>
                    <ns1:individualName>
                        <ns1:name>{fn:data($request/A_INSTALLER_NAME)}</ns1:name>
                    </ns1:individualName>
                </ns1:PartyResource>
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:CompleteSuccessfully_REQ>
};

local:func($RequestHeader, $request, $message_id)