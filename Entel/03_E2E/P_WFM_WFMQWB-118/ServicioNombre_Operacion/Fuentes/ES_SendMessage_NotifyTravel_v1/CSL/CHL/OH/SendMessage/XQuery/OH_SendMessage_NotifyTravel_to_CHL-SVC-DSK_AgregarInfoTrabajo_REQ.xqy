xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/ENTEL_WS_AgregarInfoTrabajo/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SVC-DSK_v1/ResourceAdapters/CHL-SVC-DSK-SVC-DSK_AgregarInfoTrabajo_RA_v1/CSC/CHL-SVC-DSK-SVC-DSK_ENTEL_WS_AgregarInfoTrabajo_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/NotifyTravel/Send/v1";
(:: import schema at "../../../../../ESC/Primary/EBM/NotifyTravel_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Request as element() (:: schema-element(ns1:NotifyTravel_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:NotifyTravel_REQ) ::)) as element() (:: schema-element(ns2:ENTEL_WS_AgregarInfoTrabajo_REQ) ::) {
    <ns2:ENTEL_WS_AgregarInfoTrabajo_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@countryCode)}"> </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($Request/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($Request/ns3:RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($Request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($Request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($Request/ns3:RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($Request/ns3:RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($Request/ns3:RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($Request/ns3:RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            {
                if ($Request/ns3:RequestHeader/ns4:Result)
                then 
                    <ns4:Result status="{fn:data($Request/ns3:RequestHeader/ns4:Result/@status)}">
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/@description)
                            then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/@description)}
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError)
                            then 
                                <ns5:CanonicalError>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)
                                        then attribute code {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)
                                        then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)
                                        then attribute type {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns5:CanonicalError>
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError)
                            then 
                                <ns5:SourceError>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)
                                        then attribute code {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)
                                        then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors)
                            then 
                                <ns4:CorrelativeErrors>
                                    {
                                        for $SourceError in $Request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                        return 
                                        <ns5:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns5:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns5:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns5:SourceFault)
                                                then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                                else ()
                                            }
                                        </ns5:SourceError>
                                    }
                                </ns4:CorrelativeErrors>
                            else ()
                        }
                    </ns4:Result>
                else ()
            }
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:AgregaInfoTrabajo>
                <ns2:Accion>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns2:Accion>
                <ns2:Numero_Incidencia></ns2:Numero_Incidencia>
                <ns2:z1D_Details>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:details)}</ns2:z1D_Details>
                <ns2:z1D_Summary>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:z1D_Summary>
            </ns2:AgregaInfoTrabajo>
        </ns2:Body>
    </ns2:ENTEL_WS_AgregarInfoTrabajo_REQ>
};

local:func($Request)