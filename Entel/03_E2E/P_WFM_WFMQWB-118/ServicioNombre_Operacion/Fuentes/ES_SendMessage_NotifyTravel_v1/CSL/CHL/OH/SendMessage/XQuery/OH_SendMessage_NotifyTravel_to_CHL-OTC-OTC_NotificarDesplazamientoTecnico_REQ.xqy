xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/NotificarDesplazamientoTecnico/Send/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OTC_v1/ResourceAdapters/CHL-OTC-OTC_NotificarDesplazamientoTecnico_RA_v1/CSC/NotificarDesplazamientoTecnico_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/NotifyTravel/Send/v1";
(:: import schema at "../../../../../ESC/Primary/EBM/NotifyTravel_v1_EBM.xsd" ::)

declare variable $request as element() (:: schema-element(ns1:NotifyTravel_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:NotifyTravel_REQ) ::)) as element() (:: schema-element(ns2:NotificarDesplazamientoTecnico_REQ) ::) {
    <ns2:NotificarDesplazamientoTecnico_REQ>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource)
                    then <ns2:PartyResource>
                        <ns2:pname>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</ns2:pname>
                        {
                            if ($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification)
                            then <ns2:IndividualIdentification>
                                <ns2:name>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:name)}</ns2:name></ns2:IndividualIdentification>
                            else ()
                        }</ns2:PartyResource>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:userNotification)
                    then <ns2:userNotification>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:userNotification)}</ns2:userNotification>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)
                    then <ns2:dateNotification>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)}</ns2:dateNotification>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:startedTravel)
                    then <ns2:startedTravel>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:startedTravel)}</ns2:startedTravel>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:details)
                    then <ns2:details>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:details)}</ns2:details>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:actionSD)
                    then <ns2:actionSD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns2:actionSD>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns2:requestType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:requestType>
                    else ()
                }
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:NotificarDesplazamientoTecnico_REQ>
};

local:func($request)