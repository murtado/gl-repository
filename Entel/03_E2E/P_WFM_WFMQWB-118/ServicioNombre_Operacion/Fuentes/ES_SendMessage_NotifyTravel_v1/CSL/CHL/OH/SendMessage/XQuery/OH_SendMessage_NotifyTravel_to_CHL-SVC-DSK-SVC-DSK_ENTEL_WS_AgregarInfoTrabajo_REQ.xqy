xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/ENTEL_WS_AgregarInfoTrabajo/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SVC-DSK_v1/ResourceAdapters/CHL-SVC-DSK-SVC-DSK_AgregarInfoTrabajo_RA_v1/CSC/CHL-SVC-DSK-SVC-DSK_ENTEL_WS_AgregarInfoTrabajo_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/NotifyTravel/Send/v1";
(:: import schema at "../../../../../ESC/Primary/EBM/NotifyTravel_v1_EBM.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:NotifyTravel_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:NotifyTravel_REQ) ::)) as element() (:: schema-element(ns2:ENTEL_WS_AgregarInfoTrabajo_REQ) ::) {
    <ns2:ENTEL_WS_AgregarInfoTrabajo_REQ>

        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID> </ns2:ID>
                <ns2:externalID></ns2:externalID>
                <ns2:sourceSystem> </ns2:sourceSystem>
                <ns2:BOUserName></ns2:BOUserName>
                <ns2:engineerNotes></ns2:engineerNotes>
                <ns2:appointmentDate></ns2:appointmentDate>
                <ns2:applicableDuring></ns2:applicableDuring>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:ENTEL_WS_AgregarInfoTrabajo_REQ>
};

local:func($Request)