xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)


declare namespace ns1="http://www.entel.cl/EBM/CompleteSuccessfully/Send/v1";
(:: import schema at "../../../../../ESC/Primary/EBM/CompleteSuccessfully_v1_EBM.xsd" ::)
declare namespace ns2="urn:toa:outbound";
(:: import schema at "../../../../../../OFSC/OutboundGateway/WSDL/outboundAPI.xsd" ::)


declare variable $message_id as xs:string external;
declare variable $status as xs:string external;
declare variable $description as xs:string external;
declare variable $data as xs:string external;


declare function local:func($message_id as xs:string, $status as xs:string, $description as xs:string, $data as xs:string) as element() (:: schema-element(ns2:set_message_status) ::) {
    <ns2:set_message_status>
        <user>
            <now></now>
            <login></login>
            <company></company>
            <auth_string></auth_string>
        </user>
        <messages>
            <message>
                <message_id>{$message_id}</message_id>
                <status>{$status}</status>
                <description>{$description}</description>
                <data>{$data}</data>
            </message>
        </messages>
    </ns2:set_message_status>
};

local:func($message_id,$status, $description,$data)