xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CambiaEstadoResuelto/Cancel/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SVC-DSK_v1/ResourceAdapters/CHL-SVC-DSK-SVC-DSK_CambiaEstadoResuelto_RA_v1/CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoResuelto_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CompleteSuccessfully/Send/v1";
(:: import schema at "../../../../../ESC/Primary/EBM/CompleteSuccessfully_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $request as element() (:: schema-element(ns1:CompleteSuccessfully_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:CompleteSuccessfully_REQ) ::)) as element() (:: schema-element(ns2:CambiaEstadoResuelto_REQ) ::) {
    <ns2:CambiaEstadoResuelto_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}"
                countryCode="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@countryCode)}">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($request/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" reqTimestamp="{fn:data($request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}" rspTimestamp="{fn:data($request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}" processID="{fn:data($request/ns3:RequestHeader/ns3:Trace/@processID)}" eventID="{fn:data($request/ns3:RequestHeader/ns3:Trace/@eventID)}" sourceID="{fn:data($request/ns3:RequestHeader/ns3:Trace/@sourceID)}" correlationEventID="{fn:data($request/ns3:RequestHeader/ns3:Trace/@correlationEventID)}" conversationID="{fn:data($request/ns3:RequestHeader/ns3:Trace/@conversationID)}" correlationID="{fn:data($request/ns3:RequestHeader/ns3:Trace/@correlationID)}">
                <ns3:Service code="{fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}" name="{fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}" operation="{fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}">
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel name="{fn:data($request/ns3:RequestHeader/ns3:Channel/@name)}" mode="{fn:data($request/ns3:RequestHeader/ns3:Channel/@mode)}">
            </ns3:Channel>
            <ns4:Result status="{fn:data($request/ns3:RequestHeader/ns4:Result/@status)}" description="{fn:data($request/ns3:RequestHeader/ns4:Result/@description)}">
                <ns5:CanonicalError code="{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}" description="{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}" type="{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}">
                </ns5:CanonicalError>
                <ns5:SourceError code="{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}" description="{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}">
                    <ns5:ErrorSourceDetails source="{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                    </ns5:ErrorSourceDetails>
                    {
                        if ($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                        then <ns5:SourceFault>{fn:data($request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                        else ()
                    }
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError code="{fn:data($request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@code)}" description="{fn:data($request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@description)}">
                        <ns5:ErrorSourceDetails source="{fn:data($request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                        </ns5:ErrorSourceDetails>
                        {
                            if ($request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)
                            then <ns5:SourceFault>{fn:data($request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                            else ()
                        }
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                <ns2:externalID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                <ns2:sourceSystem></ns2:sourceSystem>
                <ns2:BOUserName></ns2:BOUserName>
                <ns2:engineerNotes></ns2:engineerNotes>
                <ns2:status></ns2:status>
                <ns2:closeReasonSD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closingReasonSD)}</ns2:closeReasonSD>
                <ns2:closingMethodResolution>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)}</ns2:closingMethodResolution>
                <ns2:originClosing>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:originClosing)}</ns2:originClosing>
                <ns2:troubleTicketReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns2:troubleTicketReason>
                <ns2:troubleTicketReasonTier1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)}</ns2:troubleTicketReasonTier1>
                <ns2:troubleTicketReasonTier2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)}</ns2:troubleTicketReasonTier2>
                <ns2:troubleTicketReasonTier3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)}</ns2:troubleTicketReasonTier3>
                <ns2:responsability>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:responsability)}</ns2:responsability>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CambiaEstadoResuelto_REQ>
};

local:func($request)