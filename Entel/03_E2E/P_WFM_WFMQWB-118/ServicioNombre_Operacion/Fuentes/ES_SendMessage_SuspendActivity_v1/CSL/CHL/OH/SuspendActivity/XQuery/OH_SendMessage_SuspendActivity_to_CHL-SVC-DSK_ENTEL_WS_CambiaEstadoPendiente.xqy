xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CambiaEstadoPendiente/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SVC-DSK_v1/ResourceAdapters/CHL-SVC-DSK-SVC-DSK_CambiaEstadoPendiente_RA_v1/CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoPendiente_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/SuspendActivity/Send/v1";
(:: import schema at "../../../../../ESC/Primary/SuspendActivity_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $request as element() (:: schema-element(ns1:SuspendActivity_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:SuspendActivity_REQ) ::)) as element() (:: schema-element(ns2:CambiaEstadoPendiente_REQ) ::) {
    <ns2:CambiaEstadoPendiente_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="" eventID="">
                <ns3:Service>
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel>
            </ns3:Channel>
            <ns4:Result status="">
                <ns5:CanonicalError>
                </ns5:CanonicalError>
                <ns5:SourceError>
                    <ns5:ErrorSourceDetails>
                    </ns5:ErrorSourceDetails>
                    <ns5:SourceFault></ns5:SourceFault>
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError>
                        <ns5:ErrorSourceDetails>
                        </ns5:ErrorSourceDetails>
                        <ns5:SourceFault></ns5:SourceFault>
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                <ns2:externalID></ns2:externalID>
                <ns2:sourceSystem></ns2:sourceSystem>
                <ns2:BOUserName></ns2:BOUserName>
                <ns2:engineerNotes></ns2:engineerNotes>
                <ns2:appointmentDate></ns2:appointmentDate>
                <ns2:applicableDuring></ns2:applicableDuring>
                <ns2:statusReason></ns2:statusReason>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CambiaEstadoPendiente_REQ>
};

local:func($request)