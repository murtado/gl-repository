xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/ENTEL_WS_AgregarInfoTrabajo/Update/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_ENTEL_WS_AgregarInfoTrabajo_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $response as element() (:: schema-element(ns1:Result) ::) external;

declare function local:func($response as element() (:: schema-element(ns1:Result) ::)) as element() (:: schema-element(ns2:ENTEL_WS_AgregarInfoTrabajo_FRSP) ::) {
    <ns2:ENTEL_WS_AgregarInfoTrabajo_FRSP>
        <ns3:ResponseHeader>
            <ns3:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="" eventID="">
                <ns3:Service>
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel>
            </ns3:Channel>
            <ns1:Result status="{fn:data($response/@status)}">
                {
                    if ($response/@description)
                    then attribute description {fn:data($response/@description)}
                    else ()
                }
                {
                    if ($response/ns4:CanonicalError)
                    then 
                        <ns4:CanonicalError>
                            {
                                if ($response/ns4:CanonicalError/@code)
                                then attribute code {fn:data($response/ns4:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns4:CanonicalError/@description)
                                then attribute description {fn:data($response/ns4:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($response/ns4:CanonicalError/@type)
                                then attribute type {fn:data($response/ns4:CanonicalError/@type)}
                                else ()
                            }
                        </ns4:CanonicalError>
                    else ()
                }
                {
                    if ($response/ns4:SourceError)
                    then 
                        <ns4:SourceError>
                            {
                                if ($response/ns4:SourceError/@code)
                                then attribute code {fn:data($response/ns4:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($response/ns4:SourceError/@description)
                                then attribute description {fn:data($response/ns4:SourceError/@description)}
                                else ()
                            }
                            <ns4:ErrorSourceDetails>
                                {
                                    if ($response/ns4:SourceError/ns4:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($response/ns4:SourceError/ns4:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($response/ns4:SourceError/ns4:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($response/ns4:SourceError/ns4:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns4:ErrorSourceDetails>
                            {
                                if ($response/ns4:SourceError/ns4:SourceFault)
                                then <ns4:SourceFault>{$response/ns4:SourceError/ns4:SourceFault}</ns4:SourceFault>
                                else ()
                            }
                        </ns4:SourceError>
                    else ()
                }
                {
                    if ($response/ns1:CorrelativeErrors)
                    then 
                        <ns1:CorrelativeErrors>
                            {
                                for $SourceError in $response/ns1:CorrelativeErrors/ns4:SourceError
                                return 
                                <ns4:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            }
                        </ns1:CorrelativeErrors>
                    else ()
                }
            </ns1:Result>
        </ns3:ResponseHeader>
        <ns2:Body></ns2:Body>
    </ns2:ENTEL_WS_AgregarInfoTrabajo_FRSP>
};

local:func($response)