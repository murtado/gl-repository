xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CambiaEstadoPendiente/Update/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoPendiente_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Result as element() (:: schema-element(ns1:Result) ::) external;

declare function local:func($Result as element() (:: schema-element(ns1:Result) ::)) as element() (:: schema-element(ns2:CambiaEstadoPendiente_FRSP) ::) {
    <ns2:CambiaEstadoPendiente_FRSP>
        <ns3:ResponseHeader>
            <ns3:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="" eventID="">
                <ns3:Service>
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel>
            </ns3:Channel>
            <ns1:Result status="{fn:data($Result/@status)}">
                {
                    if ($Result/@description)
                    then attribute description {fn:data($Result/@description)}
                    else ()
                }
                {
                    if ($Result/ns4:CanonicalError)
                    then 
                        <ns4:CanonicalError>
                            {
                                if ($Result/ns4:CanonicalError/@code)
                                then attribute code {fn:data($Result/ns4:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($Result/ns4:CanonicalError/@description)
                                then attribute description {fn:data($Result/ns4:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($Result/ns4:CanonicalError/@type)
                                then attribute type {fn:data($Result/ns4:CanonicalError/@type)}
                                else ()
                            }
                        </ns4:CanonicalError>
                    else ()
                }
                {
                    if ($Result/ns4:SourceError)
                    then 
                        <ns4:SourceError>
                            {
                                if ($Result/ns4:SourceError/@code)
                                then attribute code {fn:data($Result/ns4:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($Result/ns4:SourceError/@description)
                                then attribute description {fn:data($Result/ns4:SourceError/@description)}
                                else ()
                            }
                            <ns4:ErrorSourceDetails>
                                {
                                    if ($Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns4:ErrorSourceDetails>
                            {
                                if ($Result/ns4:SourceError/ns4:SourceFault)
                                then <ns4:SourceFault>{$Result/ns4:SourceError/ns4:SourceFault}</ns4:SourceFault>
                                else ()
                            }
                        </ns4:SourceError>
                    else ()
                }
                {
                    if ($Result/ns1:CorrelativeErrors)
                    then 
                        <ns1:CorrelativeErrors>
                            {
                                for $SourceError in $Result/ns1:CorrelativeErrors/ns4:SourceError
                                return 
                                <ns4:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            }
                        </ns1:CorrelativeErrors>
                    else ()
                }
            </ns1:Result>
        </ns3:ResponseHeader>
        <ns2:Body></ns2:Body>
    </ns2:CambiaEstadoPendiente_FRSP>
};

local:func($Result)