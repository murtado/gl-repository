xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CambiaEstadoEnCurso/Send/v1";
(:: import schema at "../CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoEnCurso_v1_EBM.xsd" ::)
declare namespace ns2="urn:ENTEL_WS_CambiaEstadoIncidente";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SVC-DSK-SVC-DSK/wsdl/entel_ws_cambiaestadoincidente/ENTEL_WS_CambiaEstadoIncidenteService.wsdl" ::)

declare variable $Accion_Glosa as xs:string external;
declare variable $request as element() (:: schema-element(ns1:CambiaEstadoEnCurso_REQ) ::) external;

declare function local:func($Accion_Glosa as xs:string, 
                            $request as element() (:: schema-element(ns1:CambiaEstadoEnCurso_REQ) ::)) 
                            as element() (:: schema-element(ns2:CambiaEstadoEnCurso) ::) {
    <ns2:CambiaEstadoEnCurso>
        <ns2:Accion>{fn:data($Accion_Glosa)}</ns2:Accion>
        <ns2:GrupoAsignado>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:CustomerAccount/ns1:customerGroup)}</ns2:GrupoAsignado>
        <ns2:MotivoEstado>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:statusReason)}</ns2:MotivoEstado>
        <ns2:Numero_Incidencia>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:Numero_Incidencia>
        <ns2:UsuarioAsignado>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</ns2:UsuarioAsignado>
    </ns2:CambiaEstadoEnCurso>
};

local:func($Accion_Glosa, $request)