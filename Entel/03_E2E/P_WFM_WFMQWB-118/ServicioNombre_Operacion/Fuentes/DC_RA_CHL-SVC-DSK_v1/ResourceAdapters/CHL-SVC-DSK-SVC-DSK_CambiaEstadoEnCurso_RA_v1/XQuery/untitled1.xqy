xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/update_CHL-SVC-DSK-SVC-DSK_CambiaEstadoEnCurso_REQ.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/StartActivity/Send/v1";
(:: import schema at "../../../../ES_SendMessage_StartActivity_v1/ESC/Primary/SendMessage_StartActivity_v1_EBM.xsd" ::)

declare variable $updateWoReq as element() (:: schema-element(ns2:StartActivity_REQ) ::) external;

declare function local:func($updateWoReq as element() (:: schema-element(ns2:StartActivity_REQ) ::)) as element() (:: schema-element(ns1:Root-Element) ::) {
    <ns1:Root-Element>
        <ns1:Accion>{fn:data($updateWoReq/ns2:Body/ns2:WorkOrder/ns2:actionSD)}</ns1:Accion>
        <ns1:GrupoAsignado></ns1:GrupoAsignado>
        <ns1:MotivoEstado></ns1:MotivoEstado>
        <ns1:Numero_Incidencia></ns1:Numero_Incidencia>
        <ns1:UsuarioAsignado></ns1:UsuarioAsignado>
    </ns1:Root-Element>
};

local:func($updateWoReq)