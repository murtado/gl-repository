xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/CreateWorkOrder/Create/v1";
(:: import schema at "../../../../../../ES_CreateWorkOrder_v1/ESC/Primary/CreateWorkOrder_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CreateWorkOrderSchedule/Create/v1";
(:: import schema at "../../../../../ESC/Primary/CreateWorkOrderSchedule_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Request as element() (:: schema-element(ns1:CreateWorkOrderSchedule_REQ) ::) external;
declare variable $Resource as xs:string external;
declare variable $Source as xs:string external;


declare function local:func($Resource as xs:string,$Source as xs:string,$Request as element() (:: schema-element(ns1:CreateWorkOrderSchedule_REQ) ::)) as element() (:: schema-element(ns2:CreateWorkOrder_REQ) ::) {
    <ns2:CreateWorkOrder_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@countryCode)}"> </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($Request/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($Request/ns3:RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($Request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($Request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($Request/ns3:RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($Request/ns3:RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($Request/ns3:RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($Request/ns3:RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            {
                if ($Request/ns3:RequestHeader/ns4:Result)
                then 
                    <ns4:Result status="{fn:data($Request/ns3:RequestHeader/ns4:Result/@status)}">
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/@description)
                            then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/@description)}
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError)
                            then 
                                <ns5:CanonicalError>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)
                                        then attribute code {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)
                                        then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)
                                        then attribute type {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns5:CanonicalError>
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError)
                            then 
                                <ns5:SourceError>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)
                                        then attribute code {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)
                                        then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors)
                            then 
                                <ns4:CorrelativeErrors>
                                    {
                                        for $SourceError in $Request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                        return 
                                        <ns5:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns5:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns5:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns5:SourceFault)
                                                then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                                else ()
                                            }
                                        </ns5:SourceError>
                                    }
                                </ns4:CorrelativeErrors>
                            else ()
                        }
                    </ns4:Result>
                else ()
            }
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns2:annulmentTime>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns2:annulmentTime>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)
                    then <ns2:appointmentDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns2:appointmentDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns2:appointmentTimes>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:appointmentTimes>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
                    then <ns2:assignmentTime>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns2:assignmentTime>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
                    then <ns2:broadbandIndicator>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns2:broadbandIndicator>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
                    then <ns2:cellComercial>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns2:cellComercial>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:contactName)
                    then <ns2:contactName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns2:contactName>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns2:contractor>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:contractor>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:createdDate)
                    then <ns2:createdDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns2:createdDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
                    then <ns2:deliveryImplementacionDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns2:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:depReg)
                    then <ns2:depReg>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns2:depReg>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
                    then <ns2:emailComercial>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns2:emailComercial>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
                    then <ns2:emissionDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns2:emissionDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:entryDate)
                    then <ns2:entryDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns2:entryDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns2:estimatedFinishDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:estimatedFinishDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
                    then <ns2:externalStatus>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns2:externalStatus>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalType)
                    then <ns2:externalType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns2:externalType>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:externalZone)
                    then <ns2:externalZone>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns2:externalZone>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:finishTime)
                    then <ns2:finishTime>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns2:finishTime>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:installationCost)
                    then <ns2:installationCost>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns2:installationCost>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
                    then <ns2:legacyBOUserName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns2:legacyBOUserName>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
                    then <ns2:nameComercial>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns2:nameComercial>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns2:networkResourceID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:networkResourceID>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
                    then <ns2:networkSpeed>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns2:networkSpeed>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns2:networkType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:networkType>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:oFTcloseReason)
                    then <ns2:OFTcloseReason>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:oFTcloseReason)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:oFTcloseType)
                    then <ns2:OFTcloseType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:oFTcloseType)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:oFTsummary)
                    then <ns2:OFTsummary>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:oFTsummary)}</ns2:OFTsummary>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
                    then <ns2:phoneComercial>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns2:phoneComercial>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns2:requestType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:requestType>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
                    then <ns2:rescheduleRequester>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns2:rescheduleRequester>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
                    then <ns2:salesChannelID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns2:salesChannelID>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
                    then <ns2:salesChannelType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns2:salesChannelType>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns2:scheduleUserName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:scheduleUserName>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:segment)
                    then <ns2:segment>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:segment>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceID)
                    then <ns2:serviceID>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns2:serviceID>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceName)
                    then <ns2:serviceName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns2:serviceName>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
                    then <ns2:serviceTemplate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns2:serviceTemplate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceType)
                    then <ns2:serviceType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns2:serviceType>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
                    then <ns2:serviceTypeSIAC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns2:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
                    then <ns2:severityProblem>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns2:severityProblem>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
                    then <ns2:signatureDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns2:signatureDate>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
                    then <ns2:slaWindowEnd>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
                    then <ns2:slaWindowStart>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns2:slaWindowStart>
                    else ()
                }
                {
                    if ($Source!='')
                    then <ns2:sourceSystem>{$Source}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:startTime)
                    then <ns2:startTime>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:startTime)}</ns2:startTime>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
                    then <ns2:troubleAction>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns2:troubleAction>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
                    then <ns2:troubleDescription>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns2:troubleDescription>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
                    then <ns2:troubleDiagnosis>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns2:troubleDiagnosis>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:type)
                    then <ns2:type>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:type)}</ns2:type>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule)
                    then <ns2:WorkSchedule>
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
                            then <ns2:applicableDuring>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:applicableDuring>
                            else ()
                        }</ns2:WorkSchedule>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation)
                    then <ns2:absoluteLocalLocation>
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
                            then <ns2:X>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:X>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
                            then <ns2:Y>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:Y>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone)
                            then <ns2:timezone>
                                {
                                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)
                                    then <ns2:name>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)}</ns2:name>
                                    else ()
                                }
                            </ns2:timezone>
                            else ()
                        }</ns2:absoluteLocalLocation>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:address)
                    then <ns2:address>
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)
                            then <ns2:commune>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)}</ns2:commune>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)
                            then <ns2:city>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)}</ns2:city>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)
                            then <ns2:region>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)}</ns2:region>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)
                            then <ns2:streetName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)}</ns2:streetName>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)
                            then <ns2:addressReference>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)}</ns2:addressReference>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)
                            then <ns2:buildingType>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)}</ns2:buildingType>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)
                            then <ns2:complentaryAddress>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)}</ns2:complentaryAddress>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)
                            then <ns2:department>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)}</ns2:department>
                            else ()
                        }</ns2:address>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount)
                    then <ns2:customerAccount>
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)
                            then <ns2:alias>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns2:alias>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)
                            then <ns2:customerGroup>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns2:customerGroup>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)
                            then <ns2:portabilityIndicator>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns2:portabilityIndicator>
                            else ()
                        }
                        {
                            if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact)
                            then <ns2:contact>
                                {
                                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber)
                                    then <ns2:alternativePhoneNumber>
                                        <ns2:number>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns2:number></ns2:alternativePhoneNumber>
                                    else ()
                                }
                                {
                                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone)
                                    then <ns2:cellPhone>
                                        <ns2:number>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns2:number></ns2:cellPhone>
                                    else ()
                                }
                                {
                                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email)
                                    then <ns2:email>
                                        <ns2:eMailAddress>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns2:eMailAddress></ns2:email>
                                    else ()
                                }
                                {
                                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification)
                                    then <ns2:IndividualIdentification>
                                        <ns2:number>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns2:number></ns2:IndividualIdentification>
                                    else ()
                                }
                                {
                                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName)
                                    then <ns2:IndividualName>
                                        <ns2:formatedName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns2:formatedName></ns2:IndividualName>
                                    else ()
                                }</ns2:contact>
                            else ()
                        }</ns2:customerAccount>
                    else ()
                }
                <ns2:Language>
                    <ns2:alphabetName>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Language/ns1:alphabetName)}</ns2:alphabetName>
                </ns2:Language>
                <ns2:PartyResource>
                    {
                        if ($Resource!='')
                        then <ns2:ID>{$Resource}</ns2:ID>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification)
                        then <ns2:IndividualIdentification>
                            <ns2:number>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                            {
                                if ($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)
                                then <ns2:type>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)}</ns2:type>
                                else ()
                            }</ns2:IndividualIdentification>
                        else ()
                    }
                </ns2:PartyResource>
                <ns2:Product>
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                        then <ns2:ANISQuantity>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:ANISQuantity>
                        else (<ns2:ANISQuantity/>)
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                        then <ns2:ANISQuantityRequired>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns2:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)
                        then <ns2:Category>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)}</ns2:Category>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                        then <ns2:DECOQuantity>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns2:DECOQuantity>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                        then <ns2:DECOQuantityRequired>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)
                        then <ns2:Equipment>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)}</ns2:Equipment>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                        then <ns2:layerMPLS>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns2:layerMPLS>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                        then <ns2:ngnModel>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns2:ngnModel>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                        then <ns2:PHONEQuantity>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns2:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                        then <ns2:externalProductCode>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns2:externalProductCode>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                        then <ns2:externalProductDescription>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns2:externalProductDescription>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                        then <ns2:STBQuantity>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:STBQuantity>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)
                        then <ns2:VoiceIndicator>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)}</ns2:VoiceIndicator>
                        else ()
                    }
                    {
                        if ($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                        then <ns2:WIFIExtensorQuantity>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns2:WIFIExtensorQuantity>
                        else ()
                    }
                </ns2:Product>
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CreateWorkOrder_REQ>
};

local:func($Resource, $Source ,$Request)