xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CreateWorkOrderSchedule/Create/v1";
(:: import schema at "../../../../../ESC/Primary/CreateWorkOrderSchedule_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/GetWorkOrderSchedule/Get/v1";
(:: import schema at "../../../../../../ES_GetWorkOrderSchedule_v1/ESC/Primary/GetWorkOrderSchedule_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Request as element() (:: schema-element(ns1:CreateWorkOrderSchedule_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:CreateWorkOrderSchedule_REQ) ::)) as element() (:: schema-element(ns2:GetWorkOrderSchedule_REQ) ::) {
    <ns2:GetWorkOrderSchedule_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($Request/ns3:RequestHeader/ns3:Consumer/@countryCode)}"></ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($Request/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($Request/ns3:RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($Request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($Request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($Request/ns3:RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($Request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($Request/ns3:RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($Request/ns3:RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($Request/ns3:RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($Request/ns3:RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            {
                if ($Request/ns3:RequestHeader/ns4:Result)
                then 
                    <ns4:Result status="{fn:data($Request/ns3:RequestHeader/ns4:Result/@status)}">
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/@description)
                            then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/@description)}
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError)
                            then 
                                <ns5:CanonicalError>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)
                                        then attribute code {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)
                                        then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)
                                        then attribute type {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns5:CanonicalError>
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError)
                            then 
                                <ns5:SourceError>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)
                                        then attribute code {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)
                                        then attribute description {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($Request/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            else ()
                        }
                        {
                            if ($Request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors)
                            then 
                                <ns4:CorrelativeErrors>
                                    {
                                        for $SourceError in $Request/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                        return 
                                        <ns5:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns5:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns5:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns5:SourceFault)
                                                then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                                else ()
                                            }
                                        </ns5:SourceError>
                                    }
                                </ns4:CorrelativeErrors>
                            else ()
                        }
                    </ns4:Result>
                else ()
            }
        </ns3:RequestHeader>
<ns2:Body>
            <ns2:Allocation>
  
             
               <ns2:Datelist>
                   <ns2:AllocationDate>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns2:AllocationDate>
                </ns2:Datelist>
                
                

   

                
                    <ns2:IsCalculatedDurationRequired>true</ns2:IsCalculatedDurationRequired>


                   <ns2:IsCalculatedTravelRequired>true</ns2:IsCalculatedTravelRequired>

             
                    <ns2:IsCalculatedSkillRequired>true</ns2:IsCalculatedSkillRequired>

                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule)
                    then <ns2:WorkSchedule>
                        <ns2:ApplicableDuring>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:ApplicableDuring></ns2:WorkSchedule>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:PartyRole)
                    then <ns2:PartyRole>
                        {
                            for $name in $Request/ns1:Body/ns1:WorkOrder/ns1:PartyRole/ns1:name
                            return 
                            <ns2:Name>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyRole/ns1:name)}</ns2:Name>
                        }</ns2:PartyRole>
                    else ()
                }

            
                    <ns2:IsReturnTimeSlotInfo>true</ns2:IsReturnTimeSlotInfo>


                <ns2:IsDetermineLocationByWorkZone>true</ns2:IsDetermineLocationByWorkZone>

                <ns2:IsNotAggregateResults>false</ns2:IsNotAggregateResults>

                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:Allocation/ns1:remainingTime)
                    then <ns2:RemainingTime>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Allocation/ns1:remainingTime)}</ns2:RemainingTime>
                    else ()
                }
                {
                    if ($Request/ns1:Body/ns1:WorkOrder/ns1:Allocation/ns1:reserveFor)
                    then <ns2:ReserveFor>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Allocation/ns1:reserveFor)}</ns2:ReserveFor>
                    else ()
                }
                

                
                
                
               
                <ns2:AllocationSpecChar>
                    <ns2:name>worktype_label</ns2:name>
                    {
                     if($Request/ns1:Body/ns1:WorkOrder/ns1:type)then
                    <ns2:value>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:type)}</ns2:value>
                    else(<ns2:value/>)
                     }
                </ns2:AllocationSpecChar>


                <ns2:AllocationSpecChar>
                    <ns2:name>A_COMUNA</ns2:name>
                    {
                   if($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)then
                    <ns2:value>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)}</ns2:value>
                    else(<ns2:value/>)
                   }
                </ns2:AllocationSpecChar>


                <ns2:AllocationSpecChar>
                    <ns2:name>A_SEGMENT</ns2:name>
                     {
                     if($Request/ns1:Body/ns1:WorkOrder/ns1:segment)then
                    <ns2:value>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:value>
                    else(<ns2:value/>)
                    }
                </ns2:AllocationSpecChar>

               
               <ns2:AllocationSpecChar>
                    <ns2:name>A_ANIS_EQUIP_QTY</ns2:name>
                     {
                     if($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)then
                    <ns2:value>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:value>
                    else(<ns2:value/>)
                    }
                </ns2:AllocationSpecChar>

               
                <ns2:AllocationSpecChar>
                    <ns2:name>A_EQUIP_QTY_REQ</ns2:name>
                     {
                    if($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)then
                    <ns2:value>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:value>
                    else(<ns2:value/>)
                     }
                </ns2:AllocationSpecChar>
                 

               <ns2:AllocationSpecChar>
                    <ns2:name>A_STB_EQUIP_QTY</ns2:name>
                     {
                    if($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)then
                    <ns2:value>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:value>
                    else(<ns2:value/>)
                    }
                </ns2:AllocationSpecChar>
               
            </ns2:Allocation>
        </ns2:Body>
    </ns2:GetWorkOrderSchedule_REQ>
};

local:func($Request)