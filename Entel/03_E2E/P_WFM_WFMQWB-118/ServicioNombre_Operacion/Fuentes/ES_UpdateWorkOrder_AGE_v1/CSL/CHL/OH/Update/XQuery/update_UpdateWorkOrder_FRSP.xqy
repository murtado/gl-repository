xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/UpdateWorkOrder/Update/v1";
(:: import schema at "../../../../../../ES_UpdateWorkOrder_v1/ESC/Primary/UpdateWorkOrder_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $headerResponseError as element() (:: schema-element(ns1:ResponseHeader) ::) external;

declare function local:func($headerResponseError as element() (:: schema-element(ns1:ResponseHeader) ::)) as element() (:: schema-element(ns2:UpdateWorkOrder_FRSP) ::) {
    <ns2:UpdateWorkOrder_FRSP>
      
        <ns1:ResponseHeader>
            <ns1:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns1:Consumer>
            <ns1:Trace clientReqTimestamp="" eventID="">
                <ns1:Service>
                </ns1:Service>
            </ns1:Trace>
            <ns1:Channel>
            </ns1:Channel>
            <ns3:Result status="{fn:data($headerResponseError/ns3:Result/@status)}">
                {
                    if ($headerResponseError/ns3:Result/@description)
                    then attribute description {fn:data($headerResponseError/ns3:Result/@description)}
                    else ()
                }
                {
                    if ($headerResponseError/ns3:Result/ns4:CanonicalError)
                    then 
                        <ns4:CanonicalError>
                            {
                                if ($headerResponseError/ns3:Result/ns4:CanonicalError/@code)
                                then attribute code {fn:data($headerResponseError/ns3:Result/ns4:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns3:Result/ns4:CanonicalError/@description)
                                then attribute description {fn:data($headerResponseError/ns3:Result/ns4:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns3:Result/ns4:CanonicalError/@type)
                                then attribute type {fn:data($headerResponseError/ns3:Result/ns4:CanonicalError/@type)}
                                else ()
                            }
                        </ns4:CanonicalError>
                    else ()
                }
                {
                    if ($headerResponseError/ns3:Result/ns4:SourceError)
                    then 
                        <ns4:SourceError>
                            {
                                if ($headerResponseError/ns3:Result/ns4:SourceError/@code)
                                then attribute code {fn:data($headerResponseError/ns3:Result/ns4:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns3:Result/ns4:SourceError/@description)
                                then attribute description {fn:data($headerResponseError/ns3:Result/ns4:SourceError/@description)}
                                else ()
                            }
                            <ns4:ErrorSourceDetails>
                                {
                                    if ($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns4:ErrorSourceDetails>
                            {
                                if ($headerResponseError/ns3:Result/ns4:SourceError/ns4:SourceFault)
                                then <ns4:SourceFault>{fn:data($headerResponseError/ns3:Result/ns4:SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                else ()
                            }
                        </ns4:SourceError>
                    else ()
                }
                {
                    if ($headerResponseError/ns3:Result/ns3:CorrelativeErrors)
                    then 
                        <ns3:CorrelativeErrors>
                            {
                                for $SourceError in $headerResponseError/ns3:Result/ns3:CorrelativeErrors/ns4:SourceError
                                return 
                                <ns4:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            }
                        </ns3:CorrelativeErrors>
                    else ()
                }
            </ns3:Result>
        </ns1:ResponseHeader>
        <ns2:Body></ns2:Body>
    </ns2:UpdateWorkOrder_FRSP>
};

local:func($headerResponseError)