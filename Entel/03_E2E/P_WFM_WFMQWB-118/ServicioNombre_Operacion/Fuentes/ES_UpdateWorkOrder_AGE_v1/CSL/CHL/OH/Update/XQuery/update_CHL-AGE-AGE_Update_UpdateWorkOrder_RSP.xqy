xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/UpdateActivity_AGE/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-AGE_v1/ResourceAdapters/CHL-AGE-AGE_UpdateActivity_AGE_RA_v1/CSC/CHL-AGE-AGE_UpdateActivity_AGE_v1_EBM.xsd" ::)
declare namespace ns3="http://www.entel.cl/EBM/UpdateWorkOrder_AGE/Update/v1";
(:: import schema at "../../../../../ESC/Primary/UpdateWorkOrder_AGE_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare variable $Response as element() (:: schema-element(ns1:UpdateActivity_RSP) ::) external;
declare variable $Request as element() (:: schema-element(ns2:RequestHeader) ::) external;

declare function local:func($Response as element() (:: schema-element(ns1:UpdateActivity_RSP) ::), 
                            $Request as element() (:: schema-element(ns2:RequestHeader) ::)) 
                            as element() (:: schema-element(ns3:UpdateWorkOrder_RSP) ::) {
    <ns3:UpdateWorkOrder_RSP>
        <ns2:ResponseHeader>
            <ns2:Consumer sysCode="{fn:data($Request/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($Request/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($Request/ns2:Consumer/@countryCode)}"></ns2:Consumer>
            <ns2:Trace clientReqTimestamp="{fn:data($Request/ns2:Trace/@clientReqTimestamp)}" eventID="{fn:data($Request/ns2:Trace/@eventID)}">
                {
                    if ($Request/ns2:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($Request/ns2:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns2:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($Request/ns2:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns2:Trace/@processID)
                    then attribute processID {fn:data($Request/ns2:Trace/@processID)}
                    else ()
                }
                {
                    if ($Request/ns2:Trace/@sourceID)
                    then attribute sourceID {fn:data($Request/ns2:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($Request/ns2:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($Request/ns2:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($Request/ns2:Trace/@conversationID)
                    then attribute conversationID {fn:data($Request/ns2:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($Request/ns2:Trace/@correlationID)
                    then attribute correlationID {fn:data($Request/ns2:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($Request/ns2:Trace/ns2:Service)
                    then 
                        <ns2:Service>
                            {
                                if ($Request/ns2:Trace/ns2:Service/@code)
                                then attribute code {fn:data($Request/ns2:Trace/ns2:Service/@code)}
                                else ()
                            }
                            {
                                if ($Request/ns2:Trace/ns2:Service/@name)
                                then attribute name {fn:data($Request/ns2:Trace/ns2:Service/@name)}
                                else ()
                            }
                            {
                                if ($Request/ns2:Trace/ns2:Service/@operation)
                                then attribute operation {fn:data($Request/ns2:Trace/ns2:Service/@operation)}
                                else ()
                            }
                        </ns2:Service>
                    else ()
                }
            </ns2:Trace>
            {
                if ($Request/ns2:Channel)
                then 
                    <ns2:Channel>
                        {
                            if ($Request/ns2:Channel/@name)
                            then attribute name {fn:data($Request/ns2:Channel/@name)}
                            else ()
                        }
                        {
                            if ($Request/ns2:Channel/@mode)
                            then attribute mode {fn:data($Request/ns2:Channel/@mode)}
                            else ()
                        }
                    </ns2:Channel>
                else ()
            }
            <ns4:Result status="{fn:data($Response/ns2:ResponseHeader/ns4:Result/@status)}">
                {
                    if ($Response/ns2:ResponseHeader/ns4:Result/@description)
                    then attribute description {fn:data($Response/ns2:ResponseHeader/ns4:Result/@description)}
                    else ()
                }
                {
                    if ($Response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($Response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($Response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($Response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@code)
                                then attribute code {fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@description)
                                then attribute description {fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($Response/ns2:ResponseHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($Response/ns2:ResponseHeader/ns4:Result/ns4:CorrelativeErrors)
                    then 
                        <ns4:CorrelativeErrors>
                            {
                                for $SourceError in $Response/ns2:ResponseHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns4:CorrelativeErrors>
                    else ()
                }
            </ns4:Result>
        </ns2:ResponseHeader>
        <ns3:Body>
            <ns3:WorkOrder>
                {
                    if ($Response/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)
                    then <ns3:affectedNetwork>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:affectedNetwork)}</ns3:affectedNetwork>
                    else ()
                }
                {
                    if ($Response/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns3:annulmentTime>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns3:annulmentTime>
                    else ()
                }
                {
                    if ($Response/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns3:status>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:status)}</ns3:status>
                    else ()
                }
                <ns3:address>
                    <ns3:commune></ns3:commune>
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)
                        then <ns3:city>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)}</ns3:city>
                        else ()
                    }
                </ns3:address>
                <ns3:PartyResource>
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)
                        then <ns3:ID>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns3:ID>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)
                        then <ns3:internalID>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:internalID)}</ns3:internalID>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)
                        then <ns3:resourceTimeZone>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZone)}</ns3:resourceTimeZone>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)
                        then <ns3:UTCOffset>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:UTCOffset)}</ns3:UTCOffset>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZoneIANA)
                        then <ns3:resourceTimeZoneIANA>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:resourceTimeZoneIANA)}</ns3:resourceTimeZoneIANA>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification)
                        then <ns3:individualIdentification>
                            <ns3:number>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:number)}</ns3:number>
                            {
                                if ($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:type)
                                then <ns3:type>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:individualIdentification/ns1:type)}</ns3:type>
                                else ()
                            }</ns3:individualIdentification>
                        else ()
                    }</ns3:PartyResource>
                    {if($Response/ns1:Body/ns1:WorkOrder/ns1:Product) then
                       <ns3:Product>
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                        then <ns3:ANISQuantity>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns3:ANISQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                        then <ns3:ANISQuantityRequired>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns3:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)
                        then <ns3:category>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:category)}</ns3:category>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                        then <ns3:DECOQuantity>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns3:DECOQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                        then <ns3:DECOQuantityRequired>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns3:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)
                        then <ns3:equipment>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:equipment)}</ns3:equipment>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                        then <ns3:layerMPLS>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns3:layerMPLS>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                        then <ns3:ngnModel>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns3:ngnModel>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                        then <ns3:PHONEQuantity>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns3:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                        then <ns3:externalProductCode>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns3:externalProductCode>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                        then <ns3:externalProductDescription>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns3:externalProductDescription>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                        then <ns3:STBQuantity>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns3:STBQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)
                        then <ns3:voiceIndicator>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:voiceIndicator)}</ns3:voiceIndicator>
                        else ()
                    }
                    {
                        if ($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                        then <ns3:WIFIExtensorQuantity>{fn:data($Response/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns3:WIFIExtensorQuantity>
                        else ()
                    }</ns3:Product>
                    else()}
               </ns3:WorkOrder>
        </ns3:Body>
    </ns3:UpdateWorkOrder_RSP>
};

local:func($Response, $Request)