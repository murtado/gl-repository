xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/UpdateWorkOrder/Update/v1";
(:: import schema at "../../../../../../ES_UpdateWorkOrder_v1/ESC/Primary/UpdateWorkOrder_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $headerResponseError as element() (:: schema-element(ns1:ResponseHeader) ::) external;

declare function local:func($headerResponseError as element() (:: schema-element(ns1:ResponseHeader) ::)) as element() (:: schema-element(ns2:UpdateWorkOrder_FRSP) ::) {
    <ns2:UpdateWorkOrder_FRSP>
      
        <ns1:ResponseHeader>
            <ns1:Consumer sysCode="{fn:data($headerResponseError/ns1:Consumer/@sysCode)}" enterpriseCode="{fn:data($headerResponseError/ns1:Consumer/@enterpriseCode)}" countryCode="{fn:data($headerResponseError/ns1:Consumer/@countryCode)}"></ns1:Consumer>
            <ns1:Trace clientReqTimestamp="{fn:data($headerResponseError/ns1:Trace/@clientReqTimestamp)}" eventID="{fn:data($headerResponseError/ns1:Trace/@eventID)}">
                {
                    if ($headerResponseError/ns1:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($headerResponseError/ns1:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($headerResponseError/ns1:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($headerResponseError/ns1:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($headerResponseError/ns1:Trace/@processID)
                    then attribute processID {fn:data($headerResponseError/ns1:Trace/@processID)}
                    else ()
                }
                {
                    if ($headerResponseError/ns1:Trace/@sourceID)
                    then attribute sourceID {fn:data($headerResponseError/ns1:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($headerResponseError/ns1:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($headerResponseError/ns1:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($headerResponseError/ns1:Trace/@conversationID)
                    then attribute conversationID {fn:data($headerResponseError/ns1:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($headerResponseError/ns1:Trace/@correlationID)
                    then attribute correlationID {fn:data($headerResponseError/ns1:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($headerResponseError/ns1:Trace/ns1:Service)
                    then 
                        <ns1:Service>
                            {
                                if ($headerResponseError/ns1:Trace/ns1:Service/@code)
                                then attribute code {fn:data($headerResponseError/ns1:Trace/ns1:Service/@code)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns1:Trace/ns1:Service/@name)
                                then attribute name {fn:data($headerResponseError/ns1:Trace/ns1:Service/@name)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns1:Trace/ns1:Service/@operation)
                                then attribute operation {fn:data($headerResponseError/ns1:Trace/ns1:Service/@operation)}
                                else ()
                            }
                        </ns1:Service>
                    else ()
                }
            </ns1:Trace>
            {
                if ($headerResponseError/ns1:Channel)
                then 
                    <ns1:Channel>
                        {
                            if ($headerResponseError/ns1:Channel/@name)
                            then attribute name {fn:data($headerResponseError/ns1:Channel/@name)}
                            else ()
                        }
                        {
                            if ($headerResponseError/ns1:Channel/@mode)
                            then attribute mode {fn:data($headerResponseError/ns1:Channel/@mode)}
                            else ()
                        }
                    </ns1:Channel>
                else ()
            }
            <ns3:Result status="{fn:data($headerResponseError/ns3:Result/@status)}">
                {
                    if ($headerResponseError/ns3:Result/@description)
                    then attribute description {fn:data($headerResponseError/ns3:Result/@description)}
                    else ()
                }
                {
                    if ($headerResponseError/ns3:Result/ns4:CanonicalError)
                    then 
                        <ns4:CanonicalError>
                            {
                                if ($headerResponseError/ns3:Result/ns4:CanonicalError/@code)
                                then attribute code {fn:data($headerResponseError/ns3:Result/ns4:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns3:Result/ns4:CanonicalError/@description)
                                then attribute description {fn:data($headerResponseError/ns3:Result/ns4:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns3:Result/ns4:CanonicalError/@type)
                                then attribute type {fn:data($headerResponseError/ns3:Result/ns4:CanonicalError/@type)}
                                else ()
                            }
                        </ns4:CanonicalError>
                    else ()
                }
                {
                    if ($headerResponseError/ns3:Result/ns4:SourceError)
                    then 
                        <ns4:SourceError>
                            {
                                if ($headerResponseError/ns3:Result/ns4:SourceError/@code)
                                then attribute code {fn:data($headerResponseError/ns3:Result/ns4:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($headerResponseError/ns3:Result/ns4:SourceError/@description)
                                then attribute description {fn:data($headerResponseError/ns3:Result/ns4:SourceError/@description)}
                                else ()
                            }
                            <ns4:ErrorSourceDetails>
                                {
                                    if ($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($headerResponseError/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns4:ErrorSourceDetails>
                            {
                                if ($headerResponseError/ns3:Result/ns4:SourceError/ns4:SourceFault)
                                then <ns4:SourceFault>{fn:data($headerResponseError/ns3:Result/ns4:SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                else ()
                            }
                        </ns4:SourceError>
                    else ()
                }
                {
                    if ($headerResponseError/ns3:Result/ns3:CorrelativeErrors)
                    then 
                        <ns3:CorrelativeErrors>
                            {
                                for $SourceError in $headerResponseError/ns3:Result/ns3:CorrelativeErrors/ns4:SourceError
                                return 
                                <ns4:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            }
                        </ns3:CorrelativeErrors>
                    else ()
                }
            </ns3:Result>
        </ns1:ResponseHeader>
        <ns2:Body></ns2:Body>
    </ns2:UpdateWorkOrder_FRSP>
};

local:func($headerResponseError)