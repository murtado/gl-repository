xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/NotDone/Send/v1";
(:: import schema at "../../../../../ESC/Primary/NotDone_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $request as element() (:: schema-element(ns1:NotDone_REQ) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:NotDone_REQ) ::)) 
                            as element() (:: schema-element(ns1:NotDone_REQ) ::) {
    <ns1:NotDone_REQ>
        <ns2:RequestHeader>
            <ns2:Consumer sysCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@sysCode)}" enterpriseCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@enterpriseCode)}" countryCode="{fn:data($request/ns2:RequestHeader/ns2:Consumer/@countryCode)}"></ns2:Consumer>
            <ns2:Trace clientReqTimestamp="{fn:data($request/ns2:RequestHeader/ns2:Trace/@clientReqTimestamp)}" eventID="{fn:data($request/ns2:RequestHeader/ns2:Trace/@eventID)}">
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($request/ns2:RequestHeader/ns2:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($request/ns2:RequestHeader/ns2:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@processID)
                    then attribute processID {fn:data($request/ns2:RequestHeader/ns2:Trace/@processID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@sourceID)
                    then attribute sourceID {fn:data($request/ns2:RequestHeader/ns2:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($request/ns2:RequestHeader/ns2:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@conversationID)
                    then attribute conversationID {fn:data($request/ns2:RequestHeader/ns2:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/@correlationID)
                    then attribute correlationID {fn:data($request/ns2:RequestHeader/ns2:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service)
                    then 
                        <ns2:Service>
                            {
                                if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@code)
                                then attribute code {fn:data($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@code)}
                                else ()
                            }
                            {
                                if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@name)
                                then attribute name {fn:data($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@name)}
                                else ()
                            }
                            {
                                if ($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@operation)
                                then attribute operation {fn:data($request/ns2:RequestHeader/ns2:Trace/ns2:Service/@operation)}
                                else ()
                            }
                        </ns2:Service>
                    else ()
                }
            </ns2:Trace>
            {
                if ($request/ns2:RequestHeader/ns2:Channel)
                then 
                    <ns2:Channel>
                        {
                            if ($request/ns2:RequestHeader/ns2:Channel/@name)
                            then attribute name {fn:data($request/ns2:RequestHeader/ns2:Channel/@name)}
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns2:Channel/@mode)
                            then attribute mode {fn:data($request/ns2:RequestHeader/ns2:Channel/@mode)}
                            else ()
                        }
                    </ns2:Channel>
                else ()
            }
            {
                if ($request/ns2:RequestHeader/ns3:Result)
                then 
                    <ns3:Result status="{fn:data($request/ns2:RequestHeader/ns3:Result/@status)}">
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/@description)
                            then attribute description {fn:data($request/ns2:RequestHeader/ns3:Result/@description)}
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError)
                            then 
                                <ns4:CanonicalError>
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@code)
                                        then attribute code {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@description)
                                        then attribute description {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@type)
                                        then attribute type {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns4:CanonicalError>
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError)
                            then 
                                <ns4:SourceError>
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@code)
                                        then attribute code {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@description)
                                        then attribute description {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($request/ns2:RequestHeader/ns3:Result/ns4:SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            else ()
                        }
                        {
                            if ($request/ns2:RequestHeader/ns3:Result/ns3:CorrelativeErrors)
                            then 
                                <ns3:CorrelativeErrors>
                                    {
                                        for $SourceError in $request/ns2:RequestHeader/ns3:Result/ns3:CorrelativeErrors/ns4:SourceError
                                        return 
                                        <ns4:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns4:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns4:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns4:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns4:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns4:SourceFault)
                                                then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                                else ()
                                            }
                                        </ns4:SourceError>
                                    }
                                </ns3:CorrelativeErrors>
                            else ()
                        }
                    </ns3:Result>
                else ()
            }
        </ns2:RequestHeader>
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:ID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns1:ID>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource)
                    then 
                        <ns1:PartyResource>
                            <ns1:pname>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</ns1:pname>
                            {
                                if ($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification)
                                then 
                                    <ns1:IndividualIdentification>
                                        <ns1:number>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns1:number>
                                    </ns1:IndividualIdentification>
                                else ()
                            }
                        </ns1:PartyResource>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:details)
                    then <ns1:details>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:details)}</ns1:details>
                    else ()
                }
               
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns1:externalID>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns1:externalID>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns1:externalNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns1:externalNotes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)
                    then <ns1:customerHostCell>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:customerHostCell)}</ns1:customerHostCell>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)
                    then <ns1:engineerNotes>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns1:engineerNotes>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)
                    then <ns1:progressTask>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:progressTask)}</ns1:progressTask>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns1:OFTcloseType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns1:OFTcloseType>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns1:OFTcloseReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns1:OFTcloseReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)
                    then <ns1:accessTechQTY>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:accessTechQTY)}</ns1:accessTechQTY>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:EPC)
                    then <ns1:EPC>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:EPC)}</ns1:EPC>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:checkList)
                    then <ns1:checkList>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:checkList)}</ns1:checkList>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)
                    then <ns1:ticketCheckList>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ticketCheckList)}</ns1:ticketCheckList>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:zoneKM)
                    then <ns1:zoneKM>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:zoneKM)}</ns1:zoneKM>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns1:status>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:status)}</ns1:status>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion1)
                    then <ns1:prestacion1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion1)}</ns1:prestacion1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion2)
                    then <ns1:prestacion2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion2)}</ns1:prestacion2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion3)
                    then <ns1:prestacion3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion3)}</ns1:prestacion3>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion4)
                    then <ns1:prestacion4>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion4)}</ns1:prestacion4>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacion5)
                    then <ns1:prestacion5>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacion5)}</ns1:prestacion5>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)
                    then <ns1:prestacionQTY1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY1)}</ns1:prestacionQTY1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)
                    then <ns1:prestacionQTY2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY2)}</ns1:prestacionQTY2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)
                    then <ns1:prestacionQTY3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY3)}</ns1:prestacionQTY3>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)
                    then <ns1:prestacionQTY4>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY4)}</ns1:prestacionQTY4>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)
                    then <ns1:prestacionQTY5>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:prestacionQTY5)}</ns1:prestacionQTY5>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:PLCPrestation1)
                    then <ns1:PLCPrestation1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:PLCPrestation1)}</ns1:PLCPrestation1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:deliveryPlace)
                    then <ns1:deliveryPlace>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:deliveryPlace)}</ns1:deliveryPlace>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns1:contractor>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns1:contractor>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)
                    then <ns1:BOUserName>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns1:BOUserName>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:ticketBO)
                    then <ns1:ticketBO>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:ticketBO)}</ns1:ticketBO>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)
                    then <ns1:troubleTicketReason>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReason)}</ns1:troubleTicketReason>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:actionSD)
                    then <ns1:actionSD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns1:actionSD>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:closingReasonSD)
                    then <ns1:closingReasonSD>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closingReasonSD)}</ns1:closingReasonSD>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)
                    then <ns1:troubleTicketReasonTier1>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier1)}</ns1:troubleTicketReasonTier1>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)
                    then <ns1:troubleTicketReasonTier2>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier2)}</ns1:troubleTicketReasonTier2>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)
                    then <ns1:troubleTicketReasonTier3>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:troubleTicketReasonTier3)}</ns1:troubleTicketReasonTier3>
                    else ()
                }
                <ns1:responsability>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:responsability)}</ns1:responsability>
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)
                    then <ns1:closingMethodResolution>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:closingMethodResolution)}</ns1:closingMethodResolution>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:originClosing)
                    then <ns1:originClosing>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:originClosing)}</ns1:originClosing>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:subject)
                    then <ns1:subject>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:subject)}</ns1:subject>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:userNotification)
                    then <ns1:userNotification>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:userNotification)}</ns1:userNotification>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)
                    then <ns1:dateNotification>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:dateNotification)}</ns1:dateNotification>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns1:sourceSystem>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns1:sourceSystem>
                    else ()
                }
                {
                    if ($request/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns1:requestType>{fn:data($request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns1:requestType>
                    else ()
                }
            </ns1:WorkOrder>
        </ns1:Body>
    </ns1:NotDone_REQ>
};

local:func($request)