xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/ENTEL_WS_AgregarInfoTrabajo/Update/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SVC-DSK_v1/ResourceAdapters/CHL-SVC-DSK-SVC-DSK_AgregarInfoTrabajo_RA_v1/CSC/CHL-SVC-DSK-SVC-DSK_ENTEL_WS_AgregarInfoTrabajo_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/StartActivity/Send/v1";
(:: import schema at "../../../../../ESC/Primary/SendMessage_StartActivity_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $agregaInfoTrabajoRq as element() (:: schema-element(ns1:StartActivity_REQ) ::) external;

declare function local:func($agregaInfoTrabajoRq as element() (:: schema-element(ns1:StartActivity_REQ) ::)) as element() (:: schema-element(ns2:ENTEL_WS_AgregarInfoTrabajo_REQ) ::) {
    <ns2:ENTEL_WS_AgregarInfoTrabajo_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}"
                countryCode="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Consumer/@countryCode)}">
            </ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" reqTimestamp="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}" rspTimestamp="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}" processID="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@processID)}" eventID="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@eventID)}" sourceID="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@sourceID)}" correlationEventID="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)}" conversationID="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@conversationID)}" correlationID="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/@correlationID)}">
                <ns3:Service code="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}" name="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}" operation="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}">
                </ns3:Service>
            </ns3:Trace>
            <ns3:Channel name="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Channel/@name)}" mode="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns3:Channel/@mode)}">
            </ns3:Channel>
            <ns4:Result status="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/@status)}" description="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/@description)}">
                <ns5:CanonicalError code="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}" description="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}" type="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}">
                </ns5:CanonicalError>
                <ns5:SourceError code="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}" description="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}">
                    <ns5:ErrorSourceDetails source="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                    </ns5:ErrorSourceDetails>
                    {
                        if ($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                        then <ns5:SourceFault>{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                        else ()
                    }
                </ns5:SourceError>
                <ns4:CorrelativeErrors>
                    <ns5:SourceError code="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@code)}" description="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/@description)}">
                        <ns5:ErrorSourceDetails source="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@source)}" details="{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:ErrorSourceDetails/@details)}">
                        </ns5:ErrorSourceDetails>
                        {
                            if ($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)
                            then <ns5:SourceFault>{fn:data($agregaInfoTrabajoRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                            else ()
                        }
                    </ns5:SourceError>
                </ns4:CorrelativeErrors>
            </ns4:Result>
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:AgregaInfoTrabajo>
                <ns2:Accion>{fn:data($agregaInfoTrabajoRq/ns1:Body/ns1:WorkOrder/ns1:actionSD)}</ns2:Accion>
                <ns2:Numero_Incidencia>{fn:data($agregaInfoTrabajoRq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:Numero_Incidencia>
                <ns2:z1D_Details>{fn:data($agregaInfoTrabajoRq/ns1:Body/ns1:WorkOrder/ns1:details)}</ns2:z1D_Details>
                <ns2:z1D_Summary>{fn:data($agregaInfoTrabajoRq/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:z1D_Summary>
            </ns2:AgregaInfoTrabajo>
        </ns2:Body>
    </ns2:ENTEL_WS_AgregarInfoTrabajo_REQ>
};

local:func($agregaInfoTrabajoRq)