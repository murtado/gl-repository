xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/StartActivity/Send/v1";
(:: import schema at "../../../../../ESC/Primary/SendMessage_StartActivity_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/UpdateWorkOrder_AGE/Update/v1";
(:: import schema at "../../../../../../ES_UpdateWorkOrder_AGE_v1/ESC/Primary/UpdateWorkOrder_AGE_v1_EBM.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $agendadorRq as element() (:: schema-element(ns1:StartActivity_REQ) ::) external;

declare function local:func($agendadorRq as element() (:: schema-element(ns1:StartActivity_REQ) ::)) as element() (:: schema-element(ns2:UpdateWorkOrder_REQ) ::) {
    <ns2:UpdateWorkOrder_REQ>
        <ns3:RequestHeader>
            <ns3:Consumer sysCode="{fn:data($agendadorRq/ns3:RequestHeader/ns3:Consumer/@sysCode)}" enterpriseCode="{fn:data($agendadorRq/ns3:RequestHeader/ns3:Consumer/@enterpriseCode)}" countryCode="{fn:data($agendadorRq/ns3:RequestHeader/ns3:Consumer/@countryCode)}"></ns3:Consumer>
            <ns3:Trace clientReqTimestamp="{fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@clientReqTimestamp)}" eventID="{fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@eventID)}">
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/@processID)
                    then attribute processID {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@processID)}
                    else ()
                }
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/@sourceID)
                    then attribute sourceID {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@sourceID)}
                    else ()
                }
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)
                    then attribute correlationEventID {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@correlationEventID)}
                    else ()
                }
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($agendadorRq/ns3:RequestHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($agendadorRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($agendadorRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($agendadorRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($agendadorRq/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            {
                if ($agendadorRq/ns3:RequestHeader/ns3:Channel)
                then 
                    <ns3:Channel>
                        {
                            if ($agendadorRq/ns3:RequestHeader/ns3:Channel/@name)
                            then attribute name {fn:data($agendadorRq/ns3:RequestHeader/ns3:Channel/@name)}
                            else ()
                        }
                        {
                            if ($agendadorRq/ns3:RequestHeader/ns3:Channel/@mode)
                            then attribute mode {fn:data($agendadorRq/ns3:RequestHeader/ns3:Channel/@mode)}
                            else ()
                        }
                    </ns3:Channel>
                else ()
            }
            {
                if ($agendadorRq/ns3:RequestHeader/ns4:Result)
                then 
                    <ns4:Result status="{fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/@status)}">
                        {
                            if ($agendadorRq/ns3:RequestHeader/ns4:Result/@description)
                            then attribute description {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/@description)}
                            else ()
                        }
                        {
                            if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError)
                            then 
                                <ns5:CanonicalError>
                                    {
                                        if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)
                                        then attribute code {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)
                                        then attribute description {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@description)}
                                        else ()
                                    }
                                    {
                                        if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)
                                        then attribute type {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:CanonicalError/@type)}
                                        else ()
                                    }
                                </ns5:CanonicalError>
                            else ()
                        }
                        {
                            if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError)
                            then 
                                <ns5:SourceError>
                                    {
                                        if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)
                                        then attribute code {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)
                                        then attribute description {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($agendadorRq/ns3:RequestHeader/ns4:Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            else ()
                        }
                        {
                            if ($agendadorRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors)
                            then 
                                <ns4:CorrelativeErrors>
                                    {
                                        for $SourceError in $agendadorRq/ns3:RequestHeader/ns4:Result/ns4:CorrelativeErrors/ns5:SourceError
                                        return 
                                        <ns5:SourceError>
                                            {
                                                if ($SourceError/@code)
                                                then attribute code {fn:data($SourceError/@code)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/@description)
                                                then attribute description {fn:data($SourceError/@description)}
                                                else ()
                                            }
                                            <ns5:ErrorSourceDetails>
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@source)
                                                    then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                                    else ()
                                                }
                                                {
                                                    if ($SourceError/ns5:ErrorSourceDetails/@details)
                                                    then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                                    else ()
                                                }
                                            </ns5:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns5:SourceFault)
                                                then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                                else ()
                                            }
                                        </ns5:SourceError>
                                    }
                                </ns4:CorrelativeErrors>
                            else ()
                        }
                    </ns4:Result>
                else ()
            }
        </ns3:RequestHeader>
        <ns2:Body>
            <ns2:WorkOrder>
                <ns2:ID>{fn:data($agendadorRq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:ID>
                {
                    if ($agendadorRq/ns1:Body/ns1:WorkOrder/ns1:status)
                    then <ns2:status>{fn:data($agendadorRq/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:status>
                    else ()
                }
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:UpdateWorkOrder_REQ>
};

local:func($agendadorRq)