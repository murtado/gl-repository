xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CambiaEstadoEnCurso/Send/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SVC-DSK_v1/ResourceAdapters/CHL-SVC-DSK-SVC-DSK_CambiaEstadoEnCurso_RA_v1/CSC/CHL-SVC-DSK-SVC-DSK_CambiaEstadoEnCurso_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/StartActivity/Send/v1";
(:: import schema at "../../../../../ESC/Primary/SendMessage_StartActivity_v1_EBM.xsd" ::)

declare variable $cambioEstadoEnCursoRq as element() (:: schema-element(ns2:StartActivity_REQ) ::) external;

declare function local:func($cambioEstadoEnCursoRq as element() (:: schema-element(ns2:StartActivity_REQ) ::)) as element() (:: schema-element(ns1:CambiaEstadoEnCurso_REQ) ::) {
    <ns1:CambiaEstadoEnCurso_REQ>
        <ns1:Body>
            <ns1:WorkOrder>
                <ns1:messageID>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:messageID)}</ns1:messageID>
                <ns1:ID>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:ID)}</ns1:ID>
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:externalID)
                    then <ns1:externalID>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:externalID)}</ns1:externalID>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:engineerNotes)
                    then <ns1:engineerNotes>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:engineerNotes)}</ns1:engineerNotes>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:customerHostCell)
                    then <ns1:customerHostCell>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:customerHostCell)}</ns1:customerHostCell>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:customerHostName)
                    then <ns1:customerHostName>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:customerHostName)}</ns1:customerHostName>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:BOUserName)
                    then <ns1:BOUserName>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:BOUserName)}</ns1:BOUserName>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:subject)
                    then <ns1:subject>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:subject)}</ns1:subject>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:userNotification)
                    then <ns1:userNotification>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:userNotification)}</ns1:userNotification>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:dateNotification)
                    then <ns1:dateNotification>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:dateNotification)}</ns1:dateNotification>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:PartyResource)
                    then <ns1:PartyResource>
                        <ns1:pname>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:PartyResource/ns2:pname)}</ns1:pname>
                        {
                            if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:PartyResource/ns2:IndividualIdentification)
                            then <ns1:IndividualIdentification>
                                <ns1:number>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:PartyResource/ns2:IndividualIdentification/ns2:number)}</ns1:number></ns1:IndividualIdentification>
                            else ()
                        }</ns1:PartyResource>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:sourceSystem)
                    then <ns1:sourceSystem>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:sourceSystem)}</ns1:sourceSystem>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:startCoordinates)
                    then <ns1:startCoordinates>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:startCoordinates)}</ns1:startCoordinates>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:status)
                    then <ns1:status>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:status)}</ns1:status>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:details)
                    then <ns1:details>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:details)}</ns1:details>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:CustomerAccount)
                    then <ns1:CustomerAccount>
                        <ns1:customerGroup>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:CustomerAccount/ns2:customerGroup)}</ns1:customerGroup></ns1:CustomerAccount>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:actionSD)
                    then <ns1:actionSD>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:actionSD)}</ns1:actionSD>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:requestType)
                    then <ns1:requestType>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:requestType)}</ns1:requestType>
                    else ()
                }
                {
                    if ($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:statusReason)
                    then <ns1:statusReason>{fn:data($cambioEstadoEnCursoRq/ns2:Body/ns2:WorkOrder/ns2:statusReason)}</ns1:statusReason>
                    else ()
                }</ns1:WorkOrder>
        </ns1:Body>
    </ns1:CambiaEstadoEnCurso_REQ>
};

local:func($cambioEstadoEnCursoRq)