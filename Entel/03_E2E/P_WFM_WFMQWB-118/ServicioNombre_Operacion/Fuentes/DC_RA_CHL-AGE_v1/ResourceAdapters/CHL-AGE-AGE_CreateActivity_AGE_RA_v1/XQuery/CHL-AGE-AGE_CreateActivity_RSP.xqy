xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/CHL-AGE-AGE_CreateActivity_JSON_RSP_v1.xsd" ::)
declare namespace ns3="http://www.entel.cl/EBM/RA/AGE-AGE/CreateActivity/v1";
(:: import schema at "../CSC/CHL-AGE-AGE_CreateActivity_v1_CSM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Response as element() (:: schema-element(ns1:Root-Element) ::) external;
declare variable $Result as element() (:: schema-element(ns2:Result) ::) external;

declare function local:func($Response as element() (:: schema-element(ns1:Root-Element) ::), 
                            $Result as element() (:: schema-element(ns2:Result) ::)) 
                            as element() (:: schema-element(ns3:CreateActivity_RSP) ::) {
    <ns3:CreateActivity_RSP>
        <ns4:ResponseHeader>
            <ns4:Consumer sysCode="" enterpriseCode="" countryCode="">
            </ns4:Consumer>
            <ns4:Trace clientReqTimestamp="" eventID="">
                <ns4:Service>
                </ns4:Service>
            </ns4:Trace>
            <ns4:Channel>
            </ns4:Channel>
            <ns2:Result status="{fn:data($Result/@status)}">
                {
                    if ($Result/@description)
                    then attribute description {fn:data($Result/@description)}
                    else ()
                }
                {
                    if ($Result/ns5:CanonicalError)
                    then 
                        <ns5:CanonicalError>
                            {
                                if ($Result/ns5:CanonicalError/@code)
                                then attribute code {fn:data($Result/ns5:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($Result/ns5:CanonicalError/@description)
                                then attribute description {fn:data($Result/ns5:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($Result/ns5:CanonicalError/@type)
                                then attribute type {fn:data($Result/ns5:CanonicalError/@type)}
                                else ()
                            }
                        </ns5:CanonicalError>
                    else ()
                }
                {
                    if ($Result/ns5:SourceError)
                    then 
                        <ns5:SourceError>
                            {
                                if ($Result/ns5:SourceError/@code)
                                then attribute code {fn:data($Result/ns5:SourceError/@code)}
                                else ()
                            }
                            {
                                if ($Result/ns5:SourceError/@description)
                                then attribute description {fn:data($Result/ns5:SourceError/@description)}
                                else ()
                            }
                            <ns5:ErrorSourceDetails>
                                {
                                    if ($Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)
                                    then attribute source {fn:data($Result/ns5:SourceError/ns5:ErrorSourceDetails/@source)}
                                    else ()
                                }
                                {
                                    if ($Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)
                                    then attribute details {fn:data($Result/ns5:SourceError/ns5:ErrorSourceDetails/@details)}
                                    else ()
                                }
                            </ns5:ErrorSourceDetails>
                            {
                                if ($Result/ns5:SourceError/ns5:SourceFault)
                                then <ns5:SourceFault>{fn:data($Result/ns5:SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                else ()
                            }
                        </ns5:SourceError>
                    else ()
                }
                {
                    if ($Result/ns2:CorrelativeErrors)
                    then 
                        <ns2:CorrelativeErrors>
                            {
                                for $SourceError in $Result/ns2:CorrelativeErrors/ns5:SourceError
                                return 
                                <ns5:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns5:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns5:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns5:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns5:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns5:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns5:SourceFault)
                                        then <ns5:SourceFault>{fn:data($SourceError/ns5:SourceFault)}</ns5:SourceFault>
                                        else ()
                                    }
                                </ns5:SourceError>
                            }
                        </ns2:CorrelativeErrors>
                    else ()
                }
            </ns2:Result>
        </ns4:ResponseHeader>
        <ns3:Body>
            <ns3:WorkOrder>
                {
                    if ($Response/ns1:A_ANNULMENTTIME)
                    then <ns3:annulmentTime>{fn:data($Response/ns1:A_ANNULMENTTIME)}</ns3:annulmentTime>
                    else ()
                }
                {
                    if ($Response/ns1:date)
                    then <ns3:appointmentDate>{fn:data($Response/ns1:date)}</ns3:appointmentDate>
                    else ()
                }
                {
                    if ($Response/ns1:A_APPOINTMENTTIMES)
                    then <ns3:appointmentTimes>{fn:data($Response/ns1:A_APPOINTMENTTIMES)}</ns3:appointmentTimes>
                    else ()
                }
                {
                    if ($Response/ns1:A_ASSIGNMENTTIME)
                    then <ns3:assignmentTime>{fn:data($Response/ns1:A_ASSIGNMENTTIME)}</ns3:assignmentTime>
                    else ()
                }
                {
                    if ($Response/ns1:A_BROADBAND_IND)
                    then <ns3:broadbandIndicator>{fn:data($Response/ns1:A_BROADBAND_IND)}</ns3:broadbandIndicator>
                    else ()
                }
                {
                    if ($Response/ns1:A_COMMERCIAL_CONTACT_CELL)
                    then <ns3:cellComercial>{fn:data($Response/ns1:A_COMMERCIAL_CONTACT_CELL)}</ns3:cellComercial>
                    else ()
                }
                <ns3:contactName></ns3:contactName>
                {
                    if ($Response/ns1:A_CONTRACTOR)
                    then <ns3:contractor>{fn:data($Response/ns1:A_CONTRACTOR)}</ns3:contractor>
                    else ()
                }
                {
                    if ($Response/ns1:A_CREATEDDATE)
                    then <ns3:createdDate>{fn:data($Response/ns1:A_CREATEDDATE)}</ns3:createdDate>
                    else ()
                }
                <ns3:deliveryImplementacionDate></ns3:deliveryImplementacionDate>
                <ns3:depReg></ns3:depReg>
                {
                    if ($Response/ns1:A_COMMERCIAL_CONTACT_EMAIL)
                    then <ns3:emailComercial>{fn:data($Response/ns1:A_COMMERCIAL_CONTACT_EMAIL)}</ns3:emailComercial>
                    else ()
                }
                <ns3:emissionDate></ns3:emissionDate>
                {
                    if ($Response/ns1:endTime)
                    then <ns3:endTime>{fn:data($Response/ns1:endTime)}</ns3:endTime>
                    else ()
                }
                <ns3:entryDate></ns3:entryDate>
                <ns3:estimatedFinishDate></ns3:estimatedFinishDate>
                {
                    if ($Response/ns1:apptNumber)
                    then <ns3:externalID>{fn:data($Response/ns1:apptNumber)}</ns3:externalID>
                    else ()
                }
                {
                    if ($Response/ns1:A_NOTES)
                    then <ns3:externalNotes>{fn:data($Response/ns1:A_NOTES)}</ns3:externalNotes>
                    else ()
                }
                {
                    if ($Response/ns1:A_LEGACY_STATUS)
                    then <ns3:externalStatus>{fn:data($Response/ns1:A_LEGACY_STATUS)}</ns3:externalStatus>
                    else ()
                }
                {
                    if ($Response/ns1:A_LEGACY_WORKTYPE)
                    then <ns3:externalType>{fn:data($Response/ns1:A_LEGACY_WORKTYPE)}</ns3:externalType>
                    else ()
                }
                {
                    if ($Response/ns1:A_ZONE)
                    then <ns3:externalZone>{fn:data($Response/ns1:A_ZONE)}</ns3:externalZone>
                    else ()
                }
                {
                    if ($Response/ns1:A_FINISHTIME)
                    then <ns3:finishTime>{fn:data($Response/ns1:A_FINISHTIME)}</ns3:finishTime>
                    else ()
                }
                {
                    if ($Response/ns1:activityId)
                    then <ns3:ID>{fn:data($Response/ns1:activityId)}</ns3:ID>
                    else ()
                }
                {
                    if ($Response/ns1:A_INSTALLATIONCOST)
                    then <ns3:installationCost>{fn:data($Response/ns1:A_INSTALLATIONCOST)}</ns3:installationCost>
                    else ()
                }
                {
                    if ($Response/ns1:A_LEGACY_BO_USERNAME)
                    then <ns3:legacyBOUserName>{fn:data($Response/ns1:A_LEGACY_BO_USERNAME)}</ns3:legacyBOUserName>
                    else ()
                }
                <ns3:nameComercial></ns3:nameComercial>
                <ns3:networkResourceID></ns3:networkResourceID>
                {
                    if ($Response/ns1:A_NETWORK_SPEED)
                    then <ns3:networkSpeed>{fn:data($Response/ns1:A_NETWORK_SPEED)}</ns3:networkSpeed>
                    else ()
                }
                {
                    if ($Response/ns1:A_NETWORK_TYPE)
                    then <ns3:networkType>{fn:data($Response/ns1:A_NETWORK_TYPE)}</ns3:networkType>
                    else ()
                }
                {
                    if ($Response/ns1:A_OFT_CLOSE_REASON)
                    then <ns3:OFTcloseReason>{fn:data($Response/ns1:A_OFT_CLOSE_REASON)}</ns3:OFTcloseReason>
                    else ()
                }
                {
                    if ($Response/ns1:A_OFT_CLOSE_TYPE)
                    then <ns3:OFTcloseType>{fn:data($Response/ns1:A_OFT_CLOSE_TYPE)}</ns3:OFTcloseType>
                    else ()
                }
                {
                    if ($Response/ns1:A_OFT_SUMMARY)
                    then <ns3:OFTsummary>{fn:data($Response/ns1:A_OFT_SUMMARY)}</ns3:OFTsummary>
                    else ()
                }
                <ns3:phoneComercial></ns3:phoneComercial>
                {
                    if ($Response/ns1:recordType)
                    then <ns3:recordType>{fn:data($Response/ns1:recordType)}</ns3:recordType>
                    else ()
                }
                {
                    if ($Response/ns1:A_REQUESTED_TYPE)
                    then <ns3:requestType>{fn:data($Response/ns1:A_REQUESTED_TYPE)}</ns3:requestType>
                    else ()
                }
                {
                    if ($Response/ns1:A_RESCHEDULE_REQUESTER)
                    then <ns3:rescheduleRequester>{fn:data($Response/ns1:A_RESCHEDULE_REQUESTER)}</ns3:rescheduleRequester>
                    else ()
                }
                {
                    if ($Response/ns1:A_SALES_CHANNEL_ID)
                    then <ns3:salesChannelID>{fn:data($Response/ns1:A_SALES_CHANNEL_ID)}</ns3:salesChannelID>
                    else ()
                }
                {
                    if ($Response/ns1:A_SALES_CHANNEL_TYPE)
                    then <ns3:salesChannelType>{fn:data($Response/ns1:A_SALES_CHANNEL_TYPE)}</ns3:salesChannelType>
                    else ()
                }
                {
                    if ($Response/ns1:A_SCHEDULER_USERNAME)
                    then <ns3:scheduleUserName>{fn:data($Response/ns1:A_SCHEDULER_USERNAME)}</ns3:scheduleUserName>
                    else ()
                }
                {
                    if ($Response/ns1:A_SEGMENT)
                    then <ns3:segment>{fn:data($Response/ns1:A_SEGMENT)}</ns3:segment>
                    else ()
                }
                {
                    if ($Response/ns1:A_SERVICE_ID)
                    then <ns3:serviceID>{fn:data($Response/ns1:A_SERVICE_ID)}</ns3:serviceID>
                    else ()
                }
                {
                    if ($Response/ns1:A_SERVICE)
                    then <ns3:serviceName>{fn:data($Response/ns1:A_SERVICE)}</ns3:serviceName>
                    else ()
                }
                {
                    if ($Response/ns1:A_SERVICE_TEMPLATE)
                    then <ns3:serviceTemplate>{fn:data($Response/ns1:A_SERVICE_TEMPLATE)}</ns3:serviceTemplate>
                    else ()
                }
                {
                    if ($Response/ns1:A_SERVICE_TYPE)
                    then <ns3:serviceType>{fn:data($Response/ns1:A_SERVICE_TYPE)}</ns3:serviceType>
                    else ()
                }
                <ns3:serviceTypeSIAC></ns3:serviceTypeSIAC>
                {
                    if ($Response/ns1:serviceWindowEnd)
                    then <ns3:serviceWindowEnd>{fn:data($Response/ns1:serviceWindowEnd)}</ns3:serviceWindowEnd>
                    else ()
                }
                {
                    if ($Response/ns1:serviceWindowStart)
                    then <ns3:serviceWindowStart>{fn:data($Response/ns1:serviceWindowStart)}</ns3:serviceWindowStart>
                    else ()
                }
                {
                    if ($Response/ns1:A_PRIORITY)
                    then <ns3:severityProblem>{fn:data($Response/ns1:A_PRIORITY)}</ns3:severityProblem>
                    else ()
                }
                <ns3:signatureDate></ns3:signatureDate>
                {
                    if ($Response/ns1:slaWindowEnd)
                    then <ns3:slaWindowEnd>{fn:data($Response/ns1:slaWindowEnd)}</ns3:slaWindowEnd>
                    else ()
                }
                {
                    if ($Response/ns1:slaWindowStart)
                    then <ns3:slaWindowStart>{fn:data($Response/ns1:slaWindowStart)}</ns3:slaWindowStart>
                    else ()
                }
                {
                    if ($Response/ns1:A_SOURCE_SYSTEM)
                    then <ns3:sourceSystem>{fn:data($Response/ns1:A_SOURCE_SYSTEM)}</ns3:sourceSystem>
                    else ()
                }
                {
                    if ($Response/ns1:travelTime)
                    then <ns3:standardDrivingTime>{fn:data($Response/ns1:travelTime)}</ns3:standardDrivingTime>
                    else ()
                }
                {
                    if ($Response/ns1:duration)
                    then <ns3:standardDuration>{fn:data($Response/ns1:duration)}</ns3:standardDuration>
                    else ()
                }
                <ns3:startTime></ns3:startTime>
                {
                    if ($Response/ns1:status)
                    then <ns3:status>{fn:data($Response/ns1:status)}</ns3:status>
                    else ()
                }
                <ns3:timeOfAssignment></ns3:timeOfAssignment>
                {
                    if ($Response/ns1:timeOfBooking)
                    then <ns3:timeOfBooking>{fn:data($Response/ns1:timeOfBooking)}</ns3:timeOfBooking>
                    else ()
                }
                {
                    if ($Response/ns1:A_TROUBLE_ACTION)
                    then <ns3:troubleAction>{fn:data($Response/ns1:A_TROUBLE_ACTION)}</ns3:troubleAction>
                    else ()
                }
                {
                    if ($Response/ns1:A_TROUBLE_DESCRIPTION)
                    then <ns3:troubleDescription>{fn:data($Response/ns1:A_TROUBLE_DESCRIPTION)}</ns3:troubleDescription>
                    else ()
                }
                {
                    if ($Response/ns1:A_TROUBLE_DIAGNOSIS)
                    then <ns3:troubleDiagnosis>{fn:data($Response/ns1:A_TROUBLE_DIAGNOSIS)}</ns3:troubleDiagnosis>
                    else ()
                }
                {
                    if ($Response/ns1:activityType)
                    then <ns3:type>{fn:data($Response/ns1:activityType)}</ns3:type>
                    else ()
                }
                <ns3:absoluteLocalLocation>
                    {
                        if ($Response/ns1:latitude)
                        then <ns3:X>{fn:data($Response/ns1:latitude)}</ns3:X>
                        else ()
                    }
                    {
                        if ($Response/ns1:longitude)
                        then <ns3:Y>{fn:data($Response/ns1:longitude)}</ns3:Y>
                        else ()
                    }
                    <ns3:timezone>
                        {
                            if ($Response/ns1:timeZone)
                            then <ns3:name>{fn:data($Response/ns1:timeZone)}</ns3:name>
                            else ()
                        }
                        {
                            if ($Response/ns1:timeZoneIANA)
                            then <ns3:timeZoneIANA>{fn:data($Response/ns1:timeZoneIANA)}</ns3:timeZoneIANA>
                            else ()
                        }
                    </ns3:timezone>
                </ns3:absoluteLocalLocation>
                <ns3:address>
                    {
                        if ($Response/ns1:A_COMUNA)
                        then <ns3:commune>{fn:data($Response/ns1:A_COMUNA)}</ns3:commune>
                        else ()
                    }
                    {
                        if ($Response/ns1:city)
                        then <ns3:city>{fn:data($Response/ns1:city)}</ns3:city>
                        else ()
                    }
                    {
                        if ($Response/ns1:stateProvince)
                        then <ns3:region>{fn:data($Response/ns1:stateProvince)}</ns3:region>
                        else ()
                    }
                    {
                        if ($Response/ns1:streetAddress)
                        then <ns3:streetName>{fn:data($Response/ns1:streetAddress)}</ns3:streetName>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_ADDRESS_REFERENCE)
                        then <ns3:addressReference>{fn:data($Response/ns1:A_ADDRESS_REFERENCE)}</ns3:addressReference>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_BUILDING_TYPE)
                        then <ns3:buildingType>{fn:data($Response/ns1:A_BUILDING_TYPE)}</ns3:buildingType>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_COMPLENTARY_ADDRESS)
                        then <ns3:complentaryAddress>{fn:data($Response/ns1:A_COMPLENTARY_ADDRESS)}</ns3:complentaryAddress>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_DEPARTAMENT)
                        then <ns3:department>{fn:data($Response/ns1:A_DEPARTAMENT)}</ns3:department>
                        else ()
                    }
                </ns3:address>
                <ns3:customerAccount>
                    {
                        if ($Response/ns1:A_ALIAS)
                        then <ns3:alias>{fn:data($Response/ns1:A_ALIAS)}</ns3:alias>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_GROUP)
                        then <ns3:customerGroup>{fn:data($Response/ns1:A_GROUP)}</ns3:customerGroup>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_PORTABILITY)
                        then <ns3:portabilityIndicator>{fn:data($Response/ns1:A_PORTABILITY)}</ns3:portabilityIndicator>
                        else ()
                    }
                    <ns3:contact>
                        <ns3:alternativePhoneNumber>
                            <ns3:number></ns3:number>
                        </ns3:alternativePhoneNumber>
                        <ns3:cellPhone>
                            <ns3:number></ns3:number>
                        </ns3:cellPhone>
                        <ns3:email>
                            <ns3:eMailAddress>{fn:data($Response/ns1:customerEmail)}</ns3:eMailAddress>
                        </ns3:email>
                        <ns3:IndividualIdentification>
                            <ns3:number>{fn:data($Response/ns1:customerNumber)}</ns3:number>
                            <ns3:type></ns3:type>
                        </ns3:IndividualIdentification>
                        <ns3:IndividualName>
                            <ns3:formatedName>{fn:data($Response/ns1:customerName)}</ns3:formatedName>
                        </ns3:IndividualName>
                    </ns3:contact>
                </ns3:customerAccount>
                <ns3:Language>
                    <ns3:alphabetName>{fn:data($Response/ns1:language)}</ns3:alphabetName>
                </ns3:Language>
                <ns3:PartyResource>
                    {
                        if ($Response/ns1:resourceId)
                        then <ns3:ID>{fn:data($Response/ns1:resourceId)}</ns3:ID>
                        else ()
                    }
                    {
                        if ($Response/ns1:resourceInternalId)
                        then <ns3:internalID>{fn:data($Response/ns1:resourceInternalId)}</ns3:internalID>
                        else ()
                    }
                    {
                        if ($Response/ns1:resourceTimeZone)
                        then <ns3:resourceTimeZone>{fn:data($Response/ns1:resourceTimeZone)}</ns3:resourceTimeZone>
                        else ()
                    }
                    {
                        if ($Response/ns1:resourceTimeZoneDiff)
                        then <ns3:UTCOffset>{fn:data($Response/ns1:resourceTimeZoneDiff)}</ns3:UTCOffset>
                        else ()
                    }
                    {
                        if ($Response/ns1:resourceTimeZoneIANA)
                        then <ns3:ResourceTimeZoneIANA>{fn:data($Response/ns1:resourceTimeZoneIANA)}</ns3:ResourceTimeZoneIANA>
                        else ()
                    }
                    <ns3:IndividualIdentification>
                        <ns3:number></ns3:number>
                        <ns3:type></ns3:type>
                    </ns3:IndividualIdentification>
                </ns3:PartyResource>
                <ns3:Product>
                    {
                        if ($Response/ns1:A_ANIS_EQUIP_QTY)
                        then <ns3:ANISQuantity>{fn:data($Response/ns1:A_ANIS_EQUIP_QTY)}</ns3:ANISQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_ANIS_EQUIP_QTY_REQ)
                        then <ns3:ANISQuantityRequired>{fn:data($Response/ns1:A_ANIS_EQUIP_QTY_REQ)}</ns3:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_CATEGORY)
                        then <ns3:Category>{fn:data($Response/ns1:A_CATEGORY)}</ns3:Category>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_DECO_QTY)
                        then <ns3:DECOQuantity>{fn:data($Response/ns1:A_DECO_QTY)}</ns3:DECOQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_EQUIP_QTY_REQ)
                        then <ns3:DECOQuantityRequired>{fn:data($Response/ns1:A_EQUIP_QTY_REQ)}</ns3:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_EQUIPMENTS)
                        then <ns3:Equipment>{fn:data($Response/ns1:A_EQUIPMENTS)}</ns3:Equipment>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_LAYER_MPLS)
                        then <ns3:layerMPLS>{fn:data($Response/ns1:A_LAYER_MPLS)}</ns3:layerMPLS>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_NGN_MODEL)
                        then <ns3:ngnModel>{fn:data($Response/ns1:A_NGN_MODEL)}</ns3:ngnModel>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_PHONE_EQUIP_QTY)
                        then <ns3:PHONEQuantity>{fn:data($Response/ns1:A_PHONE_EQUIP_QTY)}</ns3:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_PRODUCT_CODE)
                        then <ns3:externalProductCode>{fn:data($Response/ns1:A_PRODUCT_CODE)}</ns3:externalProductCode>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_PRODUCT_DESC)
                        then <ns3:externalProductDescription>{fn:data($Response/ns1:A_PRODUCT_DESC)}</ns3:externalProductDescription>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_STB_EQUIP_QTY)
                        then <ns3:STBQuantity>{fn:data($Response/ns1:A_STB_EQUIP_QTY)}</ns3:STBQuantity>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_VOICE_IND)
                        then <ns3:VoiceIndicator>{fn:data($Response/ns1:A_VOICE_IND)}</ns3:VoiceIndicator>
                        else ()
                    }
                    {
                        if ($Response/ns1:A_WIFI_EQUIP_QTY)
                        then <ns3:WIFIExtensorQuantity>{fn:data($Response/ns1:A_WIFI_EQUIP_QTY)}</ns3:WIFIExtensorQuantity>
                        else ()
                    }
                </ns3:Product>
                <ns3:linkedActivities>
                    {
                        for $linkedActivities in $Response/ns1:linkedActivities
                        return 
                        <ns3:links>
                            <ns3:rel>{fn:data($linkedActivities/ns1:links/ns1:rel)}</ns3:rel>
                            <ns3:href>{fn:data($linkedActivities/ns1:links/ns1:href)}</ns3:href></ns3:links>
                    }
                </ns3:linkedActivities>
                <ns3:linkList>
                    {
                        for $links in $Response/ns1:links
                        return 
                        <ns3:links>
                            <ns3:rel>{fn:data($links/ns1:rel)}</ns3:rel>
                            <ns3:href>{fn:data($links/ns1:href)}</ns3:href></ns3:links>
                    }</ns3:linkList>
                <ns3:RequiredInventories>
                    {
                        for $requiredInventories in $Response/ns1:requiredInventories
                        return 
                        <ns3:links>
                            <ns3:rel>{fn:data($requiredInventories/ns1:links/ns1:rel)}</ns3:rel>
                            <ns3:href>{fn:data($requiredInventories/ns1:links/ns1:href)}</ns3:href></ns3:links>
                    }
                </ns3:RequiredInventories>
                <ns3:ResourcePreferences>
                    {
                        for $resourcePreferences in $Response/ns1:resourcePreferences
                        return 
                        <ns3:links>
                            <ns3:rel>{fn:data($resourcePreferences/ns1:links/ns1:rel)}</ns3:rel>
                            <ns3:href>{fn:data($resourcePreferences/ns1:links/ns1:href)}</ns3:href></ns3:links>
                    }
                </ns3:ResourcePreferences>
                <ns3:workSkill>
                    {
                        for $workSkills in $Response/ns1:workSkills
                        return 
                        <ns3:links>
                            <ns3:rel>{fn:data($workSkills/ns1:links/ns1:rel)}</ns3:rel>
                            <ns3:href>{fn:data($workSkills/ns1:links/ns1:href)}</ns3:href></ns3:links>
                    }
                </ns3:workSkill>
            </ns3:WorkOrder>
        </ns3:Body>
    </ns3:CreateActivity_RSP>
};

local:func($Response, $Result)