xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/NotificarDesplazamientoTecnico/Send/v1";
(:: import schema at "../CSC/NotificarDesplazamientoTecnico_v1_EBM.xsd" ::)
declare namespace ns2="http://www.esa.com/Provision/OrderingServ/T/enCamino";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OTC-OTC/WSDL/EnCaminoWSService/enCaminoWSService1.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:NotificarDesplazamientoTecnico_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:NotificarDesplazamientoTecnico_REQ) ::)) as element() (:: schema-element(ns2:getEnCamino) ::) {
    <ns2:getEnCamino>
        <arg0>
            <notificacionOFSC>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:requestType)}</notificacionOFSC>
            <idWO>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:ID)}</idWO>
            <numeroOXT></numeroOXT>
            <nombreTecnico>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:pname)}</nombreTecnico>
            <rutTecnico>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:name)}</rutTecnico>
            <enCamino>{fn:data($Request/ns1:Body/ns1:WorkOrder/ns1:startedTravel)}</enCamino>
        </arg0>
    </ns2:getEnCamino>
};

local:func($Request)