xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/SuspenderActividad/Send/v1";
(:: import schema at "../CSC/SuspenderActividad_v1_EBM.xsd" ::)
declare namespace ns2="http://www.esa.com/Provision/OrderingServ/T/cancelarTarea";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-OTC-OTC/WSDL/CancelaTareaWSService/cancelaTareaWSService1.xsd" ::)

declare variable $Request as element() (:: schema-element(ns1:SuspenderActividad_REQ) ::) external;

declare function local:func($Request as element() (:: schema-element(ns1:SuspenderActividad_REQ) ::)) as element() (:: schema-element(ns2:getCancelaTarea) ::) {
    <ns2:getCancelaTarea>
        {
            if ($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0)
            then <arg0>
                <notificacionOFSC>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:notificacionOFSC)}</notificacionOFSC>
                <idWO>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:idWO)}</idWO>
                <numeroOXT>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:numeroOXT)}</numeroOXT>
                <tipoTermino>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:tipoTermino)}</tipoTermino>
                <causaTermino>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:causaTermino)}</causaTermino>
                <observacionesTecnico>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:observacionesTecnico)}</observacionesTecnico>
                <estadoWO>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:estadoWO)}</estadoWO>
                <loginOFSC>{fn:data($Request/ns1:Body/ns1:getCancelaTarea/ns1:arg0/ns1:loginOFSC)}</loginOFSC></arg0>
            else ()
        }
    </ns2:getCancelaTarea>
};

local:func($Request)