xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../CSC/cancel_CHL-OTC-OTC_CancelWO-OTC-API_REQ.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CancelWorkOrderLegacy/Cancel/v1";
(:: import schema at "../CSC/CHL-OTC-OTC_CancelWO-OTC-API_v1_EBM.xsd" ::)

declare variable $cancelWoReq as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::) external;

declare function local:func($cancelWoReq as element() (:: schema-element(ns1:CancelWorkOrderLegacy_REQ) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:notificacionOFSC>Cancelar_Tarea</ns2:notificacionOFSC>
        <ns2:idWO>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:ID)}</ns2:idWO>
        <ns2:numeroOXT>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:numeroOXT>
        <ns2:loginOFSC>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:BOUserName)}</ns2:loginOFSC>
        <ns2:observacionesTecnico>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:engineerNotes)}</ns2:observacionesTecnico>
        <ns2:tipoTermino>Terminado</ns2:tipoTermino>
        <ns2:causaTermino>Solicitud de anulacion</ns2:causaTermino>
        <ns2:estadoWO>{fn:data($cancelWoReq/ns1:Body/ns1:WorkOrder/ns1:status)}</ns2:estadoWO>
    </ns2:Root-Element>
};

local:func($cancelWoReq)