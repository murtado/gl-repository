xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/CSM/RA/OFC-OFC/CreateActivity/v1";
(:: import schema at "../../../../../../DC_RA_CHL-OFC_v1/ResourceAdapters/CHL-OFC-OFC_CreateActivity_RA_v1/CSC/CHL-OFC-OFC_CreateActivity_v1_CSM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/CreateWorkOrder/Create/v1";
(:: import schema at "../../../../../ESC/Primary/CreateWorkOrder_v1_EBM.xsd" ::)

declare variable $CreateWorkOrder_REQ as element() (:: schema-element(ns1:CreateWorkOrder_REQ) ::) external;

declare function local:func($CreateWorkOrder_REQ as element() (:: schema-element(ns1:CreateWorkOrder_REQ) ::)) as element() (:: schema-element(ns2:CreateActivity_REQ) ::) {
    <ns2:CreateActivity_REQ>
    {$CreateWorkOrder_REQ/*[1]}
        <ns2:Body>
            <ns2:WorkOrder>
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)
                    then <ns2:annulmentTime>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:annulmentTime)}</ns2:annulmentTime>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)
                    then <ns2:appointmentDate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:appointmentDate)}</ns2:appointmentDate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)
                    then <ns2:appointmentTimes>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:appointmentTimes)}</ns2:appointmentTimes>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)
                    then <ns2:assignmentTime>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:assignmentTime)}</ns2:assignmentTime>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)
                    then <ns2:broadbandIndicator>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:broadbandIndicator)}</ns2:broadbandIndicator>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:cellComercial)
                    then <ns2:cellComercial>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:cellComercial)}</ns2:cellComercial>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:contactName)
                    then <ns2:contactName>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:contactName)}</ns2:contactName>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:contractor)
                    then <ns2:contractor>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:contractor)}</ns2:contractor>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:createdDate)
                    then <ns2:createdDate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:createdDate)}</ns2:createdDate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)
                    then <ns2:deliveryImplementacionDate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:deliveryImplementacionDate)}</ns2:deliveryImplementacionDate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:depReg)
                    then <ns2:depReg>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:depReg)}</ns2:depReg>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:emailComercial)
                    then <ns2:emailComercial>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:emailComercial)}</ns2:emailComercial>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:emissionDate)
                    then <ns2:emissionDate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:emissionDate)}</ns2:emissionDate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:entryDate)
                    then <ns2:entryDate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:entryDate)}</ns2:entryDate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)
                    then <ns2:estimatedFinishDate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:estimatedFinishDate)}</ns2:estimatedFinishDate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalID)
                    then <ns2:externalID>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalID)}</ns2:externalID>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalNotes)
                    then <ns2:externalNotes>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalNotes)}</ns2:externalNotes>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalStatus)
                    then <ns2:externalStatus>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalStatus)}</ns2:externalStatus>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalType)
                    then <ns2:externalType>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalType)}</ns2:externalType>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalZone)
                    then <ns2:externalZone>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:externalZone)}</ns2:externalZone>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:finishTime)
                    then <ns2:finishTime>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:finishTime)}</ns2:finishTime>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:installationCost)
                    then <ns2:installationCost>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:installationCost)}</ns2:installationCost>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)
                    then <ns2:legacyBOUserName>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:legacyBOUserName)}</ns2:legacyBOUserName>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:nameComercial)
                    then <ns2:nameComercial>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:nameComercial)}</ns2:nameComercial>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)
                    then <ns2:networkResourceID>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:networkResourceID)}</ns2:networkResourceID>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)
                    then <ns2:networkSpeed>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:networkSpeed)}</ns2:networkSpeed>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:networkType)
                    then <ns2:networkType>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:networkType)}</ns2:networkType>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)
                    then <ns2:OFTcloseReason>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTcloseReason)}</ns2:OFTcloseReason>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)
                    then <ns2:OFTcloseType>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTcloseType)}</ns2:OFTcloseType>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)
                    then <ns2:OFTsummary>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:OFTsummary)}</ns2:OFTsummary>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)
                    then <ns2:phoneComercial>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:phoneComercial)}</ns2:phoneComercial>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:requestType)
                    then <ns2:requestType>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:requestType)}</ns2:requestType>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)
                    then <ns2:rescheduleRequester>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:rescheduleRequester)}</ns2:rescheduleRequester>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)
                    then <ns2:salesChannelID>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:salesChannelID)}</ns2:salesChannelID>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)
                    then <ns2:salesChannelType>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:salesChannelType)}</ns2:salesChannelType>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)
                    then <ns2:scheduleUserName>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:scheduleUserName)}</ns2:scheduleUserName>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:segment)
                    then <ns2:segment>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:segment)}</ns2:segment>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceID)
                    then <ns2:serviceID>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceID)}</ns2:serviceID>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceName)
                    then <ns2:serviceName>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceName)}</ns2:serviceName>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)
                    then <ns2:serviceTemplate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceTemplate)}</ns2:serviceTemplate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceType)
                    then <ns2:serviceType>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceType)}</ns2:serviceType>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)
                    then <ns2:serviceTypeSIAC>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:serviceTypeSIAC)}</ns2:serviceTypeSIAC>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:severityProblem)
                    then <ns2:severityProblem>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:severityProblem)}</ns2:severityProblem>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:signatureDate)
                    then <ns2:signatureDate>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:signatureDate)}</ns2:signatureDate>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)
                    then <ns2:slaWindowEnd>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:slaWindowEnd)}</ns2:slaWindowEnd>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)
                    then <ns2:slaWindowStart>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:slaWindowStart)}</ns2:slaWindowStart>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)
                    then <ns2:sourceSystem>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:sourceSystem)}</ns2:sourceSystem>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:startTime)
                    then <ns2:startTime>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:startTime)}</ns2:startTime>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleAction)
                    then <ns2:troubleAction>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleAction)}</ns2:troubleAction>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)
                    then <ns2:troubleDescription>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleDescription)}</ns2:troubleDescription>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)
                    then <ns2:troubleDiagnosis>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:troubleDiagnosis)}</ns2:troubleDiagnosis>
                    else ()
                }
                {
                    if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:type)
                    then <ns2:type>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:type)}</ns2:type>
                    else ()
                }
                {
                 if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule)
                 then 
                <ns2:WorkSchedule>
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)
                        then <ns2:applicableDuring>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:WorkSchedule/ns1:applicableDuring)}</ns2:applicableDuring>
                        else ()
                    }
                </ns2:WorkSchedule>
                else
                ()
                }
                {
                 if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation)
                 then 
                <ns2:absoluteLocalLocation>
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)
                        then <ns2:X>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:X)}</ns2:X>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)
                        then <ns2:Y>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:Y)}</ns2:Y>
                        else ()
                    }
                    <ns2:timezone>
                        {
                            if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)
                            then <ns2:name>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:name)}</ns2:name>
                            else ()
                        }
                        {
                            if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)
                            then <ns2:timeZoneIANA>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:absoluteLocalLocation/ns1:timezone/ns1:timeZoneIANA)}</ns2:timeZoneIANA>
                            else ()
                        }
                    </ns2:timezone>
                </ns2:absoluteLocalLocation>
                else
                ()
                }
                {
                 if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address)
                 then 
                <ns2:address>
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)
                        then <ns2:commune>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:commune)}</ns2:commune>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)
                        then <ns2:city>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:city)}</ns2:city>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)
                        then <ns2:region>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:region)}</ns2:region>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)
                        then <ns2:streetName>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:streetName)}</ns2:streetName>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)
                        then <ns2:addressReference>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:addressReference)}</ns2:addressReference>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)
                        then <ns2:buildingType>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:buildingType)}</ns2:buildingType>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)
                        then <ns2:complentaryAddress>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:complentaryAddress)}</ns2:complentaryAddress>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)
                        then <ns2:department>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:address/ns1:department)}</ns2:department>
                        else ()
                    }
                </ns2:address>
                else
                ()
                }
                {
                 if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount)
                 then 
                <ns2:customerAccount>
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)
                        then <ns2:alias>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:alias)}</ns2:alias>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)
                        then <ns2:customerGroup>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:customerGroup)}</ns2:customerGroup>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)
                        then <ns2:portabilityIndicator>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:portabilityIndicator)}</ns2:portabilityIndicator>
                        else ()
                    }
                    <ns2:contact>
                     {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)
                        then
                        <ns2:alternativePhoneNumber>
                            <ns2:number>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:alternativePhoneNumber/ns1:number)}</ns2:number>
                        </ns2:alternativePhoneNumber>
                        else
                        ()
                        }
                        {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone)
                        then
                        <ns2:cellPhone>
                            <ns2:number>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:cellPhone/ns1:number)}</ns2:number>
                        </ns2:cellPhone>
                        else
                        ()
                        }
                        {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email)
                        then
                        <ns2:email>
                            <ns2:eMailAddress>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:email/ns1:eMailAddress)}</ns2:eMailAddress>
                        </ns2:email>
                        else
                        ()
                        }
                        {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification)
                        then
                        <ns2:IndividualIdentification>
                            <ns2:number>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                            {
                                if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)
                                then <ns2:type>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualIdentification/ns1:type)}</ns2:type>
                                else ()
                            }
                        </ns2:IndividualIdentification>
                        else
                        ()
                        }
                        <ns2:IndividualName>
                            <ns2:formatedName>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:customerAccount/ns1:contact/ns1:IndividualName/ns1:formatedName)}</ns2:formatedName>
                        </ns2:IndividualName>
                    </ns2:contact>
                </ns2:customerAccount>
                else
                ()
                }
                <ns2:Language>
                    <ns2:alphabetName>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Language/ns1:alphabetName)}</ns2:alphabetName>
                </ns2:Language>
                <ns2:PartyResource>
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)
                        then <ns2:ID>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:ID)}</ns2:ID>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification)
                        then
                    <ns2:IndividualIdentification>
                        <ns2:number>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:number)}</ns2:number>
                        {
                            if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)
                            then <ns2:type>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:PartyResource/ns1:IndividualIdentification/ns1:type)}</ns2:type>
                            else ()
                        }
                    </ns2:IndividualIdentification>
                    else
                    ()
                    }
                </ns2:PartyResource>
                {
                 if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                 then 
                <ns2:Product>
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)
                        then <ns2:ANISQuantity>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantity)}</ns2:ANISQuantity>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)
                        then <ns2:ANISQuantityRequired>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ANISQuantityRequired)}</ns2:ANISQuantityRequired>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)
                        then <ns2:Category>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Category)}</ns2:Category>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)
                        then <ns2:DECOQuantity>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantity)}</ns2:DECOQuantity>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)
                        then <ns2:DECOQuantityRequired>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:DECOQuantityRequired)}</ns2:DECOQuantityRequired>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)
                        then <ns2:Equipment>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:Equipment)}</ns2:Equipment>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)
                        then <ns2:layerMPLS>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:layerMPLS)}</ns2:layerMPLS>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)
                        then <ns2:ngnModel>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:ngnModel)}</ns2:ngnModel>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)
                        then <ns2:PHONEQuantity>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:PHONEQuantity)}</ns2:PHONEQuantity>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)
                        then <ns2:externalProductCode>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductCode)}</ns2:externalProductCode>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)
                        then <ns2:externalProductDescription>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:externalProductDescription)}</ns2:externalProductDescription>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)
                        then <ns2:STBQuantity>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:STBQuantity)}</ns2:STBQuantity>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)
                        then <ns2:VoiceIndicator>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:VoiceIndicator)}</ns2:VoiceIndicator>
                        else ()
                    }
                    {
                        if ($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)
                        then <ns2:WIFIExtensorQuantity>{fn:data($CreateWorkOrder_REQ/ns1:Body/ns1:WorkOrder/ns1:Product/ns1:WIFIExtensorQuantity)}</ns2:WIFIExtensorQuantity>
                        else ()
                    }
                </ns2:Product>
                else
                ()
                }
            </ns2:WorkOrder>
        </ns2:Body>
    </ns2:CreateActivity_REQ>
};

local:func($CreateWorkOrder_REQ)