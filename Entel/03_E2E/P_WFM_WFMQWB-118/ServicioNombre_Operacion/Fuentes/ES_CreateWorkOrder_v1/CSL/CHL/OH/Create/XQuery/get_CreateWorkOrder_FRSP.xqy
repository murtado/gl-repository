xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/CreateWorkOrder/Create/v1";
(:: import schema at "../../../../../ESC/Primary/CreateWorkOrder_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare variable $ResponseHeader as element() (:: schema-element(ns1:CreateWorkOrder_RSP) ::) external;

declare function local:func($ResponseHeader as element() (:: schema-element(ns1:CreateWorkOrder_RSP) ::)) as element() (:: schema-element(ns1:CreateWorkOrder_FRSP) ::) {
    <ns1:CreateWorkOrder_FRSP>
        {$ResponseHeader}
        <ns2:Body></ns2:Body>
    </ns1:CreateWorkOrder_FRSP>
};

local:func($ResponseHeader)