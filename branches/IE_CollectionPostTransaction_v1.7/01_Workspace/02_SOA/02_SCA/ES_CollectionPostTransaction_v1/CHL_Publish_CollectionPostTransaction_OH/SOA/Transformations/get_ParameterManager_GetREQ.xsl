<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:tns="http://www.entel.cl/SC/ParameterManager/get/v1" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns0="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xml:id="id_1" oraxsl:ignorexmlids="true"
                xmlns:ns6="http://www.entel.cl/ESC/AppliedCustomerBillingCharge/v1"
                xmlns:ns1="http://www.entel.cl/EBO/StockTransaction/v1"
                xmlns:ns7="http://www.entel.cl/EBO/CustomerOrder/v1"
                xmlns:ns8="http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1"
                xmlns:ns9="http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1"
                xmlns:ns10="http://www.entel.cl/EBO/Quantity/v1" xmlns:ns11="http://www.entel.cl/EBO/ReceiverCompany/v1"
                xmlns:ns12="http://www.entel.cl/EBO/EmailContact/v1"
                xmlns:ns13="http://www.entel.cl/EBO/GeographicAddress/v1" xmlns:ns14="http://www.entel.cl/EBO/Money/v1"
                xmlns:ns15="http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1"
                xmlns:ns16="http://www.entel.cl/EBO/PostalDeliveryAddress/v1"
                xmlns:ns3="http://www.entel.cl/SC/ParameterManager/v1"
                xmlns:ns17="http://www.entel.cl/EBO/CustomerBill/v1"
                xmlns:ns18="http://www.entel.cl/EBO/PhysicalResourceSpec/v1"
                xmlns:ns19="http://www.entel.cl/EBO/PaymentMethod/v1"
                xmlns:ns20="http://www.entel.cl/EBO/LoyaltyTransaction/v1"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:ns21="http://www.entel.cl/EBO/Contact/v1" xmlns:ns22="http://www.entel.cl/EBO/GeographicArea/v1"
                xmlns:ns23="http://www.entel.cl/EBO/MSISDN/v1" xmlns:ns5="http://schemas.oracle.com/bpel/extension"
                xmlns:ns2="http://www.entel.cl/ESC/PaymentOrder/v1" xmlns:ns24="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:ns25="http://www.entel.cl/ESO/Error/v1" xmlns:client="http://xmlns.oracle.com/ES/OH/BPELProcess"
                xmlns:ns26="http://www.entel.cl/EBO/AppliedCustomerBillingRate/v1"
                xmlns:ns27="http://www.entel.cl/EBO/IndividualIdentification/v1"
                xmlns:ns28="http://www.entel.cl/EBO/BillingAccount/v1"
                xmlns:ns29="http://www.entel.cl/EBO/SalesChannel/v1"
                xmlns:ns30="http://www.entel.cl/EBO/CustomerPayment/v1" xmlns:ns31="http://www.entel.cl/EBO/Voucher/v1"
                xmlns:ns32="http://www.entel.cl/EBO/Asset/v1" xmlns:ns33="http://www.entel.cl/EBO/CustomerAccount/v1"
                xmlns:ns34="http://www.entel.cl/EBO/IssuingCompany/v1"
                xmlns:ns35="http://www.entel.cl/EBO/TelephoneNumber/v1"
                xmlns:ns36="http://www.entel.cl/EBO/PartyRole/v1" xmlns:ns37="http://www.entel.cl/EBO/Customer/v1"
                xmlns:ns38="http://www.entel.cl/EBO/PhysicalResource/v1"
                xmlns:ns39="http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1"
                xmlns:ns40="http://www.entel.cl/EBO/OrganizationName/v1"
                xmlns:ns41="http://www.entel.cl/EBO/CustomerAddress/v1"
                xmlns:ns4="http://www.entel.cl/SC/ConversationManager/v1"
                xmlns:ns42="http://www.entel.cl/EBO/PartyIdentification/v1"
                xmlns:ns43="http://www.entel.cl/EBO/IndividualName/v1" xmlns:ns44="http://www.entel.cl/ESO/Result/v2"
                xmlns:ns45="http://www.entel.cl/EBO/Skill/v1" xmlns:ns46="http://www.entel.cl/ESO/MessageHeader/v1"
                xmlns:ns47="http://www.entel.cl/EBO/CustomerOrderItem/v1"
                xmlns:ns48="http://www.entel.cl/EBO/Organization/v1" xmlns:ns49="http://www.entel.cl/EBO/StreetName/v1"
                xmlns:ns50="http://www.entel.cl/EBO/TimePeriod/v1" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ns51="http://www.entel.cl/EBO/Price/v1" xmlns:ns52="http://www.entel.cl/EBO/Address/v1"
                xmlns:ns53="http://www.entel.cl/EBO/Store/v1"
                xmlns:ns54="http://www.entel.cl/ESO/EndpointConfiguration/v1"
                xmlns:rcGet="http://www.entel.cl/SC/ParameterManager/RefreshCache_Get/v1"
                xmlns:rcGetM="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetMapping/v1"
                xmlns:rc="http://www.entel.cl/SC/ParameterManager/RefreshCache/v1"
                xmlns:getC="http://www.entel.cl/SC/ParameterManager/getConfig/v1"
                xmlns:getM="http://www.entel.cl/SC/ParameterManager/getMapping/v1"
                xmlns:getE="http://www.entel.cl/SC/ParameterManager/getEndpoint/v1"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:rcGetC="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetConfig/v1"
                xmlns:rcGetE="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetEndpoint/v1"
                xmlns:ns55="http://www.entel.cl/ESC/CustomerBill/v1"
                xmlns:ns56="http://www.entel.cl/ESC/PhysicalResource/v1"
                xmlns:ns58="http://www.entel.cl/ESC/AppliedCustomerBillingCredit/v1"
                xmlns:ns59="http://www.entel.cl/EBO/Rate/v1" xmlns:ns60="http://www.entel.cl/EBO/Individual/v1"
                xmlns:ns61="http://www.entel.cl/EBO/AuthenticationEntity/v1"
                xmlns:ns57="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1"
                xmlns:ns62="http://www.entel.cl/EBO/PaymentItem/v1"
                xmlns:ns63="http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1"
                xmlns:ns64="http://www.entel.cl/EBO/CustomerAccountBalance/v1"
                xmlns:ns65="http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1"
                xmlns:ns66="http://www.entel.cl/EBO/CustomerComercialAddress/v1"
                xmlns:ns67="http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1"
                xmlns:ns68="http://www.entel.cl/EBO/PlaceOfBirth/v1"
                xmlns:ns69="http://www.entel.cl/EBO/FinancialChargeSpec/v1"
                xmlns:ns70="http://www.entel.cl/EBO/Country/v1">
   <oracle-xsl-mapper:schema xml:id="id_2">
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources xml:id="id_3">
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_4">
            <oracle-xsl-mapper:schema location="../WSDLs/BPELProcess.wsdl" xml:id="id_5"/>
            <oracle-xsl-mapper:rootElement name="Publish_CollectionPostTransaction_REQ" namespace="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xml:id="id_6"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets xml:id="id_7">
         <oracle-xsl-mapper:target type="WSDL" xml:id="id_8">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_ParameterManager/SupportAPI/WSDL/ParameterManager.wsdl" xml:id="id_9"/>
            <oracle-xsl-mapper:rootElement name="GetREQ" namespace="http://www.entel.cl/SC/ParameterManager/get/v1" xml:id="id_10"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [WED DEC 28 18:25:03 ART 2016].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/" xml:id="id_11">
      <tns:GetREQ xml:id="id_12">
         <ns46:RequestHeader xml:id="id_13">
            <ns46:Consumer sysCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Consumer/@sysCode}"
                           enterpriseCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Consumer/@enterpriseCode}"
                           countryCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Consumer/@countryCode}"
                           xml:id="id_14">
               <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Consumer"
                             xml:id="id_15"/>
            </ns46:Consumer>
            <ns46:Trace clientReqTimestamp="{/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@clientReqTimestamp}"
                        eventID="{/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@eventID}"
                        xml:id="id_16">
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@reqTimestamp"
                       xml:id="id_17">
                  <xsl:attribute name="reqTimestamp" xml:id="id_18">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@reqTimestamp"
                                   xml:id="id_19"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@rspTimestamp"
                       xml:id="id_20">
                  <xsl:attribute name="rspTimestamp" xml:id="id_21">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@rspTimestamp"
                                   xml:id="id_22"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@processID"
                       xml:id="id_23">
                  <xsl:attribute name="processID" xml:id="id_24">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@processID"
                                   xml:id="id_25"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@sourceID"
                       xml:id="id_26">
                  <xsl:attribute name="sourceID" xml:id="id_27">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@sourceID"
                                   xml:id="id_28"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@correlationEventID"
                       xml:id="id_29">
                  <xsl:attribute name="correlationEventID" xml:id="id_30">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@correlationEventID"
                                   xml:id="id_31"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@conversationID"
                       xml:id="id_32">
                  <xsl:attribute name="conversationID" xml:id="id_33">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@conversationID"
                                   xml:id="id_34"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@correlationID"
                       xml:id="id_35">
                  <xsl:attribute name="correlationID" xml:id="id_36">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/@correlationID"
                                   xml:id="id_37"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service"
                       xml:id="id_38">
                  <ns46:Service xml:id="id_39">
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                             xml:id="id_40">
                        <xsl:attribute name="code" xml:id="id_41">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                                         xml:id="id_42"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@name"
                             xml:id="id_43">
                        <xsl:attribute name="name" xml:id="id_44">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@name"
                                         xml:id="id_45"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@operation"
                             xml:id="id_46">
                        <xsl:attribute name="operation" xml:id="id_47">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@operation"
                                         xml:id="id_48"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service"
                                   xml:id="id_49"/>
                  </ns46:Service>
               </xsl:if>
            </ns46:Trace>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Channel" xml:id="id_50">
               <ns46:Channel xml:id="id_51">
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@name"
                          xml:id="id_52">
                     <xsl:attribute name="name" xml:id="id_53">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@name"
                                      xml:id="id_54"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Channel/@mode"
                          xml:id="id_55">
                     <xsl:attribute name="mode" xml:id="id_56">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Channel/@mode"
                                      xml:id="id_57"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Channel"
                                xml:id="id_58"/>
               </ns46:Channel>
            </xsl:if>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result" xml:id="id_59">
               <ns44:Result status="{/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@status}"
                            xml:id="id_60">
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                          xml:id="id_61">
                     <xsl:attribute name="description" xml:id="id_62">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                                      xml:id="id_63"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:CanonicalError"
                          xml:id="id_64">
                     <ns25:CanonicalError xml:id="id_65">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:CanonicalError/@type"
                                xml:id="id_66">
                           <xsl:attribute name="type" xml:id="id_67">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:CanonicalError/@type"
                                            xml:id="id_68"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                                xml:id="id_69">
                           <xsl:attribute name="code" xml:id="id_70">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                                            xml:id="id_71"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                                xml:id="id_72">
                           <xsl:attribute name="description" xml:id="id_73">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                                            xml:id="id_74"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:CanonicalError"
                                      xml:id="id_75"/>
                     </ns25:CanonicalError>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError"
                          xml:id="id_76">
                     <ns25:SourceError xml:id="id_77">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                                xml:id="id_78">
                           <xsl:attribute name="code" xml:id="id_79">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                                            xml:id="id_80"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                                xml:id="id_81">
                           <xsl:attribute name="description" xml:id="id_82">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                                            xml:id="id_83"/>
                           </xsl:attribute>
                        </xsl:if>
                        <ns25:ErrorSourceDetails xml:id="id_84">
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@source"
                                   xml:id="id_85">
                              <xsl:attribute name="source" xml:id="id_86">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@source"
                                               xml:id="id_87"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@details"
                                   xml:id="id_88">
                              <xsl:attribute name="details" xml:id="id_89">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@details"
                                               xml:id="id_90"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails"
                                         xml:id="id_91"/>
                        </ns25:ErrorSourceDetails>
                     </ns25:SourceError>
                  </xsl:if>
                  <ns44:CorrelativeErrors xml:id="id_92">
                     <ns25:SourceError xml:id="id_93">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                                xml:id="id_94">
                           <xsl:attribute name="code" xml:id="id_95">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code"
                                            xml:id="id_96"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                                xml:id="id_97">
                           <xsl:attribute name="description" xml:id="id_98">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/@description"
                                            xml:id="id_99"/>
                           </xsl:attribute>
                        </xsl:if>
                        <ns25:ErrorSourceDetails xml:id="id_100">
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@source"
                                   xml:id="id_101">
                              <xsl:attribute name="source" xml:id="id_102">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@source"
                                               xml:id="id_103"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@details"
                                   xml:id="id_104">
                              <xsl:attribute name="details" xml:id="id_105">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails/@details"
                                               xml:id="id_106"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns44:Result/ns25:SourceError/ns25:ErrorSourceDetails"
                                         xml:id="id_107"/>
                        </ns25:ErrorSourceDetails>
                     </ns25:SourceError>
                  </ns44:CorrelativeErrors>
               </ns44:Result>
            </xsl:if>
         </ns46:RequestHeader>
         <tns:Keys xml:id="id_108">
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.adjustmentCode')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.presentIndicator.0')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.presentIndicator.1')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.causalCode')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.debtType')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.generationFormat')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.operation')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.costCenter')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.type')" xml:id="id_110"/>
            </tns:Key>
            <tns:Key xml:id="id_109">
               <xsl:value-of select="concat(/ns0:Publish_CollectionPostTransaction_REQ/ns46:RequestHeader/ns46:Trace/ns46:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.customerType')" xml:id="id_110"/>
            </tns:Key>
         </tns:Keys>
      </tns:GetREQ>
   </xsl:template>
</xsl:stylesheet>
