xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/updateGroupStatus/v1";

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::) external;
declare variable $GroupTag as xs:string external;
declare variable $GroupStatus as xs:string external;

declare function local:get_UpdateGroupStatusREQ($RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::), 
                                             $GroupTag as xs:string, 
                                             $GroupStatus as xs:string)
                                             as element() {
    <ns2:UpdateGroupStatusREQ>
        {$RequestHeader}
        <cor:GroupTag>{fn:data($GroupTag)}</cor:GroupTag>
        <cor:GroupStatus>{fn:data($GroupStatus)}</cor:GroupStatus>
    </ns2:UpdateGroupStatusREQ>
  
};

local:get_UpdateGroupStatusREQ($RequestHeader, $GroupTag, $GroupStatus)
