<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:tns="http://www.entel.cl/EBM/AppliedCustomerBillingCredit/Create/v1" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:ns1="http://www.entel.cl/SC/ParameterManager/get/v1" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns0="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns1 ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xml:id="id_1" oraxsl:ignorexmlids="true"
                xmlns:ns6="http://www.entel.cl/ESC/AppliedCustomerBillingCharge/v1"
                xmlns:ns8="http://www.entel.cl/ESC/PhysicalResource/v1"
                xmlns:ns11="http://www.entel.cl/EBO/StockTransaction/v1"
                xmlns:ns71="http://www.entel.cl/ESO/EndpointConfiguration/v1"
                xmlns:ns12="http://www.entel.cl/EBO/CustomerOrder/v1"
                xmlns:ns13="http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1"
                xmlns:ns14="http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1"
                xmlns:ns10="http://www.entel.cl/ESC/AppliedCustomerBillingCredit/v1"
                xmlns:rcGet="http://www.entel.cl/SC/ParameterManager/RefreshCache_Get/v1"
                xmlns:ns15="http://www.entel.cl/EBO/Quantity/v1" xmlns:ns16="http://www.entel.cl/EBO/ReceiverCompany/v1"
                xmlns:ns17="http://www.entel.cl/EBO/EmailContact/v1"
                xmlns:ns18="http://www.entel.cl/EBO/GeographicAddress/v1" xmlns:ns19="http://www.entel.cl/EBO/Money/v1"
                xmlns:rcGetM="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetMapping/v1"
                xmlns:ns20="http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1"
                xmlns:ns21="http://www.entel.cl/EBO/PostalDeliveryAddress/v1"
                xmlns:ns3="http://www.entel.cl/SC/ParameterManager/v1"
                xmlns:ns22="http://www.entel.cl/EBO/CustomerBill/v1"
                xmlns:ns23="http://www.entel.cl/EBO/PhysicalResourceSpec/v1"
                xmlns:rc="http://www.entel.cl/SC/ParameterManager/RefreshCache/v1"
                xmlns:ns24="http://www.entel.cl/EBO/PaymentMethod/v1"
                xmlns:ns25="http://www.entel.cl/EBO/LoyaltyTransaction/v1" xmlns:ns26="http://www.entel.cl/EBO/Rate/v1"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:ns27="http://www.entel.cl/EBO/Contact/v1" xmlns:ns28="http://www.entel.cl/EBO/GeographicArea/v1"
                xmlns:ns29="http://www.entel.cl/EBO/Individual/v1" xmlns:ns30="http://www.entel.cl/EBO/MSISDN/v1"
                xmlns:ns5="http://schemas.oracle.com/bpel/extension"
                xmlns:ns31="http://www.entel.cl/EBO/AuthenticationEntity/v1"
                xmlns:ns2="http://www.entel.cl/ESC/PaymentOrder/v1" xmlns:ns32="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:ns33="http://www.entel.cl/ESO/Error/v1"
                xmlns:getC="http://www.entel.cl/SC/ParameterManager/getConfig/v1"
                xmlns:client="http://xmlns.oracle.com/ES/OH/BPELProcess"
                xmlns:getM="http://www.entel.cl/SC/ParameterManager/getMapping/v1"
                xmlns:ns34="http://www.entel.cl/EBO/AppliedCustomerBillingRate/v1"
                xmlns:ns35="http://www.entel.cl/EBO/IndividualIdentification/v1"
                xmlns:getE="http://www.entel.cl/SC/ParameterManager/getEndpoint/v1"
                xmlns:ns36="http://www.entel.cl/EBO/BillingAccount/v1"
                xmlns:ns37="http://www.entel.cl/EBO/SalesChannel/v1"
                xmlns:ns38="http://www.entel.cl/EBO/CustomerPayment/v1" xmlns:ns39="http://www.entel.cl/EBO/Voucher/v1"
                xmlns:ns9="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1"
                xmlns:ns40="http://www.entel.cl/EBO/Asset/v1" xmlns:ns41="http://www.entel.cl/EBO/CustomerAccount/v1"
                xmlns:ns42="http://www.entel.cl/EBO/IssuingCompany/v1"
                xmlns:ns43="http://www.entel.cl/EBO/PaymentItem/v1" xmlns:ns7="http://www.entel.cl/ESC/CustomerBill/v1"
                xmlns:ns44="http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1"
                xmlns:ns45="http://www.entel.cl/EBO/CustomerAccountBalance/v1"
                xmlns:ns46="http://www.entel.cl/EBO/TelephoneNumber/v1"
                xmlns:ns47="http://www.entel.cl/EBO/PartyRole/v1" xmlns:ns48="http://www.entel.cl/EBO/Customer/v1"
                xmlns:ns49="http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1"
                xmlns:ns50="http://www.entel.cl/EBO/PhysicalResource/v1"
                xmlns:ns51="http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1"
                xmlns:ns52="http://www.entel.cl/EBO/OrganizationName/v1"
                xmlns:ns53="http://www.entel.cl/EBO/CustomerAddress/v1"
                xmlns:ns54="http://www.entel.cl/EBO/CustomerComercialAddress/v1"
                xmlns:ns4="http://www.entel.cl/SC/ConversationManager/v1"
                xmlns:ns55="http://www.entel.cl/EBO/PartyIdentification/v1"
                xmlns:ns56="http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1"
                xmlns:ns57="http://www.entel.cl/EBO/IndividualName/v1" xmlns:ns58="http://www.entel.cl/ESO/Result/v2"
                xmlns:ns59="http://www.entel.cl/EBO/Skill/v1" xmlns:ns60="http://www.entel.cl/ESO/MessageHeader/v1"
                xmlns:ns61="http://www.entel.cl/EBO/CustomerOrderItem/v1"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:rcGetC="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetConfig/v1"
                xmlns:ns62="http://www.entel.cl/EBO/Organization/v1" xmlns:ns63="http://www.entel.cl/EBO/StreetName/v1"
                xmlns:ns64="http://www.entel.cl/EBO/TimePeriod/v1"
                xmlns:rcGetE="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetEndpoint/v1"
                xmlns:ns65="http://www.entel.cl/EBO/PlaceOfBirth/v1" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ns66="http://www.entel.cl/EBO/FinancialChargeSpec/v1"
                xmlns:ns67="http://www.entel.cl/EBO/Price/v1" xmlns:ns68="http://www.entel.cl/EBO/Address/v1"
                xmlns:ns69="http://www.entel.cl/EBO/Store/v1" xmlns:ns70="http://www.entel.cl/EBO/Country/v1"
                xmlns:ebmns_Cancel="http://www.entel.cl/EBM/AppliedCustomerBillingCredit/Cancel/v1"
                xmlns:ns72="http://www.entel.cl/EBO/BillDocument/v1"
                xmlns:ns="http://schemas.xmlsoap.org/soap/encoding/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
                xmlns:ns73="http://www.entel.cl/EBO/PaymentPlan/v1"
                xmlns:ns74="http://www.entel.cl/EBM/PhysicalResource/Release/v1">
   <oracle-xsl-mapper:schema xml:id="id_2">
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources xml:id="id_3">
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_4">
            <oracle-xsl-mapper:schema location="../WSDLs/BPELProcess.wsdl" xml:id="id_5"/>
            <oracle-xsl-mapper:rootElement name="Publish_CollectionPostTransaction_REQ" namespace="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xml:id="id_6"/>
         </oracle-xsl-mapper:source>
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_7">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_ParameterManager/SupportAPI/WSDL/ParameterManager.wsdl" xml:id="id_8"/>
            <oracle-xsl-mapper:rootElement name="GetRSP" namespace="http://www.entel.cl/SC/ParameterManager/get/v1" xml:id="id_9"/>
            <oracle-xsl-mapper:param name="ParameterManager_get_RSP3.Out" xml:id="id_10"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets xml:id="id_11">
         <oracle-xsl-mapper:target type="WSDL" xml:id="id_12">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/ES_AppliedCustomerBillingCredit_v1/ESC/Primary/AppliedCustomerBillingCredit_v1_ESC.wsdl" xml:id="id_13"/>
            <oracle-xsl-mapper:rootElement name="Create_AppliedCustomerBillingCredit_REQ" namespace="http://www.entel.cl/EBM/AppliedCustomerBillingCredit/Create/v1" xml:id="id_14"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [THU JAN 05 13:18:21 ART 2017].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:param name="ParameterManager_get_RSP3.Out" xml:id="id_15"/>
   <xsl:template match="/" xml:id="id_16">
      <tns:Create_AppliedCustomerBillingCredit_REQ xml:id="id_17">
      <ns60:RequestHeader>
            <ns60:Consumer sysCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Consumer/@sysCode}"
                           enterpriseCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Consumer/@enterpriseCode}"
                           countryCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Consumer/@countryCode}">
               <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Consumer"/>
            </ns60:Consumer>
            <ns60:Trace clientReqTimestamp="{/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@clientReqTimestamp}"
                        eventID="{/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@eventID}">
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@reqTimestamp">
                  <xsl:attribute name="reqTimestamp">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@reqTimestamp"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@rspTimestamp">
                  <xsl:attribute name="rspTimestamp">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@rspTimestamp"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@processID">
                  <xsl:attribute name="processID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@processID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@sourceID">
                  <xsl:attribute name="sourceID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@sourceID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@correlationEventID">
                  <xsl:attribute name="correlationEventID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@correlationEventID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@conversationID">
                  <xsl:attribute name="conversationID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@conversationID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@correlationID">
                  <xsl:attribute name="correlationID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/@correlationID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service">
                  <ns60:Service>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@code">
                        <xsl:attribute name="code">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@code"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@name">
                        <xsl:attribute name="name">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@name"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@operation">
                        <xsl:attribute name="operation">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@operation"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service"/>
                  </ns60:Service>
               </xsl:if>
            </ns60:Trace>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Channel">
               <ns60:Channel>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Channel/@name">
                     <xsl:attribute name="name">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Channel/@name"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Channel/@mode">
                     <xsl:attribute name="mode">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Channel/@mode"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Channel"/>
               </ns60:Channel>
            </xsl:if>
         </ns60:RequestHeader>
         <tns:Body xml:id="id_18">
            <tns:BillDocument xml:id="id_19">
               <tns:customerPayment xml:id="id_20">
                  <ns38:ID xml:id="id_21">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns38:ID"
                                   xml:id="id_22"/>
                  </ns38:ID>
                  <ns38:debtType xml:id="id_68">
                     <xsl:value-of select="$ParameterManager_get_RSP3.Out/ns1:GetRSP/ns1:Values/ns32:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.credit.debtType')]/@value"
                                   xml:id="id_69"/>
                  </ns38:debtType>
                  <ns38:exchangeType xml:id="id_66">
                     <xsl:value-of select="$ParameterManager_get_RSP3.Out/ns1:GetRSP/ns1:Values/ns32:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns60:RequestHeader/ns60:Trace/ns60:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.exchangeType')]/@value"
                                   xml:id="id_67"/>
                  </ns38:exchangeType>
                  <ns38:operatorID xml:id="id_23">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns38:operatorID"
                                   xml:id="id_24"/>
                  </ns38:operatorID>
                  <ns38:paymentDate xml:id="id_25">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns38:paymentDate"
                                   xml:id="id_26"/>
                  </ns38:paymentDate>
                  <ns38:paymentDateLogical xml:id="id_27">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns38:paymentDateLogical"
                                   xml:id="id_28"/>
                  </ns38:paymentDateLogical>
                  <ns38:trace xml:id="id_29">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns38:trace"
                                   xml:id="id_30"/>
                  </ns38:trace>
                  <tns:CustomerAccount xml:id="id_31">
                     <ns41:ID xml:id="id_32">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns41:ID"
                                      xml:id="id_33"/>
                     </ns41:ID>
                     <tns:IndividualIdentification xml:id="id_34">
                        <ns35:number xml:id="id_35">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:IndividualIdentification/ns35:number"
                                         xml:id="id_36"/>
                        </ns35:number>
                     </tns:IndividualIdentification>
                  </tns:CustomerAccount>
                  <tns:CustomerBill xml:id="id_37">
                     <ns22:billNo xml:id="id_38">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns22:billNo"
                                      xml:id="id_39"/>
                     </ns22:billNo>
                     <ns22:documentType xml:id="id_40">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns22:documentType"
                                      xml:id="id_41"/>
                     </ns22:documentType>
                     <ns22:totalAmount xml:id="id_42">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns22:totalAmount"
                                      xml:id="id_43"/>
                     </ns22:totalAmount>
                  </tns:CustomerBill>
                  <tns:IssuingCompany xml:id="id_44">
                     <tns:organizationName xml:id="id_45">
                        <ns52:shortName xml:id="id_46">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName/ns52:shortName"
                                         xml:id="id_47"/>
                        </ns52:shortName>
                     </tns:organizationName>
                  </tns:IssuingCompany>
                  <tns:PaymentMethod xml:id="id_48">
                     <ns24:billNoNC xml:id="id_49">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:PaymentMethod/ns24:billNoNC"
                                      xml:id="id_50"/>
                     </ns24:billNoNC>
                     <ns24:date xml:id="id_51">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:PaymentMethod/ns24:date"
                                      xml:id="id_52"/>
                     </ns24:date>
                     <ns24:paymentMethodType xml:id="id_53">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:PaymentMethod/ns24:paymentMethodType"
                                      xml:id="id_54"/>
                     </ns24:paymentMethodType>
                     <tns:amount xml:id="id_58">
                        <ns19:amount xml:id="id_59">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:PaymentMethod/ns0:amount/ns19:amount"
                                         xml:id="id_60"/>
                        </ns19:amount>
                     </tns:amount>
                     <tns:Money xml:id="id_55">
                        <ns19:units xml:id="id_56">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:PaymentMethod/ns0:Money/ns19:units"
                                         xml:id="id_57"/>
                        </ns19:units>
                     </tns:Money>
                  </tns:PaymentMethod>
                  <tns:ThirdPartyPayeeAgency xml:id="id_61">
                     <ns13:branch xml:id="id_62">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:ThirdPartyPayeeAgency/ns13:branch"
                                      xml:id="id_63"/>
                     </ns13:branch>
                     <ns13:branchName xml:id="id_64">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:ThirdPartyPayeeAgency/ns13:branchName"
                                      xml:id="id_65"/>
                     </ns13:branchName>
                  </tns:ThirdPartyPayeeAgency>
               </tns:customerPayment>
            </tns:BillDocument>
         </tns:Body>
      </tns:Create_AppliedCustomerBillingCredit_REQ>
   </xsl:template>
</xsl:stylesheet>
