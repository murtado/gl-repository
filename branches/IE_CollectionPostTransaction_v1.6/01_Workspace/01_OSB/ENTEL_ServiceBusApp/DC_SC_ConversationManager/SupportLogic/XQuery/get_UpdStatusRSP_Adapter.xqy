xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/updStatus/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/updStatus_ConversationManager_v1_CSM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";

declare variable $Result as element() (:: schema-element(ns1:UpdStatusRSP) ::) external;

declare function local:get_UpdStatusRSP_Adapter($Result as element() (:: schema-element(ns1:UpdStatusRSP) ::) ) as element() (:: schema-element(ns1:UpdStatusRSP) ::) {
    <ns1:UpdStatusRSP>
      {
        $Result
      }
    </ns1:UpdStatusRSP>
};

local:get_UpdStatusRSP_Adapter($Result)
