xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CorrelationManager/updateGroupStatus/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/updateGroupStatus_CorrelationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/updateGroupStatus";
(:: import schema at "../JCA/updateGroupStatus/updateGroupStatus_sp.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $SC_REQ as element() (:: schema-element(ns1:UpdateGroupStatusREQ) ::) external;

declare function local:func($SC_REQ as element() (:: schema-element(ns1:UpdateGroupStatusREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_GRP_TAG>{fn:data($SC_REQ/cor:GroupTag)}</ns2:P_GRP_TAG>
        <ns2:P_STATUS>{fn:data($SC_REQ/cor:GroupStatus)}</ns2:P_STATUS>
    </ns2:InputParameters>
};

local:func($SC_REQ)
