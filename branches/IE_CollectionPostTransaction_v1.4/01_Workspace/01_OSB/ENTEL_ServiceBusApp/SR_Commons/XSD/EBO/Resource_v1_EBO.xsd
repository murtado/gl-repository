<?xml version="1.0" encoding="UTF-8" ?>
<xs:schema 
	xmlns="http://www.w3.org/2001/XMLSchema" 	
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:tns="http://www.entel.cl/EBO/Resource/v1"
	xmlns:ebons_characteristics="http://www.entel.cl/EBO/Characteristics/v1"
	
	targetNamespace="http://www.entel.cl/EBO/Resource/v1"
	elementFormDefault="qualified" version="1.0">	

	 <!---=- start:Imports -=- -->
	<xs:import namespace="http://www.entel.cl/EBO/Characteristics/v1" schemaLocation="Characteristics_v1_EBO.xsd"/>
	
	 <!---=- end:Imports -=- -->
	 
	<!---=- start:EBO Content -=- -->
	<!---=- start:EBO root elements defs -=- -->

	<xs:element name="SN" type="xs:string"/>									
	<xs:element name="externalID" type="xs:string"/>									
	<xs:element name="format" type="xs:string"/>									
	<xs:element name="id" type="xs:string"/>									
	<xs:element name="imei" type="xs:string"/>									
	<xs:element name="isCompound" type="xs:boolean"/>									
	<xs:element name="isLogical" type="xs:boolean"/>									
	<xs:element name="isOperational" type="xs:boolean">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This attribute is used to define the operational status of the object, and is implemented as a Boolean: TRUE means that the object is currently operational, and FALSE means that the object is not currently operational.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="isPending" type="xs:string"/>									
	<xs:element name="lrStatus" type="xs:integer">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This is an enumerated integer whose value indicates the current status of the object. </xs:documentation>
					<xs:documentation>Stopped indicates that the LogicalResource is known to still exist, but it is no longer operational. However, it has not failed.</xs:documentation>
					<xs:documentation>	</xs:documentation>
					<xs:documentation>Stressed indicates that the LogicalResource is functioning, but needs attention (e.g., it may be overheating, or overloaded in some way). This is similar to Predicted Failure, which indicates that this LogicalResource is functioning properly, but is predicting a failure in the near future.</xs:documentation>
					<xs:documentation>In Maintenance indicates that this LogicalResource is being configured, maintained, or otherwise administered and is NOT available for service.</xs:documentation>
					<xs:documentation>Unable To Contact indicates that the monitoring system has knowledge of this LogicalResource but has never been able to establish communications with it. In contrast, Lost Commmunications indicates that the LogicalResource has been contacted successfully in the past and is known to still exist; however, it is currently unreachable.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="manufactureDate" type="xs:string">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This is a string attribute that defines the date of manufacture of this item in the fixed format "dd/mm/yyyy". This is an optional attribute.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="name" type="xs:string"/>									
	<xs:element name="objectID" type="xs:string"/>									
	<xs:element name="otherIdentifier" type="xs:string">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This is a string that is used to contain other important identifying data, such as a bar code, of the hardware item. This is an optional attribute.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="parentId" type="xs:string"/>									
	<xs:element name="physicalResourceId" type="xs:string"/>									
	<xs:element name="powerState" type="xs:integer">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This is an enumerated integer that defines the current power status of the hardware item. Values include:</xs:documentation>
					<xs:documentation>  0:  Unknown</xs:documentation>
					<xs:documentation>  1:  Not Applicable</xs:documentation>
					<xs:documentation>  2:  No Power Applied</xs:documentation>
					<xs:documentation>  3: Full Power Applied</xs:documentation>
					<xs:documentation>  4:  Power Save - Normal</xs:documentation>
					<xs:documentation>  5:  Power Save - Degraded</xs:documentation>
					<xs:documentation>  6:  Power Save - Standby</xs:documentation>
					<xs:documentation>  7:  Power Save - Critical</xs:documentation>
					<xs:documentation>  8:  Power Save - Low Power Mode</xs:documentation>
					<xs:documentation>  9:  Power Save - Unknown</xs:documentation>
					<xs:documentation> 10: Power Cycle</xs:documentation>
					<xs:documentation> 11: Power Warning</xs:documentation>
					<xs:documentation> 12: Power Off</xs:documentation>
					<xs:documentation>Value 1 means that the hardware item doesn't require the direct application of power (e.g., a but or bolt). If the value for this item is 3, then the PowerCapability class will describe the particular power requirements of this item through the HasPower association.</xs:documentation>
					<xs:documentation>This is an optional attribute.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="resourceNumber" type="xs:string"/>									
	<xs:element name="resourceType" type="xs:string"/>									
	<xs:element name="serialNumber" type="xs:string">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This is a string that represents a manufacturer-allocated number used to identify different instances of the same hardware item. The ModelNumber and PartNumber attributes are used to identify different types of hardware items. This is a REQUIRED attribute.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="serviceState" type="xs:integer">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This is an enumerated integer that defines the availability and usage (i.e., the service state) of this LogicalResource.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="state" type="xs:string"/>									
	<xs:element name="versionNumber" type="xs:string">
					<xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
					<xs:documentation>This is a string that identifies the version of this object. This is an optional attribute.</xs:documentation>
				</xs:annotation>
			</xs:element>									
	<xs:element name="resourceCharacteristics" type="ebons_characteristics:Characteristics_Type"/>									
	<!---=- end:EBO root elements defs -=- -->

<!---=- start:EBO complexType defs -=- -->
	<xs:element name="Resource" type="tns:Resource_Type"/>
	<xs:complexType name="Resource_Type">
		
			<xs:annotation>
			<xs:documentation xmlns:xs="http://www.w3.org/2001/XMLSchema">This object class defines the attributes that are common to all resource objects.</xs:documentation>
							
			<xs:documentation xmlns:xs="http://www.w3.org/2001/XMLSchema">See R_TMF518_NRB_I_0001.</xs:documentation>
							
			<xs:documentation xmlns:xs="http://www.w3.org/2001/XMLSchema">This object class defines the attributes that are common to all resource objects.</xs:documentation>
							
			<xs:documentation xmlns:xs="http://www.w3.org/2001/XMLSchema">See R_TMF518_NRB_I_0001.</xs:documentation>
						
			</xs:annotation>
		<xs:sequence> 
		<xs:element ref="tns:SN" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:externalID" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:format" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:id" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:imei" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:isCompound" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:isLogical" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:isOperational" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:isPending" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:lrStatus" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:manufactureDate" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:name" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:objectID" maxOccurs="1" minOccurs="1"/>
		<xs:element ref="tns:otherIdentifier" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:parentId" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:physicalResourceId" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:powerState" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:resourceNumber" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:resourceType" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:serialNumber" maxOccurs="unbounded" minOccurs="0"/>
		<xs:element ref="tns:serviceState" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:state" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:versionNumber" maxOccurs="1" minOccurs="0"/>
		<xs:element ref="tns:resourceCharacteristics" maxOccurs="unbounded" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

<!---=- end:EBO complexType defs -=- -->
	<!---=- end:EBO Content -=- -->
	 
</xs:schema>	