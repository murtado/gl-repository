<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:tns="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:ns1="http://www.entel.cl/SC/ParameterManager/get/v1" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns0="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns1 ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xml:id="id_1" oraxsl:ignorexmlids="true"
                xmlns:ns6="http://www.entel.cl/ESC/AppliedCustomerBillingCharge/v1"
                xmlns:ns8="http://www.entel.cl/EBO/CustomerOrder/v1"
                xmlns:rcGet="http://www.entel.cl/SC/ParameterManager/RefreshCache_Get/v1"
                xmlns:ns11="http://www.entel.cl/EBO/Quantity/v1"
                xmlns:ns16="http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1"
                xmlns:ns3="http://www.entel.cl/SC/ParameterManager/v1"
                xmlns:ns18="http://www.entel.cl/EBO/CustomerBill/v1"
                xmlns:ns21="http://www.entel.cl/EBO/LoyaltyTransaction/v1"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:ns22="http://www.entel.cl/EBO/Contact/v1" xmlns:ns23="http://www.entel.cl/EBO/GeographicArea/v1"
                xmlns:ns24="http://www.entel.cl/EBO/MSISDN/v1" xmlns:ns25="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:ns26="http://www.entel.cl/ESO/Error/v1"
                xmlns:getC="http://www.entel.cl/SC/ParameterManager/getConfig/v1"
                xmlns:ns27="http://www.entel.cl/EBO/AppliedCustomerBillingRate/v1"
                xmlns:ns28="http://www.entel.cl/EBO/IndividualIdentification/v1"
                xmlns:ns30="http://www.entel.cl/EBO/SalesChannel/v1" xmlns:ns33="http://www.entel.cl/EBO/Asset/v1"
                xmlns:ns34="http://www.entel.cl/EBO/CustomerAccount/v1"
                xmlns:ns36="http://www.entel.cl/EBO/TelephoneNumber/v1"
                xmlns:ns37="http://www.entel.cl/EBO/PartyRole/v1"
                xmlns:ns39="http://www.entel.cl/EBO/PhysicalResource/v1"
                xmlns:ns42="http://www.entel.cl/EBO/CustomerAddress/v1"
                xmlns:ns43="http://www.entel.cl/EBO/PartyIdentification/v1"
                xmlns:ns45="http://www.entel.cl/ESO/Result/v2" xmlns:ns46="http://www.entel.cl/EBO/Skill/v1"
                xmlns:ns47="http://www.entel.cl/ESO/MessageHeader/v1"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:ns50="http://www.entel.cl/EBO/StreetName/v1" xmlns:ns51="http://www.entel.cl/EBO/TimePeriod/v1"
                xmlns:rcGetE="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetEndpoint/v1"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ns52="http://www.entel.cl/EBO/Price/v1"
                xmlns:ns54="http://www.entel.cl/EBO/Store/v1" xmlns:ns7="http://www.entel.cl/EBO/StockTransaction/v1"
                xmlns:ns55="http://www.entel.cl/ESO/EndpointConfiguration/v1"
                xmlns:ns9="http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1"
                xmlns:ns10="http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1"
                xmlns:ns12="http://www.entel.cl/EBO/ReceiverCompany/v1"
                xmlns:ns13="http://www.entel.cl/EBO/EmailContact/v1"
                xmlns:ns14="http://www.entel.cl/EBO/GeographicAddress/v1" xmlns:ns15="http://www.entel.cl/EBO/Money/v1"
                xmlns:rcGetM="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetMapping/v1"
                xmlns:ns17="http://www.entel.cl/EBO/PostalDeliveryAddress/v1"
                xmlns:ns19="http://www.entel.cl/EBO/PhysicalResourceSpec/v1"
                xmlns:rc="http://www.entel.cl/SC/ParameterManager/RefreshCache/v1"
                xmlns:ns20="http://www.entel.cl/EBO/PaymentMethod/v1"
                xmlns:ns5="http://schemas.oracle.com/bpel/extension" xmlns:ns2="http://www.entel.cl/ESC/PaymentOrder/v1"
                xmlns:client="http://xmlns.oracle.com/ES/OH/BPELProcess"
                xmlns:getM="http://www.entel.cl/SC/ParameterManager/getMapping/v1"
                xmlns:getE="http://www.entel.cl/SC/ParameterManager/getEndpoint/v1"
                xmlns:ns29="http://www.entel.cl/EBO/BillingAccount/v1"
                xmlns:ns31="http://www.entel.cl/EBO/CustomerPayment/v1" xmlns:ns32="http://www.entel.cl/EBO/Voucher/v1"
                xmlns:ns35="http://www.entel.cl/EBO/IssuingCompany/v1" xmlns:ns38="http://www.entel.cl/EBO/Customer/v1"
                xmlns:ns40="http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1"
                xmlns:ns41="http://www.entel.cl/EBO/OrganizationName/v1"
                xmlns:ns4="http://www.entel.cl/SC/ConversationManager/v1"
                xmlns:ns44="http://www.entel.cl/EBO/IndividualName/v1"
                xmlns:ns48="http://www.entel.cl/EBO/CustomerOrderItem/v1"
                xmlns:rcGetC="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetConfig/v1"
                xmlns:ns49="http://www.entel.cl/EBO/Organization/v1" xmlns:ns53="http://www.entel.cl/EBO/Address/v1"
                xmlns:ns58="http://www.entel.cl/EBO/Individual/v1" xmlns:ns60="http://www.entel.cl/EBO/BillDocument/v1"
                xmlns:ns61="http://www.entel.cl/EBO/PaymentItem/v1" xmlns:ns="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
                xmlns:ns68="http://www.entel.cl/EBO/FinancialChargeSpec/v1"
                xmlns:ebmns_Get="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Get/v1"
                xmlns:ns69="http://www.entel.cl/EBO/Country/v1" xmlns:ns56="http://www.entel.cl/EBO/PaymentPlan/v1"
                xmlns:ns57="http://www.entel.cl/EBO/Rate/v1"
                xmlns:ns59="http://www.entel.cl/EBO/AuthenticationEntity/v1"
                xmlns:ns62="http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1"
                xmlns:ns63="http://www.entel.cl/EBO/CustomerAccountBalance/v1"
                xmlns:ns64="http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1"
                xmlns:ns65="http://www.entel.cl/EBO/CustomerComercialAddress/v1"
                xmlns:ns66="http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1"
                xmlns:ns67="http://www.entel.cl/EBO/PlaceOfBirth/v1"
                xmlns:ns70="http://www.entel.cl/ESC/CustomerBill/v1"
                xmlns:ns71="http://www.entel.cl/ESC/PhysicalResource/v1"
                xmlns:ns72="http://www.entel.cl/ESC/AppliedCustomerBillingCredit/v1">
   <oracle-xsl-mapper:schema xml:id="id_2">
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources xml:id="id_3">
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_4">
            <oracle-xsl-mapper:schema location="../WSDLs/BPELProcess.wsdl" xml:id="id_5"/>
            <oracle-xsl-mapper:rootElement name="Publish_CollectionPostTransaction_REQ" namespace="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xml:id="id_6"/>
         </oracle-xsl-mapper:source>
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_7">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_ParameterManager/SupportAPI/WSDL/ParameterManager.wsdl" xml:id="id_8"/>
            <oracle-xsl-mapper:rootElement name="GetRSP" namespace="http://www.entel.cl/SC/ParameterManager/get/v1" xml:id="id_9"/>
            <oracle-xsl-mapper:param name="ParameterManager_get_RSP.Out" xml:id="id_10"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets xml:id="id_11">
         <oracle-xsl-mapper:target type="WSDL" xml:id="id_12">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/ES_AppliedCustomerBillingCharge_v1/ESC/Primary/AppliedCustomerBillingCharge_v1_ESC.wsdl" xml:id="id_13"/>
            <oracle-xsl-mapper:rootElement name="Create_AppliedCustomerBillingCharge_REQ" namespace="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1" xml:id="id_14"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [WED DEC 28 22:51:08 ART 2016].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:param name="ParameterManager_get_RSP.Out" xml:id="id_15"/>
   <xsl:template match="/" xml:id="id_16">
      <tns:Create_AppliedCustomerBillingCharge_REQ xml:id="id_17">
         <ns47:RequestHeader xml:id="id_18">
            <ns47:Consumer sysCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Consumer/@sysCode}"
                           enterpriseCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Consumer/@enterpriseCode}"
                           countryCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Consumer/@countryCode}"
                           xml:id="id_19">
               <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Consumer"
                             xml:id="id_20"/>
            </ns47:Consumer>
            <ns47:Trace clientReqTimestamp="{/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@clientReqTimestamp}"
                        eventID="{/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@eventID}"
                        xml:id="id_21">
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@reqTimestamp"
                       xml:id="id_22">
                  <xsl:attribute name="reqTimestamp" xml:id="id_23">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@reqTimestamp"
                                   xml:id="id_24"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@rspTimestamp"
                       xml:id="id_25">
                  <xsl:attribute name="rspTimestamp" xml:id="id_26">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@rspTimestamp"
                                   xml:id="id_27"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@processID"
                       xml:id="id_28">
                  <xsl:attribute name="processID" xml:id="id_29">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@processID"
                                   xml:id="id_30"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@sourceID"
                       xml:id="id_31">
                  <xsl:attribute name="sourceID" xml:id="id_32">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@sourceID"
                                   xml:id="id_33"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@correlationEventID"
                       xml:id="id_34">
                  <xsl:attribute name="correlationEventID" xml:id="id_35">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@correlationEventID"
                                   xml:id="id_36"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@conversationID"
                       xml:id="id_37">
                  <xsl:attribute name="conversationID" xml:id="id_38">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@conversationID"
                                   xml:id="id_39"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@correlationID"
                       xml:id="id_40">
                  <xsl:attribute name="correlationID" xml:id="id_41">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/@correlationID"
                                   xml:id="id_42"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service"
                       xml:id="id_43">
                  <ns47:Service xml:id="id_44">
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                             xml:id="id_45">
                        <xsl:attribute name="code" xml:id="id_46">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                                         xml:id="id_47"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@name"
                             xml:id="id_48">
                        <xsl:attribute name="name" xml:id="id_49">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@name"
                                         xml:id="id_50"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@operation"
                             xml:id="id_51">
                        <xsl:attribute name="operation" xml:id="id_52">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@operation"
                                         xml:id="id_53"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service"
                                   xml:id="id_54"/>
                  </ns47:Service>
               </xsl:if>
            </ns47:Trace>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Channel" xml:id="id_55">
               <ns47:Channel xml:id="id_56">
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@name"
                          xml:id="id_57">
                     <xsl:attribute name="name" xml:id="id_58">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Channel/@name"
                                      xml:id="id_291"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Channel/@mode"
                          xml:id="id_60">
                     <xsl:attribute name="mode" xml:id="id_61">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Channel/@mode"
                                      xml:id="id_62"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Channel"
                                xml:id="id_63"/>
               </ns47:Channel>
            </xsl:if>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result" xml:id="id_64">
               <ns45:Result status="{/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@status}"
                            xml:id="id_65">
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                          xml:id="id_66">
                     <xsl:attribute name="description" xml:id="id_67">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                                      xml:id="id_68"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:CanonicalError"
                          xml:id="id_69">
                     <ns26:CanonicalError xml:id="id_70">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:CanonicalError/@type"
                                xml:id="id_71">
                           <xsl:attribute name="type" xml:id="id_72">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:CanonicalError/@type"
                                            xml:id="id_73"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                                xml:id="id_74">
                           <xsl:attribute name="code" xml:id="id_75">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                                            xml:id="id_76"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                                xml:id="id_77">
                           <xsl:attribute name="description" xml:id="id_78">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                                            xml:id="id_79"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:CanonicalError"
                                      xml:id="id_80"/>
                     </ns26:CanonicalError>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError"
                          xml:id="id_81">
                     <ns26:SourceError xml:id="id_82">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                                xml:id="id_83">
                           <xsl:attribute name="code" xml:id="id_84">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                                            xml:id="id_85"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                                xml:id="id_86">
                           <xsl:attribute name="description" xml:id="id_87">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                                            xml:id="id_88"/>
                           </xsl:attribute>
                        </xsl:if>
                        <ns26:ErrorSourceDetails xml:id="id_89">
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@source"
                                   xml:id="id_90">
                              <xsl:attribute name="source" xml:id="id_91">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@source"
                                               xml:id="id_92"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@details"
                                   xml:id="id_93">
                              <xsl:attribute name="details" xml:id="id_94">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@details"
                                               xml:id="id_95"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails"
                                         xml:id="id_96"/>
                        </ns26:ErrorSourceDetails>
                     </ns26:SourceError>
                  </xsl:if>
                  <ns45:CorrelativeErrors xml:id="id_97">
                     <ns26:SourceError xml:id="id_98">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                                xml:id="id_99">
                           <xsl:attribute name="code" xml:id="id_100">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code"
                                            xml:id="id_101"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                                xml:id="id_102">
                           <xsl:attribute name="description" xml:id="id_103">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/@description"
                                            xml:id="id_104"/>
                           </xsl:attribute>
                        </xsl:if>
                        <ns26:ErrorSourceDetails xml:id="id_105">
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@source"
                                   xml:id="id_106">
                              <xsl:attribute name="source" xml:id="id_107">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@source"
                                               xml:id="id_108"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@details"
                                   xml:id="id_109">
                              <xsl:attribute name="details" xml:id="id_110">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails/@details"
                                               xml:id="id_111"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns45:Result/ns26:SourceError/ns26:ErrorSourceDetails"
                                         xml:id="id_112"/>
                        </ns26:ErrorSourceDetails>
                     </ns26:SourceError>
                  </ns45:CorrelativeErrors>
               </ns45:Result>
            </xsl:if>
         </ns47:RequestHeader>
         <tns:Body xml:id="id_113">
            <tns:CustomerBill xml:id="id_114">
               <ns18:adjustmentCode xml:id="id_161">
                  <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.adjustmentCode')]/@value"
                                xml:id="id_162"/>
               </ns18:adjustmentCode>
               <ns18:adjustmentDate xml:id="id_163">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:adjustmentDate"
                                xml:id="id_164"/>
               </ns18:adjustmentDate>
               <ns18:adjustmentDescription xml:id="id_165">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:adjustmentDescription"
                                xml:id="id_166"/>
               </ns18:adjustmentDescription>
               <ns18:appliedDate xml:id="id_167">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:appliedDate"
                                xml:id="id_168"/>
               </ns18:appliedDate>
               <ns18:billDate xml:id="id_169">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:billDate"
                                xml:id="id_170"/>
               </ns18:billDate>
               <ns18:billNo xml:id="id_171">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:billNo"
                                xml:id="id_172"/>
               </ns18:billNo>
               <ns18:billNoInv xml:id="id_173">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:billNoInv"
                                xml:id="id_174"/>
               </ns18:billNoInv>
               <ns18:billNoOriginal xml:id="id_175">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:billNoOriginal"
                                xml:id="id_176"/>
               </ns18:billNoOriginal>
               <ns18:causalCode xml:id="id_267">
                  <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.causalCode')]/@value"
                                xml:id="id_268"/>
               </ns18:causalCode>
               <ns18:debtType xml:id="id_273">
                  <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.debtType')]/@value"
                                xml:id="id_274"/>
               </ns18:debtType>
               <ns18:discountAmount xml:id="id_177">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:discountAmount"
                                xml:id="id_178"/>
               </ns18:discountAmount>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:totalAmount > 0">
                   <ns18:discountPercentage xml:id="id_262">
                      <xsl:value-of select="100 * (/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:discountAmount div /ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:totalAmount)"
                                    xml:id="id_263"/>
                   </ns18:discountPercentage>
               </xsl:if>
               <ns18:documentType xml:id="id_179">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:documentType"
                                xml:id="id_180"/>
               </ns18:documentType>
               <ns18:documentTypeInv xml:id="id_181">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:documentTypeInv"
                                xml:id="id_182"/>
               </ns18:documentTypeInv>
               <ns18:documentTypeRef xml:id="id_183">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:documentTypeRef"
                                xml:id="id_184"/>
               </ns18:documentTypeRef>
               <ns18:generationFormat xml:id="id_275">
                  <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.generationFormat')]/@value"
                                xml:id="id_276"/>
               </ns18:generationFormat>
               <ns18:idTransaction xml:id="id_115">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns31:ID"
                                xml:id="id_116"/>
               </ns18:idTransaction>
               <ns18:netAmount xml:id="id_269">
                  <xsl:value-of select="sum(/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingProductCharge/ns10:netAmount)"
                                xml:id="id_270"/>
               </ns18:netAmount>
               <ns18:observation xml:id="id_266"></ns18:observation>
               <ns18:operation xml:id="id_277">
                  <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.operation')]/@value"
                                xml:id="id_278"/>
               </ns18:operation>
               <ns18:operationType xml:id="id_185">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:operationType"
                                xml:id="id_186"/>
               </ns18:operationType>
               <ns18:paymentDueDate xml:id="id_187">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:paymentDueDate"
                                xml:id="id_188"/>
               </ns18:paymentDueDate>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:totalAmount > 0">
                 <ns18:presentIndicator xml:id="id_264">
                    <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.presentIndicator.0')]/@value"
                                  xml:id="id_265"/>
                 </ns18:presentIndicator>
               </xsl:if>
               <xsl:if test="not(/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:totalAmount > 0)">
                 <ns18:presentIndicator xml:id="id_264">
                    <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.presentIndicator.1')]/@value"
                                  xml:id="id_265"/>
                 </ns18:presentIndicator>
               </xsl:if>
               <ns18:returnScenario xml:id="id_189">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:returnScenario"
                                xml:id="id_190"/>
               </ns18:returnScenario>
               <ns18:serialNumber xml:id="id_271">
                  <xsl:value-of select="substring-before(/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:billNo, '-')"
                                xml:id="id_272"/>
               </ns18:serialNumber>
               <ns18:totalAmount xml:id="id_191">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:totalAmount"
                                xml:id="id_192"/>
               </ns18:totalAmount>
               <ns18:totalTaxValue xml:id="id_193">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:totalTaxValue"
                                xml:id="id_194"/>
               </ns18:totalTaxValue>
               <ns18:vendorId xml:id="id_195">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns18:vendorId"
                                xml:id="id_196"/>
               </ns18:vendorId>
               <xsl:for-each select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingProductCharge"
                             xml:id="id_200">
                  <tns:AppliedCustomerBillingProductCharge xml:id="id_201">
                     <ns10:conceptCode xml:id="id_214">
                        <xsl:value-of select="ns10:productId" xml:id="id_215"/>
                     </ns10:conceptCode>
                     <ns10:costCenter xml:id="id_279">
                        <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.costCenter')]/@value"
                                      xml:id="id_280"/>
                     </ns10:costCenter>
                     <ns10:discountAmount xml:id="id_204">
                        <xsl:value-of select="ns10:discountAmount" xml:id="id_205"/>
                     </ns10:discountAmount>
                     <ns10:discountPercentage xml:id="id_281">
                        <xsl:value-of select="100 * (ns10:discountAmount div ns10:totalItem)"
                                      xml:id="id_282"/>
                     </ns10:discountPercentage>
                     <ns10:itemDescription xml:id="id_202">
                        <xsl:value-of select="ns10:description" xml:id="id_203"/>
                     </ns10:itemDescription>
                     <ns10:itemNumber xml:id="id_206">
                        <xsl:value-of select="ns10:itemNumber" xml:id="id_207"/>
                     </ns10:itemNumber>
                     <ns10:itemPrice xml:id="id_208">
                        <xsl:value-of select="ns10:itemPrice" xml:id="id_209"/>
                     </ns10:itemPrice>
                     <ns10:itemType xml:id="id_290">P</ns10:itemType>
                     <ns10:netAmount xml:id="id_210">
                        <xsl:value-of select="ns10:netAmount" xml:id="id_211"/>
                     </ns10:netAmount>
                     <ns10:productId xml:id="id_212">
                        <xsl:value-of select="ns10:productId" xml:id="id_213"/>
                     </ns10:productId>
                     <ns10:quantityItem xml:id="id_216">
                        <xsl:value-of select="ns10:quantityItem" xml:id="id_217"/>
                     </ns10:quantityItem>
                     <ns10:serialNumber xml:id="id_218">
                        <xsl:value-of select="ns10:serialNumber" xml:id="id_219"/>
                     </ns10:serialNumber>
                     <ns10:taxCode xml:id="id_220">
                        <xsl:value-of select="ns10:taxCode" xml:id="id_221"/>
                     </ns10:taxCode>
                     <ns10:totalItem xml:id="id_222">
                        <xsl:value-of select="ns10:totalItem" xml:id="id_223"/>
                     </ns10:totalItem>
                  </tns:AppliedCustomerBillingProductCharge>
               </xsl:for-each>
               <tns:AppliedCustomerBillingRate xml:id="id_244">
                  <tns:AppliedCustomerBillingProductAlteration xml:id="id_245">
                     <ns40:amount xml:id="id_248">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:amount"
                                      xml:id="id_249"/>
                     </ns40:amount>
                     <ns40:compensatedAmount xml:id="id_254">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:compensatedAmount"
                                      xml:id="id_255"/>
                     </ns40:compensatedAmount>
                     <ns40:compensatedDocumentNumber xml:id="id_246">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:compensatedDocumentNumber"
                                      xml:id="id_247"/>
                     </ns40:compensatedDocumentNumber>
                     <ns40:compensatedDocumentStatus xml:id="id_260">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:compensatedDocumentStatus"
                                      xml:id="id_261"/>
                     </ns40:compensatedDocumentStatus>
                     <ns40:compensatedDocumentType xml:id="id_256">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:compensatedDocumentType"
                                      xml:id="id_257"/>
                     </ns40:compensatedDocumentType>
                     <ns40:documentStatus xml:id="id_258">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:documentStatus"
                                      xml:id="id_259"/>
                     </ns40:documentStatus>
                     <ns40:documentType xml:id="id_252">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:documentType"
                                      xml:id="id_253"/>
                     </ns40:documentType>
                     <ns40:referenceNumberForCompensation xml:id="id_250">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration/ns40:referenceNumberForCompensation"
                                      xml:id="id_251"/>
                     </ns40:referenceNumberForCompensation>
                  </tns:AppliedCustomerBillingProductAlteration>
               </tns:AppliedCustomerBillingRate>
               <tns:CustomerAccount xml:id="id_121">
                  <ns34:ID xml:id="id_122">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns34:ID"
                                   xml:id="id_123"/>
                  </ns34:ID>
                  <ns34:customerType xml:id="id_286">
                     <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.customerType')]/@value"
                                   xml:id="id_287"/>
                  </ns34:customerType>
                  <tns:BillingAccount xml:id="id_127">
                     <ns29:externalID xml:id="id_128">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:BillingAccount/ns29:externalID"
                                      xml:id="id_129"/>
                     </ns29:externalID>
                  </tns:BillingAccount>
                  <tns:Customer xml:id="id_130">
                     <tns:EmailContact xml:id="id_131">
                        <ns13:eMailAddress xml:id="id_132">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:Customer/ns0:EmailContact/ns13:eMailAddress"
                                         xml:id="id_133"/>
                        </ns13:eMailAddress>
                     </tns:EmailContact>
                     <tns:IndividualIdentification xml:id="id_150">
                        <ns28:number xml:id="id_151">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:IndividualIdentification/ns28:number"
                                         xml:id="id_152"/>
                        </ns28:number>
                        <ns28:type xml:id="id_153">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:IndividualIdentification/ns28:type"
                                         xml:id="id_154"/>
                        </ns28:type>
                        <tns:Individual xml:id="id_155">
                           <tns:IndividualName xml:id="id_156">
                              <ns44:firstName xml:id="id_157">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:IndividualName/ns44:firstName"
                                               xml:id="id_158"/>
                              </ns44:firstName>
                              <ns44:lastName xml:id="id_159">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:IndividualName/ns44:lastName"
                                               xml:id="id_160"/>
                              </ns44:lastName>
                           </tns:IndividualName>
                        </tns:Individual>
                     </tns:IndividualIdentification>
                     <tns:Skill xml:id="id_288">
                        <ns46:skillSpecification xml:id="id_289"></ns46:skillSpecification>
                     </tns:Skill>
                  </tns:Customer>
                  <tns:CustomerAccountBalance xml:id="id_124">
                     <ns63:contractAccount xml:id="id_125">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns34:externalID"
                                      xml:id="id_126"/>
                     </ns63:contractAccount>
                  </tns:CustomerAccountBalance>
                  <tns:CustomerAddress xml:id="id_134">
                     <tns:postalDeliveryAddress xml:id="id_135">
                        <ns17:address xml:id="id_136">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns17:address"
                                         xml:id="id_137"/>
                        </ns17:address>
                        <ns17:city xml:id="id_142">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns23:city"
                                         xml:id="id_143"/>
                        </ns17:city>
                        <ns17:commune xml:id="id_146">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns23:commune"
                                         xml:id="id_147"/>
                        </ns17:commune>
                     </tns:postalDeliveryAddress>
                  </tns:CustomerAddress>
                  <tns:CustomerComercialAddress xml:id="id_138">
                     <tns:geographicArea xml:id="id_139">
                        <ns23:city xml:id="id_140">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns23:city"
                                         xml:id="id_141"/>
                        </ns23:city>
                        <ns23:commune xml:id="id_144">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns23:commune"
                                         xml:id="id_145"/>
                        </ns23:commune>
                        <ns23:neighborhood xml:id="id_148">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns23:neighborhood"
                                         xml:id="id_149"/>
                        </ns23:neighborhood>
                     </tns:geographicArea>
                     <tns:streetName xml:id="id_235">
                        <ns50:name xml:id="id_236">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:ReceiverCompany/ns0:streetName/ns50:name"
                                         xml:id="id_237"/>
                        </ns50:name>
                     </tns:streetName>
                  </tns:CustomerComercialAddress>
               </tns:CustomerAccount>
               <tns:IssuingCompany xml:id="id_117">
                  <tns:CustomerOrder xml:id="id_118">
                     <ns8:ID xml:id="id_119">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerOrder/ns8:ID"
                                      xml:id="id_120"/>
                     </ns8:ID>
                  </tns:CustomerOrder>
                  <tns:individualIdentification xml:id="id_224">
                     <ns28:number xml:id="id_225">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:IssuingCompany/ns0:individualIdentification/ns28:number"
                                      xml:id="id_226"/>
                     </ns28:number>
                  </tns:individualIdentification>
                  <tns:organizationName xml:id="id_227">
                     <ns41:shortName xml:id="id_228">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName/ns41:shortName"
                                      xml:id="id_229"/>
                     </ns41:shortName>
                     <ns41:tradingName xml:id="id_230">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName/ns41:tradingName"
                                      xml:id="id_231"/>
                     </ns41:tradingName>
                  </tns:organizationName>
                  <tns:thirdPartyCollectionPM xml:id="id_232">
                     <ns16:thirdPartyID xml:id="id_233">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:IssuingCompany/ns0:thirdPartyCollectionPM/ns16:thirdPartyID"
                                      xml:id="id_234"/>
                     </ns16:thirdPartyID>
                  </tns:thirdPartyCollectionPM>
               </tns:IssuingCompany>
               <tns:LoyaltyTransaction xml:id="id_197">
                  <ns21:amount xml:id="id_198">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:LoyaltyTransaction/ns21:amount"
                                   xml:id="id_199"/>
                  </ns21:amount>
               </tns:LoyaltyTransaction>
               <tns:Rate xml:id="id_283">
                  <ns57:type xml:id="id_284">
                     <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns25:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns47:RequestHeader/ns47:Trace/ns47:Service/@code,'.PER_Publish_CollectionPostTransaction_OH.type')]/@value"
                                   xml:id="id_285"/>
                  </ns57:type>
               </tns:Rate>
               <tns:appliedAmount xml:id="id_238">
                  <ns15:amount xml:id="id_239">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:appliedAmount/ns15:amount"
                                   xml:id="id_240"/>
                  </ns15:amount>
               </tns:appliedAmount>
               <tns:unit xml:id="id_241">
                  <ns15:units xml:id="id_242">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:Money/ns15:units"
                                   xml:id="id_243"/>
                  </ns15:units>
               </tns:unit>
            </tns:CustomerBill>
         </tns:Body>
      </tns:Create_AppliedCustomerBillingCharge_REQ>
   </xsl:template>
</xsl:stylesheet>
