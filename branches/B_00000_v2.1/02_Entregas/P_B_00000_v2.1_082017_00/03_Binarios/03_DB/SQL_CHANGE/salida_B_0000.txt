SQL> ------------------------- SEQUENCES -------------------------
SQL> @.\01_Scripts\03_SQL_Sequences\CREATE_ESB_CONVERSATION_STATUS_SEQ.sql

Error que empieza en la l�nea : 1 Archivo @ file:\C:\ENTEL\Branch_ENTEL\B_00000_v2.1\02_Entregas\P_B_00000_v2.1_082017_00\03_Binarios\03_DB\SQL_CHANGE\.\01_Scripts\03_SQL_Sequences\CREATE_ESB_CONVERSATION_STATUS_SEQ.sql
del comando -
CREATE SEQUENCE "ESB_CONVERSATION_STATUS_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 START WITH 1 ORDER 
Informe de error -
Error SQL: ORA-00955: este nombre ya lo est� utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
SQL> ------------------------- TABLES -------------------------
SQL> @.\01_Scripts\01_SQL_Tables\ALTER_ESB_CONVERSATION_STATUS.sql

Error que empieza en la l�nea : 1 Archivo @ file:\C:\ENTEL\Branch_ENTEL\B_00000_v2.1\02_Entregas\P_B_00000_v2.1_082017_00\03_Binarios\03_DB\SQL_CHANGE\.\01_Scripts\01_SQL_Tables\ALTER_ESB_CONVERSATION_STATUS.sql
del comando -
ALTER TABLE ESB_CONVERSATION_STATUS
DROP COLUMN "UPDATE_TIMESTAMP"
Informe de error -
Error SQL: ORA-00904: "UPDATE_TIMESTAMP": identificador no v�lido
00904. 00000 -  "%s: invalid identifier"
*Cause:    
*Action:

Error que empieza en la l�nea : 4 Archivo @ file:\C:\ENTEL\Branch_ENTEL\B_00000_v2.1\02_Entregas\P_B_00000_v2.1_082017_00\03_Binarios\03_DB\SQL_CHANGE\.\01_Scripts\01_SQL_Tables\ALTER_ESB_CONVERSATION_STATUS.sql
del comando -
ALTER TABLE ESB_CONVERSATION_STATUS
 ADD ID number default ESB_CONVERSATION_STATUS_SEQ.nextval
Informe de error -
Error SQL: ORA-01430: la columna que se est� agregando ya existe en la tabla
01430. 00000 -  "column being added already exists in table"
*Cause:    
*Action:

Error que empieza en la l�nea : 7 Archivo @ file:\C:\ENTEL\Branch_ENTEL\B_00000_v2.1\02_Entregas\P_B_00000_v2.1_082017_00\03_Binarios\03_DB\SQL_CHANGE\.\01_Scripts\01_SQL_Tables\ALTER_ESB_CONVERSATION_STATUS.sql
del comando -
ALTER TABLE ESB_CONVERSATION_STATUS
	ADD	("S_STATUS" VARCHAR2(30 BYTE) DEFAULT 'PENDING',
    	"A_STATUS" VARCHAR2(30 BYTE) DEFAULT 'NONE', 
    	"S_END_TIME" TIMESTAMP(6) DEFAULT NULL,
    	"A_END_TIME" TIMESTAMP(6) DEFAULT NULL
    )
Informe de error -
Error SQL: ORA-01430: la columna que se est� agregando ya existe en la tabla
01430. 00000 -  "column being added already exists in table"
*Cause:    
*Action:

Table ESB_CONVERSATION_STATUS alterado.


Table ESB_CONVERSATION_STATUS alterado.

SQL> @.\01_Scripts\01_SQL_Tables\ADAPT_EXISTING_CNV_STATUS.sql

0 filas actualizadas.

SQL> ------------------------- INDEXES -------------------------
SQL> @.\01_Scripts\08_SQL_Indexes\CREATE_ESB_CONVERSATION_STATUS_INDEX.sql

Error que empieza en la l�nea : 1 Archivo @ file:\C:\ENTEL\Branch_ENTEL\B_00000_v2.1\02_Entregas\P_B_00000_v2.1_082017_00\03_Binarios\03_DB\SQL_CHANGE\.\01_Scripts\08_SQL_Indexes\CREATE_ESB_CONVERSATION_STATUS_INDEX.sql
del comando -
CREATE INDEX I_CONVERSATION_STATUS_CONV_ID ON ESB_CONVERSATION_STATUS(CONVERSATION_ID) COMPUTE STATISTICS
Informe de error -
Error SQL: ORA-00955: este nombre ya lo est� utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
SQL> ------------------------- TYPES -------------------------
SQL> @.\01_Scripts\02_SQL_Types\ALTER_TYPES.sql

Type CB_GRP_MEM_T_CUR borrado.


Type CB_GRP_MEM_T compilado


Type CB_GRP_MEM_T alterado.


Type CB_GRP_MEM_T_CUR compilado


Type CB_GRP_MEM_T_CUR alterado.


Type LOG_T compilado


Type LOG_T alterado.

SQL> ------------------------- PACKAGES ------------------------
SQL> @.\01_Scripts\04_SQL_Packages\CREATE_ESB_CONVERSATION_MANAGER_PKG_SPEC.sql

Package ESB_CONVERSATION_MANAGER_PKG compilado

SQL> @.\01_Scripts\04_SQL_Packages\CREATE_ESB_CONVERSATION_MANAGER_PKG_BODY.sql

Package body ESB_CONVERSATION_MANAGER_PKG compilado

SQL> @.\01_Scripts\04_SQL_Packages\CREATE_ESB_ERROR_HOSPITAL_PKG_BODY.sql

Package body ESB_ERROR_HOSPITAL_PKG compilado

SQL> @.\01_Scripts\04_SQL_Packages\CREATE_ESB_LOGGERMANAGER_PKG_SPEC.sql

Package ESB_LOGGERMANAGER_PKG compilado

SQL> @.\01_Scripts\04_SQL_Packages\CREATE_ESB_LOGGERMANAGER_PKG_BODY.sql

Package body ESB_LOGGERMANAGER_PKG compilado

SQL> ------------------------- DATA -------------------------
SQL> @.\01_Scripts\06_SQL_Data\FRW_CoreData\INSERT_ESB_LOG_TYPE.sql
SQL> SET DEFINE OFF
SQL> SET SERVEROUTPUT ON
SQL> WHENEVER SQLERROR CONTINUE ROLLBACK
SQL> DECLARE

BEGIN

	INSERT INTO ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) VALUES (ESB_LOG_TYPE_SEQ.NEXTVAL,'UCSRSP','Unconditional Callback Service Response','1');

END;
/
Procedimiento PL/SQL terminado correctamente.
SQL> COMMIT;
Confirmaci�n terminada.
SQL> SPOOL OFF;
