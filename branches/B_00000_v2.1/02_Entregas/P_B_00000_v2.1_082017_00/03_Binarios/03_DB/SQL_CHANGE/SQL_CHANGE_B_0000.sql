SPOOL salida_B_0000.txt

------------------------- SEQUENCES -------------------------
@.\01_Scripts\03_SQL_Sequences\CREATE_ESB_CONVERSATION_STATUS_SEQ.sql

------------------------- TABLES -------------------------
@.\01_Scripts\01_SQL_Tables\ALTER_ESB_CONVERSATION_STATUS.sql
@.\01_Scripts\01_SQL_Tables\ADAPT_EXISTING_CNV_STATUS.sql


------------------------- INDEXES -------------------------
@.\01_Scripts\08_SQL_Indexes\CREATE_ESB_CONVERSATION_STATUS_INDEX.sql

------------------------- TYPES -------------------------
@.\01_Scripts\02_SQL_Types\ALTER_TYPES.sql

------------------------- PACKAGES ------------------------
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_CONVERSATION_MANAGER_PKG_SPEC.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_CONVERSATION_MANAGER_PKG_BODY.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_ERROR_HOSPITAL_PKG_BODY.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_LOGGERMANAGER_PKG_SPEC.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_LOGGERMANAGER_PKG_BODY.sql

------------------------- DATA -------------------------
@.\01_Scripts\06_SQL_Data\FRW_CoreData\INSERT_ESB_LOG_TYPE.sql

SPOOL OFF;
