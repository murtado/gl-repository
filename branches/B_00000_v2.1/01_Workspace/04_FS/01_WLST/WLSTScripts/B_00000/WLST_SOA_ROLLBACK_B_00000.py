import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000'
wlstFileDomain='SOA'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+SOA_AdminServerListenAddress+":"+SOA_AdminServerListenPort

def startEditSession():
	connect(SOA_ConUsr, SOA_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

	disconnect()
	exit()

def deleteDeploymentPlanVariable(wlstPlan, adapterAppname, name):
	wlstPlan.destroyVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	wlstPlan.destroyVariable(name)

""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------- """
""" FRW_SOA_FileStore """
""" ------------------"""
def rollbackFileStore_FRW_SOA_FileStore():
	global fName

	fName = 'rollbackFileStore_FRW_SOA_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyFileStore(getMBean('/FileStores/FRW_SOA_FileStore'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')


""" ----------------- """
""" FRW_SOA_FileStore """
""" ------------------"""
def rollbackFileStore_FRW_SOA_RoutingManager_FileStores():
	global fName

	fName = 'rollbackFileStore_FRW_SOA_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	for managedServer in SOA_ManagedServerNames.split(',') :
		cmo.destroyFileStore(getMBean('/FileStores/FRW_SOA_RoutingManager_FileStore_'+managedServer))
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')


""" ----------------- """

""" ----------------- """
""" FRW_SOA_JMSServer """
""" ----------------- """

def rollbackJMSServer_FRW_SOA_RoutingManager_JMSServers():
	global fName

	fName = 'rollbackJMSServer_FRW_SOA_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	for managedServer in SOA_ManagedServerNames.split(',') :
		cmo.destroyJMSServer(getMBean('/JMSServers/FRW_SOA_RoutingManager_JMSServer_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')

""" ----------------- """
""" FRW_SOA_JMSServer """
""" ----------------- """
def rollbackJMSServer_FRW_SOA_JMSServer():
	global fName

	fName = 'rollbackJMSServer_FRW_SOA_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyJMSServer(getMBean('/JMSServers/FRW_SOA_JMSServer'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')

""" ----------------- """

""" ----------------- """
""" FRW_SOA_JMSModule """
""" ----------------- """
def rollbackJMSModule_FRW_SOA_JMSModule():
	global beanName

	fName = 'rollbackJMSModule_FRW_SOA_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/FRW_SOA_JMSModule'))


	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ------------------------ """
""" MessageDispatcherQueue """
""" ------------------------ """
def rollbackJMSQueue_MessageDispatcherQueue():
	global beanName

	fName = 'rollbackJMSQueue_MessageDispatcherQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/LoggerDiscardedQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/LoggerDiscardedErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """

""" ------------------------- """
""" ErrorHospital_XA_DCF """
""" ------------------------- """
def rollbackJMSConnFactory_ErrorHospital_XA_DCF():
	global beanName

	fName = 'rollbackJMSConnFactory_ErrorHospital_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ------------------ """
""" SupportComponentDS """
""" ------------------ """
def rollbackDatasource_SupportComponentDS():
	global beanName

	fName = 'rollbackDatasource_SupportComponentDS'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJDBCSystemResource(getMBean('/JDBCSystemResources/SupportComponentDS'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------- """

""" ------------------------------- """
""" SupportComponentsDSTx """
""" ------------------------------- """
def rollbackDatasource_SupportComponentDSNoTx():
	global beanName

	fName = 'rollbackDatasource_SupportComponentDSNoTx'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJDBCSystemResource(getMBean('/JDBCSystemResources/SupportComponentDSNoTx'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------------- """

""" ------------------------------ """
""" eis/DB/FRW/ErrorHospital """
""" ------------------------------ """
def rollbackDBAdapterCFactory_ErrorHospital():
	global fName

	fName = 'rollbackDBAdapterCFactory_ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='ErrorHospital'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/JMS/FRW/ErrorHospital """
""" ------------------------------ """
def rollbackJMSAdapterCFactory_ErrorHospital():
	global fName

	fName = 'rollbackJMSAdapterCFactory_ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	cfName='ErrorHospital'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')

""" ------------------------------ """
""" eis/JMS/FRW/RoutingManager """
""" ------------------------------ """
def rollbackJMSAdapterCFactory_FRW_RoutingManager():
	global fName

	fName = 'rollbackJMSAdapterCFactory_RoutingManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	cfName='FRW/RoutingManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName)
	#deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """



""" ------------------------------ """
""" eis/JMS/FRW/ConversationManager """
""" ------------------------------ """
def rollbackJMSAdapterCFactory_FRW_ConversationManager():
	global fName

	fName = 'rollbackJMSAdapterCFactory_RoutingManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	cfName='FRW/ConversationManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName)
	#deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/JMS/SALESFORCE """
""" ------------------------------ """
def rollbackJMSAdapterCFactory_SALESFORCE():
	global fName

	fName = 'rollbackJMSAdapterCFactory_SALESFORCE'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	cfName='SALESFORCE'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	#-------------------------------------------------------------------------------
	# Service Template Resources --->
	#-------------------------------------------------------------------------------

	#-------------------------------------------------------------------------------
	# Framework Resources --->
	#-------------------------------------------------------------------------------

	# JMS Modules
	rollbackJMSModule_FRW_SOA_JMSModule()

	# JMS Servers
	rollbackJMSServer_FRW_SOA_JMSServer()
	rollbackJMSServer_FRW_SOA_RoutingManager_JMSServers()

	# Persistent Stores
	rollbackFileStore_FRW_SOA_FileStore()
	rollbackFileStore_FRW_SOA_RoutingManager_FileStores()

	# DB Datasources
	rollbackDatasource_SupportComponentDS()
	rollbackDatasource_SupportComponentDSNoTx()

	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	#-------------------------------------------------------------------------------
	# Service Template Applications --->
	#-------------------------------------------------------------------------------

	# JMSAdapter Connection Factories
	rollbackJMSAdapterCFactory_SALESFORCE()

	#-------------------------------------------------------------------------------
	# Framework Applications --->
	#-------------------------------------------------------------------------------

	# DBAdapter Connection Factories
	rollbackDBAdapterCFactory_ErrorHospital()

	# JMSAdapter Connection Factories
	rollbackJMSAdapterCFactory_ErrorHospital()
	rollbackJMSAdapterCFactory_FRW_RoutingManager()
	rollbackJMSAdapterCFactory_FRW_ConversationManager()

	""" @@@ """

	validate()
	endEditSession('OK')

except Exception, e:
	debugFile.write('\nOUTCOME of ' + str(e) + ' : ERROR! ')
	dumpStack()
	endEditSession('ERROR')
