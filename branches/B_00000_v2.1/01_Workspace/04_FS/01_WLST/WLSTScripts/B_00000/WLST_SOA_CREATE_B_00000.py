import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000'
wlstFileDomain='SOA'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+SOA_AdminServerListenAddress+":"+SOA_AdminServerListenPort

# If database uses services name, you must change last : for /
if (frwDBSchemaName!=''):
	frwDBConURL_nXA=frwDBUrlDriverCName_nXA+"@"+frwDBListerAddress+":"+frwDBListerPort+":"+frwDBSchemaName
	frwDBConURL_XA=frwDBUrlDriverCName_XA+"@"+frwDBListerAddress+":"+frwDBListerPort+":"+frwDBSchemaName
else:
	frwDBConURL_nXA=frwDBUrlDriverCName_nXA+"@"+frwDBListerAddress+":"+frwDBListerPort+"/"+frwDBServiceName
	frwDBConURL_XA=frwDBUrlDriverCName_XA+"@"+frwDBListerAddress+":"+frwDBListerPort+"/"+frwDBServiceName

#Local JMS Artifacts
SOA_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+SOA_ManagedServerListenURLs+';java.naming.security.principal='+SOA_ConUsr+';java.naming.security.credentials='+SOA_ConPsw

#------------------------------------------
#Shared JMS Artifacts (with OSB Domain)
#------------------------------------------
if (isTwoDomains=='true'):
	sharedUsr=OSB_SOA_crossDomainUsr
	sharedPsw=OSB_SOA_crossDomainPsw
else:
	sharedUsr=OSB_ConUsr
	sharedPsw=OSB_ConPsw

OSB_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+OSB_ManagedServerListenURLs+';java.naming.security.principal='+sharedUsr+';java.naming.security.credentials='+sharedPsw
#------------------------------------------

def startEditSession():
	connect(SOA_ConUsr, SOA_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

	disconnect()
	exit()

def makeDeploymentPlanVariable(wlstPlan, adapterAppname, name, value, xpath, origin='planbased'):
	variableAssignment = wlstPlan.createVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	variableAssignment.setXpath(xpath)
	variableAssignment.setOrigin(origin)
	wlstPlan.createVariable(name, value)

""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------- """
""" FRW_SOA_FileStore """
""" ----------------- """
def createFileStore_FRW_SOA_FileStore():
	global fName

	fName = 'createFileStore_FRW_SOA_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createFileStore('FRW_SOA_FileStore')

	cd('/FileStores/FRW_SOA_FileStore')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" --------------------------------- """
""" FRW_SOA_RoutingManager_FileStores """
""" --------------------------------- """
def createFileStore_FRW_SOA_RoutingManager_FileStores():
	global fName

	fName = 'createFileStore_FRW_SOA_RoutingManager_FileStores'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	debugFile.write('\nStarting the Execution of Function : ' + SOA_ManagedServerNames)

	for managedServer in SOA_ManagedServerNames.split(',') :
		debugFile.write('\nStarting the Execution of Function : ' + managedServer)
		cd('/')
		cmo.createFileStore('FRW_SOA_RoutingManager_FileStore_'+managedServer)
		cd('/FileStores/FRW_SOA_RoutingManager_FileStore_'+managedServer)
		set('Targets',jarray.array([ObjectName('com.bea:Name='+managedServer+',Type=Server')], ObjectName))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_SOA_JMSServer """
""" ----------------- """
def createJMSServer_FRW_SOA_JMSServer():
	global fName

	fName = 'createJMSServer_FRW_SOA_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJMSServer('FRW_SOA_JMSServer')

	cd('/JMSServers/FRW_SOA_JMSServer')
	cmo.setPersistentStore(getMBean('/FileStores/FRW_SOA_FileStore'))
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" --------------------------------- """
""" FRW_SOA_RoutingManager_JMSServers """
""" --------------------------------- """
def createJMSServer_FRW_SOA_RoutingManager_JMSServers():
	global fName

	fName = 'createJMSServer_FRW_SOA_RoutingManager_JMSServers'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	debugFile.write('\nStarting the Execution of Function : ' + SOA_ManagedServerNames)


	for managedServer in SOA_ManagedServerNames.split(',') :
		cd('/')
		cmo.createJMSServer('FRW_SOA_RoutingManager_JMSServer_'+managedServer)

		cd('/JMSServers/FRW_SOA_RoutingManager_JMSServer_'+managedServer)
		cmo.setPersistentStore(getMBean('/FileStores/FRW_SOA_RoutingManager_FileStore_'+managedServer))

		set('Targets',jarray.array([ObjectName('com.bea:Name='+managedServer+',Type=Server')], ObjectName))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_SOA_JMSModule """
""" ----------------- """
def createJMSModule_FRW_SOA_JMSModule():
	global fName

	fName = 'createJMSModule_FRW_SOA_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJMSSystemResource('FRW_SOA_JMSModule')

	cd('/JMSSystemResources/FRW_SOA_JMSModule')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))

	cmo.createSubDeployment('FRW_SOA_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/SubDeployments/FRW_SOA_SubDeploy')
	set('Targets',jarray.array([ObjectName('com.bea:Name=FRW_SOA_JMSServer,Type=JMSServer')], ObjectName))

	""" ---------------------------------------------------------------------------------------------------------- """
	""" FRW_SOA_RoutingManager_SubDeploy """
	""" ---------------------------------------------------------------------------------------------------------- """
	cd('/JMSSystemResources/FRW_SOA_JMSModule')

	cmo.createSubDeployment('FRW_SOA_RoutingManager_SubDeploy')
	loggerSubDeploy = getMBean('/JMSSystemResources/FRW_SOA_JMSModule/SubDeployments/FRW_SOA_RoutingManager_SubDeploy')

	cd('/')
	for managedServer in SOA_ManagedServerNames.split(',') :
		jmsServer = getMBean('/JMSServers/FRW_SOA_RoutingManager_JMSServer_'+managedServer)
		loggerSubDeploy.addTarget(jmsServer)
	""" ---------------------------------------------------------------------------------------------------------- """

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ------------------------ """
""" MessageDispatcherQueue """
""" ------------------------ """
def createJMSQueue_MessageDispatcherQueue():
	global fName

	fName = 'createJMSQueue_MessageDispatcherQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('MessageDispatcherQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/MessageDispatcherQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('MessageDispatcherErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherErrorQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/MessageDispatcherErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue/DeliveryFailureParams/MessageDispatcherQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue/DeliveryParamsOverrides/MessageDispatcherQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue/DeliveryFailureParams/MessageDispatcherQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherErrorQueue'))


	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" -------------------------- """
""" RoutingManagerREQQueue     """
""" -------------------------- """
def createJMSQueue_RoutingManagerREQQueue():
	global fName

	fName = 'createJMSQueue_RoutingManagerREQQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	""" Error Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingManagerREQErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerREQErrorQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingManagerREQErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerREQErrorQueue/DeliveryFailureParams/RoutingManagerREQErrorQueue')
	cmo.setExpirationPolicy('Discard')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerREQErrorQueue/DeliveryParamsOverrides/RoutingManagerREQErrorQueue')
	cmo.setRedeliveryDelay(-1)
	cmo.setTimeToLive(432000000) # 5 Days

	""" Standard Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingManagerREQQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerREQQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingManagerREQQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(2)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerREQQueue/DeliveryFailureParams/RoutingManagerREQQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(3)
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerREQErrorQueue'))

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerREQQueue/DeliveryParamsOverrides/RoutingManagerREQQueue')
	cmo.setRedeliveryDelay(2000)
	cmo.setTimeToLive(172800000)# 2 Day
	#cmo.setTimeToDeliver(50)

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------------- """

""" -------------------------- """
""" RoutingControlREQQueue     """
""" -------------------------- """
def createJMSQueue_RoutingControlREQQueue():
	global fName

	fName = 'createJMSQueue_RoutingControlREQQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	""" Error Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingControlREQErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlREQErrorQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingControlREQErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlREQErrorQueue/DeliveryFailureParams/RoutingControlREQErrorQueue')
	cmo.setExpirationPolicy('Discard')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlREQErrorQueue/DeliveryParamsOverrides/RoutingControlREQErrorQueue')
	cmo.setRedeliveryDelay(-1)
	cmo.setTimeToLive(432000000) # 5 Days

	""" Standard Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingControlREQQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlREQQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingControlREQQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(2)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlREQQueue/DeliveryFailureParams/RoutingControlREQQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(3)
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlREQErrorQueue'))

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlREQQueue/DeliveryParamsOverrides/RoutingControlREQQueue')
	cmo.setRedeliveryDelay(2000)
	cmo.setTimeToLive(172800000)# 2 Day
	#cmo.setTimeToDeliver(50)

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------------- """

""" -------------------------- """
""" RoutingManagerRSPQueue     """
""" -------------------------- """
def createJMSQueue_RoutingManagerRSPQueue():
	global fName

	fName = 'createJMSQueue_RoutingManagerRSPQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	""" Error Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingManagerRSPErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerRSPErrorQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingManagerRSPErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerRSPErrorQueue/DeliveryFailureParams/RoutingManagerRSPErrorQueue')
	cmo.setExpirationPolicy('Discard')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerRSPErrorQueue/DeliveryParamsOverrides/RoutingManagerRSPErrorQueue')
	cmo.setRedeliveryDelay(-1)
	cmo.setTimeToLive(432000000) # 5 Days

	""" Standard Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingManagerRSPQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerRSPQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingManagerRSPQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(2)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerRSPQueue/DeliveryFailureParams/RoutingManagerRSPQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(10)
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerRSPErrorQueue'))


	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingManagerRSPQueue/DeliveryParamsOverrides/RoutingManagerRSPQueue')
	cmo.setRedeliveryDelay(6000)
	cmo.setTimeToLive(172800000) # 2 Days
	#cmo.setTimeToDeliver(50)

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------------- """

""" -------------------------- """
""" RoutingControlRSPQueue     """
""" -------------------------- """
def createJMSQueue_RoutingControlRSPQueue():
	global fName

	fName = 'createJMSQueue_RoutingControlRSPQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	""" Error Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingControlRSPErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlRSPErrorQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingControlRSPErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlRSPErrorQueue/DeliveryFailureParams/RoutingControlRSPErrorQueue')
	cmo.setExpirationPolicy('Discard')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlRSPErrorQueue/DeliveryParamsOverrides/RoutingControlRSPErrorQueue')
	cmo.setRedeliveryDelay(-1)
	cmo.setTimeToLive(432000000) # 5 Days

	""" Standard Queue """
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RoutingControlRSPQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlRSPQueue')
	cmo.setJNDIName('/jms/FRW/RoutingManager/RoutingControlRSPQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(2)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlRSPQueue/DeliveryFailureParams/RoutingControlRSPQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(10)
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlRSPErrorQueue'))


	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RoutingControlRSPQueue/DeliveryParamsOverrides/RoutingControlRSPQueue')
	cmo.setRedeliveryDelay(6000)
	cmo.setTimeToLive(172800000) # 2 Days
	#cmo.setTimeToDeliver(50)

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------------- """

""" -------------------- """
""" ErrorHospital_XA_DCF """
""" -------------------- """
def createJMSConnFactory_ErrorHospital_XA_DCF():
	global fName

	fName = 'createJMSConnFactory_ErrorHospital_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createConnectionFactory('ErrorHospital_XA_DCF')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/DefaultXAConnFactory')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF/SecurityParams/ErrorHospital_XA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF/ClientParams/ErrorHospital_XA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF/TransactionParams/ErrorHospital_XA_DCF')
	cmo.setXAConnectionFactoryEnabled(true)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------- """

""" ------------------------- """
""" RoutingManager_XA_DCF """
""" ------------------------- """
def createJMSConnFactory_FRW_RoutingManager_XA_DCF():
	global fName
	fName = 'createJMSConnFactory_FRW_RoutingManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	try:
		cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/RoutingManager_XA_DCF')
		debugFile.write('\n     ConnectionFactories:RoutingManager_XA_DCF  already exists')
	except Exception:
		debugFile.write('\n      ConnectionFactories:RoutingManager_XA_DCF  creation')
		cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
		cmo.createConnectionFactory('RoutingManager_XA_DCF')
		cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/RoutingManager_XA_DCF')
		cmo.setJNDIName('/jms/FRW/RoutingManager/DefaultXAConnFactory')
		cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/RoutingManager_XA_DCF/SecurityParams/RoutingManager_XA_DCF')
		cmo.setAttachJMSXUserId(false)
		cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/RoutingManager_XA_DCF/ClientParams/RoutingManager_XA_DCF')
		cmo.setClientIdPolicy('Restricted')
		cmo.setSubscriptionSharingPolicy('Exclusive')
		cmo.setMessagesMaximum(10)
		cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/RoutingManager_XA_DCF/TransactionParams/RoutingManager_XA_DCF')
		cmo.setXAConnectionFactoryEnabled(true)
		cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/RoutingManager_XA_DCF')
		cmo.setDefaultTargetingEnabled(false)
		cmo.setSubDeploymentName('FRW_SOA_RoutingManager_SubDeploy')
		debugFile.write('\n      ConnectionFactories:RoutingManager creation finish')


	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" ------------------ """
""" SupportComponentDS """
""" ------------------ """
def createDatasource_SupportComponentDS():
	global fName

	fName = 'createDatasource_SupportComponentDS'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJDBCSystemResource('SupportComponentDS')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS')
	cmo.setName('SupportComponentDS')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDataSourceParams/SupportComponentDS')
	set('JNDINames',jarray.array([String('/jdbc/FRW/SupportComponentsDSTx')], String))

	#cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS')
	#cmo.setDatasourceType('Generic')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS')

	cmo.setUrl(frwDBConURL_XA)
	cmo.setDriverName(frwDBDriverName_XA)
	cmo.setPassword(frwDBPass)

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS')
	cmo.createProperty('user')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS/Properties/user')
	cmo.setValue(frwDBUser)


	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDataSourceParams/SupportComponentDS')
	cmo.setGlobalTransactionsProtocol('TwoPhaseCommit')

	cd('/JDBCSystemResources/SupportComponentDS')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ---------------------- """
""" SupportComponentDSNoTx """
""" ---------------------- """
def createDatasource_SupportComponentDSNoTx():
	global fName

	fName = 'createDatasource_SupportComponentDSNoTx'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJDBCSystemResource('SupportComponentDSNoTx')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx')
	cmo.setName('SupportComponentDSNoTx')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDataSourceParams/SupportComponentDSNoTx')
	set('JNDINames',jarray.array([String('/jdbc/FRW/SupportComponentsDSNoTx')], String))

	#cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx')
	#cmo.setDatasourceType('Generic')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx')

	cmo.setUrl(frwDBConURL_nXA)
	cmo.setDriverName(frwDBDriverName_nXA)
	cmo.setPassword(frwDBPass)

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx')
	cmo.createProperty('user')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx/Properties/user')
	cmo.setValue(frwDBUser)

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDataSourceParams/SupportComponentDSNoTx')
	cmo.setGlobalTransactionsProtocol('None')

	cd('/JDBCSystemResources/SupportComponentDSNoTx')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ErrorHospital """
""" ------------------------------ """
def createDBAdapterCFactory_ErrorHospital():
	global fName

	fName = 'createDBAdapterCFactory_ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	dsName='SupportComponentsDSTx'
	dsJNDI='jdbc/FRW/SupportComponentsDSTx'

	cfName='ErrorHospital'
	eisName='eis/DB/FRW/' + cfName

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="xADataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/JMS/FRW/ErrorHospital """
""" ------------------------------ """
def createJMSAdapterCFactory_ErrorHospital():
	global fName

	fName = 'createJMSAdapterCFactory_ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	connFactoryJNDI='/jms/FRW/ErrorManager/ErrorHospital/DefaultXAConnFactory'

	cfName='ErrorHospital'
	eisName='eis/JMS/FRW/' + cfName

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName, connFactoryJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ConnectionFactoryLocation"]/value')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName, SOA_JmsAdapterFactoryProperties, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="FactoryProperties"]/value')

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" -------------------------- """
""" eis/JMS/FRW/RoutingManager """
""" -------------------------- """
def createJMSAdapterCFactory_FRW_RoutingManager():
	global fName

	fName = 'createJMSAdapterCFactory_FRW_RoutingManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	connFactoryJNDI='/jms/FRW/RoutingManager/DefaultXAConnFactory'

	cfName='FRW/RoutingManager'
	eisName='eis/JMS/' + cfName

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName, connFactoryJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ConnectionFactoryLocation"]/value')
	#makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName, OSB_JmsAdapterFactoryProperties, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="FactoryProperties"]/value')

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------- """
""" eis/JMS/FRW/ConversationManager """
""" ------------------------------- """
def createJMSAdapterCFactory_FRW_ConversationManager():
	global fName

	fName = 'createJMSAdapterCFactory_FRW_ConversationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	connFactoryJNDI='/jms/FRW/ConversationManager/DefaultXAConnFactory'

	cfName='FRW/ConversationManager'
	eisName='eis/JMS/' + cfName

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName, connFactoryJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ConnectionFactoryLocation"]/value')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName, OSB_JmsAdapterFactoryProperties, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="FactoryProperties"]/value')

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/JMS/SALESFORCE """
""" ------------------------------ """
def createJMSAdapterCFactory_SALESFORCE():
	global fName

	fName = 'createJMSAdapterCFactory_SALESFORCE'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName

	connFactoryJNDI='/jms/RA/SALESFORCE/DefaultXAConnFactory'

	cfName='SALESFORCE'
	eisName='eis/JMS/' + cfName

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName, connFactoryJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ConnectionFactoryLocation"]/value')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName, OSB_JmsAdapterFactoryProperties, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="FactoryProperties"]/value')

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	#-------------------------------------------------------------------------------
	# Framework Resources --->
	#-------------------------------------------------------------------------------

	# DB Datasources
	createDatasource_SupportComponentDSNoTx()
	createDatasource_SupportComponentDS()

	# Persistent Stores
	createFileStore_FRW_SOA_FileStore()
	createFileStore_FRW_SOA_RoutingManager_FileStores()

	# JMS Servers
	createJMSServer_FRW_SOA_JMSServer()
	createJMSServer_FRW_SOA_RoutingManager_JMSServers()

	# JMS Modules
	createJMSModule_FRW_SOA_JMSModule()

	# JMS Queues
	createJMSQueue_MessageDispatcherQueue()
	createJMSQueue_RoutingManagerREQQueue()
	createJMSQueue_RoutingManagerRSPQueue()
	createJMSQueue_RoutingControlREQQueue()
	createJMSQueue_RoutingControlRSPQueue()

	# JMS Connection Factories
	createJMSConnFactory_ErrorHospital_XA_DCF()
	createJMSConnFactory_FRW_RoutingManager_XA_DCF()

	#-------------------------------------------------------------------------------
	# Service Template Resources --->
	#-------------------------------------------------------------------------------

	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	#-------------------------------------------------------------------------------
	# Framework Applications --->
	#-------------------------------------------------------------------------------

	# DBAdapter Connection Factories
	createDBAdapterCFactory_ErrorHospital()

	# JMSAdapter Connection Factories
	createJMSAdapterCFactory_ErrorHospital()
	createJMSAdapterCFactory_FRW_RoutingManager()
	createJMSAdapterCFactory_FRW_ConversationManager()

	#-------------------------------------------------------------------------------
	# Service Template Applications --->
	#-------------------------------------------------------------------------------

	# JMSAdapter Connection Factories
	createJMSAdapterCFactory_SALESFORCE()

	""" @@@ """

	validate()
	endEditSession('OK')

except Exception, e:
	debugFile.write('\nOUTCOME of ' + str(e) + ' : ERROR! ')
	dumpStack()
	endEditSession('ERROR')
