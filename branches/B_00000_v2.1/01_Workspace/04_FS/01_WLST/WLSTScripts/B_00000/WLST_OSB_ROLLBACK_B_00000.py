import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000'
wlstFileDomain='OSB'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

	disconnect()
	exit()

def deleteDeploymentPlanVariable(wlstPlan, adapterAppname, name):
	wlstPlan.destroyVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	wlstPlan.destroyVariable(name)

""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ------------------- """
""" FRW_OSB_FileStore   """
""" ------------------- """
def rollbackFileStore_FRW_OSB_FileStore():
	global fName

	fName = 'rollbackFileStore_FRW_OSB_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyFileStore(getMBean('/FileStores/FRW_OSB_FileStore'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------ """
""" FRW_OSB_Logger_FileStore """
""" ------------------------ """
def rollbackFileStore_FRW_OSB_Logger_FileStores():
	global fName

	fName = 'rollbackFileStore_FRW_OSB_Logger_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyFileStore(getMBean('/FileStores/FRW_OSB_Logger_FileStore_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------------ """
""" FRW_OSB_Conversation_FileStore """
""" ------------------------------ """
def rollbackFileStore_FRW_OSB_Vitality_FileStores():
	global fName

	fName = 'rollbackFileStore_FRW_OSB_Vitality_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyFileStore(getMBean('/FileStores/FRW_OSB_Vitality_FileStore_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------------ """
""" FRW_OSB_Conversation_FileStore """
""" ------------------------------ """
def rollbackFileStore_FRW_OSB_Conversation_FileStores():
	global fName

	fName = 'rollbackFileStore_FRW_OSB_Conversation_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyFileStore(getMBean('/FileStores/FRW_OSB_Conversation_FileStore_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_OSB_Correlation_FileStore """
""" ------------------- """
def rollbackFileStore_FRW_OSB_Correlation_FileStores():
	global fName

	fName = 'rollbackFileStore_FRW_OSB_Correlation_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyFileStore(getMBean('/FileStores/FRW_OSB_Correlation_FileStore_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_OSB_JMSServer """
""" ----------------- """
def rollbackJMSServer_FRW_OSB_JMSServer():
	global fName
	fName = 'rollbackJMSServer_FRW_OSB_JMSServer'

	cmo.destroyJMSServer(getMBean('/JMSServers/FRW_OSB_JMSServer'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------ """
""" FRW_OSB_Logger_JMSServer """
""" ------------------------ """
def rollbackJMSServer_FRW_OSB_Logger_JMSServers():
	global fName
	fName = 'rollbackJMSServer_FRW_OSB_Logger_JMSServer'

	cd('/')

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyJMSServer(getMBean('/JMSServers/FRW_OSB_Logger_JMSServer_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------ """
""" FRW_OSB_Logger_JMSServer """
""" ------------------------ """
def rollbackJMSServer_FRW_OSB_Vitality_JMSServers():
	global fName
	fName = 'rollbackJMSServer_FRW_OSB_Vitality_JMSServer'

	cd('/')

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyJMSServer(getMBean('/JMSServers/FRW_OSB_Vitality_JMSServer_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------------ """
""" FRW_OSB_Conversation_JMSServer """
""" ------------------------------ """
def rollbackJMSServer_FRW_OSB_Conversation_JMSServers():
	global fName
	fName = 'rollbackJMSServer_FRW_OSB_Conversation_JMSServer'

	cd('/')

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyJMSServer(getMBean('/JMSServers/FRW_OSB_Conversation_JMSServer_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------------------- """
""" FRW_OSB_Correlation_JMSServer """
""" ----------------------------- """
def rollbackJMSServer_FRW_OSB_Correlation_JMSServers():
	global fName
	fName = 'rollbackJMSServer_FRW_OSB_Correlation_JMSServer'

	cd('/')

	for managedServer in OSB_ManagedServerNames.split(',') :
		cmo.destroyJMSServer(getMBean('/JMSServers/FRW_OSB_Correlation_JMSServer_'+managedServer))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------ """
""" SALESFORCE_OSB_JMSServer """
""" ------------------------ """
def rollbackJMSServer_SALESFORCE_OSB_JMSServer():
	global fName

	fName = 'rollbackJMSServer_SALESFORCE_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyJMSServer(getMBean('/JMSServers/SALESFORCE_OSB_JMSServer'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" ------------------- """
""" DUMMY_OSB_JMSServer """
""" ------------------- """
def rollbackJMSServer_DUMMY_OSB_JMSServer():
	global fName

	fName = 'rollbackJMSServer_DUMMY_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyJMSServer(getMBean('/JMSServers/DUMMY_OSB_JMSServer'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ----------------- """
""" FRW_OSB_JMSModule """
""" ----------------- """
def rollbackJMSModule_FRW_OSB_JMSModule():
	global beanName

	fName = 'rollbackJMSModule_FRW_OSB_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/FRW_OSB_JMSModule'))


	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------- """

""" ----------------------- """
""" SALESFORCE_RA_JMSModule """
""" ----------------------- """
def rollbackJMSModule_SALESFORCE_RA_JMSModule():
	global beanName

	fName = 'rollbackJMSModule_SALESFORCE_RA_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule'))


	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ------------------- """
""" DUMMY_OSB_JMSModule """
""" ------------------- """
def rollbackJMSModule_DUMMY_OSB_JMSModule():
	global beanName

	fName = 'rollbackJMSModule_DUMMY_OSB_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule'))


	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" --------------------------- """
""" VitalityTestQueue """
""" --------------------------- """
def rollbackJMSQueue_VitalityTestQueue():
	global beanName

	fName = 'rollbackJMSQueue_VitalityTestQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/VitalityTestQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------- """


""" --------------------------- """
""" ConversationManagerREQQueue """
""" --------------------------- """
def rollbackJMSQueue_ConversationManagerREQQueue():
	global beanName

	fName = 'rollbackJMSQueue_ConversationManagerREQQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerREQQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerREQErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------- """

""" --------------------------- """
""" ConversationManagerRSPQueue """
""" --------------------------- """
def rollbackJMSQueue_ConversationManagerRSPQueue():
	global beanName

	fName = 'rollbackJMSQueue_ConversationManagerRSPQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerRSPQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerRSPErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------- """

""" --------------------- """
""" LoggerEnterpriseQueue """
""" --------------------- """
def rollbackJMSQueue_LoggerEnterpriseQueue():
	global beanName

	fName = 'rollbackJMSQueue_LoggerEnterpriseQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" -------------------------- """
""" CorrelationManagerREQQueue """
""" -------------------------- """
def rollbackJMSQueue_CorrelationManagerREQQueue():
	global beanName

	fName = 'rollbackJMSQueue_CorrelationManagerREQQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/CorrelationManagerREQQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/CorrelationManagerREQErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------- """

""" -------------------------- """
""" CorrelationManagerRSPQueue """
""" -------------------------- """
def rollbackJMSQueue_CorrelationManagerRSPQueue():
	global beanName

	fName = 'rollbackJMSQueue_CorrelationManagerRSPQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/CorrelationManagerRSPQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/CorrelationManagerRSPErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------- """

""" ---------------------- """
""" UPD-CLI-DATA_REQ_Queue """
""" ---------------------- """
def rollbackJMSQueue_UPDCLIDATA_REQ_Queue():
	global beanName

	fName = 'rollbackJMSQueue_UPDCLIDATA_REQ_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/ConversationManagerErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" ---------------------- """
""" UPD-CLI-DATA_RSP_Queue """
""" ---------------------- """
def rollbackJMSQueue_UPDCLIDATA_RSP_Queue():
	global beanName

	fName = 'rollbackJMSQueue_UPDCLIDATA_RSP_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_ErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" --------------------------------------------- """
""" SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue """
""" --------------------------------------------- """
def rollbackJMSQueue_SALESFORCE_CRM_UPDCLIDATA_Sync_RA_REQ_Queue():
	global beanName

	fName = 'rollbackJMSQueue_SALESFORCE_CRM_UPDCLIDATA_Sync_RA_REQ_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_REQ_ErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------------------------------- """

""" --------------------------------------------- """
""" SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue """
""" --------------------------------------------- """
def rollbackJMSQueue_SALESFORCE_CRM_UPDCLIDATA_Sync_RA_RSP_Queue():
	global beanName

	fName = 'rollbackJMSQueue_SALESFORCE_CRM_UPDCLIDATA_Sync_RA_RSP_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_RSP_ErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------------------------------- """

""" ------------------------- """
""" ConversationManagerXA_DCF """
""" ------------------------- """
def rollbackJMSConnFactory_ConversationManagerXA_DCF():
	global beanName

	fName = 'rollbackJMSConnFactory_ConversationManagerXA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/ConversationManagerXA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" --------------------- """
""" LoggerManager_NXA_DCF """
""" --------------------- """
def rollbackJMSConnFactory_LoggerManager_NXA_DCF():
	global beanName

	fName = 'rollbackJMSConnFactory_LoggerManager_NXA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_NXA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" -------------------- """
""" LoggerManager_XA_DCF """
""" -------------------- """
def rollbackJMSConnFactory_LoggerManager_XA_DCF():
	global beanName

	fName = 'rollbackJMSConnFactory_LoggerManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_XA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------- """
""" CorrelationManager_XA_DCF """
""" ------------------------- """
def rollbackJMSConnFactory_CorrelationManager_XA_DCF():
	global beanName

	fName = 'rollbackJMSConnFactory_CorrelationManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ------------------------- """
""" CorrelationManager_XA_DCF """
""" ------------------------- """
def rollbackJMSConnFactory_CorrelationManager_XA_DCF():
	global beanName

	fName = 'rollbackJMSConnFactory_CorrelationManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ------------------ """
""" SupportComponentDS """
""" ------------------ """
def rollbackDatasource_SupportComponentDS():
	global beanName

	fName = 'rollbackDatasource_SupportComponentDS'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJDBCSystemResource(getMBean('/JDBCSystemResources/SupportComponentDS'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------- """

""" ------------------------------- """
""" SupportComponentsDSTx """
""" ------------------------------- """
def rollbackDatasource_SupportComponentDSNoTx():
	global beanName

	fName = 'rollbackDatasource_SupportComponentDSNoTx'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.destroyJDBCSystemResource(getMBean('/JDBCSystemResources/SupportComponentDSNoTx'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------------ """

""" ------------------------------ """
""" eis/DB/FRW/FrameworkDataManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory_FrameworkDataManager():
	global fName

	fName = 'rollbackDBAdapterCFactory_FrameworkDataManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='FrameworkDataManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ConversationManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory_ConversationManager():
	global fName

	fName = 'rollbackDBAdapterCFactory_ConversationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='ConversationManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ErrorManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory_ErrorManager():
	global fName

	fName = 'rollbackDBAdapterCFactory_ErrorManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='ErrorManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/LoggerManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory_LoggerManager():
	global fName

	fName = 'rollbackDBAdapterCFactory_LoggerManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='LoggerManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/MessageManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory_MessageManager():
	global fName

	fName = 'rollbackDBAdapterCFactory_MessageManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='MessageManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ParameterManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory_ParameterManager():
	global fName

	fName = 'rollbackDBAdapterCFactory_ParameterManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='ParameterManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/CorrelationManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory_CorrelationManager():
	global fName

	fName = 'rollbackDBAdapterCFactory_CorrelationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='CorrelationManager'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ErrorHospital """
""" ------------------------------ """
def rollbackDBAdapterCFactory_ErrorHospital():
	global fName

	fName = 'rollbackDBAdapterCFactory_ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName

	cfName='ErrorHospital'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/Coherence/Entel  """
""" ------------------------------ """
def rollbackCOHAdapterCFactory_Entel():
	global fName

	fName = 'rollbackDBAdapterCFactory_CorrelationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	appPath = soaConnectorsAppDir + '/' + coherenceAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + coherenceAdapterPlanName

	cfName='Entel'

	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConnectionInstance_eis/Coherence/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConfigProperty_CacheConfigLocation_Value_' + cfName)
	deleteDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConfigProperty_ServiceName_Value_' + cfName)

	plan.save();

	cd('/AppDeployments/'+coherenceAdapterAppName+'/Targets');
	redeploy(coherenceAdapterAppName, planPath, targets=cmo.getTargets());

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	#-------------------------------------------------------------------------------
	# Service Template Resources --->
	#-------------------------------------------------------------------------------

	# JMS Modules
	rollbackJMSModule_SALESFORCE_RA_JMSModule()
	rollbackJMSModule_DUMMY_OSB_JMSModule()

	# JMS Servers
	rollbackJMSServer_SALESFORCE_OSB_JMSServer()
	rollbackJMSServer_DUMMY_OSB_JMSServer()


	#-------------------------------------------------------------------------------
	# Framework Resources --->
	#-------------------------------------------------------------------------------

	# JMS Modules
	rollbackJMSModule_FRW_OSB_JMSModule()

	# JMS Servers
	rollbackJMSServer_FRW_OSB_JMSServer()
	rollbackJMSServer_FRW_OSB_Logger_JMSServers()
	rollbackJMSServer_FRW_OSB_Vitality_JMSServers()
	rollbackJMSServer_FRW_OSB_Conversation_JMSServers()
	rollbackJMSServer_FRW_OSB_Correlation_JMSServers()

	# Persistent Stores
	rollbackFileStore_FRW_OSB_FileStore()
	rollbackFileStore_FRW_OSB_Logger_FileStores()
	rollbackFileStore_FRW_OSB_Vitality_FileStores()
	rollbackFileStore_FRW_OSB_Conversation_FileStores()
	rollbackFileStore_FRW_OSB_Correlation_FileStores()

	# DB Datasources
	rollbackDatasource_SupportComponentDS()
	rollbackDatasource_SupportComponentDSNoTx()

	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	#-------------------------------------------------------------------------------
	# Service Template Applications --->
	#-------------------------------------------------------------------------------

	#-------------------------------------------------------------------------------
	# Framework Applications --->
	#-------------------------------------------------------------------------------

	# DBAdapter Connection Factories
	rollbackDBAdapterCFactory_ConversationManager()
	rollbackDBAdapterCFactory_ErrorManager()
	rollbackDBAdapterCFactory_LoggerManager()
	rollbackDBAdapterCFactory_MessageManager()
	rollbackDBAdapterCFactory_ParameterManager()
	rollbackDBAdapterCFactory_CorrelationManager()
	rollbackDBAdapterCFactory_ErrorHospital()
	rollbackDBAdapterCFactory_FrameworkDataManager()

	# Coherence Adapter
	rollbackCOHAdapterCFactory_Entel()

	""" @@@ """

	validate()
	endEditSession('OK')

except:
	dumpStack()
	endEditSession('ERROR')
