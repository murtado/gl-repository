#!/bin/bash          
DATE=$(date +"%F")
TIME=$(date +"%T")
DIR=`dirname $0`
LOGFILE="$DIR/logs/JMSPathServiceVitalityCheck-$DATE.log"
URI="/FRW/JMSPathService/Vitality"

function prop {
    grep "${1}" $DIR/INT_OSB_VitalityChecks-1.0.properties|cut -d'=' -f2
}

if [ ! -e "$LOGFILE" ] 
then
    touch $LOGFILE
fi

if [ $# -eq 0 ]
then
	LOG=$TIME
	LOG+=' :: FRW/JMSPathService -> '
	LOG+=`curl -u $(prop 'osb_server_user'):$(prop 'osb_server_pass') http://$(prop 'osb_server_address'):$(prop 'osb_server_port')$URI`
	LOG+='
'
	echo $LOG >> $LOGFILE 
else
	echo "Usage: <path>/JMSPathServiceVitalityCheck.sh
       environment.properties should be modified properly."
	exit 1
fi