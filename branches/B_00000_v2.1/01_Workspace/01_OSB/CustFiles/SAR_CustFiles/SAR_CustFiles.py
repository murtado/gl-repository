import time
import os
import sys

import ConfigParser

config = ConfigParser.ConfigParser()
config.read('SAR_CustFiles.ini')

sourceCustFilesPath=sys.argv[1]
envCustFilesPath=sys.argv[2]
custFiles=[]
selectedEnv=''

""" ---------------------------------------------------------------------- """
""" Auxiliary Functions                                                    """
""" ---------------------------------------------------------------------- """

def loadCustFiles():
	for custFile in os.listdir(sourceCustFilesPath):
		if custFile.endswith(config.get('SourceCustomizationFiles','SubFix')):
			custFiles.append(custFile)

	os.system('clear')

	print '---------------------------------------------------------------------------------------------'
	for custFile in custFiles:
		print ' - ' + custFile
	print '---------------------------------------------------------------------------------------------'

	return raw_input("These files are going to processed. Continue?. Y/N :: ").upper()

def getSourceEndpoint(transport, domain):
	try:
		domain_host = config.get('SourceEndpoints',domain + '_Host')
		endpoint = transport.lower() + '://'
		server_list = ''

		for port in config.get('SourceEndpoints',domain + '_Ports').split(','):
			server_list = server_list + domain_host + ':' + port + ','

		server_list = server_list[:-1]
		endpoint = endpoint + server_list

		return endpoint
	except:
		raise

def getEnvEndpoint(transport, domain):
	try:
		domain_host = config.get('EnvEndpoints-'+selectedEnv,domain + '_Host')
		endpoint = transport.lower() + '://'
		server_list = ''

		for port in config.get('EnvEndpoints-'+selectedEnv,domain + '_Ports').split(','):
			server_list = server_list + domain_host + ':' + port + ','

		server_list = server_list[:-1]
		endpoint = endpoint + server_list

		return endpoint
	except:
		raise

def setTargetEnvironment():
	global selectedEnv
	try:
		print ''
		print 'Select target environment :'
		print ''

		env_options = config.get('Environments','EnvList').split(',')

		for env_option in env_options:
			print env_option

		while 1:
			selectedEnv = raw_input("--------> :: ").upper()
			if (env_options.__contains__(selectedEnv)):
				print ''
				break
			else:
				print 'Invalid Option. Please select target environment :'
	except:
		raise

def SAR_CustFiles():
	try:
		for custFile in custFiles:
			with open(sourceCustFilesPath+'/'+custFile) as sourceFile, open(envCustFilesPath+'/'+config.get('EnvCustomizationFiles','Prefix') + selectedEnv + '_' + custFile, 'w') as envFile:
				for line in sourceFile:
					line = line.replace(getSourceEndpoint('jms','OSB'), getEnvEndpoint('jms','OSB'))
					line = line.replace(getSourceEndpoint('http','OSB'), getEnvEndpoint('http','OSB'))
					line = line.replace(getSourceEndpoint('t3','OSB'), getEnvEndpoint('t3','OSB'))
					line = line.replace(getSourceEndpoint('jms','SOA'), getEnvEndpoint('jms','SOA'))
					line = line.replace(getSourceEndpoint('http','SOA'), getEnvEndpoint('http','SOA'))
					Line = line.replace(getSourceEndpoint('t3','SOA'), getEnvEndpoint('t3','SOA'))
					envFile.write(line)
	except:
		raise

""" ********************************************************************************** """
# 										Main
""" ********************************************************************************** """

try:
	if (loadCustFiles() == 'Y'):
		setTargetEnvironment()
		SAR_CustFiles()
	else:
		print 'Exiting (No Changes) ...'

except:
	raise
