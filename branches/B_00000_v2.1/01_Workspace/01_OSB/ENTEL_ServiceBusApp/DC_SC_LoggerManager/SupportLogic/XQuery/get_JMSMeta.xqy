xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace tp="http://www.bea.com/wli/sb/transports";
declare namespace jms="http://www.bea.com/wli/sb/transports/jms";

declare variable $Inbound as element() external;

declare function local:get_JMSMeta($Inbound as element()) as element() {
    let $jmsHeaders:=$Inbound/ctx:transport/ctx:request/tp:headers
    
    return
    
    <JMSMeta
      jmsCorrelationID="{
        if($jmsHeaders/jms:JMSCorrelationID) then
          data($jmsHeaders/jms:JMSCorrelationID)
        else
          data($jmsHeaders/jms:JMSMessageID)
        }"
      jmsMessageType="{data($jmsHeaders/jms:JMSType)}"
      jmsUnitOfOrder="{data($jmsHeaders/jms:JMS_BEA_UnitOfOrder)}"
    />
};

local:get_JMSMeta($Inbound)