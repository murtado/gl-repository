xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/LoggerManager/getConvLog/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getConvLog_LoggerManager_v2_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getLogMessageLoggerManagerAdapter";
(:: import schema at "../JCA/getLogMessageLoggerManagerAdapter/getLogMessageLoggerManagerAdapter_sp.xsd" ::)

declare namespace ns3="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare variable $AdapterRSP as element() (:: schema-element(ns1:OutputParameters) ::) external;
declare variable $Result as element()? (:: schema-element(ns3:Result) ::) external;

declare function local:get_GetConvLogRSP(
    $AdapterRSP as element() (:: schema-element(ns1:OutputParameters) ::),
    $Result as element()? (:: schema-element(ns3:Result) ::)
  ) as element() (:: schema-element(ns2:GetConvLogRSP) ::) {
    <ns2:GetConvLogRSP>
        {
          if(exists($Result/*[1])) then
            $Result
          else
            <ns2:LogMSG>
              {fn-bea:inlinedXML($AdapterRSP/*[1])}
            </ns2:LogMSG>
        }
    </ns2:GetConvLogRSP>
};

local:get_GetConvLogRSP($AdapterRSP, $Result)