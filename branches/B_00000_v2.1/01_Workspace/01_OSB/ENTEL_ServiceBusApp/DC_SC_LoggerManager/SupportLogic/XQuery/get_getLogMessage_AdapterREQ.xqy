xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/LoggerManager/getConvLog/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getConvLog_LoggerManager_v2_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getLogMessageLoggerManagerAdapter";
(:: import schema at "../JCA/getLogMessageLoggerManagerAdapter/getLogMessageLoggerManagerAdapter_sp.xsd" ::)

declare variable $SCMessage as element() (:: schema-element(ns1:GetConvLogREQ) ::) external;

declare function local:get_getLogMessage_AdapterREQ($SCMessage as element() (:: schema-element(ns1:GetConvLogREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_CONVERSATION_ID>{fn:data($SCMessage/ns1:ConversationID)}</ns2:P_CONVERSATION_ID>
        <ns2:P_LOG_TYPE_NAME>{fn:data($SCMessage/ns1:LogType)}</ns2:P_LOG_TYPE_NAME>
        {
            if ($SCMessage/ns1:LogOccurrence)
              then <ns2:P_LOG_OCCURENCE>{fn:data($SCMessage/ns1:LogOccurrence)}</ns2:P_LOG_OCCURENCE>
            else ()
        }
    </ns2:InputParameters>
};

local:get_getLogMessage_AdapterREQ($SCMessage)