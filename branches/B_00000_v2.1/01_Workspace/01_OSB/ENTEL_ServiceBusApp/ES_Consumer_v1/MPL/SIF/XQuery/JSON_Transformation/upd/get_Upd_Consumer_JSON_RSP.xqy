xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/Consumer/upd/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/upd_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/upd/v1";
(:: import schema at "../../../../../ESC/Primary/upd_Consumer_v1_EBM.xsd" ::)

import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;

declare variable $Upd_Consumer_RSP as element() (:: schema-element(ns1:Upd_Consumer_RSP) ::) external;

declare function local:func($Upd_Consumer_RSP as element() (:: schema-element(ns1:Upd_Consumer_RSP) ::)) as element() (:: schema-element(ns2:Upd_Consumer_RSP) ::) {
    <ns2:Upd_Consumer_RSP>
        {nssif_header:get_ResponseHeader($Upd_Consumer_RSP/*:ResponseHeader)}
        <ns2:Body></ns2:Body>
    </ns2:Upd_Consumer_RSP>
};

local:func($Upd_Consumer_RSP)
