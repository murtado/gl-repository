xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CacheManager/lazyRefresh/v1";

declare namespace ns2="http://www.entel.cl/SC/FrameworkDataManager/getEntityID/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";

declare variable $Result as element() external;

declare function local:get_RefreshCacheRSP_Diagnose($Result as element()) as element() {
    <ns2:RefreshCacheRSP>
        {$Result}
    </ns2:RefreshCacheRSP>
};

local:get_RefreshCacheRSP_Diagnose($Result)
