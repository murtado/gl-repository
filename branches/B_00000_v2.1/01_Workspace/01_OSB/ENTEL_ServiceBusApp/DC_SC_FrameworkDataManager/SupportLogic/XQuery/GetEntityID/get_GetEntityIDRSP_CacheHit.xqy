xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/FrameworkDataManager/getEntityID/v1";
(:: import schema at "../../../SupportAPI/XSD/CSM/getEntityID_FrameworkDataManager_v1_CSM.xsd" ::)

declare namespace ns1 = "http://www.entel.cl/SC/FrameworkDataManager/FRWEntities/Aux";
(:: import schema at "../../../SupportAPI/XSD/FRWEntities.xsd" ::)

declare variable $EntityID as xs:string external;

declare function local:func($EntityID as xs:string) as element() (:: schema-element(ns2:GetEntityIDRSP) ::) {
    <ns2:GetEntityIDRSP>
      <ns1:EntityID>{$EntityID}</ns1:EntityID>
    </ns2:GetEntityIDRSP>
};

local:func($EntityID)
