xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/FrameworkDataManager/getEntityID/v1";
(:: import schema at "../../../../SupportAPI/XSD/CSM/getEntityID_FrameworkDataManager_v1_CSM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/SC/FrameworkDataManager/FRWEntities/Aux";
(:: import schema at "../../../../SupportAPI/XSD/FRWEntities.xsd" ::)

declare namespace ns3="http://xmlns.oracle.com/pcbpel/adapter/db/sp/GET_SERVICE_ID";
(:: import schema at "../../../../SupportLogic/JCA/GET_SERVICE_ID/GET_SERVICE_ID_sp.xsd" ::)

declare variable $SCMessage as element() (:: schema-element(ns1:GetEntityIDREQ) ::) external;

declare function local:func($SCMessage as element() (:: schema-element(ns1:GetEntityIDREQ) ::)) as element() (:: schema-element(ns3:InputParameters) ::) {

      <ns3:InputParameters>
          <ns3:P_SERVICE_CODE>{fn:data($SCMessage/*[2]/*[1]/@code)}</ns3:P_SERVICE_CODE>
          <ns3:P_SERVICE_NAME>{fn:data($SCMessage/*[2]/*[1]/@name)}</ns3:P_SERVICE_NAME>

      </ns3:InputParameters>
};

local:func($SCMessage)
