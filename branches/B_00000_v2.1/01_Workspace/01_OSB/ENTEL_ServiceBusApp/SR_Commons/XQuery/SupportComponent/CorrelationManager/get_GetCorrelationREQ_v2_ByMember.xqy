xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/getCorrelation/v2";

declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $GroupStatus as xs:string external;
declare variable $GroupName as xs:string external;
declare variable $MemberName as xs:string external;
declare variable $MemberType as xs:string external;
declare variable $MemberCorrID as xs:string external;
declare variable $RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::) external;

declare function local:get_GetCorrelationREQ_v2_ByMember(
	$GroupStatus as xs:string, 
	$GroupName as xs:string, 
	$MemberName as xs:string, 
	$MemberType as xs:string, 
	$MemberCorrID as xs:string, 
	$RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::)) as element(){
        <ns2:GetCorrelationREQ>
            {$RequestHeader}
            <cor:GroupStatus>{$GroupStatus}</cor:GroupStatus>
         	<cor:GroupName>{$GroupName}</cor:GroupName>
         	<cor:MemberName>{$MemberName}</cor:MemberName>
         	<cor:MemberType>{$MemberType}</cor:MemberType>
         	<cor:MemberCorrID>{$MemberCorrID}</cor:MemberCorrID>
        </ns2:GetCorrelationREQ>
};

local:get_GetCorrelationREQ_v2_ByMember($GroupStatus, $GroupName, $MemberName, $MemberType, $MemberCorrID, $RequestHeader)
