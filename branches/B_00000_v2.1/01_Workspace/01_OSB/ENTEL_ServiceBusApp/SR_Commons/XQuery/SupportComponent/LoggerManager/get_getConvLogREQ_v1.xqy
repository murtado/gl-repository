xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/LoggerManager/getConvLog/v1";

declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $ConversationID as xs:string (:: ns1:conversationID_SType ::)  external;
declare variable $LogType as xs:string external;
declare variable $Occurrence as xs:int? external;
declare variable $RequestHeader as element(ns1:RequestHeader) external;

declare function local:getConvLogREQ(
                            $RequestHeader  as element(ns1:RequestHeader),
                            $ConversationID as xs:string (:: ns1:conversationID_SType ::) , 
                            $LogType as xs:string, 
                            $Occurrence as xs:int?) 
                            as element(ns2:GetConvLogREQ) {
    <ns2:GetConvLogREQ>
      {$RequestHeader}
      <ns2:ConversationID>{$ConversationID}</ns2:ConversationID>
      <ns2:LogType>{$LogType}</ns2:LogType>
      {
      if (not(empty($Occurrence)) and boolean($Occurrence)) then
        <ns2:LogOccurrence>{$Occurrence}</ns2:LogOccurrence>
      else
        ()
      }
    </ns2:GetConvLogREQ>
};

local:getConvLogREQ($RequestHeader, $ConversationID, $LogType, $Occurrence)
