xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns2="http://www.entel.cl/SC/LoggerManager/log/v1";

declare namespace ns3="http://www.entel.cl/ESO/Tracer/v1";
(:: import schema at "../../../XSD/ESO/Tracer_v1_ESO.xsd" ::)

declare variable $Component as xs:string external;
declare variable $Operation as xs:string external;
declare variable $Header as element() external;
declare variable $Message as element() external;
declare variable $Description as xs:string external;
declare variable $LogPlaceHolder_Time as xs:dateTime external;
declare variable $LogPlaceHolder_Place as xs:string external;
declare variable $LogMode_Type as xs:string external;
declare variable $LogSeverity as xs:string external;

declare function local:get_logREQ($Component as xs:string, $Operation as xs:string,
                                  $Header as element(), 
                                  $Message as element(), 
                                  $Description as xs:string, 
                                  $LogPlaceHolder_Time as xs:dateTime, 
                                  $LogPlaceHolder_Place as xs:string, 
                                  $LogMode_Type as xs:string, 
                                  $LogSeverity as xs:string) 
                                  as element(ns2:LogREQ) {
 <ns2:LogREQ>
    <ns3:HeaderTracer component="{$Component}" operation="{$Operation}">
        <ns3:Header>
           {$Header/*}
        </ns3:Header>
    </ns3:HeaderTracer>
    <ns2:Message>{$Message}</ns2:Message>
    <ns2:Description>{$Description}</ns2:Description>
    <ns2:LogPlaceholder time="{$LogPlaceHolder_Time}" place="{$LogPlaceHolder_Place}"/>
    <ns2:LogMode logType="{$LogMode_Type}" logSeverity="{$LogSeverity}"/>
  </ns2:LogREQ>
};

local:get_logREQ($Component, $Operation, $Header, $Message, $Description, $LogPlaceHolder_Time, $LogPlaceHolder_Place, $LogMode_Type, $LogSeverity)
