xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns0 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)
declare namespace ns1 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare variable $header as element() external;
declare variable $conversationID as xs:string? external;
declare variable $serviceCode as xs:string? external;
declare variable $serviceName as xs:string? external;
declare variable $serviceOperation as xs:string? external;

declare function local:getRequestHeaderStamp($header as element(), 
                                      $conversationID as xs:string?,
                                      $serviceCode as xs:string?,
                                      $serviceName as xs:string?,
                                      $serviceOperation as xs:string?) 
                                      as element() {
        let $eventID := if ( string-length(data($header/*[2]/@eventID)) > 50 )
                        then substring(data($header/*[2]/@eventID), (string-length(data($header/*[2]/@eventID)) - 49)  ,50)
                        else data($header/*[2]/@eventID)
        
        let $processID := if ( string-length(data($header/*[2]/@processID)) > 50 )
                        then substring(data($header/*[2]/@processID), (string-length(data($header/*[2]/@processID)) - 49)  ,50)
                        else data($header/*[2]/@processID)
        let $ChannelName := if ( string-length(data($header/*[3]/@name)) > 16 )
                        then substring(data($header/*[3]/@name), (string-length(data($header/*[3]/@name)) - 15)  ,16)
                        else data($header/*[3]/@name)
        return 
          <ns0:RequestHeader>
            {$header/*[1]}
            <ns0:Trace clientReqTimestamp = "{ fn:data($header/*[2]/@clientReqTimestamp) }"
                       reqTimestamp = "{ fn:current-dateTime() }"
                       processID = "{$processID}"
                       eventID = "{
                       if($header/*[2]/@eventID = '') then
                            fn-bea:trim(substring(concat('INV',$conversationID),1,45))
                          else
                            fn:data($eventID)
                        }" 
                       sourceID = "{ fn:data($header/*[2]/@sourceID) }" 
                       correlationEventID = "{ fn:data($header/*[2]/@correlationEventID) }"
                       conversationID = "{ $conversationID  }"
                       correlationID = "{ fn:data($header/*[2]/@correlationID) }">
                  <ns0:Service 
                    code = "{ $serviceCode }" 
                    name = "{ $serviceName }"
                    operation = "{ $serviceOperation }"/>
                </ns0:Trace>
            {
            if (fn:exists($header/*[3])) then
                 <ns0:Channel name = "{$ChannelName}">
                    {
                      if (fn:exists($header/*[3]/@mode))
                      then attribute mode {data($header/*[3]/@mode)}
                      else ()
                    }
                 </ns0:Channel>
            else(<ns0:Channel name = "UNK"></ns0:Channel>)
            }
        </ns0:RequestHeader>

};

local:getRequestHeaderStamp($header, $conversationID, $serviceCode, $serviceName, $serviceOperation)