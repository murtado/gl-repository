xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/ConversationManager/getStatus/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getStatus_ConversationManager_v1_CSM.xsd" ::)

declare namespace con = "http://www.entel.cl/SC/ConversationManager/Aux/ConversationStatus";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare function local:get_GetStatusRSP_Adapter() as element() (:: schema-element(ns2:GetStatusRSP) ::) {
    <ns2:GetStatusRSP>
        <ns2:ConversationStatus status="">
        </ns2:ConversationStatus>
    </ns2:GetStatusRSP>
};

local:get_GetStatusRSP_Adapter()
