xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $CorrOperation as xs:string external;

declare function local:get_CorrAsyncRoutingCFG($CorrOperation as xs:string) as xs:string {
    
    let $corrOperations:='createGroupMember,deleteGroupMember,deleteGroup,updateGroupStatus,registerGroup,registerGroup_v2,createGroupFlags,createMemberFlags'
    
    return
      if(contains($corrOperations,$CorrOperation)) then
        concat(
          local:get_REQ_Adapter_XQName(),
          '@',
          local:get_RSP_Adapter_XQName(),
          '@',
          local:get_BusinessName(),
          '@',
          local:get_BusinessOperation()
        )
      else
        ('UNK')
};


declare function local:get_REQ_Adapter_XQName() as xs:string {
  concat(
    'get_',
      (
        if($CorrOperation = 'createGroupMember')  then 'CreateGroupMember'  else (),
        if($CorrOperation = 'deleteGroupMember')  then 'DeleteGroupMember'  else (),
        if($CorrOperation = 'deleteGroup')        then 'DeleteGroup'        else (),
        if($CorrOperation = 'updateGroupStatus')  then 'UpdateGroupStatus'  else (),
        if($CorrOperation = 'registerGroup')      then 'RegisterGroup'      else (),
        if($CorrOperation = 'registerGroup_v2')   then 'RegisterGroup'      else (),
        if($CorrOperation = 'createGroupFlags')   then 'CreateGroupFlags'   else (),
        if($CorrOperation = 'createMemberFlags')  then 'CreateMemberFlags'  else ()
      ),
    'REQ_Adapter',
    local:get_VersionModifier()
    )
};

declare function local:get_RSP_Adapter_XQName() as xs:string {
  concat(
    'get_',
      (
        if($CorrOperation = 'createGroupMember')  then 'CreateGroupMember'  else (),
        if($CorrOperation = 'deleteGroupMember')  then 'DeleteGroupMember'  else (),
        if($CorrOperation = 'deleteGroup')        then 'DeleteGroup'        else (),
        if($CorrOperation = 'updateGroupStatus')  then 'UpdateGroupStatus'  else (),
        if($CorrOperation = 'registerGroup')      then 'RegisterGroup'      else (),
        if($CorrOperation = 'registerGroup_v2')   then 'RegisterGroup'      else (),
        if($CorrOperation = 'createGroupFlags')   then 'CreateGroupFlags'   else (),
        if($CorrOperation = 'createMemberFlags')  then 'CreateMemberFlags'  else ()
      ),
    'RSP_Adapter',
    local:get_VersionModifier()
    )
};

declare function local:get_BusinessName() as xs:string {
    concat(
      (
        if($CorrOperation = 'createGroupMember')  then 'CreateGroupMember' else (),
        if($CorrOperation = 'deleteGroupMember')  then 'DeleteGroupMember' else (),
        if($CorrOperation = 'deleteGroup')        then 'DeleteGroup' else (),
        if($CorrOperation = 'updateGroupStatus')  then 'UpdateGroupStatus' else (),
        if($CorrOperation = 'registerGroup')      then 'RegisterCorrelationMemberDBAdapter' else (),
        if($CorrOperation = 'registerGroup_v2')   then 'RegisterCorrelationMemberDBAdapter' else (),
        if($CorrOperation = 'createGroupFlags')   then 'CreateGroupFlags'   else (),
        if($CorrOperation = 'createMemberFlags')  then 'CreateMemberFlags'  else ()
      ),
    local:get_VersionModifier()
    )
};

declare function local:get_BusinessOperation() as xs:string {

    if($CorrOperation = 'createGroupMember')  then 'createMember'                         else (),
    if($CorrOperation = 'deleteGroupMember')  then 'deleteGroupMember'                    else (),
    if($CorrOperation = 'deleteGroup')        then 'deleteGroup'                          else (),
    if($CorrOperation = 'updateGroupStatus')  then 'updateGroupStatus'                    else (),
    if($CorrOperation = 'registerGroup')      then 'registerCorrelationMemberAdapter'     else (),
    if($CorrOperation = 'registerGroup_v2')   then 'registerCorrelationMemberAdapter_v2'  else (),
    if($CorrOperation = 'createGroupFlags')   then 'createGroupFlags'                     else (),
    if($CorrOperation = 'createMemberFlags')  then 'createMemberFlags'                    else ()
      
};

declare function local:get_VersionModifier() as xs:string {

      if (contains($CorrOperation,'v')) 
      then (
        fn:concat('_',fn:substring($CorrOperation,fn:string-length($CorrOperation)-1))
        )
      else ('')
};

local:get_CorrAsyncRoutingCFG($CorrOperation)
