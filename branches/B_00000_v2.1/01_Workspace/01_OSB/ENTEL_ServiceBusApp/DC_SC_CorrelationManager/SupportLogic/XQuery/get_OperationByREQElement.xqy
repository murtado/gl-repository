xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $SCMessage as element() external;

declare function local:get_OperationByREQElement($SCMessage as element()) as xs:string {
  
  let $createGroupFlags_REQElemName   :='CreateGroupFlagsREQ'
  let $createGroupFlags_REQNsName     :='http://www.entel.cl/SC/CorrelationManager/createGroupFlags/v1'
  let $createGroupFlags_OPName        :='createGroupFlags'
  
  let $createMemberFlags_REQElemName  :='CreateMemberFlagsREQ'
  let $createMemberFlags_REQNsName    :='http://www.entel.cl/SC/CorrelationManager/createMemberFlags/v1'
  let $createMemberFlags_OPName       :='createMemberFlags'
    
  let $createGroupMember_REQElemName  :='CreateGroupMemberREQ'
  let $createGroupMember_REQNsName    :='http://www.entel.cl/SC/CorrelationManager/createGroupMember/v1'
  let $createGroupMember_OPName       :='createGroupMember'
  
  let $deleteGroupMember_REQElemName  :='DeleteGroupMemberREQ'
  let $deleteGroupMember_REQNsName    :='http://www.entel.cl/SC/CorrelationManager/deleteGroupMember/v1'
  let $deleteGroupMember_OPName       := 'deleteGroupMember'
  
  let $deleteGroup_REQElemName        :='DeleteGroupREQ'
  let $deleteGroup_REQNsName          :='http://www.entel.cl/SC/CorrelationManager/deleteGroup/v1'
  let $deleteGroup_OPName             :='deleteGroup'
  
  let $updateGroupStatus_REQElemName  :='UpdateGroupStatusREQ'
  let $updateGroupStatus_REQNsName    :='http://www.entel.cl/SC/CorrelationManager/updateGroupStatus/v1'
  let $updateGroupStatus_OPName       :='updateGroupStatus'
  
  let $registerGroupv1_REQElemName    :='RegisterGroupREQ'
  let $registerGroupv1_REQNsName      :='http://www.entel.cl/SC/CorrelationMember/registerGroup/v1'
  let $registerGroupv1_OPName         := 'registerGroup'
  
  let $registerGroupv2_REQElemName    :='RegisterGroupREQ'
  let $registerGroupv2_REQNsName      :='http://www.entel.cl/SC/CorrelationManager/registerGroup/v2'
  let $registerGroupv2_OPName         :='registerGroup_v2'
  
  return
    
    if(local-name($SCMessage)=$createGroupFlags_REQElemName     and namespace-uri($SCMessage)=$createGroupFlags_REQNsName   ) then $createGroupFlags_OPName   else(
    if(local-name($SCMessage)=$createMemberFlags_REQElemName    and namespace-uri($SCMessage)=$createMemberFlags_REQNsName  ) then $createMemberFlags_OPName  else(
    if(local-name($SCMessage)=$createGroupMember_REQElemName    and namespace-uri($SCMessage)=$createGroupMember_REQNsName  ) then $createGroupMember_OPName  else(
    if(local-name($SCMessage)=$deleteGroupMember_REQElemName    and namespace-uri($SCMessage)=$deleteGroupMember_REQNsName  ) then $deleteGroupMember_OPName  else(
    if(local-name($SCMessage)=$deleteGroup_REQElemName          and namespace-uri($SCMessage)=$deleteGroup_REQNsName        ) then $deleteGroup_OPName        else(
    if(local-name($SCMessage)=$updateGroupStatus_REQElemName    and namespace-uri($SCMessage)=$updateGroupStatus_REQNsName  ) then $updateGroupStatus_OPName  else(
    if(local-name($SCMessage)=$registerGroupv1_REQElemName      and namespace-uri($SCMessage)=$registerGroupv1_REQNsName    ) then $registerGroupv1_OPName    else(
    if(local-name($SCMessage)=$registerGroupv2_REQElemName      and namespace-uri($SCMessage)=$registerGroupv2_REQNsName    ) then $registerGroupv2_OPName    else(
    
      'UNK'
    
    ))))))))
};

local:get_OperationByREQElement($SCMessage)
