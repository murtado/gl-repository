create or replace PACKAGE BODY ESB_COMMONS_PKG AS

  /*---------------------------------------------------------------------------------------------------
  
    Fills the p_SOURCE_ERROR_CODE and p_SOURCE_ERROR_DESC variable used throughout the Package, by the
    use of the following parameters :
    
    p_Locator   -->  Las known position within a Procedure or Function
    p_Reason    -->  Descriptive information about an Error.
    p_ErrCode   -->  Unique code of an Error, within the context of this package.
    p_SqlErr    -->  SQLERR Information, as it exists within the error context 
                                from which this procedure is called.
    
  */---------------------------------------------------------------------------------------------------
  PROCEDURE LOAD_SOURCE_ERROR(
      p_Locator                   IN VARCHAR,
      p_ErrCode                   IN VARCHAR,
      p_Reason                    IN VARCHAR,
      p_SqlErr                    IN VARCHAR,
      p_SOURCE_ERROR_CODE         OUT VARCHAR,
      p_SOURCE_ERROR_DESC         OUT VARCHAR
    )
    IS
      BEGIN
            
            p_SOURCE_ERROR_CODE := p_ErrCode;
            p_SOURCE_ERROR_DESC := 
              p_Reason 
              || ' @ [' 
              || p_Locator
              || '] --- Error [' 
              || p_SqlErr
              || ']';
      EXCEPTION
        WHEN OTHERS THEN
          p_SOURCE_ERROR_CODE := p_ErrCode;
          p_SOURCE_ERROR_DESC := SUBSTR(
              p_Reason 
              || ' @ [' 
              || p_Locator
              || '] --- Error [' 
              || p_SqlErr
              || ']',
              0,
              250
            );
            
  END LOAD_SOURCE_ERROR;
  
  /*---------------------------------------------------------------------------------------------------
  
    **** FUNCTION CREATE_COUNTRY :: Creates a registry in ESB_COUNTRY returning the ID of the new entry.
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  
  FUNCTION CREATE_COUNTRY(
    p_COUNTRY_CODE                IN VARCHAR2, 
    p_COUNTRY_NAME                IN VARCHAR2,
    p_DESCRIPTION                 IN VARCHAR2,
    p_RCD_STATUS                  IN NUMBER,
    p_Locator                     OUT VARCHAR, 
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )  RETURN NUMBER
    IS
    v_COUNTRY_ID NUMBER;
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully stored.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG-CREATE_COUNTRY - INIT';
      
        INSERT INTO ESB_COUNTRY
        (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          RCD_STATUS,
          CREATIONDATE
        )
        VALUES
        (
          ESB_COUNTRY_SEQ.NEXTVAL,
          p_COUNTRY_CODE,
          p_COUNTRY_NAME,
          p_DESCRIPTION,
          p_RCD_STATUS,
          CURRENT_TIMESTAMP
        )
        RETURNING ID
        INTO v_COUNTRY_ID;
        RETURN v_COUNTRY_ID;
      EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END CREATE_COUNTRY;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome        --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND     --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS     --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR   --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_COUNTRY_ID(
    p_COUNTRY_CODE                IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
    
      v_COUNTRY_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_COUNTRY_ID - INIT';
      
        SELECT 
          ID,
          RCD_STATUS 
            INTO 
              v_COUNTRY_ID,
              v_RCD_STATUS 
        FROM 
          ESB_COUNTRY 
        WHERE 
          CODE = p_COUNTRY_CODE;

        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Country Code]=' || p_COUNTRY_CODE || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;
        
        RETURN v_COUNTRY_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Country Code]=' || p_COUNTRY_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Country Code]=' || p_COUNTRY_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_COUNTRY_ID;
  /*---------------------------------------------------------------------------------------------------
  
    **** FUNCTION CREATE_CHANNEL :: Creates a registry in ESB_CHANNEL returning the ID of the new entry.
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_CHANNEL(
    p_CHANNEL_CODE                IN VARCHAR2, 
    p_CHANNEL_NAME                IN VARCHAR2, 
    p_RCD_STATUS                  IN NUMBER, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
      
      v_CHANNEL_ID NUMBER;
    
      BEGIN
    
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.CREATE_CHANNEL - INIT';
    
        INSERT INTO ESB_CHANNEL
        (
          ID,
          CODE,
          NAME,
          RCD_STATUS,
          CREATIONDATE
        )
        VALUES
        (
          ESB_CHANNEL_SEQ.NEXTVAL,
          p_CHANNEL_CODE,
          p_CHANNEL_NAME,
          p_RCD_STATUS,
          CURRENT_TIMESTAMP
        )
        RETURNING ID
        INTO v_CHANNEL_ID;
        
        RETURN v_CHANNEL_ID;
        
      EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END CREATE_CHANNEL;
  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome        --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND     --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS     --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR   --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CHANNEL_ID_BY_NAME(
    p_CHANNEL_NAME                IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
      
      v_CHANNEL_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
    
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_CHANNEL_ID - INIT';
    
        SELECT 
          ID,
          RCD_STATUS 
            INTO 
              v_CHANNEL_ID,
              v_RCD_STATUS 
        FROM 
          ESB_CHANNEL 
        WHERE 
          NAME = COALESCE(p_CHANNEL_NAME, 'UNK');
        
        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Channel Code]=' || p_CHANNEL_NAME || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;

        RETURN v_CHANNEL_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Channel Code]=' || p_CHANNEL_NAME || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Channel Code]=' || p_CHANNEL_NAME || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_CHANNEL_ID_BY_NAME;
  
  /*---------------------------------------------------------------------------------------------------
  
    **** FUNCTION CREATE_SYSTEM :: Creates a registry in ESB_SYSTEM returning the ID of the new entry.
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  
  FUNCTION CREATE_SYSTEM(
    p_SYSTEM_CODE                 IN VARCHAR2, 
    p_SYSTEM_NAME                 IN VARCHAR2, 
    p_DESCRIPTION                 IN VARCHAR2, 
    p_RCD_STATUS                  IN NUMBER,
    p_Locator                     OUT VARCHAR, 
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )  RETURN NUMBER
    IS
    v_SYSTEM_ID NUMBER;
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully stored.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.CREATE_SYSTEM - INIT';
      
        INSERT INTO ESB_SYSTEM
        (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          RCD_STATUS,
          CREATIONDATE
        )
        VALUES
        (
          ESB_SYSTEM_SEQ.NEXTVAL,
          p_SYSTEM_CODE,
          p_SYSTEM_NAME,
          p_DESCRIPTION,
          p_RCD_STATUS,
          CURRENT_TIMESTAMP
        )
        RETURNING ID
        INTO v_SYSTEM_ID;
        RETURN v_SYSTEM_ID;
      EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END CREATE_SYSTEM;
      

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_SYSTEM_ID(
    p_SYSTEM_CODE               IN VARCHAR2, 
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
      
      v_SYSTEM_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_SYSTEM_ID - INIT';
          
        SELECT 
          ID,
          RCD_STATUS 
            INTO 
              v_SYSTEM_ID,
              v_RCD_STATUS 
        FROM 
          ESB_SYSTEM 
        WHERE 
          CODE = p_SYSTEM_CODE;

        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [System Code]=' || p_SYSTEM_CODE || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;
        
        RETURN v_SYSTEM_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [System Code]=' || p_SYSTEM_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [System Code]=' || p_SYSTEM_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_SYSTEM_ID;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_DEF_SYSTEM_ID_FRW(
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
      
      v_SYSTEM_ID NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_DEF_SYSTEM_ID_FRW - INIT';
          
        v_SYSTEM_ID := GET_SYSTEM_ID('FRW',p_Locator,p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC);
        
        RETURN v_SYSTEM_ID;
        
      EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_DEF_SYSTEM_ID_FRW;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_ENTERPRISE(
    p_ENTERPRISE_CODE           IN VARCHAR2, 
    p_ENTERPRISE_NAME           IN VARCHAR2, 
    p_RCD_STATUS                IN NUMBER,
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
    
      v_ENTERPRISE_ID NUMBER;
    
      BEGIN
      
      p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
      p_SOURCE_ERROR_CODE := '0';      
      p_Locator := 'ESB_COMMONS_PKG.CREATE_ENTERPRISE - INIT';
      
      INSERT INTO ESB_ENTERPRISE
      (
        ID,
        CODE,
        NAME,
        RCD_STATUS,
        CREATIONDATE
      )
      VALUES
      (
        ESB_ENTERPRISE_SEQ.NEXTVAL,
        p_ENTERPRISE_CODE,
        p_ENTERPRISE_NAME,
        p_RCD_STATUS,
        CURRENT_TIMESTAMP
      )
      RETURNING ID
      INTO v_ENTERPRISE_ID;

      RETURN v_ENTERPRISE_ID;
        
      EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END CREATE_ENTERPRISE;


  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_ENTERPRISE_ID(
    p_ENTERPRISE_CODE           IN VARCHAR2, 
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
    
      v_ENTERPRISE_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
      
      p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
      p_SOURCE_ERROR_CODE := '0';      
      p_Locator := 'ESB_COMMONS_PKG.GET_ENTERPRISE_ID - INIT';
      
      SELECT 
        ID,
        RCD_STATUS
          INTO 
            v_ENTERPRISE_ID,
            v_RCD_STATUS
      FROM 
        ESB_ENTERPRISE 
      WHERE 
        CODE = p_ENTERPRISE_CODE;
      
      IF v_RCD_STATUS = 2 THEN 
        LOAD_SOURCE_ERROR(
          p_Locator,
          '-4',
          'Logically deleted record with this info : [Enterprise Code]=' || p_ENTERPRISE_CODE || '.',
          '',
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
      END IF;

      RETURN v_ENTERPRISE_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Enterprise Code]=' || p_ENTERPRISE_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Enterprise Code]=' || p_ENTERPRISE_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_ENTERPRISE_ID;


/*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_CONSUMER(
    p_SYSTEM_ID                 IN NUMBER, 
    p_COUNTRY_ID                IN NUMBER, 
    p_ENTERPRISE_ID             IN NUMBER,
    p_RCD_STATUS                IN NUMBER,
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
    ) 
      RETURN NUMBER
      IS
    
        v_CONSUMER_ID NUMBER;
        
        BEGIN
        
          p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
          p_SOURCE_ERROR_CODE := '0';      
          p_Locator := 'ESB_COMMONS_PKG.CREATE_CONSUMER - INIT';
        
          INSERT INTO ESB_CONSUMER
          (
            ID,
            SYSCODE,
            COUNTRY_ID,
            ENT_CODE,
            RCD_STATUS,
            CREATIONDATE
          )
          VALUES
          (
            ESB_CONSUMER_SEQ.NEXTVAL,
            p_SYSTEM_ID,
            p_COUNTRY_ID,
            p_ENTERPRISE_ID,
            p_RCD_STATUS,
            CURRENT_TIMESTAMP
          )
          RETURNING ID 
          INTO v_CONSUMER_ID;

          RETURN v_CONSUMER_ID;
      
        EXCEPTION
          WHEN OTHERS THEN
            LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END CREATE_CONSUMER;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CONSUMER_ID(
    p_SYSTEM_ID                 IN NUMBER, 
    p_COUNTRY_ID                IN NUMBER, 
    p_ENTERPRISE_ID             IN NUMBER, 
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
    ) 
      RETURN NUMBER
      IS
    
        v_CONSUMER_ID NUMBER;
        v_RCD_STATUS NUMBER;
        
        BEGIN
        
          p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
          p_SOURCE_ERROR_CODE := '0';      
          p_Locator := 'ESB_COMMONS_PKG.GET_CONSUMER_ID - INIT';
          
          SELECT 
            C.ID,
            C.RCD_STATUS
              INTO 
                v_CONSUMER_ID,
                v_RCD_STATUS
          FROM 
            ESB_CONSUMER C 
          WHERE 
            C.SYSCODE = p_SYSTEM_ID AND 
            C.COUNTRY_ID = p_COUNTRY_ID AND 
            C.ENT_CODE = p_ENTERPRISE_ID;

          IF v_RCD_STATUS = 2 THEN 
            LOAD_SOURCE_ERROR(
              p_Locator,
              '-4',
              'Logically deleted record with this info : [System Id]=' || p_SYSTEM_ID || ' - [Enterprise Id]=' || p_ENTERPRISE_ID || ' - [Country Id]=' || p_COUNTRY_ID || '.',
              '',
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
          END IF;

          RETURN v_CONSUMER_ID;
      
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
              LOAD_SOURCE_ERROR(
                p_Locator,
                '-1',
                'There is no record with this info : [System Id]=' || p_SYSTEM_ID || ' - [Enterprise Id]=' || p_ENTERPRISE_ID || ' - [Country Id]=' || p_COUNTRY_ID || '.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );
              RETURN 0;
            WHEN TOO_MANY_ROWS THEN
              LOAD_SOURCE_ERROR(
                p_Locator,
                '-2',
                'Duplicate records matching this info : [System Id]=' || p_SYSTEM_ID || ' - [Enterprise Id]=' || p_ENTERPRISE_ID || ' - [Country Id]=' || p_COUNTRY_ID || '.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );
              RETURN 0;
            WHEN OTHERS THEN
              LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );
              RETURN 0;
  END GET_CONSUMER_ID;     

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CONSUMER_ID_BY_CODES(
    p_SYSTEM_CODE               IN VARCHAR2, 
    p_COUNTRY_CODE              IN VARCHAR2, 
    p_ENTERPRISE_CODE           IN VARCHAR2, 
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
    ) 
      RETURN NUMBER
      IS
    
        v_SYSTEM_ID NUMBER;
        v_ENTERPRISE_ID NUMBER;
        v_COUNTRY_ID NUMBER;

        v_CONSUMER_ID NUMBER;
        
        BEGIN
        
          p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
          p_SOURCE_ERROR_CODE := '0';      
          p_Locator := 'ESB_COMMONS_PKG.GET_CONSUMER_ID_BY_CODES - INIT';
        
          -- Getting the System
          v_SYSTEM_ID:=GET_SYSTEM_ID(p_SYSTEM_CODE, p_Locator, p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESC);
          IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN 0; END IF;
          
          -- Getting the Enterprise
          v_ENTERPRISE_ID:=GET_ENTERPRISE_ID(p_ENTERPRISE_CODE, p_Locator, p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESC);
          IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN 0; END IF;
          
          -- Getting the Country
          v_COUNTRY_ID:=GET_COUNTRY_ID(p_COUNTRY_CODE, p_Locator, p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESC);
          IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN 0; END IF;
          
          p_Locator := 'ESB_COMMONS_PKG.GET_CONSUMER_ID_BY_CODES - EXEC - ESB_COMMONS_PKG.GET_CONSUMER_ID';

          v_CONSUMER_ID := GET_CONSUMER_ID(
            v_SYSTEM_ID,
            v_COUNTRY_ID,
            v_ENTERPRISE_ID,
            p_Locator,
            p_SOURCE_ERROR_CODE,
            p_SOURCE_ERROR_DESC
          );

          RETURN v_CONSUMER_ID;
      
  END GET_CONSUMER_ID_BY_CODES;

/*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_CAPABILITY(
    p_SERVICE_ID                IN NUMBER, 
    p_CAPABILITY_CODE           IN VARCHAR2, 
    p_CAPABILITY_NAME           IN VARCHAR2, 
    p_RCD_STATUS                IN NUMBER,
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR, 
    p_SOURCE_ERROR_DESC         OUT VARCHAR
    ) 
      RETURN NUMBER
      IS
    
        v_CAPABILITY_ID NUMBER;
        
        BEGIN
        
          p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
          p_SOURCE_ERROR_CODE := '0';      
          p_Locator := 'ESB_COMMONS_PKG.CREATE_CAPABILITY - INIT';
        
          INSERT INTO ESB_CAPABILITY 
          (
            ID,
            SERVICE_ID,
            CODE,
            NAME,
            RCD_STATUS
          )
          VALUES(
            ESB_CAPABILITY_SEQ.NEXTVAL,
            p_SERVICE_ID,
            p_CAPABILITY_CODE,
            p_CAPABILITY_NAME,
            p_RCD_STATUS
          )
          RETURNING ID 
          INTO v_CAPABILITY_ID;

          RETURN v_CAPABILITY_ID;
      
        EXCEPTION
          WHEN OTHERS THEN
            LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END CREATE_CAPABILITY;

/*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CAPABILITY_ID(
    p_SERVICE_ID                  IN NUMBER, 
    p_CAPABILITY_NAME             IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
    
      v_CAPABILITY_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_CAPABILITY_ID - INIT';
      
        SELECT 
          ID,
          RCD_STATUS
            INTO 
              v_CAPABILITY_ID,
              v_RCD_STATUS
        FROM 
          ESB_CAPABILITY 
        WHERE 
          NAME = p_CAPABILITY_NAME AND 
          SERVICE_ID = p_SERVICE_ID;

        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Capability Name]=' || p_CAPABILITY_NAME || ' - [Service Id]=' || p_SERVICE_ID || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;                                                             
        
        RETURN v_CAPABILITY_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Capability Name]=' || p_CAPABILITY_NAME || ' - [Service Id]=' || p_SERVICE_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Capability Name]=' || p_CAPABILITY_NAME || ' - [Service Id]=' || p_SERVICE_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_CAPABILITY_ID;

/*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CAPABILITY_ID_BY_CODE(
    p_CAPABILITY_CODE             IN VARCHAR2, 
    p_CAPABILITY_NAME             IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
    
      v_CAPABILITY_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_CAPABILITY_ID_BY_CODE - INIT';
      
        SELECT 
          ID,
          RCD_STATUS
            INTO 
              v_CAPABILITY_ID,
              v_RCD_STATUS
        FROM 
          ESB_CAPABILITY 
        WHERE 
          NAME = p_CAPABILITY_NAME AND 
          CODE = p_CAPABILITY_CODE;
        
        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Capability Name]=' || p_CAPABILITY_NAME || ' - [Capability Code]=' || p_CAPABILITY_CODE || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;      

        RETURN v_CAPABILITY_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Capability Name]=' || p_CAPABILITY_NAME || ' - [Capability Code]=' || p_CAPABILITY_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Capability Name]=' || p_CAPABILITY_NAME || ' - [Capability Code]=' || p_CAPABILITY_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_CAPABILITY_ID_BY_CODE;

/*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_SERVICE(
    p_SERVICE_CODE                IN VARCHAR2, 
    p_SERVICE_NAME                IN VARCHAR2, 
    p_RCD_STATUS                  IN NUMBER,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
    ) 
      RETURN NUMBER
      IS
    
        v_SERVICE_ID NUMBER;
        
        BEGIN
        
          p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
          p_SOURCE_ERROR_CODE := '0';      
          p_Locator := 'ESB_COMMONS_PKG.CREATE_SERVICE - INIT';
        
          INSERT INTO ESB_SERVICE
          (
            ID,
            CODE,
            NAME,
            RCD_STATUS
          )
          VALUES
          (
            ESB_SERVICE_SEQ.NEXTVAL,
            p_SERVICE_CODE,
            p_SERVICE_NAME,
            p_RCD_STATUS
          )
          RETURNING ID 
          INTO v_SERVICE_ID;

          RETURN v_SERVICE_ID;
      
        EXCEPTION
            WHEN OTHERS THEN
              LOAD_SOURCE_ERROR(
                  p_Locator,
                  '-3',
                  'Unexpected Error.',
                  SQLERRM,
                  p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );
              RETURN 0;

  END CREATE_SERVICE;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_SERVICE_ID(
    p_SERVICE_CODE                IN VARCHAR2, 
    p_SERVICE_NAME                IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
    
      v_SERVICE_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_SERVICE_ID - INIT';
      
        SELECT 
          ID,
          RCD_STATUS 
            INTO 
              v_SERVICE_ID,
              v_RCD_STATUS 
        FROM 
          ESB_SERVICE 
        WHERE
          NAME = p_SERVICE_NAME AND 
          CODE = p_SERVICE_CODE;
        
        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Service Name]=' || p_SERVICE_NAME || ' - [Service Code]=' || p_SERVICE_CODE || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF; 

        RETURN v_SERVICE_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Service Name]=' || p_SERVICE_NAME || ' - [Service Code]=' || p_SERVICE_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Service Name]=' || p_SERVICE_NAME || ' - [Service Code]=' || p_SERVICE_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_SERVICE_ID;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_SERVICE_ID_BY_CODE(
    p_SERVICE_CODE                IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) 
    RETURN NUMBER
    IS
    
      v_SERVICE_ID NUMBER;
      v_RCD_STATUS NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_SERVICE_ID_BY_CODE - INIT';
      
        SELECT 
          ID,
          RCD_STATUS
            INTO 
              v_SERVICE_ID,
              v_RCD_STATUS
        FROM 
          ESB_SERVICE 
        WHERE 
          CODE = p_SERVICE_CODE;
        
        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Service Code]=' || p_SERVICE_CODE || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF; 

        RETURN v_SERVICE_ID;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Service Code]=' || p_SERVICE_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Service Code]=' || p_SERVICE_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_SERVICE_ID_BY_CODE;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------
  
  PROCEDURE GET_SERVICE_BY_ID (
    p_SERVICE_ID                  IN  NUMBER,
    p_SERVICE_NAME                OUT VARCHAR2,
    p_SERVICE_CODE                OUT VARCHAR2,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) IS
  
    v_RCD_STATUS NUMBER;
  BEGIN
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_SERVICE_BY_ID - INIT';
      
        SELECT 
          NAME,
          CODE,
          RCD_STATUS
            INTO 
              p_SERVICE_NAME,
              p_SERVICE_CODE,
              v_RCD_STATUS
        FROM 
          ESB_SERVICE 
        WHERE 
          ID = p_SERVICE_ID;
        
        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Service ID]=' || p_SERVICE_ID || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF; 
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Service ID]=' || p_SERVICE_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END;
  
  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    LOGICALLY DELETED --> p_SOURCE_ERROR_CODE = -4
    
  */---------------------------------------------------------------------------------------------------

  PROCEDURE GET_CAPABILITY_BY_ID (
    p_CAPABILITY_ID                  IN  NUMBER,
    p_CAPABILITY_NAME                OUT VARCHAR2,
    p_CAPABILITY_CODE                OUT VARCHAR2,
    p_SERVICE_NAME                  OUT VARCHAR2,
    p_SERVICE_CODE                  OUT VARCHAR2,
    p_Locator                       OUT VARCHAR,
    p_SOURCE_ERROR_CODE             OUT VARCHAR, 
    p_SOURCE_ERROR_DESC             OUT VARCHAR
  ) IS
    v_SERVICE_ID NUMBER;
    v_RCD_STATUS NUMBER;
    BEGIN
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_COMMONS_PKG.GET_CAPABILITY_BY_ID - INIT';
      
        SELECT 
          C.NAME,
          C.CODE,
          C.SERVICE_ID,
          C.RCD_STATUS
            INTO 
              p_CAPABILITY_NAME,
              p_CAPABILITY_CODE,
              v_SERVICE_ID,
              v_RCD_STATUS
        FROM 
          ESB_CAPABILITY C
        WHERE 
          C.ID = p_CAPABILITY_ID;
        
        IF v_RCD_STATUS = 2 THEN 
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : [Capability ID]=' || p_CAPABILITY_ID || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF; 
        
      GET_SERVICE_BY_ID (
                      v_SERVICE_ID,
                      p_SERVICE_NAME,
                      p_SERVICE_CODE,
                      p_Locator,
                      p_SOURCE_ERROR_CODE, 
                      p_SOURCE_ERROR_DESC
                    );

      IF p_SOURCE_ERROR_CODE <> '0' THEN RETURN; END IF;
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Capability ID]=' || p_CAPABILITY_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
           'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END;

END ESB_COMMONS_PKG;