create or replace PACKAGE BODY ESB_LOGGERMANAGER_PKG AS

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_MESSAGE_TRANSACTION
    (
      p_CAPABILITY_ID           IN NUMBER,
      p_PROC_ID                 IN VARCHAR2,
      p_EVENT_ID                IN VARCHAR2,
      p_CLIENT_REQ_TIMESTAMP    IN VARCHAR2,
      p_CORRELATION_ID          IN VARCHAR2,
      p_SOURCE_ID               IN VARCHAR2,
      p_CORRELATION_EVENT_ID    IN VARCHAR2,
      p_MESSAGE_TRANSACTION_ID  OUT NUMBER,
      p_CREATE_INVALID     IN CHAR,
      p_Locator                 OUT VARCHAR,
      p_SOURCE_ERROR_CODE       OUT VARCHAR,
      p_SOURCE_ERROR_DESC       OUT VARCHAR
    )
  IS
    CAPABILITY_ID_AUX NUMBER;
    V_TX_AMOUNT NUMBER;
    V_TX_AMOUNT_CAP NUMBER;
    V_RCD_STATUS NUMBER;

  BEGIN

    p_SOURCE_ERROR_DESC := 'Verifying the Transaction';
    p_SOURCE_ERROR_CODE := '0';
    p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_MESSAGE_TRANSACTION - INIT';

    /*
        Se busca la Tx. asociada a la conversacion sobre la cual se solicita un Log.

        En caso de no encontrar una Tx. asociada, se la genera para evitar perder el LOG. Esto es una accion esperada, como el componente
        MessageManager solo registra manualmente (sincronicamente) la Tx. cuando las validaciones configuradas para la dupla
        Servicio + Capacidad dependan de informacion para la cual no puede esperarse el Logging Async del LoggerManager (Idempotencia x ejemplo).

    */

    p_MESSAGE_TRANSACTION_ID  := ESB_MESSAGEMANAGER_PKG.GET_MESSAGE_TRANSACTION_ID(
                                      p_EVENT_ID,
                                      p_PROC_ID,
                                      p_CORRELATION_ID,
                                      p_Locator,
                                      p_SOURCE_ERROR_CODE,
                                      p_SOURCE_ERROR_DESC
                                    );

    -- Does it exist?.
    IF(p_SOURCE_ERROR_CODE = '-1') THEN
      p_MESSAGE_TRANSACTION_ID := ESB_MESSAGEMANAGER_PKG.CREATE_MESSAGE_TRANSACTION(
                                      p_EVENT_ID,
                                      p_PROC_ID,
                                      0,
                                      1,
                                      p_CLIENT_REQ_TIMESTAMP,
                                      p_CORRELATION_ID,
                                      p_SOURCE_ID,
                                      p_CORRELATION_EVENT_ID,
                                      p_Locator,
                                      p_SOURCE_ERROR_CODE,
                                      p_SOURCE_ERROR_DESC
                                    );
      IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
    ELSE
      IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-3',
          'Unexpected error while Verifing the Tx with this info : [Event Id]='               || p_EVENT_ID               ||
                                                               ' - [Process Id]='             || p_PROC_ID                ||
                                                               ' - [Client REQ Timestamp]='   || p_CLIENT_REQ_TIMESTAMP   ||
                                                               ' - [CorrelationID]='          || p_CORRELATION_ID         ||
                                                               ' - [SourceID]='               || p_SOURCE_ID              ||
                                                               ' - [CorrelationEventID]='     || p_CORRELATION_EVENT_ID   || '.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
    RETURN;

  END VERIFY_MESSAGE_TRANSACTION;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_SERVICE_CAPABILITY(
    p_SERVICE_NAME IN VARCHAR2,
    p_SERVICE_OPERATION IN VARCHAR2,
    p_SERVICE_CODE IN VARCHAR2,
    p_CAPABILITY_ID OUT NUMBER,
    p_CREATE_INVALID     IN CHAR,
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR,
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) IS

      v_SERVICE_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'Verifying the Transaction';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_SERVICE_CAPABILITY - INIT';

        /*--------------------------------------------------------
          Getting the Service.
        --------------------------------------------------------*/
        v_SERVICE_ID  := ESB_COMMONS_PKG.GET_SERVICE_ID(
                                        p_SERVICE_CODE,
                                        p_SERVICE_NAME,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE!=0) THEN -- It does not.

          IF (p_CREATE_INVALID!='T') THEN
            RETURN;
          ELSE

            IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.

              -- Updating the service with RCD_STATUS = 0 (Invalid).
              UPDATE
                  ESB_SERVICE
              SET
                  RCD_STATUS = 0
              WHERE
                CODE = p_SERVICE_CODE AND
                NAME = p_SERVICE_NAME;

            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                -- Creating the service with RCD_STATUS = 0 (Invalid).
                v_SERVICE_ID  := ESB_COMMONS_PKG.CREATE_SERVICE(
                                              p_SERVICE_CODE,
                                              p_SERVICE_NAME,
                                              0,
                                              p_Locator,
                                              p_SOURCE_ERROR_CODE,
                                              p_SOURCE_ERROR_DESC
                                            );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                RETURN;
              END IF;
            END IF;
          END IF;
        END IF;
        ----------------------------------------------------------------

        /*--------------------------------------------------------
          Getting the capability.
        --------------------------------------------------------*/
        p_CAPABILITY_ID  := ESB_COMMONS_PKG.GET_CAPABILITY_ID(
                                        v_SERVICE_ID,
                                        p_SERVICE_OPERATION,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE!=0) THEN
          IF (p_CREATE_INVALID!='T') THEN
            RETURN;
          ELSE
            IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.

              -- Updating the capability with RCD_STATUS = 0 (Invalid).
              UPDATE
                  ESB_CAPABILITY
              SET
                  RCD_STATUS = 0
              WHERE
                  SERVICE_ID = v_SERVICE_ID AND
                  NAME = p_SERVICE_OPERATION;

            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                -- Creating the capability with RCD_STATUS = 0 (Invalid).
                p_CAPABILITY_ID  := ESB_COMMONS_PKG.CREATE_CAPABILITY(
                                              v_SERVICE_ID,
                                              'UNK_'|| p_SERVICE_OPERATION,
                                              p_SERVICE_OPERATION,
                                              0,
                                              p_Locator,
                                              p_SOURCE_ERROR_CODE,
                                              p_SOURCE_ERROR_DESC
                                            );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                RETURN;
              END IF;
            END IF;
          END IF;
        END IF;
        ----------------------------------------------------------------

    EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
  END VERIFY_SERVICE_CAPABILITY;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_LOG_CONVERSATION(
      p_TRANSACTION_ID     IN NUMBER,
      p_HEADER_TRACER      IN HEADERTRACER_T,
      p_LOG_TYPE           IN VARCHAR2,
      p_RESULT_STATUS      IN VARCHAR2,
      p_PLACEHOLDER_TIME   IN VARCHAR2,
      p_LOG_OPERATION      IN VARCHAR2,
      p_CONVERSATION_ID_ID OUT NUMBER,
      p_CAPABILITY_ID      IN NUMBER,
      p_CREATE_INVALID     IN CHAR,
      p_Locator            OUT VARCHAR,
      p_SOURCE_ERROR_CODE  OUT VARCHAR,
      p_SOURCE_ERROR_DESC  OUT VARCHAR
    )
    IS

        v_CONSUMER_ID         NUMBER;
        v_COUNTRY_ID          NUMBER;
        v_CHANNEL_ID          NUMBER;
        v_ENTERPRISE_ID       NUMBER;
        v_CONV_SEQ            NUMBER;
        v_CAPABILITY_ID       NUMBER;
        v_SYSTEM_ID           NUMBER;

        --updateConversationStatus variables.
        v_S_STATUS            VARCHAR2(30) := null;
        v_A_STATUS            VARCHAR2(30) := null;
        v_CAN_ERROR_CODE      VARCHAR2(50) := null;
        v_CAN_ERROR_TYPE      VARCHAR2(50) := null;
        v_S_END_TIME          VARCHAR2(50) := null;
        v_A_END_TIME          VARCHAR2(50) := null;

      BEGIN

          p_SOURCE_ERROR_DESC := 'Verifying the Transaction';
          p_SOURCE_ERROR_CODE := '0';
          p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_CONVERSATION - INIT';


          /* ---------------------------------------------------------------------------------------
            Getting the Conversation.
          ---------------------------------------------------------------------------------------*/
          p_CONVERSATION_ID_ID := ESB_CONVERSATION_MANAGER_PKG.GET_CONVERSATION_ID(
            p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_,
            p_Locator,
            p_SOURCE_ERROR_CODE,
            p_SOURCE_ERROR_DESC
          );

            -- If -1, it means that the conversation does not exists. This would be the 1st Log.
            IF(p_SOURCE_ERROR_CODE=-1) THEN

              /* ---------------------------------------------------------------------------------------
                Getting the Country.
              ---------------------------------------------------------------------------------------*/
              v_COUNTRY_ID  := ESB_COMMONS_PKG.GET_COUNTRY_ID(
                                            p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_,
                                            p_Locator,
                                            p_SOURCE_ERROR_CODE,
                                            p_SOURCE_ERROR_DESC
                                          );

              -- Does it exist?.
              IF(p_SOURCE_ERROR_CODE!=0) THEN
                IF (p_CREATE_INVALID!='T') THEN
                  RETURN;
                ELSE
                  IF(p_SOURCE_ERROR_CODE=-1) THEN
                    v_COUNTRY_ID := ESB_COMMONS_PKG.CREATE_COUNTRY(
                                              p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_,
                                              'UNK_'|| p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_,
                                              'UNK Country.',
                                              0,
                                              p_Locator,
                                              p_SOURCE_ERROR_CODE,
                                              p_SOURCE_ERROR_DESC
                                          );
                    IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
                  ELSE
                    IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                      -- Updating country with RCD_STATUS = 0 (Invalid).
                      UPDATE
                          ESB_COUNTRY
                      SET
                          RCD_STATUS = 0
                      WHERE
                        CODE = p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_;
                    ELSE
                      RETURN;
                    END IF;
                  END IF;
                END IF;
              END IF;

              /* ---------------------------------------------------------------------------------------
                Getting the Channel.
              ---------------------------------------------------------------------------------------*/
              v_CHANNEL_ID  := ESB_COMMONS_PKG.GET_CHANNEL_ID_BY_NAME(
                                            p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_,
                                            p_Locator,
                                            p_SOURCE_ERROR_CODE,
                                            p_SOURCE_ERROR_DESC
                                          );
              p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_CONVERSATION - EXEC - ESB_COMMONS_PKG.GET_CHANNEL_ID_BY_NAME';

              -- Does it exist?.
              IF(p_SOURCE_ERROR_CODE!=0) THEN
                IF (p_CREATE_INVALID!='T') THEN
                  RETURN;
                ELSE
                  IF(p_SOURCE_ERROR_CODE=-1) THEN
                    v_CHANNEL_ID := ESB_COMMONS_PKG.CREATE_CHANNEL(
                                          'UNK_'|| p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_,
                                          p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_,
                                          0,
                                          p_Locator,
                                          p_SOURCE_ERROR_CODE,
                                          p_SOURCE_ERROR_DESC
                                      );
                    IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
                  ELSE
                    IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                      -- Updating channel with RCD_STATUS = 0 (Invalid).
                      UPDATE
                          ESB_CHANNEL
                      SET
                          RCD_STATUS = 0
                      WHERE
                        NAME = p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_;
                    ELSE
                      RETURN;
                    END IF;
                  END IF;
                END IF;
              END IF;

              /* ---------------------------------------------------------------------------------------
                Getting the Enterprise.
              ---------------------------------------------------------------------------------------*/
              v_ENTERPRISE_ID  := ESB_COMMONS_PKG.GET_ENTERPRISE_ID(
                                            p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_,
                                            p_Locator,
                                            p_SOURCE_ERROR_CODE,
                                            p_SOURCE_ERROR_DESC
                                          );

              -- Does it exist?.
              IF(p_SOURCE_ERROR_CODE!=0) THEN
                IF (p_CREATE_INVALID!='T') THEN
                  RETURN;
                ELSE
                  IF(p_SOURCE_ERROR_CODE=-1) THEN
                    v_ENTERPRISE_ID := ESB_COMMONS_PKG.CREATE_ENTERPRISE(
                                          p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_,
                                          'UNK_'|| p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_,
                                          0,
                                          p_Locator,
                                          p_SOURCE_ERROR_CODE,
                                          p_SOURCE_ERROR_DESC
                                      );
                    IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
                  ELSE
                    IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                      -- Updating Enterprise with RCD_STATUS = 0 (Invalid).
                      UPDATE
                          ESB_ENTERPRISE
                      SET
                          RCD_STATUS = 0
                      WHERE
                        CODE = p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_;
                    ELSE
                      RETURN;
                    END IF;
                  END IF;
                END IF;
              END IF;

              /* ---------------------------------------------------------------------------------------
                Getting the System.
              ---------------------------------------------------------------------------------------*/
              v_SYSTEM_ID  := ESB_COMMONS_PKG.GET_SYSTEM_ID(
                                            p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_,
                                            p_Locator,
                                            p_SOURCE_ERROR_CODE,
                                            p_SOURCE_ERROR_DESC
                                          );

              -- Does it exist?.
              IF(p_SOURCE_ERROR_CODE!=0) THEN
                IF (p_CREATE_INVALID!='T') THEN
                  RETURN;
                ELSE
                  IF(p_SOURCE_ERROR_CODE=-1) THEN
                      v_SYSTEM_ID := ESB_COMMONS_PKG.CREATE_SYSTEM(
                                          p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_,
                                          'UNK_'|| p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_,
                                          'UNK ConsumerSystem.',
                                          0,
                                          p_Locator,
                                          p_SOURCE_ERROR_CODE,
                                          p_SOURCE_ERROR_DESC
                                      );
                    IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
                  ELSE
                    IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                      -- Updating System with RCD_STATUS = 0 (Invalid).
                      UPDATE
                          ESB_SYSTEM
                      SET
                          RCD_STATUS = 0
                      WHERE
                        CODE = p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_;
                    ELSE
                      RETURN;
                    END IF;
                  END IF;
                END IF;
              END IF;

              /* ---------------------------------------------------------------------------------------
                Getting the Consumer.
              ---------------------------------------------------------------------------------------*/
              v_CONSUMER_ID := ESB_COMMONS_PKG.GET_CONSUMER_ID(
                                                  v_SYSTEM_ID,
                                                  v_COUNTRY_ID,
                                                  v_ENTERPRISE_ID,
                                                  p_Locator,
                                                  p_SOURCE_ERROR_CODE,
                                                  p_SOURCE_ERROR_DESC
                                                );

              -- Does it exist?.
              IF(p_SOURCE_ERROR_CODE!=0) THEN
                IF (p_CREATE_INVALID!='T') THEN
                  RETURN;
                ELSE
                  IF(p_SOURCE_ERROR_CODE=-1) THEN
                      v_CONSUMER_ID := ESB_COMMONS_PKG.CREATE_CONSUMER(
                                          v_SYSTEM_ID,
                                          v_COUNTRY_ID,
                                          v_ENTERPRISE_ID,
                                          0,
                                          p_Locator,
                                          p_SOURCE_ERROR_CODE,
                                          p_SOURCE_ERROR_DESC
                                      );
                    IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
                  ELSE
                     IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                          -- Updating Consumer with RCD_STATUS = 0 (Invalid).
                          UPDATE
                              ESB_CONSUMER
                          SET
                              RCD_STATUS = 0
                          WHERE
                            SYSCODE = v_SYSTEM_ID AND
                            COUNTRY_ID = v_COUNTRY_ID AND
                            ENT_CODE = v_ENTERPRISE_ID;
                      ELSE
                        RETURN;
                    END IF;
                  END IF;
                END IF;
              END IF;

              -- Creating the Conversation.
              p_CONVERSATION_ID_ID := ESB_CONVERSATION_MANAGER_PKG.CREATE_CONVERSATION(
                                        p_TRANSACTION_ID,
                                        p_CAPABILITY_ID,
                                        p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_,
                                        v_CHANNEL_ID,
                                        v_CONSUMER_ID,
                                        1,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                    );

              IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

              -- Creating the ConversationStatus.
              ESB_CONVERSATION_MANAGER_PKG.putConversationStatus(
                                        p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                    );

              IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

            ELSE
              IF (p_SOURCE_ERROR_CODE!=0) THEN
                RETURN;
              END IF;

          END IF;

          IF(p_LOG_OPERATION = 'LOG' AND p_RESULT_STATUS IS NOT NULL) THEN

              --LOG operation, not ERROR. Status is only updated during RSP, so if the LOG_TYPE parameter is associated with a RSP, it's assumed that p_RESULT_STATUS is not null.
              IF(p_LOG_TYPE = 'SRSP' OR p_LOG_TYPE = 'USRSP') THEN

                --SYNC STATUS UPDATE
                v_S_STATUS := p_RESULT_STATUS;
                v_S_END_TIME := p_PLACEHOLDER_TIME;

              ELSE

                IF p_LOG_TYPE = 'CSRSP' OR p_LOG_TYPE = 'UCSRSP' THEN
                  --ASYNC STATUS UPDATE
                  v_A_STATUS := p_RESULT_STATUS;
                  v_A_END_TIME := p_PLACEHOLDER_TIME;
                END IF;

              END IF;

              IF (p_RESULT_STATUS = 'ERROR') THEN

                BEGIN
                  SELECT
                    CODE, TYPE
                      INTO v_CAN_ERROR_CODE, v_CAN_ERROR_TYPE
                  FROM
                      (
                        SELECT CE.CODE CODE, CET.TYPE TYPE
                        FROM ESB_ERROR ER
                          INNER JOIN ESB_CANONICAL_ERROR CE ON CE.ID = ER.CAN_ERR_ID
                          INNER JOIN ESB_CANONICAL_ERROR_TYPE CET ON CE.TYPE_ID = CET.ID
                          INNER JOIN ESB_TRACE TR ON TR.ID = ER.TRACE_ID
                          INNER JOIN ESB_CONVERSATION CONV ON CONV.ID = TR.CONV_ID
                        WHERE
                          CONV.CONVERSATION_ID = p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_
                          ORDER BY TRACE_ID DESC --check for optimization
                      )
                    WHERE ROWNUM = 1;
                  EXCEPTION
                    WHEN OTHERS THEN
                      v_CAN_ERROR_CODE := NULL;
                      v_CAN_ERROR_TYPE := NULL;
                  END;
              END IF;

              ESB_CONVERSATION_MANAGER_PKG.updateConversationStatus(
                  p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_,
                  v_S_STATUS,
                  v_A_STATUS,
                  null, --RSP_MSG Deprecated
                  v_CAN_ERROR_CODE,
                  v_CAN_ERROR_TYPE,
                  v_S_END_TIME,
                  v_A_END_TIME,
                  p_SOURCE_ERROR_CODE,
                  p_SOURCE_ERROR_DESC
              );

            END IF;

          ---------------------------------------------------------------------------------------

        EXCEPTION
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );

  END VERIFY_LOG_CONVERSATION;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_LOG_TRACE(
    p_HEADER_TRACER      IN HEADERTRACER_T DEFAULT NULL,
    p_LOG_PLACEHOLDER    IN PLACEHOLDER_T,
    p_CONVERSATION_ID_ID    IN NUMBER,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR

  ) RETURN NUMBER
    IS

    v_TRACE_ID       NUMBER;
    v_SEQUENCE_TRACE NUMBER;

  BEGIN

    p_SOURCE_ERROR_DESC := 'Creating a Trace Element';
    p_SOURCE_ERROR_CODE := '0';
    p_Locator := 'ESB_LOGGERMANAGER_PKG.CREATE_LOG_TRACE - INIT';

    /* ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------*/
      SELECT
        COUNT(T.ID)
          INTO
            v_SEQUENCE_TRACE
      FROM ESB_TRACE T
        INNER JOIN ESB_CONVERSATION C ON T.CONV_ID = C.ID
      WHERE C.ID = p_CONVERSATION_ID_ID;
    ---------------------------------------------------------------------------------------

    INSERT INTO
      ESB_TRACE(
        ID,
        CONV_ID,
        SEQUENCE,
        COMPONENT,
        OPERATION,
        LOG_TIMESTAMP,
        LOG_PLACEHOLDER,
        RCD_STATUS)
      VALUES (
        ESB_TRACE_SEQ.NEXTVAL,
        p_CONVERSATION_ID_ID,
        v_SEQUENCE_TRACE,
        p_HEADER_TRACER.COMPONENT_,
        p_HEADER_TRACER.OPERATION_,
        TO_TIMESTAMP_TZ(p_LOG_PLACEHOLDER.TIME_,'YYYY-MM-DD HH24:MI:SS:FF TZH:TZM'),
        p_LOG_PLACEHOLDER.PLACE_,
        1
      )
      RETURNING ID INTO v_TRACE_ID
      ;

      RETURN v_TRACE_ID;

  END CREATE_LOG_TRACE;

    /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE GET_SEVERITY(p_LOGSEVERITY IN VARCHAR2, v_SEVERITY_ID OUT NUMBER) IS
  BEGIN

    SELECT ID
      INTO v_SEVERITY_ID
    FROM ESB_SEVERITY
    WHERE "LEVEL" = UPPER(p_LOGSEVERITY);

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      INSERT INTO ESB_SEVERITY(ID, "LEVEL", DESCRIPTION, "ORDER", RCD_STATUS) VALUES(ESB_SEVERITY_SEQ.NEXTVAL, UPPER(p_LOGSEVERITY), 'Unknown Level', 100, 0) RETURNING "ID" INTO v_SEVERITY_ID;
  END GET_SEVERITY;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_LOG_TYPE_ID(
    p_LOG_TYPE_NAME      IN VARCHAR2,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  ) RETURN NUMBER
    IS

      v_LOG_TYPE_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The LogType was successfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.GET_LOG_TYPE_ID - INIT';

        SELECT
          ID
            INTO
              v_LOG_TYPE_ID
            FROM
              ESB_LOG_TYPE
            WHERE
              "NAME" = UPPER(p_LOG_TYPE_NAME) AND
              RCD_STATUS <> 2;

        RETURN v_LOG_TYPE_ID;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-1',
              'There is no record with this info : [LogType]=' || p_LOG_TYPE_NAME || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [LogType]=' ||  p_LOG_TYPE_NAME || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END GET_LOG_TYPE_ID;

  /*---------------------------------------------------------------------------------------------------

    **** FUNCTION GET_LOG_MESSAGE :: Creates a registry in ESB_CONVERSATION returning the ID of the new entry.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_LOG_MESSAGE(
    p_CONVERSATION_ID             IN VARCHAR2,
    p_LOG_TYPE_NAME               IN VARCHAR2,
    p_LOG_OCCURENCE               IN NUMBER, -- If more than one message is found for the p_LOG_TYPE_NAME,
                                                -- then the occurrence N (Starting from 1) of that group is returned..
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )  RETURN CLOB
    IS
      v_CONVERSATION_ID_ID NUMBER;
      p_LOG_TYPE_ID NUMBER;
      p_LOG_MESSAGE CLOB;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The LOG was successfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.GET_LOG_MESSAGE - INIT';

        /* ---------------------------------------------------------------------------------------
          Getting the Log Type.
        ---------------------------------------------------------------------------------------*/
        p_LOG_TYPE_ID := GET_LOG_TYPE_ID(
                                  p_LOG_TYPE_NAME,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                                );
        IF(p_SOURCE_ERROR_CODE!='0') THEN RETURN ''; END IF;
        ---------------------------------------------------------------------------------------

        SELECT
          MSG
          RNUM
            INTO
              p_LOG_MESSAGE
        FROM
          (
            SELECT
              LOG.MESSAGE MSG,
              ROWNUM RNUM
            FROM
              ESB_LOG LOG
                INNER JOIN ESB_LOG_TYPE LGT ON LGT.ID = LOG.TYPE_ID
                INNER JOIN ESB_TRACE TRC ON TRC.ID = LOG.TRACE_ID
                INNER JOIN ESB_CONVERSATION CNV ON CNV.ID = TRC.CONV_ID
            WHERE
              CNV.CONVERSATION_ID = p_CONVERSATION_ID AND
              LGT.ID = p_LOG_TYPE_ID
          )
        WHERE
          (p_LOG_OCCURENCE IS NULL OR RNUM=p_LOG_OCCURENCE);

        RETURN p_LOG_MESSAGE;

      EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-1',
              'There is no record with this info : [ConversationID]=' || p_CONVERSATION_ID || ' - [LogTypeName]=' || p_LOG_TYPE_NAME || ' .',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); -- To trigger retries, if requiered.
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [ConversationID]=' || p_CONVERSATION_ID || ' - [LogTypeName]=' || p_LOG_TYPE_NAME || ' .',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN '';
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN '';
  END GET_LOG_MESSAGE;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_LOG_SEVERITY_ID(
    p_LOG_SEVERITY_LEVEL      IN VARCHAR2,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  ) RETURN NUMBER
    IS

      v_LOG_SEVERITY_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The LogType was successfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.GET_LOG_SEVERITY_ID - INIT';

        SELECT
          ID
            INTO
              v_LOG_SEVERITY_ID
        FROM
          ESB_SEVERITY
        WHERE
          "LEVEL" = UPPER(p_LOG_SEVERITY_LEVEL) AND
          RCD_STATUS <> 2;

        RETURN v_LOG_SEVERITY_ID;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-1',
              'There is no record with this info : [LogSeverity]=' || p_LOG_SEVERITY_LEVEL || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );

            RETURN 0;
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [LogSeverity]=' ||  p_LOG_SEVERITY_LEVEL || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END GET_LOG_SEVERITY_ID;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_LOG_CONFIG(
    p_LOG_CONFIG         IN LOGMODE_T,
    p_LOG_TYPE_ID        OUT VARCHAR,
    p_LOG_SEVERITY_ID    OUT VARCHAR,
    p_CREATE_INVALID     IN CHAR,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  )
    IS

      BEGIN

        p_SOURCE_ERROR_DESC := 'Verifing the Log Config';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_CONFIG - INIT';

        /* ---------------------------------------------------------------------------------------
          Getting the Log Type.
        ---------------------------------------------------------------------------------------*/
        p_LOG_TYPE_ID := GET_LOG_TYPE_ID(
                                  p_LOG_CONFIG.LOGTYPE_,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                                );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE=-1) THEN
          -- Assuming the generic 'LOG' LogType
          p_LOG_TYPE_ID := GET_LOG_TYPE_ID(
                                    'LOG',
                                    p_Locator,
                                    p_SOURCE_ERROR_CODE,
                                    p_SOURCE_ERROR_DESC
                                  );
          IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        ELSE
          IF (p_SOURCE_ERROR_CODE!=0) THEN
            RETURN;
          END IF;
        END IF;
        ---------------------------------------------------------------------------------------

        /* ---------------------------------------------------------------------------------------
          Getting the Log Type.
        ---------------------------------------------------------------------------------------*/
        p_LOG_SEVERITY_ID := GET_LOG_SEVERITY_ID(
                                  p_LOG_CONFIG.LOGSEVERITY_,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                                );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE=-1) THEN
          -- Assuming the generic 'LOG' LogType
          p_LOG_SEVERITY_ID := GET_LOG_SEVERITY_ID(
                                    'INFO',
                                    p_Locator,
                                    p_SOURCE_ERROR_CODE,
                                    p_SOURCE_ERROR_DESC
                                  );
          IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        ELSE
          IF (p_SOURCE_ERROR_CODE!=0) THEN
            RETURN;
          END IF;
        END IF;
        ---------------------------------------------------------------------------------------

      EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
  END VERIFY_LOG_CONFIG;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_LOG_METADATA(
    p_HEADER_TRACER      IN HEADERTRACER_T,
    p_LOG_TYPE           IN VARCHAR2,
    p_RESULT_STATUS      IN VARCHAR2,
    p_PLACEHOLDER_TIME   IN VARCHAR2,
    p_LOG_OPERATION      IN VARCHAR2,
    p_CAPABILITY_ID      OUT NUMBER,
    p_CONVERSATION_ID    OUT NUMBER,
    p_TRANSACTION_ID     OUT NUMBER,
    p_CREATE_INVALID     IN CHAR,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  )
    IS

      BEGIN

        p_SOURCE_ERROR_DESC := 'Verifing the Log Metadata';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_METADATA - INIT';

        VERIFY_SERVICE_CAPABILITY(
                  p_HEADER_TRACER.HEADER_.TRACE_.SERVICE_.NAME_,
                  p_HEADER_TRACER.HEADER_.TRACE_.SERVICE_.OPERATION_,
                  p_HEADER_TRACER.HEADER_.TRACE_.SERVICE_.CODE_,
                  p_CAPABILITY_ID,
                  p_CREATE_INVALID,
                  p_Locator,
                  p_SOURCE_ERROR_CODE,
                  p_SOURCE_ERROR_DESC
                );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        VERIFY_MESSAGE_TRANSACTION(
                  p_CAPABILITY_ID,
                  p_HEADER_TRACER.HEADER_.TRACE_.PROCESSID_,
                  p_HEADER_TRACER.HEADER_.TRACE_.EVENTID_,
                  p_HEADER_TRACER.HEADER_.TRACE_.CLIENTREQTIMESTAMP_,
                  p_HEADER_TRACER.HEADER_.TRACE_.CORRELATIONID_,
                  p_HEADER_TRACER.HEADER_.TRACE_.SOURCEID_,
                  p_HEADER_TRACER.HEADER_.TRACE_.CORRELATIONEVENTID_,
                  p_TRANSACTION_ID,
                  p_CREATE_INVALID,
                  p_Locator,
                  p_SOURCE_ERROR_CODE,
                  p_SOURCE_ERROR_DESC
                );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        VERIFY_LOG_CONVERSATION(
            p_TRANSACTION_ID,
            p_HEADER_TRACER,
            p_LOG_TYPE,
            p_RESULT_STATUS,
            p_PLACEHOLDER_TIME,
            p_LOG_OPERATION,
            p_CONVERSATION_ID,
            p_CAPABILITY_ID,
            p_CREATE_INVALID,
            p_Locator,
            p_SOURCE_ERROR_CODE,
            p_SOURCE_ERROR_DESC
          );
       IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

      EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
  END;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE LOG(
    LOGREQ IN LOG_T,
    p_LOG_FLAG IN VARCHAR,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  ) IS

    v_LOG_TYPE_ID     NUMBER;
    v_LOG_SEVERITY_ID NUMBER;
    v_CONVERSATION_ID NUMBER;
    v_CONVERSATION_ID_AUX  NUMBER;
    v_TRANSACTION_ID NUMBER;
    v_CAPABILITY_ID NUMBER;
    v_TRACE_ID NUMBER;
    v_LOG_OPERATION VARCHAR(30);

    BEGIN

      v_LOG_OPERATION := 'LOG';
      p_Locator := 'ESB_LOGGERMANAGER_PKG.LOG - INIT';
      p_SOURCE_ERROR_CODE := '0' ;
      p_SOURCE_ERROR_DESC := '' ;

      VERIFY_LOG_METADATA (
          LOGREQ.HEADERTRACER_,
          LOGREQ.LOGMODE_.LOGTYPE_,
          LOGREQ.RESULTSTATUS_,
          LOGREQ.LOGPLACEHOLDER_.TIME_,
          v_LOG_OPERATION,
          v_CAPABILITY_ID,
          v_CONVERSATION_ID,
          v_TRANSACTION_ID,
          'T',
          p_Locator,
          p_SOURCE_ERROR_CODE,
          p_SOURCE_ERROR_DESC
      );
      IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

      -- Checking whether the LOG function for the loggerManager is activated. If not, just the state of the associated Conversation and Transaction is reviewed (as done before).
       IF ( p_LOG_FLAG <> '3'  ) THEN

        v_TRACE_ID := CREATE_LOG_TRACE(
            LOGREQ.HEADERTRACER_,
            LOGREQ.LOGPLACEHOLDER_,
            v_CONVERSATION_ID,
            p_Locator,
            p_SOURCE_ERROR_CODE,
            p_SOURCE_ERROR_DESC
        );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

        VERIFY_LOG_CONFIG(
            LOGREQ.LOGMODE_,
            v_LOG_TYPE_ID,
            v_LOG_SEVERITY_ID,
            'T',
            p_Locator,
            p_SOURCE_ERROR_CODE,
            p_SOURCE_ERROR_DESC
        );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

        INSERT INTO ESB_LOG(
          ID,
          TRACE_ID,
          MESSAGE, TYPE_ID,
          SEVERITY_ID,
          DESCRIPTION
          )
        VALUES(
          ESB_LOG_SEQ.NEXTVAL,
          v_TRACE_ID,
          LOGREQ.MESSAGE_,
          v_LOG_TYPE_ID,
          v_LOG_SEVERITY_ID,
          LOGREQ.DESCRIPTION_
        );

      END IF;

      -----------------------------------------------------------------------------------------------------------------------
      -- Los siguientes statement tienen como proposito actualizar el estado de la transaccion en ESB_MESSAGE_TRANSACTION,
      -- en casos donde la dupla Servicio+Capacidad no sea configurada para aplicar validaciones sobre sus mensajes por el
      -- message manager. En estos casos, para optimizar el MessageManager, no se realizan tareas Sync para con las tablas
      -- de traza.
      --
      -- El LoggerManager esta preparado para este comportamiento y genera el registro en ESB_MESSAGE_TRANSACTION si no lo existe.
      --
      -- Ahora bien, para mantener la integridad de los registros existentes en dicha tabla, se debe tambien actualizar el estado
      -- cuando se informe la finalizacion de la Tx. la Tx. se considera finalizada para el cliente cuando la capacidad de
      -- secuencia "0" retorna un mensaje. Dicha capacidad se considera comola "Owner" de la transaccion al ser la que representa
      -- su ejecucion desde el punto del vista del cliente.
      --
      -- Por ello lo primero que hacemos en verificar que la operacion solicitada al LoggerManager sea LOG, y el tipo sea SRSP,
      -- indicando que se informa la respuesta de un Servicio (Capacidad). Luego verificamos si la capacidad que se esta tratando
      -- es la de sencuencia 0, y el estado de la Tx. sigue siendo "0" (Pending. De aqui asumimos la no participacion del
      -- MessageManager).
      -----------------------------------------------------------------------------------------------------------------------

      IF (LOGREQ.LOGMODE_.LOGTYPE_ = 'SRSP' OR LOGREQ.LOGMODE_.LOGTYPE_ = 'USRSP') THEN

        v_CONVERSATION_ID_AUX := ESB_MESSAGEMANAGER_PKG.GET_TX_OWNER_ID(
                                v_TRANSACTION_ID,
                                p_Locator,
                                p_SOURCE_ERROR_CODE,
                                p_SOURCE_ERROR_DESC);
        IF(p_SOURCE_ERROR_CODE=0) THEN

          /** Updating TX. Status to 1 (Finished) **/
          IF (v_CONVERSATION_ID = v_CONVERSATION_ID_AUX) THEN
            v_TRANSACTION_ID := ESB_MESSAGEMANAGER_PKG.UPDATE_TRANSACTION_STATUS(
                                  LOGREQ.HEADERTRACER_.HEADER_.TRACE_.EVENTID_,
                                  LOGREQ.HEADERTRACER_.HEADER_.TRACE_.PROCESSID_,
                                  LOGREQ.HEADERTRACER_.HEADER_.TRACE_.CORRELATIONID_,
                                  LOGREQ.HEADERTRACER_.HEADER_.TRACE_.CLIENTREQTIMESTAMP_,
                                  v_CAPABILITY_ID,
                                  1,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                                );
            IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

          END IF;
         END IF;
      END IF;
      --------------------------------------------------

      p_Locator := '';
      p_SOURCE_ERROR_CODE := '0' ;
      p_SOURCE_ERROR_DESC := '' ;

  EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
          ROLLBACK;
          --RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC);
  END LOG;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE ERROR(
    ERRORREQ IN ERROR_T,
    p_LOG_FLAG IN VARCHAR,
    p_Locator                   OUT VARCHAR,
    p_SOURCE_ERROR_CODE         OUT VARCHAR,
    p_SOURCE_ERROR_DESC         OUT VARCHAR
  ) IS

    v_STATUS_ID NUMBER;
    v_CANONICAL_ERROR_ID NUMBER;
    v_SOURCE_ERROR_SYSTEM_ID NUMBER;
    v_CAPABILITY_ID NUMBER;
    v_CONVERSATION_ID NUMBER;
    v_TRANSACTION_ID NUMBER;
    v_TRACE_ID NUMBER;

  v_LOG_OPERATION VARCHAR(30);

  BEGIN

      v_LOG_OPERATION := 'ERROR';
      p_Locator := 'ESB_LOGGERMANAGER_PKG.ERROR - INIT';
      p_SOURCE_ERROR_CODE := '0' ;
      p_SOURCE_ERROR_DESC := '' ;

      VERIFY_LOG_METADATA (
          ERRORREQ.HEADERTRACER_,
          null,
          null,
          null,
          v_LOG_OPERATION,
          v_CAPABILITY_ID,
          v_CONVERSATION_ID,
          v_TRANSACTION_ID,
          'F',
          p_Locator,
          p_SOURCE_ERROR_CODE,
          p_SOURCE_ERROR_DESC
      );
      IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

      -- Checking whether the LOG function for the loggerManager is activated. If not, just the state of the associated Conversation and Transaction is reviewed (as done before).
      IF (p_LOG_FLAG <> '3') THEN

        v_TRACE_ID := CREATE_LOG_TRACE(
            ERRORREQ.HEADERTRACER_,
            ERRORREQ.ERRORPLACEHOLDER_,
            v_CONVERSATION_ID,
            p_Locator,
            p_SOURCE_ERROR_CODE,
            p_SOURCE_ERROR_DESC
        );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

      /* --------------------------------------------------------------------------------------------------
        Retrieving the Result State. If its not found, the 'ERROR' Result Statue used is used instead.
      */--------------------------------------------------------------------------------------------------
        v_STATUS_ID := ESB_ERROR_MANAGER_PKG.GET_RESULT_STATUS_ID(
                                    ERRORREQ.RESULT_.STATUS_,
                                    p_Locator,
                                    p_SOURCE_ERROR_CODE,
                                    p_SOURCE_ERROR_DESC);

        IF(p_SOURCE_ERROR_CODE!='0') THEN
            v_STATUS_ID := ESB_ERROR_MANAGER_PKG.GET_DEF_RESULT_S_ID_ERROR(
                                    p_Locator,
                                    p_SOURCE_ERROR_CODE,
                                    p_SOURCE_ERROR_DESC);
            IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;
        END IF;
      ------------------------------------------------------------------------------------------------------

      /* --------------------------------------------------------------------------------------------------
        Retrieving the Canonical Error. If its not found, the default CANONICAL ERROR used
        for those types of scenarios is used.
      */ --------------------------------------------------------------------------------------------------
        v_CANONICAL_ERROR_ID := ESB_ERROR_MANAGER_PKG.GET_CAN_ERR_ID(
                                                          ERRORREQ.RESULT_.CANONICALERROR_.CODE_,
                                                          ERRORREQ.RESULT_.CANONICALERROR_.TYPE_,
                                                          p_Locator,
                                                          p_SOURCE_ERROR_CODE,
                                                          p_SOURCE_ERROR_DESC
                                                        );

        IF(p_SOURCE_ERROR_CODE!='0') THEN
          v_CANONICAL_ERROR_ID := ESB_ERROR_MANAGER_PKG.GET_DEF_CAN_ERR_ID_NO_DATA(
                                                          p_Locator,
                                                          p_SOURCE_ERROR_CODE,
                                                          p_SOURCE_ERROR_DESC
                                                        );
          IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;
        END IF;
      ------------------------------------------------------------------------------------------------------

      /* ---------------------------------------------------------------------------------------
              Getting the Source Error System.
      ---------------------------------------------------------------------------------------*/
      v_SOURCE_ERROR_SYSTEM_ID  := ESB_COMMONS_PKG.GET_SYSTEM_ID(
                                    ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_,
                                    p_Locator,
                                    p_SOURCE_ERROR_CODE,
                                    p_SOURCE_ERROR_DESC
                                  );
      -- Does it exist?.
      IF(p_SOURCE_ERROR_CODE=-1) THEN

        v_SOURCE_ERROR_SYSTEM_ID := ESB_COMMONS_PKG.CREATE_SYSTEM(
                                  ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_,
                                  'UNK_'|| ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_,
                                  'UNK SourceErrorSystem.',
                                  0,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                              );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

      ELSE
        IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.

          -- Updating System with RCD_STATUS = 0 (Invalid).
          UPDATE
              ESB_SYSTEM
          SET
              RCD_STATUS = 0
          WHERE
            CODE = ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_;

        ELSE

          IF (p_SOURCE_ERROR_CODE!=0) THEN
            RETURN;
          END IF;

        END IF;
      END IF;

      ------------------------------------------------------------------------------------------------------

      INSERT INTO ESB_ERROR(
        ID,
        TRACE_ID,
        STATUS_ID,
        DESCRIPTION,
        CAN_ERR_ID,
        RAW_FAULT,
        RAW_CODE,
        RAW_DESCRIPTION,
        ERROR_SOURCE,
        ERROR_SOURCE_DETAILS,
        MODULE,
        SUB_MODULE,
        RCD_STATUS
      )
      VALUES(
        ESB_ERROR_SEQ.NEXTVAL,
        v_TRACE_ID,
        v_STATUS_ID,
        ERRORREQ.RESULT_.DESCRIPTION_,
        v_CANONICAL_ERROR_ID,
        ERRORREQ.RESULT_.SOURCEERROR_.SOURCEFAULT_,
        ERRORREQ.RESULT_.SOURCEERROR_.CODE_,
        ERRORREQ.RESULT_.SOURCEERROR_.DESCRIPTION_,
        v_SOURCE_ERROR_SYSTEM_ID,
        ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.DETAILS,
        ERRORREQ.ERRORINDEX_.MODULE_,
        ERRORREQ.ERRORINDEX_.SUBMODULE_,
        1
      );
    END IF;

    p_Locator := '';
    p_SOURCE_ERROR_CODE := '0' ;
    p_SOURCE_ERROR_DESC := '' ;

    EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
          ROLLBACK;

  END ERROR;

END ESB_LOGGERMANAGER_PKG;
/
