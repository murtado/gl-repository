--------------------------------------------------------
--  DDL for Table ESB_CONVERSATION_STATUS
--------------------------------------------------------
  
  CREATE TABLE "ESB_CONVERSATION_STATUS" 
  (	
  	"ID" NUMBER NOT NULL,
	"CONVERSATION_ID" VARCHAR2(100 BYTE) NOT NULL, 
	"STATUS" VARCHAR2(30 BYTE) DEFAULT 'PENDING', 
	"S_STATUS" VARCHAR2(30 BYTE) DEFAULT 'PENDING', 
	"A_STATUS" VARCHAR2(30 BYTE) DEFAULT 'NONE', 
	"RSP_MSG" CLOB, 
	"CAN_ERR_ID" NUMBER, 
	"CREATION_TIMESTAMP" TIMESTAMP, 
	"UPDATE_COMPONENT" VARCHAR2(50),
	"UPDATE_OPERATION" VARCHAR2(50),
	"S_END_TIME" TIMESTAMP(6),
	"A_END_TIME" TIMESTAMP(6),
	"OBSERVATIONS" VARCHAR2(255),
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	"CREATIONDATE" DATE DEFAULT CURRENT_DATE,
	CONSTRAINT "ESB_CONVERSATION_STATUS_PK" PRIMARY KEY ("ID"), 
	CONSTRAINT "CANONICAL_ERROR_FK" FOREIGN KEY ("CAN_ERR_ID") REFERENCES "ESB_CANONICAL_ERROR" ("ID") ON DELETE CASCADE,
	CONSTRAINT "ESB_CONVERSATION_STATUS_CK" CHECK ("RCD_STATUS" IN(0,1,2))
  );
  COMMENT ON COLUMN "ESB_CONVERSATION_STATUS"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE, 2=DELETED';