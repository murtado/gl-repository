SPOOL salida_DROP_ALL_PACKAGES.txt
@.\01_Scripts\04_SQL_Packages\DROP_ESB_CONVERSATION_MANAGER_PKG.sql
@.\01_Scripts\04_SQL_Packages\DROP_ESB_CORRELATIONMANAGER_PKG.sql
@.\01_Scripts\04_SQL_Packages\DROP_ESB_ERROR_HOSPITAL_PKG.sql
@.\01_Scripts\04_SQL_Packages\DROP_ESB_ERROR_MANAGER_PKG.sql
@.\01_Scripts\04_SQL_Packages\DROP_ESB_LOGGERMANAGER_PKG.sql
@.\01_Scripts\04_SQL_Packages\DROP_ESB_MESSAGEMANAGER_PKG.sql
@.\01_Scripts\04_SQL_Packages\DROP_ESB_PARAMETERMANAGER_PKG.sql
@.\01_Scripts\04_SQL_Packages\DROP_ESB_COMMONS_PKG.sql

--------------------------------------------------------
--  RETRYMANAGER
--------------------------------------------------------
@.\01_Scripts\04_SQL_Packages\DROP_ESB_RETRY_MANAGER_PKG.sql
SPOOL OFF;
/