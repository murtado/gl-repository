SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		DELETE FROM  ESB_CDM_FIELD WHERE ID = (SELECT ID FROM ESB_CDM_FIELD WHERE ENTITY_ID = (SELECT ID FROM ESB_CDM_ENTITY WHERE NAME = 'Client') AND NAME = 'Type' AND DESCRIPTION = 'Client Type' AND RCD_STATUS='1');
	END;
	
	BEGIN
		DELETE FROM  ESB_CDM_FIELD WHERE ID = (SELECT ID FROM ESB_CDM_FIELD WHERE ENTITY_ID = (SELECT ID FROM ESB_CDM_ENTITY WHERE NAME = 'Client') AND NAME ='Status' AND DESCRIPTION = 'Client Status' AND RCD_STATUS='1');
	END;

	BEGIN
		DELETE FROM  ESB_CDM_FIELD WHERE ID = (SELECT ID FROM ESB_CDM_FIELD WHERE ENTITY_ID = (SELECT ID FROM ESB_CDM_ENTITY WHERE NAME = 'CanonicalError') AND NAME = 'code' AND DESCRIPTION = 'CanonicalError code' AND RCD_STATUS='1');
	END;
	
END;
/

COMMIT;