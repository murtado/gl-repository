SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

UPDATE ESB_CONFIG_LIST
SET VALUE = 'http://10.51.12.17:8502/salesforce/clientDelete'
WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'CHL-SALESFORCE-CRM_ClientDelete_HTTPSOAP12') AND
   PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url');

UPDATE ESB_CONFIG_LIST
SET VALUE = '""'
WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'CHL-SALESFORCE-CRM_ClientDelete_HTTPSOAP12') AND
   PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction');

END;
/
COMMIT;
