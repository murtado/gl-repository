SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK
DECLARE
	--@Autor: ---

	V_SYS_CODE VARCHAR2(50);
	V_ENTERPRISE_CODE VARCHAR2(50);
	V_COUNTRY_CODE VARCHAR2(50);

	V_SERVICE_CODE VARCHAR2(50);
	V_SERVICE_NAME VARCHAR2(50);
	V_CAPABILITY_NAME VARCHAR2(50);

	V_CAPABILITY_ID NUMBER;
	V_CAPABILITY_DETAIL_ID number;
	V_SYS_ID NUMBER;
	V_ENTERPRISE_ID NUMBER;
	V_COUNTRY_ID NUMBER;

	V_CONSUMER_ID NUMBER;

	V_CONSUMER_DETAILS_ID NUMBER;

	V_CALLBACK_URI VARCHAR2(150);
	V_CALLBACK_TRANSPORT VARCHAR2(50);

	V_FORCE_CREATE_CONSUMER NUMBER;

	V_FORCE_CREATE_DETAIL_ID NUMBER;

	V_SOAP_ACTION VARCHAR2(150);
BEGIN

	-- Tablas involucradas en este ADP
	--ESB_SYSTEM
	--ESB_CONSUMER
	--ESB_ENTERPRISE
	--ESB_COUNTRY
	--ESB_CONSUMER_CAP_DETAILS
	--ESB_CAPABILITY
	--ESB_SERVICE
	--ESB_CAPABILITY_DETAILS
	--ESB_CAPABILITY_DETAILS_TYPE
	--ESB_CONSUMER_CAP_DETAILS_TYPE


	--Identificadores de la Capacidad del Servicio que va a tener configurada de callback
	V_SERVICE_CODE :='REPLACE_WITH_SERVICE_CODE';
	V_SERVICE_NAME :='REPLACE_WITH_SERVICE_NAME';
	V_CAPABILITY_NAME :='REPLACE_WITH_CAPABILITY_NAME';
	

	--Codigo de sistema, empresa y pais para identificar el consumidor de la capacidad
	--Codigo de sistema consumidor
	V_SYS_CODE:= 'REPLACE_WITH_SYSTEM_CODE';
	--Codigo de empresa  consumidora
	V_ENTERPRISE_CODE:= 'REPLACE_WITH_ENTERPRISE_CODE';
	--Codigo de pais consumidor
	V_COUNTRY_CODE := 'REPLACE_WITH_COUNTRY_CODE';

	-- URL Endpoint donde se enviara el callback. Puede ser por HTTP o JMS
	-- endpoint http://www.siebel.com/service/receive/calbackk/entel  | jms://hostname:7001/.jms.connFactory.xa/.jms.siebel.receive.callback
	V_CALLBACK_URI := 'REPLACE_WITH_COUNTRY_CODE';
	
	-- Protocolo de transporte y mensajeria  que se utilizara para enviar el callback
	-- IMPORTANTE: SELECCIONAR UNO DE LOS SIGUIENTES VALORES
	-- IMPORTANTE: SELECCIONAR UNO DE LOS SIGUIENTES VALORES
	-- transport  HTTPSOAP11, HTTPSOAP12.
	V_CALLBACK_TRANSPORT := 'REPLACE_WITH_CALLBACK_TRANSPORT';

	-- SOAP Action que se utilizara para identificar el servicio
	-- Si se utiliza TRANSPORTE JMS no se utiliza
	V_SOAP_ACTION := 'REPLACE_WITH_CALLBACK_SOAPACTION';
  
  -- FLAG para forzar la creacion del consumidor si este no se encuentra.
  -- 1: si no existe, se crea el consumer
  -- cualquier otro valor: Si no existe, se comunica que no se encuentra, y finaliza el script
  V_FORCE_CREATE_CONSUMER := 1;
  
  -- FLAG para forzar la creacion de la configuracion. Si la configuracion existe, se actualiza
  -- 1: si no existe, se crea el consumer
  -- cualquier otro valor: Si no existe, se comunica que no se encuentra, y finaliza el script. 
  V_FORCE_CREATE_DETAIL_ID := 1;



  -- Busqueda de la Capacidad del Servicio que va a tener configurado un consumidor con un callback
  BEGIN
    SELECT C.ID INTO V_CAPABILITY_ID
    FROM ESB_SERVICE S INNER JOIN ESB_CAPABILITY C ON (S.ID = C.SERVICE_ID)
    WHERE S.RCD_STATUS = 1 AND C.RCD_STATUS = 1 AND S.CODE = V_SERVICE_CODE AND S.NAME = V_SERVICE_NAME AND C.NAME = V_CAPABILITY_NAME;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        BEGIN
          DBMS_OUTPUT.PUT_LINE('Servicio y capacidad no se encuentra, o no esta activada');
          raise;
        END;
  	END;
 	BEGIN 
		SELECT ID INTO V_CAPABILITY_DETAIL_ID
		FROM ESB_CAPABILITY_DETAILS
		WHERE CAPABILITY_ID = V_CAPABILITY_ID AND DETAIL_TYPE_ID = ( SELECT ID  FROM ESB_CAPABILITY_DETAILS_TYPE  WHERE DESCRIPTION = 'SOAP Action' AND RCD_STATUS <> 2 )  AND RCD_STATUS <> 2;
		
		UPDATE ESB_CAPABILITY_DETAILS SET DETAIL_CONTENT = V_SOAP_ACTION, RCD_STATUS = 1 WHERE ID = V_CAPABILITY_DETAIL_ID;

	    EXCEPTION
			WHEN NO_DATA_FOUND THEN
			BEGIN
				INSERT INTO ESB_CAPABILITY_DETAILS(ID, CAPABILITY_ID, DETAIL_TYPE_ID, DETAIL_CONTENT) VALUES (ESB_CAPABILITY_DETAILS_SEQ.NEXTVAL, V_CAPABILITY_ID, ( SELECT ID  FROM ESB_CAPABILITY_DETAILS_TYPE  WHERE DESCRIPTION = 'SOAP Action' AND RCD_STATUS <> 2 ), V_SOAP_ACTION);
			END;
	END;



	BEGIN
		SELECT ID INTO V_SYS_ID FROM ESB_SYSTEM WHERE CODE = V_SYS_CODE;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			BEGIN 
				DBMS_OUTPUT.PUT_LINE('Sistema con codigo: ' || V_SYS_CODE || ' no existe ');
        raise;
			END;
	end;

	BEGIN
		SELECT ID INTO V_ENTERPRISE_ID FROM ESB_ENTERPRISE WHERE CODE = V_ENTERPRISE_CODE;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			BEGIN 
				DBMS_OUTPUT.PUT_LINE('Empresa con codigo: ' || V_ENTERPRISE_CODE || ' no existe ');
        raise;
			END;
	end;

	BEGIN
		SELECT ID INTO V_COUNTRY_ID FROM ESB_COUNTRY WHERE CODE = V_COUNTRY_CODE;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			BEGIN 
				DBMS_OUTPUT.PUT_LINE('Pais con codigo: ' || V_COUNTRY_CODE || ' no existe ');
        raise;
			END;
	end;


	BEGIN
		SELECT ID INTO V_CONSUMER_ID 
		FROM ESB_CONSUMER 
		WHERE SYSCODE = V_SYS_ID AND COUNTRY_ID = V_COUNTRY_ID AND ENT_CODE = V_ENTERPRISE_ID;
		
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			BEGIN 
        		if V_FORCE_CREATE_CONSUMER = 1 
          		then
           			INSERT INTO ESB_CONSUMER (ID, SYSCODE, COUNTRY_ID, ENT_CODE) VALUES (ESB_CONSUMER_SEQ.NEXTVAL, V_SYS_ID, V_COUNTRY_ID, V_ENTERPRISE_ID) RETURNING ID INTO V_CONSUMER_ID;
				ELSE 
            		begin
                DBMS_OUTPUT.PUT_LINE('Consumer con sistema : ' || V_SYS_CODE || ', Empresa: ' ||  V_ENTERPRISE_CODE || ' y pais: ' || V_COUNTRY_CODE || ' no existe' );
                raise;
            		end;
        		end if;
			END;
	END;

	BEGIN
	
		select id into V_CONSUMER_DETAILS_ID
		from  ESB_CONSUMER_CAP_DETAILS	
		WHERE CONSUMER_ID = V_CONSUMER_ID AND CAPABILITY_ID = V_CAPABILITY_ID AND DETAIL_TYPE_ID = (SELECT ID FROM ESB_CONSUMER_CAP_DETAILS_TYPE WHERE NAME = 'CALLBACK_URL');
      
		UPDATE ESB_CONSUMER_CAP_DETAILS	
		SET DETAIL_CONTENT = V_CALLBACK_URI, RCD_STATUS = 1
		WHERE id = V_CONSUMER_DETAILS_ID;
    DBMS_OUTPUT.PUT_LINE('El URL Callback del consumidor ya existe. Se ha actualizado');
		EXCEPTION 
			WHEN no_data_found THEN
				BEGIN
					if V_FORCE_CREATE_DETAIL_ID = 1
	        		then
						begin
							INSERT INTO ESB_CONSUMER_CAP_DETAILS (ID, CAPABILITY_ID, CONSUMER_ID, CONTEXT, DETAIL_TYPE_ID, DETAIL_CONTENT)
							VALUES (ESB_CONSUMER_CAP_DETAILS_SEQ.NEXTVAL, V_CAPABILITY_ID, V_CONSUMER_ID, '', (SELECT ID FROM ESB_CONSUMER_CAP_DETAILS_TYPE WHERE NAME = 'CALLBACK_URL'), V_CALLBACK_URI  );
							DBMS_OUTPUT.PUT_LINE('El URL Callback del consumidor se ha insertado');
						end;
					else
						    DBMS_OUTPUT.PUT_LINE('El URL Callback del consumidor no existe');       
					end if;
				end;
	END;

	BEGIN
		select id into V_CONSUMER_DETAILS_ID
		from  ESB_CONSUMER_CAP_DETAILS	
		WHERE CONSUMER_ID = V_CONSUMER_ID AND CAPABILITY_ID = V_CAPABILITY_ID AND DETAIL_TYPE_ID = (SELECT ID FROM ESB_CONSUMER_CAP_DETAILS_TYPE WHERE NAME = 'TRANSPORT');
    
	    update esb_consumer_cap_details
	    SET DETAIL_CONTENT = V_CALLBACK_TRANSPORT, RCD_STATUS = 1
	    where id = V_CONSUMER_DETAILS_ID;

    DBMS_OUTPUT.PUT_LINE('El Transporte del consumidor ya existe. Se ha actualizado');
		EXCEPTION 
			when no_data_found then
				begin
	        		if V_FORCE_CREATE_DETAIL_ID = 1
			        then
						begin
							INSERT INTO ESB_CONSUMER_CAP_DETAILS (ID, CAPABILITY_ID, CONSUMER_ID, CONTEXT, DETAIL_TYPE_ID, DETAIL_CONTENT)
							VALUES (ESB_CONSUMER_CAP_DETAILS_SEQ.NEXTVAL, V_CAPABILITY_ID, V_CONSUMER_ID, '', (SELECT ID FROM ESB_CONSUMER_CAP_DETAILS_TYPE WHERE NAME = 'TRANSPORT'), V_CALLBACK_TRANSPORT  );	
							DBMS_OUTPUT.PUT_LINE('El Transporte del consumidor se ha insertado');
						end;
			        else
            			 DBMS_OUTPUT.PUT_LINE('El transporte del consumidor no existe');        
        			end if;
     			 end;
	END;
  COMMIT;
END;
/
