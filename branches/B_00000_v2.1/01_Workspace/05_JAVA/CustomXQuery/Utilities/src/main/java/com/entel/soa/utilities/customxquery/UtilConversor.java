package com.entel.soa.utilities.customxquery;

import org.apache.commons.codec.binary.Base64;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UtilConversor {

	 public static String encodeBase64(String stringToEncode){
	 byte[] encodedBytes = Base64.encodeBase64(stringToEncode.getBytes());
	  return new String(encodedBytes);
	 }
	 
	 public static String decodeBase64(String stringToDecode){
	 byte[] decodedBytes = Base64.decodeBase64(stringToDecode.getBytes());
	  return new String(decodedBytes);
	 }
	 
	 
	public static byte[] sha1(byte[] toHash) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(toHash);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}

		return result;
	}
	
	
	public static String sha1(String input) throws NoSuchAlgorithmException {
	        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
	        byte[] result = mDigest.digest(input.getBytes());
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < result.length; i++) {
	            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
	        }
	         
	        return sb.toString();
	   }


	
	
	public static String passwordDigest(String nonce, String fecha ,String password )  {
		try {
		ByteBuffer buf = ByteBuffer.allocate(1000);
		buf.put(decodeBase64(nonce).getBytes());
	    buf.put(fecha.getBytes("UTF-8"));
		buf.put(password.getBytes("UTF-8"));		
		byte[] toHash = new byte[buf.position()];
		buf.rewind();
		buf.get(toHash);
		byte[] hash = UtilConversor.sha1(toHash);
		
		return new String(Base64.encodeBase64(hash));
		
		} catch (Exception e) {			
			e.printStackTrace();
			
	 }
		return"error passwordDigest";
	
	
	
	}
	
	 
	 
	
}
