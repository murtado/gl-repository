package com.entel.frw.comp.cache.core.test.cases;

import java.util.Date;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.exception.ValueIsNullException;
import com.entel.frw.comp.cache.core.test.CacheTest;

public class Test_FullRefresh extends CacheTest  {
	
	public void execute() throws ValueIsNullException  {
	
		// -----------------------------------------
			String testName = "test_FullRefresh";
			Date startDate = new Date();
			boolean testResult = false;
			String testObs = null;
			int auxIndex = 0;
		// -----------------------------------------
	
		CacheCore.put("A", "1", "1", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("A", "2", "2", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("A", "2", "3", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "1", "4", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "2", "5", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "3", "6", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "4", "7", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		
		CacheCore.lazyRefresh("", "", "");
		
		if (CacheCore.getKeyCount() != 0){
			testObs = "While test a FULL REFRESH, we had + [" + auxIndex + "] PUT requests. Final Count is [" + CacheCore.getKeyCount() + "]. Should be 0";
			CacheTest.printResult(testName, startDate, testResult, testObs);
			return;
		}
		
		auxIndex = 0;
		
		CacheCore.put("A", "1", "1", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("A", "2", "2", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("A", "2", "3", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "1", "4", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "2", "5", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "3", "6", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		CacheCore.put("B", "4", "7", XmlObject.Factory.newInstance(), 0); ++auxIndex;
		
		CacheCore.lazyRefresh("B", "", "");
		
		if (CacheCore.getKeyCount() != 3){
			testObs = "While test a SOURCE REFRESH, we had + [" + auxIndex + "] PUT requests. Final Count is [" + CacheCore.getKeyCount() + "]. Should be 3";
			CacheTest.printResult(testName, startDate, testResult, testObs);
			return;
		}
	
		CacheCore.lazyRefresh("A", "1", "");
		
		if (CacheCore.getKeyCount() != 2){
			testObs = "While test a CATEGORY REFRESH,  Final Count is [" + CacheCore.getKeyCount() + "]. Should be 2";
			CacheTest.printResult(testName, startDate, testResult, testObs);
			return;
		}
		
		CacheCore.lazyRefresh("A", "2", "3");
		
		if (CacheCore.getKeyCount() != 1){
			testObs = "While test a INSTANCE REFRESH,  Final Count is [" + CacheCore.getKeyCount() + "]. Should be 1";
			CacheTest.printResult(testName, startDate, testResult, testObs);
			return;
		}
		
		testResult = true;
		testObs = "";
		
		CacheTest.printResult(testName, startDate, testResult, testObs);
		
	}
}
