package com.entel.frw.comp.cache.core.test.cases;

import java.util.Date;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.config.CacheConfig;
import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.exception.ValueIsNullException;
import com.entel.frw.comp.cache.core.test.CacheTest;
import com.entel.frw.comp.cache.core.test.executors.RandomSetter;
import com.entel.frw.comp.cache.monitoring.CacheMonitor;

public class Test_StallExpiration extends CacheTest  {

	public void execute() throws ValueIsNullException  {
		
		// -----------------------------------------
			String testName = "Test_StallExpiration";
			Date startDate = new Date();
			boolean testResult = false;
			String testObs = null;
		// -----------------------------------------
		
		
		testObs = "";
		
		RandomSetter randomSetter = new RandomSetter();
		
		try {
			// Filling the Cache with Random keys
				randomSetter.start();
				randomSetter.join(); // Waiting for the PUTs.
				Thread.sleep(1000); // Giving it time to settle
				if(CacheMonitor.getKeyCount() != CacheConfig.CACHE_MAX_KEYS){ // Should be CacheConfig.CACHE_MAX_KEYS
					testObs = "Invalid Purge. There are [" + CacheMonitor.getKeyCount() + "] keys; should be [" + CacheConfig.CACHE_MAX_KEYS+  "].";
					testResult = false;
					CacheTest.printResult(testName, startDate, testResult, testObs); return;
				}
			
			// Forcing MaxValue
				CacheMonitor.purgeStallKeys(true);
				if(CacheMonitor.getKeyCount() != 0){ // Should be 0
					testObs = "Invalid Purge. There are [" + CacheMonitor.getKeyCount() + "] keys; should be [0].";
					testResult = false;
					CacheTest.printResult(testName, startDate, testResult, testObs); return;
				}

			// Re-Inserting new Keys
				CacheCore.put("1", "2", "3", XmlObject.Factory.newInstance(), 0);
				CacheCore.put("1", "2", "4", XmlObject.Factory.newInstance(), 0);
				CacheCore.put("1", "3", "5", XmlObject.Factory.newInstance(), 0);
				if(CacheMonitor.getKeyCount() != 3){ // Should be 3
					testObs = "Invalid Purge. There are [" + CacheMonitor.getKeyCount() + "] keys; should be [3].";
					testResult = false;
					CacheTest.printResult(testName, startDate, testResult, testObs); return;
				}
			
			// Reading some keys enough times to be ignored on a purge
			// Forcing MaxValue
				CacheCore.put("A", "B", "C1", XmlObject.Factory.newInstance(), 0);
				CacheCore.put("A", "B", "C2", XmlObject.Factory.newInstance(), 0);
				CacheCore.put("A", "B", "C3", XmlObject.Factory.newInstance(), 0);
				CacheCore.put("A", "B", "C4", XmlObject.Factory.newInstance(), 0);
				
				 // Two GETs
				CacheCore.get("A", "B", "C1"); CacheCore.get("A", "B", "C1");
				CacheCore.get("A", "B", "C2"); CacheCore.get("A", "B", "C2"); 
				
				 // One GET 
				CacheCore.get("A", "B", "C3");
				CacheCore.get("A", "B", "C4");
				
				// Checking the re-fill of the Cache, and automatic Purge.
				randomSetter.run();
				randomSetter.join(); // Waiting for the PUTs.
				Thread.sleep(1000); // Giving it time to settle.
				if(CacheMonitor.isMaxCount()){ // Should be false
					testObs = "Invalid Size. Should not be at Max value.";
					testResult = false;
					CacheTest.printResult(testName, startDate, testResult, testObs); return;
				}
				
				// Checking if used Keys are maintained
				CacheMonitor.purgeStallKeys(true); // Getting read of non-used keys that were added after purge.
				if(CacheMonitor.getKeyCount() != 4){// Should be 4
					testObs = "Invalid Purge. There are [" + CacheMonitor.getKeyCount() + "] keys; should be [4].";
					testResult = false;
					CacheTest.printResult(testName, startDate, testResult, testObs); return;
				}
				
				Thread.sleep((long) (1000/CacheConfig.CACHE_STALL_USE_FACTOR)); // waiting enough to lower the UseFactor of one-use keys bellow the threshold.
				CacheMonitor.purgeStallKeys(true);
				if(CacheMonitor.getKeyCount() != 2){ // Should be 2
					testObs = "Invalid Purge. There are [" + CacheMonitor.getKeyCount() + "] keys; should be [2].";
					testResult = false;
					CacheTest.printResult(testName, startDate, testResult, testObs); return;
				}
				
				Thread.sleep((long) (1000/CacheConfig.CACHE_STALL_USE_FACTOR)); // waiting enough to lower the UseFactor of two-use keys bellow the threshold.
				CacheMonitor.purgeStallKeys(true);
				if(CacheMonitor.getKeyCount() != 0){ // Should be 0
					testObs = "Invalid Purge. There are [" + CacheMonitor.getKeyCount() + "] keys; should be [0].";
					testResult = false;
					CacheTest.printResult(testName, startDate, testResult, testObs); return;
				}
			
			testResult = true;
			CacheTest.printResult(testName, startDate, testResult, testObs);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
