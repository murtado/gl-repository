package com.entel.frw.comp.cache.config;

public interface CacheConfig {

	/*
	-------------------------------------------------- 
	ConcurrentHasmap Default Configuration Parameters.
	-------------------------------------------------- 
	*/
	
	// Main ConcurrentHashmap Configuration
	static int 		CACHE_CORE_INITIAL_CAP = 750;
	static float 	CACHE_CORE_LOAD_FACTOR = 0.8f;
	static int 		CACHE_CORE_CONCR_LEVEL = 3;
	
	/*
	-------------------------------------------------- 
	Cache Value Parameters.
	-------------------------------------------------- 
	*/
	static String CACHE_KEY_SEPARATOR = String.valueOf(((char)007));
	static long CACHE_DEFAULT_EXPIRATION = 86400000; // 1 day
	static float CACHE_STALL_USE_FACTOR = 0.1f; // GETs per second
	
	/*
	-------------------------------------------------- 
	Cache Monitor Parameters.
	-------------------------------------------------- 
	*/
	// Cache Monitor Configuration
	static int CACHE_MAX_KEYS = 10000;
	
}
