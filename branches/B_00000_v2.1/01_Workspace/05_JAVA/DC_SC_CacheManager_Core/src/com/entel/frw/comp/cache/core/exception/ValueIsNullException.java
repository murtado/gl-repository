package com.entel.frw.comp.cache.core.exception;

public class ValueIsNullException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValueIsNullException() {}

    public ValueIsNullException(String message)
    {
       super(message);
    }
	
}
