package com.entel.frw.comp.cache.core.test.cases;

import java.util.Date;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.config.CacheConfig;
import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.exception.ValueIsNullException;
import com.entel.frw.comp.cache.core.test.CacheTest;

public class Test_MaxCount extends CacheTest  {

	public void execute() throws ValueIsNullException  {
	
		// -----------------------------------------
			String testName = "test_MaxCount";
			Date startDate = new Date();
			boolean testResult = false;
			String testObs = null;
			int auxIndex = 0;
		// -----------------------------------------
		
		int overMaxValue = CacheConfig.CACHE_MAX_KEYS*2;
		
		for(auxIndex = 0; auxIndex < overMaxValue; auxIndex++){
			CacheCore.put("A", "B", String.valueOf(auxIndex), XmlObject.Factory.newInstance(), 0);
		}
		
		if (CacheCore.getKeyCount() < CacheConfig.CACHE_MAX_KEYS+1){
			testResult = true;
		} else {
			testObs = "We had + [" + auxIndex + "] PUT requests. Final Count is [" + CacheCore.getKeyCount() + "]. Should be less than [" + CacheConfig.CACHE_MAX_KEYS + "]";
		}
		
		testObs = "We had + [" + auxIndex + "] PUT requests. Final Count is [" + CacheCore.getKeyCount() + "].";
		
		CacheTest.printResult(testName, startDate, testResult, testObs);

	}

}
