package com.entel.frw.comp.cache.core.utils;

import com.entel.frw.comp.cache.config.CacheConfig;

public class CacheUtils {

	public static String marshallKey(String source, String category, String instance){
		return source + CacheConfig.CACHE_KEY_SEPARATOR + category + CacheConfig.CACHE_KEY_SEPARATOR + instance;
	}
	
	public static String[] unMarshallKey(String key){
		return key.split(CacheConfig.CACHE_KEY_SEPARATOR);
	}
	
}
