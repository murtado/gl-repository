package com.entel.frw.comp.cache.monitoring;

import com.entel.frw.comp.cache.core.CacheCore;

public class CacheMonitor extends CacheCore{
	
	// This is a Thread made to purge the CacheCore from stall Keys.
	private static CachePurge cacheCleaner;
	
	static{
		cacheCleaner = CachePurge.getInstance();
	}
	
	public static void purgeStallKeys(boolean mustJoin){
		
		cacheCleaner.purge();
		
		if(mustJoin)
			try {
				cacheCleaner.join();
			} catch (InterruptedException e) {
				return;
			}
	}
	
    public static boolean isMaxCount(){
    	
    	if (!(getKeyCount() < CACHE_MAX_KEYS)){
    		purgeStallKeys(false);
    		return true;
    	}
    	return false;
    }
    

}
