package com.entel.frw.comp.cache.monitoring;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import com.entel.frw.comp.cache.config.CacheConfig;
import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.CacheValue;

public class CachePurge extends Thread implements Runnable{
	
	// This is a Thread made to purge the CacheCore from stall Keys.
	private static CachePurge instance;
	
	private static long lastPurgeDate;
	private static int lastPurgeQt;
	private static ArrayList<String> lastPurgeKeys;
	
	@Override
	public void run() {
		purgeStallKeys();
	}
	
	private CachePurge() {
		// Singleton private constructor.
	}
	
	static{
		instance = new CachePurge();
		lastPurgeKeys = new ArrayList<String>();
	}
	
	public static CachePurge getInstance(){
        return instance;
    } 
	
	public void purge(){
		switch ((getState())) {
		case NEW:
			start();
			break;
		case RUNNABLE:
			try {
				join();
			} catch (InterruptedException e) {
			} finally {
				run();
			}
		case TERMINATED:
			run();
		default:
			break;
		}
	}
	
	public static void purgeStallKeys(){
    	
		CacheValue valueAux;
		
		int auxIndex = 0;
		ArrayList<String> auxArray = new ArrayList<String>();
    	
    	for (Map.Entry<String, CacheValue> e : CacheCore.getCacheIterator()) {
    		valueAux = e.getValue();
    		
    		if(valueAux.getStallFactor() < CacheConfig.CACHE_STALL_USE_FACTOR){
    			CacheCore.deleteRecord(e.getKey());
    			auxArray.add(e.getKey());
    			++auxIndex;
    		}
    		
		}
    	
    	if(auxIndex!=0){
    		lastPurgeDate = new Date().getTime();
    		lastPurgeQt = auxIndex;
    		lastPurgeKeys.clear();
    		lastPurgeKeys.addAll(auxArray);
    	}
    	
    }
	
	protected static long getLastPurgeDate(){
		return lastPurgeDate;
	}
	
	protected static int getLastPurgeQt(){
		return lastPurgeQt;
	}
	
	protected static ArrayList<String> getLastPurgeKeys(){
		return lastPurgeKeys;
	}
	
}
