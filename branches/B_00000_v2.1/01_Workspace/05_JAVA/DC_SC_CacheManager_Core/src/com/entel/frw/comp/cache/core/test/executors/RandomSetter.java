package com.entel.frw.comp.cache.core.test.executors;

import java.util.UUID;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.config.CacheConfig;
import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.exception.ValueIsNullException;

public class RandomSetter extends Thread implements CacheConfig{
	
	@Override
	public void run() {
		
		UUID randomSource = null;
		UUID randomCategory = null;
		UUID randomInstance = null;
		XmlObject auxValue = null;
		
		for(int i=0; i < CacheConfig.CACHE_MAX_KEYS; i++){
			randomSource = UUID.randomUUID();
			randomCategory = UUID.randomUUID();
			randomInstance = UUID.randomUUID();
			auxValue = XmlObject.Factory.newInstance();
			
			try {
				CacheCore.put(randomSource.toString(), randomCategory.toString(), randomInstance.toString(), auxValue, 0);
			} catch (ValueIsNullException e) {
				e.printStackTrace();
			}
			
		}
		
	}

}
