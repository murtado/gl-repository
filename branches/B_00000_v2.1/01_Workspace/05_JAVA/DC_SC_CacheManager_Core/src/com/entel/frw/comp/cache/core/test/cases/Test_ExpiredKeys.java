package com.entel.frw.comp.cache.core.test.cases;

import java.util.Date;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.config.CacheConfig;
import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.exception.ValueIsNullException;
import com.entel.frw.comp.cache.core.test.CacheTest;

public class Test_ExpiredKeys extends CacheTest {

	public void execute() throws ValueIsNullException {
	
		// -----------------------------------------
			String testName = "test_ExpiredKeys";
			Date startDate = new Date();
			boolean testResult = false;
			String testObs = null;
			int auxIndex = 0;
		// -----------------------------------------
		
		int overMaxValue = CacheConfig.CACHE_MAX_KEYS*2;
		
		for(auxIndex = 0; auxIndex < overMaxValue; auxIndex++){
			CacheCore.put("A", "B", "C2", XmlObject.Factory.newInstance(), 0); ++auxIndex;
			CacheCore.put("A", "B", "C3", XmlObject.Factory.newInstance(), 400); ++auxIndex;
		}
		
		testObs = "Values expired before due.";
		
		if (
				CacheCore.getKeyCount() == 2
			){
				testResult = false;
				testObs = testObs + " Key count is [" + CacheCore.getKeyCount() + "].";
		}
		
		try {
			Thread.sleep(600);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (
				CacheCore.get("A", "B", "C2") != null &&
				CacheCore.get("A", "B", "C3") == null &&
						CacheCore.getKeyCount() == 1
			){
				testResult = true;
				testObs = "Values expired correctly. Key count is [" + CacheCore.getKeyCount() + "].";
		}
		
		CacheTest.printResult(testName, startDate, testResult, testObs);

	}

}
