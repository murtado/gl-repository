package com.entel.frw.comp.cache.monitoring;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import com.entel.frw.comp.cache.config.CacheConfig;
import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.CacheValue;
import com.entel.frw.comp.cache.core.utils.CacheUtils;

public class CacheReports{

	public static String getLastPurgeKeysAsJSON(){
		String recordArrayJson = "\"purgeRecords\": [ ";
		String[] auxKeyArray = new String[3];
		ArrayList<String> auxArray = CachePurge.getLastPurgeKeys();
		
		for (int i= 0; i < auxArray.size(); i++) {
			
			auxKeyArray = CacheUtils.unMarshallKey(auxArray.get(i));
			
			recordArrayJson = recordArrayJson + "{" + (
				      "\"source\": \"" + auxKeyArray[0] + "\"," + 
				      "\"category\": \"" + auxKeyArray[1] + "\"," + 
				      "\"instance\": \"" + auxKeyArray[2] + "\"},"
				    );

		}
		
		recordArrayJson = recordArrayJson.substring(0,recordArrayJson.length()-1) + "]";
		
		return "{" + getLastPurgeInfo() + ", " + recordArrayJson + "}";
	}
	
	protected static String getLastPurgeInfo(){
		
		return "\"purgeInfo\": {\"lastPurgeDate\": " + 
				CachePurge.getLastPurgeDate() + ", \"lastPurgeQt\": " + 
				CachePurge.getLastPurgeQt() + "}";
	}
	
	public static String getCacheStatusAsJSON(){
		
		String json = "{" + (
			"\"keyCount\": " + CacheCore.getKeyCount() + "," +
			"\"use\": \"" + (Float.valueOf((CacheCore.getKeyCount()))*100)/CacheConfig.CACHE_MAX_KEYS + "%\"," +
			"\"remaining\": " + (CacheConfig.CACHE_MAX_KEYS - CacheCore.getKeyCount()) + "," +
			"\"isFull\": \"" + CacheMonitor.isMaxCount() + "\"," 
		);
		
		json = json + getLastPurgeInfo();

		json = json + "}";
		
		return json;
	}
	
	public static String getCacheKeysAsJSON(){
		return getCacheKeysAsJSON("","","");
	}
	
	public static String getCacheKeysAsJSON(String source, String category, String instance){
		
		String[] keyArrayAux = new String[3];
		String source_Aux;
		String category_Aux;
		String instance_Aux;
		
		int auxCount = 0;
		
		String json = "{";
		String recordArrayJson = "\"records\": [ ";
		
		Iterable<Entry<String, CacheValue>> cacheEntries = CacheCore.getCacheIterator();
		
		/*
		-------------------------------------------------- 
		Printing the whole Cache
		-------------------------------------------------- 
		*/
			if(
					(instance == null || instance.isEmpty()) &&
					(category == null || category.isEmpty()) &&
					(source == null || source.isEmpty()) 
			){
				for (Map.Entry<String, CacheValue> e : cacheEntries) {
				    
					keyArrayAux = CacheUtils.unMarshallKey(e.getKey());
					instance_Aux = keyArrayAux[2];
					category_Aux = keyArrayAux[1];
					source_Aux = keyArrayAux[0];

					recordArrayJson = recordArrayJson + (
							"{\"source\": \"" + source_Aux + "\"," + 
							"\"category\": \"" + category_Aux + "\"," + 
							"\"instance\": \"" + instance_Aux + "\"}," 
							);
					++auxCount;
					
				}
			}
		
		/*
		-------------------------------------------------- 
		Filtering by Source
		-------------------------------------------------- 
		*/
			if(
					(instance == null || instance.isEmpty()) &&
					(category == null || category.isEmpty()) &&
					(source != null && !source.isEmpty()) 
			){
				for (Map.Entry<String, CacheValue> e : cacheEntries) {
				    
					if(e.getKey().contains(source)){
						
						keyArrayAux = CacheUtils.unMarshallKey(e.getKey());
						instance_Aux = keyArrayAux[2];
						category_Aux = keyArrayAux[1];
						source_Aux = keyArrayAux[0];

						recordArrayJson = recordArrayJson + (
								"{\"source\": \"" + source_Aux + "\"," + 
								"\"category\": \"" + category_Aux + "\"," + 
								"\"instance\": \"" + instance_Aux + "\"}," 
								);
						++auxCount;
					}
				    
				}
			}
		
		/*
		-------------------------------------------------- 
		Filtering by Category
		-------------------------------------------------- 
		*/
			if(
					(instance == null || instance.isEmpty()) &&
					(category != null && !category.isEmpty()) &&
					(source != null && !source.isEmpty()) 
			){
				for (Map.Entry<String, CacheValue> e : cacheEntries) {
				    
					if(e.getKey().contains(CacheUtils.marshallKey(source, category, ""))){
						
						keyArrayAux = CacheUtils.unMarshallKey(e.getKey());
						instance_Aux = keyArrayAux[2];
						category_Aux = keyArrayAux[1];
						source_Aux = keyArrayAux[0];

						recordArrayJson = recordArrayJson + (
								"{\"source\": \"" + source_Aux + "\"," + 
								"\"category\": \"" + category_Aux + "\"," + 
								"\"instance\": \"" + instance_Aux + "\"}," 
								);
						++auxCount;
					}
				    
				}
				
			}
		
		/*
		-------------------------------------------------- 
		Filtering by a single Key
		-------------------------------------------------- 
		*/
			if(
					(instance != null && !instance.isEmpty()) &&
					(category != null && !category.isEmpty()) &&
					(source != null && !source.isEmpty())
			){
				if(CacheCore.get(source, category, instance) != null){
	
					recordArrayJson = recordArrayJson + (
							"{\"source\": \"" + source + "\"," + 
							"\"category\": \"" + category + "\"," + 
							"\"instance\": \"" + instance + "\"}," 
							);
					++auxCount;
				} else {
					recordArrayJson = recordArrayJson + ",";
				}
				
			}
	
		recordArrayJson = recordArrayJson.substring(0,recordArrayJson.length()-1) + "]";
		json = json + "\"keyCount\": " + auxCount + "," +  recordArrayJson + "}";
		
		return json;
	}
}
