package com.entel.frw.comp.cache.core.test;

import java.util.Date;

import com.entel.frw.comp.cache.core.CacheCore;
import com.entel.frw.comp.cache.core.exception.ValueIsNullException;
import com.entel.frw.comp.cache.core.test.cases.Test_ExpiredKeys;
import com.entel.frw.comp.cache.core.test.cases.Test_FullRefresh;
import com.entel.frw.comp.cache.core.test.cases.Test_KeyUpdate;
import com.entel.frw.comp.cache.core.test.cases.Test_MaxCount;
import com.entel.frw.comp.cache.core.test.cases.Test_StallExpiration;

public class CacheTest {
	
	@SuppressWarnings("unused")
	private static void test() throws ValueIsNullException {
		new Test_KeyUpdate().execute();
			CacheCore.lazyRefresh("", "", "");
		
		new Test_MaxCount().execute();
			CacheCore.lazyRefresh("", "", "");
		
		new Test_FullRefresh().execute(); 	
			CacheCore.lazyRefresh("", "", "");
			
		new Test_ExpiredKeys().execute(); 	
			CacheCore.lazyRefresh("", "", "");
			
		new Test_StallExpiration().execute(); 	
			CacheCore.lazyRefresh("", "", "");
	}
	
	protected static void printResult(String testName, Date startDate, boolean testResult, String testObs){
		
		String resultPrint = "ERROR";
				
		if (testResult) resultPrint = "OK";
		
		System.out.println(
				"[" + testName  + "] - [" +
				(new Date().getTime() - startDate.getTime()) +
				"] - [" +
				resultPrint +
				"] - [" +
				testObs
				+ "] ");
	}

}
