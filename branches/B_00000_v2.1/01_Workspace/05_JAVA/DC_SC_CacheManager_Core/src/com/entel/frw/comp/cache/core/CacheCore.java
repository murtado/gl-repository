package com.entel.frw.comp.cache.core;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.config.CacheConfig;
import com.entel.frw.comp.cache.core.exception.ValueIsNullException;
import com.entel.frw.comp.cache.core.utils.CacheUtils;
import com.entel.frw.comp.cache.monitoring.CacheMonitor;

public class CacheCore implements CacheConfig{

	private static ConcurrentHashMap<String,CacheValue> cacheCore;

	static{
		initCache();
	}
	
    public static int getKeyCount(){
    	return cacheCore.size();
    }
	
	public static void put(String source, String category, String instance, XmlObject value, long expiration) throws ValueIsNullException{
		
		String key;
		
		/* ---------------------------
			Content Validations
		--------------------------- */
		if (value == null)  // Value cannot be NULL.
			throw new ValueIsNullException("Cache Value cannot be NULL in a PUT operation");
		
		if (source == null || category == null || instance == null) // Key must contain all-three.
			throw new ValueIsNullException("Source, Category nor Instance can be NULL in a PUT operation");
		/* --------------------------- */
		
		key = CacheUtils.marshallKey(source,category,instance); // Creating the Key 
		
		if(cacheCore.containsKey(key)){
			cacheCore.get(key).updateValue(expiration, value);
		}else{
			if(!CacheMonitor.isMaxCount())
				cacheCore.put(key,new CacheValue(expiration, value));
		}
	}

	public static XmlObject get(String source, String category, String instance){
		
		CacheValue cacheValue = null;
		String key = CacheUtils.marshallKey(source,category,instance);
		
		try{

			if(cacheCore.containsKey(key)) {
				cacheValue = (CacheValue) cacheCore.get(key);
			
				if (!(cacheValue.isExpired())){ 
					return cacheValue.getValue(); 
				} else {
					lazyRefresh(source, category, instance);
					return null;
				}
			} else {
				return null;
			}

		}
		catch (Exception e){
			return null;
		}
	}
	
	public static void deleteRecord(String key){
		cacheCore.remove(key);
	}
	
	public static void lazyRefresh(String source, String category, String instance){
		
		/*
		-------------------------------------------------- 
		Clearing a whole Source
		-------------------------------------------------- 
		*/
			if(
					(instance == null || instance.isEmpty()) &&
					(category == null || category.isEmpty()) &&
					(source != null && !source.isEmpty()) 
			){
				for (Map.Entry<String, CacheValue> e : cacheCore.entrySet()) {
				    
					if(e.getKey().contains(source))
						deleteRecord(e.getKey());
				    
				}
				return;
			}
		
		/*
		-------------------------------------------------- 
		Clearing a whole Category
		-------------------------------------------------- 
		*/
			if(
					(instance == null || instance.isEmpty()) &&
					(category != null && !category.isEmpty()) &&
					(source != null && !source.isEmpty()) 
			){
				for (Map.Entry<String, CacheValue> e : cacheCore.entrySet()) {
				    
					if(e.getKey().contains(CacheUtils.marshallKey(source, category, "")))
						deleteRecord(e.getKey());
				    
				}
				return;
			}
		
		/*
		-------------------------------------------------- 
		Clearing a single Key
		-------------------------------------------------- 
		*/
			if(
					(instance != null && !instance.isEmpty()) &&
					(category != null && !category.isEmpty()) &&
					(source != null && !source.isEmpty()) 
			){
				deleteRecord(CacheUtils.marshallKey(source, category, instance));
				return;
			}


		/*
		-------------------------------------------------- 
		Clearing the whole Cache!
		-------------------------------------------------- 
		*/
			clearCache();
	}
	
	public static Iterable<Entry<String,CacheValue>> getCacheIterator(){
		return cacheCore.entrySet();
	}
	
	private static void initCache(){
		
		cacheCore = new ConcurrentHashMap<>(
				CACHE_CORE_INITIAL_CAP, 
				CACHE_CORE_LOAD_FACTOR, 
				CACHE_CORE_CONCR_LEVEL
			);
	}
	
	private static void clearCache(){
		cacheCore.clear();
	}
	
}
