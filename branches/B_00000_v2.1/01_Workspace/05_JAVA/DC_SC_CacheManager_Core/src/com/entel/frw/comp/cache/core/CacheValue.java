package com.entel.frw.comp.cache.core;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.config.CacheConfig;

public class CacheValue implements CacheConfig{

	private long created;
	private long expiration;
	private long lastStallEvaluation;
	
	private XmlObject value;
	
	private AtomicInteger useCount;
	
	public CacheValue(long expiration, XmlObject value) {
		created = new Date().getTime();
		useCount = new AtomicInteger(0);
		lastStallEvaluation = 0;
		
		if(expiration > 0){
			this.expiration = expiration;
		} else {
			this.expiration = CACHE_DEFAULT_EXPIRATION;
		}
		
		this.value = value;
	}

	protected boolean isExpired(){
		return (expiration != 0 && (((new Date()).getTime() - created) >= expiration));
	}

	protected void expire(){
		expiration = 1;
	}
	
	public XmlObject getValue() {
		useCount.incrementAndGet();
		return value;
	}

	public void setValue(XmlObject value) {
		this.value = value;
	}
	
	public void updateValue(long expiration, XmlObject value) {
		created = new Date().getTime();
		this.value = value;
		this.expiration = expiration;
	}
	
	public int getUseCount(){
		return useCount.get();
	}
	
	public float getStallFactor(){
		lastStallEvaluation = new Date().getTime() ;
		return (useCount.get()/(Float.valueOf((lastStallEvaluation - created))/1000));
	}
}
