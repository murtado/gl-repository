create or replace PACKAGE BODY ESB_ERROR_MANAGER_PKG AS

  /*---------------------------------------------------------------------------------------------------
    DEPRECATED as it belongs to COMMONS PKG

    Fills the p_SOURCE_ERROR_CODE and p_SOURCE_ERROR_DESC variable sused thorughout the Package, by the
    use of the following parameters :

    p_Locator   -->  Las known position within a Procedure or Function
    p_Reason    -->  Descriptive information about an Error.
    p_ErrCode   -->  Unique code of an Error, within the context of this package.
    p_SqlErr    -->  SQLERR Information, as it exists within the error context
                                from which this procedure is called.

  ---------------------------------------------------------------------------------------------------
  PROCEDURE LOAD_SOURCE_ERROR(
      p_Locator IN VARCHAR,
      p_ErrCode IN VARCHAR,
      p_Reason IN VARCHAR,
      p_SqlErr IN VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    IS
      BEGIN

            p_SOURCE_ERROR_CODE := p_ErrCode;
            p_SOURCE_ERROR_DESC :=
              p_Reason
              || ' @ ['
              || p_Locator
              || '] --- Error ['
              || p_SqlErr
              || ']';

  END LOAD_SOURCE_ERROR;
*/
  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_DEF_RESULT_S_ID_OK(
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  )
  RETURN NUMBER
  IS

     v_STATUS_ID NUMBER;

    BEGIN

      p_SOURCE_ERROR_DESC := 'The Result status was successfully recovered.';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - GET_DEF_RESULT_S_ID_OK';

      v_STATUS_ID := GET_RESULT_STATUS_ID(
                          'OK',
                          p_Locator,
                          p_SOURCE_ERROR_CODE,
                          p_SOURCE_ERROR_DESC
                      );

    EXCEPTION
        WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );
              RETURN 0;

  END GET_DEF_RESULT_S_ID_OK;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_DEF_RESULT_S_ID_ERROR(
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  )
  RETURN NUMBER
  IS

     v_STATUS_ID NUMBER;

    BEGIN

      p_SOURCE_ERROR_DESC := 'The Result status was successfully recovered.';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - GET_DEF_RESULT_S_ID_ERROR';

      v_STATUS_ID := GET_RESULT_STATUS_ID(
                          'ERROR',
                          p_Locator,
                          p_SOURCE_ERROR_CODE,
                          p_SOURCE_ERROR_DESC
                      );
      IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN 0; END IF;

      RETURN v_STATUS_ID;

    EXCEPTION
        WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );
              RETURN 0;

  END GET_DEF_RESULT_S_ID_ERROR;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_RESULT_STATUS_ID(
    p_STATUS IN VARCHAR2,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  )
  RETURN NUMBER
  IS

      v_STATUS_ID NUMBER;
      v_RCD_STATUS NUMBER;
      v_STATUS VARCHAR(50);

    BEGIN

      v_STATUS := UPPER(p_STATUS);

      p_SOURCE_ERROR_DESC := 'The Result status was successfully recovered.';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - GET_RESULT_STATUS_ID';

      SELECT
        ID,
        RCD_STATUS
          INTO
            v_STATUS_ID,
            v_RCD_STATUS
      FROM
        ESB_ERROR_STATUS_TYPE
      WHERE
        NAME = v_STATUS;

      IF v_RCD_STATUS = 2 THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-4',
          'Logically deleted record with this info : [Error Status]=' || v_STATUS || '.',
          '',
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
      END IF;

      RETURN v_STATUS_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no Result status with this info : [Error Status]=' || v_STATUS || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'The Result has duplicate records matching its attributes : [Error Status]='|| v_STATUS || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;

  END GET_RESULT_STATUS_ID;

  /*---------------------------------------------------------------------------------------------------

    Returnes the Canonical Error Type from the ESB_CANONICAL_ERROR_TYPE, based on the
    Canopnical Error Type Code.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CAN_ERR_TYPE_ID
    (
      p_CAN_ERR_TYPE IN VARCHAR,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER

    IS

      v_CAN_ERR_TYPE NUMBER;
      v_RCD_STATUS NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The Canonical Error Type was successfully recovered.';
        p_SOURCE_ERROR_CODE := '0';

        p_Locator := 'INIT - GET_CAN_ERR_TYPE_ID';

        SELECT
          CANERRT.ID,
          CANERRT.RCD_STATUS
            INTO
              v_CAN_ERR_TYPE,
              v_RCD_STATUS
        FROM
          ESB_CANONICAL_ERROR_TYPE CANERRT
        WHERE
          CANERRT.TYPE = p_CAN_ERR_TYPE;

        IF v_RCD_STATUS = 2 THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : ' || p_CAN_ERR_TYPE || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;

        RETURN v_CAN_ERR_TYPE;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no Canonical Error Type with this info : ' || p_CAN_ERR_TYPE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'The Canonical Error Type has duplicate records matching its attributes : '|| p_CAN_ERR_TYPE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END GET_CAN_ERR_TYPE_ID;

  /*---------------------------------------------------------------------------------------------------

    Returns the Canonical Error that depicts a generic "NO DATA FOUND" scenario.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_DEF_CAN_ERR_ID_NO_DATA
    (
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER

    IS

      v_CAN_ERR_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The Canonical Error was successfully recovered.';
        p_SOURCE_ERROR_CODE := '0';

        p_Locator := 'INIT - GET_DEFAULT_CAN_ERR_ID_NO_DATA';

        v_CAN_ERR_ID := GET_CAN_ERR_ID(
                            '50006',
                            'FWCF',
                            p_Locator,
                            p_SOURCE_ERROR_CODE,
                            p_SOURCE_ERROR_DESC
                        );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN 0; END IF;

        RETURN v_CAN_ERR_ID;

        EXCEPTION
            WHEN OTHERS THEN
              ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                  p_Locator,
                  '-3',
                  'Unexpected Error.',
                  SQLERRM,
                  p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
                );
                RETURN 0;
  END GET_DEF_CAN_ERR_ID_NO_DATA;

  /*---------------------------------------------------------------------------------------------------

    Returnes the Canonical Error from the ESB_CANONICAL_ERROR, based on the Canonical Error Code and
    Canonical Error Type.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CAN_ERR_ID
    (
      p_CAN_ERR_CODE IN VARCHAR,
      p_CAN_ERR_TYPE IN VARCHAR,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER

    IS

      v_CAN_ERR_ID NUMBER;
      v_RCD_STATUS NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The Canonical Error was successfully recovered.';
        p_SOURCE_ERROR_CODE := '0';

        p_Locator := 'INIT - GET_CAN_ERR_ID';

        SELECT
            CANERR.ID,
            CANERR.RCD_STATUS
            INTO
              v_CAN_ERR_ID,
              v_RCD_STATUS
          FROM
            ESB_CANONICAL_ERROR CANERR
              INNER JOIN
                ESB_CANONICAL_ERROR_TYPE CANERRT ON CANERRT.ID = CANERR.TYPE_ID
          WHERE
            CANERR.CODE   = p_CAN_ERR_CODE AND
            CANERRT.TYPE  = p_CAN_ERR_TYPE;

          IF v_RCD_STATUS = 2 THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-4',
              'Logically deleted record with this info : ' || p_CAN_ERR_CODE || ' - ' || p_CAN_ERR_TYPE || '.',
              '',
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
          END IF;

          RETURN v_CAN_ERR_ID;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-1',
              'There is no Canonical Error with this info : ' || p_CAN_ERR_CODE || ' - ' || p_CAN_ERR_TYPE || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'The Canonical Error has duplicate records matching its attributes : ' || p_CAN_ERR_CODE || ' - ' || p_CAN_ERR_TYPE || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );
              RETURN 0;
  END GET_CAN_ERR_ID;

  /*---------------------------------------------------------------------------------------------------

    Returnes the CMP_STEP from the ESB_CMP_CFG, based on the Component Name and
    Component Step Name.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_CMP_STEP_ID
    (
      p_CMP_NAME IN VARCHAR,
      p_CMP_STEP IN VARCHAR,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER
    IS

      v_STEP_ID NUMBER;
      v_RCD_STATUS NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The Component Step was successfully recovered.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'INIT - GET_CMP_STEP_ID';

        SELECT
          CCFG.ID,
          CCFG.RCD_STATUS
            INTO
              v_STEP_ID,
              v_RCD_STATUS
        FROM
          ESB_ERR_CMP_CFG CCFG
        WHERE
          CCFG.CMP_STEP = p_CMP_STEP AND
          CCFG.CMP = (SELECT ID FROM ESB_ERR_CMP WHERE CMP_NAME = p_CMP_NAME AND RCD_STATUS <> 2);

        IF v_RCD_STATUS = 2 THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : ' || p_CMP_NAME || ' - ' || p_CMP_STEP || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;

      RETURN v_STEP_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no step with this information : ' || p_CMP_NAME || ' - ' || p_CMP_STEP || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'The Step has duplicate records matching its attributes : ' || p_CMP_NAME || ' - ' || p_CMP_STEP || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END GET_CMP_STEP_ID;

  /*---------------------------------------------------------------------------------------------------

    Returnes the Diagnostic Action from the ESB_ERR_ACITON, based on the
    Action Code.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_DIAG_ACTION_ID
    (
      p_DIAG_ACT_CODE IN VARCHAR,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER

    IS
      p_DIAG_ACT_ID NUMBER;
      v_RCD_STATUS NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The Action was successfully recovered.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'INIT - GET_DIAG_ACTION_ID';

        SELECT
          EACT.ID,
          EACT.RCD_STATUS
            INTO
              p_DIAG_ACT_ID,
              v_RCD_STATUS
        FROM
          ESB_ERR_ACTION EACT
        WHERE
          EACT.CODE = p_DIAG_ACT_CODE;

        IF v_RCD_STATUS = 2 THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Logically deleted record with this info : ' || p_DIAG_ACT_CODE || '.',
            '',
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        END IF;

        RETURN p_DIAG_ACT_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no Diagnostic Action with this info : ' || p_DIAG_ACT_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'The Diagnostic Action has duplicate records matching its attributes : '|| p_DIAG_ACT_CODE || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END GET_DIAG_ACTION_ID;

  /*---------------------------------------------------------------------------------------------------

    Creates a new Diagnostic Action Configuration.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    DUP VAL ON INDEX   --> p_SOURCE_ERROR_CODE = -4
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_DIAG_ACTION_CFG
    (
      p_ACT_CFG IN ERR_ACT_CFG_T,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER

    IS

      p_DIAG_ACT_CFG_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The Action Config was successfully created.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'INIT - CREATE_DIAG_ACTION_CFG';

        INSERT INTO ESB_ERR_ACTION_CFG
          (
            ID,
            RT_MAX,
            RT_TTD
          )
        VALUES
          (
            ESB_ERR_ACTION_CFG_SEQ.NEXTVAL,
            p_ACT_CFG.RT_MAX,
            p_ACT_CFG.RT_TTD
          )
        RETURNING ID INTO p_DIAG_ACT_CFG_ID;

        RETURN p_DIAG_ACT_CFG_ID;

      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-4',
            'Duplicate Record with data :' || p_ACT_CFG.RT_MAX || ' - ' || p_ACT_CFG.RT_TTD || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END CREATE_DIAG_ACTION_CFG;

  FUNCTION CREATE_COMPONENT(
    p_ERR_CMP IN ERR_CMP_T,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  )
  RETURN NUMBER
  IS

    v_CMP_ID NUMBER;

    BEGIN

      p_SOURCE_ERROR_DESC := 'The Component was successfully Created.';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - CREATE_COMPONENT';

      INSERT INTO
        ESB_ERR_CMP (ID, CMP_NAME, DESCRIPTION, RCD_STATUS)
          VALUES
            (
              ESB_ERR_CMP_SEQ.NEXTVAL,
              p_ERR_CMP.CMP_NAME,
              p_ERR_CMP.CMP_DESC,
              1
            )
          RETURNING ID INTO v_CMP_ID;

      RETURN v_CMP_ID;

    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '96',
            'Duplicate Record with data :' || p_ERR_CMP.CMP_NAME || ' - ' || p_ERR_CMP.CMP_DESC || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
      WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '99',
              'Unexpected Error while creating a Component.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            ROLLBACK;
            RETURN NULL;
  END CREATE_COMPONENT;

  FUNCTION CREATE_COMPONENT_CFG(
    p_ERR_CMP_CFG IN ERR_CMP_CFG_T,
    p_CMP_ID IN NUMBER,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  )
  RETURN NUMBER
  IS
    v_CMP_CFG_ID NUMBER;

    BEGIN

      p_SOURCE_ERROR_DESC := 'The Component Config was successfully Created.';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - CREATE_COMPONENT_CFG';

      INSERT INTO
        ESB_ERR_CMP_CFG (ID, CMP, CMP_STEP, DESCRIPTION, RCD_STATUS)
          VALUES
            (
              ESB_ERR_CMP_CFG_SEQ.NEXTVAL,
              p_CMP_ID,
              p_ERR_CMP_CFG.CMP_STEP,
              p_ERR_CMP_CFG.CMP_STEP_DESC,
              1
            )
          RETURNING ID INTO v_CMP_CFG_ID;

      RETURN v_CMP_CFG_ID;

    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '96',
            'Duplicate Record with data :' || p_CMP_ID || ' - ' || p_ERR_CMP_CFG.CMP.CMP_NAME || ' - ' || p_ERR_CMP_CFG.CMP.CMP_DESC || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
      WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '99',
              'Unexpected Error while creating a Component.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            ROLLBACK;
            RETURN NULL;
  END CREATE_COMPONENT_CFG;

  FUNCTION CREATE_COMPONENT_CFG(
    p_ERR_CMP_CFG IN ERR_CMP_CFG_T,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  )
  RETURN NUMBER
  IS

    v_CMP_ID NUMBER;
    v_CMP_CFG_ID NUMBER;

    BEGIN

      p_SOURCE_ERROR_DESC := 'The Component Config was successfully Created.';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - CREATE_COMPONENT_CFG';

      SELECT ID INTO v_CMP_ID FROM ESB_ERR_CMP WHERE CMP_NAME = p_ERR_CMP_CFG.CMP.CMP_NAME;

      INSERT INTO
        ESB_ERR_CMP_CFG (ID, CMP, CMP_STEP, DESCRIPTION, RCD_STATUS)
          VALUES
            (
              ESB_ERR_CMP_CFG_SEQ.NEXTVAL,
              v_CMP_ID,
              p_ERR_CMP_CFG.CMP_STEP,
              p_ERR_CMP_CFG.CMP_STEP_DESC,
              1
            )
          RETURNING ID INTO v_CMP_CFG_ID;

      RETURN v_CMP_CFG_ID;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no Component Action with this info :' || p_ERR_CMP_CFG.CMP.CMP_NAME || ' - ' || p_ERR_CMP_CFG.CMP.CMP_DESC || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
      WHEN DUP_VAL_ON_INDEX THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '96',
            'Duplicate Record with data :' || p_ERR_CMP_CFG.CMP.CMP_NAME || ' - ' || p_ERR_CMP_CFG.CMP.CMP_DESC || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
      WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '99',
              'Unexpected Error while creating a Component.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            ROLLBACK;
            RETURN NULL;
  END CREATE_COMPONENT_CFG;

  FUNCTION CREATE_DIAGNOSTIC(
    p_ERR_DIAG IN ERR_DIAG_T,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_CODE OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR
  )
  RETURN NUMBER
  IS

    v_DIAG_ID NUMBER;

    v_STEP_ID NUMBER;
    v_CAN_ERR_ID NUMBER;
    v_CAN_ERR_TYPE_ID NUMBER;
    v_DIAG_ACTION_ID NUMBER;
    v_DIAG_ACTION_CFG_ID NUMBER;

    v_ERR_SEL_WEIGHT_AUX NUMBER;

    BEGIN

      v_DIAG_ID               := NULL;
      v_STEP_ID               := NULL;
      v_CAN_ERR_ID            := NULL;
      v_CAN_ERR_TYPE_ID       := NULL;
      v_DIAG_ACTION_ID        := NULL;
      v_DIAG_ACTION_CFG_ID    := NULL;

      v_ERR_SEL_WEIGHT_AUX    := 0;

      p_SOURCE_ERROR_DESC := 'The Diagnostic was successfully Created.';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - CREATE_DIAGNOSTIC';

      /*
        -------------------------------------------------------------------------------------
          Getting the Component Step on the ESB_ERR_CMP_CFG
        -------------------------------------------------------------------------------------
      */
      p_Locator := 'EXEC - GET_CMP_STEP_ID';

      v_STEP_ID := GET_CMP_STEP_ID(
                    P_ERR_DIAG.CMP_NAME,
                    P_ERR_DIAG.CMP_STEP,
                    p_Locator,
                    p_SOURCE_ERROR_CODE,
                    p_SOURCE_ERROR_DESC
                    );

      CASE p_SOURCE_ERROR_CODE
        WHEN '0' -- OK
          THEN NULL;

        WHEN '-1' -- No data
          THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

        WHEN '-2' -- Too Many
          THEN p_SOURCE_ERROR_CODE:='97'; RETURN NULL;

        WHEN '-3' -- Unexpected Error
          THEN p_SOURCE_ERROR_CODE:='99'; RETURN NULL;

        WHEN '-4' -- Logically Deleted
          THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

        ELSE
          p_SOURCE_ERROR_CODE:='99'; RETURN NULL;
      END CASE;


      /*
        -------------------------------------------------------------------------------------
          Getting the Canonical Error on the ESB_CANONICAL_ERROR
        -------------------------------------------------------------------------------------
      */
      p_Locator := 'EXEC - GET_CAN_ERR_ID';

      IF (
        (P_ERR_DIAG.ERR_SEL_SINGLE_CODE IS NOT NULL)
        AND
        (P_ERR_DIAG.ERR_SEL_SINGLE_TYPE IS NOT NULL)

          ) THEN


        v_CAN_ERR_ID := GET_CAN_ERR_ID(
                          P_ERR_DIAG.ERR_SEL_SINGLE_CODE,
                          P_ERR_DIAG.ERR_SEL_SINGLE_TYPE,
                          p_Locator,
                          p_SOURCE_ERROR_CODE,
                          p_SOURCE_ERROR_DESC
                          );

        CASE p_SOURCE_ERROR_CODE
          WHEN '0' -- OK
            THEN NULL;

          WHEN '-1' -- No data
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

          WHEN '-2' -- Too Many
            THEN p_SOURCE_ERROR_CODE:='97'; RETURN NULL;

          WHEN '-3' -- Unexpected Error
            THEN p_SOURCE_ERROR_CODE:='99'; RETURN NULL;

          WHEN '-4' -- Logically Deleted
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

          ELSE
            p_SOURCE_ERROR_CODE:='99'; RETURN NULL;
        END CASE;

      END IF;

      /*
        -------------------------------------------------------------------------------------
          Getting the Canonical Error Type on the ESB_CANONICAL_ERROR_TYPE
        -------------------------------------------------------------------------------------
      */
      p_Locator := 'EXEC - GET_CAN_ERR_TYPE_ID';

      IF ( P_ERR_DIAG.ERR_SEL_TYPE IS NOT NULL) THEN

        v_CAN_ERR_TYPE_ID := GET_CAN_ERR_TYPE_ID(
                              P_ERR_DIAG.ERR_SEL_TYPE,
                              p_Locator,
                              p_SOURCE_ERROR_CODE,
                              p_SOURCE_ERROR_DESC
                              );

        CASE p_SOURCE_ERROR_CODE
          WHEN '0' -- OK
            THEN NULL;

          WHEN '-1' -- No data
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

          WHEN '-2' -- Too Many
            THEN p_SOURCE_ERROR_CODE:='97'; RETURN NULL;

          WHEN '-3' -- Unexpected Error
            THEN p_SOURCE_ERROR_CODE:='99'; RETURN NULL;

          WHEN '-4' -- Logically Deleted
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

          ELSE
            p_SOURCE_ERROR_CODE:='99'; RETURN NULL;
        END CASE;

      END IF;

      /*
        -------------------------------------------------------------------------------------
          Getting the Diagnostic action on the ESB_ERR_ACTION
        -------------------------------------------------------------------------------------
      */
      p_Locator := 'EXEC - GET_DIAG_ACTION_ID';

      v_DIAG_ACTION_ID := GET_DIAG_ACTION_ID(
                            P_ERR_DIAG.ACTION,
                            p_Locator,
                            p_SOURCE_ERROR_CODE,
                            p_SOURCE_ERROR_DESC
                            );

      CASE p_SOURCE_ERROR_CODE
        WHEN '0' -- OK
          THEN NULL;

        WHEN '-1' -- No data
          THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

        WHEN '-2' -- Too Many
          THEN p_SOURCE_ERROR_CODE:='97'; RETURN NULL;

        WHEN '-3' -- Unexpected Error
          THEN p_SOURCE_ERROR_CODE:='99'; RETURN NULL;

        WHEN '-4' -- Logically Deleted
          THEN p_SOURCE_ERROR_CODE:='98'; RETURN NULL;

        ELSE
          p_SOURCE_ERROR_CODE:='99'; RETURN NULL;
      END CASE;

      /*
        -------------------------------------------------------------------------------------
          Creating the Diagnostic Action CFG on the ESB_ERR_ACTION_CFG
        -------------------------------------------------------------------------------------
      */
      p_Locator := 'EXEC - CREATE_DIAG_ACTION_CFG';
      IF (P_ERR_DIAG.ACTION_CFG IS NOT NULL) THEN

        v_DIAG_ACTION_CFG_ID := CREATE_DIAG_ACTION_CFG(
                              P_ERR_DIAG.ACTION_CFG,
                              p_Locator,
                              p_SOURCE_ERROR_CODE,
                              p_SOURCE_ERROR_DESC
                              );

        CASE p_SOURCE_ERROR_CODE
          WHEN '0' -- OK
            THEN NULL;

          WHEN '-1' -- No data
            THEN p_SOURCE_ERROR_CODE:='98'; ROLLBACK; RETURN NULL;

          WHEN '-2' -- Too Many
            THEN p_SOURCE_ERROR_CODE:='97'; ROLLBACK; RETURN NULL;

          WHEN '-3' -- Unexpected Error
            THEN p_SOURCE_ERROR_CODE:='99'; ROLLBACK; RETURN NULL;

          WHEN '-4' -- Duplicate
            THEN p_SOURCE_ERROR_CODE:='96'; ROLLBACK; RETURN NULL;

          ELSE
            p_SOURCE_ERROR_CODE:='99'; RETURN NULL;
        END CASE;

      END IF;

      /*
        -------------------------------------------------------------------------------------
          Creating the Diagnostic on the ESB_ERR_DIAGNOSTICS
        -------------------------------------------------------------------------------------
      */
      p_Locator := 'Creating the Diagnostic.';

      IF(p_ERR_DIAG.ERR_SEL_WEIGHT IS NOT NULL) THEN v_ERR_SEL_WEIGHT_AUX := p_ERR_DIAG.ERR_SEL_WEIGHT; END IF;

      INSERT INTO ESB_ERR_DIAGNOSTICS
        (
          ID,
          CMP_CFG,
          ERR_SEL_SINGLE,
          ERR_SEL_FROM,
          ERR_SEL_TO,
          ERR_SEL_TYPE,
          ERR_SEL_WEIGHT,
          ACTION,
          ACTION_CFG
        )
      VALUES
        (
          ESB_ERR_DIAGNOSTICS_SEQ.NEXTVAL,
          v_STEP_ID,
          v_CAN_ERR_ID,
          P_ERR_DIAG.ERR_SEL_FROM,
          P_ERR_DIAG.ERR_SEL_TO,
          v_CAN_ERR_TYPE_ID,
          v_ERR_SEL_WEIGHT_AUX,
          v_DIAG_ACTION_ID,
          v_DIAG_ACTION_CFG_ID
        )
        RETURNING ID INTO v_DIAG_ID;

        RETURN v_DIAG_ID;

    EXCEPTION
      WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '99',
              'Unexpected Error while creating a Diagnosis.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            ROLLBACK;
            RETURN NULL;
  END CREATE_DIAGNOSTIC;

  PROCEDURE GET_ACTION_CONFIG(
    p_ERR_ACTION_CFG_ID IN NUMBER,
    p_ERR_ACTION_CFG OUT SYS_REFCURSOR,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  )
  AS
  BEGIN

    p_SOURCE_ERROR_DESC := 'The Action Config was successfully recovered.';
    p_SOURCE_ERROR_CODE := '0';

    p_Locator := 'INIT - GET_ACTION_CONFIG';

    /*
    ------------------------------------------------
        Getting Action Configuration
    ------------------------------------------------
    */
    OPEN p_ERR_ACTION_CFG FOR
      SELECT
        --------------------------------------------------------------------------------------------------------------------
        -- This should always have every single column on ESB_ERR_ACTION_CFG except for ID, RCD_STATUS and CREATIONDATE. --
        --------------------------------------------------------------------------------------------------------------------
        ERRAC.RT_MAX,
        ERRAC.RT_TTD
      FROM
        ESB_ERR_ACTION_CFG ERRAC
      WHERE
        ERRAC.ID = p_ERR_ACTION_CFG_ID;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected Error while getting an Action Config.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );

  END GET_ACTION_CONFIG;


  PROCEDURE GET_DIAGNOSIS_ACTION_CFG_INDIV(
    p_CMP_CFG_ID IN NUMBER,
    p_CAN_ERR_ID IN NUMBER,
    p_ERR_ACTION OUT VARCHAR,
    p_ERR_ACTION_CFG OUT SYS_REFCURSOR,
    p_ERR_DIAG OUT NUMBER,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  )
  AS

    v_ERR_ACTION_CFG_ID ESB_ERR_ACTION_CFG.ID%TYPE;

  BEGIN

    p_SOURCE_ERROR_DESC := 'The Diagnostic was successfully recovered.';
    p_SOURCE_ERROR_CODE := '0';

    p_Locator := 'INIT - GET_DIAGNOSIS_ACTION_CFG_INDIV';

    SELECT
      (SELECT CODE FROM ESB_ERR_ACTION WHERE ID = ERRD.ACTION),
      ERRD.ACTION_CFG,
      ERRD.ID
      INTO
        p_ERR_ACTION,
        v_ERR_ACTION_CFG_ID,
        p_ERR_DIAG
    FROM
      ESB_ERR_DIAGNOSTICS ERRD
    WHERE
      ERRD.CMP_CFG          = p_CMP_CFG_ID AND
      ERRD.ERR_SEL_SINGLE   = p_CAN_ERR_ID;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-1',
          'No Diagnosis was found.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected Error while getting a Diagnosis.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END GET_DIAGNOSIS_ACTION_CFG_INDIV;

  PROCEDURE GET_DIAGNOSIS_ACTION_CFG_FULL(
    p_CMP_CFG_ID IN NUMBER,
    p_ERR_CODE IN NUMBER,
    p_ERR_TYPE IN VARCHAR,
    p_ERR_ACTION_CFG_ID OUT NUMBER,
    p_ERR_DIAG OUT NUMBER,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  )
  AS

  BEGIN

    p_SOURCE_ERROR_DESC := 'The Diagnostic was successfully recovered.';
    p_SOURCE_ERROR_CODE := '0';

    p_Locator := 'INIT - GET_DIAGNOSIS_ACTION_CFG_FULL';

    SELECT
      ACFG,
      DIAG
      INTO
        p_ERR_ACTION_CFG_ID,
        p_ERR_DIAG
    FROM(
      SELECT
        ERRD.ACTION_CFG ACFG,
        ERRD.ID DIAG
      FROM
        ESB_ERR_DIAGNOSTICS ERRD
      WHERE
        ERRD.CMP_CFG        = p_CMP_CFG_ID AND
        ERRD.ERR_SEL_SINGLE IS NULL        AND
        ERRD.ERR_SEL_TYPE   = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = p_ERR_TYPE) AND
        (ERRD.ERR_SEL_FROM IS NOT NULL AND ERRD.ERR_SEL_TO IS NOT NULL) AND
        p_ERR_CODE BETWEEN
          ERRD.ERR_SEL_FROM  AND ERRD.ERR_SEL_TO
      ORDER BY ERRD.ERR_SEL_WEIGHT DESC
    )
    WHERE
      ROWNUM = 1;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-1',
          'No Diagnosis was found.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected Error while getting a Diagnosis.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END GET_DIAGNOSIS_ACTION_CFG_FULL;

  PROCEDURE GET_DIAGNOSIS_ACTION_CFG_CODE(
    p_CMP_CFG_ID IN NUMBER,
    p_ERR_CODE IN NUMBER,
    p_ERR_ACTION_CFG_ID OUT NUMBER,
    p_ERR_DIAG OUT NUMBER,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  )
  AS

  BEGIN

    p_SOURCE_ERROR_DESC := 'The Diagnostic was successfully recovered.';
    p_SOURCE_ERROR_CODE := '0';

    p_Locator := 'INIT - GET_DIAGNOSIS_ACTION_CFG_CODE';

SELECT
      ACFG,
      DIAG
      INTO
        p_ERR_ACTION_CFG_ID,
        p_ERR_DIAG
    FROM(
      SELECT
        ERRD.ACTION_CFG ACFG,
        ERRD.ID DIAG
      FROM
        ESB_ERR_DIAGNOSTICS ERRD
      WHERE
        ERRD.CMP_CFG        = p_CMP_CFG_ID AND
        ERRD.ERR_SEL_SINGLE IS NULL        AND
        ERRD.ERR_SEL_TYPE   IS NULL        AND
        (ERRD.ERR_SEL_FROM IS NOT NULL AND ERRD.ERR_SEL_TO IS NOT NULL) AND
        p_ERR_CODE BETWEEN
          ERRD.ERR_SEL_FROM  AND ERRD.ERR_SEL_TO
      ORDER BY ERRD.ERR_SEL_WEIGHT DESC
    )
    WHERE
      ROWNUM = 1;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-1',
          'No Diagnosis was found.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected Error while getting a Diagnosis.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END GET_DIAGNOSIS_ACTION_CFG_CODE;

  PROCEDURE GET_DIAGNOSIS_ACTION_CFG_TYPE(
    p_CMP_CFG_ID IN NUMBER,
    p_ERR_TYPE IN VARCHAR,
    p_ERR_ACTION_CFG_ID OUT NUMBER,
    p_ERR_DIAG OUT NUMBER,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  )
  AS

  BEGIN

    p_SOURCE_ERROR_DESC := 'The Diagnostic was successfully recovered.';
    p_SOURCE_ERROR_CODE := '0';

    p_Locator := 'INIT - GET_DIAGNOSIS_ACTION_CFG_TYPE';

    SELECT
      ACFG,
      DIAG
      INTO
        p_ERR_ACTION_CFG_ID,
        p_ERR_DIAG
    FROM(
      SELECT
        ERRD.ACTION_CFG ACFG,
        ERRD.ID DIAG
      FROM
        ESB_ERR_DIAGNOSTICS ERRD
      WHERE
        ERRD.CMP_CFG        = p_CMP_CFG_ID AND
        ERRD.ERR_SEL_SINGLE IS NULL        AND
        ERRD.ERR_SEL_TYPE   = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = p_ERR_TYPE) AND
        (ERRD.ERR_SEL_FROM IS NULL OR ERRD.ERR_SEL_TO IS NULL)
      ORDER BY ERRD.ERR_SEL_WEIGHT DESC
    )
    WHERE
      ROWNUM = 1;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-1',
          'No Diagnosis was found.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected Error while getting a Diagnosis.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END GET_DIAGNOSIS_ACTION_CFG_TYPE;

  PROCEDURE GET_DIAGNOSIS_ACTION_CFG_NULL(
    p_CMP_CFG_ID IN NUMBER,
    p_ERR_ACTION_CFG_ID OUT NUMBER,
    p_ERR_DIAG OUT NUMBER,
    p_Locator OUT VARCHAR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  )
  AS

  BEGIN

    p_SOURCE_ERROR_DESC := 'The Diagnostic was successfully recovered.';
    p_SOURCE_ERROR_CODE := '0';

    p_Locator := 'INIT - GET_DIAGNOSIS_ACTION_CFG_NULL';

  SELECT
      ACFG,
      DIAG
      INTO
        p_ERR_ACTION_CFG_ID,
        p_ERR_DIAG
    FROM(
      SELECT
        ERRD.ACTION_CFG ACFG,
        ERRD.ID DIAG
      FROM
        ESB_ERR_DIAGNOSTICS ERRD
      WHERE
        ERRD.CMP_CFG        = p_CMP_CFG_ID AND
        ERRD.ERR_SEL_SINGLE IS NULL        AND
        ERRD.ERR_SEL_TYPE   IS NULL        AND
        (ERRD.ERR_SEL_FROM IS NULL OR ERRD.ERR_SEL_TO IS NULL)
      ORDER BY ERRD.ERR_SEL_WEIGHT DESC
    )
    WHERE
      ROWNUM = 1;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-1',
          'No Diagnosis was found.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected Error while getting a Diagnosis.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END GET_DIAGNOSIS_ACTION_CFG_NULL;

  /*---------------------------------------------------------------------------------------------------

    Returns the configured diagnosis for an error occurring at an specific step within a component,
    as the combination of an Action and a Configuration associated to it.

    The Diagnosis is looked for within the table ESB_ERR_DIAGNOSTICS.

    It has two posible search patterns, based on an implicit priority :

      1 - If there is a diagnosis specifically associated with the Component Step and the
          Canonical Error sent as paremeter, it will preffer it regardless of other
          diagnosis associated with that same Component Step.

      2 - If no such combination of data was found, a diagnosis is looked for by the
          Error Selector parameters. These are the Canonical Error Code Range and Canonical Error Type.

          The order and priority on which the search pattern was defined about, is as following :

            2a - First it will look for diagnosis that have are both associated with the
                Canonical Error Code Range and the Canonical Error Type.

            2b - Then, it will then look for diagnosis associated with the
                Canonical Error Type; without regarding the Canonical Error Code Range.

            2c - Then, it will then look for diagnosis associated with the
                Canonical Error Code Range; without regarding the Canonical Error Type.

            2d - At last, it will then look for diagnosis associated with the
                 without regarding the Canonical Error Type nor the Canonical Error Code Range.
                 (Diagnosis associated only with the Component Step).

            * For every search pattern described under 2, only one diagnosis will ever be returned
              based on the ERR_SEL_WEIGHT colum. The one with the greatest value will be the one
              returned.

              2e - After the previous search patters have chosen their most prioritized diagnostic,
                   an aditional prioritization between each other take place. The winner is the
                   diagnostic to be returned by this procedure.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE DIAGNOSE(
    p_CMP_NAME IN VARCHAR,
    p_CMP_STEP IN VARCHAR,
    p_ERR_CODE IN VARCHAR,
    p_ERR_TYPE IN VARCHAR,
    p_ERR_ACTION OUT VARCHAR,
    p_ERR_ACTION_CFG OUT SYS_REFCURSOR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  )
  AS

    v_CMP_CFG_ID ESB_ERR_CMP_CFG.ID%TYPE;

    v_CAN_ERR_ID ESB_CANONICAL_ERROR.ID%TYPE;

    v_ERR_DIAG_ID ESB_ERR_DIAGNOSTICS.ID%TYPE;

    v_ERR_ACTION_CFG_ID ESB_ERR_ACTION_CFG.ID%TYPE;

    p_ERR_ACTION_FULL ESB_ERR_ACTION.CODE%TYPE;
    p_ERR_ACTION_TYPE ESB_ERR_ACTION.CODE%TYPE;
    p_ERR_ACTION_CODE ESB_ERR_ACTION.CODE%TYPE;
    p_ERR_ACTION_NULL ESB_ERR_ACTION.CODE%TYPE;

    v_ERR_ACTION_CFG_ID_FULL ESB_ERR_ACTION_CFG.ID%TYPE;
    v_ERR_ACTION_CFG_ID_TYPE ESB_ERR_ACTION_CFG.ID%TYPE;
    v_ERR_ACTION_CFG_ID_CODE ESB_ERR_ACTION_CFG.ID%TYPE;
    v_ERR_ACTION_CFG_ID_NULL ESB_ERR_ACTION_CFG.ID%TYPE;

    v_ERR_DIAG_ID_FULL ESB_ERR_DIAGNOSTICS.ID%TYPE;
    v_ERR_DIAG_ID_TYPE ESB_ERR_DIAGNOSTICS.ID%TYPE;
    v_ERR_DIAG_ID_CODE ESB_ERR_DIAGNOSTICS.ID%TYPE;
    v_ERR_DIAG_ID_NULL ESB_ERR_DIAGNOSTICS.ID%TYPE;

    v_SOURCE_ERROR_CODE_INDIV VARCHAR(5);
    v_SOURCE_ERROR_CODE_FULL  VARCHAR(5);
    v_SOURCE_ERROR_CODE_TYPE  VARCHAR(5);
    v_SOURCE_ERROR_CODE_CODE  VARCHAR(5);
    v_SOURCE_ERROR_CODE_NULL  VARCHAR(5);

    v_Locator VARCHAR(255);

  BEGIN

    v_Locator := 'INIT - DIAGNOSE';

    p_SOURCE_ERROR_DESC := 'The Diagnostic was successfully recovered.';
    p_SOURCE_ERROR_CODE := '0';

    /*
      -------------------------------------------------------------------------------------
          Getting the Component Step on the ESB_ERR_CMP_CFG
      -------------------------------------------------------------------------------------
    */
        v_Locator := 'EXEC - GET_CMP_STEP_ID';

        v_CMP_CFG_ID := GET_CMP_STEP_ID(
                      p_CMP_NAME,
                      p_CMP_STEP,
                      v_Locator,
                      p_SOURCE_ERROR_CODE,
                      p_SOURCE_ERROR_DESC
                      );

        CASE p_SOURCE_ERROR_CODE
          WHEN '0' -- OK
            THEN NULL;

          WHEN '-1' -- No data
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN;

          WHEN '-2' -- Too Many
            THEN p_SOURCE_ERROR_CODE:='97'; RETURN;

          WHEN '-3' -- Unexpected Error
            THEN p_SOURCE_ERROR_CODE:='99'; RETURN;

          WHEN '-4' -- Logically Deleted
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN;

          ELSE
            p_SOURCE_ERROR_CODE:='99'; RETURN;
        END CASE;

    /*
      ----------------------------------------------------------------------------------------------------------------
      Getting the Canonical Error sent as a parameter.
      ----------------------------------------------------------------------------------------------------------------
    */
      v_Locator := 'EXEC - GET_CAN_ERR_ID';


        v_CAN_ERR_ID := GET_CAN_ERR_ID(
                          p_ERR_CODE,
                          p_ERR_TYPE,
                          v_Locator,
                          p_SOURCE_ERROR_CODE,
                          p_SOURCE_ERROR_DESC
                          );

        CASE p_SOURCE_ERROR_CODE
          WHEN '0' -- OK
            THEN NULL;

          WHEN '-1' -- No data
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN;

          WHEN '-2' -- Too Many
            THEN p_SOURCE_ERROR_CODE:='97'; RETURN;

          WHEN '-3' -- Unexpected Error
            THEN p_SOURCE_ERROR_CODE:='99'; RETURN;

          WHEN '-4' -- Logically Deleted
            THEN p_SOURCE_ERROR_CODE:='98'; RETURN;

          ELSE
            p_SOURCE_ERROR_CODE:='99'; RETURN;
        END CASE;
    ------------------------------------------------------------------------------------------------------------------

    /*
      ----------------------------------------------------------------------------------------------------------------
      We validate if there is a Diagnosis specific to the Canonical Error found before.
      ----------------------------------------------------------------------------------------------------------------
    */
    GET_DIAGNOSIS_ACTION_CFG_INDIV(
          v_CMP_CFG_ID,
          v_CAN_ERR_ID,
          p_ERR_ACTION,
          p_ERR_ACTION_CFG,
          v_ERR_DIAG_ID,
          v_Locator,
          p_SOURCE_ERROR_DESC,
          v_SOURCE_ERROR_CODE_INDIV
    );
    IF(v_SOURCE_ERROR_CODE_INDIV='-1') THEN
      NULL;
    ELSE
      p_SOURCE_ERROR_DESC := p_SOURCE_ERROR_DESC || ' - The Diagnostic has an ID of : ' ||  v_ERR_DIAG_ID;
      RETURN;
    END IF;

    /**************************************************************************************************
           At this point there was no specific Diagnosis found to be individually
           associated with the Canonical Error sent as parameter.
    ***************************************************************************************************/

    /*
      ----------------------------------------------------------------------------------------------------------------
        We validate if there is a Diagnosis associated with the whole Error Selector sent as parameter.
      ----------------------------------------------------------------------------------------------------------------
    */
     GET_DIAGNOSIS_ACTION_CFG_FULL(
          v_CMP_CFG_ID,
          p_ERR_CODE,
          p_ERR_TYPE,
          v_ERR_ACTION_CFG_ID_FULL,
          v_ERR_DIAG_ID_FULL,
          v_Locator,
          p_SOURCE_ERROR_DESC,
          v_SOURCE_ERROR_CODE_FULL
    );

    /*
      ----------------------------------------------------------------------------------------------------------------
        We validate if there is a Diagnosis associated with the Error Selector Type, regardless of its code.
      ----------------------------------------------------------------------------------------------------------------
    */
    GET_DIAGNOSIS_ACTION_CFG_TYPE(
          v_CMP_CFG_ID,
          p_ERR_TYPE,
          v_ERR_ACTION_CFG_ID_TYPE,
          v_ERR_DIAG_ID_TYPE,
          v_Locator,
          p_SOURCE_ERROR_DESC,
          v_SOURCE_ERROR_CODE_TYPE
    );

    /*
      ----------------------------------------------------------------------------------------------------------------
        We validate if there is a Diagnosis associated with the Error Selector Code, regardless of its Type.
      ----------------------------------------------------------------------------------------------------------------
    */
    GET_DIAGNOSIS_ACTION_CFG_CODE(
          v_CMP_CFG_ID,
          p_ERR_CODE,
          v_ERR_ACTION_CFG_ID_CODE,
          v_ERR_DIAG_ID_CODE,
          v_Locator,
          p_SOURCE_ERROR_DESC,
          v_SOURCE_ERROR_CODE_CODE
    );

    /*
    ----------------------------------------------------------------------------------------------------------------
      We validate if there is a Diagnosis associated with the Component, regardless of the Error Selector.
    ----------------------------------------------------------------------------------------------------------------
    */
    GET_DIAGNOSIS_ACTION_CFG_NULL(
          v_CMP_CFG_ID,
          v_ERR_ACTION_CFG_ID_NULL,
          v_ERR_DIAG_ID_NULL,
          v_Locator,
          p_SOURCE_ERROR_DESC,
          v_SOURCE_ERROR_CODE_NULL
    );

    v_Locator := 'Finished searching for Diagnostics, with outcomes :'
                      || v_SOURCE_ERROR_CODE_FULL || '|'
                      || v_SOURCE_ERROR_CODE_TYPE || '|'
                      || v_SOURCE_ERROR_CODE_CODE || '|'
                      || v_SOURCE_ERROR_CODE_NULL;

    IF(
        (
          v_SOURCE_ERROR_CODE_FULL +
          v_SOURCE_ERROR_CODE_TYPE +
          v_SOURCE_ERROR_CODE_CODE +
          v_SOURCE_ERROR_CODE_NULL
        )
        =-4
      ) THEN
            RETURN;
    END IF;

    v_Locator := 'Getting the most prioritized diagnostic, with :
               FULL:' || v_ERR_DIAG_ID_FULL || '|' ||
              'TYPE:' || v_ERR_DIAG_ID_TYPE || '|' ||
              'CODE:' || v_ERR_DIAG_ID_CODE || '|' ||
              'NULL:' || v_ERR_DIAG_ID_NULL;

    SELECT
      ERRD,
      ACODE,
      ACFG
      INTO
        v_ERR_DIAG_ID,
        p_ERR_ACTION,
        v_ERR_ACTION_CFG_ID
    FROM
      (
        SELECT
          ID ERRD,
          (SELECT CODE FROM ESB_ERR_ACTION WHERE ID = ACTION) ACODE,
          ACTION_CFG ACFG
        FROM
          ESB_ERR_DIAGNOSTICS
        WHERE
          (v_SOURCE_ERROR_CODE_FULL <> '-1' AND ID = v_ERR_DIAG_ID_FULL) OR
          (v_SOURCE_ERROR_CODE_TYPE <> '-1' AND ID = v_ERR_DIAG_ID_TYPE) OR
          (v_SOURCE_ERROR_CODE_CODE <> '-1' AND ID = v_ERR_DIAG_ID_CODE) OR
          (v_SOURCE_ERROR_CODE_NULL <> '-1' AND ID = v_ERR_DIAG_ID_NULL)
        ORDER BY ERR_SEL_WEIGHT DESC
      )
    WHERE
      ROWNUM = 1;

    GET_ACTION_CONFIG(v_ERR_ACTION_CFG_ID,p_ERR_ACTION_CFG,v_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);

    p_SOURCE_ERROR_DESC := p_SOURCE_ERROR_DESC || ' - The Diagnostic has an ID of : ' ||  v_ERR_DIAG_ID;

    EXCEPTION
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            v_Locator,
            '99',
            'Unexpected Error while getting a Diagnosis.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
  END DIAGNOSE;


  /* ================== MAIN PROCEDURE ==================

    Se manejan cuatro pasos de ejecucion:
      1.- Sys_Code, Raw_Code, Module, Sub-Module
      2.- Sys_Code, Module, Sub_Module.
      3.- Sys_Code, Module
      4.- Sys_Code

    El primer paso busca si existe una traducciÃƒÂ³n para los datos ingresados (traducciÃƒÂ³n para un error particular).
    En caso que no se encuentre una traducciÃƒÂ³n para el paso 1, se continua con el paso 2, el cual busca una traducciÃƒÂ³n un
    poco mas genÃƒÂ©rica utilizando solo 3 parametros (Sistema, API, Operacion).
    En caso de que tampoco se encuentre una traducciÃƒÂ³n, se continua con el paso 3, con 2 parÃƒÂ¡metros (Sistema, API).
    Finalmente, si no se encuentra ninguna traducciÃƒÂ³n para el paso 3, se busca un error generico para el sistema.

    */

  PROCEDURE getCanonicalError(
      p_MODULE IN VARCHAR2,
      p_SUB_MODULE IN VARCHAR2,
      p_RAW_CODE IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,

      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2) AS



  BEGIN

    /* Paso 1: se busca un error canonico para un Sys_Code, Raw_Code, Modulo y SubModulo dados. */
    IF ((p_MODULE IS NOT NULL) AND (p_SUB_MODULE IS NOT NULL) AND (p_RAW_CODE IS NOT NULL) ) THEN
        Get_Can_Err_Complete(p_MODULE, p_SUB_MODULE, p_RAW_CODE, p_ERROR_DETAILS_SOURCE, p_RESULT_STATUS,
                                p_RESULT_DESCRIPTION, p_CANONICAL_ERROR_CODE, p_CANONICAL_ERROR_TYPE, p_CANONICAL_ERROR_DESCRIPTION,
                                p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESCRIPTION, p_ERROR_DETAILS, p_ERROR_SOURCE);
    END IF;

    /*En caso de que el paso anterior no encuentre un error canonico, se busca para caso generico para API*/
    IF (p_CANONICAL_ERROR_CODE IS NULL) then
         Get_Generic_Err_For_API(p_MODULE,p_RAW_CODE, p_ERROR_DETAILS_SOURCE, p_RESULT_STATUS, p_RESULT_DESCRIPTION,
                                 p_CANONICAL_ERROR_CODE, p_CANONICAL_ERROR_TYPE, p_CANONICAL_ERROR_DESCRIPTION,
                                 p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESCRIPTION, p_ERROR_DETAILS,p_ERROR_SOURCE);
    END IF;

    /*En caso de que el paso anterior no encuentre un error canonico, se busca para caso generico para SYSTEM*/
    IF (p_CANONICAL_ERROR_CODE IS NULL) then
        Get_Generic_Err_For_System(p_RAW_CODE,p_ERROR_DETAILS_SOURCE, p_RESULT_STATUS,p_RESULT_DESCRIPTION,
                                  p_CANONICAL_ERROR_CODE,p_CANONICAL_ERROR_TYPE,p_CANONICAL_ERROR_DESCRIPTION, p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESCRIPTION, p_ERROR_DETAILS, p_ERROR_SOURCE);
    END IF;

    /* En caso de que el paso anterior no encuentre un error canonico, se busca para el Sys_Code, Modulo y SubModulo dados. */
    IF (p_CANONICAL_ERROR_CODE IS NULL) then
        Get_Can_Err_For_Sys_API_Oper(p_MODULE, p_SUB_MODULE,p_ERROR_DETAILS_SOURCE, p_RESULT_STATUS,
                                p_RESULT_DESCRIPTION, p_CANONICAL_ERROR_CODE, p_CANONICAL_ERROR_TYPE, p_CANONICAL_ERROR_DESCRIPTION,
                                p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESCRIPTION, p_ERROR_DETAILS, p_ERROR_SOURCE);
    END IF;

    /* En caso de que el paso anterior no encuentre un error canonico, se busca para el Sys_Code y Modulo dados. */
    IF (p_CANONICAL_ERROR_CODE IS NULL ) then
        Get_Can_Err_For_Sys_API(p_MODULE, p_ERROR_DETAILS_SOURCE, p_RESULT_STATUS,
                                p_RESULT_DESCRIPTION, p_CANONICAL_ERROR_CODE, p_CANONICAL_ERROR_TYPE, p_CANONICAL_ERROR_DESCRIPTION,
                                p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESCRIPTION, p_ERROR_DETAILS, p_ERROR_SOURCE);
    END IF;

    /* En caso de que el paso anterior no encuentre un error canonico, se busca para el Sys_Code dado. */
    IF (p_CANONICAL_ERROR_CODE IS NULL) then
        Get_Can_Err_For_System(p_ERROR_DETAILS_SOURCE, p_RESULT_STATUS,
                                p_RESULT_DESCRIPTION, p_CANONICAL_ERROR_CODE, p_CANONICAL_ERROR_TYPE, p_CANONICAL_ERROR_DESCRIPTION,
                                p_SOURCE_ERROR_CODE, p_SOURCE_ERROR_DESCRIPTION, p_ERROR_DETAILS, p_ERROR_SOURCE);
    END IF;

    /**/
    IF (p_CANONICAL_ERROR_CODE IS NULL) then
      p_RESULT_STATUS := 'ERROR';
      p_RESULT_DESCRIPTION := 'Ejecucion finalizada con Errores';
      p_CANONICAL_ERROR_TYPE := 'FWCF';
      p_CANONICAL_ERROR_DESCRIPTION := 'NO SE HAN ENCONTRADO DATOS.';
      p_CANONICAL_ERROR_CODE := -1;
    END IF;

  EXCEPTION

    WHEN OTHERS THEN
      p_RESULT_STATUS := 'ERROR';
      p_CANONICAL_ERROR_TYPE := 'FRW';
      p_CANONICAL_ERROR_DESCRIPTION := 'ERROR GENERICO.';
      p_CANONICAL_ERROR_CODE := -2;

  END getCanonicalError;

  /* Procedure para buscar un error canonico para el Sys_Code, Raw_Code, Modulo y SubModulo dados. */

  PROCEDURE Get_Can_Err_Complete (p_MODULE IN VARCHAR2,
  p_SUB_MODULE IN VARCHAR2,
  p_RAW_CODE IN VARCHAR2,
  p_ERROR_DETAILS_SOURCE IN VARCHAR2,

  p_RESULT_STATUS OUT VARCHAR2,
  p_RESULT_DESCRIPTION OUT VARCHAR2,
  p_CANONICAL_ERROR_CODE OUT VARCHAR2,
  p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
  p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
  p_SOURCE_ERROR_CODE OUT NUMBER,
  p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
  p_ERROR_DETAILS OUT VARCHAR2,
  p_ERROR_SOURCE OUT VARCHAR2) IS

  BEGIN
    p_RESULT_STATUS := '';
    p_RESULT_DESCRIPTION := '';
    p_CANONICAL_ERROR_CODE := '';
    p_CANONICAL_ERROR_TYPE := '';
    p_CANONICAL_ERROR_DESCRIPTION := '';
    p_SOURCE_ERROR_CODE := '';
    p_SOURCE_ERROR_DESCRIPTION := '';
    p_ERROR_DETAILS := '';
    p_ERROR_SOURCE := '';
  SELECT ET.NAME,
      ET.DESCRIPTION,
      CANERR.CODE,
      CANERRTY.TYPE,
      CANERR.DESCRIPTION,
      p_RAW_CODE,
      p_ERROR_DETAILS_SOURCE,
      SYS.DESCRIPTION,
      SYS.NAME

    INTO p_RESULT_STATUS,
     p_RESULT_DESCRIPTION,
     p_CANONICAL_ERROR_CODE,
     p_CANONICAL_ERROR_TYPE,
     p_CANONICAL_ERROR_DESCRIPTION,
     p_SOURCE_ERROR_CODE,
     p_SOURCE_ERROR_DESCRIPTION,
     p_ERROR_DETAILS,
     p_ERROR_SOURCE

  FROM ESB_ERROR_MAPPING MA
     INNER JOIN ESB_SYSTEM SYS ON SYS.ID = MA.ERROR_SOURCE
     INNER JOIN ESB_CANONICAL_ERROR CANERR ON CANERR.ID = MA.CAN_ERR_ID
     INNER JOIN ESB_ERROR_STATUS_TYPE ET ON ET.ID = MA.STATUS_ID
     INNER JOIN ESB_CANONICAL_ERROR_TYPE CANERRTY ON CANERRTY.ID = CANERR.TYPE_ID

   WHERE (SYS.CODE = p_ERROR_DETAILS_SOURCE)
     AND (MA.RAW_CODE = p_RAW_CODE)
     AND (MA.MODULE = p_MODULE)
     AND (MA.SUB_MODULE = p_SUB_MODULE)
     AND MA.RCD_STATUS <> 2  ;
   EXCEPTION WHEN OTHERS THEN
    NULL;
  END Get_Can_Err_Complete;


  /* Procedure para buscar un error canonico para el Sys_Code dado. */

  PROCEDURE Get_Can_Err_For_System (p_ERROR_DETAILS_SOURCE IN VARCHAR2,
  p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2) IS

      BEGIN

      p_RESULT_STATUS := '';
      p_RESULT_DESCRIPTION := '';
      p_CANONICAL_ERROR_CODE := '';
      p_CANONICAL_ERROR_TYPE := '';
      p_CANONICAL_ERROR_DESCRIPTION := '';
      p_SOURCE_ERROR_CODE := '';
      p_SOURCE_ERROR_DESCRIPTION := '';
      p_ERROR_DETAILS := '';
      p_ERROR_SOURCE := '';

      SELECT ET.NAME,
      ET.DESCRIPTION,
      CANERR.CODE,
      CANERRTY.TYPE,
      CANERR.DESCRIPTION,
    /*  p_RAW_CODE,  */
      p_ERROR_DETAILS_SOURCE,
      SYS.DESCRIPTION,
      SYS.NAME

      INTO p_RESULT_STATUS,
           p_RESULT_DESCRIPTION,
           p_CANONICAL_ERROR_CODE,
           p_CANONICAL_ERROR_TYPE,
           p_CANONICAL_ERROR_DESCRIPTION,
           /* p_SOURCE_ERROR_CODE, */
           p_SOURCE_ERROR_DESCRIPTION,
           p_ERROR_DETAILS,
           p_ERROR_SOURCE


      FROM ESB_ERROR_MAPPING MA
         INNER JOIN ESB_SYSTEM SYS ON SYS.ID = MA.ERROR_SOURCE
         INNER JOIN ESB_CANONICAL_ERROR CANERR ON CANERR.ID = MA.CAN_ERR_ID
         INNER JOIN ESB_ERROR_STATUS_TYPE ET ON ET.ID = MA.STATUS_ID
         INNER JOIN ESB_CANONICAL_ERROR_TYPE CANERRTY ON CANERRTY.ID = CANERR.TYPE_ID

    WHERE (SYS.CODE = p_ERROR_DETAILS_SOURCE)
      AND (MA.RAW_CODE IS NULL)
      AND (MA.MODULE IS NULL)
      AND (MA.SUB_MODULE IS NULL)
      AND MA.RCD_STATUS <> 2;

  EXCEPTION WHEN OTHERS THEN
    NULL;
  END Get_Can_Err_For_System;


  /* Procedure para buscar un error canonico para el Sys_Code, Modulo y SubModulo dados. */

  PROCEDURE Get_Can_Err_For_Sys_API_Oper(p_MODULE IN VARCHAR2,
  p_SUB_MODULE IN VARCHAR2,
  p_ERROR_DETAILS_SOURCE IN VARCHAR2,

  p_RESULT_STATUS OUT VARCHAR2,
  p_RESULT_DESCRIPTION OUT VARCHAR2,
  p_CANONICAL_ERROR_CODE OUT VARCHAR2,
  p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
  p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
  p_SOURCE_ERROR_CODE OUT NUMBER,
  p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
  p_ERROR_DETAILS OUT VARCHAR2,
  p_ERROR_SOURCE OUT VARCHAR2) IS

  BEGIN
    p_RESULT_STATUS := '';
    p_RESULT_DESCRIPTION := '';
    p_CANONICAL_ERROR_CODE := '';
    p_CANONICAL_ERROR_TYPE := '';
    p_CANONICAL_ERROR_DESCRIPTION := '';
    p_SOURCE_ERROR_CODE := '';
    p_SOURCE_ERROR_DESCRIPTION := '';
    p_ERROR_DETAILS := '';
    p_ERROR_SOURCE := '';

  SELECT ET.NAME,
      ET.DESCRIPTION,
      CANERR.CODE,
      CANERRTY.TYPE,
      CANERR.DESCRIPTION,
    /*  p_RAW_CODE,  */
      p_ERROR_DETAILS_SOURCE,
      SYS.DESCRIPTION,
      SYS.NAME

      INTO p_RESULT_STATUS,
           p_RESULT_DESCRIPTION,
           p_CANONICAL_ERROR_CODE,
           p_CANONICAL_ERROR_TYPE,
           p_CANONICAL_ERROR_DESCRIPTION,
           /* p_SOURCE_ERROR_CODE, */
           p_SOURCE_ERROR_DESCRIPTION,
           p_ERROR_DETAILS,
           p_ERROR_SOURCE


      FROM ESB_ERROR_MAPPING MA
         INNER JOIN ESB_SYSTEM SYS ON SYS.ID = MA.ERROR_SOURCE
         INNER JOIN ESB_CANONICAL_ERROR CANERR ON CANERR.ID = MA.CAN_ERR_ID
         INNER JOIN ESB_ERROR_STATUS_TYPE ET ON ET.ID = MA.STATUS_ID
         INNER JOIN ESB_CANONICAL_ERROR_TYPE CANERRTY ON CANERRTY.ID = CANERR.TYPE_ID

    WHERE (SYS.CODE = p_ERROR_DETAILS_SOURCE)
      AND (MA.RAW_CODE IS NULL)
      AND (MA.MODULE = p_MODULE)
      AND (MA.SUB_MODULE = p_SUB_MODULE)
      AND MA.RCD_STATUS <> 2;

   EXCEPTION WHEN OTHERS THEN
    NULL;
  END Get_Can_Err_For_Sys_API_Oper;


  /* Procedure para buscar un error canonico para el Sys_Code y Modulo dados. */

  PROCEDURE Get_Can_Err_For_Sys_API(p_MODULE IN VARCHAR2,
    p_ERROR_DETAILS_SOURCE IN VARCHAR2,

    p_RESULT_STATUS OUT VARCHAR2,
    p_RESULT_DESCRIPTION OUT VARCHAR2,
    p_CANONICAL_ERROR_CODE OUT VARCHAR2,
    p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
    p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
    p_SOURCE_ERROR_CODE OUT NUMBER,
    p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
    p_ERROR_DETAILS OUT VARCHAR2,
    p_ERROR_SOURCE OUT VARCHAR2) IS

  BEGIN
    p_RESULT_STATUS := '';
    p_RESULT_DESCRIPTION := '';
    p_CANONICAL_ERROR_CODE := '';
    p_CANONICAL_ERROR_TYPE := '';
    p_CANONICAL_ERROR_DESCRIPTION := '';
    p_SOURCE_ERROR_CODE := '';
    p_SOURCE_ERROR_DESCRIPTION := '';
    p_ERROR_DETAILS := '';
    p_ERROR_SOURCE := '';

    SELECT ET.NAME,
        ET.DESCRIPTION,
        CANERR.CODE,
        CANERRTY.TYPE,
        CANERR.DESCRIPTION,
      /*  p_RAW_CODE,  */
        p_ERROR_DETAILS_SOURCE,
        SYS.DESCRIPTION,
        SYS.NAME

        INTO p_RESULT_STATUS,
             p_RESULT_DESCRIPTION,
             p_CANONICAL_ERROR_CODE,
             p_CANONICAL_ERROR_TYPE,
             p_CANONICAL_ERROR_DESCRIPTION,
             /* p_SOURCE_ERROR_CODE, */
             p_SOURCE_ERROR_DESCRIPTION,
             p_ERROR_DETAILS,
             p_ERROR_SOURCE

        FROM ESB_ERROR_MAPPING MA
           INNER JOIN ESB_SYSTEM SYS ON SYS.ID = MA.ERROR_SOURCE
           INNER JOIN ESB_CANONICAL_ERROR CANERR ON CANERR.ID = MA.CAN_ERR_ID
           INNER JOIN ESB_ERROR_STATUS_TYPE ET ON ET.ID = MA.STATUS_ID
           INNER JOIN ESB_CANONICAL_ERROR_TYPE CANERRTY ON CANERRTY.ID = CANERR.TYPE_ID

      WHERE (SYS.CODE = p_ERROR_DETAILS_SOURCE)
        AND (MA.RAW_CODE IS NULL)
        AND (MA.MODULE = p_MODULE)
        AND (MA.SUB_MODULE IS NULL)
        AND MA.RCD_STATUS <> 2;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END Get_Can_Err_For_Sys_API;

  /* Procedure para buscar un error canonico para el Sys_Code, Raw_Code, Modulo indicado y SubModulo generico. */
  PROCEDURE Get_Generic_Err_For_API(
      p_MODULE               IN VARCHAR2,
      /* p_SUB_MODULE           IN VARCHAR2, */
      p_RAW_CODE             IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2)
  IS
  BEGIN
    p_RESULT_STATUS               := '';
    p_RESULT_DESCRIPTION          := '';
    p_CANONICAL_ERROR_CODE        := '';
    p_CANONICAL_ERROR_TYPE        := '';
    p_CANONICAL_ERROR_DESCRIPTION := '';
    p_SOURCE_ERROR_CODE           := '';
    p_SOURCE_ERROR_DESCRIPTION    := '';
    p_ERROR_DETAILS               := '';
    p_ERROR_SOURCE                := '';
    SELECT ET.NAME,
      ET.DESCRIPTION,
      CANERR.CODE,
      CANERRTY.TYPE,
      CANERR.DESCRIPTION,
      p_RAW_CODE,
      p_ERROR_DETAILS_SOURCE,
      SYS.DESCRIPTION,
      SYS.NAME
    INTO p_RESULT_STATUS,
      p_RESULT_DESCRIPTION,
      p_CANONICAL_ERROR_CODE,
      p_CANONICAL_ERROR_TYPE,
      p_CANONICAL_ERROR_DESCRIPTION,
      p_SOURCE_ERROR_CODE,
      p_SOURCE_ERROR_DESCRIPTION,
      p_ERROR_DETAILS,
      p_ERROR_SOURCE
    FROM ESB_ERROR_MAPPING MA
      INNER JOIN ESB_SYSTEM SYS
        ON SYS.ID = MA.ERROR_SOURCE
      INNER JOIN ESB_CANONICAL_ERROR CANERR
        ON CANERR.ID = MA.CAN_ERR_ID
      INNER JOIN ESB_ERROR_STATUS_TYPE ET
        ON ET.ID = MA.STATUS_ID
      INNER JOIN ESB_CANONICAL_ERROR_TYPE CANERRTY
        ON CANERRTY.ID     = CANERR.TYPE_ID
    WHERE (SYS.CODE    = p_ERROR_DETAILS_SOURCE)
      AND (MA.RAW_CODE   = p_RAW_CODE)
      AND (MA.MODULE     = p_MODULE)
      AND (MA.SUB_MODULE IS NULL)
      AND MA.RCD_STATUS <> 2 ;
  EXCEPTION
  WHEN OTHERS THEN
    NULL;
  END Get_Generic_Err_For_API;

/* Procedure para buscar un error canonico para el Sys_Code, Raw_Code, Modulo y SubModulo generico. */
  PROCEDURE Get_Generic_Err_For_System(
     /* p_MODULE               IN VARCHAR2,
       p_SUB_MODULE           IN VARCHAR2, */
      p_RAW_CODE             IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2)
  IS
  BEGIN
    p_RESULT_STATUS               := '';
    p_RESULT_DESCRIPTION          := '';
    p_CANONICAL_ERROR_CODE        := '';
    p_CANONICAL_ERROR_TYPE        := '';
    p_CANONICAL_ERROR_DESCRIPTION := '';
    p_SOURCE_ERROR_CODE           := '';
    p_SOURCE_ERROR_DESCRIPTION    := '';
    p_ERROR_DETAILS               := '';
    p_ERROR_SOURCE                := '';
    SELECT ET.NAME,
      ET.DESCRIPTION,
      CANERR.CODE,
      CANERRTY.TYPE,
      CANERR.DESCRIPTION,
      p_RAW_CODE,
      p_ERROR_DETAILS_SOURCE,
      SYS.DESCRIPTION,
      SYS.NAME
    INTO p_RESULT_STATUS,
      p_RESULT_DESCRIPTION,
      p_CANONICAL_ERROR_CODE,
      p_CANONICAL_ERROR_TYPE,
      p_CANONICAL_ERROR_DESCRIPTION,
      p_SOURCE_ERROR_CODE,
      p_SOURCE_ERROR_DESCRIPTION,
      p_ERROR_DETAILS,
      p_ERROR_SOURCE
    FROM ESB_ERROR_MAPPING MA
      INNER JOIN ESB_SYSTEM SYS
        ON SYS.ID = MA.ERROR_SOURCE
      INNER JOIN ESB_CANONICAL_ERROR CANERR
        ON CANERR.ID = MA.CAN_ERR_ID
      INNER JOIN ESB_ERROR_STATUS_TYPE ET
        ON ET.ID = MA.STATUS_ID
      INNER JOIN ESB_CANONICAL_ERROR_TYPE CANERRTY
        ON CANERRTY.ID     = CANERR.TYPE_ID
    WHERE (SYS.CODE    = p_ERROR_DETAILS_SOURCE)
      AND (MA.RAW_CODE   = p_RAW_CODE)
      AND (MA.MODULE     IS NULL)
      AND (MA.SUB_MODULE IS NULL)
      AND MA.RCD_STATUS <> 2 ;
  EXCEPTION
  WHEN OTHERS THEN
    NULL;
  END Get_Generic_Err_For_System;

END ESB_ERROR_MANAGER_PKG;
/
