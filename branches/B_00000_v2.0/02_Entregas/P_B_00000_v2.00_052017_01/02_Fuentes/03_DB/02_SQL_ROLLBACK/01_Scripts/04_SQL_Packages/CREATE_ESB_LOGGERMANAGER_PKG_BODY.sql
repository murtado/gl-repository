create or replace PACKAGE BODY ESB_LOGGERMANAGER_PKG AS

  /*---------------------------------------------------------------------------------------------------

    Fills the p_SOURCE_ERROR_CODE and p_SOURCE_ERROR_DESC variable used thorughout the Package, by the
    use of the following parameters :

    p_Locator   -->  Las known position within a Procedure or Function
    p_Reason    -->  Descriptive information about an Error.
    p_ErrCode   -->  Unique code of an Error, within the context of this package.
    p_SqlErr    -->  SQLERR Information, as it exists within the error context
                                from which this procedure is called.

  ---------------------------------------------------------------------------------------------------
  PROCEDURE LOAD_SOURCE_ERROR(
      p_Locator IN VARCHAR,
      p_ErrCode IN VARCHAR,
      p_Reason IN VARCHAR,
      p_SqlErr IN VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    IS
      BEGIN

            p_SOURCE_ERROR_CODE := p_ErrCode;
            p_SOURCE_ERROR_DESC :=
              p_Reason
              || ' @ ['
              || p_Locator
              || '] --- Error ['
              || p_SqlErr
              || ']';
      EXCEPTION
        WHEN OTHERS THEN
          p_SOURCE_ERROR_CODE := p_ErrCode;
          p_SOURCE_ERROR_DESC := SUBSTR(
              p_Reason
              || ' @ ['
              || p_Locator
              || '] --- Error ['
              || p_SqlErr
              || ']',
              0,
              250
            );


  END LOAD_SOURCE_ERROR;*/

  /*---------------------------------------------------------------------------------------------------

    **** FUNCTION CREATE_LOG_CONVERSATION :: Creates a registry in ESB_CONVERSATION returning the ID of the new entry.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------

  FUNCTION CREATE_LOG_CONVERSATION(
    p_TRANSACTION_ID              IN NUMBER,
    p_CAPABILITY_ID               IN NUMBER,
    p_CONVERSATION_ID             IN VARCHAR2,
    p_CHANNEL_ID                  IN NUMBER,
    p_CONSUMER_ID                 IN NUMBER,
    p_RCD_STATUS                  IN NUMBER,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )  RETURN NUMBER
    IS
      v_CONVERSATION_ID_ID NUMBER;
      v_CONV_SEQ NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The LOG Conversation was successfully created.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.CREATE_LOG_CONVERSATION - INIT';

        /* ---------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------*/
          SELECT
            COUNT(*)
              INTO
              v_CONV_SEQ
          FROM ESB_CONVERSATION C
            INNER JOIN ESB_MESSAGE_TRANSACTION M ON M.ID = C.MESSAGE_TX_ID
          WHERE M.ID = p_TRANSACTION_ID;
        ---------------------------------------------------------------------------------------

        INSERT INTO
          ESB_CONVERSATION(
            ID,
            MESSAGE_TX_ID,
            CAPABILITY_ID,
            CONVERSATION_ID,
            CHANNEL_ID,
            CONSUMER_ID,
            SEQUENCE,
            RCD_STATUS
          )
          VALUES (
              ESB_CONVERSATION_SEQ.NEXTVAL,
              p_TRANSACTION_ID,
              p_CAPABILITY_ID,
              p_CONVERSATION_ID,
              p_CHANNEL_ID,
              p_CONSUMER_ID,
              v_CONV_SEQ,
              p_RCD_STATUS
            )
        RETURNING "ID" INTO v_CONVERSATION_ID_ID;

        RETURN v_CONVERSATION_ID_ID;

      EXCEPTION
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [ConversationID]=' || p_CONVERSATION_ID || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END CREATE_LOG_CONVERSATION;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_MESSAGE_TRANSACTION
    (
      p_CAPABILITY_ID           IN NUMBER,
      p_PROC_ID                 IN VARCHAR2,
      p_EVENT_ID                IN VARCHAR2,
      p_CLIENT_REQ_TIMESTAMP    IN VARCHAR2,
      p_CORRELATION_ID          IN VARCHAR2,
      p_MESSAGE_TRANSACTION_ID  OUT NUMBER,
      p_CREATE_INVALID     IN CHAR,
      p_Locator                 OUT VARCHAR,
      p_SOURCE_ERROR_CODE       OUT VARCHAR,
      p_SOURCE_ERROR_DESC       OUT VARCHAR
    )
  IS
    CAPABILITY_ID_AUX NUMBER;
    V_TX_AMOUNT NUMBER;
    V_TX_AMOUNT_CAP NUMBER;
    V_RCD_STATUS NUMBER;
  BEGIN

    p_SOURCE_ERROR_DESC := 'Verifying the Transaction';
    p_SOURCE_ERROR_CODE := '0';
    p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_MESSAGE_TRANSACTION - INIT';

    /*
        Se busca la Tx. asociada a la conversacion sobre la cual se solicita un Log. Para identificar dicha Tx. se dependen de los datos :
        PROCESS_ID, EVENT_ID. Si solo si los 2 datos (que siempre son enviados por el cliente y no modificados)
        existen en la tabla ESB_MESSAGE_TRANSACTION se considera la Tx. como la owner de la conversacion antes mencionada.

        En caso de no encontrar una Tx. asociada, se la genera para evitar perder el LOG. Esto es una accion esperada, como el componente
        MessageManager solo registra manualmente (sincronicamente) la Tx. cuando las validaciones configuradas para la dupla
        Servicio + Capacidad dependan de informacion para la cual no puede esperarse el Logging Async del LoggerManager (Idempotencia x ejemplo).

        Importante : Si un cliente envia estos 2 mismos datos iguales, para distintas Tx., y la dupla Servicio + Capacidad
        asociada a ellas no percibe de validaciones que impidan mensajes duplicados, nuevas conversaciones generadas en base
        a estos 2 datos van a verse relacionados con el ULTIMO registro de la tabla ESB_MESSAGE_TRANSACTION que sea vea asociado
        a ellos.

        Las comparaciones entre valores NULL realizadas mediante el operador "=" tiene como resultado FALSE en todos los casos.
        Por este motivo se utiliza la funcion COALESCE para aquellos valores definidos como opcionales (ProcessID por ejemplo).
    */

        SELECT M.ID INTO p_MESSAGE_TRANSACTION_ID
        FROM ESB_MESSAGE_TRANSACTION M
        WHERE COALESCE(M.PROC_ID, './*@*@') = COALESCE(p_PROC_ID, './*@*@') AND
              M.EVENT_ID = p_EVENT_ID AND COALESCE(M.CORRELATION_ID, './*@*@') = COALESCE(p_CORRELATION_ID, './*@*@') AND
          /*
            Este OrderBY y ROWNUM se estabece por la (remota) posibilidad de que para una misma combinacion de ProcessID+EventID+CorrelationID
            se encuentren mas de un registro.

            Se debe asumir que el ProcessID+EventID no son modificados una vez son recibidos de un cliente externo, y el CorrelationID solo es
            modificado por el FRW durante los re-procesos ejecutados por el ErrorHospital.

            Cada re-proceso (reintento por el ErrorHospital) va a tener un CorrelationID distinto.

            El CorrelationID no debiera ser modificado por ningun otro actor que el ErrorHospital; de ser modificado, podria ocurrir un comportamiento inesperado.
          */
          ROWNUM=1
        ORDER BY M.MSG_TIMESTAMP;

  EXCEPTION

    -- Aqui deberia llegarse si solo si el registro de la Tx. no fue realizada por el MessageManager de manera Sync. A su vez, este caso debiera occurir
    -- unicamente en casos donde la capacidad de servicio asociada carezca de validaciones de Tx. configuradas (Duplicidad por ejemplo).
  WHEN NO_DATA_FOUND THEN

    INSERT INTO ESB_MESSAGE_TRANSACTION
      (
        ID,
        PROC_ID,
        EVENT_ID,
        CLIENT_REQ_TIMESTAMP,
        STATUS,
        MSG_TIMESTAMP,
        "SEQUENCE",
        CORRELATION_ID,
        RCD_STATUS
      )
      VALUES
      (
        ESB_MESSAGE_TRANSACTION_SEQ.NEXTVAL,
        p_PROC_ID,
        p_EVENT_ID,
        TO_TIMESTAMP_TZ(p_CLIENT_REQ_TIMESTAMP,'YYYY-MM-DD HH24:MI:SS:FF TZH:TZM'),
        -- Se asume que es la primera Tx, y que el MessageManager eligio no registrarla sincronicamente.
        0,
        CURRENT_TIMESTAMP,
        /*
          Se genera una Tx con SEQUENCE igual a la cantidad de Tx. con equivalentes PROC_ID and EVENT_ID; (Si no encuentra transaccion, va a generar la de SEQUENCE = 0).

        Las comparaciones entre valores NULL realizadas mediante el operador "=" tiene como resultado FALSE en todos los casos.
        Por este motivo se utiliza la funcion COALESCE para aquellos valores definidos como opcionales (ProcessID por ejemplo).
        */
        (
          SELECT COUNT(*)
              FROM ESB_MESSAGE_TRANSACTION MT
              WHERE
                COALESCE(MT.PROC_ID, './*@*@') = COALESCE(p_PROC_ID, './*@*@') AND
                MT.EVENT_ID  = p_EVENT_ID
        ),
        p_CORRELATION_ID,
        1
      )
    RETURNING "ID"
    INTO p_MESSAGE_TRANSACTION_ID;
  END VERIFY_MESSAGE_TRANSACTION;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_SERVICE_CAPABILITY(
    p_SERVICE_NAME IN VARCHAR2,
    p_SERVICE_OPERATION IN VARCHAR2,
    p_SERVICE_CODE IN VARCHAR2,
    p_CAPABILITY_ID OUT NUMBER,
    p_CREATE_INVALID     IN CHAR,
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR,
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) IS

      v_SERVICE_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'Verifying the Transaction';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_SERVICE_CAPABILITY - INIT';

        /*--------------------------------------------------------
          Getting the Service.
        --------------------------------------------------------*/
        v_SERVICE_ID  := ESB_COMMONS_PKG.GET_SERVICE_ID(
                                        p_SERVICE_CODE,
                                        p_SERVICE_NAME,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE!=0) THEN -- It does not.

          IF (p_CREATE_INVALID!='T') THEN
            RETURN;
          ELSE

            IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.

              -- Updating the service with RCD_STATUS = 0 (Invalid).
              UPDATE
                  ESB_SERVICE
              SET
                  RCD_STATUS = 0
              WHERE
                CODE = p_SERVICE_CODE AND
                NAME = p_SERVICE_NAME;

            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                -- Creating the service with RCD_STATUS = 0 (Invalid).
                v_SERVICE_ID  := ESB_COMMONS_PKG.CREATE_SERVICE(
                                              p_SERVICE_CODE,
                                              p_SERVICE_NAME,
                                              0,
                                              p_Locator,
                                              p_SOURCE_ERROR_CODE,
                                              p_SOURCE_ERROR_DESC
                                            );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                RETURN;
              END IF;
            END IF;
          END IF;
        END IF;
        ----------------------------------------------------------------

        /*--------------------------------------------------------
          Getting the capability.
        --------------------------------------------------------*/
        p_CAPABILITY_ID  := ESB_COMMONS_PKG.GET_CAPABILITY_ID(
                                        v_SERVICE_ID,
                                        p_SERVICE_OPERATION,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE!=0) THEN
          IF (p_CREATE_INVALID!='T') THEN
            RETURN;
          ELSE
            IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.

              -- Updating the capability with RCD_STATUS = 0 (Invalid).
              UPDATE
                  ESB_CAPABILITY
              SET
                  RCD_STATUS = 0
              WHERE
                  SERVICE_ID = v_SERVICE_ID AND
                  NAME = p_SERVICE_OPERATION;

            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                -- Creating the capability with RCD_STATUS = 0 (Invalid).
                p_CAPABILITY_ID  := ESB_COMMONS_PKG.CREATE_CAPABILITY(
                                              v_SERVICE_ID,
                                              'UNK_'|| p_SERVICE_OPERATION,
                                              p_SERVICE_OPERATION,
                                              0,
                                              p_Locator,
                                              p_SOURCE_ERROR_CODE,
                                              p_SOURCE_ERROR_DESC
                                            );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                RETURN;
              END IF;
            END IF;
          END IF;
        END IF;
        ----------------------------------------------------------------

    EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
  END VERIFY_SERVICE_CAPABILITY;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_LOG_CONVERSATION(
      p_TRANSACTION_ID     IN NUMBER,
      p_HEADER_TRACER      IN HEADERTRACER_T,
      p_CONVERSATION_ID_ID OUT NUMBER,
      p_CAPABILITY_ID      IN NUMBER,
      p_CREATE_INVALID     IN CHAR,
      p_Locator            OUT VARCHAR,
      p_SOURCE_ERROR_CODE  OUT VARCHAR,
      p_SOURCE_ERROR_DESC  OUT VARCHAR
    )
    IS

        v_CONSUMER_ID         NUMBER;
        v_COUNTRY_ID          NUMBER;
        v_CHANNEL_ID          NUMBER;
        v_ENTERPRISE_ID       NUMBER;
        v_CONV_SEQ            NUMBER;
        v_CAPABILITY_ID       NUMBER;
        v_SYSTEM_ID       NUMBER;

      BEGIN

          p_SOURCE_ERROR_DESC := 'Verifying the Transaction';
          p_SOURCE_ERROR_CODE := '0';
          p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_CONVERSATION - INIT';


          /* ---------------------------------------------------------------------------------------
            Getting the Country.
          ---------------------------------------------------------------------------------------*/
          v_COUNTRY_ID  := ESB_COMMONS_PKG.GET_COUNTRY_ID(
                                        p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );

          -- Does it exist?.
          IF(p_SOURCE_ERROR_CODE!=0) THEN
            IF (p_CREATE_INVALID!='T') THEN
              RETURN;
            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                v_COUNTRY_ID := ESB_COMMONS_PKG.CREATE_COUNTRY(
                                          p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_,
                                          'UNK_'|| p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_,
                                          'UNK Country.',
                                          0,
                                          p_Locator,
                                          p_SOURCE_ERROR_CODE,
                                          p_SOURCE_ERROR_DESC
                                      );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                  -- Updating country with RCD_STATUS = 0 (Invalid).
                  UPDATE
                      ESB_COUNTRY
                  SET
                      RCD_STATUS = 0
                  WHERE
                    CODE = p_HEADER_TRACER.HEADER_.CONSUMER_.COUNTRYCODE_;
                ELSE
                  RETURN;
                END IF;
              END IF;
            END IF;
          END IF;

          /* ---------------------------------------------------------------------------------------
            Getting the Channel.
          ---------------------------------------------------------------------------------------*/
          v_CHANNEL_ID  := ESB_COMMONS_PKG.GET_CHANNEL_ID_BY_NAME(
                                        p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );
          p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_CONVERSATION - EXEC - ESB_COMMONS_PKG.GET_CHANNEL_ID_BY_NAME';

          -- Does it exist?.
          IF(p_SOURCE_ERROR_CODE!=0) THEN
            IF (p_CREATE_INVALID!='T') THEN
              RETURN;
            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                v_CHANNEL_ID := ESB_COMMONS_PKG.CREATE_CHANNEL(
                                      'UNK_'|| p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_,
                                      p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_,
                                      0,
                                      p_Locator,
                                      p_SOURCE_ERROR_CODE,
                                      p_SOURCE_ERROR_DESC
                                  );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                  -- Updating channel with RCD_STATUS = 0 (Invalid).
                  UPDATE
                      ESB_CHANNEL
                  SET
                      RCD_STATUS = 0
                  WHERE
                    NAME = p_HEADER_TRACER.HEADER_.CHANNEL_.NAME_;
                ELSE
                  RETURN;
                END IF;
              END IF;
            END IF;
          END IF;

          /* ---------------------------------------------------------------------------------------
            Getting the Enterprise.
          ---------------------------------------------------------------------------------------*/
          v_ENTERPRISE_ID  := ESB_COMMONS_PKG.GET_ENTERPRISE_ID(
                                        p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );

          -- Does it exist?.
          IF(p_SOURCE_ERROR_CODE!=0) THEN
            IF (p_CREATE_INVALID!='T') THEN
              RETURN;
            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                v_ENTERPRISE_ID := ESB_COMMONS_PKG.CREATE_ENTERPRISE(
                                      p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_,
                                      'UNK_'|| p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_,
                                      0,
                                      p_Locator,
                                      p_SOURCE_ERROR_CODE,
                                      p_SOURCE_ERROR_DESC
                                  );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                  -- Updating Enterprise with RCD_STATUS = 0 (Invalid).
                  UPDATE
                      ESB_ENTERPRISE
                  SET
                      RCD_STATUS = 0
                  WHERE
                    CODE = p_HEADER_TRACER.HEADER_.CONSUMER_.ENTERPRISECODE_;
                ELSE
                  RETURN;
                END IF;
              END IF;
            END IF;
          END IF;

          /* ---------------------------------------------------------------------------------------
            Getting the System.
          ---------------------------------------------------------------------------------------*/
          v_SYSTEM_ID  := ESB_COMMONS_PKG.GET_SYSTEM_ID(
                                        p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_,
                                        p_Locator,
                                        p_SOURCE_ERROR_CODE,
                                        p_SOURCE_ERROR_DESC
                                      );

          -- Does it exist?.
          IF(p_SOURCE_ERROR_CODE!=0) THEN
            IF (p_CREATE_INVALID!='T') THEN
              RETURN;
            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                  v_SYSTEM_ID := ESB_COMMONS_PKG.CREATE_SYSTEM(
                                      p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_,
                                      'UNK_'|| p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_,
                                      'UNK ConsumerSystem.',
                                      0,
                                      p_Locator,
                                      p_SOURCE_ERROR_CODE,
                                      p_SOURCE_ERROR_DESC
                                  );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                  -- Updating System with RCD_STATUS = 0 (Invalid).
                  UPDATE
                      ESB_SYSTEM
                  SET
                      RCD_STATUS = 0
                  WHERE
                    CODE = p_HEADER_TRACER.HEADER_.CONSUMER_.SYSCODE_;
                ELSE
                  RETURN;
                END IF;
              END IF;
            END IF;
          END IF;

          /* ---------------------------------------------------------------------------------------
            Getting the Consumer.
          ---------------------------------------------------------------------------------------*/
          v_CONSUMER_ID := ESB_COMMONS_PKG.GET_CONSUMER_ID(
                                              v_SYSTEM_ID,
                                              v_COUNTRY_ID,
                                              v_ENTERPRISE_ID,
                                              p_Locator,
                                              p_SOURCE_ERROR_CODE,
                                              p_SOURCE_ERROR_DESC
                                            );

          -- Does it exist?.
          IF(p_SOURCE_ERROR_CODE!=0) THEN
            IF (p_CREATE_INVALID!='T') THEN
              RETURN;
            ELSE
              IF(p_SOURCE_ERROR_CODE=-1) THEN
                  v_CONSUMER_ID := ESB_COMMONS_PKG.CREATE_CONSUMER(
                                      v_SYSTEM_ID,
                                      v_COUNTRY_ID,
                                      v_ENTERPRISE_ID,
                                      0,
                                      p_Locator,
                                      p_SOURCE_ERROR_CODE,
                                      p_SOURCE_ERROR_DESC
                                  );
                IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;
              ELSE
                 IF(p_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.
                      -- Updating Consumer with RCD_STATUS = 0 (Invalid).
                      UPDATE
                          ESB_CONSUMER
                      SET
                          RCD_STATUS = 0
                      WHERE
                        SYSCODE = v_SYSTEM_ID AND
                        COUNTRY_ID = v_COUNTRY_ID AND
                        ENT_CODE = v_ENTERPRISE_ID;
                  ELSE
                    RETURN;
                END IF;
              END IF;
            END IF;
          END IF;

          /* ---------------------------------------------------------------------------------------
            Getting the Conversation.
          ---------------------------------------------------------------------------------------*/
          p_CONVERSATION_ID_ID := ESB_CONVERSATION_MANAGER_PKG.GET_CONVERSATION_ID(
                                              p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_,
                                              p_Locator,
                                              p_SOURCE_ERROR_CODE,
                                              p_SOURCE_ERROR_DESC
                                            );

          -- Does it exist?.
          -- If -1, it means that the conversation does not exists. This would be the 1st Log.
          IF(p_SOURCE_ERROR_CODE=-1) THEN
            -- Creating the Conversation.
            p_CONVERSATION_ID_ID := CREATE_LOG_CONVERSATION(
                                      p_TRANSACTION_ID,
                                      p_CAPABILITY_ID,
                                      p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_,
                                      v_CHANNEL_ID,
                                      v_CONSUMER_ID,
                                      1,
                                      p_Locator,
                                      p_SOURCE_ERROR_CODE,
                                      p_SOURCE_ERROR_DESC
                                  );
            IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

          ELSE
            IF (p_SOURCE_ERROR_CODE!=0) THEN
              RETURN;
            END IF;
          END IF;
          ---------------------------------------------------------------------------------------

        EXCEPTION
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [ConversationID]=' || p_HEADER_TRACER.HEADER_.TRACE_.CONVERSATIONID_ || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
              );

  END VERIFY_LOG_CONVERSATION;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_LOG_TRACE(
    p_HEADER_TRACER      IN HEADERTRACER_T DEFAULT NULL,
    p_LOG_PLACEHOLDER    IN PLACEHOLDER_T,
    p_CONVERSATION_ID_ID    IN NUMBER,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR

  ) RETURN NUMBER
    IS

    v_TRACE_ID       NUMBER;
    v_SEQUENCE_TRACE NUMBER;

  BEGIN

    p_SOURCE_ERROR_DESC := 'Creating a Trace Element';
    p_SOURCE_ERROR_CODE := '0';
    p_Locator := 'ESB_LOGGERMANAGER_PKG.CREATE_LOG_TRACE - INIT';

    /* ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------*/
      SELECT
        COUNT(T.ID)
          INTO
            v_SEQUENCE_TRACE
      FROM ESB_TRACE T
        INNER JOIN ESB_CONVERSATION C ON T.CONV_ID = C.ID
      WHERE C.ID = p_CONVERSATION_ID_ID;
    ---------------------------------------------------------------------------------------

    INSERT INTO
      ESB_TRACE(
        ID,
        CONV_ID,
        SEQUENCE,
        COMPONENT,
        OPERATION,
        LOG_TIMESTAMP,
        LOG_PLACEHOLDER,
        RCD_STATUS)
      VALUES (
        ESB_TRACE_SEQ.NEXTVAL,
        p_CONVERSATION_ID_ID,
        v_SEQUENCE_TRACE,
        p_HEADER_TRACER.COMPONENT_,
        p_HEADER_TRACER.OPERATION_,
        TO_TIMESTAMP_TZ(p_LOG_PLACEHOLDER.TIME_,'YYYY-MM-DD HH24:MI:SS:FF TZH:TZM'),
        p_LOG_PLACEHOLDER.PLACE_,
        1
      )
      RETURNING ID INTO v_TRACE_ID
      ;

      RETURN v_TRACE_ID;

  END CREATE_LOG_TRACE;

    /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE GET_SEVERITY(p_LOGSEVERITY IN VARCHAR2, v_SEVERITY_ID OUT NUMBER) IS
  BEGIN
    SELECT ID INTO v_SEVERITY_ID FROM ESB_SEVERITY WHERE "LEVEL" = UPPER(p_LOGSEVERITY);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      INSERT INTO ESB_SEVERITY(ID, "LEVEL", DESCRIPTION, "ORDER", RCD_STATUS) VALUES(ESB_SEVERITY_SEQ.NEXTVAL, UPPER(p_LOGSEVERITY), 'Unknown Level', 100, 0) RETURNING "ID" INTO v_SEVERITY_ID;
  END GET_SEVERITY;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_LOG_TYPE_ID(
    p_LOG_TYPE_NAME      IN VARCHAR2,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  ) RETURN NUMBER
    IS

      v_LOG_TYPE_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The LogType was successfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.GET_LOG_TYPE_ID - INIT';

        SELECT
          ID
            INTO
              v_LOG_TYPE_ID
            FROM
              ESB_LOG_TYPE
            WHERE
              "NAME" = UPPER(p_LOG_TYPE_NAME) AND
              RCD_STATUS <> 2;

        RETURN v_LOG_TYPE_ID;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-1',
              'There is no record with this info : [LogType]=' || p_LOG_TYPE_NAME || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [LogType]=' ||  p_LOG_TYPE_NAME || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END GET_LOG_TYPE_ID;

  /*---------------------------------------------------------------------------------------------------

    **** FUNCTION GET_LOG_MESSAGE :: Creates a registry in ESB_CONVERSATION returning the ID of the new entry.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_LOG_MESSAGE(
    p_CONVERSATION_ID             IN VARCHAR2,
    p_LOG_TYPE_NAME               IN VARCHAR2,
    p_LOG_OCCURENCE               IN NUMBER, -- If more than one message is found for the p_LOG_TYPE_NAME,
                                                -- then the occurrence N (Starting from 1) of that group is returned..
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )  RETURN CLOB
    IS
      v_CONVERSATION_ID_ID NUMBER;
      p_LOG_TYPE_ID NUMBER;
      p_LOG_MESSAGE CLOB;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The LOG was successfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.GET_LOG_MESSAGE - INIT';

        /* ---------------------------------------------------------------------------------------
          Getting the Log Type.
        ---------------------------------------------------------------------------------------*/
        p_LOG_TYPE_ID := GET_LOG_TYPE_ID(
                                  p_LOG_TYPE_NAME,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                                );
        IF(p_SOURCE_ERROR_CODE!='0') THEN RETURN ''; END IF;
        ---------------------------------------------------------------------------------------

        SELECT
          MSG
          RNUM
            INTO
              p_LOG_MESSAGE
        FROM
          (
            SELECT
              LOG.MESSAGE MSG,
              ROWNUM RNUM
            FROM
              ESB_LOG LOG
                INNER JOIN ESB_LOG_TYPE LGT ON LGT.ID = LOG.TYPE_ID
                INNER JOIN ESB_TRACE TRC ON TRC.ID = LOG.TRACE_ID
                INNER JOIN ESB_CONVERSATION CNV ON CNV.ID = TRC.CONV_ID
            WHERE
              CNV.CONVERSATION_ID = p_CONVERSATION_ID AND
              LGT.ID = p_LOG_TYPE_ID
          )
        WHERE
          (p_LOG_OCCURENCE IS NULL OR RNUM=p_LOG_OCCURENCE);

        RETURN p_LOG_MESSAGE;

      EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-1',
              'There is no record with this info : [ConversationID]=' || p_CONVERSATION_ID || ' - [LogTypeName]=' || p_LOG_TYPE_NAME || ' .',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); -- To trigger retries, if requiered.
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [ConversationID]=' || p_CONVERSATION_ID || ' - [LogTypeName]=' || p_LOG_TYPE_NAME || ' .',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN '';
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN '';
  END GET_LOG_MESSAGE;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_LOG_SEVERITY_ID(
    p_LOG_SEVERITY_LEVEL      IN VARCHAR2,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  ) RETURN NUMBER
    IS

      v_LOG_SEVERITY_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The LogType was successfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.GET_LOG_TYPE_ID - INIT';

        SELECT
          ID
            INTO
              v_LOG_SEVERITY_ID
        FROM
          ESB_SEVERITY
        WHERE
          "LEVEL" = UPPER(p_LOG_SEVERITY_LEVEL) AND
          RCD_STATUS <> 2;

        RETURN v_LOG_SEVERITY_ID;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-1',
              'There is no record with this info : [LogSeverity]=' || p_LOG_SEVERITY_LEVEL || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [LogSeverity]=' ||  p_LOG_SEVERITY_LEVEL || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END GET_LOG_SEVERITY_ID;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_LOG_CONFIG(
    p_LOG_CONFIG         IN LOGMODE_T,
    p_LOG_TYPE_ID        OUT VARCHAR,
    p_LOG_SEVERITY_ID    OUT VARCHAR,
    p_CREATE_INVALID     IN CHAR,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  )
    IS

      BEGIN

        p_SOURCE_ERROR_DESC := 'Verifing the Log Config';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_CONFIG - INIT';

        /* ---------------------------------------------------------------------------------------
          Getting the Log Type.
        ---------------------------------------------------------------------------------------*/
        p_LOG_TYPE_ID := GET_LOG_TYPE_ID(
                                  p_LOG_CONFIG.LOGTYPE_,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                                );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE=-1) THEN
          -- Assuming the generic 'LOG' LogType
          p_LOG_TYPE_ID := GET_LOG_TYPE_ID(
                                    'LOG',
                                    p_Locator,
                                    p_SOURCE_ERROR_CODE,
                                    p_SOURCE_ERROR_DESC
                                  );
          IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        ELSE
          IF (p_SOURCE_ERROR_CODE!=0) THEN
            RETURN;
          END IF;
        END IF;
        ---------------------------------------------------------------------------------------

        /* ---------------------------------------------------------------------------------------
          Getting the Log Type.
        ---------------------------------------------------------------------------------------*/
        p_LOG_SEVERITY_ID := GET_LOG_SEVERITY_ID(
                                  p_LOG_CONFIG.LOGSEVERITY_,
                                  p_Locator,
                                  p_SOURCE_ERROR_CODE,
                                  p_SOURCE_ERROR_DESC
                                );

        -- Does it exist?.
        IF(p_SOURCE_ERROR_CODE=-1) THEN
          -- Assuming the generic 'LOG' LogType
          p_LOG_SEVERITY_ID := GET_LOG_SEVERITY_ID(
                                    'LOG',
                                    p_Locator,
                                    p_SOURCE_ERROR_CODE,
                                    p_SOURCE_ERROR_DESC
                                  );
          IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        ELSE
          IF (p_SOURCE_ERROR_CODE!=0) THEN
            RETURN;
          END IF;
        END IF;
        ---------------------------------------------------------------------------------------

      EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
  END VERIFY_LOG_CONFIG;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_LOG_METADATA(
    p_HEADER_TRACER      IN HEADERTRACER_T,
    p_CAPABILITY_ID      OUT NUMBER,
    p_CONVERSATION_ID    OUT NUMBER,
    p_TRANSACTION_ID     OUT NUMBER,
    p_CREATE_INVALID     IN CHAR,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  )
    IS

      BEGIN

        p_SOURCE_ERROR_DESC := 'Verifing the Log Metadata';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_LOGGERMANAGER_PKG.VERIFY_LOG_METADATA - INIT';

        VERIFY_SERVICE_CAPABILITY(
                  p_HEADER_TRACER.HEADER_.TRACE_.SERVICE_.NAME_,
                  p_HEADER_TRACER.HEADER_.TRACE_.SERVICE_.OPERATION_,
                  p_HEADER_TRACER.HEADER_.TRACE_.SERVICE_.CODE_,
                  p_CAPABILITY_ID,
                  p_CREATE_INVALID,
                  p_Locator,
                  p_SOURCE_ERROR_CODE,
                  p_SOURCE_ERROR_DESC
                );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        VERIFY_MESSAGE_TRANSACTION(
                  p_CAPABILITY_ID,
                  p_HEADER_TRACER.HEADER_.TRACE_.PROCESSID_,
                  p_HEADER_TRACER.HEADER_.TRACE_.EVENTID_,
                  p_HEADER_TRACER.HEADER_.TRACE_.CLIENTREQTIMESTAMP_,
                  p_HEADER_TRACER.HEADER_.TRACE_.CORRELATIONID_,
                  p_TRANSACTION_ID,
                  p_CREATE_INVALID,
                  p_Locator,
                  p_SOURCE_ERROR_CODE,
                  p_SOURCE_ERROR_DESC
                );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

        VERIFY_LOG_CONVERSATION(
            p_TRANSACTION_ID,
            p_HEADER_TRACER,
            p_CONVERSATION_ID,
            p_CAPABILITY_ID,
            p_CREATE_INVALID,
            p_Locator,
            p_SOURCE_ERROR_CODE,
            p_SOURCE_ERROR_DESC
          );
       IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN; END IF;

      EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
  END;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE LOG(
    LOGREQ IN LOG_T,
    REGISTERTIME IN TIMESTAMP DEFAULT NULL
  ) IS

    v_LOG_TYPE_ID     NUMBER;
    v_LOG_SEVERITY_ID NUMBER;


    v_CONVERSATION_ID NUMBER;
    v_TRANSACTION_ID NUMBER;

    v_CAPABILITY_ID NUMBER;

    v_TRACE_ID NUMBER;

    v_Locator VARCHAR(255);
    v_SOURCE_ERROR_CODE VARCHAR(50);
    v_SOURCE_ERROR_DESC VARCHAR(255);

    BEGIN

      v_Locator := 'ESB_LOGGERMANAGER_PKG.LOG - INIT';
      v_SOURCE_ERROR_CODE := '0' ;
      v_SOURCE_ERROR_DESC := '' ;

      VERIFY_LOG_METADATA (
          LOGREQ.HEADERTRACER_,
          v_CAPABILITY_ID,
          v_CONVERSATION_ID,
          v_TRANSACTION_ID,
          'T',
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
      );
      IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

      v_TRACE_ID := CREATE_LOG_TRACE(
          LOGREQ.HEADERTRACER_,
          LOGREQ.LOGPLACEHOLDER_,
          v_CONVERSATION_ID,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
      );
      IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

      VERIFY_LOG_CONFIG(
          LOGREQ.LOGMODE_,
          v_LOG_TYPE_ID,
          v_LOG_SEVERITY_ID,
          'T',
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
      );
      IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

      INSERT INTO ESB_LOG(
        ID,
        TRACE_ID,
        MESSAGE, TYPE_ID,
        SEVERITY_ID,
        DESCRIPTION
        )
      VALUES(
        ESB_LOG_SEQ.NEXTVAL,
        v_TRACE_ID,
        LOGREQ.MESSAGE_,
        v_LOG_TYPE_ID,
        v_LOG_SEVERITY_ID,
        LOGREQ.DESCRIPTION_
      );

      -----------------------------------------------------------------------------------------------------------------------
      -- Los siguientes statement tienen como proposito actualizar el estado de la transaccion en ESB_MESSAGE_TRANSACTION,
      -- en casos donde la dupla Servicio+Capacidad no sea configurada para aplicar validaciones sobre sus mensajes por el
      -- message manager. En estos casos, para optimizar el MessageManager, no se realizan tareas Sync para con las tablas
      -- de traza.
      --
      -- El LoggerManager esta preparado para este comportamiento y genera el registro en ESB_MESSAGE_TRANSACTION si no lo existe.
      --
      -- Ahora bien, para mantener la integridad de los registros existentes en dicha tabla, se debe tambien actualizar el estado
      -- cuando se informe la finalizacion de la Tx. la Tx. se considera finalizada para el cliente cuando la capacidad de
      -- secuencia "0" retorna un mensaje. Dicha capacidad se considera comola "Owner" de la transaccion al ser la que representa
      -- su ejecucion desde el punto del vista del cliente.
      --
      -- Por ello lo primero que hacemos en verificar que la operacion solicitada al LoggerManager sea LOG, y el tipo sea SRSP,
      -- indicando que se informa la respuesta de un Servicio (Capacidad). Luego verificamos si la capacidad que se esta tratando
      -- es la de sencuencia 0, y el estado de la Tx. sigue siendo "0" (Pending. De aqui asumimos la no participacion del
      -- MessageManager).
      -----------------------------------------------------------------------------------------------------------------------

      IF (LOGREQ.LOGMODE_.LOGTYPE_ = 'SRSP' OR LOGREQ.LOGMODE_.LOGTYPE_ = 'USRSP') THEN

        BEGIN

          SELECT
            T.ID
              INTO
                v_TRANSACTION_ID
          FROM
            ESB_CONVERSATION C
            INNER JOIN ESB_MESSAGE_TRANSACTION T
              ON C.MESSAGE_TX_ID = T.ID
            INNER JOIN ESB_TRACE TR
              ON TR.CONV_ID = C.ID
          WHERE
            TR.ID = v_TRACE_ID AND
            T.STATUS = '0' AND
            C.SEQUENCE='0';

          -- Si no salio por NO_DATA_FOUND, la capacidad es efectivamente la de secuencia 0, y la Tx. no fue actualizada por el
          -- MessageManager.
          --
          -- Subsiguientemente, actualizamos el estado de la Tx. a "1" ~ Finalizada.

          UPDATE
              ESB_MESSAGE_TRANSACTION T
          SET
              T.STATUS = '1'
          WHERE
            T.ID = v_TRANSACTION_ID;

        EXCEPTION

          WHEN NO_DATA_FOUND THEN
            NULL;
        END;
      END IF;
      --------------------------------------------------

  EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              v_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              v_SOURCE_ERROR_CODE,v_SOURCE_ERROR_DESC
            );
          ROLLBACK;
          RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC);
  END LOG;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  PROCEDURE ERROR(
    ERRORREQ IN ERROR_T,
    REGISTERTIME IN TIMESTAMP
  ) IS

    v_Locator VARCHAR(255);
    v_SOURCE_ERROR_CODE VARCHAR(50);
    v_SOURCE_ERROR_DESC VARCHAR(255);

    v_STATUS_ID NUMBER;
    v_CANONICAL_ERROR_ID NUMBER;
    v_SOURCE_ERROR_SYSTEM_ID NUMBER;

    v_CAPABILITY_ID NUMBER;
    v_CONVERSATION_ID NUMBER;
    v_TRANSACTION_ID NUMBER;

    v_TRACE_ID NUMBER;

  BEGIN

      v_Locator := 'ESB_LOGGERMANAGER_PKG.ERROR - INIT';
      v_SOURCE_ERROR_CODE := '0' ;
      v_SOURCE_ERROR_DESC := '' ;

      VERIFY_LOG_METADATA (
          ERRORREQ.HEADERTRACER_,
          v_CAPABILITY_ID,
          v_CONVERSATION_ID,
          v_TRANSACTION_ID,
          'F',
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
      );
      IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

      v_TRACE_ID := CREATE_LOG_TRACE(
          ERRORREQ.HEADERTRACER_,
          ERRORREQ.ERRORPLACEHOLDER_,
          v_CONVERSATION_ID,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
      );
      IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

    /* --------------------------------------------------------------------------------------------------
      Retrieving the Result State. If its not found, the 'ERROR' Result Statue used is used instead.
    */--------------------------------------------------------------------------------------------------
      v_STATUS_ID := ESB_ERROR_MANAGER_PKG.GET_RESULT_STATUS_ID(
                                  ERRORREQ.RESULT_.STATUS_,
                                  v_Locator,
                                  v_SOURCE_ERROR_CODE,
                                  v_SOURCE_ERROR_DESC);

      IF(v_SOURCE_ERROR_CODE!='0') THEN
          v_STATUS_ID := ESB_ERROR_MANAGER_PKG.GET_DEF_RESULT_S_ID_ERROR(
                                  v_Locator,
                                  v_SOURCE_ERROR_CODE,
                                  v_SOURCE_ERROR_DESC);
          IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;
      END IF;
    ------------------------------------------------------------------------------------------------------

    /* --------------------------------------------------------------------------------------------------
      Retrieving the Canonical Error. If its not found, the default CANONICAL ERROR used
      for those types of scenarios is used.
    */ --------------------------------------------------------------------------------------------------
      v_CANONICAL_ERROR_ID := ESB_ERROR_MANAGER_PKG.GET_CAN_ERR_ID(
                                                        ERRORREQ.RESULT_.CANONICALERROR_.CODE_,
                                                        ERRORREQ.RESULT_.CANONICALERROR_.TYPE_,
                                                        v_Locator,
                                                        v_SOURCE_ERROR_CODE,
                                                        v_SOURCE_ERROR_DESC
                                                      );

      IF(v_SOURCE_ERROR_CODE!='0') THEN
        v_CANONICAL_ERROR_ID := ESB_ERROR_MANAGER_PKG.GET_DEF_CAN_ERR_ID_NO_DATA(
                                                        v_Locator,
                                                        v_SOURCE_ERROR_CODE,
                                                        v_SOURCE_ERROR_DESC
                                                      );
        IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;
      END IF;
    ------------------------------------------------------------------------------------------------------

    /* ---------------------------------------------------------------------------------------
            Getting the Source Error System.
    ---------------------------------------------------------------------------------------*/
    v_SOURCE_ERROR_SYSTEM_ID  := ESB_COMMONS_PKG.GET_SYSTEM_ID(
                                  ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_,
                                  v_Locator,
                                  v_SOURCE_ERROR_CODE,
                                  v_SOURCE_ERROR_DESC
                                );
    -- Does it exist?.
    IF(v_SOURCE_ERROR_CODE=-1) THEN

      v_SOURCE_ERROR_SYSTEM_ID := ESB_COMMONS_PKG.CREATE_SYSTEM(
                                ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_,
                                'UNK_'|| ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_,
                                'UNK SourceErrorSystem.',
                                0,
                                v_Locator,
                                v_SOURCE_ERROR_CODE,
                                v_SOURCE_ERROR_DESC
                            );
      IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

    ELSE
      IF(v_SOURCE_ERROR_CODE=-4) THEN -- It's logically deleted.

        -- Updating System with RCD_STATUS = 0 (Invalid).
        UPDATE
            ESB_SYSTEM
        SET
            RCD_STATUS = 0
        WHERE
          CODE = ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.SOURCE_;

      ELSE

        IF (v_SOURCE_ERROR_CODE!=0) THEN
          RETURN;
        END IF;

      END IF;
    END IF;

    ------------------------------------------------------------------------------------------------------

    INSERT INTO ESB_ERROR(
      ID,
      TRACE_ID,
      STATUS_ID,
      DESCRIPTION,
      CAN_ERR_ID,
      RAW_FAULT,
      RAW_CODE,
      RAW_DESCRIPTION,
      ERROR_SOURCE,
      ERROR_SOURCE_DETAILS,
      MODULE,
      SUB_MODULE,
      RCD_STATUS
    )
    VALUES(
      ESB_ERROR_SEQ.NEXTVAL,
      v_TRACE_ID,
      v_STATUS_ID,
      ERRORREQ.RESULT_.DESCRIPTION_,
      v_CANONICAL_ERROR_ID,
      ERRORREQ.RESULT_.SOURCEERROR_.SOURCEFAULT_,
      ERRORREQ.RESULT_.SOURCEERROR_.CODE_,
      ERRORREQ.RESULT_.SOURCEERROR_.DESCRIPTION_,
      v_SOURCE_ERROR_SYSTEM_ID,
      ERRORREQ.RESULT_.SOURCEERROR_.ERRORSOURCEDETAILS_.DETAILS,
      ERRORREQ.ERRORINDEX_.MODULE_,
      ERRORREQ.ERRORINDEX_.SUBMODULE_,
      1
    );

    EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              v_Locator,
              '-3',
              'Unexpected Error.',
              SQLERRM,
              v_SOURCE_ERROR_CODE,v_SOURCE_ERROR_DESC
            );
          ROLLBACK;
          RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC);

  END ERROR;

END ESB_LOGGERMANAGER_PKG;
