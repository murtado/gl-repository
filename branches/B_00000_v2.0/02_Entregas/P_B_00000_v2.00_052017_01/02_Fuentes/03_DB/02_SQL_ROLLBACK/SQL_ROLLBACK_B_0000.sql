SPOOL salida_B_0000.txt

------------------------- TABLES -------------------------
@.\01_Scripts\01_SQL_Tables\ALTER_TABLE_ESB_CONSUMER.sql
@.\01_Scripts\01_SQL_Tables\ALTER_TABLE_ESB_ERR_CMP_CFG.sql
@.\01_Scripts\01_SQL_Tables\ALTER_TABLE_ESB_ERR_CMP.sql
@.\01_Scripts\01_SQL_Tables\ALTER_TABLE_ESB_CONSUMER_CAP_DETAILS_TYPE.sql
@.\01_Scripts\01_SQL_Tables\ALTER_TABLE_ESB_CONSUMER_DETAILS_TYPE.sql
@.\01_Scripts\01_SQL_Tables\ALTER_TABLE_ESB_COUNTRY.sql

------------------------- TYPES -------------------------

------------------------- SEQUENCES -------------------------

------------------------- PACKAGES -------------------------
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_LOGGERMANAGER_PKG_SPEC.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_LOGGERMANAGER_PKG_BODY.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_MESSAGEMANAGER_PKG_SPEC.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_MESSAGEMANAGER_PKG_BODY.sql

------------------------- INDEXES -------------------------
@.\01_Scripts\08_SQL_Indexes\DROP_ALL_INDEXES.sql


------------------------- DATA -------------------------

SPOOL OFF;
