create or replace PACKAGE ESB_LOGGERMANAGER_PKG
AS
  PROCEDURE LOG(
      LOGREQ       IN LOG_T,
      REGISTERTIME IN TIMESTAMP DEFAULT NULL);
  PROCEDURE ERROR(
      ERRORREQ     IN ERROR_T,
      REGISTERTIME IN TIMESTAMP);
  FUNCTION GET_LOG_MESSAGE(
    p_CONVERSATION_ID             IN VARCHAR2,
    p_LOG_TYPE_NAME               IN VARCHAR2,
    p_LOG_OCCURENCE               IN NUMBER,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )  RETURN CLOB;
END ESB_LOGGERMANAGER_PKG;
/