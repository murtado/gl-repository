import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000_WM'
wlstFileDomain='OSB'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

def debug(message):
	debugFile.write(message)
	print message

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debug('\n----------------------------------------------------------------------------------')
	debug('\n--- Starting the Activate Session ---')
	debug('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debug('\n')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n--- Ending the Activate Session (SUCCESS) ---')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n All the tasks were completed Sucessfully')
		debug('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debug('\n')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n--- Ending the Activate Session (ERROR) ---')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n There was an error excecution the following function : ' + fName)
		debug('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

	disconnect()
	exit()

""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------------- """
""" Delete FRW WorkManagers """
""" ----------------------- """
def deleteFRW_WorkManager(name):
	global fName
	global wlstVersion

	fName = 'deleteFRW_WorkManager'
	debug('\n')
	debug('\nStarting the Execution of Function : ' + fName)
	debug('\n')

	wmName='FRW_' + name + '_WM'
	mntcWmName='FRW_' + name + '_WM_MNTC'
	mxtcWmName='FRW_' + name + '_WM_MXTC'

	debug('DELETING WORK MANAGER with [NAME = ' + wmName + '] AND [MNTC = ' + mntcWmName + '] AND [MXTC = ' + mxtcWmName + '].')
	debug('\n')

	# ------------------------------------------------------------------------
	cd('/SelfTuning/' + OSB_DomName + '/WorkManagers/')
	if (getMBean(wmName) == None):
			debug('NON EXISTENT WORK MANAGER with [NAME = ' + wmName + ']')
	else:
		delete(wmName,'WorkManagers')
		debug('DELETED [' + wmName + '].')

	# ------------------------------------------------------------------------
	cd('../MaxThreadsConstraints/')
	if (getMBean(mxtcWmName) == None):
			debug('NON EXISTENT MNTC with [NAME = ' + mxtcWmName + ']')
	else:
		delete(mxtcWmName,'MaxThreadsConstraints')
		debug('DELETED [' + mntcWmName + '].')

	# ------------------------------------------------------------------------
	cd('../MinThreadsConstraints/')
	if (getMBean(mntcWmName) == None):
		debug('NON EXISTENT MXTC with [NAME = ' + mntcWmName + ']')
	else:
		delete(mntcWmName,'MinThreadsConstraints')
		debug('DELETED [' + mxtcWmName + '].')

	debug('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	deleteFRW_WorkManager('ConversationManager_AC')
	deleteFRW_WorkManager('PublishResult_MEM_Redirection')
	deleteFRW_WorkManager('RoutingManager_RSP_Ignore')
	deleteFRW_WorkManager('LoggerManager_AC_Discarded')
	deleteFRW_WorkManager('LoggerManager_AC')
	deleteFRW_WorkManager('LoggerManager_AP')
	deleteFRW_WorkManager('ConversationManager_AP')
	deleteFRW_WorkManager('CorrelationManager_AC')
	deleteFRW_WorkManager('CorrelationManager_AC_EXP')
	deleteFRW_WorkManager('CacheManager_Get')

	""" @@@ """

	validate()
	endEditSession('OK')

except:
	dumpStack()
	endEditSession('ERROR')
