--------------------------------------------------------
--  DDL for Table ESB_MESSAGE_TRANSACTION
--------------------------------------------------------

  ALTER TABLE "ESB_MESSAGE_TRANSACTION"
  ADD
    (
      SOURCE_ID VARCHAR2(255),
      CORRELATION_EVENT_ID VARCHAR2(255)
    );
