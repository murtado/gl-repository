SPOOL salida_B_0000.txt

------------------------- TABLES -------------------------
@.\01_Scripts\01_SQL_Tables\ALTER_TABLE_ESB_MESSAGE_TRANSACTION.sql
------------------------- TYPES -------------------------

------------------------- SEQUENCES -------------------------

------------------------- PACKAGES ------------------------
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_CONVERSATION_MANAGER_PKG_BODY.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_MESSAGEMANAGER_PKG_SPEC.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_MESSAGEMANAGER_PKG_BODY.sql
@.\01_Scripts\04_SQL_Packages\CREATE_ESB_LOGGERMANAGER_PKG_BODY.sql
------------------------- INDEXES -------------------------

------------------------- DATA -------------------------

SPOOL OFF;
