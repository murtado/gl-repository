create or replace PACKAGE BODY ESB_MESSAGEMANAGER_PKG AS

/*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_TX_BY_CONV_ID(
    p_CONV_ID                     IN VARCHAR2,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )
    RETURN NUMBER
    IS

      v_TX_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.GET_TX_BY_CONV_ID - INIT';

          SELECT C.MESSAGE_TX_ID
            INTO v_TX_ID
          FROM
            ESB_CONVERSATION C
          WHERE
            C.CONVERSATION_ID = p_CONV_ID AND
            C.RCD_STATUS <> 2;

        RETURN v_TX_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Conversation Id]='    || p_CONV_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Conversation Id]='    || p_CONV_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_TX_BY_CONV_ID;

   /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_TX_OWNER_ID(
    p_TX_ID                       IN NUMBER,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )
    RETURN NUMBER
    IS

      v_CONVERSATION_ID_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.GET_TX_OWNER_ID - INIT';

          SELECT C.ID
            INTO v_CONVERSATION_ID_ID
          FROM
            ESB_CONVERSATION C
          WHERE
            C.MESSAGE_TX_ID = p_TX_ID AND
            C.SEQUENCE='0' AND
            C.RCD_STATUS <> 2;

        RETURN v_CONVERSATION_ID_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Transaction Id]='    || p_TX_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Transaction Id]='    || p_TX_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
  END GET_TX_OWNER_ID;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_TX_OWNER(
    p_TX_ID                       IN NUMBER,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )
    RETURN VARCHAR2
    IS

      v_CONVERSATION_ID VARCHAR2(50);

      BEGIN

        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.GET_TX_OWNER - INIT';

          SELECT C.CONVERSATION_ID
            INTO v_CONVERSATION_ID
          FROM
            ESB_CONVERSATION C
          WHERE
            C.MESSAGE_TX_ID = p_TX_ID AND
            C.SEQUENCE='0' AND
            C.RCD_STATUS <> 2;

        RETURN v_CONVERSATION_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Transaction Id]='    || p_TX_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Transaction Id]='    || p_TX_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
  END GET_TX_OWNER;

/*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_TX_OWNER_BY_CONV_ID(
    p_CONV_ID                     IN VARCHAR2,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )
    RETURN VARCHAR2
    IS

      v_TX_ID           NUMBER;
      v_CONVERSATION_ID VARCHAR2(50);

      BEGIN

        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.GET_TX_OWNER_BY_CONV_ID - EXEC - ESB_MESSAGEMANAGER_PKG.GET_TX_BY_CONV_ID';

        v_TX_ID := GET_TX_BY_CONV_ID(
          p_CONV_ID,
          p_Locator,
          p_SOURCE_ERROR_CODE,
          p_SOURCE_ERROR_DESC
        );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

        v_CONVERSATION_ID := GET_TX_OWNER(
          v_TX_ID,
          p_Locator,
          p_SOURCE_ERROR_CODE,
          p_SOURCE_ERROR_DESC
        );
        IF(p_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

        RETURN v_CONVERSATION_ID;

      EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN NULL;
  END GET_TX_OWNER_BY_CONV_ID;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION UPDATE_TRANSACTION_STATUS(
    p_EVENT_ID             IN VARCHAR2,
    p_PROC_ID              IN VARCHAR2,
    p_CORRELATION_ID       IN VARCHAR2,
    p_CLIENT_REQ_TIMESTAMP IN VARCHAR2, --NOT USED
    p_CAPABILITY_ID        IN NUMBER,
    p_NEWSTATUS            IN NUMBER,
    p_Locator              OUT VARCHAR,
    p_SOURCE_ERROR_CODE    OUT VARCHAR,
    p_SOURCE_ERROR_DESC    OUT VARCHAR
  ) RETURN NUMBER
    IS
      v_MESSAGE_TRANSACTION_ID NUMBER;
      BEGIN

        p_SOURCE_ERROR_DESC := 'Status successfully updated.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.UPDATE_TRANSACTION_STATUS - INIT';

        UPDATE ESB_MESSAGE_TRANSACTION MT
        SET MT.STATUS     = p_NEWSTATUS
        --Debido a que la comparacion con un valor si es '', osea NULL, no se puede realizar con el comparador de igual =
        --Se utiliza la funcion COALESCE para para no agregar conplejidad al evaluar de forma diferente cuando se trata con valor nulos
        WHERE
          MT.EVENT_ID = p_EVENT_ID AND
          COALESCE(MT.PROC_ID, './*@*/*@')        = COALESCE(p_PROC_ID, './*@*/*@') AND
          COALESCE(MT.CORRELATION_ID, './*@*/*@') = COALESCE(p_CORRELATION_ID , './*@*/*@')

        RETURNING MT."ID"
        INTO v_MESSAGE_TRANSACTION_ID;

        RETURN v_MESSAGE_TRANSACTION_ID;

      EXCEPTION
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
    END;


  /*---------------------------------------------------------------------------------------------------
    ************** DEPRECATED ******************

    ****

    OK Outcome      -->
    ERROR           -->

  */---------------------------------------------------------------------------------------------------

   PROCEDURE UPDATETRANSACTION
    (
      p_EVENT_ID             IN VARCHAR2,
      p_PROC_ID              IN VARCHAR2,
      p_CORRELATION_ID       IN VARCHAR2,
      p_CLIENT_REQ_TIMESTAMP IN VARCHAR2,
      p_CAPABILITY_ID        IN NUMBER,
      p_NEWSTATUS            IN NUMBER,
      RESULT_ID OUT NUMBER
    )
    IS
      v_Locator VARCHAR(255);
      v_SOURCE_ERROR_CODE VARCHAR(50);
      v_SOURCE_ERROR_DESC VARCHAR(255);

      v_MESSAGE_TRANSACTION_ID NUMBER;

      BEGIN
        v_MESSAGE_TRANSACTION_ID := UPDATE_TRANSACTION_STATUS(
          p_EVENT_ID,
          p_PROC_ID,
          p_CORRELATION_ID,
          p_CLIENT_REQ_TIMESTAMP,
          p_CAPABILITY_ID,
          p_NEWSTATUS,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
        );
        IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

        RESULT_ID := v_MESSAGE_TRANSACTION_ID;

      END UPDATETRANSACTION;


    /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_MESSAGE_TRANSACTION(
    p_EVENT_ID              IN VARCHAR2,
    p_PROC_ID               IN VARCHAR2,
    p_STATUS                IN NUMBER,
    p_RCD_STATUS            IN NUMBER,
    p_CLIENT_REQ_TIMESTAMP  IN VARCHAR2,
    p_CORRELATION_ID        IN VARCHAR2,
    p_Locator               OUT VARCHAR,
    p_SOURCE_ERROR_CODE     OUT VARCHAR,
    p_SOURCE_ERROR_DESC     OUT VARCHAR
  ) RETURN NUMBER

    IS
      v_MESSAGE_TRANSACTION_ID NUMBER;
      BEGIN

        p_SOURCE_ERROR_DESC := 'MESSAGE_TRANSACTION successfully created.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.CREATE_MESSAGE_TRANSACTION - INIT';

        INSERT INTO ESB_MESSAGE_TRANSACTION
        (
          ID,
          PROC_ID,
          EVENT_ID,
          STATUS,
          RCD_STATUS,
          CLIENT_REQ_TIMESTAMP,
          MSG_TIMESTAMP,
          "SEQUENCE",
          CORRELATION_ID
        )
        VALUES
        (
          ESB_MESSAGE_TRANSACTION_SEQ.NEXTVAL,
          p_PROC_ID,
          p_EVENT_ID,
          p_STATUS,
          p_RCD_STATUS,
          TO_TIMESTAMP_TZ(p_CLIENT_REQ_TIMESTAMP,'YYYY-MM-DD HH24:MI:SS:FF TZH:TZM'),
          CURRENT_TIMESTAMP,
          --Si no encuentra una transaccion a partir de PROC_ID + EVENT_ID + CORRELATION_ID, siendo un segmento e la TX,
          --se genera una transacion con SEQUENCE + 1 del PROC_ID + EVENT_ID (Si no encuentra transaccion, va a generar la de SEQUENCE = 0)
          (
            SELECT COUNT(*)
            FROM ESB_MESSAGE_TRANSACTION MT
            --Debido a que la comparacion con un valor si es '', osea NULL, no se puede realizar con el comparador de igual =
            --Se utiliza la funcion COALESCE para para no agregar conplejidad al evaluar de forma diferente cuando se trata con valor nulos
            WHERE COALESCE(MT.PROC_ID, './*@*/*@') = COALESCE(p_PROC_ID, './*@*/*@') AND
                  EVENT_ID = p_EVENT_ID AND
                  MT.RCD_STATUS <> 2
          ),
          p_CORRELATION_ID
        )
        RETURNING "ID"
        INTO v_MESSAGE_TRANSACTION_ID;

        RETURN v_MESSAGE_TRANSACTION_ID;

      EXCEPTION
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Event Id]='              || p_EVENT_ID             ||
                                                ' - [Process Id]='            || p_PROC_ID              ||
                                                ' - [Status Id]='             || p_STATUS               ||
                                                ' - [RCD Status]='            || p_RCD_STATUS           ||
                                                ' - [Client REQ Timestamp]='  || p_CLIENT_REQ_TIMESTAMP ||
                                                ' - [Correlation Id]='        || p_CORRELATION_ID       || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
    END;

 /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_TRANSACTION_CHECK(
    p_MESSAGE_TX_ID      IN NUMBER,
    p_CONVERSATION_ID    IN VARCHAR2,
    p_CAPABILITY_ID      IN NUMBER,
    p_RCD_STATUS         IN NUMBER,
    p_Locator            OUT VARCHAR,
    p_SOURCE_ERROR_CODE  OUT VARCHAR,
    p_SOURCE_ERROR_DESC  OUT VARCHAR
  ) RETURN NUMBER
    IS
      v_TRANSACTION_CHECK_ID NUMBER;
      BEGIN

        p_SOURCE_ERROR_DESC := 'TRANSACTION_CHECK successfully created.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.CREATE_TRANSACTION_CHECK - INIT';

        INSERT INTO ESB_TRANSACTION_CHECK
        (
          ID,
          MESSAGE_TX_ID,
          CONVERSATION_ID,
          CAPABILITY_ID,
          RCD_STATUS
        )
        VALUES
        (
          ESB_TRANSACTION_CHECK_SEQ.NEXTVAL,
          p_MESSAGE_TX_ID,
          p_CONVERSATION_ID,
          p_CAPABILITY_ID,
          p_RCD_STATUS
        )
        RETURNING "ID"
        INTO v_TRANSACTION_CHECK_ID;

        RETURN v_TRANSACTION_CHECK_ID;

      EXCEPTION
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Tx Id]='           || p_MESSAGE_TX_ID   ||
                                                  ' [Conversation Id]=' || p_CONVERSATION_ID ||
                                                  ' [Capability Id]='   || p_CAPABILITY_ID   ||
                                                  ' [RCD_STATUS]='      || p_RCD_STATUS      || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
    END;


  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      -->
    ERROR           -->

  */---------------------------------------------------------------------------------------------------

  PROCEDURE REGISTRYTRANSACTION(
      p_EVENT_ID             IN VARCHAR2,
      p_PROC_ID              IN VARCHAR2,
      p_CLIENT_REQ_TIMESTAMP IN VARCHAR2,
      p_CAPABILITY_ID        IN NUMBER,
      p_CONVERSATION_ID      IN VARCHAR2,
      p_CORRELATION_ID       IN VARCHAR2,
      RESULT_ID OUT NUMBER
  )
    IS
      v_Locator VARCHAR(255);
      v_SOURCE_ERROR_CODE VARCHAR(50);
      v_SOURCE_ERROR_DESC VARCHAR(255);

      v_MESSAGE_TRANSACTION_ID NUMBER;
      v_TRANSACTION_CHECK_ID NUMBER;

      BEGIN

        v_MESSAGE_TRANSACTION_ID := CREATE_MESSAGE_TRANSACTION(
          p_PROC_ID,
          p_EVENT_ID,
          0,
          1,
          TO_TIMESTAMP_TZ(p_CLIENT_REQ_TIMESTAMP,'YYYY-MM-DD HH24:MI:SS TZH:TZM'),
          p_CORRELATION_ID,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
        );
        IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

        v_TRANSACTION_CHECK_ID := CREATE_TRANSACTION_CHECK(
          v_MESSAGE_TRANSACTION_ID,
          p_CONVERSATION_ID,
          p_CAPABILITY_ID,
          1,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
        );
        IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

        RESULT_ID := v_MESSAGE_TRANSACTION_ID;
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          RESULT_ID:= -1;
          ROLLBACK;
  END REGISTRYTRANSACTION;


  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> RESULT_ID
    ERROR           --> -20100 :: Error insertando transaccion

  */---------------------------------------------------------------------------------------------------

  PROCEDURE REGISTRYTRANSACTIONCHECK(
      p_MESSAGE_TX_ID   IN NUMBER,
      p_CONVERSATION_ID IN VARCHAR2,
      p_RESPONSE_MSG    IN CLOB,
      p_RESULT_CODE     IN VARCHAR2,
      p_RESULT_DESC     IN VARCHAR2,
      RESULT_ID OUT NUMBER)
  IS
  BEGIN

    UPDATE ESB_TRANSACTION_CHECK
    SET
      CONVERSATION_ID = p_CONVERSATION_ID,
      RSP_MSG           = p_RESPONSE_MSG,
      RESULT_CODE       = p_RESULT_CODE,
      RESULT_DESC       = p_RESULT_DESC
    WHERE MESSAGE_TX_ID = p_MESSAGE_TX_ID RETURNING "ID"
    INTO RESULT_ID;

    EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20100,'Error insertando transaccion');
  END REGISTRYTRANSACTIONCHECK;


 /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      -->
    ERROR           -->

  */---------------------------------------------------------------------------------------------------


  FUNCTION GET_TRANSACTION_INFO(
    p_EVENT_ID              IN VARCHAR2,
    p_PROC_ID               IN VARCHAR2,
    p_Locator               OUT VARCHAR,
    p_SOURCE_ERROR_CODE     OUT VARCHAR,
    p_SOURCE_ERROR_DESC     OUT VARCHAR
  ) RETURN SYS_REFCURSOR

  IS
    MESSAGE_TRANSACTION SYS_REFCURSOR;

    BEGIN

      p_SOURCE_ERROR_DESC := 'Transaction info succesfully retrieved.';
      p_SOURCE_ERROR_CODE := '0';
      p_Locator := 'ESB_MESSAGEMANAGER_PKG.GET_TRANSACTION_INFO - INIT';

      OPEN MESSAGE_TRANSACTION FOR
        SELECT
          MT."ID",
          MT.STATUS,
          MT.MSG_TIMESTAMP,
          TC.CAPABILITY_ID,
          MT."SEQUENCE",
          MT.CORRELATION_ID
        FROM ESB_MESSAGE_TRANSACTION MT
          INNER JOIN ESB_TRANSACTION_CHECK TC
            ON MT.ID = TC.MESSAGE_TX_ID
        WHERE
          EVENT_ID = p_EVENT_ID AND
          COALESCE(MT.PROC_ID, './*@*/*@') = COALESCE(p_PROC_ID, './*@*/*@') AND
          MT.RCD_STATUS = 1
        ORDER BY MT."SEQUENCE" DESC;

    RETURN MESSAGE_TRANSACTION;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
          p_Locator,
          '-1',
          'No records matching this info : [Event Id]=' || p_EVENT_ID ||
                                          ' [Process Id]=' || p_PROC_ID   || '.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN MESSAGE_TRANSACTION;
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN MESSAGE_TRANSACTION;

  END GET_TRANSACTION_INFO;


  /*---------------------------------------------------------------------------------------------------
    *********** DEPRECATED ***********
    ****

    OK Outcome      -->
    ERROR           -->

  */---------------------------------------------------------------------------------------------------


  PROCEDURE GETTRANSACTIONINFO(
      p_EVENT_ID             IN VARCHAR2,
      p_PROC_ID              IN VARCHAR2,
      p_CLIENT_REQ_TIMESTAMP IN TIMESTAMP WITH TIME ZONE, --NOT USED
      p_CAPABILITY_ID        IN NUMBER, --NOT USED
      p_CORRELATION_ID       IN VARCHAR2, --NOT USED
      MESSAGE_TRANSACTION OUT SYS_REFCURSOR
  )
    IS
      v_Locator VARCHAR2(255);
      v_SOURCE_ERROR_CODE VARCHAR2(50);
      v_SOURCE_ERROR_DESC VARCHAR2(255);

      BEGIN
        MESSAGE_TRANSACTION := GET_TRANSACTION_INFO(
          p_EVENT_ID,
          p_PROC_ID,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
        );

      END GETTRANSACTIONINFO;

/*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_MESSAGE_TRANSACTION_ID(
    p_EVENT_ID                    IN VARCHAR2,
    p_PROC_ID                     IN VARCHAR2,
    p_CORRELATION_ID              IN VARCHAR2,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )
    RETURN NUMBER
    IS

      v_MESSAGE_TRANSACTION_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_MESSAGEMANAGER_PKG.GET_MESSAGE_TRANSACTION_ID - INIT';

        SELECT M.ID
        INTO v_MESSAGE_TRANSACTION_ID
        FROM ESB_MESSAGE_TRANSACTION M
        WHERE
          COALESCE(M.PROC_ID, './*@*@') = COALESCE(p_PROC_ID, './*@*@') AND
          COALESCE(M.EVENT_ID, './*@*@')  = COALESCE(p_EVENT_ID, './*@*@') AND
          COALESCE(M.CORRELATION_ID, './*@*@') = COALESCE(p_CORRELATION_ID, './*@*@') AND
          M.RCD_STATUS <> 2 AND
          /*
            Este OrderBY y ROWNUM se estabece por la (remota) posibilidad de que para una misma combinacion de ProcessID+EventID+CorrelationID
            se encuentren mas de un registro.

            Se debe asumir que el ProcessID+EventID no son modificados una vez son recibidos de un cliente externo, y el CorrelationID solo es
            modificado por el FRW durante los re-procesos ejecutados por el ErrorHospital.

            Cada re-proceso (reintento por el ErrorHospital) va a tener un CorrelationID distinto.

            El CorrelationID no debiera ser modificado por ningun otro actor que el ErrorHospital; de ser modificado, podria ocurrir un comportamiento inesperado.
          */
          ROWNUM=1
        ORDER BY M.MSG_TIMESTAMP;

        RETURN v_MESSAGE_TRANSACTION_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Process Id]='    || p_PROC_ID           ||
                                             ' - [Event Id]='      || p_EVENT_ID          ||
                                             ' - [Correlation Id]='|| p_CORRELATION_ID    || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [Process Id]='    || p_PROC_ID           ||
                                                ' - [Event Id]='      || p_EVENT_ID          ||
                                                ' - [Correlation Id]='|| p_CORRELATION_ID    || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_MESSAGE_TRANSACTION_ID;




  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> CHECK_MESSAGE, CAPABILITY_ID with data
    UNHANDLED ERROR --> CHECK_MESSAGE = -1 | CAPABILITY_ID undefined

  */---------------------------------------------------------------------------------------------------

  PROCEDURE GETCAPABILITYCHECKSTATUS(
      p_SERVICE_CODE      IN VARCHAR2,
      p_SERVICE_NAME      IN VARCHAR2,
      p_SERVICE_OPERATION IN VARCHAR2,
      CHECK_MESSAGE       OUT NUMBER,
      CAPABILITY_ID       OUT NUMBER
  )
  IS
    BEGIN

      SELECT CC.CHECK_INDICATOR, C."ID"
        INTO CHECK_MESSAGE, CAPABILITY_ID
      FROM ESB_SERVICE S
        INNER JOIN ESB_CAPABILITY C         ON C.SERVICE_ID     = S.ID
        INNER JOIN ESB_CAPABILITY_CHECK CC  ON CC.CAPABILITY_ID = C.ID
      WHERE S.CODE          = p_SERVICE_CODE
        AND S."NAME"        = p_SERVICE_NAME
        AND C."NAME"        = p_SERVICE_OPERATION
        AND C.RCD_STATUS    = 1
        AND S.RCD_STATUS    = 1
        AND CC.RCD_STATUS   = 1;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        CHECK_MESSAGE := -1;
  END GETCAPABILITYCHECKSTATUS;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      --> SYS_REFCURSOR with data
    ERROR           --> EMPTY SYS_REFCURSOR

  */---------------------------------------------------------------------------------------------------

  PROCEDURE GETCAPABILITYCHECKS(
      p_SERVICE_CODE      IN VARCHAR2,
      p_SERVICE_NAME      IN VARCHAR2,
      p_SERVICE_OPERATION IN VARCHAR2,
      CHECK_TYPES         OUT SYS_REFCURSOR
  )
  IS
    BEGIN
      OPEN CHECK_TYPES FOR
        SELECT
          CT.ID,
          CT."NAME",
          CT.GRADE,
          TO_CHAR(CCO.ELAPSED_TIME) AS ELAPSED_TIME,
          3 AS TYPE
        FROM ESB_SERVICE S
          INNER JOIN ESB_CAPABILITY C         ON C.SERVICE_ID       = S.ID
          INNER JOIN ESB_CAPABILITY_CHECK CC  ON CC.CAPABILITY_ID   = C.ID
          INNER JOIN ESB_CHECK_CONFIG CCO     ON CCO.CAP_CHK_ID     = CC.ID
          INNER JOIN ESB_CHECK_TYPE CT        ON CT.ID              = CCO.CHECK_TYPE_ID
        WHERE S.CODE = p_SERVICE_CODE
          AND S."NAME" = p_SERVICE_NAME
          AND C."NAME" = p_SERVICE_OPERATION
          AND C.RCD_STATUS = 1
          AND S.RCD_STATUS = 1
          AND CC.RCD_STATUS = 1
          AND CCO.RCD_STATUS = 1
          AND CT.RCD_STATUS = 1
        ORDER BY CT.GRADE DESC;

    END GETCAPABILITYCHECKS;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      -->
    ERROR           -->

  */---------------------------------------------------------------------------------------------------


  PROCEDURE GETDEFAULTCHECKS(
      p_CHECK_TYPE IN NUMBER,
      CHECK_TYPES OUT SYS_REFCURSOR)
  IS
  BEGIN
    OPEN CHECK_TYPES FOR
      SELECT
        "ID",
        "NAME",
        GRADE,
        TO_CHAR(ELAPSED_TIME) AS ELAPSED_TIME,
        "TYPE"
      FROM ESB_DEFAULT_CHECK
      WHERE
        "TYPE" = p_CHECK_TYPE AND
        RCD_STATUS = 1
      ORDER BY "TYPE", GRADE DESC;

  END GETDEFAULTCHECKS;

  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      -->
    ERROR           -->

  */---------------------------------------------------------------------------------------------------

  PROCEDURE GETMESSAGESTATUS(
      p_EVENT_ID             IN VARCHAR2,
      p_PROC_ID              IN VARCHAR2,
      p_CLIENT_REQ_TIMESTAMP IN TIMESTAMP WITH TIME ZONE,
      p_CAPABILITY_ID        IN NUMBER,
      MESSAGE_TRANSACTION OUT SYS_REFCURSOR)
  IS
  BEGIN
    OPEN MESSAGE_TRANSACTION FOR
      SELECT
        MT."ID",
        MT.PROC_ID,
        MT.EVENT_ID,
        TO_CHAR(MT.CLIENT_REQ_TIMESTAMP) CLIENT_REQ_TIMESTAMP,
        MT.STATUS,
        MT.MSG_TIMESTAMP,
        MT."SEQUENCE",
        TC.CAPABILITY_ID,
        MT.CREATIONDATE
      FROM ESB_MESSAGE_TRANSACTION MT
        INNER JOIN ESB_TRANSACTION_CHECK TC ON MT.ID = TC.MESSAGE_TX_ID
      WHERE
        MT.EVENT_ID = p_EVENT_ID AND
        COALESCE(MT.PROC_ID, './*@*/*@') = COALESCE(p_PROC_ID, './*@*/*@') AND
        MT.CLIENT_REQ_TIMESTAMP = p_CLIENT_REQ_TIMESTAMP AND
        TC.CAPABILITY_ID = p_CAPABILITY_ID AND
        MT.RCD_STATUS = 1
      ORDER BY MT."SEQUENCE" ASC;
  END GETMESSAGESTATUS;


  /*---------------------------------------------------------------------------------------------------

    ****

    OK Outcome      -->
    ERROR           -->

  */---------------------------------------------------------------------------------------------------

  PROCEDURE GETCHECKTRANSACTION(
      p_EVENT_ID             IN VARCHAR2,
      p_PROC_ID              IN VARCHAR2,
      p_CLIENT_REQ_TIMESTAMP IN TIMESTAMP WITH TIME ZONE, --NOT USED
      p_CAPABILITY_ID        IN NUMBER,
      TRANSACTION_CHECK OUT SYS_REFCURSOR)
  IS
  BEGIN
    OPEN TRANSACTION_CHECK FOR
      SELECT MT.PROC_ID,
        MT.EVENT_ID,
        TO_CHAR(MT.CLIENT_REQ_TIMESTAMP) CLIENT_REQ_TIMESTAMP,
        MT.STATUS,
        TC.CONVERSATION_ID,
        TC.RSP_MSG,
        TC.RESULT_CODE,
        TC.RESULT_DESC
      FROM ESB_MESSAGE_TRANSACTION MT
        INNER JOIN ESB_TRANSACTION_CHECK TC ON TC.MESSAGE_TX_ID = MT.ID
      --Debido a que la comparacion con un valor si es '', osea NULL, no se puede realizar con el comparador de igual =
      --Se utiliza la funcion COALESCE para para no agregar conplejidad al evaluar de forma diferente cuando se trata con valor nulos
      WHERE
        MT.EVENT_ID = p_EVENT_ID AND
        COALESCE(MT.PROC_ID, './*@*/*@') = COALESCE(p_PROC_ID, './*@*/*@') AND
        --MT.CLIENT_REQ_TIMESTAMP = p_CLIENT_REQ_TIMESTAMP AND ||| Sacamos esto para evitar tomar el CLIENT_REQ_TIMESTAMP como clave para determinar un mensaje duplicado (en este caso para que encuentre el registro,
        --. Si el flujo desde OSB llega a este punto, significa que se considera la Tx procesada como equivalente a alguna existente en ESB_MESSAGE_TRANSACTION. Si dejamos este campo aqui, validaciones que dependan
        -- de este SP estarian retornando datos de Tx que cumplan tambien con el mismo CLIENT_REQ_TIMESTAMP lo que carece de sentido una vez no considerado este campo como parte de la KEY.).
        TC.CAPABILITY_ID = p_CAPABILITY_ID AND
        MT.RCD_STATUS = 1 AND
        TC.RCD_STATUS = 1
      ORDER BY MT."SEQUENCE" ASC;
  END GETCHECKTRANSACTION;

END ESB_MESSAGEMANAGER_PKG;
/
