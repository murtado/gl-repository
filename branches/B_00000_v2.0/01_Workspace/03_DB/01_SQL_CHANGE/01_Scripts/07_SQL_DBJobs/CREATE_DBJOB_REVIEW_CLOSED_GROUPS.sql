BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'REVIEW_CLOSED_GROUPS',
   job_type           =>  'STORED_PROCEDURE',
   job_action         =>  'ESB_CONVERSATION_MANAGER_PKG.REVIEW_CLOSED_CB_GROUPS',
   repeat_interval    =>  'FREQ=SECONDLY;INTERVAL=30',
   auto_drop          =>   FALSE);
END;
/

BEGIN
  DBMS_SCHEDULER.ENABLE('REVIEW_CLOSED_GROUPS');
END;
/