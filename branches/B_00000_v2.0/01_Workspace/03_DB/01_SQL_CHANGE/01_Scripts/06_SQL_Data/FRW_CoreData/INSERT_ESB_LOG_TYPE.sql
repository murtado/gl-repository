SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'SREQ','Service Request','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'SRSP','Service Response','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'CSRSP','CallBack Service Response','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'LOG','Generic Log','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'LREQ','Legacy Service Request','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'LRSP','Legacy Service Response','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'ULREQ','Unconditional Service Request','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'ULRSP','Unconditional Service Response','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'USREQ','Unconditional Service Request','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'USRSP','Unconditional Service Response','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'ULOG','Unconditional Generic Log','1');
	END;

	BEGIN
		Insert into ESB_LOG_TYPE (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_LOG_TYPE_SEQ.NEXTVAL,'URMREQ','Unconditional RoutingManager Request','1');
	END;

END;
/

COMMIT;
