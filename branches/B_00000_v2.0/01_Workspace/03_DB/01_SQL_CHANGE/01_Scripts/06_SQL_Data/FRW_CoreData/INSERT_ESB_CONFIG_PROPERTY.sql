SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

	V_CONFIG_PROPERTY_ID NUMBER;

BEGIN

	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'soapAction','SOAPAction, para 1.1 o 1.2','1');
	END;
	
	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'url','URL for service','1');
	END;
	
	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'path','path','1');
	END;
	
	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'legalType','legalType','1');
	END;

	--News properties for B_0000_v1.12

	BEGIN
		SELECT ID INTO V_CONFIG_PROPERTY_ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'MAILTO';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		BEGIN
			INSERT INTO ESB_CONFIG_PROPERTY(ID, NAME, DESCRIPTION, RCD_STATUS) VALUES (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL, 'MAILTO', 'Mail Destination', 1) RETURNING ID INTO V_CONFIG_PROPERTY_ID;
		END;
	END;

	BEGIN
		SELECT ID INTO V_CONFIG_PROPERTY_ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'MAILSUBJECT';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		BEGIN
			INSERT INTO ESB_CONFIG_PROPERTY(ID, NAME, DESCRIPTION, RCD_STATUS) VALUES (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL, 'MAILSUBJECT', 'Mail Destination', 1) RETURNING ID INTO V_CONFIG_PROPERTY_ID;
		END;
	END;

	BEGIN
		SELECT ID INTO V_CONFIG_PROPERTY_ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'UserAgent';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		BEGIN 
			INSERT INTO ESB_CONFIG_PROPERTY(ID, NAME, DESCRIPTION, RCD_STATUS) VALUES (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL, 'UserAgent', 'HTTP Header User-Agent', 1) RETURNING ID INTO V_CONFIG_PROPERTY_ID;
		END;
	END;

	BEGIN
		SELECT ID INTO V_CONFIG_PROPERTY_ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'Port';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		BEGIN 
			INSERT INTO ESB_CONFIG_PROPERTY(ID, NAME, DESCRIPTION, RCD_STATUS) VALUES (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL, 'Port', 'Port', 1) RETURNING ID INTO V_CONFIG_PROPERTY_ID;
		END;
	END;

	BEGIN
		SELECT ID INTO V_CONFIG_PROPERTY_ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'IP';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		BEGIN 
			INSERT INTO ESB_CONFIG_PROPERTY(ID, NAME, DESCRIPTION, RCD_STATUS) VALUES (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL, 'IP', 'IP', 1) RETURNING ID INTO V_CONFIG_PROPERTY_ID;
		END;
	END;

	BEGIN
		SELECT ID INTO V_CONFIG_PROPERTY_ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'SecondsToLive';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		BEGIN 
			INSERT INTO ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) VALUES (ESB_CONFIG_PROPERTY_SEQ.nextval, 'SecondsToLive', 'Tiempo de expiracion del mensaje en WS Security', '1') RETURNING ID INTO V_CONFIG_PROPERTY_ID;
		END;
	END;


END;
/

COMMIT;
