create or replace PACKAGE BODY ESB_CORRELATIONMANAGER_PKG AS

  /*------------------------------------------------------------------------------------------------------------------------------------

  INTERNAL PROCEDURES AND FUNCTIONS

  */------------------------------------------------------------------------------------------------------------------------------------

  /*---------------------------------------------------------------------------------------------------

    Fills the p_SOURCE_ERROR_CODE and p_SOURCE_ERROR_DESC variable sused thorughout the Package, by the
    use of the following parameters :

    p_Locator   -->  Las known position within a Procedure or Function
    p_Reason    -->  Descriptive information about an Error.
    p_ErrCode   -->  Unique code of an Error, within the context of this package.
    p_SqlErr    -->  SQLERR Information, as it exists within the error context
                                from which this procedure is called.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE LOAD_SOURCE_ERROR(
      p_Locator IN VARCHAR,
      p_ErrCode IN VARCHAR,
      p_Reason IN VARCHAR,
      p_SqlErr IN VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    IS
      BEGIN

            p_SOURCE_ERROR_CODE := p_ErrCode;
            p_SOURCE_ERROR_DESC :=
              p_Reason
              || ' @ ['
              || p_Locator
              || '] --- Error ['
              || p_SqlErr
              || ']';

    END LOAD_SOURCE_ERROR;

  /*---------------------------------------------------------------------------------------------------

    Returns the TableID of a CorrelationGroup, identified by its TAG on p_GRP_TAG, and its status
    on p_GRP_STATUS.

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_GROUP_ID(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      p_GRP_STATUS IN VARCHAR,          -- CorrelationGroup Status
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    )
    RETURN INTEGER IS

  v_GRP_ID INTEGER; -- Correlation Group ID

  BEGIN
    p_Locator := 'INIT - GET_GROUP_ID';

    p_SOURCE_ERROR_DESC := ' ';
    p_SOURCE_ERROR_CODE := '0';

    SELECT
      CG.ID
    INTO
      v_GRP_ID
    FROM ESB_CORRELATION_GROUP CG
    WHERE
      CG.TAG = p_GRP_TAG AND
      CG.STATUS = p_GRP_STATUS;

    p_Locator := 'After - Searching by TAG';

    RETURN v_GRP_ID;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '96',
          'There is no Group with that TAG : ' || p_GRP_TAG || '.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN 0;

  END GET_GROUP_ID;


  /*---------------------------------------------------------------------------------------------------

    Returns the TableID of a CorrelationGroup, identified by its TAG on p_GRP_TAG.

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_GROUP_ID(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    )
    RETURN INTEGER IS

  v_GRP_ID INTEGER; -- Correlation Group ID

  BEGIN
    p_Locator := 'INIT - GET_GROUP_ID';

    p_SOURCE_ERROR_DESC := ' ';
    p_SOURCE_ERROR_CODE := '0';

    SELECT
      CG.ID
    INTO
      v_GRP_ID
    FROM ESB_CORRELATION_GROUP CG
    WHERE
      CG.TAG = p_GRP_TAG;

    p_Locator := 'After - Searching by TAG';

    RETURN v_GRP_ID;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '96',
          'There is no Group with that TAG : ' || p_GRP_TAG || '.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN 0;

  END GET_GROUP_ID;

  /*---------------------------------------------------------------------------------------------------

    Returns the TableID of a CorrelationGroup, identified by a combination of :


    * Data from a Correlation Group :
       --> GroupName
       --> GroupStatus (If not sent, its not taken into account as a filter)
    * Data from a Member of that Group :
        --> MemberName
        --> MemberType
        --> MemberCorrelationID

  If more than one group is found by these parameters, an error will be thrown.

  This was made to limit the use of the CorrelationManager, and to move Solution Complexity towards its
  consumers (should it exist), aimed to increase its scalability.

  The reasons for which the second criteria could return more than one group, are identified as following :

  1 - The CorrelationGroup was created without a TAG, an there is more than one CorrelationGroup sharing the
      same group name, and those groups also hold a CorrelationMember with the same Name, Type and CorrelationID
      as those sent as parameters.

  As it is evident, the only reason why this limitation should worry someone is if that someone would use the
  CorrelationManager to save information sets that are not distinguishable from other information sets. That is
  seen as a design flaw/limintation from the requester side, and its not supported form the CorrelationManager side.

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_GROUP_ID(
      p_GRP_NAME IN VARCHAR,            -- CorrelationGroup Name
      p_GRP_STATUS IN VARCHAR,          -- CorrelationGroup Status
      p_GRP_MEM IN CRR_MEM_T,           -- The new Correlation Member definition.
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    )
    RETURN INTEGER IS

  v_GRP_ID INTEGER; -- Correlation Group ID

  BEGIN
    p_Locator := 'INIT - GET_GROUP_ID';

    p_SOURCE_ERROR_DESC := ' ';
    p_SOURCE_ERROR_CODE := '0';

      SELECT UNIQUE
        CG.ID
          INTO
            v_GRP_ID
        FROM ESB_CORRELATION C
          INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.ID
          INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.ID =  C.member_instance
        WHERE CG.ID  IN (
                            SELECT CG.ID
                            FROM ESB_CORRELATION C
                              INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.ID
                              INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.ID
                              INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.ID =  C.member_instance
                             WHERE
                              CI.correlation_id   = p_GRP_MEM.MEM_CORR_ID_  AND
                              CI.member_name      = p_GRP_MEM.MEM_NAME_     AND
                              CG.name             = p_GRP_NAME              AND
                              MT.NAME             = p_GRP_MEM.MEM_TYPE_     AND
                              CG.STATUS           = p_GRP_STATUS
                           );

    p_Locator := 'GET_GROUPID_BY_NAME_AND_MEMBER - After - Searching by Name and Member';

    RETURN v_GRP_ID;

    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '95',
          'There is more than one Group with that information.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN 0;
      WHEN NO_DATA_FOUND THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '96',
          'There is no Group with that information.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN 0;

  END GET_GROUP_ID;

  /*---------------------------------------------------------------------------------------------------

    Returns the TableID of a CorrelationGroup, identified by a combination of :


    * Data from a Correlation Group :
       --> GroupName
    * Data from a Member of that Group :
        --> MemberName
        --> MemberType
        --> MemberCorrelationID

  If more than one group is found by these parameters, an error will be thrown.

  This was made to limit the use of the CorrelationManager, and to move Solution Complexity towards its
  consumers (should it exist), aimed to increase its scalability.

  The reasons for which the second criteria could return more than one group, are identified as following :

  1 - The CorrelationGroup was created without a TAG, an there is more than one CorrelationGroup sharing the
      same group name, and those groups also hold a CorrelationMember with the same Name, Type and CorrelationID
      as those sent as parameters.

  As it is evident, the only reason why this limitation should worry someone is if that someone would use the
  CorrelationManager to save information sets that are not distinguishable from other information sets. That is
  seen as a design flaw/limintation from the requester side, and its not supported form the CorrelationManager side.

  */---------------------------------------------------------------------------------------------------
  FUNCTION GET_GROUP_ID(
      p_GRP_NAME IN VARCHAR,            -- CorrelationGroup Name
      p_GRP_MEM IN CRR_MEM_T,           -- The new Correlation Member definition.
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    )
    RETURN INTEGER IS

  v_GRP_ID INTEGER; -- Correlation Group ID

  BEGIN
    p_Locator := 'INIT - GET_GROUP_ID';

    p_SOURCE_ERROR_DESC := ' ';
    p_SOURCE_ERROR_CODE := '0';

      SELECT UNIQUE
        CG.ID
          INTO
            v_GRP_ID
        FROM ESB_CORRELATION C
          INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.ID
          INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.ID =  C.member_instance
        WHERE CG.ID  IN (
                            SELECT CG.ID
                            FROM ESB_CORRELATION C
                              INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.ID
                              INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.ID
                              INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.ID =  C.member_instance
                             WHERE
                              CI.correlation_id   = p_GRP_MEM.MEM_CORR_ID_  AND
                              CI.member_name      = p_GRP_MEM.MEM_NAME_     AND
                              CG.name             = p_GRP_NAME              AND
                              MT.NAME             = p_GRP_MEM.MEM_TYPE_
                           );

    p_Locator := 'GET_GROUPID_BY_NAME_AND_MEMBER - After - Searching by Name and Member';

    RETURN v_GRP_ID;

    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '95',
          'There is more than one Group with that information.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN 0;
      WHEN NO_DATA_FOUND THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '96',
          'There is no Group with that information.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        RETURN 0;

  END GET_GROUP_ID;

  /*---------------------------------------------------------------------------------------------------

  Deletes members from a group, found by the GroupTAG and by the Member cursor sent as parameter.

  The CorrelationGroup is identified only by P_GRP_TAG, whie the CorrelationMember (within that group)
  is itendified by the MEM_NAME_ and MEM_CORR_ID_ of each item within P_MEM_T_CUR.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE DELETE_GROUP_MEMBERS(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      p_MEM_T_CUR IN CRR_MEM_T_CUR,     -- Array of CorrelationMembers
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS

    v_MEM_ID INTEGER;
    v_GRP_ID INTEGER;

    BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - DELETE_GROUP_MEMBER';

      -- Getting the CorrelationGroup.
      v_GRP_ID := GET_GROUP_ID(p_GRP_TAG,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
      IF(p_SOURCE_ERROR_CODE != 0) THEN RETURN; END IF;

      /*-----------------------------------------------------------------------------
        Deleting each member sent as parameter.
      */-----------------------------------------------------------------------------
        FOR I IN P_MEM_T_CUR.FIRST .. P_MEM_T_CUR.LAST LOOP

          SELECT
            CI.ID
            INTO
              v_MEM_ID
          FROM
            ESB_CORRELATION_INSTANCE CI
             INNER JOIN ESB_CORRELATION C ON C.MEMBER_INSTANCE = CI.ID
             INNER JOIN ESB_CORRELATION_GROUP CG ON CG.ID = C.GROUP_INSTANCE
          WHERE
            CI.CORRELATION_ID   = P_MEM_T_CUR(I).MEM_CORR_ID_  AND
            CI.MEMBER_NAME      = P_MEM_T_CUR(I).MEM_NAME_     AND
            CG.ID = v_GRP_ID;

          DELETE
            FROM
              ESB_CORRELATION_INSTANCE CI
            WHERE
              CI.ID = v_MEM_ID;

          END LOOP;
          -----------------------------------------------------------------------------

        /*
          The relationship between a Member and its CorrelationGroup is held by the ESB_CORRELATION table,
          for which a Delete on Cascase was configured. When one Member is deleted, its relationship
          with the CorrelationGroup is automatically deleted. Same applies to the deletion of a
          CorrelationGroup, which is inmediatly followed by the deletion of every relationship on the
          ESB_CORRELATION table associated to it.
        */

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '96',
            'The Correlation Member was not found.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          ROLLBACK; -- We avoid any chance of getting one member deleted, while others remain.
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected error while Deleting the CorrelationMember',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          ROLLBACK;

  END DELETE_GROUP_MEMBERS;

  /*---------------------------------------------------------------------------------------------------

  Deletes group, found by the GroupTAG.

  The CorrelationGroup is identified only by P_GRP_TAG.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE DELETE_GROUP (
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS

    v_GRP_ID INTEGER;

    BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - DELETE_GROUP';

      -- Getting the CorrelationGroup.
      v_GRP_ID := GET_GROUP_ID(p_GRP_TAG,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
      IF(p_SOURCE_ERROR_CODE != 0) THEN RETURN; END IF;

      DELETE
      FROM
        ESB_CORRELATION_INSTANCE CI
      WHERE
        CI.ID IN (
                  SELECT
                    CIAux.ID
                  FROM
                    ESB_CORRELATION_INSTANCE CIAux
                  INNER JOIN ESB_CORRELATION C ON C.MEMBER_INSTANCE = CIAux.ID
                  INNER JOIN ESB_CORRELATION_GROUP CG ON CG.ID = C.GROUP_INSTANCE

                  WHERE
                    CG.ID = v_GRP_ID
                );

      /*
        The relationship between a Member and its CorrelationGroup is held by the ESB_CORRELATION table,
        for which a Delete on Cascase was configured. When one Member is deleted, its relationship
        with the CorrelationGroup is automatically deleted. Same applies to the deletion of a
        CorrelationGroup, which is inmediatly followed by the deletion of every relationship on the
        ESB_CORRELATION table associated to it.
      */

      -- Deleting the CorrelationGroup.
      DELETE
      FROM
        ESB_CORRELATION_GROUP CG
      WHERE
        CG.ID = v_GRP_ID;

      EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected error while Deleting the CorrelationMember',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          ROLLBACK;
  END DELETE_GROUP;

  /*---------------------------------------------------------------------------------------------------

  Creates all the Group Flags recieved as parameter on P_GRP_FLAGS_CUR, for the group itendified by
  the P_GRP_ID parameter.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_GROUP_FLAGS(
      P_GRP_ID IN INTEGER,                -- CorrelationGroup unique Table ID
      P_GRP_FLAGS_CUR IN CRR_FLAGS_T_CUR, -- Array of Flags.
      p_Locator OUT VARCHAR2,             -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2,   -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2    -- Holds the Error Code (If Any)
    ) IS

    BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - CREATE_GROUP_FLAGS';

      FOR I IN P_GRP_FLAGS_CUR.FIRST .. P_GRP_FLAGS_CUR.LAST LOOP

        INSERT INTO ESB_CORRELATION_GROUP_FLAGS(
          ID,
          GROUP_INSTANCE,
          NAME,
          VALUE
        )
        VALUES (
          ESB_CORR_GRP_FLAGS_SEQ.NEXTVAL,
          P_GRP_ID,
          P_GRP_FLAGS_CUR(I).NAME_,
          P_GRP_FLAGS_CUR(I).VALUE_
        );
      END LOOP;

    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '97',
          'Flag already exists within Group.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;
      WHEN OTHERS THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '99',
          'Unexpected error while creating a Group Flags.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;

  END CREATE_GROUP_FLAGS;

  /*---------------------------------------------------------------------------------------------------

  Creates all the Group Flags recieved as parameter on P_GRP_FLAGS_CUR, for the group itendified by
  the P_GRP_TAG parameter.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_GROUP_FLAGS(
      P_GRP_FLAGS_CUR IN CRR_FLAGS_T_CUR, -- Array of Flags.
      P_GRP_TAG IN VARCHAR,               -- CorrelationGroup unique TAG.
      p_Locator OUT VARCHAR2,             -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2,   -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2    -- Holds the Error Code (If Any)
    ) IS

        v_GRP_ID NUMBER;

        BEGIN

          p_SOURCE_ERROR_DESC := '';
          p_SOURCE_ERROR_CODE := '0';

          p_Locator := 'INIT - CREATE_GROUP_FLAGS';

          -- Getting the CorrelationGroup.
          v_GRP_ID := GET_GROUP_ID(p_GRP_TAG,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
          IF(p_SOURCE_ERROR_CODE != 0) THEN RETURN; END IF;

          /* ----------------------------------------------------------------------
              Creating the group flags.
          ----------------------------------------------------------------------*/
          CREATE_GROUP_FLAGS(
              v_GRP_ID,
              P_GRP_FLAGS_CUR,
              p_Locator,
              p_SOURCE_ERROR_DESC,
              p_SOURCE_ERROR_CODE
          );
          IF(p_SOURCE_ERROR_CODE != 0) THEN RETURN; END IF;
          ----------------------------------------------------------------------

        EXCEPTION
          WHEN OTHERS THEN
            LOAD_SOURCE_ERROR(
              p_Locator,
              '99',
              'Unexpected error while creating a Group Flags.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            ROLLBACK;

  END CREATE_GROUP_FLAGS;

  /*---------------------------------------------------------------------------------------------------

  Creates all the GroupMember Flags recieved as parameter on P_MEM_FLAGS_CUR, for the member itendified
  by the P_MEM_ID parameter.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MEMBER_FLAGS(
      P_MEM_ID IN INTEGER,                -- CorrelationGroup unique Table ID
      P_MEM_FLAGS_CUR IN CRR_FLAGS_T_CUR, -- Array of Flags.
      p_Locator OUT VARCHAR2,             -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2,   -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2    -- Holds the Error Code (If Any)
    ) IS

      v_FLAG_ID NUMBER;

    BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - CREATE_MEMBER_FLAGS';

      FOR I IN P_MEM_FLAGS_CUR.FIRST .. P_MEM_FLAGS_CUR.LAST LOOP
        BEGIN
          /* --------------------------------------------------------------------------------
              If there is another flag of the same name for this Member, it is updated.
          --------------------------------------------------------------------------------*/
            SELECT
              ID
                INTO
                  v_FLAG_ID
            FROM
              ESB_CORRELATION_MEMBER_FLAGS
            WHERE
              MEMBER_INSTANCE=P_MEM_ID AND
              NAME=P_MEM_FLAGS_CUR(I).NAME_;

            --Updating the existing flag.
            UPDATE ESB_CORRELATION_MEMBER_FLAGS SET VALUE=P_MEM_FLAGS_CUR(I).VALUE_ WHERE ID=v_FLAG_ID;
          --------------------------------------------------------------------------------

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              /* --------------------------------------------------------------------------------
                    There is no flag with that name. Creating it.
              --------------------------------------------------------------------------------*/
              INSERT INTO ESB_CORRELATION_MEMBER_FLAGS(
                ID,
                MEMBER_INSTANCE,
                NAME,
                VALUE
              )
              VALUES (
                ESB_CORR_MEM_FLAGS_SEQ.NEXTVAL,
                P_MEM_ID,
                P_MEM_FLAGS_CUR(I).NAME_,
                P_MEM_FLAGS_CUR(I).VALUE_
              );
              --------------------------------------------------------------------------------
          END;
      END LOOP;

    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '97',
          'Flag already exists within Member.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;
      WHEN OTHERS THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '99',
          'Unexpected error while creating a Member Flags.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;

  END CREATE_MEMBER_FLAGS;

  /*---------------------------------------------------------------------------------------------------

  Creates all the GroupMember Flags recieved as parameter on P_MEM_FLAGS_CUR, for the member itendified
  by the P_MEM_ID parameter.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MEMBER_FLAGS(
      p_MEM_CORR_ID IN VARCHAR2,          -- Group Member Correlation ID, unique amongst other members of the same CorrelationGroup.
      p_MEM_NAME IN VARCHAR2,             -- Group Member Name.
      p_MEM_TYPE IN VARCHAR2,             -- Group Member Type.
      p_GRP_TAG IN VARCHAR2,              -- CorrelationGroup unique TAG.
      P_MEM_FLAGS_CUR IN CRR_FLAGS_T_CUR, -- Array of Flags.
      p_Locator OUT VARCHAR2,             -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2,   -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2    -- Holds the Error Code (If Any)
    ) IS

      v_GRP_ID NUMBER;
      v_GRP_MEM_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := ' ';
        p_SOURCE_ERROR_CODE := '0';

        p_Locator := 'INIT - CREATE_MEMBER_FLAGS';

        -- Getting the CorrelationGroup.
        v_GRP_ID := GET_GROUP_ID(p_GRP_TAG,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
        IF(p_SOURCE_ERROR_CODE != 0) THEN RETURN; END IF;

        /* ----------------------------------------------------------------------
            Getting the Member ID.
        ----------------------------------------------------------------------*/
        SELECT
          CI.ID
            INTO
              v_GRP_MEM_ID
        FROM
          ESB_CORRELATION C
            INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.ID
            INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.ID
            INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.ID =  C.member_instance
        WHERE
          CI.CORRELATION_ID   = p_MEM_CORR_ID  AND
          CI.MEMBER_NAME      = p_MEM_NAME     AND
          CG.ID               = v_GRP_ID              AND
          MT.NAME             = p_MEM_TYPE;
        ----------------------------------------------------------------------

        /* ----------------------------------------------------------------------
            Creating the member flags.
        ----------------------------------------------------------------------*/
        CREATE_MEMBER_FLAGS(
            v_GRP_MEM_ID,
            P_MEM_FLAGS_CUR,
            p_Locator,
            p_SOURCE_ERROR_DESC,
            p_SOURCE_ERROR_CODE
        );
        IF(p_SOURCE_ERROR_CODE != 0) THEN RETURN; END IF;
        ----------------------------------------------------------------------

      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '97',
            'Flag already exists within Member.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          ROLLBACK;
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected error while creating a Member Flags.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          ROLLBACK;

  END CREATE_MEMBER_FLAGS;

  /*---------------------------------------------------------------------------------------------------

    Creates a new Corerlation Member, and associates it with an existinent CorrelationGroup.

    The CorrelationGroup is identified by the p_GRP_TAG parameter, while the Correlation Member
    definition is held by the GRP_MEM parameter.

  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_GROUP_MEMBER(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG .
      GRP_MEM IN CRR_MEM_T,             -- The new Correlation Member definition.
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any).
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any).
    ) RETURN INTEGER IS

    v_MEM_ID INTEGER;
    v_MEM_TYPE_ID INTEGER;
    v_GRP_ID INTEGER;

    BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

       p_Locator := 'INIT - CREATE_GROUP_MEMBER';

      -- Getting the CorrelationGroup.
      v_GRP_ID := GET_GROUP_ID(p_GRP_TAG,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
      IF(p_SOURCE_ERROR_CODE != 0) THEN RETURN 0; END IF;

      v_MEM_ID  := ESB_CORRELATION_INSTANCE_SEQ.NEXTVAL;

      -- Creating the new Correlation Member.
      INSERT INTO ESB_CORRELATION_INSTANCE(
        ID,
        CORRELATION_ID,
        MEMBER_NAME,
        MEMBER_LABEL,
        MEMBER_ADDRESS
      )
        VALUES (
          v_MEM_ID,
          GRP_MEM.MEM_CORR_ID_,
          GRP_MEM.MEM_NAME_,
          GRP_MEM.MEM_LABEL_,
          GRP_MEM.MEM_ADDR_
        );

        -- Locating the MemberType as sent by parameter.
        SELECT
          CT.ID
          INTO
            v_MEM_TYPE_ID
        FROM ESB_CORRELATION_MEMBER_TYPE CT
          WHERE
            CT.NAME= GRP_MEM.MEM_TYPE_;

        -- Associating the new Correlation Member with the Correlation Group.
        INSERT INTO ESB_CORRELATION (
          ID,
          GROUP_INSTANCE,
          MEMBER_INSTANCE,
          TYPE_MEMBER
          )
        VALUES(
          ESB_CORRELATION_SEQ.NEXTVAL,
          v_GRP_ID,
          v_MEM_ID,
          v_MEM_TYPE_ID
        );

        p_Locator := 'Creating the new Correlation Member Flags';
        IF (GRP_MEM.MEM_FLAGS_ IS NOT NULL) THEN
          CREATE_MEMBER_FLAGS(v_MEM_ID,GRP_MEM.MEM_FLAGS_,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
          IF(p_SOURCE_ERROR_CODE != 0) THEN ROLLBACK; RETURN 0; END IF;
        END IF;

        RETURN v_MEM_ID;

    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '97',
          'Member already exists within Group.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;
        RETURN 0;
      WHEN NO_DATA_FOUND THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '96',
          'The Correlation Group was not found.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;
        RETURN 0;
      WHEN OTHERS THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '99',
          'Unexpected error while Creating the CorrelationMember',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;
        RETURN 0;

  END CREATE_GROUP_MEMBER;

  /*---------------------------------------------------------------------------------------------------

    Creates a new Corerlation Group including each of its members, as they are defined within the
    p_GROUP parameter.

  */---------------------------------------------------------------------------------------------------
  FUNCTION CREATE_GROUP (
      p_GROUP IN CRR_GRP_T,             -- The new Correlation Group Definition.
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any).
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any).
    )
    RETURN INTEGER IS

    v_GRP_ID INTEGER;
    v_MEM_ID INTEGER;
    v_AuxFlag CHAR;

    BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

      v_AuxFlag := 0;

      p_Locator := 'INIT - CREATE_GROUP';

      /*---------------------------------------------------------------------------------------------------------------------------
      This is an Assertion made to return an error when there is a request to create a new Correlation Group with a Group TAG
      equivalent to an existing Correlation Group.
      */---------------------------------------------------------------------------------------------------------------------------
      IF (p_GROUP.GRP_TAG_ IS NOT NULL) THEN
        BEGIN
          SELECT CG.ID INTO v_GRP_ID FROM ESB_CORRELATION_GROUP CG WHERE CG.TAG = p_GROUP.GRP_TAG_;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
          v_AuxFlag := '1';
        END;

         IF v_AuxFlag = '0' THEN
          p_SOURCE_ERROR_DESC := 'There is another Correlation Group with that same TAG : ' || p_GROUP.GRP_TAG_;
          p_SOURCE_ERROR_CODE := '97';
          RETURN 0;
        END IF;
      END IF;
      -----------------------------------------------------------------------------------------------------------------------------

      v_GRP_ID := ESB_CORRELATION_GROUP_SEQ.NEXTVAL;

      INSERT INTO ESB_CORRELATION_GROUP(
          ID,
          NAME,
          TAG,
          REQUESTHEADER,
          STATUS,
          DESCRIPTION
         )
         VALUES(
          v_GRP_ID,
          p_GROUP.GRP_NAME_,
          p_GROUP.GRP_TAG_,
          p_GROUP.GRP_HEADER_,
          'O',
          p_GROUP.GRP_DESC_
         );

      ---------------------------------------------------------------------------------------------------------------
      -- For each Group Member attached to the Correlation Group sent as parameter.
      ---------------------------------------------------------------------------------------------------------------
      FOR I IN p_GROUP.GRP_MEMS_.FIRST .. p_GROUP.GRP_MEMS_.LAST LOOP

        p_Locator := 'Creating the new Correlation Member';

        v_MEM_ID := CREATE_GROUP_MEMBER(
            p_GROUP.GRP_TAG_,
            p_GROUP.GRP_MEMS_(I),
            p_Locator,
            p_SOURCE_ERROR_DESC,
            p_SOURCE_ERROR_CODE
        );
        IF(p_SOURCE_ERROR_CODE != 0) THEN ROLLBACK; RETURN 0; END IF;

      END LOOP;
      ---------------------------------------------------------------------------------------------------------------


      IF (p_GROUP.GRP_FLAGS_ IS NOT NULL) THEN
        CREATE_GROUP_FLAGS(v_GRP_ID,p_GROUP.GRP_FLAGS_,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
        IF(p_SOURCE_ERROR_CODE != 0) THEN ROLLBACK; RETURN 0; END IF;
      END IF;

      RETURN v_GRP_ID;

    EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            p_Locator,
            '99',
            'Unexpected error while creating the new CorrelationGroup.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          ROLLBACK;
          RETURN 0;

  END CREATE_GROUP;

  /*---------------------------------------------------------------------------------------------------

    Updates the status of an existent Correlation Group.

    The Correlation Group is identified by its Tag, as it is sent on the p_GRP_TAG parameter.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE UPDATE_GROUP_STATUS(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      p_STATUS IN CHAR,                 -- The new CorrelationGroup status.
      p_Locator OUT VARCHAR2,           -- Holds the last known position within this procedure.
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS

    v_GroupID INTEGER;

    BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

      p_Locator := 'INIT - UPDATE_GROUP_STATUS';

      v_GroupID := GET_GROUP_ID(p_GRP_TAG,p_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
      IF(p_SOURCE_ERROR_CODE != 0) THEN ROLLBACK; RETURN; END IF;

      UPDATE
       ESB_CORRELATION_GROUP CG
      SET
        CG.STATUS = p_STATUS
      WHERE CG.ID = v_GroupID;

    EXCEPTION
      WHEN OTHERS THEN
        LOAD_SOURCE_ERROR(
          p_Locator,
          '99',
          'Unexpected error while uodating the CorrelationGroup status.',
          SQLERRM,
          p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
        );
        ROLLBACK;
  END UPDATE_GROUP_STATUS;

  --------------------------------------------------------------------------------------------------------------------------------------

  --                                                     ######################

  /*------------------------------------------------------------------------------------------------------------------------------------

  PUBLIC PROCEDURES AND FUNCTIONS

  */------------------------------------------------------------------------------------------------------------------------------------

  /*---------------------------------------------------------------------------------------------------

  Public Wrapper for the private DELETE_GROUP procedure, defined above.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE DELETE_GROUP (
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS
    v_Locator VARCHAR(255);
    BEGIN
      DELETE_GROUP(p_GRP_TAG,v_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
  END DELETE_GROUP;

  /*---------------------------------------------------------------------------------------------------

  Public Wrapper for the private DELETE_GROUP_MEMBER procedure, defined above.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE DELETE_GROUP_MEMBER(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      P_MEM_T_CUR IN CRR_MEM_T_CUR,
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS
    v_Locator VARCHAR(255);
    BEGIN
      DELETE_GROUP_MEMBERS(p_GRP_TAG, P_MEM_T_CUR,v_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
  END DELETE_GROUP_MEMBER;

  /*---------------------------------------------------------------------------------------------------

  Public Wrapper for the private UPDATE_GROUP_STATUS procedure, defined above.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE UPDATE_GROUP_STATUS(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      p_STATUS IN CHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS
    v_Locator VARCHAR(255);
    BEGIN
      UPDATE_GROUP_STATUS(p_GRP_TAG,p_STATUS,v_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
  END UPDATE_GROUP_STATUS;

  /*---------------------------------------------------------------------------------------------------

  Public Wrapper for the private CREATE_GROUP_MEMBER procedure, defined above.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_GROUP_MEMBER(
      p_GRP_TAG IN VARCHAR,             -- CorrelationGroup unique TAG
      GRP_MEM IN CRR_MEM_T,
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS

    v_Locator VARCHAR(255);
    v_MEM_ID INTEGER;

    BEGIN

      v_MEM_ID := CREATE_GROUP_MEMBER(
        p_GRP_TAG,
        GRP_MEM,
        v_Locator,
        p_SOURCE_ERROR_DESC,
        p_SOURCE_ERROR_CODE
      );

  END CREATE_GROUP_MEMBER;

  /*---------------------------------------------------------------------------------------------------

  [DEPRECATED FROM 01/2017 ONWARDS]

  */---------------------------------------------------------------------------------------------------
  PROCEDURE REGISTER (GROUP_NAME IN VARCHAR2, GROUP_DESC IN VARCHAR2, GROUP_HEADER IN CLOB, MEMBER_TYPES IN MEMBER_T_CUR, ID_GROUP OUT NUMBER, p_SOURCE_ERROR_DESC OUT VARCHAR2, p_SOURCE_ERROR_CODE OUT VARCHAR2) IS
  v_member_instance_id NUMBER;

   id_type NUMBER;
  BEGIN

      p_SOURCE_ERROR_DESC := ' ';
      p_SOURCE_ERROR_CODE := '0';

	  ID_GROUP:= ESB_CORRELATION_GROUP_SEQ.NEXTVAL;
	  INSERT INTO ESB_CORRELATION_GROUP (ID, NAME,REQUESTHEADER,DESCRIPTION)
	  VALUES (ID_GROUP, GROUP_NAME, GROUP_HEADER, GROUP_DESC);
	  FOR I IN MEMBER_TYPES.FIRST .. MEMBER_TYPES.LAST LOOP
		v_member_instance_id  := ESB_CORRELATION_INSTANCE_SEQ.NEXTVAL;

		INSERT INTO ESB_CORRELATION_INSTANCE (ID, CORRELATION_ID, MEMBER_NAME,MEMBER_ADDRESS)
		VALUES (v_member_instance_id,  MEMBER_TYPES(I).CORRELATION_ID_,  MEMBER_TYPES(I).MEMBER_NAME_,MEMBER_TYPES(I).MEMBER_ADDRESS_);


		Select ct.id into  id_type   from ESB_CORRELATION_MEMBER_TYPE ct where ct.name= MEMBER_TYPES(I).TYPE_MEMBER_;

		INSERT INTO ESB_CORRELATION (ID, GROUP_INSTANCE, MEMBER_INSTANCE, TYPE_MEMBER)
		VALUES (ESB_CORRELATION_SEQ.NEXTVAL, ID_GROUP, v_member_instance_id, id_type );
	  END LOOP;

	EXCEPTION
	   WHEN NO_DATA_FOUND
	   THEN
		  p_SOURCE_ERROR_DESC := 'UNKNOWN TYPE MEMBER';
		  p_SOURCE_ERROR_CODE := '2';
		WHEN OTHERS
			   THEN
				   p_SOURCE_ERROR_DESC := 'UNKNOWN_ERROR';
		  p_SOURCE_ERROR_CODE := '99';
		  ID_GROUP :=-1;

	END REGISTER;

  /*---------------------------------------------------------------------------------------------------

  [MUST USE FROM 01/2017 ONWARDS]

  New version of the REGISTER procedure, defined above.

  It creates a new CorrelationGroup, by the CRR_GRP_T Object received as parameter.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE REGISTER_v2 (
      p_GROUP IN CRR_GRP_T,
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS

      v_GRP_ID NUMBER;
      v_Locator VARCHAR2(255);

    BEGIN

      v_Locator := 'INIT - REGISTER_v2';

      v_GRP_ID := CREATE_GROUP(p_GROUP,v_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);

	END REGISTER_v2;

  /*---------------------------------------------------------------------------------------------------

  [DEPRECATED FROM 01/2017 ONWARDS]

  */---------------------------------------------------------------------------------------------------
  PROCEDURE GET (GROUP_NAME IN VARCHAR2, MEMBER_TYPES IN MEMBER_T , CORRELATION_MESSAGE OUT SYS_REFCURSOR,p_SOURCE_ERROR_DESC OUT VARCHAR2, p_SOURCE_ERROR_CODE OUT VARCHAR2) IS
		  aux_group_name ESB_CORRELATION_GROUP.NAME %TYPE;
		  aux_group_header ESB_CORRELATION_GROUP.REQUESTHEADER%TYPE;
		  aux_description ESB_CORRELATION_GROUP.DESCRIPTION%TYPE;
		  aux_group_id ESB_CORRELATION_GROUP.ID%TYPE;
		  aux_member_name ESB_CORRELATION_INSTANCE.MEMBER_NAME%TYPE;
		  aux_member_correlation_id ESB_CORRELATION_INSTANCE.CORRELATION_ID%TYPE;
		  aux_member_type ESB_CORRELATION_MEMBER_TYPE.NAME%TYPE;
		  aux_member_address ESB_CORRELATION_INSTANCE.MEMBER_ADDRESS%TYPE;
		  BEGIN
		   p_SOURCE_ERROR_DESC := ' ';
		   p_SOURCE_ERROR_CODE := '0';

		   SELECT CG.name AS "GROUP_NAME", CG.REQUESTHEADER AS "GROUP_HEADER", CG.description AS "GROUP_DESC", CG.id AS "GROUP_ID" ,CI.member_name  AS "MEMBER_NAME",CI.correlation_id AS "MEMBER_CORRELATION_ID"  ,MT.name AS "MEMBER_TYPE" ,CI.member_address  AS "MEMBER_ADDRESS"
		   INTO aux_group_name, aux_group_header, aux_description, aux_group_id, aux_member_name, aux_member_correlation_id, aux_member_type, aux_member_address
		   FROM ESB_CORRELATION C
				  INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.id
				  INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.id
				  INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.id =  C.member_instance
				  WHERE CG.id  IN (
								  SELECT CG.id  FROM ESB_CORRELATION C
								  INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.id
								  INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.id
								  INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.id =  C.member_instance
								  WHERE CI.correlation_id =MEMBER_TYPES.CORRELATION_ID_  and CI.member_name =MEMBER_TYPES.MEMBER_NAME_   and CG.name = GROUP_NAME and  mt.name=MEMBER_TYPES.TYPE_MEMBER_
								  );

			   OPEN CORRELATION_MESSAGE FOR
			  SELECT CG.name AS "GROUP_NAME", CG.REQUESTHEADER AS "GROUP_HEADER", CG.description AS "GROUP_DESC", CG.id AS "GROUP_ID" ,CI.member_name  AS "MEMBER_NAME",CI.correlation_id AS "MEMBER_CORRELATION_ID"  ,MT.name AS "MEMBER_TYPE" ,CI.member_address  AS "MEMBER_ADDRESS"  FROM ESB_CORRELATION C
				  INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.id
				  INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.id
				  INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.id =  C.member_instance
				  WHERE CG.id  IN (
								  SELECT CG.id  FROM ESB_CORRELATION C
								  INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.id
								  INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.id
								  INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.id =  C.member_instance
								  WHERE CI.correlation_id =MEMBER_TYPES.CORRELATION_ID_  and CI.member_name =MEMBER_TYPES.MEMBER_NAME_   and CG.name = GROUP_NAME and  mt.name=MEMBER_TYPES.TYPE_MEMBER_
								  );

		 EXCEPTION
		  WHEN TOO_MANY_ROWS THEN

				  OPEN CORRELATION_MESSAGE FOR
				  SELECT CG.name AS "GROUP_NAME", CG.REQUESTHEADER AS "GROUP_HEADER", CG.description AS "GROUP_DESC", CG.id AS "GROUP_ID" ,CI.member_name  AS "MEMBER_NAME",CI.correlation_id AS "MEMBER_CORRELATION_ID"  ,MT.name AS "MEMBER_TYPE" ,CI.member_address  AS "MEMBER_ADDRESS"  FROM ESB_CORRELATION C
				  INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.id
				  INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.id
				  INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.id =  C.member_instance
				  WHERE CG.id  IN (
								  SELECT
                    MAX(CG.id)  -- Check comment below
                  FROM
                    ESB_CORRELATION C
                      INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT ON C.type_member = MT.id
                      INNER JOIN ESB_CORRELATION_GROUP CG ON C.group_instance = CG.id
                      INNER JOIN ESB_CORRELATION_INSTANCE CI ON CI.id =  C.member_instance
								  WHERE
                    CI.correlation_id =MEMBER_TYPES.CORRELATION_ID_  and
                    CI.member_name =MEMBER_TYPES.MEMBER_NAME_   and
                    CG.name = GROUP_NAME and
                    mt.name=MEMBER_TYPES.TYPE_MEMBER_
                    /* ------------------------------------------------------------------------------------ */
                    /*
                      The  MAX(CG.id)  was additioned to return the first group found the SELECT statement

                      We found scenarios on which RAs were executed multiple times within the same Tx.,
                      and for which the CorrelaitonID against the legacy were the EventID. The EventID
                      remains the same within a Tx, and so mutiples executions to those RA would render them
                      unable to find the correct CorrelationGroup; many would have been returned.

                      The fix is basically to return the LAST correlationGroup amogst every other found
                      by the same information on this select. The LAST one is determined by the highest
                      Sequence on the ID column. This entails a responsability over the componen which
                      calls the RA mutiple times on the same Tx, to wait for each iteration to finish
                      before executing a new one.
                    */
                    /* ------------------------------------------------------------------------------------ */
								  )
                  /* ------------------------------------------------------------------------------------ */
                  ;
				p_SOURCE_ERROR_DESC := ' ';
				p_SOURCE_ERROR_CODE := '0';

  END GET;

  /*---------------------------------------------------------------------------------------------------

  [MUST USE FROM 01/2017 ONWARDS]

  New version of the GET procedure, defined above.

  It returns a while CorrelationGrop, based on two choices of criteria :

    1 - By GroupTAG
    2 - By GroupName plus a Member from that Grouop. The Member is Identified by its Name + Type + CorrelationID.

  The second criteria wont return more than one Correlation Group. If more than one group is found by the
  information sent as parameters, an error will be thrown. This was made to limit the use of the CorrelationManager,
  and to move Solution Complexity towards its consumers (should it exists), aimed to increase its scalability.

  The reasons for which the second criteria could return more than one group, are identified as following :

  1 - The CorrelationGroup was created without a TAG, an there is more than one CorrelationGroup sharing the
      same group name, and those groups also hold a CorrelationMember with the same Name, Type and CorrelationID
      as those sent as parameters.

  As it is evident, the only reason why this limitation should worry someone is if that someone would use the
  CorrelationManager to save information sets that are not distinguishable from other information sets. That is
  seen as a design flaw/limintation from the requester side, and its not supported form the CorrelationManager side.

  */---------------------------------------------------------------------------------------------------
  PROCEDURE GET_v2 (
      p_GRP_NAME IN VARCHAR2,
      p_GRP_TAG IN VARCHAR2,
      p_GRP_STATUS IN CHAR,
      p_GRP_MEM IN CRR_MEM_T,
      p_GROUP OUT CRR_GRP_T,
      p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
      p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
    ) IS

      v_GRP_ID INT;

      v_Locator VARCHAR(255);

      v_GRP_MEMBERS SYS_REFCURSOR;

        v_MEMBER_ID INTEGER;
        v_MEMBER_NAME VARCHAR(255);
        v_MEMBER_LABEL VARCHAR(100);
        v_MEMBER_TYPE VARCHAR(100);
        v_MEMBER_CORRELATION_ID VARCHAR(255);
        v_MEMBER_ADDRESS VARCHAR(255);

      v_GROUP_FLAGS   SYS_REFCURSOR;
      v_MEMBER_FLAGS  SYS_REFCURSOR;

        v_FLAG_NAME VARCHAR(255);
        v_FLAG_VALUE  VARCHAR(255);

      v_INDEX_AUX_1 INTEGER;
      v_INDEX_AUX_2 INTEGER;
      v_INDEX_AUX_3 INTEGER;

      BEGIN

        p_SOURCE_ERROR_DESC := ' ';
        p_SOURCE_ERROR_CODE := '0';

        v_INDEX_AUX_1 := 1;
        v_INDEX_AUX_2 := 1;
        v_INDEX_AUX_3 := 1;

        p_GROUP :=
          NEW CRR_GRP_T(
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NEW CRR_MEM_T_CUR(NULL),
            NEW CRR_FLAGS_T_CUR(NULL)
          );

        /*------------------------------------------------------------------------------------------------------------------------------------

        There are two types of search that can be done to get a specific Group with this procedure :

        1 - [Introspection]
              The Consumer must know the GroupName, and at least one of its Members. In turn, the Member will be identified
              by its Name and Correlation ID.
        2 - [Selection]
              The Consumer must know the GroupTag. That's the only information required to get the Group that uniquely holds
              that particular TAG.

        Both search types were ment to give the ability to find the Group within several known scenarios; and to limit the use of this
        component, with aim to reduce its overall load and to keep it agnostic as possible.

        */------------------------------------------------------------------------------------------------------------------------------------

        IF (p_GRP_TAG IS NOT NULL) THEN

          p_GROUP.GRP_TAG_ := p_GRP_TAG;

          v_GRP_ID := GET_GROUP_ID(p_GRP_TAG,v_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
          IF(p_SOURCE_ERROR_CODE != 0) THEN RAISE_APPLICATION_ERROR(-20001-TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;


        ELSE

          p_GROUP.GRP_NAME_ := p_GRP_NAME;

          v_GRP_ID := GET_GROUP_ID(p_GRP_NAME,p_GRP_MEM,v_Locator,p_SOURCE_ERROR_DESC,p_SOURCE_ERROR_CODE);
          IF(p_SOURCE_ERROR_CODE != 0) THEN RAISE_APPLICATION_ERROR(-20001-TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC); END IF;

          END IF;

          SELECT
              CG.NAME,
              CG.TAG,
              CG.DESCRIPTION,
              CG.STATUS,
              CG.ID,
              CG.REQUESTHEADER
            INTO
              p_GROUP.GRP_NAME_,
              p_GROUP.GRP_TAG_,
              p_GROUP.GRP_DESC_,
              p_GROUP.GRP_STATUS_,
              v_GRP_ID,
              p_GROUP.GRP_HEADER_
            FROM ESB_CORRELATION_GROUP CG
            WHERE
              CG.ID = v_GRP_ID;

          /*
          ----------------------------------------------------------------------------------------------------------------------------------
          This cursor wil be populated with every CorrelationGroup with name equal to that sent by paremeter, and those for which one
          of its members has its attributes values equal to those sent by parameter (MEMBER_T).
          ----------------------------------------------------------------------------------------------------------------------------------
          */
          v_Locator := 'Before - Getting Members';
          OPEN v_GRP_MEMBERS FOR

            SELECT
              CI.ID             AS "MEMBER_ID",
              CI.MEMBER_NAME    AS "MEMBER_NAME",
              CI.MEMBER_LABEL   AS "MEMBER_LABEL",
              CI.CORRELATION_ID AS "MEMBER_CORRELATION_ID",
              MT.NAME           AS "MEMBER_TYPE",
              CI.MEMBER_ADDRESS AS "MEMBER_ADDRESS"
            FROM ESB_CORRELATION C
              INNER JOIN ESB_CORRELATION_MEMBER_TYPE MT   ON C.TYPE_MEMBER = MT.ID
              INNER JOIN ESB_CORRELATION_GROUP CG         ON C.GROUP_INSTANCE = CG.ID
              INNER JOIN ESB_CORRELATION_INSTANCE CI      ON CI.ID =  C.MEMBER_INSTANCE
            WHERE CG.ID = v_GRP_ID;

            v_INDEX_AUX_1 := 1;
            v_INDEX_AUX_2 := 1;
            v_INDEX_AUX_3 := 1;

            LOOP
              FETCH v_GRP_MEMBERS
                INTO
                  v_MEMBER_ID,
                  v_MEMBER_NAME,
                  v_MEMBER_LABEL,
                  v_MEMBER_CORRELATION_ID,
                  v_MEMBER_TYPE,
                  v_MEMBER_ADDRESS
                ;
              EXIT WHEN v_GRP_MEMBERS%NOTFOUND;

              v_Locator := 'Before - Loading Member - ' || v_INDEX_AUX_1;

              IF(v_INDEX_AUX_1!=1) THEN p_GROUP.GRP_MEMS_.EXTEND(1); END IF;

              p_GROUP.GRP_MEMS_(v_INDEX_AUX_1) :=
                NEW CRR_MEM_T(
                  NULL,
                  NULL,
                  NULL,
                  NULL,
                  NULL,
                  NULL
                );

              p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_NAME_     := v_MEMBER_NAME;
              p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_LABEL_    := v_MEMBER_LABEL;
              p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_TYPE_     := v_MEMBER_TYPE;
              p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_CORR_ID_  := v_MEMBER_CORRELATION_ID;
              p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_ADDR_     := v_MEMBER_ADDRESS;

              OPEN v_MEMBER_FLAGS FOR
                SELECT
                  CMF.NAME AS FLAG_NAME,
                  CMF.VALUE AS FLAG_VALUE
                FROM ESB_CORRELATION_MEMBER_FLAGS CMF
                  INNER JOIN ESB_CORRELATION_INSTANCE CI ON CMF.MEMBER_INSTANCE = CI.ID
                WHERE CI.ID = v_MEMBER_ID;

                v_INDEX_AUX_2 := 1;

                p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_FLAGS_ := NEW CRR_FLAGS_T_CUR(NULL);

                LOOP
                  FETCH v_MEMBER_FLAGS
                    INTO
                      v_FLAG_NAME,
                      v_FLAG_VALUE
                    ;
                  EXIT WHEN v_MEMBER_FLAGS%NOTFOUND;

                  v_Locator := 'Before - Loading Member - ' || v_INDEX_AUX_1 || ' - Flag - ' || v_INDEX_AUX_2;

                  IF(v_INDEX_AUX_2!=1) THEN p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_FLAGS_.EXTEND(1); END IF;

                  p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_FLAGS_(v_INDEX_AUX_2) := NEW CRR_FLAG_T(NULL,NULL);

                  p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_FLAGS_(v_INDEX_AUX_2).NAME_   := v_FLAG_NAME;
                  p_GROUP.GRP_MEMS_(v_INDEX_AUX_1).MEM_FLAGS_(v_INDEX_AUX_2).VALUE_  := v_FLAG_VALUE;

                  v_INDEX_AUX_2 := v_INDEX_AUX_2 + 1;

                  v_Locator := 'After - Loading Member - ' || v_INDEX_AUX_1 || ' - Flag - ' || v_INDEX_AUX_2;

                END LOOP;

              v_INDEX_AUX_1 := v_INDEX_AUX_1 + 1;
              v_Locator := 'After - Loading Member - ' || v_INDEX_AUX_1;

              CLOSE v_MEMBER_FLAGS;

            END LOOP;

            v_Locator := 'After - Getting Members';

          OPEN v_GROUP_FLAGS FOR
            SELECT
              CGF.NAME AS FLAG_NAME,
              CGF.VALUE AS FLAG_VALUE
            FROM ESB_CORRELATION_GROUP_FLAGS CGF
              INNER JOIN ESB_CORRELATION_GROUP CG ON CGF.GROUP_INSTANCE = CG.ID
            WHERE CG.ID = v_GRP_ID;

          v_INDEX_AUX_3 := 1;

          LOOP
            FETCH v_GROUP_FLAGS
              INTO
                v_FLAG_NAME,
                v_FLAG_VALUE
              ;
            EXIT WHEN v_GROUP_FLAGS%NOTFOUND;

            v_Locator := 'Before - Loading Group Flag - ' || v_INDEX_AUX_3;

            IF(v_INDEX_AUX_3!=1) THEN p_GROUP.GRP_FLAGS_.EXTEND(1); END IF;
            p_GROUP.GRP_FLAGS_(v_INDEX_AUX_3) := NEW CRR_FLAG_T(NULL,NULL);

            p_GROUP.GRP_FLAGS_(v_INDEX_AUX_3).NAME_  := v_FLAG_NAME;
            p_GROUP.GRP_FLAGS_(v_INDEX_AUX_3).VALUE_ := v_FLAG_VALUE;


            v_INDEX_AUX_3 := v_INDEX_AUX_3 + 1;

            v_Locator := 'After - Loading Group Flag - ' || v_INDEX_AUX_3;

          END LOOP;

          CLOSE v_GROUP_FLAGS;
          CLOSE v_GRP_MEMBERS;

      EXCEPTION
        WHEN OTHERS THEN
          LOAD_SOURCE_ERROR(
            v_Locator,
            '99',
            'Unexpected error while Retrieving the CorrelationGroup.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RAISE_APPLICATION_ERROR(-20001-TO_NUMBER(p_SOURCE_ERROR_CODE), p_SOURCE_ERROR_DESC);

  END GET_v2;

  --------------------------------------------------------------------------------------------------------------------------------------

END ESB_CORRELATIONMANAGER_PKG;
/