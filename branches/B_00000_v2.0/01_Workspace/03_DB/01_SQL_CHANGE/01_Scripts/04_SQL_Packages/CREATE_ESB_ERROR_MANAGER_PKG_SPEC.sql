create or replace PACKAGE ESB_ERROR_MANAGER_PKG
AS

    /* ================== MAIN PROCEDURE ================== */
    FUNCTION GET_DEF_RESULT_S_ID_OK(
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

    /* ================== MAIN PROCEDURE ================== */
    FUNCTION GET_DEF_RESULT_S_ID_ERROR(
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

    /* ================== MAIN PROCEDURE ================== */
    FUNCTION GET_RESULT_STATUS_ID(
      p_STATUS IN VARCHAR2,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

  /* ================== MAIN PROCEDURE ================== */
    FUNCTION GET_DEF_CAN_ERR_ID_NO_DATA
    (
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

  /* ================== MAIN PROCEDURE ================== */
    FUNCTION GET_CAN_ERR_ID
    (
      p_CAN_ERR_CODE IN VARCHAR,
      p_CAN_ERR_TYPE IN VARCHAR,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

    /* ================== MAIN PROCEDURE ================== */
    FUNCTION CREATE_COMPONENT(
      p_ERR_CMP IN ERR_CMP_T,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

    /* ================== MAIN PROCEDURE ================== */
    FUNCTION CREATE_COMPONENT_CFG(
      p_ERR_CMP_CFG IN ERR_CMP_CFG_T,
      p_CMP_ID IN NUMBER,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

    /* ================== MAIN PROCEDURE ================== */
    FUNCTION CREATE_COMPONENT_CFG(
      p_ERR_CMP_CFG IN ERR_CMP_CFG_T,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

    /* ================== MAIN PROCEDURE ================== */
    FUNCTION CREATE_DIAGNOSTIC(
      p_ERR_DIAG IN ERR_DIAG_T,
      p_Locator OUT VARCHAR,
      p_SOURCE_ERROR_CODE OUT VARCHAR,
      p_SOURCE_ERROR_DESC OUT VARCHAR
    )
    RETURN NUMBER;

  /* ================== MAIN PROCEDURE ================== */
    PROCEDURE DIAGNOSE(
    p_CMP_NAME IN VARCHAR,
    p_CMP_STEP IN VARCHAR,
    p_ERR_CODE IN VARCHAR,
    p_ERR_TYPE IN VARCHAR,
    p_ERR_ACTION OUT VARCHAR,
    p_ERR_ACTION_CFG OUT SYS_REFCURSOR,
    p_SOURCE_ERROR_DESC OUT VARCHAR2, -- Holds the Error Description (If Any)
    p_SOURCE_ERROR_CODE OUT VARCHAR2  -- Holds the Error Code (If Any)
  );

  /* ================== MAIN PROCEDURE ================== */

  PROCEDURE getCanonicalError(
      p_MODULE               IN VARCHAR2,
      p_SUB_MODULE           IN VARCHAR2,
      p_RAW_CODE             IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2);

  /* ================== COMPLETE PROCEDURE ==================

  It gets the canonical error for a given error source with four parameters (Sys_Code, Raw_Code, Module, Sub_Module)
  */
  PROCEDURE Get_Can_Err_Complete(
      p_MODULE               IN VARCHAR2,
      p_SUB_MODULE           IN VARCHAR2,
      p_RAW_CODE             IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2);

  /* ================== PROCEDURE ==================

  It gets the canonical error for a given error source with three parameters (Sys_Code, Module, Sub_Module)
  */
  PROCEDURE Get_Can_Err_For_Sys_API_Oper(
      p_MODULE               IN VARCHAR2,
      p_SUB_MODULE           IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2);

  /* ================== PROCEDURE ==================

  It gets the canonical error for a given error source with two parameters (Sys_Code, Module)
  */
  PROCEDURE Get_Can_Err_For_Sys_API(
      p_MODULE               IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2);

  /* ================== PROCEDURE ==================

  It gets the canonical error for a given error source with one parameter (Sys_Code)
  */
  PROCEDURE Get_Can_Err_For_System(
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2);

  /* ================== PROCEDURE ==================

  It gets the canonical error for a given error source without parameter (SUB_MODULE)
  */
    PROCEDURE Get_Generic_Err_For_API(
      p_MODULE               IN VARCHAR2,
      p_RAW_CODE             IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2);

  /* ================== PROCEDURE ==================
  It gets the canonical error for a given error source without parameters (MODULE and SUB_MODULE)

  */
      PROCEDURE Get_Generic_Err_For_System(
      p_RAW_CODE             IN VARCHAR2,
      p_ERROR_DETAILS_SOURCE IN VARCHAR2,
      p_RESULT_STATUS OUT VARCHAR2,
      p_RESULT_DESCRIPTION OUT VARCHAR2,
      p_CANONICAL_ERROR_CODE OUT VARCHAR2,
      p_CANONICAL_ERROR_TYPE OUT VARCHAR2,
      p_CANONICAL_ERROR_DESCRIPTION OUT VARCHAR2,
      p_SOURCE_ERROR_CODE OUT NUMBER,
      p_SOURCE_ERROR_DESCRIPTION OUT VARCHAR2,
      p_ERROR_DETAILS OUT VARCHAR2,
      p_ERROR_SOURCE OUT VARCHAR2);

END ESB_ERROR_MANAGER_PKG;
/