create or replace PACKAGE BODY ESB_CONVERSATION_MANAGER_PKG
AS

/*---------------------------------------------------------------------------------------------------

    Creates a record in ESB_CONVERSATION returning the ID of the new entry.

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------

  FUNCTION CREATE_CONVERSATION(
    p_TRANSACTION_ID              IN NUMBER,
    p_CAPABILITY_ID               IN NUMBER,
    p_CONVERSATION_ID             IN VARCHAR2,
    p_CHANNEL_ID                  IN NUMBER,
    p_CONSUMER_ID                 IN NUMBER,
    p_RCD_STATUS                  IN NUMBER,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR,
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  )  RETURN NUMBER
    IS
      v_CONVERSATION_ID_ID NUMBER;
      v_CONV_SEQ NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'The Conversation was successfully created.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_CONVERSATION_MANAGER_PKG.CREATE_CONVERSATION - INIT';

        /* ---------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------*/
          SELECT
            COUNT(*)
              INTO
              v_CONV_SEQ
          FROM ESB_CONVERSATION C
            INNER JOIN ESB_MESSAGE_TRANSACTION M ON M.ID = C.MESSAGE_TX_ID
          WHERE M.ID = p_TRANSACTION_ID;
        ---------------------------------------------------------------------------------------

        INSERT INTO
          ESB_CONVERSATION(
            ID,
            MESSAGE_TX_ID,
            CAPABILITY_ID,
            CONVERSATION_ID,
            CHANNEL_ID,
            CONSUMER_ID,
            SEQUENCE,
            RCD_STATUS
          )
          VALUES (
              ESB_CONVERSATION_SEQ.NEXTVAL,
              p_TRANSACTION_ID,
              p_CAPABILITY_ID,
              p_CONVERSATION_ID,
              p_CHANNEL_ID,
              p_CONSUMER_ID,
              v_CONV_SEQ,
              p_RCD_STATUS
            )
        RETURNING "ID" INTO v_CONVERSATION_ID_ID;

        RETURN v_CONVERSATION_ID_ID;

      EXCEPTION
          WHEN TOO_MANY_ROWS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              p_Locator,
              '-2',
              'Duplicate records matching this info : [ConversationID]=' || p_CONVERSATION_ID || '.',
              SQLERRM,
              p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
          WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                p_Locator,
                '-3',
                'Unexpected Error.',
                SQLERRM,
                p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
            );
            RETURN 0;
  END CREATE_CONVERSATION;

  FUNCTION GET_CONVERSATION_ID(
    p_CONVERSATION_ID     IN VARCHAR2, /* This is the ConversationID as found in the headers,
                                              which in turn is is the CONVERSATION_ID column on the ESB_CONVERSATION */
    p_Locator             OUT VARCHAR,
    p_SOURCE_ERROR_CODE   OUT VARCHAR,
    p_SOURCE_ERROR_DESC   OUT VARCHAR
  ) RETURN NUMBER

    IS

      v_CONVERSATION_ID_ID NUMBER;

      BEGIN

        p_SOURCE_ERROR_DESC := 'Retrieving the Conversation.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_CONVERSATION_MANAGER_PKG.GET_CONVERSATION_ID - INIT';

        SELECT MIN(ID)
          INTO v_CONVERSATION_ID_ID
        FROM
          ESB_CONVERSATION
        WHERE
          CONVERSATION_ID = p_CONVERSATION_ID AND
          RCD_STATUS <> 2;
      
        IF (v_CONVERSATION_ID_ID IS NULL) THEN RAISE NO_DATA_FOUND; END IF;

        RETURN v_CONVERSATION_ID_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [ConversationID]=' || p_CONVERSATION_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [ConversationID]=' || p_CONVERSATION_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN 0;
  END GET_CONVERSATION_ID;


  PROCEDURE GET_CONVERSATION_STATUS(
    p_CONVERSATION_ID     IN VARCHAR2,
    p_STATUS              OUT VARCHAR2,
    p_RSP_MSG             OUT CLOB,
    p_CAN_ERROR_ID        OUT VARCHAR2,
    p_Locator             OUT VARCHAR2,
    p_SOURCE_ERROR_CODE   OUT VARCHAR2,
    p_SOURCE_ERROR_DESC   OUT VARCHAR2
    ) IS
      BEGIN
  
        p_SOURCE_ERROR_DESC := 'Retrieving the Conversation Status.';
        p_SOURCE_ERROR_CODE := '0';
        p_Locator := 'ESB_CONVERSATION_MANAGER_PKG.GET_CONVERSATION_STATUS - INIT';
        
        SELECT 
          CS.STATUS, CS.RSP_MSG, CS.CAN_ERR_ID
            INTO p_STATUS, p_RSP_MSG, p_CAN_ERROR_ID
        FROM 
          ESB_CONVERSATION_STATUS CS
        WHERE
          CONVERSATION_ID = p_CONVERSATION_ID AND
          CS.RCD_STATUS <> 2;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [ConversationID]=' || p_CONVERSATION_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [ConversationID]=' || p_CONVERSATION_ID || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
    END;

  PROCEDURE getConversationStatus(

    p_CONVERSATION_ID     IN VARCHAR2,
    p_STATUS              OUT VARCHAR2,
    p_RSP_MSG             OUT CLOB,
    p_CAN_ERROR_ID        OUT NUMBER
  )
  IS
    v_Locator VARCHAR(255);
    v_SOURCE_ERROR_CODE VARCHAR(50);
    v_SOURCE_ERROR_DESC VARCHAR(255);

    BEGIN

      p_STATUS := '';
      p_RSP_MSG := null;
      p_CAN_ERROR_ID := '';
      p_STATUS := '';
      p_RSP_MSG := null;
      p_CAN_ERROR_ID := '';
      
      GET_CONVERSATION_STATUS(p_CONVERSATION_ID,p_STATUS, p_RSP_MSG, p_CAN_ERROR_ID, v_Locator, v_SOURCE_ERROR_CODE, v_SOURCE_ERROR_DESC );
      
      IF v_SOURCE_ERROR_CODE <> '0' THEN p_STATUS:= v_SOURCE_ERROR_DESC; END IF;

  END getConversationStatus;

---------------------------------------------------------------------------------------------------
  PROCEDURE updateConversationStatus(

    p_CONVERSATION_ID     IN VARCHAR2,
    p_STATUS              IN VARCHAR2,
    p_RSP_MSG             IN CLOB,
    p_CAN_ERROR_CODE      IN VARCHAR2,
    p_CAN_ERROR_TYPE      IN VARCHAR2,
    p_UPDATE_COMPONENT    IN VARCHAR2,
    p_UPDATE_OPERATION    IN VARCHAR2,

    p_RESULT_CODE     OUT VARCHAR2,
    p_RESULT_DESCRIPTION  OUT VARCHAR2
  )
  IS

    V_MESSAGE_TX_ID VARCHAR2(200);
    V_CAPABILITY_ID NUMBER;
    V_SEQUENCE NUMBER;
    V_CAN_ERR_ID NUMBER;
    V_TRANSACTION_CHECK_ID NUMBER;
    v_locator VARCHAR(255);
    v_OBSERVATIONS VARCHAR(255);
    V_SOURCE_ERROR_CODE VARCHAR2(255);
    V_SOURCE_ERROR_DESC VARCHAR2(255);
    V_RESULT_CODE VARCHAR2(255);
    V_RESULT_DESC VARCHAR2(255);
    BEGIN

      v_locator := 'INIT - updateConversationStatus';
      v_OBSERVATIONS := '';

      BEGIN

        SELECT CONVERSATION_ID
          INTO p_RESULT_CODE
        FROM ESB_CONVERSATION_STATUS
        WHERE
          CONVERSATION_ID = p_CONVERSATION_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              v_locator,
              '98',
              'There is no ConversationID on the ESB_CONVERSATION_STATUS matching the following data : ' || p_CONVERSATION_ID  || ' .',
              SQLERRM,
              p_RESULT_CODE,
              p_RESULT_DESCRIPTION
            );
          v_OBSERVATIONS :='INV_CAN_ERROR::'||P_CAN_ERROR_CODE||'-'||P_CAN_ERROR_TYPE;
          RETURN;
      END;

      p_RESULT_CODE := 0;
      p_RESULT_DESCRIPTION := 'Conversation has been updated Successfully.';

      /* ------------------------------------------------------------------------------------------------
        Finding the Canonical Error from P_CAN_ERROR_CODE and P_CAN_ERROR_CODE parameters.
      */ ------------------------------------------------------------------------------------------------
      IF(P_CAN_ERROR_CODE IS NOT NULL) THEN -- Checking if there is an error to check.
        BEGIN

          SELECT CE.ID
            INTO V_CAN_ERR_ID
          FROM ESB_CANONICAL_ERROR CE
            INNER JOIN ESB_CANONICAL_ERROR_TYPE CET ON CET.ID = CE.TYPE_ID
          WHERE
            CE.CODE = P_CAN_ERROR_CODE AND
            CET.TYPE = P_CAN_ERROR_TYPE;

        EXCEPTION
          -- This is done so the update goes on, regardless of whether the canonical error was found or not.
          WHEN OTHERS THEN
              V_CAN_ERR_ID := ESB_ERROR_MANAGER_PKG.GET_DEF_CAN_ERR_ID_NO_DATA( v_Locator, V_SOURCE_ERROR_CODE, V_SOURCE_ERROR_DESC);
              ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                v_locator,
                '98',
                'There are problem to found Canonical Error with that information : CODE-' || P_CAN_ERROR_CODE || ' TYPE=' || P_CAN_ERROR_TYPE,
                SQLERRM,
                V_RESULT_CODE,
                V_RESULT_DESC
              );
            v_OBSERVATIONS := SUBSTR( ('INV_CAN_ERROR::'||V_RESULT_CODE||'-'||V_RESULT_DESC),1 , 246 );
        END;
      END IF;

      /* ------------------------------------------------------------------------------------------------
        Updating the Conversation Status
      */ ------------------------------------------------------------------------------------------------

      UPDATE
        ESB_CONVERSATION_STATUS CS
      SET
        CS.STATUS             = p_STATUS,
        CS.RSP_MSG            = p_RSP_MSG,
        CS.CAN_ERR_ID         = V_CAN_ERR_ID,
        CS.UPDATE_COMPONENT   = p_UPDATE_COMPONENT,
        CS.UPDATE_OPERATION   = p_UPDATE_OPERATION,
        CS.UPDATE_TIMESTAMP   = current_timestamp,
        CS.OBSERVATIONS       = v_OBSERVATIONS
      WHERE
        CS.CONVERSATION_ID = P_CONVERSATION_ID;

      /* ------------------------------------------------------------------------------------------------
        Checking wether the associated Tx. should be hospitalized.

        10/05/2017 - Mario Mesaglio :

          We found this statement to force a tight coupling between the ConversationManager and the
          LoggerManager, by which there were cases producing this procedure to fail when the conversation
          (@ESB_CONVERSATION) was not able to be created yet (by the LoggerManager).

          The Conversation@ESB_CONVERSATION is only created, as for today, when the Logging is enabled,
          and on a different thread than this procedure. We cannot have this procedure to fail by those
          parameters, reason why the following logic is encompassed on a different execution block with
          no chance of producing exceptions.

      */ ------------------------------------------------------------------------------------------------
        BEGIN

          IF (p_STATUS = 'ERROR') THEN

            -- Getting the Conversation Sequence
            SELECT
              MESSAGE_TX_ID,
              CAPABILITY_ID,
              SEQUENCE
                INTO
                  V_MESSAGE_TX_ID,
                  V_CAPABILITY_ID,
                  V_SEQUENCE
            FROM
              ESB_CONVERSATION
            WHERE
              CONVERSATION_ID  = p_CONVERSATION_ID;

            -- Checking if the Conversations is the Tx. Owner
            IF (V_SEQUENCE = '0') THEN

              -- Setting the Transaction to be Hospitalized.
              INSERT INTO ESB_ERROR_CONVERSATION
              (
                ID,
                MESSAGE_TX_ID,
                CAPABILITY_ID,
                CAN_ERR_ID,
                STATUS
              )
              VALUES
              (
                ESB_ERROR_CONVERSATION_SEQ.NEXTVAL,
                V_MESSAGE_TX_ID,
                V_CAPABILITY_ID,
                V_CAN_ERR_ID,
                'UNREAD'
              );
            END IF;
          END IF;

          EXCEPTION
            WHEN OTHERS THEN NULL;
        END;
      /* ------------------------------------------------------------------------ */

    EXCEPTION
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            v_locator,
            '99',
            'Unexpected error while updating a Conversation status',
            SQLERRM,
            p_RESULT_CODE,
            p_RESULT_DESCRIPTION
          );

    END updateConversationStatus;

---------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_CLOSED_CB_GROUP(

    p_CB_GRP_TAG    IN VARCHAR2,
    p_CB_GRP_OWN_JMS_TYPE  IN VARCHAR2,
    p_CB_GRP_OWN_JMS_MSGID IN VARCHAR2,
    p_RESULT_CODE     OUT VARCHAR2,
    p_RESULT_DESCRIPTION  OUT VARCHAR2) IS

    v_locator VARCHAR(255);

    BEGIN

       v_locator := 'INIT - CREATE_CLOSED_CB_GROUP';

       INSERT INTO ESB_CONVERSATION_CB_GROUP
       (
         ID,
         CB_GRP_TAG,
         CB_GRP_OWN_JMS_TYPE,
         CB_GRP_OWN_JMS_MSGID
       )
       VALUES
       (
        ESB_CONV_GRP_CB_GRP_SEQ.NEXTVAL,
        p_CB_GRP_TAG,
        p_CB_GRP_OWN_JMS_TYPE,
        p_CB_GRP_OWN_JMS_MSGID
       );

    EXCEPTION
      WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              v_locator,
              '99',
              'Unexpected error while Updating the Status of a Closed CB Group',
              SQLERRM,
              p_RESULT_CODE,p_RESULT_DESCRIPTION
            );

    END CREATE_CLOSED_CB_GROUP;

---------------------------------------------------------------------------------------------------
  PROCEDURE REVIEW_CLOSED_CB_GROUPS IS

    v_locator VARCHAR(255);

    BEGIN

      v_locator := 'INIT - REVIEW_CLOSED_CB_GROUPS';

      UPDATE
        ESB_CONVERSATION_CB_GROUP
      SET
        CB_GRP_STATUS = 'READY'
      WHERE
        CB_GRP_STATUS = 'PENDING' AND
        CB_GRP_TAG NOT IN (
          SELECT
              UNIQUE(CRRG.TAG)
            FROM
              ESB_CORRELATION_INSTANCE CRRI
                LEFT JOIN ESB_CORRELATION CRR ON CRR.MEMBER_INSTANCE = CRRI.ID
                RIGHT JOIN ESB_CORRELATION_GROUP CRRG ON CRR.GROUP_INSTANCE = CRRG.ID
                RIGHT JOIN ESB_CONVERSATION_STATUS CRRS ON CRRS.CONVERSATION_ID = CRRI.CORRELATION_ID
                RIGHT JOIN ESB_CONVERSATION_CB_GROUP CBGRP ON CBGRP.CB_GRP_TAG = CRRG.TAG
                RIGHT JOIN ESB_CORRELATION_MEMBER_FLAGS CRRMF ON CRRMF.MEMBER_INSTANCE = CRRI.ID
            WHERE
              CRRG.NAME = 'cbSUB' AND
              CRRI.MEMBER_NAME = 'PRV' AND
              (CRRS.STATUS = 'PENDING' OR ((CRRS.STATUS = 'OK' OR CRRS.STATUS = 'ERROR') AND CRRMF.NAME = 'canClose' AND CRRMF.value = 'F')
          )
      );

    END REVIEW_CLOSED_CB_GROUPS;

---------------------------------------------------------------------------------------------------
  PROCEDURE GET_CB_DETAILS(
    p_CB_IDS              IN    CNV_CB_IDS_T_CUR,
    p_CB_DETAILS          OUT   CNV_CB_DETAILS_T_CUR,
    p_RESULT_CODE         OUT   VARCHAR2,
    p_RESULT_DESCRIPTION  OUT   VARCHAR2
    ) IS

    v_locator VARCHAR(255);
    v_CB_MSG CLOB;

    BEGIN

      v_locator := 'INIT - GET_CB_DETAILS';

      p_RESULT_CODE := 0;
      p_RESULT_DESCRIPTION := 'Callback Details have been successfully retrieved.';

      p_CB_DETAILS := new CNV_CB_DETAILS_T_CUR();

      FOR I IN p_CB_IDS.FIRST .. p_CB_IDS.LAST LOOP

        v_CB_MSG := 'NOT_FOUND';
        v_locator := 'Processing CB with ID : ' || p_CB_IDS(I);

        ------------------------------------------------------------------------------
        BEGIN
          SELECT
            LOG.MESSAGE
            INTO
              v_CB_MSG
          FROM
            ESB_LOG LOG
          WHERE
            LOG.ID = p_CB_IDS(I) AND
            LOG.TYPE_ID = (SELECT ID FROM ESB_LOG_TYPE WHERE NAME = 'CSRSP')
          ORDER BY LOG.ID ASC;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                v_locator,
                '1',
                'At least one of the Callbacks were not found',
                SQLERRM,
                p_RESULT_CODE,p_RESULT_DESCRIPTION
              );
        END;
        ------------------------------------------------------------------------------

        p_CB_DETAILS.EXTEND(1);
        p_CB_DETAILS(I) := NEW CNV_CB_DETAIL_T(p_CB_IDS(I),v_CB_MSG);

      END LOOP;

    EXCEPTION
      WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              v_locator,
              '99',
              'Unexpected error while getting the detail of a Callback',
              SQLERRM,
              p_RESULT_CODE,p_RESULT_DESCRIPTION
            );

    END GET_CB_DETAILS;

---------------------------------------------------------------------------------------------------
  PROCEDURE GET_CLOSED_CB_GROUPS(

    p_CB_GRP_TAGs         IN   CNV_GRP_T_CUR,
    p_CB_GRPS              OUT   CB_GRP_T_CUR,
    p_RESULT_CODE         OUT   VARCHAR2,
    p_RESULT_DESCRIPTION  OUT   VARCHAR2
  )
  IS

    v_locator VARCHAR(255);

    v_SUB_CNV_ID      VARCHAR(255);
    v_SUB_LABEL       VARCHAR(255);
    v_CNV_STATUS      VARCHAR(10);
    v_SRV_NAME        VARCHAR(255);
    v_SRV_OP_NAME     VARCHAR(255);
    v_CB_ID           VARCHAR(255);
    v_GRP_TAG     VARCHAR(255);
    v_GRP_LABEL   VARCHAR(255);
    v_GRP_HEADER  CLOB;
    v_CB_GRP_MEMS SYS_REFCURSOR;
    v_INDEX_AUX_1 INTEGER;

    BEGIN

      p_RESULT_CODE := 0;
      p_RESULT_DESCRIPTION := 'Groups have been successfully retrieved.';

      v_locator := 'INIT - GET_CLOSED_CB_GROUPS';

      p_CB_GRPS := new CB_GRP_T_CUR(NULL);

      FOR I IN p_CB_GRP_TAGs.FIRST .. p_CB_GRP_TAGs.LAST LOOP

        v_INDEX_AUX_1 := 1;

        IF(I>1) THEN p_CB_GRPS.EXTEND(1); END IF;

        p_CB_GRPS(I) := NEW CB_GRP_T(NULL,NULL,NULL,NEW CB_GRP_MEM_T_CUR());

        v_locator := 'Processing Group w/Tag : ' || p_CB_GRP_TAGs(I);

        ------------------------------------------------------------------------------
          SELECT
            CRG.TAG,
            CRG.REQUESTHEADER
              INTO
                v_GRP_TAG,
                v_GRP_HEADER
          FROM
            ESB_CORRELATION_GROUP CRG
          WHERE
            CRG.TAG = p_CB_GRP_TAGs(I);

          p_CB_GRPS(I).CB_GRP_TAG_      := v_GRP_TAG;
          p_CB_GRPS(I).CB_GRP_SUB_ID_   := '';
          p_CB_GRPS(I).CB_GRP_HEADER_   := v_GRP_HEADER;

         ------------------------------------------------------------------------------

         v_locator := 'Found a Header for Group w/Tag : ' || p_CB_GRP_TAGs(I);

        OPEN v_CB_GRP_MEMS FOR
          SELECT

            CRI.CORRELATION_ID,
            CRI.MEMBER_LABEL,
            CS.STATUS,
            SRV.NAME,
            CAP.NAME,
            LOG.ID

          FROM
            ESB_CONVERSATION_CB_GROUP CBG
              LEFT JOIN ESB_CORRELATION_GROUP CRG ON CRG.TAG = CBG.CB_GRP_TAG
              LEFT JOIN ESB_CORRELATION CRR ON CRR.GROUP_INSTANCE = CRG.ID
              LEFT JOIN ESB_CORRELATION_INSTANCE CRI ON CRI.ID = CRR.MEMBER_INSTANCE
              LEFT JOIN ESB_CONVERSATION_STATUS CS ON CS.CONVERSATION_ID = CRI.CORRELATION_ID
              LEFT JOIN ESB_CONVERSATION CNV ON CS.CONVERSATION_ID = CNV.CONVERSATION_ID
              LEFT JOIN ESB_TRACE TRC ON TRC.CONV_ID = CNV.ID
              LEFT JOIN ESB_LOG LOG ON LOG.TRACE_ID = TRC.ID
              LEFT JOIN ESB_CAPABILITY CAP ON CAP.ID = CNV.CAPABILITY_ID
              LEFT JOIN ESB_SERVICE SRV ON SRV.ID = CAP.SERVICE_ID
          WHERE
            CRI.MEMBER_NAME = 'PRV' AND
            LOG.TYPE_ID = (SELECT ID FROM ESB_LOG_TYPE WHERE NAME = 'CSRSP') AND
            CBG.CB_GRP_TAG = p_CB_GRP_TAGs(I);

        LOOP
          FETCH v_CB_GRP_MEMS
            INTO
                  v_SUB_CNV_ID,
                  v_SUB_LABEL,
                  v_CNV_STATUS,
                  v_SRV_NAME,
                  v_SRV_OP_NAME,
                  v_CB_ID;
            EXIT WHEN v_CB_GRP_MEMS%NOTFOUND;

            v_locator := 'Processing Member w/ConversationID v_SUB_CNV_ID: ' || v_SUB_CNV_ID;

            p_CB_GRPS(I).CB_GRP_MEMS.EXTEND(1);
            p_CB_GRPS(I).CB_GRP_MEMS(v_INDEX_AUX_1) := NEW CB_GRP_MEM_T(v_SUB_LABEL,v_SUB_CNV_ID,v_CNV_STATUS,v_SRV_NAME,v_SRV_OP_NAME,v_CB_ID);

            v_INDEX_AUX_1 := v_INDEX_AUX_1 + 1;

        END LOOP;

      END LOOP;

    EXCEPTION
      WHEN OTHERS THEN
            ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
              v_locator,
              '99',
              'Unexpected error while getting the detail of a Closed CB Group',
              SQLERRM,
              p_RESULT_CODE,p_RESULT_DESCRIPTION
            );

    END GET_CLOSED_CB_GROUPS;
---------------------------------------------------------------------------------------------------

PROCEDURE putConversationStatus(
  P_CONVERSATION_ID IN VARCHAR2,
  p_RESULT_CODE OUT VARCHAR2,
  p_RESULT_DESCRIPTION OUT VARCHAR2 )
  IS

    v_Status VARCHAR2(30);
    duplicate_transaction EXCEPTION;

    v_locator VARCHAR(255);

  BEGIN

    p_RESULT_CODE := 0;
    p_RESULT_DESCRIPTION := 'Conversation has been successfully created.';

    v_locator := 'INIT - putConversationStatus';

    INSERT INTO  ESB_CONVERSATION_STATUS
      (
        CONVERSATION_ID,
        STATUS,
        CAN_ERR_ID,
        CREATION_TIMESTAMP,
        UPDATE_TIMESTAMP
      )
      VALUES
      (
        P_CONVERSATION_ID,
        'PENDING',
        NULL,
        CURRENT_TIMESTAMP,
        NULL
      );

  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            v_locator,
            '96',
            'Duplicate Record with data :' || P_CONVERSATION_ID || '.',
            SQLERRM,
            p_RESULT_CODE,p_RESULT_DESCRIPTION
          );
    WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
                v_locator,
                '99',
                'Unexpected error while creating a Conversation',
                SQLERRM,
                p_RESULT_CODE,
                p_RESULT_DESCRIPTION
              );

  END putConversationStatus;


---------------------------------------------------------------------------------------------------
/* DEPRECATED - Belongs to COMMONS / NOT USED
PROCEDURE getCapabilityCode(
  p_SERVICE_CODE    IN VARCHAR2,
  p_CAPABILITY_NAME IN VARCHAR2,
  p_CAPABILITY_CODE OUT VARCHAR2
) AS

  v_SERVICE_ID NUMBER;
  v_SERVICE_ID NUMBER;

  v_locator VARCHAR(255);
  v_SOURCE_ERROR_CODE VARCHAR(50);
  v_SOURCE_ERROR_DESC VARCHAR(255);

  BEGIN

    v_locator := 'ESB_CONVERSATION_MANAGER_PKG.getCapabilityCode - EXEC - ESB_COMMONS_PKG.GET_SERVICE_ID_BY_CODE';
    v_SOURCE_ERROR_CODE := '0';
    v_SOURCE_ERROR_DESC := 'OK';

    v_SERVICE_ID :=

    SELECT EC.CODE INTO p_CAPABILITY_CODE FROM ESB_CAPABILITY EC
    --SELECT EC.ID INTO p_CAPABILITY_CODE FROM ESB_CAPABILITY EC
    WHERE
      EC.SERVICE_ID = (SELECT ID FROM esb_service WHERE CODE = p_SERVICE_CODE)
    AND
      EC.NAME = p_CAPABILITY_NAME;

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_CAPABILITY_CODE := '-1';
  END;
*/
---------------------------------------------------------------------------------------------------
PROCEDURE getSOAPAction(
    p_CAPABILITY_ID       IN NUMBER,
    p_SOAP_ACTION         OUT VARCHAR2) AS

    BEGIN

      SELECT
        DETAIL_CONTENT
          INTO p_SOAP_ACTION
      FROM
        ESB_CAPABILITY_DETAILS
      WHERE
        CAPABILITY_ID = p_CAPABILITY_ID AND
        DETAIL_TYPE_ID = (
          SELECT ID
          FROM ESB_CAPABILITY_DETAILS_TYPE
          WHERE DESCRIPTION = 'SOAP Action' AND
                RCD_STATUS <> 2
        ) AND
        RCD_STATUS <> 2;

      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_SOAP_ACTION := '-1';

    END;

---------------------------------------------------------------------------------------------------
PROCEDURE getConsumerCallbackURLLessCap(
    p_CONSUMER_ID         IN NUMBER,
    p_TRANSPORT           OUT VARCHAR2,
    p_CALLBACK_URL        OUT VARCHAR2,
    p_RESULT_CODE         OUT VARCHAR2,
    p_RESULT_DESCRIPTION  OUT VARCHAR2) AS

    BEGIN

        SELECT
          ECCD.DETAIL_CONTENT
            INTO
              p_TRANSPORT
        FROM
          ESB_CONSUMER_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (
            SELECT ID
            FROM ESB_CONSUMER_DETAILS_TYPE
            WHERE NAME = 'TRANSPORT'  AND
                  RCD_STATUS <> 2
          ) AND
          ECCD.consumer_id = p_CONSUMER_ID AND
          ECCD.RCD_STATUS <> 2;

        SELECT
          ECCD.DETAIL_CONTENT
            INTO
              p_CALLBACK_URL
        FROM ESB_CONSUMER_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (
            SELECT ID
            FROM ESB_CONSUMER_DETAILS_TYPE
            WHERE NAME = 'URL' AND
                  RCD_STATUS <> 2
          ) AND
          ECCD.consumer_id = p_CONSUMER_ID AND
          ECCD.RCD_STATUS <> 2;

      p_RESULT_CODE := '0';
      p_RESULT_DESCRIPTION := 'Ejecucion exitosa.';

      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_RESULT_CODE := '-99';
        p_RESULT_DESCRIPTION := 'NO SE HAN ENCONTRADO DATOS. '  || SQLERRM;

       WHEN OTHERS THEN
        p_RESULT_CODE := '-98';
        p_RESULT_DESCRIPTION := 'SE HA PRODUCIDO UN ERROR EN LA BUSQUEDA DE LA URI. ' || SQLERRM;

    END;

---------------------------------------------------------------------------------------------------

PROCEDURE getConsumerCallbackURLWithCap(
    p_CONSUMER_ID         IN NUMBER,
    p_CAPABILITY_ID       IN NUMBER,
    p_TRANSPORT           OUT VARCHAR2,
    p_CALLBACK_URL        OUT VARCHAR2,
    p_SOAP_ACTION         OUT VARCHAR2,
    p_RESULT_CODE         OUT VARCHAR2,
    p_RESULT_DESCRIPTION  OUT VARCHAR2) AS

    BEGIN

        getSOAPAction(p_CAPABILITY_ID, p_SOAP_ACTION);

        SELECT
          ECCD.DETAIL_CONTENT
            INTO
              p_TRANSPORT
        FROM
          ESB_CONSUMER_CAP_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (
            SELECT ID
            FROM ESB_CONSUMER_CAP_DETAILS_TYPE
            WHERE NAME = 'TRANSPORT'  AND
                  RCD_STATUS <> 2
          ) AND
          ECCD.consumer_id = p_CONSUMER_ID AND
          ECCD.capability_id = p_CAPABILITY_ID AND
          ECCD.RCD_STATUS <> 2;


        SELECT
          ECCD.DETAIL_CONTENT
            INTO
              p_CALLBACK_URL
        FROM
          ESB_CONSUMER_CAP_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (
            SELECT ID
            FROM ESB_CONSUMER_CAP_DETAILS_TYPE
            WHERE NAME = 'CALLBACK_URL'  AND
                  RCD_STATUS <> 2
          ) AND
          ECCD.consumer_id = p_CONSUMER_ID AND
          ECCD.capability_id = p_CAPABILITY_ID AND
          ECCD.RCD_STATUS <> 2;

      p_RESULT_CODE := '0';
      p_RESULT_DESCRIPTION := 'Ejecucion exitosa.';

      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        getConsumerCallbackURLLessCap(p_CONSUMER_ID, p_TRANSPORT, p_CALLBACK_URL, p_RESULT_CODE, p_RESULT_DESCRIPTION);

      WHEN OTHERS THEN
       p_RESULT_CODE := '-98';
       p_RESULT_DESCRIPTION := 'SE HA PRODUCIDO UN ERROR EN LA BUSQUEDA DE LA URI. ' || SQLERRM;

    END;



---------------------------------------------------------------------------------------------------
/* DEPRECATED - Belongs to COMMONS
  PROCEDURE existsConsumer(
    p_SYSTEM_CODE         IN VARCHAR2,
    p_COUNTRY_CODE        IN VARCHAR2,
    p_ENTERPRISE_CODE     IN VARCHAR2,
    p_CONSUMER_ID OUT NUMBER
  )AS

  BEGIN
          SELECT ID INTO p_CONSUMER_ID FROM esb_consumer WHERE ESB_CONSUMER.syscode = (SELECT ID FROM esb_system WHERE CODE = p_SYSTEM_CODE)
                                        AND ESB_CONSUMER.country_id = (SELECT ID FROM esb_country WHERE CODE = p_COUNTRY_CODE)
                                        AND ESB_CONSUMER.ent_code = (SELECT ID FROM esb_enterprise WHERE CODE = p_ENTERPRISE_CODE);
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
          p_CONSUMER_ID := -1;

  END;
*/
---------------------------------------------------------------------------------------------------

  PROCEDURE getSequenceStatusConversation(
    p_CONVERSATION_ID      IN VARCHAR2,
    p_CONVERSATION_STATUS  OUT VARCHAR2,
    p_SEQ_STATUS           OUT NUMBER
  )
  IS

    P_RSP_MSG CLOB;
    P_CAN_ERROR_ID NUMBER;

    BEGIN

      getConversationStatus(p_CONVERSATION_ID, p_CONVERSATION_STATUS, P_RSP_MSG, P_CAN_ERROR_ID);
      getSequenceStatus(p_CONVERSATION_ID, p_SEQ_STATUS);

    END;

---------------------------------------------------------------------------------------------------

  PROCEDURE getSequenceStatus(
    p_CONVERSATION_ID      IN VARCHAR2,
    p_SEQ_STATUS   OUT NUMBER
  )
  IS
    BEGIN

      SELECT
        SEQUENCE
          INTO
            p_SEQ_STATUS
      FROM
        ESB_CONVERSATION
      WHERE
        CONVERSATION_ID = p_CONVERSATION_ID AND
        RCD_STATUS <> 2;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_SEQ_STATUS := -1;

    END;

  /*---------------------------------------------------------------------------------------------------

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  
  FUNCTION GET_CONVERSATION_INFO(
    p_CONV_ID             IN  NUMBER,
    p_Locator             OUT VARCHAR2,
    p_SOURCE_ERROR_CODE   OUT VARCHAR2,
    p_SOURCE_ERROR_DESC   OUT VARCHAR2
  ) RETURN CONVERSATION_T
  IS
    v_CONVERSATION CONVERSATION_T;
  BEGIN
    
    p_SOURCE_ERROR_DESC := 'Get Conversation by ID';
    p_SOURCE_ERROR_CODE := '0';
    p_Locator := 'ESB_CONVERSATION_MANAGER_PKG.GET_TX_INSTANCE - INIT';
    
    v_CONVERSATION := CONVERSATION_T(NULL,NULL,NULL,NULL,NULL,NULL);
    
    SELECT C.MESSAGE_TX_ID, C.CAPABILITY_ID, C.CONVERSATION_ID, C.CHANNEL_ID, C.CONSUMER_ID, C.SEQUENCE 
      INTO v_CONVERSATION.MESSAGE_TX_ID_, v_CONVERSATION.CAPABILITY_ID_, v_CONVERSATION.CONVERSATION_ID_, v_CONVERSATION.CHANNEL_ID_, v_CONVERSATION.CONSUMER_ID_, v_CONVERSATION.SEQUENCE_
    FROM 
      ESB_CONVERSATION C
    WHERE C.ID = p_CONV_ID;
    
    RETURN v_CONVERSATION;
    
  EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Conversation ID]= ' || p_CONV_ID ,
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        RETURN NULL;
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        RETURN NULL;
  END;

  /*---------------------------------------------------------------------------------------------------

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  
  FUNCTION GET_TX_INSTANCE(
    p_EVENT_ID            IN  VARCHAR2,
    p_PROCESS_ID          IN  VARCHAR2,
    p_Locator             OUT VARCHAR2,
    p_SOURCE_ERROR_CODE   OUT VARCHAR2,
    p_SOURCE_ERROR_DESC   OUT VARCHAR2
  ) RETURN NUMBER
  IS
    v_TX_ID NUMBER;
  BEGIN
    
    p_SOURCE_ERROR_DESC := 'Get Main Transaction Instance';
    p_SOURCE_ERROR_CODE := '0';
    p_Locator := 'ESB_CONVERSATION_MANAGER_PKG.GET_TX_INSTANCE - INIT';
    
    SELECT TX_ID
      INTO v_TX_ID
    FROM (
      SELECT MT.ID AS TX_ID
      FROM ESB_MESSAGE_TRANSACTION MT 
        INNER JOIN ESB_CONVERSATION C ON MT.ID = C.MESSAGE_TX_ID
        INNER JOIN ESB_TRACE T ON C.ID = T.CONV_ID
      WHERE 
        MT.EVENT_ID = p_EVENT_ID AND
        COALESCE(MT.PROC_ID, './@') = COALESCE(p_PROCESS_ID, './@')
      ORDER BY T.LOG_TIMESTAMP)
    WHERE ROWNUM = 1;
    
    RETURN v_TX_ID;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Event ID]= ' || p_EVENT_ID ||
                                                '[Process ID] = ' || p_PROCESS_ID,
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        RETURN '';
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        RETURN '';
  END;
  
  /*---------------------------------------------------------------------------------------------------

    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3

  */---------------------------------------------------------------------------------------------------
  
  FUNCTION GET_TX_INSTANCE_OWNER(
    p_TRANSACTION_ID      NUMBER, 
    p_Locator             OUT VARCHAR2,
    p_SOURCE_ERROR_CODE   OUT VARCHAR2,
    p_SOURCE_ERROR_DESC   OUT VARCHAR2
  ) RETURN CONVERSATION_T
  IS
    v_CONVERSATION_T CONVERSATION_T;
    v_CONV_ID NUMBER;
  BEGIN
    p_SOURCE_ERROR_DESC := 'Get Main Transaction Instance Owner';
    p_SOURCE_ERROR_CODE := '0';
    p_Locator := 'ESB_CONVERSATION_MANAGER_PKG.GET_TX_INSTANCE_OWNER - INIT';

    SELECT CONV_ID
      INTO v_CONV_ID
    FROM (
      SELECT C.ID AS CONV_ID
      FROM ESB_CONVERSATION C
        INNER JOIN ESB_TRACE T ON C.ID = T.CONV_ID
      WHERE 
        C.MESSAGE_TX_ID = p_TRANSACTION_ID
      ORDER BY T.LOG_TIMESTAMP)
    WHERE ROWNUM = 1;

    RETURN GET_CONVERSATION_INFO(
        v_CONV_ID,
        p_Locator,
        p_SOURCE_ERROR_CODE,
        p_SOURCE_ERROR_DESC
      );
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [Message Transaction ID]= ' || p_TRANSACTION_ID,
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        RETURN NULL;
      WHEN OTHERS THEN
        ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
        RETURN NULL;
END;

PROCEDURE getInfo(
    p_CONVERSATION_ID IN VARCHAR2,
    p_CONV_TYPE     OUT VARCHAR2,
    p_CONV_SEQUENCE     OUT VARCHAR2,
    p_CONV_SERVICE    OUT VARCHAR2,
    p_CONV_CAPABILITY     OUT VARCHAR2,
    p_CONV_STATUS     OUT VARCHAR2,
    p_CONV_TX_TYPE    OUT VARCHAR2,
    p_CONV_TX_SEQUENCE    OUT VARCHAR2,
    p_CONV_TX_EVENT_ID    OUT VARCHAR2,
    p_CONV_TX_PROCESS_ID    OUT VARCHAR2,
    p_CONV_TX_CORR_ID     OUT VARCHAR2,
    p_CONV_TX_STATUS    OUT VARCHAR2,
    p_RESULT_CODE OUT VARCHAR2,
    p_RESULT_DESC OUT VARCHAR2
    ) IS

    /*
    Este procedimiento tiene como objetivo retornar informacion asociada a la conversacion enviada por parametro.

    * v1.0 @ 13/7/2016 ~ Mario Mesaglio

    Conceptos :

    Las Transacciones son definidas, logicamente, como la relacion entre un Consumidor, un Mensaje, y una Capacidad de Servicio.
    Toda ejecucion de una capacidad de servicio, genera una Conversacion que representa univocamente esa instancia de ejecucion.

    Consumidor --Mensaje--> Capacidad (Servicio) ~ Conversacion

    Llamamos a esta Transaccion, como "Main Transaction".
    Llamamos a esta capacidad como "Main Transaction Capability".
    Llamamos a esta Conversacion como "Main Transaction Owner".

    Se denomina como "Main Transaction Owner" debido a que, la conversacion, es el unico actor responsable de determinar el resultado
    funcional de la Main Transaction. Solo cuando la Main Transaction Owner se determina a si mismo como Exitosa, se puede considerar
    la Main Transaction asociada como FUNCIONALMENTE exitosa. Lo mismo aplica para casos donde no se haya considerado como exitosa.

    Ahora bien, el Framework permite la instanciacion de una Main Transaction, en forma de uno a varios registros correlativos
    en la tabla ESB_MESSAGE_TRANSACTION.

    Las instancias de una misma transaccion comparten, al menos, la siguiente informacion: EVENT_ID y PROCESS_ID.

    Cada instancia de transaccion percibe de una SEQUENCE (campo en la tabla) distinta y determinada de manera incremental
    para cada una, comenzando siempre por el primer registro con SEQUENCE 0. Cada Instancia de Transaccion, a su vez, puede percibir
    de varias Conversaciones, tambien identificadas mediante un SEQUENCE (@ESB_CONVERSATION) comenzando siempre por el primer registro con
    SEQUENCE 0. Cada instancia de transaccion, percibe de un unico CORRELATION_ID. Dos instancias de Transaccion nunca van a tener un mismo valor
    para este campo.

    Llamamos a las instancias de transaccion como "Transaction Instance". La primer conversacion (SEQUENCE = 0 @ ESB_CONVERSATION) asociada a
    cada Transaction Instance, es siempre denominada "Transaction Instance Owner", y finalmente el resto de las conversaciones asociadas
    (SEQUENCE != 0 @ ESB_CONVERSATION) son siempre denominadas como "Transaciton Instance Member".

    Por ello, la primer Transaction Instance de una misma Main Transaction tiene siempre como SEQUENCE el valor 0,
    y un CORRELATION_ID determinado por el Consumidor del Servicio. El CORRELATION_ID podra tambien ser omitido en este caso.

    Entonces :
    --> La Transaction Instance de SEQUENCE = 0  se denomina  como "Main Transaction Instance", al ser la que representa mas fehacientemente
        el estado original de la Main Transaciton asociada.
    --> La Transaction Instance Owner de la Main Transaction Instance se denomina como "Main Transaction Instance Owner",
        al ser la que representa mas fehacientamente el estado original de la Main Transaction Owner.

    El resto de las Transaction Instance pueden ser sub-categorizadas en los siguientes tipos :

    1 - "Main Transaction Clone"

        Representa un re-proceso de la Main Transaction Instance.

        Este esenario puede ser dado por acciones del Framework (Ej.: Tratamiento de Reintentos), o bien por la re-emision de una
        Main Transaction (Mismo EVENT_ID y PROCESS_ID, a la misma Capacidad) con CORRELATION_ID distinto por un Consumidor de Servicio.
        Una Main Transacion Clone percibe de cualidades propias de una Main Transaction Instance.
        Ej.: Validaciones x Duplicidad, Tratamiento de Errores, Agrupacion de Logs, etc.

        Para generar una Main Transaction Clone, se debe ejecutar la misma Capacidad de Servicio involucrada originalmente en la
        Main Transaction, con los mismos EVENT_ID y PROCESS ID; pero con un CORRELATION_ID diferente.

          --> De esta manera trabaja el tratamiento de reintentos, donde en cada reintento se envia un CORRELATION_ID distinto.

        Una Transaction Instance es una Main Transaction Clone, si solo si :

          A - Su SEQUENCE (@ ESB_MESSAGE_TRANSACTION) != 0.
          B - Su CORRELATION_ID =! al CORRELATION_ID de la Main Transaction Instance.
          C - Su Transaction Instance Owner, tiene la misma CAPABILITY_ID (@ESB_CONVERSATION) que la Main Transaction Instance Owner.

    ~~~~~~~~~~ RESUMEN :

    Tipos de Transaction Instance (@ESB_MESSAGE_TRANSACTION) :

    --> Main Transaction Instance
    --> Main Transaction Clone

    Tipos de Transaction Owner (@ESB_CONVERSATION) :

    --> Main Transaction Owner
    --> Main Transaction Member

    ~~~~~~~~~~ IMPORTANTE :

    --> Cualquier otra Transaction Instance cuyo tipo difiera de los antes mencionados, no es soportada por el FRW al dia de la fecha.

    --> Cualquier otra Transaction Instance Owner cuyo tipo difiera de los antes mencionados, no es soportada por el FRW al dia de la fecha.

    --> La creacion de Transaction Instance y Transaction Instance Owner de tipo diferente de los antes mencionados, puede causar comportamiento
        inesperado por parte del FRW.

    -----------------------------------------------------------------------------------------------------------------------------

    */

    p_CONV_CAP_ID             NUMBER; -- ID @ ESB_CAPABILITY (Capacidad Asociada) de la Conversacion enviada por parametro.

    p_CONV_TX_ID              NUMBER; -- ID @ ESB_MESSAGE_TRANSACTION de la Transaction Instance asociada a la Conversacion enviada por parametro.

    p_MT_INST_OWN_CONV_CAP_ID NUMBER; -- ID @ ESB_CONVERSATION de la Main Transaction Instance Owner
    p_MT_INST_TX_ID           NUMBER; -- ID @ ESB_MESSAGE_TRANSACTION de la Main Transaction Instance
    p_MT_INST_TX_CORR_ID      VARCHAR2(100); -- ID @ ESB_MESSAGE_TRANSACTION de la Main Transaction Instance
  
    v_RSP_MSG                 CLOB;
    v_CAN_ERROR_ID            VARCHAR2(50);
    v_CAPABILITY_CODE         VARCHAR(255);
    v_SERVICE_CODE            VARCHAR(255); 
    v_TRANSACTION_CONV_ID     TRANSACTION_T;
    v_CONVERSATION            CONVERSATION_T;
    v_Locator                 VARCHAR(255);
    v_SOURCE_ERROR_CODE       VARCHAR(50);
    v_SOURCE_ERROR_DESC       VARCHAR(255);

    BEGIN

      BEGIN
        ---------------------------------------------------------------------------------------------------------------------------------------------------
        -- Obtenemos informacion detallada de la conversacion, y Transaction Instance asociada.
        ---------------------------------------------------------------------------------------------------------------------------------------------------
        SELECT
          C.MESSAGE_TX_ID,
          C.SEQUENCE,
          C.CAPABILITY_ID
            INTO p_CONV_TX_ID,p_CONV_SEQUENCE,p_CONV_CAP_ID
        FROM ESB_CONVERSATION C
        WHERE
          C.CONVERSATION_ID = p_CONVERSATION_ID AND
          ROWNUM = 1;

        ESB_COMMONS_PKG.GET_CAPABILITY_BY_ID (
          p_CONV_CAP_ID,
          P_CONV_CAPABILITY,
          v_CAPABILITY_CODE,
          P_CONV_SERVICE,
          v_SERVICE_CODE,
          v_Locator,
          v_SOURCE_ERROR_CODE, 
          v_SOURCE_ERROR_DESC
        );

        GET_CONVERSATION_STATUS(
          p_CONVERSATION_ID, 
          p_CONV_STATUS, 
          v_RSP_MSG, 
          v_CAN_ERROR_ID, 
          v_Locator, 
          v_SOURCE_ERROR_CODE, 
          v_SOURCE_ERROR_DESC
        );
        
        IF v_SOURCE_ERROR_CODE <> '0' THEN p_CONV_STATUS := 'UNK'; END IF;

        ESB_MESSAGEMANAGER_PKG.GET_TRANSACTION_INFO_BY_ID(
          p_CONV_TX_ID,
          v_TRANSACTION_CONV_ID,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
        ) ;

        p_CONV_TX_SEQUENCE:= v_TRANSACTION_CONV_ID.SEQUENCE_ ;
        p_CONV_TX_EVENT_ID:= v_TRANSACTION_CONV_ID.EVENT_ID_;
        p_CONV_TX_PROCESS_ID:= v_TRANSACTION_CONV_ID.PROCESS_ID_;
        p_CONV_TX_CORR_ID:= v_TRANSACTION_CONV_ID.CORRELATION_ID_;
        p_CONV_TX_STATUS:= v_TRANSACTION_CONV_ID.STATUS_;

        ---------------------------------------------------------------------------------------------------------------------------------------------------

        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_RESULT_CODE := '-1';
          p_RESULT_DESC := 'No se encontraron datos para la Conversacion enviada por parametro INIT';
          RETURN;
      END;

      ---------------------------------------------------------------------------------------------------------------------------------------------------
      -- Obtenemos la Main Transaction Instance y la Main Tranaction Instance Owner, para la Main Transaction identificada segun los EVENT_ID y PROCESS_ID
      ---------------------------------------------------------------------------------------------------------------------------------------------------
      BEGIN
        p_MT_INST_TX_ID := GET_TX_INSTANCE(
          p_CONV_TX_EVENT_ID,
          p_CONV_TX_PROCESS_ID,
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
        );
        
        IF v_SOURCE_ERROR_CODE <> '0' THEN RAISE NO_DATA_FOUND; END IF;
        
        SELECT 
          M.CORRELATION_ID INTO p_MT_INST_TX_CORR_ID
        FROM 
          ESB_MESSAGE_TRANSACTION M
        WHERE 
          M.ID = p_MT_INST_TX_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_RESULT_CODE := '-2';
          p_RESULT_DESC := 'No se encontraron datos para la Conversacion enviada por parametro';
          RETURN;

        WHEN TOO_MANY_ROWS THEN
          p_RESULT_CODE := '-3';
          p_RESULT_DESC := 'La Transaccion asociada no se corresponde con ningun tipo de Transaction Instance definido.';
          RETURN;
      END;

     BEGIN
     
      v_CONVERSATION := GET_TX_INSTANCE_OWNER(
        p_MT_INST_TX_ID,
        v_Locator,
        v_SOURCE_ERROR_CODE,
        v_SOURCE_ERROR_DESC
      );
      
      IF v_SOURCE_ERROR_CODE = '1' THEN RAISE NO_DATA_FOUND; END IF; 
      
      p_MT_INST_OWN_CONV_CAP_ID := v_CONVERSATION.CAPABILITY_ID_;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_RESULT_CODE := '-1';
          p_RESULT_DESC := 'No se encontraron datos para la Conversacion enviada por parametro';
          RETURN;

        WHEN TOO_MANY_ROWS THEN
          p_RESULT_CODE := '-2';
          p_RESULT_DESC := 'El tipo de a Conversacion no se corresponde con ningun tipo de Transaction Owner definido.';
          RETURN;
      END;
      ---------------------------------------------------------------------------------------------------------------------------------------------------

       ---------------------------------------------------------------------------------------------------------------------------------------------------
      -- Determinamos el Tipo de la Conversacion enviada por parametro
      ---------------------------------------------------------------------------------------------------------------------------------------------------
      IF (p_CONVERSATION_ID = v_CONVERSATION.CONVERSATION_ID_) THEN
        p_CONV_TYPE := 'OWN'; -- Main Transaction Owner
      ELSE
        p_CONV_TYPE := 'MEM'; -- Main Transaction Member (Al 14/07/2016 no existen otros tipos)
      END IF;

      IF (p_CONV_TYPE IS NULL) THEN
        p_RESULT_CODE := '-2';
        p_RESULT_DESC := 'El tipo de a Conversacion no se corresponde con ningun tipo de Transaction Owner definido.';
        RETURN;

      END IF;

      ---------------------------------------------------------------------------------------------------------------------------------------------------

      ---------------------------------------------------------------------------------------------------------------------------------------------------
      -- Determinamos el Tipo de Transaction Instance asociada.
      ---------------------------------------------------------------------------------------------------------------------------------------------------
      IF (p_CONV_TX_SEQUENCE = 0) THEN
           p_CONV_TX_TYPE := 'MTI'; -- Main Transaction Instance
      ELSE
        IF(
          p_MT_INST_TX_CORR_ID <> p_CONV_TX_CORR_ID AND -- != CorrelationID
          p_CONV_CAP_ID = p_MT_INST_OWN_CONV_CAP_ID -- = Capacidad
            ) THEN
          p_CONV_TX_TYPE := 'MTC'; -- Main Transaction Clone
        END IF;
      END IF;

      IF (p_CONV_TX_TYPE IS NULL) THEN
        p_RESULT_CODE := '-3';
        p_RESULT_DESC := 'La Transaccion asociada no se corresponde con ningun tipo de Transaction Instance definido.';
        RETURN;

      END IF;

      ---------------------------------------------------------------------------------------------------------------------------------------------------

      p_RESULT_CODE := '0';
      p_RESULT_DESC := '';
      
      EXCEPTION
         WHEN OTHERS THEN
            p_RESULT_CODE := '-4';
            p_RESULT_DESC := 'Error inesperado';
            RETURN;
    END;

---------------------------------------------------------------------------------------------------
PROCEDURE getConsumerCallbackURL(
    p_SYSTEM_CODE         IN VARCHAR2,
    p_COUNTRY_CODE        IN VARCHAR2,
    p_ENTERPRISE_CODE     IN VARCHAR2,
    p_SERVICE_CODE        IN VARCHAR2,
    p_CAPABILITY_NAME     IN VARCHAR2,
    p_TRANSPORT           OUT VARCHAR2,
    p_CALLBACK_URL        OUT VARCHAR2,
    p_SOAP_ACTION         OUT VARCHAR2,
    p_RESULT_CODE         OUT VARCHAR2,
    p_RESULT_DESCRIPTION  OUT VARCHAR2
) AS

    v_CONSUMER_ID NUMBER;
    v_SERVICE_ID NUMBER;
    v_CAPABILITY_ID NUMBER;

    v_locator VARCHAR(255);
    v_SOURCE_ERROR_CODE VARCHAR(50);
    v_SOURCE_ERROR_DESC VARCHAR(255);

    BEGIN

    v_locator := 'ESB_CONVERSATION_MANAGER_PKG.getConsumerCallbackURL - EXEC - ESB_COMMONS_PKG.GET_CONSUMER_ID_BY_CODES';
    v_SOURCE_ERROR_CODE := '0';
    v_SOURCE_ERROR_DESC := 'OK';

    v_CONSUMER_ID := ESB_COMMONS_PKG.GET_CONSUMER_ID_BY_CODES(
      p_SYSTEM_CODE,
      p_COUNTRY_CODE,
      p_ENTERPRISE_CODE,
      v_locator,
      v_SOURCE_ERROR_CODE,
      v_SOURCE_ERROR_DESC
    );

    IF(v_SOURCE_ERROR_CODE!=0) THEN RAISE_APPLICATION_ERROR(-20001+TO_NUMBER(v_SOURCE_ERROR_CODE), v_SOURCE_ERROR_DESC); END IF;

    v_SERVICE_ID := ESB_COMMONS_PKG.GET_SERVICE_ID_BY_CODE(
      p_SERVICE_CODE,
      v_locator,
      v_SOURCE_ERROR_CODE,
      v_SOURCE_ERROR_DESC
    );

    IF(v_SOURCE_ERROR_CODE=0) THEN
      v_CAPABILITY_ID := ESB_COMMONS_PKG.GET_CAPABILITY_ID(
        v_SERVICE_ID,
        p_CAPABILITY_NAME,
        v_Locator,
        v_SOURCE_ERROR_CODE,
        v_SOURCE_ERROR_DESC
      );
    END IF;

    IF(v_SOURCE_ERROR_CODE=0) THEN
      getConsumerCallbackURLWithCap(
        v_CONSUMER_ID,
        v_CAPABILITY_ID,
        p_TRANSPORT,
        p_CALLBACK_URL,
        p_SOAP_ACTION,
        p_RESULT_CODE,
        p_RESULT_DESCRIPTION);
    ELSE
      getConsumerCallbackURLLessCap(
        v_CONSUMER_ID,
        p_TRANSPORT,
        p_CALLBACK_URL,
        p_RESULT_CODE,
        p_RESULT_DESCRIPTION);
    END IF;

    EXCEPTION
      WHEN OTHERS THEN
        p_RESULT_CODE := '-1';
        p_RESULT_DESCRIPTION := 'CONSUMIDOR NO REGISTRADO. '  || SQLERRM;

    END;

END ESB_CONVERSATION_MANAGER_PKG;