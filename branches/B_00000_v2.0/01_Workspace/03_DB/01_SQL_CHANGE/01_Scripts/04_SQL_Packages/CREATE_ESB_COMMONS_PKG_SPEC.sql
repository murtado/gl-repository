create or replace PACKAGE "ESB_COMMONS_PKG" AS 

  PROCEDURE LOAD_SOURCE_ERROR(
      p_Locator               IN VARCHAR,
      p_ErrCode               IN VARCHAR,
      p_Reason                IN VARCHAR,
      p_SqlErr                IN VARCHAR,
      p_SOURCE_ERROR_CODE     OUT VARCHAR,
      p_SOURCE_ERROR_DESC     OUT VARCHAR
  );
  
  FUNCTION CREATE_COUNTRY(
    p_COUNTRY_CODE            IN VARCHAR2, 
    p_COUNTRY_NAME            IN VARCHAR2,
    p_DESCRIPTION             IN VARCHAR2,
    p_RCD_STATUS              IN NUMBER,
    p_Locator                 OUT VARCHAR, 
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;
  
  FUNCTION GET_COUNTRY_ID(
    p_COUNTRY_CODE            IN VARCHAR2, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;
  
  FUNCTION CREATE_CHANNEL(
    p_CHANNEL_CODE            IN VARCHAR2, 
    p_CHANNEL_NAME            IN VARCHAR2, 
    p_RCD_STATUS              IN NUMBER, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION GET_CHANNEL_ID_BY_NAME(
    p_CHANNEL_NAME            IN VARCHAR2, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION CREATE_SYSTEM(
    p_SYSTEM_CODE             IN VARCHAR2, 
    p_SYSTEM_NAME             IN VARCHAR2, 
    p_DESCRIPTION             IN VARCHAR2, 
    p_RCD_STATUS              IN NUMBER,
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;
  
  FUNCTION GET_SYSTEM_ID(
    p_SYSTEM_CODE             IN VARCHAR2, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;
  
  FUNCTION GET_DEF_SYSTEM_ID_FRW(
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;
  
  FUNCTION CREATE_ENTERPRISE(
    p_ENTERPRISE_CODE         IN VARCHAR2, 
    p_ENTERPRISE_NAME         IN VARCHAR2, 
    p_RCD_STATUS              IN NUMBER,
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION GET_ENTERPRISE_ID(
    p_ENTERPRISE_CODE         IN VARCHAR2, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION CREATE_CONSUMER(
    p_SYSTEM_ID               IN NUMBER, 
    p_COUNTRY_ID              IN NUMBER, 
    p_ENTERPRISE_ID           IN NUMBER,
    p_RCD_STATUS              IN NUMBER,
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;
  
  FUNCTION GET_CONSUMER_ID_BY_CODES(
    p_SYSTEM_CODE             IN VARCHAR2, 
    p_COUNTRY_CODE            IN VARCHAR2, 
    p_ENTERPRISE_CODE         IN VARCHAR2, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION GET_CONSUMER_ID(
    p_SYSTEM_ID               IN NUMBER, 
    p_COUNTRY_ID              IN NUMBER, 
    p_ENTERPRISE_ID           IN NUMBER, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION CREATE_CAPABILITY(
    p_SERVICE_ID              IN NUMBER, 
    p_CAPABILITY_CODE         IN VARCHAR2, 
    p_CAPABILITY_NAME         IN VARCHAR2, 
    p_RCD_STATUS              IN NUMBER,
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION GET_CAPABILITY_ID(
    p_SERVICE_ID              IN NUMBER, 
    p_CAPABILITY_NAME         IN VARCHAR2, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION GET_CAPABILITY_ID_BY_CODE(
    p_CAPABILITY_CODE         IN VARCHAR2, 
    p_CAPABILITY_NAME         IN VARCHAR2, 
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION CREATE_SERVICE(
    p_SERVICE_CODE            IN VARCHAR2, 
    p_SERVICE_NAME            IN VARCHAR2, 
    p_RCD_STATUS              IN NUMBER,
    p_Locator                 OUT VARCHAR,
    p_SOURCE_ERROR_CODE       OUT VARCHAR, 
    p_SOURCE_ERROR_DESC       OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION GET_SERVICE_ID(
    p_SERVICE_CODE                IN VARCHAR2, 
    p_SERVICE_NAME                IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) RETURN NUMBER;

  FUNCTION GET_SERVICE_ID_BY_CODE(
    p_SERVICE_CODE                IN VARCHAR2, 
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  ) RETURN NUMBER;
  
  PROCEDURE GET_SERVICE_BY_ID (
    p_SERVICE_ID                  IN  NUMBER,
    p_SERVICE_NAME                OUT VARCHAR2,
    p_SERVICE_CODE                OUT VARCHAR2,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  );

  PROCEDURE GET_CAPABILITY_BY_ID (
    p_CAPABILITY_ID               IN  NUMBER,
    p_CAPABILITY_NAME             OUT VARCHAR2,
    p_CAPABILITY_CODE             OUT VARCHAR2,
    p_SERVICE_NAME                OUT VARCHAR2,
    p_SERVICE_CODE                OUT VARCHAR2,
    p_Locator                     OUT VARCHAR,
    p_SOURCE_ERROR_CODE           OUT VARCHAR, 
    p_SOURCE_ERROR_DESC           OUT VARCHAR
  );
END ESB_COMMONS_PKG;