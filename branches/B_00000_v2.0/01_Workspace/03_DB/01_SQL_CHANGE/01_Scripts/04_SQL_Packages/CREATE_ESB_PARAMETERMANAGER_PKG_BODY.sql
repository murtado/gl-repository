--------------------------------------------------------
--  DDL for Package Body ESB_PARAMETERMANAGER_PKG
--------------------------------------------------------
create or replace PACKAGE BODY ESB_PARAMETERMANAGER_PKG AS


  /*---------------------------------------------------------------------------------------------------
    
    ****

    OK Outcome      --> p_SOURCE_ERROR.CODE_ = 0 | p_SOURCE_ERROR.DESCRIPTION_ = 'Transaccion exitosa.'
    NO DATA FOUND   --> p_SOURCE_ERROR.CODE_ = 1 | p_SOURCE_ERROR.DESCRIPTION_ = 'Hay parametros faltantes en el repositorio.'
                    --> p_SOURCE_ERROR.CODE_ = 2 | p_SOURCE_ERROR.DESCRIPTION_ = 'Existen claves sin valor asignado.'
                    --> p_SOURCE_ERROR.CODE_ = 3 | p_SOURCE_ERROR.DESCRIPTION_ = 'No se encontraron los parametros especificados en el repositorio.'
    TOO MANY ROWS   --> 
    UNHANDLED ERROR --> 
    
  */---------------------------------------------------------------------------------------------------
  PROCEDURE get(
    p_Keys        IN KEYS_T, 
    p_KeyValues   OUT sys_refcursor, 
    p_SourceError OUT SOURCERROR_T)
  AS
    v_KeyValues     KEYVALUETABLE_T := KEYVALUETABLE_T();
    v_KeyValue      KEYVALUE_T      := KEYVALUE_T(NULL, NULL);
    v_SourceError   SOURCERROR_T    := SOURCERROR_T(NULL, NULL);
    v_CountNulls    NUMBER          := 0;
  BEGIN
  
    FOR i IN p_Keys.FIRST .. p_Keys.LAST
    LOOP
      BEGIN
        
        SELECT 
          key, 
          value 
            INTO 
              v_KeyValue.key, 
              v_KeyValue.value
        FROM 
          esb_parameter
        WHERE 
          key = p_Keys(i) AND
          RCD_STATUS <> 2;
        
        v_KeyValues.EXTEND;
        v_KeyValues(v_KeyValues.COUNT) := KEYVALUE_T(v_KeyValue.key, v_KeyValue.value);
        
        IF (v_KeyValue.value IS NULL) THEN
          v_CountNulls := v_CountNulls + 1;
        END IF;
        
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    END LOOP;
    
    IF (v_KeyValues.COUNT = 0) THEN
      v_SourceError.CODE_:= '3';
      v_SourceError.DESCRIPTION_:= 'No se encontraron los parametros especificados en el repositorio.';
    ELSIF (v_KeyValues.COUNT < p_Keys.COUNT) THEN
      v_SourceError.CODE_:= '1';
      v_SourceError.DESCRIPTION_:= 'Hay parametros faltantes en el repositorio.';
    ELSIF (v_CountNulls > 0) THEN
      v_SourceError.CODE_:= '2';
      v_SourceError.DESCRIPTION_:= 'Existen claves sin valor asignado.';
    ELSE
      v_SourceError.CODE_:= '0';
      v_SourceError.DESCRIPTION_:= 'Transaccion exitosa.';
    END IF;
      
    p_SourceError := SOURCERROR_T(
      v_SourceError.CODE_, 
      v_SourceError.DESCRIPTION_
    );
        
    OPEN p_KeyValues FOR
      SELECT 
        KEY, 
        VALUE 
      FROM 
        TABLE(v_KeyValues);
	  
    RETURN;
  END get;

  /*---------------------------------------------------------------------------------------------------
  
    ****
    
    OK Outcome      --> p_SOURCE_ERROR_CODE = 0
    NO DATA FOUND   --> p_SOURCE_ERROR_CODE = -1
    TOO MANY ROWS   --> p_SOURCE_ERROR_CODE = -2
    UNHANDLED ERROR --> p_SOURCE_ERROR_CODE = -3
    
  */---------------------------------------------------------------------------------------------------

  FUNCTION GET_MAP_OUTPUT(
    p_MAP_IN              IN MAP_IN_T,
    p_Locator             OUT VARCHAR,
    p_SOURCE_ERROR_CODE   OUT VARCHAR, 
    p_SOURCE_ERROR_DESC   OUT VARCHAR
  ) 
    RETURN MAP_OUT_T
    IS
      v_MAP_OUT           MAP_OUT_T   := MAP_OUT_T(NULL,p_MAP_IN.SCODE_,p_MAP_IN.ENTITY_,p_MAP_IN.FIELD_);
      v_SRC_SYSTEM_ID     NUMBER;
      v_DST_SYSTEM_ID     NUMBER;
    
      BEGIN
      
        p_SOURCE_ERROR_DESC := 'The information was succesfully retrieved.';
        p_SOURCE_ERROR_CODE := '0';      
        p_Locator := 'ESB_PARAMETERMANAGER_PKG.GET_MAP_OUTPUT - INIT';

        v_SRC_SYSTEM_ID := ESB_COMMONS_PKG.GET_SYSTEM_ID(
          p_MAP_IN.SSYS_,
          p_Locator,
          p_SOURCE_ERROR_CODE,
          p_SOURCE_ERROR_DESC
        );

        IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN v_MAP_OUT; END IF;

        v_DST_SYSTEM_ID := ESB_COMMONS_PKG.GET_SYSTEM_ID(
          p_MAP_IN.DSYS_,
          p_Locator,
          p_SOURCE_ERROR_CODE,
          p_SOURCE_ERROR_DESC
        );

        IF(p_SOURCE_ERROR_CODE!=0) THEN RETURN v_MAP_OUT; END IF;
      
        SELECT 
          em.DESTINATION_CODE, 
          em.SOURCE_CODE, 
          ece.NAME,
          ecf.NAME
            INTO 
              v_MAP_OUT.DCODE_, 
              v_MAP_OUT.SCODE_, 
              v_MAP_OUT.ENTITY_, 
              v_MAP_OUT.FIELD_
        FROM 
          ESB_MAPPING em 
            INNER JOIN ESB_CDM_FIELD ecf ON (em.FIELD_ID = ecf.ID)
            INNER JOIN ESB_CDM_ENTITY ece ON (ecf.ENTITY_ID = ece.ID)
        WHERE 
          em.SOURCE_CODE = p_MAP_IN.SCODE_ AND 
          em.DESTINATION_SYSTEM = v_DST_SYSTEM_ID AND 
          em.SOURCE_SYSTEM = v_SRC_SYSTEM_ID AND 
          (ecf.NAME = p_MAP_IN.FIELD_ AND ece.NAME = p_MAP_IN.ENTITY_ OR 
            em.FIELD_ID IS NULL AND p_MAP_IN.FIELD_ IS NULL) AND 
          (em.CONTEXT = p_MAP_IN.CONTEXT_ 
            OR em.CONTEXT IS NULL AND p_MAP_IN.CONTEXT_ IS NULL) AND 
          ecf.RCD_STATUS <> 2 AND 
          ece.RCD_STATUS <> 2 AND 
          em.RCD_STATUS <> 2;
        
        RETURN v_MAP_OUT;
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-1',
            'There is no record with this info : [SourceCode]='   || p_MAP_IN.SCODE_    || 
                                            ' - [Context]='       || p_MAP_IN.CONTEXT_  ||
                                            ' - [Entity]='        || p_MAP_IN.ENTITY_   ||
                                            ' - [Field]='         || p_MAP_IN.FIELD_    ||
                                            ' - [DstSystemCode]=' || p_MAP_IN.DSYS_     ||
                                            ' - [SrcSystemCode]=' || p_MAP_IN.SSYS_     || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN v_MAP_OUT;

        WHEN TOO_MANY_ROWS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-2',
            'Duplicate records matching this info : [SourceCode]='    || p_MAP_IN.SCODE_    || 
                                                ' - [Context]='       || p_MAP_IN.CONTEXT_  ||
                                                ' - [Entity]='        || p_MAP_IN.ENTITY_   ||
                                                ' - [Field]='         || p_MAP_IN.FIELD_    ||
                                                ' - [DstSystemCode]=' || p_MAP_IN.DSYS_     ||
                                                ' - [SrcSystemCode]=' || p_MAP_IN.SSYS_     || '.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN v_MAP_OUT;
        WHEN OTHERS THEN
          ESB_COMMONS_PKG.LOAD_SOURCE_ERROR(
            p_Locator,
            '-3',
            'Unexpected Error.',
            SQLERRM,
            p_SOURCE_ERROR_CODE,p_SOURCE_ERROR_DESC
          );
          RETURN v_MAP_OUT;
  END GET_MAP_OUTPUT;

  /*---------------------------------------------------------------------------------------------------
    
    ****

    OK Outcome      --> p_SOURCE_ERROR.CODE_ = 0 | p_SOURCE_ERROR.DESCRIPTION_ = 'Transaccion exitosa.'
    NO DATA FOUND   --> p_SOURCE_ERROR.CODE_ = 1 | p_SOURCE_ERROR.DESCRIPTION_ = 'Hay parametros faltantes en el repositorio.'
                    --> p_SOURCE_ERROR.CODE_ = 2 | p_SOURCE_ERROR.DESCRIPTION_ = 'Existen claves sin valor asignado.'
                    --> p_SOURCE_ERROR.CODE_ = 3 | p_SOURCE_ERROR.DESCRIPTION_ = 'No se encontraron los parametros especificados en el repositorio.'
    TOO MANY ROWS   --> 
    UNHANDLED ERROR --> 
    
  */---------------------------------------------------------------------------------------------------
  
  PROCEDURE getMapping(
    p_MapInput    IN MAPS_IN_T, 
    p_MapOutput   OUT sys_refcursor, 
    p_SourceError OUT SOURCERROR_T)
  AS
    v_MapsOutput        MAPS_OUT_T    := MAPS_OUT_T();
    v_MapRecord         MAP_OUT_T;
    v_SourceError       SOURCERROR_T  := SOURCERROR_T(NULL, NULL);
    v_CountNulls        NUMBER        := 0;

    v_Locator           VARCHAR(255);
    v_SOURCE_ERROR_CODE VARCHAR(50)       := '0';
    v_SOURCE_ERROR_DESC VARCHAR(255);
  BEGIN
    FOR i IN p_MapInput.FIRST .. p_MapInput.LAST
    LOOP
      v_MapRecord := MAP_OUT_T(NULL, NULL, NULL, NULL);
      
      BEGIN
        
        v_MapRecord := GET_MAP_OUTPUT(
          p_MapInput(i),
          v_Locator,
          v_SOURCE_ERROR_CODE,
          v_SOURCE_ERROR_DESC
        );

        EXIT WHEN TO_NUMBER(v_SOURCE_ERROR_CODE) < -1;

        IF (v_SOURCE_ERROR_CODE = '0') THEN
          v_MapsOutput.EXTEND;
          v_MapsOutput(v_MapsOutput.COUNT) := MAP_OUT_T(v_MapRecord.DCODE_, v_MapRecord.SCODE_, v_MapRecord.ENTITY_, v_MapRecord.FIELD_);
          IF (v_MapRecord.DCODE_ IS NULL) THEN
            v_CountNulls := v_CountNulls + 1;
          END IF;
        END IF;
        
      END;
    END LOOP;
	
    IF (v_MapsOutput.COUNT = 0) THEN
      v_SourceError.CODE_:= '6';
      v_SourceError.DESCRIPTION_:= 'No se encontraron los mapeos especificados en el repositorio.';
    ELSIF (v_MapsOutput.COUNT < p_MapInput.COUNT) THEN
      v_SourceError.CODE_:= '4';
      v_SourceError.DESCRIPTION_:= 'Mapeos faltantes en el repositorio.';
    ELSIF (v_CountNulls > 0) THEN
      v_SourceError.CODE_:= '5';
      v_SourceError.DESCRIPTION_:= 'Existen claves sin valor asignado.';
    ELSE
      v_SourceError.CODE_:= '0';
      v_SourceError.DESCRIPTION_:= 'Transaccion exitosa.';
    END IF;
      
    p_SourceError := SOURCERROR_T(v_SourceError.CODE_, v_SourceError.DESCRIPTION_);    
        
    OPEN p_MapOutput FOR
      SELECT DCODE_, SCODE_, ENTITY_, FIELD_ FROM TABLE(v_MapsOutput);
      
    RETURN;
  END getMapping;

  /*---------------------------------------------------------------------------------------------------
    
    ****

    OK Outcome      --> p_SOURCE_ERROR.CODE_ = 0 | p_SOURCE_ERROR.DESCRIPTION_ = 'Transaccion exitosa.'
    NO DATA FOUND   --> p_SOURCE_ERROR.CODE_ = 1 | p_SOURCE_ERROR.DESCRIPTION_ = 'Hay parametros faltantes en el repositorio.'
                    --> p_SOURCE_ERROR.CODE_ = 2 | p_SOURCE_ERROR.DESCRIPTION_ = 'Existen claves sin valor asignado.'
                    --> p_SOURCE_ERROR.CODE_ = 3 | p_SOURCE_ERROR.DESCRIPTION_ = 'No se encontraron los parametros especificados en el repositorio.'
    TOO MANY ROWS   --> 
    UNHANDLED ERROR --> 
    
  */---------------------------------------------------------------------------------------------------

  PROCEDURE getConfig(
    p_ConfigName  IN VARCHAR2, 
    p_Config      OUT sys_refcursor, 
    p_SourceError OUT SOURCERROR_T)
  AS
    v_KeyValues KEYVALUETABLE_T := KEYVALUETABLE_T();
    v_SourceError SOURCERROR_T := SOURCERROR_T(NULL, NULL);
    v_countNotNulls NUMBER := 0;
    BEGIN

      SELECT 
        KEYVALUE_T(ecp.name, ecl.value) 
          BULK COLLECT 
            INTO v_KeyValues
      FROM 
        esb_config ec 
          INNER JOIN esb_config_list ecl      ON (ec.id           = ecl.config_id)
          INNER JOIN esb_config_property ecp  ON (ecl.property_id = ecp.id)
      WHERE 
        ec.NAME = p_ConfigName AND 
        ec.rcd_status <> 2 AND 
        ecl.rcd_status <> 2 AND 
        ecp.rcd_status <> 2;
      
      SELECT COUNT(VALUE) 
        INTO v_countNotNulls 
      FROM TABLE (v_KeyValues);
      
      IF (v_KeyValues.COUNT = 0) THEN
        v_SourceError.CODE_:= '8';
        v_SourceError.DESCRIPTION_:= 'No se encontro informacion para la configuracion en el repositorio.';
      ELSIF (v_countNotNulls < v_KeyValues.COUNT) THEN
        v_SourceError.CODE_:= '7';
        v_SourceError.DESCRIPTION_:= 'Existen propiedades no valuadas en la configuracion.';
      ELSE
        v_SourceError.CODE_:= '0';
        v_SourceError.DESCRIPTION_:= 'Transaccion exitosa.';
      END IF;
      
      p_SourceError := SOURCERROR_T(v_SourceError.CODE_, v_SourceError.DESCRIPTION_);   
          
      OPEN p_Config FOR
        SELECT KEY, VALUE FROM TABLE(v_KeyValues);
    END getConfig;

  /*---------------------------------------------------------------------------------------------------
    
    ****

    OK Outcome      --> p_SOURCE_ERROR.CODE_ = 0 | p_SOURCE_ERROR.DESCRIPTION_ = 'Transaccion exitosa.'
    NO DATA FOUND   --> p_SOURCE_ERROR.CODE_ = 1 | p_SOURCE_ERROR.DESCRIPTION_ = 'Hay parametros faltantes en el repositorio.'
                    --> p_SOURCE_ERROR.CODE_ = 2 | p_SOURCE_ERROR.DESCRIPTION_ = 'Existen claves sin valor asignado.'
                    --> p_SOURCE_ERROR.CODE_ = 3 | p_SOURCE_ERROR.DESCRIPTION_ = 'No se encontraron los parametros especificados en el repositorio.'
    TOO MANY ROWS   --> 
    UNHANDLED ERROR --> 
    
  */---------------------------------------------------------------------------------------------------

  PROCEDURE getEndpoint(
    p_Target        IN OUT TARGET_T, 
    p_Endpoint      OUT sys_refcursor, 
    p_EndpType      OUT VARCHAR2, 
    p_EndpInstance  OUT VARCHAR2, 
    p_SourceError   OUT SOURCERROR_T)
  AS
    v_KeyValues KEYVALUETABLE_T := KEYVALUETABLE_T();
    v_SourceError SOURCERROR_T := SOURCERROR_T(NULL, NULL);
    v_countNotNulls NUMBER := 0;
  BEGIN
    BEGIN
      SELECT 
        eet.name, 
        ee.instance 
        INTO 
          p_EndpType, 
          p_EndpInstance
      FROM 
        esb_endpoint ee 
        INNER JOIN esb_endpoint_transport eet     ON (ee.transport_id     = eet.id)
        INNER JOIN esb_system_api_operation esao  ON (ee.operation_id     = esao.id)
        INNER JOIN esb_system_api esa             ON (esao.system_api_id  = esa.id)
        INNER JOIN esb_system es                  ON (esa.system_id       = es.id)
      WHERE 
        esao.name = p_Target.operation_ AND 
        es.code = p_Target.provider_ AND 
        esa.code = p_Target.api_ AND 
        esao.version = p_Target.version_ AND 
        ee.rcd_status <> 2 AND 
        eet.rcd_status <> 2 AND 
        esao.rcd_status <> 2 AND 
        esa.rcd_status <> 2 AND 
        es.rcd_status <> 2;
      
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_SourceError.CODE_:= '10';
        v_SourceError.DESCRIPTION_:= 'No se encontro informacion para el endpoint en el repositorio.';
    END;
    
    IF (v_SourceError.CODE_ IS NULL) THEN

      SELECT 
        KEYVALUE_T (ecp.name, ecl.value) 
          BULK COLLECT 
            INTO 
              v_KeyValues
      FROM 
        esb_endpoint ee 
          INNER JOIN esb_system_api_operation esao  ON (ee.operation_id     = esao.id)
          INNER JOIN esb_system_api esa             ON (esao.system_api_id  = esa.id)
          INNER JOIN esb_system es                  ON (esa.system_id       = es.id)
          INNER JOIN esb_config ec                  ON (ee.config_id        = ec.id)
          INNER JOIN esb_config_list ecl            ON (ec.id               = ecl.config_id)
          INNER JOIN esb_config_property ecp        ON (ecl.property_id     = ecp.id)
      WHERE 
        esao.name = p_Target.operation_ AND 
        es.code = p_Target.provider_ AND 
        esa.code = p_Target.api_ AND 
        esao.version = p_Target.version_ AND 
        ee.rcd_status <> 2 AND 
        ec.rcd_status <> 2 AND 
        ecl.rcd_status <> 2 AND
        ecp.rcd_status <> 2;
        
      SELECT 
        COUNT(VALUE) 
          INTO 
            v_countNotNulls 
      FROM TABLE (v_KeyValues);
      
      IF (v_countNotNulls < v_KeyValues.COUNT) THEN
        v_SourceError.CODE_:= '9';
        v_SourceError.DESCRIPTION_:= 'Existen propiedades no valuadas en el endpoint.';
      ELSE
        v_SourceError.CODE_:= '0';
        v_SourceError.DESCRIPTION_:= 'Transaccion exitosa.';
      END IF;
      
      OPEN p_Endpoint FOR
        SELECT KEY, VALUE FROM TABLE(v_KeyValues);
      
    END IF;
    p_SourceError := SOURCERROR_T(v_SourceError.CODE_, v_SourceError.DESCRIPTION_);
  END getEndpoint;

 

END ESB_PARAMETERMANAGER_PKG;
/