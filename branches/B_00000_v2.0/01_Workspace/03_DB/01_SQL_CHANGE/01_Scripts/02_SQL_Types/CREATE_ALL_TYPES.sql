create or replace 
TYPE SERVICE_T AS OBJECT (
  CODE_ VARCHAR2(30),
  NAME_ VARCHAR2(50),
  OPERATION_ VARCHAR2(100)
);
/
create or replace 
TYPE CHANNEL_T AS OBJECT (
  NAME_ VARCHAR2(50),
  MODE_ VARCHAR2(50)
);
/
create or replace 
TYPE TRACE_T AS OBJECT (
  CLIENTREQTIMESTAMP_ VARCHAR2(50),
  REQTIMESTAMP_ VARCHAR2(50),
  RSPTIMESTAMP_ VARCHAR2(50),
  PROCESSID_ VARCHAR2(50),
  EVENTID_ VARCHAR2(50),
  SOURCEID_ VARCHAR2(50),
  CORRELATIONEVENTID_ VARCHAR2(50),
  CONVERSATIONID_ VARCHAR2(50),
  CORRELATIONID_ VARCHAR2(50),
  SERVICE_ SERVICE_T
);
/
create or replace 
TYPE CONSUMER_T AS OBJECT (
  SYSCODE_ VARCHAR2(30),
  ENTERPRISECODE_ VARCHAR2(30),
  COUNTRYCODE_ VARCHAR2(30)
);
/
create or replace 
TYPE HEADER_T AS OBJECT (
  CONSUMER_ CONSUMER_T,
  TRACE_ TRACE_T,
  CHANNEL_ CHANNEL_T
);
/
create or replace 
TYPE PLACEHOLDER_T AS OBJECT (
  TIME_ VARCHAR2(50),
  PLACE_ VARCHAR2(100)
);
/
create or replace 
TYPE HEADERTRACER_T AS OBJECT (
  COMPONENT_ VARCHAR2(100),
  OPERATION_ VARCHAR2(100),
  HEADER_ HEADER_T
);
/
create or replace 
TYPE AUDIT_T AS OBJECT (
  HEADERTRACER_ HEADERTRACER_T,
  AUDITPLACEHOLDER_ PLACEHOLDER_T
);
/
create or replace 
TYPE CANONICALERROR_T AS OBJECT (
  CODE_ VARCHAR2(50),
  DESCRIPTION_ VARCHAR2(255),
  TYPE_ VARCHAR2(30)
);
/
create or replace 
TYPE ERRORDETAIL_T AS OBJECT (
  DETAILNUMBER_ VARCHAR(10),
  DETAILMESSAGE_ CLOB
);
/
create or replace 
TYPE ERRORDETAILS_T AS OBJECT (
  DETAIL1_ ERRORDETAIL_T,
  DETAIL2_ ERRORDETAIL_T,
  DETAIL3_ ERRORDETAIL_T,
  DETAIL4_ ERRORDETAIL_T,
  DETAIL5_ ERRORDETAIL_T
);
/
create or replace 
TYPE ERRORINDEX_T AS OBJECT (
  MODULE_ VARCHAR2(50),
  SUBMODULE_ VARCHAR2(50)
);
/
create or replace 
TYPE ERRORSOURCEDETAILS_T AS OBJECT (
  SOURCE_ VARCHAR2(100),
  DETAILS VARCHAR2(255)
);
/
create or replace 
TYPE KEYS_T IS TABLE OF varchar2(100);
/
create or replace 
TYPE KEYVALUE_T IS OBJECT (
  "KEY" VARCHAR2(100),
  "VALUE" VARCHAR2(255)
)
/
create or replace 
TYPE KEYVALUETABLE_T IS TABLE OF KEYVALUE_T;
/
create or replace 
TYPE LOGMODE_T AS OBJECT (
  LOGTYPE_ VARCHAR2(10),
  LOGSEVERITY_ VARCHAR2(20)
);
/
create or replace 
TYPE MAP_IN_T AS OBJECT
(
  SCODE_ VARCHAR2(250),
  CONTEXT_ VARCHAR2(100),
  ENTITY_ VARCHAR2(50),
  FIELD_ VARCHAR2(50),
  DSYS_ VARCHAR2(20),
  SSYS_ VARCHAR2(20)
);
/
create or replace 
TYPE MAP_OUT_T AS OBJECT
(
  DCODE_ VARCHAR2(250),
  SCODE_ VARCHAR2(250),
  ENTITY_ VARCHAR(50),
  FIELD_ VARCHAR2(50)
);
/
create or replace 
TYPE MAPS_IN_T IS TABLE OF MAP_IN_T;
/
create or replace 
TYPE MAPS_OUT_T IS TABLE OF MAP_OUT_T;
/
create or replace 
TYPE MEMBER_T  IS OBJECT (
  MEMBER_NAME_ VARCHAR2(100), 
  TYPE_MEMBER_ VARCHAR2(30),
  CORRELATION_ID_ VARCHAR2(255),
  MEMBER_ADDRESS_ VARCHAR2(250)
 
);
/
create or replace 
TYPE MEMBER_T_CUR IS TABLE OF MEMBER_T;
/
create or replace 
TYPE SUBM_T AS OBJECT (
  ID_ VARCHAR(100),
  VALUE_ VARCHAR(100)
);
/
create or replace 
TYPE MODULE_T AS OBJECT (
  ID_ VARCHAR(100),
  SUBM1 SUBM_T,
  SUBM2 SUBM_T,
  SUBM3 SUBM_T,
  SUBM4 SUBM_T,
  SUBM5 SUBM_T,
  SUBM6 SUBM_T,
  SUBM7 SUBM_T,
  SUBM8 SUBM_T,
  SUBM9 SUBM_T,
  SUBM10 SUBM_T
);
/
create or replace 
TYPE PARAMETER_T AS OBJECT 
(
  KEY_ VARCHAR2(20),
  VALUE_ VARCHAR2(50)
);
/
create or replace 
TYPE LOG_T AS OBJECT (
  HEADERTRACER_ HEADERTRACER_T,
  MESSAGE_ CLOB,
  DESCRIPTION_ VARCHAR2(255),
  LOGPLACEHOLDER_ PLACEHOLDER_T,
  LOGMODE_ LOGMODE_T
);
/

create or replace 
TYPE QUALITYOFSERVICE_T AS OBJECT (
  TTL_ INTEGER(20),
  PRIORITY_ INTEGER(10),
  RETRYCOUNTER_ INTEGER(10),
  MAXRETRYCOUNT_ INTEGER(10)
);
/
create or replace 
TYPE SECURITYASSERTION_T AS OBJECT (
  NAME_ VARCHAR(100),
  VALUE_ VARCHAR(255)
);
/
create or replace 
TYPE SECURITYASSERTIONS_T AS OBJECT (
  SECASS1 SECURITYASSERTION_T,
  SECASS2 SECURITYASSERTION_T,
  SECASS3 SECURITYASSERTION_T,
  SECASS4 SECURITYASSERTION_T,
  SECASS5 SECURITYASSERTION_T,
  SECASS6 SECURITYASSERTION_T,
  SECASS7 SECURITYASSERTION_T,
  SECASS8 SECURITYASSERTION_T,
  SECASS9 SECURITYASSERTION_T,
  SECASS10 SECURITYASSERTION_T
);
/
create or replace 
TYPE SECURITYCREDENTIAL_T AS OBJECT (
  PRINCIPAL_ VARCHAR(100),
  CREDENTIALID_ VARCHAR(100)
);
/
create or replace 
TYPE SOURCEERROR_T AS OBJECT (
  CODE_ VARCHAR2(50),
  DESCRIPTION_ VARCHAR2(255),
  ERRORSOURCEDETAILS_ ERRORSOURCEDETAILS_T,
  SOURCEFAULT_ CLOB
);
/
create or replace 
TYPE SOURCEERRORS_T AS OBJECT (
  SOURCEERROR1_ SOURCEERROR_T,
  SOURCEERROR2_ SOURCEERROR_T,
  SOURCEERROR3_ SOURCEERROR_T,
  SOURCEERROR4_ SOURCEERROR_T,
  SOURCEERROR5_ SOURCEERROR_T
);
/
create or replace 
TYPE SOURCERROR_T AS OBJECT(
  CODE_ VARCHAR2(5),
  DESCRIPTION_ VARCHAR2(100)
);
/
create or replace 
TYPE RESULT_T AS OBJECT (
  STATUS_ VARCHAR2(50),
  DESCRIPTION_ VARCHAR2(255),
  CANONICALERROR_ CANONICALERROR_T,
  SOURCEERROR_ SOURCEERROR_T,
  CORRELATIVEERRORS_ SOURCEERRORS_T
);
/
create or replace 
TYPE TARGET_T AS OBJECT(
	  OPERATION_ VARCHAR2(50),
	  PROVIDER_ VARCHAR2(50),
	  API_ VARCHAR2(50),
	  VERSION_ VARCHAR2(10)
    );
/	
create or replace 
TYPE ERROR_T AS OBJECT (
  HEADERTRACER_ HEADERTRACER_T,
  RESULT_ RESULT_T,
  ERRORINDEX_ ERRORINDEX_T,
  ERRORPLACEHOLDER_ PLACEHOLDER_T
);
/
create or replace 
TYPE "CONTEXTMETADATA_T" AS OBJECT (
  FAMILY_ VARCHAR(100),
  CLASS_ VARCHAR(100),
  MEMBER_ VARCHAR(100),
  MODULE_ VARCHAR(100)
);
/
create or replace 
TYPE "LOGGER_T" AS OBJECT (
  COMPONENT_ VARCHAR(100),
  OPERATION_ VARCHAR(100)
);
/
create or replace TYPE "CRR_FLAG_T"  AS OBJECT (
  NAME_ VARCHAR2(100), 
  VALUE_ VARCHAR2(255)
);
/
create or replace TYPE "CRR_FLAGS_T_CUR"  AS TABLE OF CRR_FLAG_T ;
/
create or replace TYPE "CRR_MEM_T"  AS OBJECT (
  MEM_NAME_ VARCHAR2(100),
  MEM_LABEL_ VARCHAR2(255),
  MEM_TYPE_ VARCHAR2(30),
  MEM_CORR_ID_ VARCHAR2(255),
  MEM_ADDR_ VARCHAR2(255),
  MEM_FLAGS_ CRR_FLAGS_T_CUR
);
/
create or replace TYPE "CRR_MEM_T_CUR"  AS TABLE OF CRR_MEM_T;
/
create or replace TYPE "CRR_GRP_T"  AS OBJECT (
  GRP_NAME_ VARCHAR2(100), 
  GRP_TAG_ VARCHAR2(255),
  GRP_HEADER_ CLOB,
  GRP_STATUS_ CHAR,
  GRP_DESC_ VARCHAR(255),
  GRP_MEMS_ CRR_MEM_T_CUR,
  GRP_FLAGS_ CRR_FLAGS_T_CUR
);
/
create or replace TYPE "CB_GRP_MEM_T"  AS OBJECT (
  MEM_PROV_ID_ VARCHAR2(255),
  MEM_CNV_ID_ VARCHAR2(255),
  MEM_CNV_STATUS_ VARCHAR2(10),
  MEM_SRV_NAME_ VARCHAR2(255),
  MEM_SRV_OP_ VARCHAR2(255),
  MEM_CB_ID VARCHAR2(255)
);
/
create or replace TYPE "CB_GRP_MEM_T_CUR"  AS TABLE OF CB_GRP_MEM_T;
/
create or replace TYPE "CNV_GRP_T_CUR"  AS TABLE OF VARCHAR(255);
/
create or replace TYPE "CB_GRP_T"  AS OBJECT (
  CB_GRP_TAG_ VARCHAR2(255),
  CB_GRP_SUB_ID_ VARCHAR2(255),
  CB_GRP_HEADER_ CLOB,
  CB_GRP_MEMS CB_GRP_MEM_T_CUR
);
/
create or replace TYPE "CB_GRP_T_CUR"  AS TABLE OF CB_GRP_T;
/
create or replace TYPE "CNV_CB_IDS_T_CUR"  AS TABLE OF VARCHAR(255);
/
create or replace TYPE "CNV_CB_DETAIL_T"  AS OBJECT (
  CB_ID VARCHAR2(255),
  CB_MSG CLOB
);
/
create or replace TYPE "CNV_CB_DETAILS_T_CUR"  AS TABLE OF CNV_CB_DETAIL_T;
/
--------------------------------------------------------
--  ErrorManager
--------------------------------------------------------

-- RetryManager

create or replace 
TYPE RETRYMANAGER_ID_T_CUR IS TABLE OF NUMBER;
/
create or replace 
TYPE RETRYMANAGER_STATUS_T_CUR IS TABLE OF VARCHAR2(100);
/

--Diagnose

create or replace TYPE "ERR_CMP_T"  AS OBJECT (
  CMP_NAME VARCHAR(255),
  CMP_DESC VARCHAR(255)
);
/

create or replace TYPE "ERR_CMP_CFG_T"  AS OBJECT (
  CMP ERR_CMP_T,
  CMP_STEP VARCHAR(255),
  CMP_STEP_DESC VARCHAR(255)
);
/

create or replace TYPE "ERR_ACT_CFG_T"  AS OBJECT (
  RT_MAX NUMBER,
  RT_TTD NUMBER
);
/

create or replace TYPE "ERR_DIAG_T"  AS OBJECT (
  CMP_NAME    VARCHAR(50),
  CMP_STEP    VARCHAR(255),
  ERR_SEL_SINGLE_CODE  VARCHAR(100),
  ERR_SEL_SINGLE_TYPE  VARCHAR(50),
  ERR_SEL_FROM  NUMBER,
  ERR_SEL_TO    NUMBER,
  ERR_SEL_TYPE  VARCHAR(50),
  ERR_SEL_WEIGHT  NUMBER,
  ACTION      VARCHAR(10),
  ACTION_CFG    ERR_ACT_CFG_T
);
/
create or replace TYPE CONVERSATION_T AS OBJECT (
  MESSAGE_TX_ID_ NUMBER,
  CAPABILITY_ID_ NUMBER,
  CONVERSATION_ID_ VARCHAR2(50),
  CHANNEL_ID_ NUMBER,
  CONSUMER_ID_ NUMBER,
  SEQUENCE_ NUMBER
);
/
create or replace TYPE TRANSACTION_T AS OBJECT (
  EVENT_ID_ VARCHAR2(50),
  PROCESS_ID_ VARCHAR2(50),
  CORRELATION_ID_ VARCHAR2(50),
  STATUS_ VARCHAR2(20),
  RCD_STATUS_ NUMBER(1,0),
  SEQUENCE_ NUMBER,
  SOURCE_ID_ VARCHAR2(255),
  CORRELATION_EVENT_ID_ VARCHAR2(255)
);
/