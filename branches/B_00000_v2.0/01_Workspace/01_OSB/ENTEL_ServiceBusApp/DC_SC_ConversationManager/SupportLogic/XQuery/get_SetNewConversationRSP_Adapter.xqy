xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/setNewConversation/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/setNewConversation_ConversationManager_v1_CSM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare variable $Result as element() (:: schema-element(ns2:Result) ::) external;

declare function local:get_SetNewConversationRSP_Adapter($Result as element() (:: schema-element(ns2:Result) ::)) as element() (:: schema-element(ns1:SetNewConversationRSP) ::) {
    <ns1:SetNewConversationRSP>
        {
          $Result
        }
    </ns1:SetNewConversationRSP>
};

local:get_SetNewConversationRSP_Adapter($Result)