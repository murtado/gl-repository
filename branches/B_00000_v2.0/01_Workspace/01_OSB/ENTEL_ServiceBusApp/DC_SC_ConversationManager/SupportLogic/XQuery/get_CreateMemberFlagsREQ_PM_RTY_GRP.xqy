xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns20="http://www.entel.cl/SC/CorrelationManager/createMemberFlags/v1";
(:: import schema at "../../../DC_SC_CorrelationManager/SupportAPI/XSD/CSM/createMemberFlags_CorrelationManager_v1_CSM.xsd" ::)

declare namespace ns21="http://www.entel.cl/SC/CorrelationManager/getCorrelation/v2";
(:: import schema at "../../../DC_SC_CorrelationManager/SupportAPI/XSD/CSM/getCorrelation_CorrelationManager_v2_CSM.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";
declare namespace cal = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare namespace ns3="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::) external;
declare variable $GetCorrelationRSP as element() (:: schema-element(ns21:GetCorrelationRSP) ::) external;
declare variable $SubCorrID as xs:string external;
declare variable $RetryGroup as xs:string external;

declare function local:get_CreateMemberFlagsREQ_PM_RTY_GRP(
  $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::),
  $GetCorrelationRSP as element() (:: schema-element(ns21:GetCorrelationRSP) ::),
  $SubCorrID as xs:string,
  $RetryGroup as xs:string)
as element() (:: schema-element(ns20:CreateMemberFlagsREQ) ::) {

    let $GroupMembers:=$GetCorrelationRSP/*:Group/*:Members
    let $ProviderInstance:=$GroupMembers/*[@cor:memberCorrID=$SubCorrID]
    
    return

    <ns20:CreateMemberFlagsREQ>
        <cor:MemberName>{data($ProviderInstance/@cor:memberName)}</cor:MemberName>
        <cor:MemberType>{data($ProviderInstance/@cor:memberType)}</cor:MemberType>
        <cor:MemberCorrID>{data($ProviderInstance/@cor:memberCorrID)}</cor:MemberCorrID>
        <cor:GroupTag>{data($GetCorrelationRSP/*:Group/@cor:groupTag)}</cor:GroupTag>
        <cor:Flags>
            <cor:Flag name="rtyGrp" value="{$RetryGroup}"/>
        </cor:Flags>
    </ns20:CreateMemberFlagsREQ>
};

local:get_CreateMemberFlagsREQ_PM_RTY_GRP($RequestHeader, $GetCorrelationRSP, $SubCorrID, $RetryGroup)
