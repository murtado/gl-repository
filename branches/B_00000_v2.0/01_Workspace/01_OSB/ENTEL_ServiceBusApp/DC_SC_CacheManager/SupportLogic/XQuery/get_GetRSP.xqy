xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CacheManager/get/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/get_CacheManager_v1_CSM.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/EDD/Dictionary/v1";

declare variable $CacheValue as element() external;

declare function local:getRSP_Adapter($CacheValue as element()) as element() (:: schema-element(ns1:GetRSP) ::) {
    <ns1:GetRSP>
          { 
            if( exists($CacheValue/*) ) then
              <ns1:Item>
                <ns3:Value>
                  {$CacheValue/*}
                </ns3:Value>
              </ns1:Item>
            else 
              if (data($CacheValue/.)!='') then
                <ns1:Item>
                  <ns3:Value>
                  {data($CacheValue)}
                  </ns3:Value>
                </ns1:Item>
              else
                ()
          }
    </ns1:GetRSP>
};

local:getRSP_Adapter($CacheValue)
