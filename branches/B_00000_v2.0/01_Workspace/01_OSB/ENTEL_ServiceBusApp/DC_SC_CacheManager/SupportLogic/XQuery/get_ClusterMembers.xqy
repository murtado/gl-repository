xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare function local:get_CacheDistribution() as element() {
    <ClusterMembers>
      <ManagedServer name='osb_server1' url='http://localhost:7003'/>
      <ManagedServer name='osb_server2' url='http://localhost:7004'/>
      <ManagedServer name='osb_server2' url='http://localhost:7005'/>
    </ClusterMembers>
};

local:get_CacheDistribution()
