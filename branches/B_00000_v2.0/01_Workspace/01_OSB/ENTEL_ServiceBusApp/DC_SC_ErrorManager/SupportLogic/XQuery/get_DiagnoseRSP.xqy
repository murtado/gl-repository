xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)
declare namespace ns3="http://www.entel.cl/SC/ErrorManager/ErrorDiagnostics/Aux";
(:: import schema at "../../SupportAPI/XSD/ErrorDiagnostics.xsd" ::)
declare namespace ns4="http://www.entel.cl/SC/ErrorManager/diagnose/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/diagnose_ErrorManager_v1_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/diagnose";
(:: import schema at "../JCA/diagnoseAdapter/diagnose_sp.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare variable $Result as element() (:: schema-element(ns2:Result) ::) external;
declare variable $DiagFile as element()? (:: schema-element(ns3:DiagFile) ::) external;

declare function local:get_DiagnoseRSP(
                                       $Result as element() (:: schema-element(ns2:Result) ::), 
                                       $DiagFile as element()? (:: schema-element(ns3:DiagFile) ::)) 
                                       as element() (:: schema-element(ns4:DiagnoseRSP) ::) {
    <ns4:DiagnoseRSP>
        {$Result}
        {$DiagFile}
    </ns4:DiagnoseRSP>
};

local:get_DiagnoseRSP($Result, $DiagFile)
