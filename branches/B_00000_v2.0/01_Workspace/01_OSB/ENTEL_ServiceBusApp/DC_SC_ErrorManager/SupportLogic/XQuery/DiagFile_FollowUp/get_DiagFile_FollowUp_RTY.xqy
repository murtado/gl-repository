xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ErrorManager/ErrorDiagnostics/Aux";
(:: import schema at "../../../SupportAPI/XSD/ErrorDiagnostics.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/diagnose";
(:: import schema at "../../JCA/diagnoseAdapter/diagnose_sp.xsd" ::)

declare variable $DiagFile as element()? (:: schema-element(ns1:DiagFile) ::) external;
declare variable $AdapterRSP as element() (:: schema-element(ns2:OutputParameters) ::) external;

declare function local:get_Diagnose_FollowUp_Retry($DiagFile as element()? (:: schema-element(ns1:DiagFile) ::), 
                                                   $AdapterRSP as element() (:: schema-element(ns2:OutputParameters) ::)) as element() (:: schema-element(ns1:DiagFile) ::) {

    let $current_RT_CNT:=
      if($DiagFile/@RT_CNT) then data($DiagFile/@RT_CNT) else (0)
    
    let $new_Action  := data($AdapterRSP/*[1])
    let $new_RT_MAX := data($AdapterRSP/*[2]/*[1]/*[@name='RT_MAX'])
    let $new_RT_TTD := data($AdapterRSP/*[2]/*[1]/*[@name='RT_TTD'])

    
    let $processed_Action:=
      if($new_Action!='RTY') then
        $new_Action
      else
        if(number($new_RT_MAX)>number($current_RT_CNT)) then 'RTY' else 'THR'
    
    let $new_RT_CNT := 
      if($processed_Action='RTY') then 
        ($current_RT_CNT+1) 
      else 
        ($current_RT_CNT)
    
    return
    
      <ns1:DiagFile 
            action="{$processed_Action}"
            RT_CNT="{$new_RT_CNT}"
            >
            {
                  
                  if(boolean($DiagFile)) then
                    attribute pvAction {$DiagFile/@action}
                  else
                    ()
            }
            {
                  for $ActionCFG in $AdapterRSP/*[2]/*[1]/*
                    return  
                      if(data($ActionCFG)!='') then
                        attribute{data($ActionCFG/@name)} {data($ActionCFG)}
                      else
                        ()
            }
      </ns1:DiagFile>
};

local:get_Diagnose_FollowUp_Retry($DiagFile, $AdapterRSP)