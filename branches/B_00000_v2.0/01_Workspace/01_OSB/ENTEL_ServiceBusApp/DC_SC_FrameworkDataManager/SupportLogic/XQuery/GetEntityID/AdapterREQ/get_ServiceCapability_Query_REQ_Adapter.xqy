xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/FrameworkDataManager/getEntityID/v1";

declare namespace ns2 = "http://www.entel.cl/SC/FrameworkDataManager/FRWEntities/Aux";

declare namespace ns3="http://xmlns.oracle.com/pcbpel/adapter/db/sp/GET_CAPABILITY_ID";
(:: import schema at "../../../../SupportLogic/JCA/GET_CAPABILITY_ID/GET_CAPABILITY_ID_sp.xsd" ::)

declare variable $SCMessage as element() external;

declare function local:func($SCMessage as element()) as element() (:: schema-element(ns3:InputParameters) ::) {

      <ns3:InputParameters>
          <ns3:P_SERVICE_ID>{fn:data($SCMessage/*[2]/*[1]/@serviceID)}</ns3:P_SERVICE_ID>
          <ns3:P_CAPABILITY_NAME>{fn:data($SCMessage/*[2]/*[1]/@name)}</ns3:P_CAPABILITY_NAME>
      </ns3:InputParameters>
};

local:func($SCMessage)
