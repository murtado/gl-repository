xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $SCRequest as element() external;

declare function local:get_getEntityID_RouteCFG($SCRequest as element()) as xs:string {


let $EQueryName:=local-name($SCRequest/*[2]/*[1])

  return
        concat(
          local:get_REQ_Adapter_XQName($EQueryName),
          '@',
          local:get_RSP_Adapter_XQName($EQueryName),
          '@',
          local:get_BusinessName($EQueryName),
          '@',
          local:get_BusinessOperation($EQueryName)
          )

};


declare function local:get_REQ_Adapter_XQName($EQueryName as xs:string) as xs:string {
  concat(
    'get_',
    $EQueryName,
    '_REQ_Adapter'
    )
};

declare function local:get_RSP_Adapter_XQName($EQueryName as xs:string) as xs:string {
  concat(
    'get_',
    $EQueryName,
    '_RSP_Adapter'
    )
};
declare function local:get_BusinessName($EQueryName as xs:string) as xs:string {
   
    if($EQueryName = 'EnterpriseService_Query')        then 'GET_SERVICE_ID'             else (),
    if($EQueryName = 'ServiceCapability_Query')        then 'GET_CAPABILITY_ID'          else (),
    if($EQueryName = 'ServiceCapability_ByCode_Query') then 'GET_CAPABILITY_ID_BY_CODE'  else (),
    if($EQueryName = 'Consumer_Query')                 then 'GET_CONSUMER_ID'            else (),
    if($EQueryName = 'Consumer_Query_ByCodes')         then 'GET_CONSUMER_ID_BY_CODES'   else (),
    if($EQueryName = 'Enterprise_Query')               then 'GET_ENTERPRISE_ID'          else (),
    if($EQueryName = 'System_Query')                   then 'GET_SYSTEM_ID'              else (),
    if($EQueryName = 'Country_Query')                  then 'GET_COUNTRY_ID'             else (),
    if($EQueryName = 'Channel_Query')                  then 'GET_CHANNEL_ID_BY_NAME'     else ()
};

declare function local:get_BusinessOperation($EQueryName as xs:string) as xs:string {

    if($EQueryName = 'EnterpriseService_Query')        then 'GET_SERVICE_ID'             else (),
    if($EQueryName = 'ServiceCapability_Query')        then 'GET_CAPABILITY_ID'          else (),
    if($EQueryName = 'ServiceCapability_ByCode_Query') then 'GET_CAPABILITY_ID_BY_CODE'  else (),
    if($EQueryName = 'Consumer_Query')                 then 'GET_CONSUMER_ID'            else (),
    if($EQueryName = 'Consumer_Query_ByCodes')         then 'GET_CONSUMER_ID_BY_CODES'   else (),
    if($EQueryName = 'Enterprise_Query')               then 'GET_ENTERPRISE_ID'          else (),
    if($EQueryName = 'System_Query')                   then 'GET_SYSTEM_ID'              else (),
    if($EQueryName = 'Country_Query')                  then 'GET_COUNTRY_ID'             else (),
    if($EQueryName = 'Channel_Query')                  then 'GET_CHANNEL_ID_BY_NAME'     else ()

};

local:get_getEntityID_RouteCFG($SCRequest)
