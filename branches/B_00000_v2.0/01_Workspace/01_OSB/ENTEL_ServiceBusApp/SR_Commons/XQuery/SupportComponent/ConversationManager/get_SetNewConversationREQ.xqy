xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/setNewConversation/v1";
declare namespace conversationAux="http://www.entel.cl/SC/ConversationManager/Aux/Conversation";
declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $RequestHeader as element() external;
declare variable $ConversationID as xs:string external;

declare function local:get_SetNewConversationREQ($ConversationID as xs:string, $RequestHeader as element()) as element() {
    
   
    <ns1:SetNewConversationREQ>
      {$RequestHeader}
      <conversationAux:ConversationID>{$ConversationID}</conversationAux:ConversationID>
    </ns1:SetNewConversationREQ>
};

local:get_SetNewConversationREQ($ConversationID,$RequestHeader)