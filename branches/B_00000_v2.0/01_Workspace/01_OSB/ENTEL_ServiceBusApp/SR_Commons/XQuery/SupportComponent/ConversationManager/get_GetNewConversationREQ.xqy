xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/getNewConversation/v1";
declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $RequestHeader as element() external;

declare function local:get_GetNewConversationREQ($RequestHeader as element()) as element() {
    <ns1:GetNewConversationREQ>
      {$RequestHeader}
    </ns1:GetNewConversationREQ>
};

local:get_GetNewConversationREQ($RequestHeader)