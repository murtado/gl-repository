xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns13="http://www.entel.cl/SC/ErrorManager/ErrorDiagnostics/Aux";
declare namespace ns12="http://www.entel.cl/SC/ErrorManager/diagnose/v1";

declare namespace ns10="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns1="http://www.entel.cl/EDD/Dictionary/v1";
(:: import schema at "../../../XSD/EDD/Dictionary_v1_EDD.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/Tracer/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";
(:: import schema at "../../../XSD/ESO/Error_v1_ESO.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../XSD/ESO/Result_v2_ESO.xsd" ::)

declare variable $RequestHeader as element(ns10:RequestHeader) external;
declare variable $DiagFile as element(ns13:DiagFile)? external;
declare variable $CmpName as xs:string external;
declare variable $CmpStep as xs:string external;
declare variable $Result as element(ns5:Result) external;



declare function local:get_DiagnoseREQ_v1(
  $RequestHeader as element(ns10:RequestHeader),
  $DiagFile as element(ns13:DiagFile)?,
  $CmpName as xs:string,
  $CmpStep as xs:string,
  $Result as element(ns5:Result)
    ) as element(ns12:DiagnoseREQ) {

            <ns12:DiagnoseREQ>
                {$RequestHeader}
                <ns13:IncidentMeta cmpName="{$CmpName}" cmpStep="{$CmpStep}">
                    {$Result}
                </ns13:IncidentMeta>
                {
                  if(exists($DiagFile)) then
                    $DiagFile
                  else
                    ()
                }
            </ns12:DiagnoseREQ>
};

local:get_DiagnoseREQ_v1(
  $RequestHeader,
  $DiagFile,
  $CmpName,
  $CmpStep,
  $Result)
