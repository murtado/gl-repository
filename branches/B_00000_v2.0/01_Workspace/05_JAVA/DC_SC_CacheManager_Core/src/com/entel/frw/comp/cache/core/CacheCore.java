package com.entel.frw.comp.cache.core;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.xmlbeans.XmlObject;

import com.entel.frw.comp.cache.core.exception.ValueIsNullException;

public class CacheCore {

	private static ConcurrentHashMap<String,HashMap<String,HashMap<String,CacheValue>>> cacheCore;
	
	static{
		initCache();
	}
	
	public static void put(String source, String category, String instance, XmlObject value, long expiration) throws ValueIsNullException{
		
		if (value == null)  
			throw new ValueIsNullException("Cache Value cannot be NULL in a PUT operation");
		
		if (source == null || category == null || instance == null)
			throw new ValueIsNullException("Source, Category nor Instance can be NULL in a PUT operation");

		// ----------------------------------------------------------------------------------------------------------------------------
		// LOGGING
			/*
				System.out.println("[CacheCore] - " + String.valueOf(new Date()) + " - " + 
					"PUT with "
					+ "[Source=" + source +"] & [Category=" + category +"] & [Instance=" + instance + "] & [Expiration=" + expiration + "] "
							+ "& [Value="+ value  + "].");
			*/
		// ----------------------------------------------------------------------------------------------------------------------------
		
		if (!cacheCore.containsKey(source)) 
			cacheCore.put(source, new HashMap<>());
		
		if (!cacheCore.get(source).containsKey(category)) 
			cacheCore.get(source).put(category, new HashMap<>());
		
		if (!cacheCore.get(source).get(category).containsKey(instance)) 
			cacheCore.get(source).get(category).put(instance, new CacheValue(expiration,value));
		else
			cacheCore.get(source).get(category).get(instance).updateValue(expiration,value);;
	}
	
	@SuppressWarnings("rawtypes")
	public static XmlObject get(String source, String category, String instance){
		
		CacheValue cacheValue = null;
		HashMap auxMap;
		XmlObject cacheValueValue = null;
		
		try{

			if((auxMap = cacheCore.get(source)) != null)
				if((auxMap = (HashMap) auxMap.get(category)) != null)
					cacheValue = (CacheValue) auxMap.get(instance);
			
			if(cacheValue != null)
				if (!(cacheValue.isExpired())) 
					cacheValueValue =  cacheValue.getValue(); 
			
			// ----------------------------------------------------------------------------------------------------------------------------
			// LOGGING
				/*
					System.out.println("[CacheCore] - " + String.valueOf(new Date()) + " - " + 
							"GET with "
							+ "[Source=" + source +"] & [Category=" + category +"] & [Instance=" + instance + "]" + " & [IsExpired=" + isExpired + "]" 
							+ " & [Value=" + cacheValueValue + "].");
				*/
			// ----------------------------------------------------------------------------------------------------------------------------
			
			return cacheValueValue;
		}
		catch (Exception e){
			
			// ----------------------------------------------------------------------------------------------------------------------------
			// LOGGING
				/*
					System.out.println("[CacheCore] - " + String.valueOf(new Date()) + " - " + 
							"GET in ERROR with "
							+ "[Source=" + source +"] & [Category=" + category +"] & [Instance=" + instance + "]."
							+ " & [ERROR]");
				*/
			// ----------------------------------------------------------------------------------------------------------------------------
			
				return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static void lazyRefresh(String source, String category, String instance){
		
		HashMap auxMap;
		
		// ----------------------------------------------------------------------------------------------------------------------------
		// LOGGING
			/*
				System.out.println("[CacheCore] - " + String.valueOf(new Date()) + " - " + 
						"LAZY REFRESH with "
						+ "[Source=" + source +"] & [Category=" + category +"] & [Instance=" + instance + "].");
			*/
		// ----------------------------------------------------------------------------------------------------------------------------
		
		if (source!=""){ 
			if (category!="")
				if (instance!=""){
					if((auxMap = cacheCore.get(source)) != null){
						if((auxMap = (HashMap) auxMap.get(category)) != null){
							auxMap.remove(instance);
						}
					}
				}
				else{
					if((auxMap = cacheCore.get(source)) != null){
						auxMap.remove(category);
						}
				}
			else{
				cacheCore.remove(source);
			}
		}
		else{
			clearCache();
		}
	}
	
	private static void initCache(){
		cacheCore = new ConcurrentHashMap<>();
	}
	
	private static void clearCache(){
		cacheCore.clear();
	}
	
}
