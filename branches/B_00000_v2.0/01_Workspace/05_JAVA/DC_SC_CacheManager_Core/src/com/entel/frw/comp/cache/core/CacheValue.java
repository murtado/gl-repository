package com.entel.frw.comp.cache.core;

import java.util.Date;

import org.apache.xmlbeans.XmlObject;

public class CacheValue {

	private long created;
	private long expiration;
	
	private XmlObject value;
	
	public CacheValue(long expiration, XmlObject value) {
		created = new Date().getTime();
		this.expiration = expiration;
		this.value = value;
	}
	
	protected boolean isExpired(){
		return (expiration != 0 && (((new Date()).getTime() - created) >= expiration));
	}

	public XmlObject getValue() {
		return value;
	}

	public void setValue(XmlObject value) {
		this.value = value;
	}
	
	public void updateValue(long expiration, XmlObject value) {
		created = new Date().getTime();
		this.value = value;
		this.expiration = expiration;
	}
}
