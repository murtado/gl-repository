package com.entel.soa.utilities.customxquery;

import static org.junit.Assert.*;
import org.junit.Test;

import com.entel.soa.utilities.customxquery.UtilConversor;

public class TestUtilConversor {

	@Test
	public void testDecodeBase64() {
		try {
			String bytes = new String(
					"cGVwZQ==")
					;
			System.out.println("testDecodeBase64: "+UtilConversor.decodeBase64(bytes));				
			
		} catch (Exception e) {
			fail("Error: "+e.getMessage());
		}
	}

	@Test
	public void testEncodeBase64(){
		try {
			String bytes = new String(
					"pepe");
			System.out.println("testEncodeBase64: "+UtilConversor.encodeBase64(bytes));
			} catch (Exception e) {
			fail("Error: "+e.getMessage());
		}
		
	}
	
	
	
	@Test
	public void testSha1(){
		try {
			String bytes = new String(
					"pepe");
			System.out.println("testSha1: "+UtilConversor.sha1(bytes.getBytes()));
			} catch (Exception e) {
			fail("Error: "+e.getMessage());
		}
		
	}
	

	@Test
	public void testDigest()  {
		
		try {
			
			System.out.println("testDigest: "+UtilConversor.passwordDigest("NlQCwvqqL6dwLzt57+Is3A==","2016-09-16T14:16:26.589Z","ents1mUSER"));
			} catch (Exception e) {
			fail("Error: "+e.getMessage());
		}
		
	

	}	 
	
	
	
	 
	
	
	
	
}
