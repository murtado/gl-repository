package com.entel.soa.utilities.customxquery;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UtilDates {
  
  
	public static String addSecondsToDateTime(Integer Seconds, String dateTime) {
		try {
			String format;

			int length = dateTime.length();
			switch (length) {
			case 25:
				format = "yyyy-MM-dd'T'HH:mm:ssXXX";
				break;
			case 27:
				format = "yyyy-MM-dd'T'HH:mm:ss.SXXX";
				break;
			case 28:
				format = "yyyy-MM-dd'T'HH:mm:ss.SSXXX";
				break;
			default:
				format = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
			}
			DateFormat formatter = new SimpleDateFormat(format);
			String timezone = dateTime.substring(length - 6, length - 3);
			String timezoneNumber = timezone.substring(1);
			if (timezoneNumber.charAt(0) == '0')
				timezoneNumber = timezoneNumber.substring(1);
			String timezoneSign = (timezone.charAt(0) == '+') ? "-" : "+";
			timezone = timezoneSign + timezoneNumber;
			formatter.setTimeZone(TimeZone.getTimeZone("Etc/GMT" + timezone));
			Date date;
			date = formatter.parse(dateTime);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.SECOND, Seconds);
			return formatter.format(calendar.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return "error de parseo";
		}

	}

}