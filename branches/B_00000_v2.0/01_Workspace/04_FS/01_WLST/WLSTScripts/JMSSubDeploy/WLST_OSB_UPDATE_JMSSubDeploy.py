import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='JMSSubDeploy'

wlstFileDomain='OSB'
wlstFileType='UPDATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileDir + wlstVersion + "/"+ wlstVersion + ".properties")



scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

#------------------------------------------
#Shared JMS Artifacts (with SOA Domain)
#------------------------------------------
if (isTwoDomains=='true'):
	sharedUsr=OSB_SOA_crossDomainUsr
	sharedPsw=OSB_SOA_crossDomainPsw
else:
	sharedUsr=SOA_ConUsr
	sharedPsw=SOA_ConPsw
	
#------------------------------------------

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

def makeDeploymentPlanVariable(wlstPlan, adapterAppname, name, value, xpath, origin='planbased'):
	variableAssignment = wlstPlan.createVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	variableAssignment.setXpath(xpath)
	variableAssignment.setOrigin(origin)
	wlstPlan.createVariable(name, value)
 		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------------- """
""" Legacy_JMSModule """
""" ----------------------- """
def retargetJMSSubdeployment_JMSModule(systemName):
	global fName
	
	fName = 'retargetJMSSubdeployment_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	
	cd('/JMSSystemResources/'+systemName+'_JMSModule/SubDeployments/'+systemName+'_SubDeploy')

	set('Targets',jarray.array([ObjectName('com.bea:Name='+systemName+'_OSB_JMSServer,Type=JMSServer')], ObjectName))
	debugFile.write('\n     JMSModule: LG_'+systemName+'_JMSModule'+'  updated finish')

	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	#-------------------------------------------------------------------------------
	
	for systemName in systemNames.split(','):
		retargetJMSSubdeployment_JMSModule(systemName)

	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')
