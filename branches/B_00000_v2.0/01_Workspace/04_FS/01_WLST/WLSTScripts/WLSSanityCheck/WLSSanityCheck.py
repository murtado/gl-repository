import time

pynName='WLSSanityCheck'

loadProperties(sys.argv[1]+'/'+pynName+'.properties')

scriptInstance = time.strftime("%d%m%Y%H%M%S")
reportFile = open(sys.argv[1]+'/_Reports/'+sys.argv[2]+'.'+scriptInstance, "a")

conURL="t3://"+AdminServerHost+":"+AdminServerPort

warningTypes=[]
warningArray=[]
warningArrayCount=0

""" ---------------------------------------------------------------------- """
""" Auxiliary Functions                                                    """
""" ---------------------------------------------------------------------- """

def logWarning(module, component, warning):
	global warningArray
	global warningArrayCount
	global warningTypes

	warningArrayCount=warningArrayCount+1
	warningArray.append('['+module+']&&['+component+']&&['+warning+']')

	if(warning not in warningTypes):
		warningTypes.append(warningTypes)

def printWarnings():
	global warningArray
	global warningArrayCount

	writeTitle(0,'--------------------------------------------- WARNINGS ---------------------------------------------')
	writeAttribute(1,'WARNING COUNT',warningArrayCount)
	writeTitle(1,'WARNING LIST')
	
	for warning in warningArray:
		writeWarningArrayItem(2,warning)

def writeConsole(line):
	if(DebugConsole=='true'):
		print line

def writeTitle(level, title):
	outS = ''

	for x in range(0, level):
		outS='\t' + outS

	outS=outS+title+' >> '
	reportFile.write(outS+'\n')
	writeConsole(outS)

def writeAttribute(level, attribute, value):
	outS = ''

	for x in range(0, level):
		outS='\t' + outS

	if(value is not None):
		value=str(value)

	outS=outS+attribute+' :: '+value
	reportFile.write(outS+'\n')
	writeConsole(outS)

def writeWarningArrayItem(level,warning):
	outS = ''

	for x in range(0, level):
		outS='\t' + outS

	outS=outS+warning.split('&&')[2] + ' :: ' + warning.split('&&')[0] + ' -- ' + warning.split('&&')[1]
	reportFile.write(outS+'\n')
	writeConsole(outS)

""" ---------------------------------------------------------------------- """
""" Custom Validations                                                     """
""" ---------------------------------------------------------------------- """
def checkCrossDomain(level):
	writeTitle(level,'CheckCrossDomain')
	
	securityConfiguration = cmo.getSecurityConfiguration()
	realms=securityConfiguration.getRealms()

	defaultRealm=securityConfiguration.getDefaultRealm()
	credentialMappers=defaultRealm.getCredentialMappers()

	for credentialMapper in credentialMappers:
		print credentialMapper

def listCustomValidations(level):
	writeTitle(level,'Custom Validations')
	checkCrossDomain(level+1)

""" ---------------------------------------------------------------------- """
""" WLS Resources                                                          """
""" ---------------------------------------------------------------------- """

""" ------------------------------ """
""" listJMSConnectionFactories     """
""" ------------------------------ """
def listJMSConnectionFactories(level, jmsResource):
	writeTitle(level,'ConnectionFactories')

	jmsConnectionFactories=jmsResource.getJMSResource().getConnectionFactories()

	for jmsConnectionFactory in jmsConnectionFactories:
		writeTitle(level+1,jmsConnectionFactory.getName())
		writeAttribute(level+2,'JNDI',jmsConnectionFactory.getJNDIName())
		writeAttribute(level+2,'DefaultTargetingEnabled',jmsConnectionFactory.isDefaultTargetingEnabled())
		writeTitle(level+2,'TransactionParams')
		writeAttribute(level+2,'TransactionTimeout',jmsConnectionFactory.getTransactionParams().getTransactionTimeout())
		writeAttribute(level+2,'XAConnectionFactoryEnabled',jmsConnectionFactory.getTransactionParams().isXAConnectionFactoryEnabled())
		writeAttribute(level+2,'SubDeploymentName',jmsConnectionFactory.getSubDeploymentName())

""" -------------------- """
""" listJMSServers       """
""" -------------------- """
def listJMSServers(level):
	writeTitle(level,'JMSServers')
	jmsServers= cmo.getJMSServers()

	for jmsServer in jmsServers:
		writeTitle(level+1,jmsServer.getName())

""" -------------------- """
""" listJMSSubdeploys    """
""" -------------------- """
def listJMSSubdeploys(level,jmsResource):
	subDeploymentsArray = jmsResource.getSubDeployments()
	writeTitle(level,'SubDeployments')

	for subDeploy in subDeploymentsArray:
		writeAttribute(level+1,'Name',subDeploy.getName())

def listJMSQueues(level,jmsResource):
	jmsQueues = jmsResource.getJMSResource().getUniformDistributedQueues()
	writeTitle(level,'DistributedQueues')

	for jmsQueue in jmsQueues:

		jmsQueueName=jmsQueue.getName()
		writeTitle(level+1,jmsQueue.getName())
		writeAttribute(level+2,'JNDI',jmsQueueName)
		writeAttribute(level+2,'DefaultTargetingEnabled',jmsQueue.isDefaultTargetingEnabled())
		writeAttribute(level+2,'SubDeploymentName',jmsQueue.getSubDeploymentName())
		writeAttribute(level+2,'UnitOfOrderRouting',jmsQueue.getUnitOfOrderRouting())
		writeAttribute(level+2,'ForwardDelay',jmsQueue.getForwardDelay())
		writeTitle(level+3,'DeliveryFailureParams')
		if (jmsQueue.getDeliveryFailureParams().getErrorDestination() is not None):
			errorQueue=jmsQueue.getDeliveryFailureParams().getErrorDestination().getName()
		else:
			errorQueue='NONE'
		writeAttribute(level+4,'ErrorDestination',errorQueue)
		writeAttribute(level+4,'ExpirationPolicy',jmsQueue.getDeliveryFailureParams().getExpirationPolicy())
		
		redeliveryLimit=jmsQueue.getDeliveryFailureParams().getRedeliveryLimit()
		writeAttribute(level+4,'RedeliveryLimit',redeliveryLimit)

		if(redeliveryLimit==-1):
			logWarning('DistributedQueues',jmsQueueName,'NO REDELIVERY LIMIT!')

		writeTitle(level+3,'DeliveryParamsOverrides')
		writeAttribute(level+4,'TimeToLive:',jmsQueue.getDeliveryParamsOverrides().getTimeToLive())
		writeAttribute(level+4,'RedeliveryDelay:',jmsQueue.getDeliveryParamsOverrides().getRedeliveryDelay())
		writeAttribute(level+4,'TimeToDeliver:',jmsQueue.getDeliveryParamsOverrides().getTimeToDeliver())

""" -------------------- """
""" listJMSModules       """
""" -------------------- """
def listJMSModules(level):
	jmsResources = cmo.getJMSSystemResources()
	writeTitle(level,'JMSModules')

	for jmsResource in jmsResources:

		writeTitle(level+1,'JMSModule['+jmsResource.getName()+']')

		#------------------------------------------------------------------------------------
		# JMS Subdeployments
		#------------------------------------------------------------------------------------
		listJMSSubdeploys(level+2,jmsResource)
		#------------------------------------------------------------------------------------

		#------------------------------------------------------------------------------------
		# JMS Connection Factories
		#------------------------------------------------------------------------------------
		listJMSConnectionFactories(level+2,jmsResource)
		#------------------------------------------------------------------------------------

		#------------------------------------------------------------------------------------
		# JMS Distributed Queues
		#------------------------------------------------------------------------------------
		listJMSQueues(level+2,jmsResource)
		#------------------------------------------------------------------------------------

""" -------------------- """
""" listJMSResources     """
""" -------------------- """
def listJMSResources(level):

	writeTitle(level,'JMSResources')

	#-------------------------------------------------------------------------------------------------------
	# JMS Servers
	#-------------------------------------------------------------------------------------------------------
	listJMSServers(level+1)

	#-------------------------------------------------------------------------------------------------------
	# JMS Modules
	#-------------------------------------------------------------------------------------------------------
	listJMSModules(level+1)

""" -------------------- """
""" listFileStores """
""" -------------------- """
def listFileStores(level):

	writeTitle(level,'FileStores')

	stores= cmo.getFileStores()

	for store in stores:
		writeTitle(level+1,store.getName())


""" -------------------- """
""" listJdbcStores """
""" -------------------- """
def listJdbcStores(level):

	writeTitle(level,'JDBCStores')

	fileStores= cmo.getJDBCStores()

	for fileStore in fileStores:
		writeTitle(level+1,fileStore.getName())
		writeAttribute(level+2,'TimeToDeliver:',jmsQueue.getDeliveryParamsOverrides().getTimeToDeliver())

""" -------------------- """
""" listPersistentStores """
""" -------------------- """
def listPersistentStores(level):
	writeTitle(level,'PersistentStores')

	listFileStores(level+1)
	listJdbcStores(level+1)

""" -------------------- """
""" listDataSources       """
""" -------------------- """
def listDataSources(level):
	jmsResources = cmo.getJDBCSystemResources()
	writeTitle(level,'DataSources')

	for jmsResource in jmsResources:
		writeTitle(level+1,jmsResource.getJDBCResource().getName())

		jdbcDriverParams = jmsResource.getJDBCResource().getJDBCDriverParams()
		writeTitle(level+2,'JDBCDriverParams')
		writeAttribute(level+3,'DriverName',jdbcDriverParams.getDriverName())
		writeAttribute(level+3,'UseXaDataSourceInterface',jdbcDriverParams.isUseXaDataSourceInterface())

		jdbcDriverProperties=jdbcDriverParams.getProperties().getProperties()
		writeTitle(level+3,'Properties')
		for property in jdbcDriverProperties:
			propertyString='['+property.getName()+' :::: '+property.getValue()+']'
			writeAttribute(level+4,'Property',propertyString)

		jdbcDataSourceParams = jmsResource.getJDBCResource().getJDBCDataSourceParams()
		writeTitle(level+2,'JDBCDataSourceParams')
		writeAttribute(level+3,'GlobalTransactionsProtocol',jdbcDataSourceParams.getGlobalTransactionsProtocol())

		jndiNames=jdbcDataSourceParams.getJNDINames()
		writeTitle(level+3,'JNDINames')
		for jndiName in jndiNames:
			writeAttribute(level+4,'Name',jndiName)

""" ********************************************************************************** """
# 										Main
""" ********************************************************************************** """
try:

	connect(AdminServerConUsr, AdminServerConPsw, conURL)

	""" @@@ """

	listCustomValidations(0);reportFile.flush();

	listPersistentStores(0);reportFile.flush();
	listJMSResources(0);reportFile.flush();
	listDataSources(0);reportFile.flush();
	printWarnings()

	""" @@@ """

	cd('/')
	disconnect()
	reportFile.close()

except:
	dumpStack()
	reportFile.close()
