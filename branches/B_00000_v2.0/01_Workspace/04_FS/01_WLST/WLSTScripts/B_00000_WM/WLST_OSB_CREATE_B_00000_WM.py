import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000_WM'
wlstFileDomain='OSB'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

def debug(message):
	debugFile.write(message)
	print message

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debug('\n----------------------------------------------------------------------------------')
	debug('\n--- Starting the Activate Session ---')
	debug('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debug('\n')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n--- Ending the Activate Session (SUCCESS) ---')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n All the tasks were completed Sucessfully')
		debug('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debug('\n')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n--- Ending the Activate Session (ERROR) ---')
		debug('\n----------------------------------------------------------------------------------')
		debug('\n There was an error excecution the following function : ' + fName)
		debug('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

	disconnect()
	exit()

""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ------------------------- """
""" CreateMaxThreadConstraint """
""" ------------------------- """
def createMaxThreadConstraint(maxThreadConstraintName,maxThread):
	global fName
	global wlstVersion

	fName = 'createMaxThreadConstraint'
	debug('\n')
	debug('\nStarting the Execution of Function : ' + fName)
	debug('\n')
	# ------------------------------------------------------------------------
	cd('/SelfTuning/' + OSB_DomName + '/MaxThreadsConstraints/')
	if (getMBean(maxThreadConstraintName) != None):
			debug('MXTC ALREADY EXISTS with [NAME = ' + maxThreadConstraintName + ']')
	else:
		create(maxThreadConstraintName,'MaxThreadsConstraints')
		cd(maxThreadConstraintName)
		set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName+',Type='+ OSB_TargetType)], ObjectName))
		set('Count',maxThread)
		debug('CREATED [' + maxThreadConstraintName + '].')
		save()

	save()

	debug('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------- """
""" CreateMinThreadConstraint """
""" ------------------------- """
def createMinThreadConstraint(minThreadConstraintName, minThread):
	global fName
	global wlstVersion

	fName = 'createMinThreadConstraint'
	debug('\n')
	debug('\nStarting the Execution of Function : ' + fName)
	debug('\n')

	# ------------------------------------------------------------------------
	cd('/SelfTuning/' + OSB_DomName + '/MinThreadsConstraints/')
	if (getMBean(minThreadConstraintName) != None):
			debug('MNTC ALREADY EXISTS with [NAME = ' + minThreadConstraintName + ']')
	else:
		create(minThreadConstraintName,'MinThreadsConstraints')
		cd(minThreadConstraintName)
		set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName+',Type='+ OSB_TargetType)], ObjectName))
		set('Count',minThread)
		debug('CREATED [' + minThreadConstraintName + '].')
		save()

	debug('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ----------------- """
""" CreateWorkManager """
""" ----------------- """
def createWorkManager(workManagerName, minThreadConstraintName, maxThreadConstraintName):
	global fName
	global wlstVersion

	fName = 'createWorkManager'
	debug('\n')
	debug('\nStarting the Execution of Function : ' + fName)
	debug('\n')

	# ------------------------------------------------------------------------
	cd('/SelfTuning/' + OSB_DomName + '/WorkManagers/')
	if (getMBean(workManagerName) != None):
			debug('WORK MANAGER ALREADY EXISTS with [NAME = ' + workManagerName + ']')
	else:
		create(workManagerName,'WorkManagers')
		cd(workManagerName)
		set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName+',Type='+ OSB_TargetType)], ObjectName))
		debug('CREATED [' + workManagerName + '].')
		save()

		bean=getMBean('/SelfTuning/' + OSB_DomName + '/MaxThreadsConstraints/' + maxThreadConstraintName)
		cmo.setMaxThreadsConstraint(bean)

		bean=getMBean('/SelfTuning/' + OSB_DomName + '/MinThreadsConstraints/' + minThreadConstraintName)
		cmo.setMinThreadsConstraint(bean)

		save()

	debug('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ----------------------- """
""" Create FRW WorkManagers """
""" ----------------------- """
def createFRW_WorkManager(name,mntc,mxtc):
	global fName
	global wlstVersion

	fName = 'createFRW_WorkManagers'
	debug('\n')
	debug('\nStarting the Execution of Function : ' + fName)
	debug('\n')

	wmName='FRW_' + name + '_WM'
	mntcWmName='FRW_' + name + '_WM_MNTC'
	mxtcWmName='FRW_' + name + '_WM_MXTC'

	debug('Creating WORK MANAGER with [NAME = ' + wmName + '] AND [MNTC = ' + mntcWmName + '] AND [MXTC = ' + mxtcWmName + '].')
	debug('\n')

	createMinThreadConstraint(mntcWmName,mntc)
	createMaxThreadConstraint(mxtcWmName,mxtc)
	createWorkManager(wmName,mntcWmName,mxtcWmName)

	debug('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	createFRW_WorkManager('ConversationManager_AC',2,4)
	createFRW_WorkManager('PublishResult_MEM_Redirection',1,2)
	createFRW_WorkManager('RoutingManager_RSP_Ignore',1,2)
	createFRW_WorkManager('LoggerManager_AC_Discarded',1,2)
	createFRW_WorkManager('LoggerManager_AC',3,5)
	createFRW_WorkManager('LoggerManager_AP',1,4)
	createFRW_WorkManager('ConversationManager_AP',1,2)
	createFRW_WorkManager('CorrelationManager_AC',2,3)
	createFRW_WorkManager('CorrelationManager_AC_EXP',1,2)
	createFRW_WorkManager('CacheManager_Get',6,14)

	""" @@@ """

	validate()
	endEditSession('OK')

except:
	dumpStack()
	endEditSession('ERROR')
