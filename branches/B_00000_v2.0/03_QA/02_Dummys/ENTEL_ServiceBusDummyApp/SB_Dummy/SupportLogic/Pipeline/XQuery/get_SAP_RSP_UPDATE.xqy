xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/SAP_Update.xsd" ::)

declare variable $SAP_REQ_UPDATE as element() (:: schema-element(SAP_REQ_UPDATE) ::) external;




declare function local:func($SAP_REQ_UPDATE as element() (:: schema-element(SAP_REQ_UPDATE) ::)) as element() (:: schema-element(SAP_RSP_UPDATE) ::) {
    <SAP_RSP_UPDATE>
        <Cliente_ID>{fn:data($SAP_REQ_UPDATE/Cliente_ID)}</Cliente_ID>
        <Cliente_Nombre>{fn:data($SAP_REQ_UPDATE/Cliente_Nombre)}</Cliente_Nombre>
        <Cliente_Apellido>{fn:data($SAP_REQ_UPDATE/Cliente_Apellido)}</Cliente_Apellido>
        <Cliente_Edad>{fn:data($SAP_REQ_UPDATE/Cliente_Edad)}</Cliente_Edad>
        <Cliente_Estado>{fn:data($SAP_REQ_UPDATE/Cliente_Estado)}</Cliente_Estado>
        <Codigo_Servicio>{fn:data($SAP_REQ_UPDATE/Codigo_Servicio)}</Codigo_Servicio>
        <StatusCode>{
            if(xs:int(fn:data($SAP_REQ_UPDATE/Cliente_Edad))>=18) then (0)
            else (210)
        
        }</StatusCode>
        <StatusDesc>{
            if(xs:int(fn:data($SAP_REQ_UPDATE/Cliente_Edad))>=18) then ("Transaccion OK")
            else  ("ConsumerID no encontrado")
        
        }</StatusDesc>
    </SAP_RSP_UPDATE>
};

local:func($SAP_REQ_UPDATE)