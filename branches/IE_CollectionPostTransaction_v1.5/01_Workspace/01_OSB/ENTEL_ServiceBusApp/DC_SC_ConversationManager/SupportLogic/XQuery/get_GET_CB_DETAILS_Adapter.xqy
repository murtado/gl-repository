xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/getCallbackDetail/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getCallbackDetail_ConversationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getCallbackDetail";
(:: import schema at "../JCA/getCallbackDetail/getCallbackDetail_sp.xsd" ::)

declare namespace cal = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare variable $GetCallbackDetailREQ as element() (:: schema-element(ns1:GetCallbackDetailREQ) ::) external;

declare function local:get_GET_CB_DETAILS_Adapter($GetCallbackDetailREQ as element() (:: schema-element(ns1:GetCallbackDetailREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_CB_IDS>
            {
            for $CbGroupMember in $GetCallbackDetailREQ/cal:CbGroupMembers/cal:CbGroupMember
              return <ns2:P_CB_IDS_ITEM>{fn:data($CbGroupMember/@cbID)}</ns2:P_CB_IDS_ITEM>
            }
        </ns2:P_CB_IDS>
    </ns2:InputParameters>
};

local:get_GET_CB_DETAILS_Adapter($GetCallbackDetailREQ)
