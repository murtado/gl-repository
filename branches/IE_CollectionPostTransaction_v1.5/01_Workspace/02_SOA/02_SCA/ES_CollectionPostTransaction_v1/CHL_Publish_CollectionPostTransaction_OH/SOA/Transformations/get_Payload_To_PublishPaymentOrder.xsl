<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:tns="http://www.entel.cl/EBM/PaymentOrder/Publish/v1" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns0="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xml:id="id_1" oraxsl:ignorexmlids="true"
                xmlns:ns1="http://www.entel.cl/EBO/StockTransaction/v1"
                xmlns:ns4="http://www.entel.cl/EBO/CustomerOrder/v1"
                xmlns:ns6="http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1"
                xmlns:ns7="http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1"
                xmlns:ns8="http://www.entel.cl/EBO/Quantity/v1" xmlns:ns9="http://www.entel.cl/EBO/ReceiverCompany/v1"
                xmlns:ns10="http://www.entel.cl/EBO/EmailContact/v1"
                xmlns:ns11="http://www.entel.cl/EBO/GeographicAddress/v1" xmlns:ns12="http://www.entel.cl/EBO/Money/v1"
                xmlns:ns13="http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1"
                xmlns:ns14="http://www.entel.cl/EBO/PostalDeliveryAddress/v1"
                xmlns:ns3="http://www.entel.cl/SC/ParameterManager/v1"
                xmlns:ns15="http://www.entel.cl/EBO/CustomerBill/v1"
                xmlns:ns16="http://www.entel.cl/EBO/PhysicalResourceSpec/v1"
                xmlns:ns17="http://www.entel.cl/EBO/PaymentMethod/v1"
                xmlns:ns18="http://www.entel.cl/EBO/LoyaltyTransaction/v1"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:ns19="http://www.entel.cl/EBO/Contact/v1" xmlns:ns20="http://www.entel.cl/EBO/GeographicArea/v1"
                xmlns:ns21="http://www.entel.cl/EBO/MSISDN/v1" xmlns:ns5="http://schemas.oracle.com/bpel/extension"
                xmlns:ns2="http://www.entel.cl/ESC/PaymentOrder/v1" xmlns:ns22="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:ns23="http://www.entel.cl/ESO/Error/v1" xmlns:client="http://xmlns.oracle.com/ES/OH/BPELProcess"
                xmlns:ns24="http://www.entel.cl/EBO/AppliedCustomerBillingRate/v1"
                xmlns:ns25="http://www.entel.cl/EBO/IndividualIdentification/v1"
                xmlns:ns26="http://www.entel.cl/EBO/BillingAccount/v1"
                xmlns:ns27="http://www.entel.cl/EBO/SalesChannel/v1"
                xmlns:ns28="http://www.entel.cl/EBO/CustomerPayment/v1" xmlns:ns29="http://www.entel.cl/EBO/Voucher/v1"
                xmlns:ns30="http://www.entel.cl/EBO/Asset/v1" xmlns:ns31="http://www.entel.cl/EBO/CustomerAccount/v1"
                xmlns:ns32="http://www.entel.cl/EBO/IssuingCompany/v1"
                xmlns:ns33="http://www.entel.cl/EBO/TelephoneNumber/v1"
                xmlns:ns34="http://www.entel.cl/EBO/PartyRole/v1" xmlns:ns35="http://www.entel.cl/EBO/Customer/v1"
                xmlns:ns36="http://www.entel.cl/EBO/PhysicalResource/v1"
                xmlns:ns37="http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1"
                xmlns:ns38="http://www.entel.cl/EBO/OrganizationName/v1"
                xmlns:ns39="http://www.entel.cl/EBO/CustomerAddress/v1"
                xmlns:ns40="http://www.entel.cl/EBO/PartyIdentification/v1"
                xmlns:ns41="http://www.entel.cl/EBO/IndividualName/v1" xmlns:ns42="http://www.entel.cl/ESO/Result/v2"
                xmlns:ns43="http://www.entel.cl/EBO/Skill/v1" xmlns:ns44="http://www.entel.cl/ESO/MessageHeader/v1"
                xmlns:ns45="http://www.entel.cl/EBO/CustomerOrderItem/v1"
                xmlns:ns46="http://www.entel.cl/EBO/Organization/v1" xmlns:ns47="http://www.entel.cl/EBO/StreetName/v1"
                xmlns:ns48="http://www.entel.cl/EBO/TimePeriod/v1" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ns49="http://www.entel.cl/EBO/Price/v1" xmlns:ns50="http://www.entel.cl/EBO/Address/v1"
                xmlns:ns51="http://www.entel.cl/EBO/Store/v1"
                xmlns:ns52="http://www.entel.cl/EBO/BusinessInteraction/v1"
                xmlns:ns53="http://www.entel.cl/EBO/Individual/v1"
                xmlns:ns54="http://www.entel.cl/EBO/EmployeeIdentification/v1"
                xmlns:ns55="http://www.entel.cl/EBO/ProductOffering/v1"
                xmlns:ns56="http://www.entel.cl/EBO/ProductSpecification/v1"
                xmlns:ns57="http://www.entel.cl/EBO/Characteristics/v1"
                xmlns:ns58="http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1"
                xmlns:ns59="http://www.entel.cl/EBO/PickupStore/v1" xmlns:ns60="http://www.entel.cl/EBO/Delivery/v1"
                xmlns:ns="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:ns61="http://www.entel.cl/EBO/ProductOfferingPrice/v1"
                xmlns:ebmns_Create="http://www.entel.cl/EBM/PaymentOrder/Create/v1"
                xmlns:ebmns_Cancel="http://www.entel.cl/EBM/PaymentOrder/Cancel/v1"
                xmlns:ns62="http://www.entel.cl/EBO/ProductRelationship/v1"
                xmlns:ns63="http://www.entel.cl/EBO/CompriseOf/v1"
                xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:ns64="http://www.entel.cl/EBO/PlaceOfBirth/v1"
                xmlns:ns65="http://www.entel.cl/EBO/Country/v1"
                xmlns:ns66="http://www.entel.cl/SC/ConversationManager/v1"
                xmlns:ns67="http://www.entel.cl/ESC/AppliedCustomerBillingCharge/v1"
                xmlns:ns68="http://www.entel.cl/ESC/CustomerBill/v1"
                xmlns:ns69="http://www.entel.cl/ESC/PhysicalResource/v1"
                xmlns:ns71="http://www.entel.cl/ESC/AppliedCustomerBillingCredit/v1"
                xmlns:ns70="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1"
                xmlns:ns74="http://www.entel.cl/EBO/PaymentItem/v1"
                xmlns:ns79="http://www.entel.cl/EBO/FinancialChargeSpec/v1" xmlns:ns72="http://www.entel.cl/EBO/Rate/v1"
                xmlns:ns73="http://www.entel.cl/EBO/AuthenticationEntity/v1"
                xmlns:ns75="http://www.entel.cl/EBO/CustomerAccountBalance/v1"
                xmlns:ns76="http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1"
                xmlns:ns77="http://www.entel.cl/EBO/CustomerComercialAddress/v1"
                xmlns:ns78="http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1"
                xmlns:ns80="http://www.entel.cl/EBM/PhysicalResource/Release/v1">
   <oracle-xsl-mapper:schema xml:id="id_2">
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources xml:id="id_3">
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_4">
            <oracle-xsl-mapper:schema location="../WSDLs/BPELProcess.wsdl" xml:id="id_5"/>
            <oracle-xsl-mapper:rootElement name="Publish_CollectionPostTransaction_REQ" namespace="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xml:id="id_6"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets xml:id="id_7">
         <oracle-xsl-mapper:target type="WSDL" xml:id="id_8">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/ES_PaymentOrder_v1/ESC/Primary/PaymentOrder_v1_ESC.wsdl" xml:id="id_9"/>
            <oracle-xsl-mapper:rootElement name="Publish_PaymentOrder_REQ" namespace="http://www.entel.cl/EBM/PaymentOrder/Publish/v1" xml:id="id_10"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [TUE DEC 27 14:10:05 ART 2016].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/" xml:id="id_11">
      <tns:Publish_PaymentOrder_REQ xml:id="id_12">
         <ns44:RequestHeader xml:id="id_13">
            <ns44:Consumer sysCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Consumer/@sysCode}"
                           enterpriseCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Consumer/@enterpriseCode}"
                           countryCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Consumer/@countryCode}"
                           xml:id="id_14">
               <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Consumer"
                             xml:id="id_15"/>
            </ns44:Consumer>
            <ns44:Trace clientReqTimestamp="{/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@clientReqTimestamp}"
                        eventID="{/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@eventID}"
                        xml:id="id_16">
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@reqTimestamp"
                       xml:id="id_17">
                  <xsl:attribute name="reqTimestamp" xml:id="id_18">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@reqTimestamp"
                                   xml:id="id_19"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@rspTimestamp"
                       xml:id="id_20">
                  <xsl:attribute name="rspTimestamp" xml:id="id_21">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@rspTimestamp"
                                   xml:id="id_22"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@processID"
                       xml:id="id_23">
                  <xsl:attribute name="processID" xml:id="id_24">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@processID"
                                   xml:id="id_25"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@sourceID"
                       xml:id="id_26">
                  <xsl:attribute name="sourceID" xml:id="id_27">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@sourceID"
                                   xml:id="id_28"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@correlationEventID"
                       xml:id="id_29">
                  <xsl:attribute name="correlationEventID" xml:id="id_30">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@correlationEventID"
                                   xml:id="id_31"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@conversationID"
                       xml:id="id_32">
                  <xsl:attribute name="conversationID" xml:id="id_33">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@conversationID"
                                   xml:id="id_34"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@correlationID"
                       xml:id="id_35">
                  <xsl:attribute name="correlationID" xml:id="id_36">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/@correlationID"
                                   xml:id="id_37"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service"
                       xml:id="id_38">
                  <ns44:Service xml:id="id_39">
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service/@code"
                             xml:id="id_40">
                        <xsl:attribute name="code" xml:id="id_41">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service/@code"
                                         xml:id="id_42"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service/@name"
                             xml:id="id_43">
                        <xsl:attribute name="name" xml:id="id_44">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service/@name"
                                         xml:id="id_45"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service/@operation"
                             xml:id="id_46">
                        <xsl:attribute name="operation" xml:id="id_47">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service/@operation"
                                         xml:id="id_48"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Trace/ns44:Service"
                                   xml:id="id_49"/>
                  </ns44:Service>
               </xsl:if>
            </ns44:Trace>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Channel" xml:id="id_50">
               <ns44:Channel xml:id="id_51">
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Channel/@name"
                          xml:id="id_52">
                     <xsl:attribute name="name" xml:id="id_53">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Channel/@name"
                                      xml:id="id_54"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Channel/@mode"
                          xml:id="id_55">
                     <xsl:attribute name="mode" xml:id="id_56">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Channel/@mode"
                                      xml:id="id_57"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns44:Channel"
                                xml:id="id_58"/>
               </ns44:Channel>
            </xsl:if>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result" xml:id="id_59">
               <ns42:Result status="{/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/@status}"
                            xml:id="id_60">
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/@description"
                          xml:id="id_61">
                     <xsl:attribute name="description" xml:id="id_62">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/@description"
                                      xml:id="id_63"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError"
                          xml:id="id_64">
                     <ns23:CanonicalError xml:id="id_65">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError/@type"
                                xml:id="id_66">
                           <xsl:attribute name="type" xml:id="id_67">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError/@type"
                                            xml:id="id_68"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError/@code"
                                xml:id="id_69">
                           <xsl:attribute name="code" xml:id="id_70">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError/@code"
                                            xml:id="id_71"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError/@description"
                                xml:id="id_72">
                           <xsl:attribute name="description" xml:id="id_73">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError/@description"
                                            xml:id="id_74"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:CanonicalError"
                                      xml:id="id_75"/>
                     </ns23:CanonicalError>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError"
                          xml:id="id_76">
                     <ns23:SourceError xml:id="id_77">
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/@code"
                                xml:id="id_78">
                           <xsl:attribute name="code" xml:id="id_79">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/@code"
                                            xml:id="id_80"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/@description"
                                xml:id="id_81">
                           <xsl:attribute name="description" xml:id="id_82">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/@description"
                                            xml:id="id_83"/>
                           </xsl:attribute>
                        </xsl:if>
                        <ns23:ErrorSourceDetails xml:id="id_84">
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/ns23:ErrorSourceDetails/@source"
                                   xml:id="id_85">
                              <xsl:attribute name="source" xml:id="id_86">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/ns23:ErrorSourceDetails/@source"
                                               xml:id="id_87"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/ns23:ErrorSourceDetails/@details"
                                   xml:id="id_88">
                              <xsl:attribute name="details" xml:id="id_89">
                                 <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/ns23:ErrorSourceDetails/@details"
                                               xml:id="id_90"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns23:SourceError/ns23:ErrorSourceDetails"
                                         xml:id="id_91"/>
                        </ns23:ErrorSourceDetails>
                     </ns23:SourceError>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns42:CorrelativeErrors"
                          xml:id="id_92">
                     <ns42:CorrelativeErrors xml:id="id_93">
                        <xsl:for-each select="/ns0:Publish_CollectionPostTransaction_REQ/ns44:RequestHeader/ns42:Result/ns42:CorrelativeErrors/ns23:SourceError"
                                      xml:id="id_94">
                           <ns23:SourceError xml:id="id_95">
                              <xsl:if test="@code" xml:id="id_96">
                                 <xsl:attribute name="code" xml:id="id_97">
                                    <xsl:value-of select="@code" xml:id="id_98"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <xsl:if test="@description" xml:id="id_99">
                                 <xsl:attribute name="description" xml:id="id_100">
                                    <xsl:value-of select="@description" xml:id="id_101"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <ns23:ErrorSourceDetails xml:id="id_102">
                                 <xsl:if test="ns23:ErrorSourceDetails/@source" xml:id="id_103">
                                    <xsl:attribute name="source" xml:id="id_104">
                                       <xsl:value-of select="ns23:ErrorSourceDetails/@source" xml:id="id_105"/>
                                    </xsl:attribute>
                                 </xsl:if>
                                 <xsl:if test="ns23:ErrorSourceDetails/@details" xml:id="id_106">
                                    <xsl:attribute name="details" xml:id="id_107">
                                       <xsl:value-of select="ns23:ErrorSourceDetails/@details" xml:id="id_108"/>
                                    </xsl:attribute>
                                 </xsl:if>
                                 <xsl:value-of select="ns23:ErrorSourceDetails" xml:id="id_109"/>
                              </ns23:ErrorSourceDetails>
                           </ns23:SourceError>
                        </xsl:for-each>
                     </ns42:CorrelativeErrors>
                  </xsl:if>
               </ns42:Result>
            </xsl:if>
         </ns44:RequestHeader>
         <tns:Body xml:id="id_110">
            <tns:CustomerOrder xml:id="id_111">
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerOrder/ns4:ID">
                  <ns4:ID xml:id="id_112">
                    <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerOrder/ns4:ID"
                                xml:id="id_117"/>
                  </ns4:ID>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerOrder/ns4:state">               
                 <ns4:state xml:id="id_118">
                    <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerOrder/ns4:state"
                                  xml:id="id_119"/>
                 </ns4:state>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns28:paymentDate">               
                 <tns:BusinessInteraction xml:id="id_114">
                     <ns52:interactionCode xml:id="id_137"/>
                     <ns52:interactionDate xml:id="id_115">
                       <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns28:paymentDate"
                                     xml:id="id_116"/>
                    </ns52:interactionDate>
                     <ns52:interactionDescription xml:id="id_138"/>
                  </tns:BusinessInteraction>
               </xsl:if>
               <xsl:for-each select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingProductCharge"
                             xml:id="id_124">
                  <tns:CustomerOrderItem xml:id="id_120">
                    <xsl:if test="ns7:productId">
                       <ns45:ID xml:id="id_121">
                          <xsl:value-of select="ns7:productId" xml:id="id_125"/>
                       </ns45:ID>
                    </xsl:if>
                    <xsl:if test="ns0:PhysicalResource/ns36:serialNumber">
                       <tns:ProductSpecification xml:id="id_126">
                          <tns:PhysicalResource xml:id="id_127">
                             <ns36:serialNumber xml:id="id_128">
                                <xsl:value-of select="ns0:PhysicalResource/ns36:serialNumber" xml:id="id_129"/>
                             </ns36:serialNumber>                             
                             <xsl:if test="ns0:PhysicalResource/ns0:PhysicalResourceSpec/ns16:skuNumber">
                                <tns:PhysicalResourceSpec xml:id="id_134">
                                   <ns16:skuNumber xml:id="id_135">
                                      <xsl:value-of select="ns0:PhysicalResource/ns0:PhysicalResourceSpec/ns16:skuNumber"
                                                    xml:id="id_136"/>
                                   </ns16:skuNumber>
                                </tns:PhysicalResourceSpec>
                             </xsl:if>
                           </tns:PhysicalResource>
                       </tns:ProductSpecification>
                    </xsl:if>
                  </tns:CustomerOrderItem>
               </xsl:for-each>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns28:ID">
                 <tns:PaymentMethod xml:id="id_130">
                    <tns:CustomerPayment xml:id="id_131">
                       <ns28:ID xml:id="id_132">
                          <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns28:ID"
                                        xml:id="id_133"/>
                       </ns28:ID>
                    </tns:CustomerPayment>
                 </tns:PaymentMethod>
               </xsl:if>
            </tns:CustomerOrder>
         </tns:Body>
      </tns:Publish_PaymentOrder_REQ>
   </xsl:template>
</xsl:stylesheet>
