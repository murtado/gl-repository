<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:ns1="http://www.entel.cl/SC/ParameterManager/get/v1" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns0="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tns="http://www.entel.cl/EBM/CustomerBill/Publish/v1" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns1 ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xml:id="id_1" oraxsl:ignorexmlids="true"
                xmlns:ns6="http://www.entel.cl/ESC/AppliedCustomerBillingCharge/v1"
                xmlns:ns8="http://www.entel.cl/EBO/StockTransaction/v1"
                xmlns:ns56="http://www.entel.cl/ESO/EndpointConfiguration/v1"
                xmlns:ns9="http://www.entel.cl/EBO/CustomerOrder/v1"
                xmlns:ns10="http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1"
                xmlns:ns11="http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1"
                xmlns:rcGet="http://www.entel.cl/SC/ParameterManager/RefreshCache_Get/v1"
                xmlns:ns12="http://www.entel.cl/EBO/Quantity/v1" xmlns:ns13="http://www.entel.cl/EBO/ReceiverCompany/v1"
                xmlns:ns14="http://www.entel.cl/EBO/EmailContact/v1"
                xmlns:ns15="http://www.entel.cl/EBO/GeographicAddress/v1" xmlns:ns16="http://www.entel.cl/EBO/Money/v1"
                xmlns:rcGetM="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetMapping/v1"
                xmlns:ns17="http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1"
                xmlns:ns18="http://www.entel.cl/EBO/PostalDeliveryAddress/v1"
                xmlns:ns3="http://www.entel.cl/SC/ParameterManager/v1"
                xmlns:ns19="http://www.entel.cl/EBO/CustomerBill/v1"
                xmlns:ns20="http://www.entel.cl/EBO/PhysicalResourceSpec/v1"
                xmlns:rc="http://www.entel.cl/SC/ParameterManager/RefreshCache/v1"
                xmlns:ns21="http://www.entel.cl/EBO/PaymentMethod/v1"
                xmlns:ns22="http://www.entel.cl/EBO/LoyaltyTransaction/v1"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:ns23="http://www.entel.cl/EBO/Contact/v1" xmlns:ns24="http://www.entel.cl/EBO/GeographicArea/v1"
                xmlns:ns25="http://www.entel.cl/EBO/MSISDN/v1" xmlns:ns5="http://schemas.oracle.com/bpel/extension"
                xmlns:ns2="http://www.entel.cl/ESC/PaymentOrder/v1" xmlns:ns26="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:ns27="http://www.entel.cl/ESO/Error/v1"
                xmlns:getC="http://www.entel.cl/SC/ParameterManager/getConfig/v1"
                xmlns:client="http://xmlns.oracle.com/ES/OH/BPELProcess"
                xmlns:getM="http://www.entel.cl/SC/ParameterManager/getMapping/v1"
                xmlns:ns28="http://www.entel.cl/EBO/AppliedCustomerBillingRate/v1"
                xmlns:ns29="http://www.entel.cl/EBO/IndividualIdentification/v1"
                xmlns:getE="http://www.entel.cl/SC/ParameterManager/getEndpoint/v1"
                xmlns:ns30="http://www.entel.cl/EBO/BillingAccount/v1"
                xmlns:ns31="http://www.entel.cl/EBO/SalesChannel/v1"
                xmlns:ns32="http://www.entel.cl/EBO/CustomerPayment/v1" xmlns:ns33="http://www.entel.cl/EBO/Voucher/v1"
                xmlns:ns34="http://www.entel.cl/EBO/Asset/v1" xmlns:ns35="http://www.entel.cl/EBO/CustomerAccount/v1"
                xmlns:ns36="http://www.entel.cl/EBO/IssuingCompany/v1"
                xmlns:ns7="http://www.entel.cl/ESC/CustomerBill/v1"
                xmlns:ns37="http://www.entel.cl/EBO/TelephoneNumber/v1"
                xmlns:ns38="http://www.entel.cl/EBO/PartyRole/v1" xmlns:ns39="http://www.entel.cl/EBO/Customer/v1"
                xmlns:ns40="http://www.entel.cl/EBO/PhysicalResource/v1"
                xmlns:ns41="http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1"
                xmlns:ns42="http://www.entel.cl/EBO/OrganizationName/v1"
                xmlns:ns43="http://www.entel.cl/EBO/CustomerAddress/v1"
                xmlns:ns4="http://www.entel.cl/SC/ConversationManager/v1"
                xmlns:ns44="http://www.entel.cl/EBO/PartyIdentification/v1"
                xmlns:ns45="http://www.entel.cl/EBO/IndividualName/v1" xmlns:ns46="http://www.entel.cl/ESO/Result/v2"
                xmlns:ns47="http://www.entel.cl/EBO/Skill/v1" xmlns:ns48="http://www.entel.cl/ESO/MessageHeader/v1"
                xmlns:ns49="http://www.entel.cl/EBO/CustomerOrderItem/v1"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:rcGetC="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetConfig/v1"
                xmlns:ns50="http://www.entel.cl/EBO/Organization/v1" xmlns:ns51="http://www.entel.cl/EBO/StreetName/v1"
                xmlns:ns52="http://www.entel.cl/EBO/TimePeriod/v1"
                xmlns:rcGetE="http://www.entel.cl/SC/ParameterManager/RefreshCache_GetEndpoint/v1"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ns53="http://www.entel.cl/EBO/Price/v1"
                xmlns:ns54="http://www.entel.cl/EBO/Address/v1" xmlns:ns55="http://www.entel.cl/EBO/Store/v1"
                xmlns:ns57="http://www.entel.cl/EBO/UserNameResourceRole/v1"
                xmlns:ns58="http://www.entel.cl/EBO/Party/v1" xmlns:ns59="http://www.entel.cl/EBO/TransportCompany/v1"
                xmlns:ebmns_Cancel="http://www.entel.cl/EBM/CustomerBill/Cancel/v1"
                xmlns:ebmns_Create="http://www.entel.cl/EBM/CustomerBill/Create/v1"
                xmlns:ns62="http://www.entel.cl/EBO/EmployeeIdentification/v1"
                xmlns:ns63="http://www.entel.cl/EBO/LoyaltyAccount/v1"
                xmlns:ns64="http://www.entel.cl/EBO/Characteristics/v1"
                xmlns:ns="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:ns66="http://www.entel.cl/EBO/Concessionaire/v1"
                xmlns:ns68="http://www.entel.cl/EBO/CodeValueItem/v1" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
                xmlns:ns70="http://www.entel.cl/EBO/FinancialChargeSpec/v1"
                xmlns:ns71="http://www.entel.cl/EBO/CustomerBillAttachment/v1"
                xmlns:ns72="http://www.entel.cl/EBO/Requester/v1"
                xmlns:ebmns_Submit="http://www.entel.cl/EBM/CustomerBill/Submit/v1"
                xmlns:ns60="http://www.entel.cl/EBO/CustomerBillReference/v1"
                xmlns:ns61="http://www.entel.cl/EBO/AppliedCustomerBillingDiscount/v1"
                xmlns:ns65="http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1"
                xmlns:ns67="http://www.entel.cl/EBO/Resource/v1" xmlns:ns69="http://www.entel.cl/EBO/RetainingAgent/v1"
                xmlns:ns73="http://www.entel.cl/ESC/PhysicalResource/v1"
                xmlns:ns75="http://www.entel.cl/ESC/AppliedCustomerBillingCredit/v1"
                xmlns:ns77="http://www.entel.cl/EBO/Individual/v1"
                xmlns:ns74="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1"
                xmlns:ns79="http://www.entel.cl/EBO/PaymentItem/v1" xmlns:ns85="http://www.entel.cl/EBO/Country/v1"
                xmlns:ns76="http://www.entel.cl/EBO/Rate/v1"
                xmlns:ns78="http://www.entel.cl/EBO/AuthenticationEntity/v1"
                xmlns:ns80="http://www.entel.cl/EBO/CustomerAccountBalance/v1"
                xmlns:ns81="http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1"
                xmlns:ns82="http://www.entel.cl/EBO/CustomerComercialAddress/v1"
                xmlns:ns83="http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1"
                xmlns:ns84="http://www.entel.cl/EBO/PlaceOfBirth/v1">
   <oracle-xsl-mapper:schema xml:id="id_2">
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources xml:id="id_3">
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_4">
            <oracle-xsl-mapper:schema location="../WSDLs/BPELProcess.wsdl" xml:id="id_5"/>
            <oracle-xsl-mapper:rootElement name="Publish_CollectionPostTransaction_REQ" namespace="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1" xml:id="id_6"/>
         </oracle-xsl-mapper:source>
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_7">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_ParameterManager/SupportAPI/WSDL/ParameterManager.wsdl" xml:id="id_8"/>
            <oracle-xsl-mapper:rootElement name="GetRSP" namespace="http://www.entel.cl/SC/ParameterManager/get/v1" xml:id="id_9"/>
            <oracle-xsl-mapper:param name="ParameterManager_get_RSP.Out" xml:id="id_10"/>
         </oracle-xsl-mapper:source>
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_11">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_ParameterManager/SupportAPI/WSDL/ParameterManager.wsdl" xml:id="id_12"/>
            <oracle-xsl-mapper:rootElement name="GetRSP" namespace="http://www.entel.cl/SC/ParameterManager/get/v1" xml:id="id_13"/>
            <oracle-xsl-mapper:param name="ParameterManager_get_RSP2.Out" xml:id="id_14"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets xml:id="id_15">
         <oracle-xsl-mapper:target type="WSDL" xml:id="id_16">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/ES_CustomerBill_v1/ESC/Primary/CustomerBill_v1_ESC.wsdl" xml:id="id_17"/>
            <oracle-xsl-mapper:rootElement name="Publish_CustomerBill_REQ" namespace="http://www.entel.cl/EBM/CustomerBill/Publish/v1" xml:id="id_18"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [MON JAN 02 15:31:31 ART 2017].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:param name="ParameterManager_get_RSP.Out" xml:id="id_19"/>
   <xsl:param name="ParameterManager_get_RSP2.Out" xml:id="id_20"/>
   <xsl:template match="/" xml:id="id_21">
      <tns:Publish_CustomerBill_REQ xml:id="id_22">
         <ns48:RequestHeader>
            <ns48:Consumer sysCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Consumer/@sysCode}"
                           enterpriseCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Consumer/@enterpriseCode}"
                           countryCode="{/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Consumer/@countryCode}">
               <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Consumer"/>
            </ns48:Consumer>
            <ns48:Trace clientReqTimestamp="{/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@clientReqTimestamp}"
                        eventID="{/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@eventID}">
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@reqTimestamp">
                  <xsl:attribute name="reqTimestamp">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@reqTimestamp"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@rspTimestamp">
                  <xsl:attribute name="rspTimestamp">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@rspTimestamp"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@processID">
                  <xsl:attribute name="processID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@processID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@sourceID">
                  <xsl:attribute name="sourceID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@sourceID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@correlationEventID">
                  <xsl:attribute name="correlationEventID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@correlationEventID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@conversationID">
                  <xsl:attribute name="conversationID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@conversationID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@correlationID">
                  <xsl:attribute name="correlationID">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/@correlationID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service">
                  <ns48:Service>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code">
                        <xsl:attribute name="code">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@name">
                        <xsl:attribute name="name">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@name"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@operation">
                        <xsl:attribute name="operation">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@operation"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service"/>
                  </ns48:Service>
               </xsl:if>
            </ns48:Trace>
            <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Channel">
               <ns48:Channel>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Channel/@name">
                     <xsl:attribute name="name">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Channel/@name"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Channel/@mode">
                     <xsl:attribute name="mode">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Channel/@mode"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Channel"/>
               </ns48:Channel>
            </xsl:if>
         </ns48:RequestHeader>
         <tns:Body xml:id="id_23">
            <tns:CustomerBill xml:id="id_24">
               <ns19:billComments1 xml:id="id_138"></ns19:billComments1>
               <ns19:billComments2 xml:id="id_140"></ns19:billComments2>
               <ns19:billDate xml:id="id_41">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:billDate"
                                xml:id="id_42"/>
               </ns19:billDate>
               <ns19:billNo xml:id="id_45">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:billNo"
                                xml:id="id_46"/>
               </ns19:billNo>
               <ns19:billNoOriginal xml:id="id_47">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:billNoOriginal"
                                xml:id="id_48"/>
               </ns19:billNoOriginal>
               <ns19:causalCode xml:id="id_147">
                  <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.causalCode')]/@value"
                                xml:id="id_148"/>
               </ns19:causalCode>
               <ns19:dateDigitalSignature xml:id="id_43">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:billDate"
                                xml:id="id_44"/>
               </ns19:dateDigitalSignature>
               <ns19:digitalSignature xml:id="id_49">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:digitalSignature"
                                xml:id="id_50"/>
               </ns19:digitalSignature>
               <ns19:discountAmount xml:id="id_51">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:discountAmount"
                                xml:id="id_52"/>
               </ns19:discountAmount>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:totalAmount > 0">
                   <ns19:discountPercentage xml:id="id_126">
                      <xsl:value-of select="100 * (/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:discountAmount div /ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:totalAmount)"
                                    xml:id="id_127"/>
                   </ns19:discountPercentage>               
               </xsl:if>
               <ns19:documentId xml:id="id_53">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:documentType"
                                xml:id="id_54"/>
               </ns19:documentId>
               <ns19:documentOriginalId xml:id="id_55">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:documentTypeRef"
                                xml:id="id_56"/>
               </ns19:documentOriginalId>
               <ns19:documentStatus xml:id="id_132">
                  <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.documentStatus')]/@value"
                                xml:id="id_133"/>
               </ns19:documentStatus>
               <ns19:exemptAmount xml:id="id_124">
                  <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.exemptAmount')]/@value"
                                xml:id="id_125"/>
               </ns19:exemptAmount>
               <ns19:generationFormat xml:id="id_136">
                  <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.generationFormat')]/@value"
                                xml:id="id_137"/>
               </ns19:generationFormat>
               <ns19:indPaymenMethod xml:id="id_119">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:PaymentMethod/ns21:paymentMethodType"
                                xml:id="id_120"/>
               </ns19:indPaymenMethod>
               <ns19:marginAmount xml:id="id_130">
                  <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.marginAmount')]/@value"
                                xml:id="id_131"/>
               </ns19:marginAmount>
               <ns19:netAmount xml:id="id_141">
                  <xsl:value-of select="sum(/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingProductCharge/ns11:netAmount)"
                                xml:id="id_142"/>
               </ns19:netAmount>
               <ns19:nonBillable xml:id="id_143">
                  <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.nonBillable')]/@value"
                                xml:id="id_144"/>
               </ns19:nonBillable>
               <ns19:paymentDueDate xml:id="id_57">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:paymentDueDate"
                                xml:id="id_58"/>
               </ns19:paymentDueDate>
               <xsl:if test="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:totalAmount > 0">               
                   <ns19:presentIndicator xml:id="id_128">
                      <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.presentIndicator.0')]/@value"
                                    xml:id="id_129"/>
                   </ns19:presentIndicator>
               </xsl:if>
               <xsl:if test="not(/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:totalAmount > 0)">
                   <ns19:presentIndicator xml:id="id_128">
                      <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.presentIndicator.1')]/@value"
                                    xml:id="id_129"/>
                   </ns19:presentIndicator>
               </xsl:if>
               <ns19:rechargeAmount xml:id="id_145">
                  <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.rechargeAmount')]/@value"
                                xml:id="id_146"/>
               </ns19:rechargeAmount>
               <ns19:rechargePercentage xml:id="id_134">
                  <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.rechargePercentage')]/@value"
                                xml:id="id_135"/>
               </ns19:rechargePercentage>
               <ns19:totalAmount xml:id="id_59">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:totalAmount"
                                xml:id="id_60"/>
               </ns19:totalAmount>
               <ns19:totalTaxValue xml:id="id_61">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:totalTaxValue"
                                xml:id="id_62"/>
               </ns19:totalTaxValue>
               <ns19:urlAttachedDocument xml:id="id_63">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:urlAttachedDocument"
                                xml:id="id_64"/>
               </ns19:urlAttachedDocument>
               <ns19:vendorId xml:id="id_65">
                  <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:vendorId"
                                xml:id="id_66"/>
               </ns19:vendorId>
               <xsl:for-each select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:AppliedCustomerBillingProductCharge">
                   <tns:AppliedCustomerBillingProductCharge xml:id="id_76">
                     <ns11:conceptCode xml:id="id_162"></ns11:conceptCode>
                     <ns11:costCenter xml:id="id_149">
                        <xsl:value-of select="$ParameterManager_get_RSP.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.costCenter')]/@value"
                                      xml:id="id_150"/>
                     </ns11:costCenter>
                     <ns11:description xml:id="id_77">
                        <xsl:value-of select="ns11:description" xml:id="id_78"/>
                     </ns11:description>
                     <ns11:discountAmount xml:id="id_79">
                        <xsl:value-of select="100 * (ns11:discountAmount div ns11:totalItem)" xml:id="id_80"/>
                     </ns11:discountAmount>
                     <ns11:discountPercentage xml:id="id_151">
                        <xsl:value-of select="ns11:discountAmount" xml:id="id_152"/>
                     </ns11:discountPercentage>
                     <ns11:itemCode xml:id="id_87">
                        <xsl:value-of select="ns11:productId" xml:id="id_88"/>
                     </ns11:itemCode>
                     <ns11:itemNumber xml:id="id_81">
                        <xsl:value-of select="ns11:itemNumber" xml:id="id_82"/>
                     </ns11:itemNumber>
                     <ns11:itemPrice xml:id="id_83">
                        <xsl:value-of select="ns11:itemPrice" xml:id="id_84"/>
                     </ns11:itemPrice>
                     <ns11:itemType xml:id="id_163">
                        <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.itemType')]/@value"
                                      xml:id="id_164"/>
                     </ns11:itemType>
                     <ns11:netAmount xml:id="id_85">
                        <xsl:value-of select="ns11:netAmount" xml:id="id_86"/>
                     </ns11:netAmount>
                     <ns11:quantityItem xml:id="id_89">
                        <xsl:value-of select="ns11:quantityItem" xml:id="id_90"/>
                     </ns11:quantityItem>
                     <ns11:rechargePercentage xml:id="id_153">
                        <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.rechargePercentage')]/@value"
                                      xml:id="id_154"/>
                     </ns11:rechargePercentage>
                     <ns11:serialNumber xml:id="id_91">
                        <xsl:value-of select="ns11:serialNumber" xml:id="id_92"/>
                     </ns11:serialNumber>
                     <ns11:taxIndicator xml:id="id_93">
                        <xsl:value-of select="ns11:taxIndicator" xml:id="id_94"/>
                     </ns11:taxIndicator>
                     <ns11:totalItem xml:id="id_95">
                        <xsl:value-of select="ns11:totalItem" xml:id="id_96"/>
                     </ns11:totalItem>
                  </tns:AppliedCustomerBillingProductCharge>              
               </xsl:for-each>
               <tns:CustomerAccount xml:id="id_25">
                  <ns35:accountType xml:id="id_158">
                     <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.accountType')]/@value"
                                   xml:id="id_159"/>
                  </ns35:accountType>
                  <ns35:billingID xml:id="id_29">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns35:billingID"
                                   xml:id="id_30"/>
                  </ns35:billingID>
                  <ns35:contract xml:id="id_31">
                     <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns35:externalID"
                                   xml:id="id_32"/>
                  </ns35:contract>
                  <tns:CustomerAddress xml:id="id_33">
                     <tns:postalDeliveryAddress xml:id="id_34">
                        <ns18:address xml:id="id_35">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns18:address"
                                         xml:id="id_36"/>
                        </ns18:address>
                        <ns18:city xml:id="id_37">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns24:city"
                                         xml:id="id_38"/>
                        </ns18:city>
                        <ns18:commune xml:id="id_39">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns24:commune"
                                         xml:id="id_40"/>
                        </ns18:commune>
                     </tns:postalDeliveryAddress>
                  </tns:CustomerAddress>
                  <tns:CustomerPayment xml:id="id_26">
                     <ns32:ID xml:id="id_27">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns32:ID"
                                      xml:id="id_28"/>
                     </ns32:ID>
                     <tns:Organization xml:id="id_67">
                        <ns50:partyId xml:id="id_114">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:ReceiverCompany/ns0:organizationName/ns42:shortName"
                                         xml:id="id_115"/>
                        </ns50:partyId>
                        <tns:PartyRole xml:id="id_68">
                           <tns:Resource xml:id="id_69">
                              <tns:UserNameResourceRole xml:id="id_70">
                                 <ns57:commonName xml:id="id_71">
                                    <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:vendorId"
                                                  xml:id="id_72"/>
                                 </ns57:commonName>
                              </tns:UserNameResourceRole>
                           </tns:Resource>
                        </tns:PartyRole>
                        <tns:EmployeeIdentification xml:id="id_73">
                           <ns62:employeeNr xml:id="id_74">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns19:vendorId"
                                            xml:id="id_75"/>
                           </ns62:employeeNr>
                        </tns:EmployeeIdentification>
                        <tns:OrganizationName xml:id="id_116">
                           <ns42:tradingName xml:id="id_117">
                              <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:ReceiverCompany/ns0:organizationName/ns42:tradingName"
                                            xml:id="id_118"/>
                           </ns42:tradingName>
                        </tns:OrganizationName>
                     </tns:Organization>
                     <tns:ThirdPartyCollectionPM xml:id="id_97">
                        <ns17:thirdPartyID xml:id="id_98">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:ThirdPartyCollectionPM/ns17:thirdPartyID"
                                         xml:id="id_99"/>
                        </ns17:thirdPartyID>
                     </tns:ThirdPartyCollectionPM>
                     <tns:ThirdPartyPayeeAgency xml:id="id_121">
                        <ns10:branch xml:id="id_122">
                           <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:ThirdPartyPayeeAgency/ns10:branch"
                                         xml:id="id_123"/>
                        </ns10:branch>
                     </tns:ThirdPartyPayeeAgency>
                  </tns:CustomerPayment>
                  <tns:FinancialChargeSpec xml:id="id_155">
                     <ns70:type xml:id="id_156">
                        <xsl:value-of select="$ParameterManager_get_RSP2.Out/ns1:GetRSP/ns1:Values/ns26:Item[@key=concat(/ns0:Publish_CollectionPostTransaction_REQ/ns48:RequestHeader/ns48:Trace/ns48:Service/@code,'.CHL_Publish_CollectionPostTransaction_OH.financialChargeSpec.type')]/@value"
                                      xml:id="id_157"/>
                     </ns70:type>
                  </tns:FinancialChargeSpec>
               </tns:CustomerAccount>
               <tns:ReceiverCompany xml:id="id_100">
                  <tns:emailContact xml:id="id_101">
                     <ns14:eMailAddress xml:id="id_102">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:Customer/ns0:EmailContact/ns14:eMailAddress"
                                      xml:id="id_103"/>
                     </ns14:eMailAddress>
                  </tns:emailContact>
                  <tns:geographicArea xml:id="id_104">
                     <ns24:city xml:id="id_105">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns24:city"
                                      xml:id="id_106"/>
                     </ns24:city>
                     <ns24:commune xml:id="id_107">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns24:commune"
                                      xml:id="id_108"/>
                     </ns24:commune>
                     <ns24:neighborhood xml:id="id_109">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerAccount/ns0:GeographicArea/ns24:neighborhood"
                                      xml:id="id_110"/>
                     </ns24:neighborhood>
                  </tns:geographicArea>
                  <tns:skill xml:id="id_160">
                     <ns47:skillSpecification xml:id="id_161"></ns47:skillSpecification>
                  </tns:skill>
                  <tns:streetName xml:id="id_111">
                     <ns51:name xml:id="id_112">
                        <xsl:value-of select="/ns0:Publish_CollectionPostTransaction_REQ/ns0:Body/ns0:CustomerPayment/ns0:CustomerBill/ns0:ReceiverCompany/ns0:streetName/ns51:name"
                                      xml:id="id_113"/>
                     </ns51:name>
                  </tns:streetName>
               </tns:ReceiverCompany>
            </tns:CustomerBill>
         </tns:Body>
      </tns:Publish_CustomerBill_REQ>
   </xsl:template>
</xsl:stylesheet>
