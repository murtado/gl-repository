xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/getCorrelation/v2";

declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $GroupStatus as xs:string external;
declare variable $GroupTag as xs:string external;
declare variable $RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::) external;

declare function local:get_GetCorrelationREQ_v2_ByTag($GroupStatus as xs:string, $GroupTag as xs:string, $RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::)) as element(){
        <ns2:GetCorrelationREQ>
            {$RequestHeader}
            <cor:GroupStatus>{$GroupStatus}</cor:GroupStatus>
            <cor:GroupTag>{$GroupTag}</cor:GroupTag>
        </ns2:GetCorrelationREQ>
};

local:get_GetCorrelationREQ_v2_ByTag($GroupStatus, $GroupTag, $RequestHeader)
