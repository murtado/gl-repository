xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/getNewConversation/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getNewConversation_ConversationManager_v1_CSM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";

declare variable $Result as element() (:: schema-element(ns2:Result) ::) external;
declare variable $Conv_ID as xs:string external;

declare function local:get_GetNewConversationRSP_Adapter($Result as element() (:: schema-element(ns2:Result) ::), $Conv_ID as xs:string) as element() (:: schema-element(ns1:GetNewConversationRSP) ::) {
    <ns1:GetNewConversationRSP>
        {
          $Result
        }
        <ns1:ConversationID>{fn:data($Conv_ID)}</ns1:ConversationID>
    </ns1:GetNewConversationRSP>
};

local:get_GetNewConversationRSP_Adapter($Result, $Conv_ID)
