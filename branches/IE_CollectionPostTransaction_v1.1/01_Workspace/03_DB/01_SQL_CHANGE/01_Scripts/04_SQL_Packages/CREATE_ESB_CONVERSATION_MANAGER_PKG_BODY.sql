create or replace PACKAGE BODY ESB_CONVERSATION_MANAGER_PKG
AS

---------------------------------------------------------------------------------------------------
	PROCEDURE getConversationStatus(

		p_CONVERSATION_ID 		IN VARCHAR2, 
		
		p_STATUS				OUT VARCHAR2,
		p_RSP_MSG 			    OUT CLOB,
		p_CAN_ERROR_ID 			OUT NUMBER) IS

			BEGIN
      
				p_STATUS := '';
				p_RSP_MSG := null;
				p_CAN_ERROR_ID := '';
     		
				SELECT CS.STATUS, CS.RSP_MSG, CS.CAN_ERR_ID INTO p_STATUS, p_RSP_MSG, p_CAN_ERROR_ID FROM ESB_CONVERSATION_STATUS CS
				WHERE CONVERSATION_ID = p_CONVERSATION_ID;
			
			EXCEPTION
				WHEN NO_DATA_FOUND THEN 
					p_STATUS := 'NO SE ENCONTRARON DATOS. '||SQLERRM;
				WHEN OTHERS THEN 
					p_STATUS := 'ERROR AL CONSULTAR EN TABLA ESB_CONVERSATION_STATUS. '||SQLERRM;						
				
	END getConversationStatus;

---------------------------------------------------------------------------------------------------
	PROCEDURE updateConversationStatus(

		p_CONVERSATION_ID 	  IN VARCHAR2, 
		p_STATUS 			      	IN VARCHAR2,
		p_RSP_MSG 			    	IN CLOB,
		p_CAN_ERROR_CODE  	 	IN NUMBER,
		p_CAN_ERROR_TYPE     	IN VARCHAR2,
		p_UPDATE_COMPONENT		IN VARCHAR2,
		p_UPDATE_OPERATION		IN VARCHAR2,
    
		p_RESULT_CODE			OUT VARCHAR2,
		p_RESULT_DESCRIPTION 	OUT VARCHAR2) IS
    
    status VARCHAR2(30);
    
		BEGIN
       
      SELECT CS.STATUS INTO status FROM ESB_CONVERSATION_STATUS CS
      WHERE CONVERSATION_ID = p_CONVERSATION_ID;
      
      IF NOT(status IS NULL) THEN 
          UPDATE esb_conversation_status CS SET cs.status = p_status, 
                              cs.rsp_msg = p_rsp_msg, 
                              cs.can_err_id = (select CE.ID from esb_canonical_error CE  INNER JOIN esb_canonical_error_type CET ON CET.ID = CE.TYPE_ID
                                       where CE.CODE = p_CAN_ERROR_CODE AND CET.TYPE = p_CAN_ERROR_TYPE),
                              cs.update_component = p_update_component, 
                              cs.update_operation = p_update_operation, 
                              cs.update_timestamp = TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF') 
                             
                              WHERE cs.conversation_id = p_conversation_id
    
          RETURNING cs.conversation_id INTO p_RESULT_CODE;
          		
     END IF;
     
		EXCEPTION    
    
      WHEN NO_DATA_FOUND then
      
       insert into esb_conversation_status (CONVERSATION_ID, STATUS, RSP_MSG, CAN_ERR_ID, CREATION_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_COMPONENT, UPDATE_OPERATION)
       values (p_CONVERSATION_ID, p_STATUS, p_RSP_MSG, (select CE.ID from esb_canonical_error CE  INNER JOIN esb_canonical_error_type CET ON CET.ID = CE.TYPE_ID
                                       where CE.CODE = p_CAN_ERROR_CODE AND CET.TYPE = p_CAN_ERROR_TYPE), TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF') ,
                                       TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF') , p_UPDATE_COMPONENT, p_UPDATE_OPERATION );
                          
	  WHEN OTHERS THEN 
			p_RESULT_CODE := '-99';
			p_RESULT_DESCRIPTION := 'ERROR AL INSERTAR EN TABLA ESB_CONVERSATION_STATUS. '||SQLERRM;	
    
    END updateConversationStatus;
---------------------------------------------------------------------------------------------------    
PROCEDURE putConversationStatus(P_CONVERSATION_ID IN VARCHAR2, p_RESULT_CODE OUT VARCHAR2, p_RESULT_DESCRIPTION OUT VARCHAR2 )
IS

status VARCHAR2(30);
duplicate_transaction EXCEPTION;

BEGIN

   SELECT CS.STATUS INTO status FROM ESB_CONVERSATION_STATUS CS
   WHERE CONVERSATION_ID = p_CONVERSATION_ID;
   
   IF NOT(status IS NULL) THEN RAISE duplicate_transaction;
   END IF;

    
EXCEPTION 
  WHEN NO_DATA_FOUND THEN 
      INSERT INTO ESB_CONVERSATION_STATUS ( CONVERSATION_id, STATUS, CAN_ERR_ID, CREATION_TIMESTAMP,UPDATE_TIMESTAMP )
      VALUES ( P_CONVERSATION_ID,'PENDING',0,TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF'),null) ;
      COMMIT;
	
  WHEN duplicate_transaction THEN
      p_RESULT_CODE := '-99';
      p_RESULT_DESCRIPTION := 'ERROR AL INSERTAR EN TABLA ESB_CONVERSATION_STATUS. ' ||SQLERRM; 
  WHEN OTHERS THEN 
			p_RESULT_CODE := '-99';
			p_RESULT_DESCRIPTION := 'ERROR AL INSERTAR EN TABLA ESB_CONVERSATION_STATUS. '||SQLERRM;		
	
END putConversationStatus;
---------------------------------------------------------------------------------------------------  
PROCEDURE getConsumerCallbackURL(
    p_SYSTEM_CODE 		    IN VARCHAR2, 
    p_COUNTRY_CODE        IN VARCHAR2,
    p_ENTERPRISE_CODE     IN VARCHAR2,
    p_TRANSPORT           OUT VARCHAR2,
    p_URL                 OUT VARCHAR2,
		p_RESULT_CODE 			  OUT VARCHAR2,
		p_RESULT_DESCRIPTION 	OUT VARCHAR2) AS
       
    
    BEGIN
      --Get URL
      SELECT ESB_CONSUMER_DETAILS.DETAIL_CONTENT INTO p_URL
      FROM ESB_COUNTRY, ESB_CONSUMER, ESB_CONSUMER_DETAILS, ESB_CONSUMER_DETAILS_TYPE, ESB_SYSTEM, ESB_ENTERPRISE
      WHERE ESB_COUNTRY.ID = ESB_CONSUMER.COUNTRY_ID AND ESB_COUNTRY.CODE = p_COUNTRY_CODE AND 
            ESB_CONSUMER.ID = ESB_CONSUMER_DETAILS.CONSUMER_ID AND ESB_CONSUMER_DETAILS.DETAIL_TYPE_ID = ESB_CONSUMER_DETAILS_TYPE.ID AND ESB_CONSUMER_DETAILS_TYPE.NAME = 'URL' AND 
            ESB_SYSTEM.ID = ESB_CONSUMER.SYSCODE AND ESB_SYSTEM.CODE = p_SYSTEM_CODE AND 
            ESB_ENTERPRISE.ID = ESB_CONSUMER.ENT_CODE AND ESB_ENTERPRISE.CODE = p_ENTERPRISE_CODE ;
      --Get Transport
      SELECT ESB_CONSUMER_DETAILS.DETAIL_CONTENT INTO p_TRANSPORT
      FROM ESB_COUNTRY, ESB_CONSUMER, ESB_CONSUMER_DETAILS, ESB_CONSUMER_DETAILS_TYPE, ESB_SYSTEM, ESB_ENTERPRISE
      WHERE ESB_COUNTRY.ID = ESB_CONSUMER.COUNTRY_ID AND ESB_COUNTRY.CODE = p_COUNTRY_CODE AND 
            ESB_CONSUMER.ID = ESB_CONSUMER_DETAILS.CONSUMER_ID AND ESB_CONSUMER_DETAILS.DETAIL_TYPE_ID = ESB_CONSUMER_DETAILS_TYPE.ID AND ESB_CONSUMER_DETAILS_TYPE.NAME = 'TRANSPORT' AND 
            ESB_SYSTEM.ID = ESB_CONSUMER.SYSCODE AND ESB_SYSTEM.CODE = p_SYSTEM_CODE AND 
            ESB_ENTERPRISE.ID = ESB_CONSUMER.ENT_CODE AND ESB_ENTERPRISE.CODE = p_ENTERPRISE_CODE ;
        
      p_RESULT_CODE := '0';
      p_RESULT_DESCRIPTION := 'Ejecucion exitosa.';
      
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_RESULT_CODE := '-99';
        p_RESULT_DESCRIPTION := 'NO SE HAN ENCONTRADO DATOS. '  || SQLERRM;
        
       WHEN OTHERS THEN
        p_RESULT_CODE := '-98';
        p_RESULT_DESCRIPTION := 'SE HA PRODUCIDO UN ERROR EN LA BUSQUEDA DE LA URI. ' || SQLERRM;
      

    END;


END ESB_CONVERSATION_MANAGER_PKG;