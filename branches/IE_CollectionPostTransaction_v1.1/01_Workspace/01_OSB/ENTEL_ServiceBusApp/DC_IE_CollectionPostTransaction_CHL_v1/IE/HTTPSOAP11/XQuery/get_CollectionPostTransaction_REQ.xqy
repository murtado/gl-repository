xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://v2.ws.poslog.xcenter.dtv/";
(:: import schema at "../XSD/PoslogObjReceiverApiService_schema1.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1";
(:: import schema at "../../../../ES_CollectionPostTransaction_v1/ESC/Primary/Publish_CollectionPostTransaction_v1_EBM.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/EBO/CustomerPayment/v1";

declare namespace ns6 = "http://www.entel.cl/EBO/BillingAccount/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/EmailContact/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/TelephoneNumber/v1";

declare namespace ns13 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns9 = "http://www.entel.cl/EBO/PostalDeliveryAddress/v1";

declare namespace ns12 = "http://www.entel.cl/EBO/StreetName/v1";

declare namespace ns11 = "http://www.entel.cl/EBO/Address/v1";

declare namespace ns10 = "http://www.entel.cl/EBO/GeographicArea/v1";

declare namespace ns17 = "http://www.entel.cl/EBO/PhysicalResource/v1";

declare namespace ns16 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1";

declare namespace ns15 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns14 = "http://www.entel.cl/EBO/IndividualName/v1";

declare namespace ns19 = "http://www.entel.cl/EBO/PhysicalResourceSpec/v1";

declare namespace ns18 = "http://www.entel.cl/EBO/MSISDN/v1";

declare namespace ns20 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1";

declare namespace ns24 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns23 = "http://www.entel.cl/EBO/LoyaltyTransaction/v1";

declare namespace ns22 = "http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1";

declare namespace ns21 = "http://www.entel.cl/EBO/OrganizationName/v1";

declare namespace ns28 = "http://www.entel.cl/EBO/StockTransaction/v1";

declare namespace ns27 = "http://www.entel.cl/EBO/Store/v1";

declare namespace ns26 = "http://www.entel.cl/EBO/PaymentMethod/v1";

declare namespace ns25 = "http://www.entel.cl/EBO/CustomerOrder/v1";

declare namespace ns29 = "http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1";

declare namespace ns30 = "http://v2.ws.poslog-ext.xcenter.dtv/";

declare variable $CollectionPostTransaction_IE_REQ as element() (:: schema-element(ns1:postTransaction) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::) external;



declare function local:getProperties($input as element ()) as element(){

  <items>
  {
  for $prop in $input/*:PosTransactionProperties
  return 
    <item code="{$prop/*:PosTransactionPropertyCode}" value="{$prop/*:PosTransactionPropertyValue}" />
  }
  </items>
  };

declare function local:get_ColecctionPostTransaction_REQ($CollectionPostTransaction_IE_REQ as element() (:: schema-element(ns1:postTransaction) ::), $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::)) as element() (:: schema-element(ns2:Publish_CollectionPostTransaction_REQ) ::) {
    let $items := local:getProperties($CollectionPostTransaction_IE_REQ/transaction)
    return
    <ns2:Publish_CollectionPostTransaction_REQ>
        {$RequestHeader}
        <ns2:Body>

            <ns2:CustomerPayment>
                <ns4:ID>{fn:concat(
                fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailStoreID),
                fn:data($CollectionPostTransaction_IE_REQ/transaction/WorkstationID), 
                fn:data($CollectionPostTransaction_IE_REQ/transaction/TillID), 
                fn:data($CollectionPostTransaction_IE_REQ/transaction/SequenceNumber), 
                xs:string(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate)),
                xs:string(fn:data($CollectionPostTransaction_IE_REQ/transaction/BeginDateTime)),
                xs:string(fn:data($CollectionPostTransaction_IE_REQ/transaction/EndDateTime)),
                fn:data($CollectionPostTransaction_IE_REQ/transaction/OperatorID)
                )
                }</ns4:ID>
                {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/TransactionLink[@ReasonCode='Return'])
                    then 
                    <ns4:cancelTransactionID>{fn:concat(
                    fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/TransactionLink/RetailStoreID),
                    fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/TransactionLink/WorkstationID),
                    fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/TransactionLink/SequenceNumber),
                    xs:string(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate))
                    )
                    }</ns4:cancelTransactionID>
                    else ()
                }
                {
                if ($items/item[@code='CLAVE_RECONCILIACION']/@value) then
                  <ns4:operatorID>{fn:data($items/item[@code='CLAVE_RECONCILIACION']/@value)}</ns4:operatorID>
                else 
                (
                  <ns4:operatorID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/OperatorID)}</ns4:operatorID>
                )
                }
                {
                if ($items/item[@code='FECHA_CONTABLE']/@value) then
                  <ns4:paymentDate>{fn:data($items/item[@code='FECHA_CONTABLE']/@value)}</ns4:paymentDate>
                else 
                (
                  <ns4:paymentDate>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time())}</ns4:paymentDate>
                )
                }
				
                <ns4:paymentDateLogical>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time())}</ns4:paymentDateLogical>
               {
					if ($CollectionPostTransaction_IE_REQ/transaction/SequenceNumber) then
                      <ns4:sequenceNumber>{fn:data($CollectionPostTransaction_IE_REQ/transaction/SequenceNumber)}</ns4:sequenceNumber>
                    else ()
			   }
			   {
					if (data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder/@Action) !='') then
                    <ns4:transactionDetail>{data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder/@Action)}</ns4:transactionDetail>
                    else ()
			   }
               
                {
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Canceled']) then
                    <ns4:transactionSubType>Canceled</ns4:transactionSubType>
                  else 
                (
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Completed']) then 
                    <ns4:transactionSubType>Completed</ns4:transactionSubType>
                  else 
                (
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Paywithoutpickup']) then 
                    <ns4:transactionSubType>Paywithoutpickup</ns4:transactionSubType>
                  else ()
                )
                )
                }
                {
                if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder) then
                  <ns4:transactionType>PreviousCustomerOrder</ns4:transactionType>
                else 
                (
                if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Sale[@ItemType="Stock"]) then
                  <ns4:transactionType>Sale</ns4:transactionType>
                else 
                (
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType="Stock"]) then
                  <ns4:transactionType>Return</ns4:transactionType>
                  else () 
                )
                )
                }
                { 
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Cancelled']) then
                         <ns4:transactionCode>1</ns4:transactionCode>
                         else    
                         if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Completed']) then
                               <ns4:transactionCode>0</ns4:transactionCode>                               
                               else()
                   
                }
                
                <ns2:CustomerAccount>
                    <ns5:ID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/AccountNumber)}</ns5:ID>
                    {
                    if ($items/item[@code='CUSTOMER_BSCS_ID']/@value) then
                      <ns5:billingID>{fn:data($items/item[@code='CUSTOMER_BSCS_ID']/@value)}</ns5:billingID>
                    else ()
                    }
                    {
                    if ($items/item[@code='CONTRACT_ACCOUNT']/@value) then
                      <ns5:externalID>{fn:data($items/item[@code='CONTRACT_ACCOUNT']/@value)}</ns5:externalID>
                    else ()
                    }
                    <ns2:BillingAccount>
                    {
                    if ($items/item[@code='CUSTOMER_BSCS_ID']/@value) then
                        <ns6:externalID>{fn:data($items/item[@code='CUSTOMER_BSCS_ID']/@value)}</ns6:externalID>
                    else ()
                    }
                    </ns2:BillingAccount>
                    <ns2:Contact>
                        <ns2:cellPhone>
										{
											if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/TelephoneNumber) then
											<ns7:number>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/TelephoneNumber)}</ns7:number>
											else ()
										}
                            
                        </ns2:cellPhone>
                    </ns2:Contact>
                    <ns2:Customer>
                        <ns2:EmailContact>
                            <ns8:eMailAddress>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/eMail)}</ns8:eMailAddress>
                        </ns2:EmailContact>
                    </ns2:Customer>
                    <ns2:CustomerAddress>
                        <ns2:postalDeliveryAddress>
                            <ns9:address>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/PostalCode)}</ns9:address>
                        </ns2:postalDeliveryAddress>
                    </ns2:CustomerAddress>
                    <ns2:GeographicArea>
                        {
                        if ($items/item[@code='STORE_CITY']/@value) then
                        <ns10:city>{fn:data($items/item[@code='STORE_CITY']/@value)}</ns10:city>
                        else ()
                        }
                        {
                        if ($items/item[@code='STORE_NEIGHBORHOOD']/@value) then
                          <ns10:commune>{fn:data($items/item[@code='STORE_NEIGHBORHOOD']/@value)}</ns10:commune>
                        else ()
                        }
                        <ns10:neighborhood>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/Neighborhood)}</ns10:neighborhood>
                    </ns2:GeographicArea>
                    <ns2:GeographicalAddress>
                        <ns2:Address>
                            {
                            if ($items/item[@code='STORE_ADDR']/@value) then
                              <ns11:addressName>{fn:data($items/item[@code='STORE_ADDR']/@value)}</ns11:addressName>
                            else ()
                            }
                        </ns2:Address>
                    </ns2:GeographicalAddress>
                    <ns2:IndividualIdentification>
                        {
                        if ($items/item[@code='CUSTOMER_DOC_TYPE']/@value) then
                          <ns13:type>{fn:data($items/item[@code='CUSTOMER_DOC_TYPE']/@value)}</ns13:type>
                        else ()
                        }
                    </ns2:IndividualIdentification>
                    <ns2:IndividualName>
                        <ns14:firstName>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Name)}</ns14:firstName>
                        <ns14:lastName>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Name)}</ns14:lastName>
                    </ns2:IndividualName>
                </ns2:CustomerAccount>
                <ns2:CustomerBill>
                    <ns15:adjustmentDate>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time())}</ns15:adjustmentDate>
                    {
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock']) then
                      <ns15:adjustmentDescription></ns15:adjustmentDescription>
                      else()
                    }
                    <ns15:appliedDate>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate),fn:current-time())}</ns15:appliedDate>
                    {
                    if ($items/item[@code = 'FECHA_TRANSACCION']/@value) then
                      <ns15:billDate>{fn:data($items/item[@code = 'FECHA_TRANSACCION']/@value)}</ns15:billDate>
                    else (
                      <ns15:billDate>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time())}</ns15:billDate>
                    )
                    }
                    {
                    if ($items/item[@code = 'FOLIO']/@value) then 
                      <ns15:billNo>{fn:data($items/item[@code='FOLIO']/@value)}</ns15:billNo>
                    else ()
                    }
                    {
                    if ($items/item[@code='FOLIO_ORIGINAL']/@value) then
                    <ns15:billNoInv>{fn:data($items/item[@code='FOLIO_ORIGINAL']/@value)}</ns15:billNoInv>
                    else ()
                    }
                    {
                    if ($items/item[@code='FOLIO_ORIGINAL']/@value) then
                      <ns15:billNoOriginal>{fn:data($items/item[@code='FOLIO_ORIGINAL']/@value)}</ns15:billNoOriginal>
                    else ()
                    }
                    {
                    if ($items/item[@code='DIGSIG_DATE']/@value) then
                      <ns15:dateDigitalSignature>{fn:data($items/item[@code='DIGSIG_DATE']/@value)}</ns15:dateDigitalSignature>
                    else ()
                    }
                    {
                    if ($items/item[@code='TIMBRE_HASH']/@value) then
                      <ns15:digitalSignature>{fn:data($items/item[@code='TIMBRE_HASH']/@value)}</ns15:digitalSignature>
                    else ()
                    }
                    <ns15:discountAmount>{fn:sum($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/SaleForDelivery/RetailPriceModifier[1]/Amount[@Action='Subtract'])}</ns15:discountAmount>
                    
                    {
                    if ($items/item[@code='TRANSACTION_DOC_TYPE']/@value) then
                      <ns15:documentType>{fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)}</ns15:documentType>
                    else ()
                    }
                    {
                    if ($items/item[@code='DOC_TYPE_ORIGINAL']/@value) then
                      <ns15:documentTypeInv>{fn:data($items/item[@code='DOC_TYPE_ORIGINAL']/@value)}</ns15:documentTypeInv>
                    else ()
                    }
                    {
                    if ($items/item[@code='DOC_TYPE_ORIGINAL']/@value) then
                      <ns15:documentTypeRef>{fn:data($items/item[@code='DOC_TYPE_ORIGINAL']/@value)}</ns15:documentTypeRef>
                    else ()
                    }
                    {
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock']) then
                      <ns15:operationType>I: Crea el ajuste</ns15:operationType>
                      else ()
                    }
					
					{
					  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender/Amount) then
                      <ns15:paymentAmount>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender/Amount)}</ns15:paymentAmount>
                      else ()
					}
                    
                    <ns15:paymentDueDate>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time())}</ns15:paymentDueDate>
                    
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return[@ItemType='Stock'] and xs:date($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return/ns30:LineItemProperty[ns30:LineItemPropertyCode='ORIGINAL_DATE']/ns30:LineItemPropertyValue) < $CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate) then
                      <ns15:returnScenario>DDP</ns15:returnScenario>
                    else 
                    (
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return[@ItemType='Stock'] and xs:date($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return/ns30:LineItemProperty[ns30:LineItemPropertyCode='ORIGINAL_DATE']/ns30:LineItemPropertyValue) = $CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate) then
                        <ns15:returnScenario>DMD</ns15:returnScenario>
                      else 
                      (
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender[@TenderType='Voucher'] and $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender/TenderID = '<!CDATA[ISSUE_STORE_CREDIT]]>') then
                          <ns15:returnScenario>DCT</ns15:returnScenario>
                        else 
                      (
                        if ($items/item[@code='<!CDATA[ENTEL_STORE_IND]]> = "N"']) then
                          <ns15:returnScenario>FP</ns15:returnScenario>
                        else 
                        (
                          <ns15:returnScenario>VTA</ns15:returnScenario>
                        )
                      )
                      )
                    )
                    }
                    {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount'])
                        then <ns15:totalAmount>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount'])}</ns15:totalAmount>
                        else ()
                    }
					{
						 if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Tax[1]/Amount)
                        then <ns15:totalTaxValue>{fn:sum($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Tax[1]/Amount)}</ns15:totalTaxValue>
                        else ()
					}
                    
                    {
                    if ($items/item[@code='TIMBRE_DOC_URL']/@value) then
                      <ns15:urlAttachedDocument>{fn:data($items/item[@code='TIMBRE_DOC_URL']/@value)}</ns15:urlAttachedDocument>
                    else ()
                    }
                    <ns15:vendorId>{fn:data($CollectionPostTransaction_IE_REQ/transaction/OperatorID)}</ns15:vendorId>
                    {
                        for $LineItem in $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem
                        return 
                        <ns2:AppliedCustomerBillingProductCharge>
                            {
                            if ($items/item[@code='COST_CENTER']/@value) then
                            <ns16:costCenter>{fn:data($items/item[@code='COST_CENTER']/@value)}</ns16:costCenter>
                            else ()
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/Description) then
                            <ns16:description>{fn:data($LineItem/PreviousCustomerOrder/Description)}</ns16:description>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock']/Description) then
                                <ns16:description>{fn:data($LineItem/Sale[@ItemType='Stock']/Description)}</ns16:description>
                              else 
                              (
                              if ($LineItem/Return[@ItemType='Stock']/Description) then
                                <ns16:description>{fn:data($LineItem/Return[@ItemType='Stock']/Description)}</ns16:description>
                              else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/RetailPriceModifier/Amount[@Action='Subtract']) then
                            <ns16:discountAmount>{fn:data($LineItem/PreviousCustomerOrder/RetailPriceModifier/Amount[@Action='Subtract'])}</ns16:discountAmount>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock']/RetailPriceModifier/Amount[@Action='Subtract']) then
                                <ns16:discountAmount>{fn:data($LineItem/Sale[@ItemType='Stock']/RetailPriceModifier/Amount[@Action='Subtract'])}</ns16:discountAmount>
                              else ()
                            )
                            }
                            <ns16:itemNumber>{fn:data($LineItem/SequenceNumber)}</ns16:itemNumber>
                            {
                            if ($LineItem/PreviousCustomerOrder/RegularSalesUnitPrice) then
                            <ns16:itemPrice>{fn:data($LineItem/PreviousCustomerOrder/RegularSalesUnitPrice)}</ns16:itemPrice>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock']/RegularSalesUnitPrice) then
                                <ns16:itemPrice>{fn:data($LineItem/Sale[@ItemType='Stock']/RegularSalesUnitPrice)}</ns16:itemPrice>
                              else 
                              (
                               if ($LineItem/Return[@ItemType='Stock']/RegularSalesUnitPrice) then
                                <ns16:itemPrice>{fn:data($LineItem/Return[@ItemType='Stock']/RegularSalesUnitPrice)}</ns16:itemPrice>
                              else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/ExtendedAmount) then
                            <ns16:netAmount>{fn:data($LineItem/PreviousCustomerOrder/ExtendedAmount)}</ns16:netAmount>
                            else 
                            (
                            if ($LineItem/Sale[@ItemType='Stock']/ExtendedAmount) then
                              <ns16:netAmount>{fn:data($LineItem/Sale[@ItemType='Stock']/ExtendedAmount)}</ns16:netAmount>
                            else 
                            (
                            if ($LineItem/Return[@ItemType='Stock']/ExtendedAmount) then
                              <ns16:netAmount>{fn:data($LineItem/Return[@ItemType='Stock']/ExtendedAmount)}</ns16:netAmount>
                            else ()
                            )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/ItemID) then
                              <ns16:productId>{fn:data($LineItem/PreviousCustomerOrder/ItemID)}</ns16:productId>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock']/ItemID) then
                                <ns16:productId>{fn:data($LineItem/Sale[@ItemType='Stock']/ItemID)}</ns16:productId>
                              else
                              (
                              if ($LineItem/Return[@ItemType='Stock']/ItemID) then
                                <ns16:productId>{fn:data($LineItem/Return[@ItemType='Stock']/ItemID)}</ns16:productId>
                              else()
                              )
                            )
                            }
                            {
                              if ($LineItem/PreviousCustomerOrder/Quantity) then
                                <ns16:quantityItem>{fn:data($LineItem/PreviousCustomerOrder/Quantity)}</ns16:quantityItem>
                              else 
                              (
                                if ($LineItem/Sale[@ItemType='Stock']/Quantity) then
                                  <ns16:quantityItem>{fn:data($LineItem/Sale[@ItemType='Stock']/Quantity)}</ns16:quantityItem>
                                else 
                                (
                                  if ($LineItem/Return[@ItemType='Stock']/Quantity) then
                                    <ns16:quantityItem>{fn:data($LineItem/Return[@ItemType='Stock']/Quantity)}</ns16:quantityItem>
                                  else () 
                                )
                              )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/SerialNumber) then
                              <ns16:serialNumber>{fn:data($LineItem/PreviousCustomerOrder/SerialNumber)}</ns16:serialNumber>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock']/SerialNumber) then
                                <ns16:serialNumber>{fn:data($LineItem/Sale[@ItemType='Stock']/SerialNumber)}</ns16:serialNumber>
                              else 
                              (
                                if ($LineItem/Return[@ItemType='Stock']/SerialNumber) then
                                  <ns16:serialNumber>{fn:data($LineItem/Return[@ItemType='Stock']/SerialNumber)}</ns16:serialNumber>
                                else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId) then
                            <ns16:taxCode>{fn:data($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId)}</ns16:taxCode>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxCode>{fn:data($LineItem/Sale[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId)}</ns16:taxCode>
                              else 
                              (
                              if ($LineItem/Return[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxCode>{fn:data($LineItem/Return[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId)}</ns16:taxCode>
                              else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId) then
                            <ns16:taxIndicator>{fn:data($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId)}</ns16:taxIndicator>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxIndicator>{fn:data($LineItem/Sale[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId)}</ns16:taxIndicator>
                              else 
                              (
                              if ($LineItem/Return[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxIndicator>{fn:data($LineItem/Return[@ItemType='Stock']/Tax[1]/ns30:TaxGroupId)}</ns16:taxIndicator>
                              else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/ExtendedAmount) then
                              <ns16:totalItem>{fn:data($LineItem/PreviousCustomerOrder/ExtendedAmount)}</ns16:totalItem>
                            else 
                            (
                            if ($LineItem/Sale[@ItemType='Stock']/ExtendedAmount) then
                              <ns16:totalItem>{fn:data($LineItem/Sale[@ItemType='Stock']/ExtendedAmount)}</ns16:totalItem>
                            else 
                            (
                            if ($LineItem/Return[@ItemType='Stock']/ExtendedAmount) then
                              <ns16:totalItem>{fn:data($LineItem/Return[@ItemType='Stock']/ExtendedAmount)}</ns16:totalItem>
                            else ()
                            )
                            )
                            }
                            <ns2:PhysicalResource>
                              {
                              if ($LineItem/PreviousCustomerOrder/SerialNumber) then
                                <ns17:imei>{fn:data($LineItem/PreviousCustomerOrder/SerialNumber)}</ns17:imei>
                              else 
                                (
                                  if ($LineItem/Sale[@ItemType='Stock']/SerialNumber) then
                                    <ns17:imei>{fn:data($LineItem/Sale[@ItemType='Stock']/SerialNumber)}</ns17:imei>
                                  else 
                                  (
                                    if ($LineItem/Return[@ItemType='Stock']/SerialNumber) then
                                      <ns17:imei>{fn:data($LineItem/Return[@ItemType='Stock']/SerialNumber)}</ns17:imei>
                                    else ()
                                  )
                                )
                              }
                              {
                                if ($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                  <ns17:serialNumber>{fn:data($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:serialNumber>
                                else 
                                (
                                  if ($LineItem/Sale[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                    <ns17:serialNumber>{fn:data($LineItem/Sale[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:serialNumber>
                                  else 
                                  (
                                    if ($LineItem/Return[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                      <ns17:serialNumber>{fn:data($LineItem/Return[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:serialNumber>
                                    else ()
                                  )
                                )
                              }
                                <ns2:Asset>
                                    <ns2:MSISDN>
                                        {
                                        if ($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue) then
                                        <ns18:SN>{fn:data($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)}</ns18:SN>
                                        else 
                                        (
                                          if ($LineItem/Sale[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue) then
                                            <ns18:SN>{fn:data($LineItem/Sale[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)}</ns18:SN>
                                          else 
                                          (
                                          if ($LineItem/Return[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue) then
                                            <ns18:SN>{fn:data($LineItem/Return[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)}</ns18:SN>
                                          else ()
                                          )
                                        )
                                        }
                                    </ns2:MSISDN>
                                </ns2:Asset>
                                <ns2:PhysicalResourceSpec>
                                    {
                                    if ($LineItem/PreviousCustomerOrder/Description) then
                                    <ns19:description>{fn:data($LineItem/PreviousCustomerOrder/Description)}</ns19:description>
                                    else 
                                    (
                                      if ($LineItem/Sale[@ItemType='Stock']/Description) then
                                        <ns19:description>{fn:data($LineItem/Sale[@ItemType='Stock']/Description)}</ns19:description>
                                      else 
                                      (
                                      if ($LineItem/Return[@ItemType='Stock']/Description) then
                                        <ns19:description>{fn:data($LineItem/Return[@ItemType='Stock']/Description)}</ns19:description>
                                      else ()
                                      )
                                    )
                                    }
                                    {
                                    if ($LineItem/PreviousCustomerOrder/ItemID) then
                                      <ns19:skuNumber>{fn:data($LineItem/PreviousCustomerOrder/ItemID)}</ns19:skuNumber>
                                    else 
                                    (
                                      if ($LineItem/Sale[@ItemType='Stock']/ItemID) then
                                        <ns19:skuNumber>{fn:data($LineItem/Sale[@ItemType='Stock']/ItemID)}</ns19:skuNumber>
                                      else 
                                      (
                                      if ($LineItem/Return[@ItemType='Stock']/ItemID) then
                                        <ns19:skuNumber>{fn:data($LineItem/Return[@ItemType='Stock']/ItemID)}</ns19:skuNumber>
                                      else ()
                                      )
                                    )
                                    }
                                </ns2:PhysicalResourceSpec>
                            </ns2:PhysicalResource>
                        </ns2:AppliedCustomerBillingProductCharge>
                    }
                    <ns2:AppliedCustomerBillingRate>
                        <ns2:AppliedCustomerBillingProductAlteration>
                            {
                            if ($items/item[@code='FOLIO']/@value) then
                              <ns20:compensatedDocumentNumber>{fn:data($items/item[@code='FOLIO']/@value)}</ns20:compensatedDocumentNumber>
                            else ()
                            }
                            {
                            if ($items/item[@code='DOC_TYPE_ORIGINAL']/@value) then
                              <ns20:compensatedDocumentType>{fn:data($items/item[@code='DOC_TYPE_ORIGINAL']/@value)}</ns20:compensatedDocumentType>
                            else()
                            }
                            
                            {
                            if ($items/item[@code='TRANSACTION_DOC_TYPE']/@value) then
                              <ns20:documentType>{fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)}</ns20:documentType>
                            else ()
                            }
                            {
                            if ($items/item[@code='FOLIO_ORIGINAL']/@value) then
                              <ns20:referenceNumberForCompensation>{fn:data($items/item[@code='FOLIO_ORIGINAL']/@value)}</ns20:referenceNumberForCompensation>
                            else ()
                            }
                        </ns2:AppliedCustomerBillingProductAlteration>
                    </ns2:AppliedCustomerBillingRate>
                    <ns2:IssuingCompany>
                        <ns2:individualIdentification>
                            {
                            if ($items/item[@code='CUSTOMER_RUT']/@value) then 
                              <ns13:number>{fn:data($items/item[@code='CUSTOMER_RUT']/@value)}</ns13:number>
                            else ()
                            }
                        </ns2:individualIdentification>
                        <ns2:organizationName>
                            <ns21:shortName>PCS</ns21:shortName>
                            <ns21:tradingName>ENTEL PCS</ns21:tradingName>
                        </ns2:organizationName>
                    </ns2:IssuingCompany>
                    <ns2:LoyaltyTransaction>
                    {
                    if ($items/item[@code='LOYALTY_TRN_AMOUNT']/@value) then
                      <ns23:amount>{fn:data($items/item[@code='LOYALTY_TRN_AMOUNT']/@value)}</ns23:amount>
                    else ()
                    }
                    </ns2:LoyaltyTransaction>
                    <ns2:ReceiverCompany>
                        <ns2:emailContact>
                            <ns8:eMailAddress>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/eMail)}</ns8:eMailAddress>
                        </ns2:emailContact>
                        <ns2:geographicArea>
                            <ns10:city>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/City)}</ns10:city>
                            
                            <ns10:neighborhood>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/Neighborhood)}</ns10:neighborhood>
                        </ns2:geographicArea>
                        <ns2:organizationName>
                            <ns21:shortName>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Name)}</ns21:shortName>
                            
                        </ns2:organizationName>
                        <ns2:streetName>
                            <ns12:name>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine)}</ns12:name>
                        </ns2:streetName>
                    </ns2:ReceiverCompany>
                    <ns2:appliedAmount>
                        {
                            if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount'])
                            then <ns24:amount>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount'])}</ns24:amount>
                            else ()
                        }
                        
                    </ns2:appliedAmount>
                </ns2:CustomerBill>
                <ns2:CustomerOrder>
                    {
                    if ($items/item[@code='EOC_ORDER_ID']/@value) then
                      <ns25:ID>{fn:data($items/item[@code='EOC_ORDER_ID']/@value)}</ns25:ID>
                    else ()
                    }
                    {
                  <ns25:state>DELIVERY</ns25:state>
                   
                    }
                </ns2:CustomerOrder>
                <ns2:Money>
                    <ns24:units>{fn:data($CollectionPostTransaction_IE_REQ/transaction/CurrencyCode)}</ns24:units>
                </ns2:Money>
                <ns2:PaymentMethod>
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender[@TenderType='dtv:AccountCredit']) then 
                    <ns26:amountRef>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender[@TenderType='dtv:AccountCredit']/Amount)}</ns26:amountRef>
                    else ()
                    }
                    {
                    if ($items/item[@code='FOLIO']/@value and $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return/@ItemType = 'Stock') then 
                      <ns26:billNoNC>{fn:data($items/item[@code='FOLIO']/@value)}</ns26:billNoNC>
                    else ()
                    }
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='FECHA_VENCIMIENTO']/ns30:LineItemPropertyValue) then
                    <ns26:date>{xs:date(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='FECHA_VENCIMIENTO']/ns30:LineItemPropertyValue))}/</ns26:date>
                    else 
                    (
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Sale[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='FECHA_VENCIMIENTO']/ns30:LineItemPropertyValue) then
                      <ns26:date>{xs:date(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Sale[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='FECHA_VENCIMIENTO']/ns30:LineItemPropertyValue))}/</ns26:date>
                    else 
                    (
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='FECHA_VENCIMIENTO']/ns30:LineItemPropertyValue) then
                      <ns26:date>{xs:date(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock']/ns30:LineItemProperty[ns30:LineItemPropertyCode='FECHA_VENCIMIENTO']/ns30:LineItemPropertyValue))}/</ns26:date>
                    else ()
                    )
                    )
                    }
                    <ns26:paymentMethodType>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender/@TenderType)}</ns26:paymentMethodType>
                    
                    <ns2:amount>
					{
						if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender/Amount) then
                         <ns24:amount>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender/Amount)}</ns24:amount>
						else ()
					}
                        
                        
                    </ns2:amount>
                </ns2:PaymentMethod>
                <ns2:Store>
                    <ns27:ID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailStoreID)}</ns27:ID>
                    {
                    if ($items/item[@code='STORE_DESCRIPTION']/@value) then
                      <ns27:name>{fn:data($items/item[@code='STORE_DESCRIPTION']/@value)}</ns27:name>
                      else ()
                    }
                    {
                    if ($items/item[@code='ENTEL_STORE_IND']/@value) then
                      <ns27:storeInd>{fn:data($items/item[@code='ENTEL_STORE_IND']/@value)}</ns27:storeInd>
                    else ()
                    }
                    <ns2:GeographicAddress>
                        <ns2:Address>
                            <ns2:streetNameFull>
                                <ns12:name>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine)}</ns12:name>
                            </ns2:streetNameFull>
                        </ns2:Address>
                    </ns2:GeographicAddress>
                    <ns2:StockTransaction>
                        {
                        if ($items/item[@code='DELIVERY_IND']/@value) then
                          <ns28:deliveryInd>{fn:data($items/item[@code='DELIVERY_IND']/@value)}</ns28:deliveryInd>
                        else ()
                        }
                        <ns28:returnDate>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time())}</ns28:returnDate>
                        <ns28:transactionDate>{fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time())}</ns28:transactionDate>
                        {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock']) then
                          <ns28:transactionType>DVT</ns28:transactionType>
                        else 
                        (
                          <ns28:transactionType>VTA</ns28:transactionType>
                        )
                        }
                    </ns2:StockTransaction>
                </ns2:Store>
                <ns2:ThirdPartyCollectionPM>
                    <ns22:thirdPartyID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailStoreID)}</ns22:thirdPartyID>
                </ns2:ThirdPartyCollectionPM>
                <ns2:ThirdPartyPayeeAgency>
                    <ns29:branch>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailStoreID)}</ns29:branch>
                    {
                    if ($items/item[@code='STORE_DESCRIPTION']/@value) then
                      <ns29:branchName>{fn:data($items/item[@code='STORE_DESCRIPTION']/@value)}</ns29:branchName>
                    else ()
                    }
                    <ns29:terminal>{fn:data($CollectionPostTransaction_IE_REQ/transaction/WorkstationID)}</ns29:terminal>
                </ns2:ThirdPartyPayeeAgency>
            </ns2:CustomerPayment>
        </ns2:Body>
    </ns2:Publish_CollectionPostTransaction_REQ>
};

local:get_ColecctionPostTransaction_REQ($CollectionPostTransaction_IE_REQ, $RequestHeader)