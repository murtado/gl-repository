xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://v2.ws.poslog.xcenter.dtv/";
(:: import schema at "../../XSD/PoslogObjReceiverApiService_schema1.xsd" ::)

declare variable $REQ as element() (:: schema-element(ns1:postTransaction) ::) external;

declare function local:REQValidator($REQ as element() (:: schema-element(ns1:postTransaction) ::)) as xs:string {

let $ValErrCode := 'BEA-382505'
let $ValErrDescription := 

  if ($REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder) then
    ''
  else 
    (
    if ($REQ/transaction/RetailTransaction/LineItem[1]/Sale[@ItemType="Stock"]) then
      ''
    else 
      (
        if ($REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType="Stock"]) then
          ''
        else 
          (
            'TransactionType not Found on : transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder, nor transaction/RetailTransaction/LineItem[1]/Sale[@ItemType="Stock"], nor transaction/RetailTransaction/LineItem[1]/Return[@ItemType="Stock"])'
          ) 
      )
    )
    
    return 
      
      if ($ValErrDescription = '') then '' else concat ($ValErrCode,'---',$ValErrDescription)
                    
};

local:REQValidator($REQ)
