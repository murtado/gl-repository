SPOOL salida_CREATE_ALL_PACKAGES.txt
@CREATE_ESB_CONVERSATION_MANAGER_PKG_SPEC.sql
@CREATE_ESB_CONVERSATION_MANAGER_PKG_BODY.sql
@CREATE_ESB_CORRELATIONMANAGER_PKG_SPEC.sql
@CREATE_ESB_CORRELATIONMANAGER_PKG_BODY.sql
@CREATE_ESB_ERROR_HOSPITAL_PKG_SPEC.sql
@CREATE_ESB_ERROR_HOSPITAL_PKG_BODY.sql
@CREATE_ESB_ERROR_MANAGER_PKG_SPEC.sql
@CREATE_ESB_ERROR_MANAGER_PKG_BODY.sql
@CREATE_ESB_LOGGERMANAGER_PKG_SPEC.sql
@CREATE_ESB_LOGGERMANAGER_PKG_BODY.sql
@CREATE_ESB_MESSAGEMANAGER_PKG_SPEC.sql
@CREATE_ESB_MESSAGEMANAGER_PKG_BODY.sql
@CREATE_ESB_PARAMETERMANAGER_PKG_SPEC.sql
@CREATE_ESB_PARAMETERMANAGER_PKG_BODY.sql

--------------------------------------------------------
--  RETRYMANAGER
--------------------------------------------------------

@CREATE_ESB_RETRYMANAGER_PKG_SPEC.sql
@CREATE_ESB_RETRYMANAGER_PKG_BODY.sql


SPOOL OFF;
