xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/Consumer/upd/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/upd_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/upd/v1";
(:: import schema at "../../../../../ESC/Primary/upd_Consumer_v1_EBM.xsd" ::)


import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;


declare variable $faultString as xs:string external;

declare variable $faultCode as xs:string external;

declare variable $UpdConsumerFRSP as element() (:: schema-element(ns1:Upd_Consumer_FRSP) ::) external;

declare function local:func($UpdConsumerFRSP as element() (:: schema-element(ns1:Upd_Consumer_FRSP) ::), $faultCode as xs:string ,$faultString as xs:string ) as element() (:: schema-element(ns2:Fault) ::) {
    <ns2:Fault>
        <ns2:faultcode>{fn:data($faultCode)}</ns2:faultcode>
        <ns2:faultstring>{fn:data($faultString)}</ns2:faultstring>
        <ns2:detail>
            <ns2:Upd_Consumer_FRSP>
               {nssif_header:get_ResponseHeader($UpdConsumerFRSP/*:ResponseHeader)}
                <ns2:Body></ns2:Body>
            </ns2:Upd_Consumer_FRSP>
        </ns2:detail>
    </ns2:Fault>
};

local:func($UpdConsumerFRSP,$faultCode,$faultString)
