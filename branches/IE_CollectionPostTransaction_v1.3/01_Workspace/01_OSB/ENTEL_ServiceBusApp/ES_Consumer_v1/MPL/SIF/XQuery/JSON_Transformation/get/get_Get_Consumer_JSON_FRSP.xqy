xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;

declare namespace ns2="http://www.entel.cl/EBM/Consumer/get/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/get_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/get/v1";
(:: import schema at "../../../../../ESC/Primary/get_Consumer_v1_EBM.xsd" ::)

declare variable $Get_Consumer_FRSP as element() (:: schema-element(ns1:Get_Consumer_FRSP) ::) external;
declare variable $faultcode as xs:string external;
declare variable $faultstring as xs:string external;


declare function local:func($Get_Consumer_FRSP as element() (:: schema-element(ns1:Get_Consumer_FRSP) ::),
                            $faultcode as xs:string ,
                            $faultstring as xs:string 
                            ) as element() (:: schema-element(ns2:Fault) ::) {
  <ns2:Fault>
       <ns2:faultcode>{fn:data($faultcode)}</ns2:faultcode>
       <ns2:faultstring>{fn:data($faultstring)}</ns2:faultstring>
       <ns2:detail>
           <ns2:Get_Consumer_FRSP>
                     {nssif_header:get_ResponseHeader($Get_Consumer_FRSP/*:ResponseHeader)}
               <ns2:Body></ns2:Body>
           </ns2:Get_Consumer_FRSP>
       </ns2:detail>
 </ns2:Fault>
};

local:func($Get_Consumer_FRSP, $faultcode, $faultstring )
