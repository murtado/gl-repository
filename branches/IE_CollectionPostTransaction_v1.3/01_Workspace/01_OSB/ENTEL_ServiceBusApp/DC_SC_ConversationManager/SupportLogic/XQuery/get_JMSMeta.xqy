xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace tp="http://www.bea.com/wli/sb/transports";
declare namespace jms="http://www.bea.com/wli/sb/transports/jms";

declare variable $Inbound as element() external;

declare function local:get_JMSMeta($Inbound as element()) as element() {
    <JMSMeta
      jmsCorrelationID="{data($Inbound/ctx:transport/ctx:request/tp:headers/jms:JMSCorrelationID)}"
      jmsMessageType="{data($Inbound/ctx:transport/ctx:request/tp:headers/jms:JMSType)}"
    />
};

local:get_JMSMeta($Inbound)
