xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://v2.ws.poslog.xcenter.dtv/";
(:: import schema at "../XSD/PoslogObjReceiverApiService_schema1.xsd" ::)


declare function local:func() as element() (:: schema-element(ns1:postTransactionResponse) ::) {
    <ns1:postTransactionResponse/>
};

local:func()