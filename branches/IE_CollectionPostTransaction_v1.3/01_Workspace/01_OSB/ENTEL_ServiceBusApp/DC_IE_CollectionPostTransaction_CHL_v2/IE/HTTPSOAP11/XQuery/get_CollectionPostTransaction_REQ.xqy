xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://v2.ws.poslog.xcenter.dtv/";
(:: import schema at "../XSD/PoslogObjReceiverApiService_schema1.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/CollectionPostTransaction/Publish/v1";
(:: import schema at "../../../../ES_CollectionPostTransaction_v1/ESC/Primary/Publish_CollectionPostTransaction_v1_EBM.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/EBO/CustomerPayment/v1";

declare namespace ns6 = "http://www.entel.cl/EBO/BillingAccount/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/EmailContact/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/TelephoneNumber/v1";

declare namespace ns13 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns9 = "http://www.entel.cl/EBO/PostalDeliveryAddress/v1";

declare namespace ns12 = "http://www.entel.cl/EBO/StreetName/v1";

declare namespace ns11 = "http://www.entel.cl/EBO/Address/v1";

declare namespace ns10 = "http://www.entel.cl/EBO/GeographicArea/v1";

declare namespace ns17 = "http://www.entel.cl/EBO/PhysicalResource/v1";

declare namespace ns16 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1";

declare namespace ns15 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns14 = "http://www.entel.cl/EBO/IndividualName/v1";

declare namespace ns19 = "http://www.entel.cl/EBO/PhysicalResourceSpec/v1";

declare namespace ns18 = "http://www.entel.cl/EBO/MSISDN/v1";

declare namespace ns20 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1";

declare namespace ns24 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns23 = "http://www.entel.cl/EBO/LoyaltyTransaction/v1";

declare namespace ns22 = "http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1";

declare namespace ns21 = "http://www.entel.cl/EBO/OrganizationName/v1";

declare namespace ns28 = "http://www.entel.cl/EBO/StockTransaction/v1";

declare namespace ns27 = "http://www.entel.cl/EBO/Store/v1";

declare namespace ns26 = "http://www.entel.cl/EBO/PaymentMethod/v1";

declare namespace ns25 = "http://www.entel.cl/EBO/CustomerOrder/v1";

declare namespace ns29 = "http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1";

declare namespace ns30 = "http://v2.ws.poslog-ext.xcenter.dtv/";

declare variable $CollectionPostTransaction_IE_REQ as element() (:: schema-element(ns1:postTransaction) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::) external;



declare function local:getProperties($input as element ()) as element(){

  <items>
  {
  for $prop in $input/*:PosTransactionProperties
  return 
    <item code="{$prop/*:PosTransactionPropertyCode}" value="{$prop/*:PosTransactionPropertyValue}" />
  }
  </items>
  };
  

declare function local:get_ColecctionPostTransaction_REQ($CollectionPostTransaction_IE_REQ as element() (:: schema-element(ns1:postTransaction) ::), $RequestHeader as element() (:: schema-element(ns3:RequestHeader) ::)) as element() (:: schema-element(ns2:Publish_CollectionPostTransaction_REQ) ::) {
    let $items := local:getProperties($CollectionPostTransaction_IE_REQ/transaction)return
    let $totalTaxSale := fn:sum($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[@VoidFlag = fn:false()]/Sale/Tax/Amount)return
    let $totalTaxReturn := fn:sum($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[@VoidFlag = fn:false()]/Return/Tax/Amount)return
    let $totalTaxPreviousCustomerOrder := fn:sum($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[@VoidFlag = fn:false()]/PreviousCustomerOrder/Tax/Amount) return
    <ns2:Publish_CollectionPostTransaction_REQ>
        {$RequestHeader}
        <ns2:Body>
            <ns2:CustomerPayment>
                <ns4:ID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/SequenceNumber)}</ns4:ID>
                {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/TransactionLink[@ReasonCode='Return']/SequenceNumber)
                    then 
                    <ns4:cancelTransactionID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/TransactionLink[@ReasonCode='Return']/SequenceNumber[1])}</ns4:cancelTransactionID>
                    else ()
                }
                {
                if ($items/item[@code='CLAVE_RECONCILIACION']/@value) then
                  <ns4:operatorID>{fn:data($items/item[@code='CLAVE_RECONCILIACION']/@value)}</ns4:operatorID>
                else 
                (
                  <ns4:operatorID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/OperatorID)}</ns4:operatorID>
                )
                }
                {
                  if ($items/item[@code='FECHA_TRANSACCION']/@value) then
                  <ns4:paymentDate>{fn:data($items/item[@code='FECHA_TRANSACCION']/@value)}</ns4:paymentDate>
                else 
                (
                  <ns4:paymentDate>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time()),())}</ns4:paymentDate>
                )
                }
				
                <ns4:paymentDateLogical>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time()),())}</ns4:paymentDateLogical>
               {
					if ($CollectionPostTransaction_IE_REQ/transaction/SequenceNumber) then
                      <ns4:sequenceNumber>{fn:data($CollectionPostTransaction_IE_REQ/transaction/SequenceNumber)}</ns4:sequenceNumber>
                    else ()
			   }
                      { 
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Cancelled']) then
                         <ns4:transactionCode>1</ns4:transactionCode>
                   else(
                      <ns4:transactionCode>0</ns4:transactionCode>
                      )
                  }
			   {
					if (data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder/@Action) !='') then
                    <ns4:transactionDetail>{data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder/@Action)}</ns4:transactionDetail>
                    else ()
			   }
               
                {
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Canceled']) then
                    <ns4:transactionSubType>Canceled</ns4:transactionSubType>
                  else 
                (
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='Completed']) then 
                    <ns4:transactionSubType>Completed</ns4:transactionSubType>
                  else 
                (
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder[@Action='dtv:Paywithoutpickup']) then 
                    <ns4:transactionSubType>Paywithoutpickup</ns4:transactionSubType>
                  else ()
                )
                )
                }
                {
                if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/PreviousCustomerOrder) then
                  <ns4:transactionType>PreviousCustomerOrder</ns4:transactionType>
                else 
                (
                if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Sale[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']) then
                  <ns4:transactionType>Sale</ns4:transactionType>
                else 
                (
                  if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']) then
                  <ns4:transactionType>Return</ns4:transactionType>
                  else () 
                )
                )
                }
              
                
                <ns2:CustomerAccount>
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/AccountNumber) then
                    <ns5:ID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/AccountNumber)}</ns5:ID>
                    else ()
                    }
                    {
                    if ($items/item[@code='CUSTOMER_BSCS_ID']/@value) then
                      <ns5:billingID>{fn:data($items/item[@code='CUSTOMER_BSCS_ID']/@value)}</ns5:billingID>
                    else ()
                    }
                    {
                    if ($items/item[@code='CONTRACT_ACCOUNT']/@value) then
                      <ns5:externalID>{fn:data($items/item[@code='CONTRACT_ACCOUNT']/@value)}</ns5:externalID>
                    else ()
                    }
                    <ns2:BillingAccount>
                    {
                    if ($items/item[@code='CUSTOMER_BSCS_ID']/@value) then
                        <ns6:externalID>{fn:data($items/item[@code='CUSTOMER_BSCS_ID']/@value)}</ns6:externalID>
                    else ()
                    }
                    </ns2:BillingAccount>
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/TelephoneNumber) then								
                      <ns2:Contact>
                        <ns2:cellPhone>
                          <ns7:number>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/TelephoneNumber)}</ns7:number>
                        </ns2:cellPhone>
                      </ns2:Contact>
                    else ()
                    }
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/eMail) then
                      <ns2:Customer>
                          <ns2:EmailContact>
                              <ns8:eMailAddress>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/eMail)}</ns8:eMailAddress>
                          </ns2:EmailContact>
                      </ns2:Customer>
                    else ()
                    }
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine) then
                    <ns2:CustomerAddress>
                        <ns2:postalDeliveryAddress>
                            <ns9:address>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine)}</ns9:address>
                        </ns2:postalDeliveryAddress>
                    </ns2:CustomerAddress>
                    else ()
                    }
                    <ns2:GeographicArea>
                    
                       {
                        if (fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/City))
                            then  <ns10:city>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/City)}</ns10:city>
                            else <ns10:city>{fn:data($items/item[@code='STORE_CITY']/@value)}</ns10:city>
                        }
                        {
                       if (fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/Neighborhood))
                            then <ns10:commune>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/Neighborhood)}</ns10:commune>
                            else(  if (fn:data($items/item[@code='STORE_NEIGHBORHOOD']/@value))
                               then     <ns10:commune>{fn:data($items/item[@code='STORE_NEIGHBORHOOD']/@value)}</ns10:commune>
                               else <ns10:commune>{fn:data($items/item[@code='STORE_CITY']/@value)}</ns10:commune>)
                        }
                        <ns10:neighborhood>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/Neighborhood)}</ns10:neighborhood>
                    </ns2:GeographicArea>
                    <ns2:GeographicalAddress>
                        <ns2:Address>
                            {
                            if ($items/item[@code='STORE_ADDR']/@value) then
                              <ns11:addressName>{fn:data($items/item[@code='STORE_ADDR']/@value)}</ns11:addressName>
                            else ()
                            }
                        </ns2:Address>
                    </ns2:GeographicalAddress>
                    <ns2:IndividualIdentification>
                        {
                        if ($items/item[@code='CUSTOMER_RUT']/@value) then
                          <ns13:number>{fn:data($items/item[@code='CUSTOMER_RUT']/@value)}</ns13:number>
                        else ()
                        }
                        {
                        if ($items/item[@code='CUSTOMER_DOC_TYPE']/@value) then
                          <ns13:type>{fn:data($items/item[@code='CUSTOMER_DOC_TYPE']/@value)}</ns13:type>
                        else ()
                        }
                    </ns2:IndividualIdentification>
                    {
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Name) then
                        <ns2:IndividualName>
                          <ns14:firstName>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Name)}</ns14:firstName>
                          <ns14:lastName>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Name)}</ns14:lastName>
                        </ns2:IndividualName>
                      else ()
                    }
                </ns2:CustomerAccount>
                <ns2:CustomerBill>
                    <ns15:adjustmentDate>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time()),())}</ns15:adjustmentDate>
                    {
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']) then
                      <ns15:adjustmentDescription></ns15:adjustmentDescription>
                      else()
                    }
                    <ns15:appliedDate>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate),fn:current-time()),())}</ns15:appliedDate>
                    <ns15:billDate>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time()),())}</ns15:billDate>
                    {
                    if ($items/item[@code = 'FOLIO']/@value) then 
                      <ns15:billNo>{fn:data($items/item[@code='FOLIO']/@value)}</ns15:billNo>
                    else ()
                    }
                   {
                    if ($items/item[@code='FOLIO_ORIGINAL']/@value) then
                    <ns15:billNoInv>{fn:data($items/item[@code='FOLIO_ORIGINAL']/@value)}</ns15:billNoInv>
                    else ()
                    }
                    {
                    if ($items/item[@code='FOLIO_ORIGINAL']/@value) then
                      <ns15:billNoOriginal>{fn:data($items/item[@code='FOLIO_ORIGINAL']/@value)}</ns15:billNoOriginal>
                    else ()
                    }
                    {
                    if ($items/item[@code='DIGSIG_DATE']/@value) then
                      <ns15:dateDigitalSignature>{fn:data($items/item[@code='DIGSIG_DATE']/@value)}</ns15:dateDigitalSignature>
                    else ()
                    }
                    {
                    if ($items/item[@code='TIMBRE_BARCODE']/@value) then
                       <ns15:digitalSignature>{fn-bea:serialize(fn:data($items/item[@code='TIMBRE_BARCODE']/@value))}</ns15:digitalSignature>
                    else ()
                    }
                    <ns15:discountAmount>{fn:sum($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/SaleForDelivery/RetailPriceModifier[1]/Amount[@Action='Subtract'])}</ns15:discountAmount>
                    
                    {
                    if ($items/item[@code='TRANSACTION_DOC_TYPE']/@value) then
                      <ns15:documentType>{fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)}</ns15:documentType>
                    else ()
                    }
                    {
                    if ($items/item[@code='DOC_TYPE_ORIGINAL']/@value) then
                      <ns15:documentTypeInv>{fn:data($items/item[@code='DOC_TYPE_ORIGINAL']/@value)}</ns15:documentTypeInv>
                    else ()
                    }
                    {
                    if ($items/item[@code='DOC_TYPE_ORIGINAL']/@value) then
                      <ns15:documentTypeRef>{fn:data($items/item[@code='DOC_TYPE_ORIGINAL']/@value)}</ns15:documentTypeRef>
                    else ()
                    }
                    {
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']) then
                      <ns15:operationType>I: Crea el ajuste</ns15:operationType>
                      else ()
                    }
                    {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType="TransactionGrandAmount"])
                        then <ns15:paymentAmount>{fn:abs(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType="TransactionGrandAmount"]))}</ns15:paymentAmount>
                        else ()
                    }
                    <ns15:paymentDueDate>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time()),())}</ns15:paymentDueDate>
                    
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise'] and xs:date(substring-before($items/item[@code='ORIGINAL_DATE']/@value,'T')) < $CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate) then
                      <ns15:returnScenario>DDP</ns15:returnScenario>
                    else 
                    (
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise'] and xs:date(substring-before($items/item[@code='ORIGINAL_DATE']/@value,'T')) = $CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate) then
                        <ns15:returnScenario>DMD</ns15:returnScenario>
                      else 
                      (
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender[@TenderType='Voucher'] and $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Tender/TenderID = '<!CDATA[ISSUE_STORE_CREDIT]]>') then
                          <ns15:returnScenario>DCT</ns15:returnScenario>
                        else 
                      (
                        if ($items/item[@code='<!CDATA[ENTEL_STORE_IND]]> = "N"']) then
                          <ns15:returnScenario>FP</ns15:returnScenario>
                        else 
                        (
                          <ns15:returnScenario>VTA</ns15:returnScenario>
                        )
                      )
                      )
                    )
                    }
                    {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount'])
                        then <ns15:totalAmount>{fn:abs(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount']))}</ns15:totalAmount>
                        else ()
                    }
                    {
                      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/@VoidFlag = fn:false()) then
      if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[@VoidFlag = fn:false()]/PreviousCustomerOrder/Tax[1]/Amount)
                        then <ns15:totalTaxValue>{fn:abs($totalTaxPreviousCustomerOrder)}</ns15:totalTaxValue>
                        else 
                        (
                            if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[@VoidFlag = fn:false()]/Sale[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/Tax[1]/Amount)
                            then <ns15:totalTaxValue>{fn:abs($totalTaxSale)}</ns15:totalTaxValue>
                            else 
                            (
                              if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[@VoidFlag = fn:false()]/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/Tax[1]/Amount)
                                then <ns15:totalTaxValue>{fn:abs($totalTaxReturn)}</ns15:totalTaxValue>
                              else ()
                            )
                        )
                        else()
                    }
                    
                    {
                    if ($items/item[@code='TIMBRE_DOC_URL']/@value) then
                      <ns15:urlAttachedDocument>{fn:data($items/item[@code='TIMBRE_DOC_URL']/@value)}</ns15:urlAttachedDocument>
                    else ()
                    }
                    <ns15:vendorId>{fn:data($CollectionPostTransaction_IE_REQ/transaction/OperatorID)}</ns15:vendorId>
                    {
                        for $LineItem in $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem
                        return
                        if($LineItem/@VoidFlag = fn:false())then 
                          if ($LineItem/PreviousCustomerOrder or $LineItem/Sale[@ItemType='Stock'] or $LineItem/Sale[@ItemType='dtv:NonMerchandise'] 
                          or $LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise'] or $LineItem/Return[@ItemType='dtv:NonMerchandise']) then
                          <ns2:AppliedCustomerBillingProductCharge>
                            {
                            if ($items/item[@code='COST_CENTER']/@value) then
                            <ns16:costCenter>{fn:data($items/item[@code='COST_CENTER']/@value)}</ns16:costCenter>
                            else ()
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/Description) then
                            <ns16:description>{fn:data($LineItem/PreviousCustomerOrder/Description)}</ns16:description>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Description) then
                                <ns16:description>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Description)}</ns16:description>
                              else 
                              (
                              if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Description) then
                                <ns16:description>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Description)}</ns16:description>
                              else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/RetailPriceModifier/Amount[@Action='Subtract']) then
                            <ns16:discountAmount>{fn:sum($LineItem/PreviousCustomerOrder/RetailPriceModifier/Amount[@Action='Subtract'])}</ns16:discountAmount>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/RetailPriceModifier/Amount[@Action='Subtract']) then
                                <ns16:discountAmount>{fn:sum($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/RetailPriceModifier/Amount[@Action='Subtract'])}</ns16:discountAmount>
                              else ()
                            )
                            }
                            <ns16:itemNumber>{fn:data($LineItem/SequenceNumber)}</ns16:itemNumber>
                            {
                            if ($LineItem/PreviousCustomerOrder/ActualSalesUnitPrice) then
                               if ($LineItem/PreviousCustomerOrder/Tax/Amount)then
                                 <ns16:itemPrice>{fn:data($LineItem/PreviousCustomerOrder/ActualSalesUnitPrice)- fn:abs(fn:sum($LineItem/PreviousCustomerOrder/Tax/Amount))}</ns16:itemPrice>
                                 else 
                                 ( <ns16:itemPrice>{fn:data($LineItem/PreviousCustomerOrder/ActualSalesUnitPrice)}</ns16:itemPrice>)
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ActualSalesUnitPrice) then
                                if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax/Amount)then
                                  <ns16:itemPrice>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ActualSalesUnitPrice)-fn:abs(fn:sum($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax/Amount))}</ns16:itemPrice>
                                 else 
                                 ( <ns16:itemPrice>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ActualSalesUnitPrice)}</ns16:itemPrice>)
                              else 
                              (
                              if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ActualSalesUnitPrice) then
                                 if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax/Amount)then
                                    <ns16:itemPrice>{fn:abs(fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ActualSalesUnitPrice))-fn:abs(fn:sum($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax/Amount))}</ns16:itemPrice>
                                  else 
                                 ( <ns16:itemPrice>{fn:abs(fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ActualSalesUnitPrice))}</ns16:itemPrice>)
                              else ()
                              )
                            )
                            }
                            {
                              if ($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ITEM_TYPE']/ns30:LineItemPropertyValue)then
                              <ns16:itemType>{fn:data($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ITEM_TYPE']/ns30:LineItemPropertyValue)}</ns16:itemType>
                              else(
                                    if ($LineItem/Sale/ns30:LineItemProperty[ns30:LineItemPropertyCode='ITEM_TYPE']/ns30:LineItemPropertyValue)then
                                      <ns16:itemType>{fn:data($LineItem/Sale/ns30:LineItemProperty[ns30:LineItemPropertyCode='ITEM_TYPE']/ns30:LineItemPropertyValue)}</ns16:itemType>
                                    else(
                                        if ($LineItem/Return/ns30:LineItemProperty[ns30:LineItemPropertyCode='ITEM_TYPE']/ns30:LineItemPropertyValue)then
                                      <ns16:itemType>{fn:data($LineItem/Return/ns30:LineItemProperty[ns30:LineItemPropertyCode='ITEM_TYPE']/ns30:LineItemPropertyValue)}</ns16:itemType>
                                      else()
                                        )
                                  )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/ExtendedAmount) then
                                if ($LineItem/PreviousCustomerOrder/Tax/Amount)then
                                   <ns16:netAmount>{(fn:data($LineItem/PreviousCustomerOrder/ExtendedAmount)-fn:abs(fn:sum($LineItem/PreviousCustomerOrder/Tax/Amount)))* fn:data($LineItem/PreviousCustomerOrder/Quantity)}</ns16:netAmount>
                                else 
                                 ( <ns16:netAmount>{fn:data($LineItem/PreviousCustomerOrder/ExtendedAmount)}</ns16:netAmount>)
                            else 
                            (
                            if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ExtendedAmount) then
                               if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax/Amount)then
                                  <ns16:netAmount>{(fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ExtendedAmount)-fn:abs(fn:sum($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax/Amount)))* fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Quantity)}</ns16:netAmount>
                               else 
                                  (<ns16:netAmount>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ExtendedAmount)}</ns16:netAmount>)
                            else 
                            (
                            if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ExtendedAmount) then
                              if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax/Amount)then
                                  <ns16:netAmount>{(fn:abs(fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ExtendedAmount))-fn:abs(fn:sum($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax/Amount)))*fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Quantity)}</ns16:netAmount>
                              else
                                (<ns16:netAmount>{fn:abs(fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ExtendedAmount))}</ns16:netAmount>)
                            else ()
                            )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/ItemID) then
                              <ns16:productId>{fn:data($LineItem/PreviousCustomerOrder/ItemID)}</ns16:productId>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ItemID) then
                                <ns16:productId>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ItemID)}</ns16:productId>
                              else
                              (
                              if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ItemID) then
                                <ns16:productId>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ItemID)}</ns16:productId>
                              else()
                              )
                            )
                            }
                            {
                              if ($LineItem/PreviousCustomerOrder/Quantity) then
                                <ns16:quantityItem>{fn:data($LineItem/PreviousCustomerOrder/Quantity)}</ns16:quantityItem>
                              else 
                              (
                                if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Quantity) then
                                  <ns16:quantityItem>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Quantity)}</ns16:quantityItem>
                                else 
                                (
                                  if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Quantity) then
                                    <ns16:quantityItem>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Quantity)}</ns16:quantityItem>
                                  else () 
                                )
                              )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/SerialNumber) then
                              <ns16:serialNumber>{fn:data($LineItem/PreviousCustomerOrder/SerialNumber)}</ns16:serialNumber>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/SerialNumber) then
                                <ns16:serialNumber>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/SerialNumber)}</ns16:serialNumber>
                              else 
                              (
                                if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/SerialNumber) then
                                  <ns16:serialNumber>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/SerialNumber)}</ns16:serialNumber>
                                else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId) then
                            <ns16:taxCode>{fn:data($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId)}</ns16:taxCode>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxCode>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId)}</ns16:taxCode>
                              else 
                              (
                              if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxCode>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId)}</ns16:taxCode>
                              else ()
                              )
                            )
                            }
                            {
                            if ($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId) then
                            <ns16:taxIndicator>{fn:data($LineItem/PreviousCustomerOrder/Tax[1]/ns30:TaxGroupId)}</ns16:taxIndicator>
                            else 
                            (
                              if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxIndicator>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId)}</ns16:taxIndicator>
                              else 
                              (
                              if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId) then
                                <ns16:taxIndicator>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax[1]/ns30:TaxGroupId)}</ns16:taxIndicator>
                              else ()
                              )
                            )
                            }
                            {
                          if ($LineItem/PreviousCustomerOrder/ExtendedAmount) then
                            if ($LineItem/PreviousCustomerOrder/Tax/Amount)then
                               <ns16:totalItem>{(fn:data($LineItem/PreviousCustomerOrder/ExtendedAmount)-fn:abs(fn:sum($LineItem/PreviousCustomerOrder/Tax/Amount)))*fn:data($LineItem/PreviousCustomerOrder/Quantity)}</ns16:totalItem>
                            else 
                             ( <ns16:totalItem>{fn:data($LineItem/PreviousCustomerOrder/ExtendedAmount)}</ns16:totalItem>)
                            else 
                            (
                            if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ExtendedAmount) then
                               if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax/Amount)then
                                      <ns16:totalItem>{(fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ExtendedAmount)- fn:abs(fn:sum($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Tax/Amount)))* fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Quantity)}</ns16:totalItem>
                               else 
                                      (<ns16:totalItem>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ExtendedAmount)}</ns16:totalItem>)
                            else 
                            (
                            if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ExtendedAmount) then
                              if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax/Amount)then
                                      <ns16:totalItem>{(fn:abs(fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ExtendedAmount))-fn:abs(fn:sum($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Tax/Amount)))*fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/Quantity)}</ns16:totalItem>
                              else
                                    (<ns16:totalItem>{fn:abs(fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ExtendedAmount))}</ns16:totalItem>)
                            else ()
                            )
                            )
                            }
                            <ns2:PhysicalResource>
                              {
                              if ($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                  <ns17:imei>{fn:data($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:imei>
                               else 
                               (
                                 if ($LineItem/PreviousCustomerOrder/SerialNumber) then
                                  <ns17:imei>{fn:data($LineItem/PreviousCustomerOrder/SerialNumber)}</ns17:imei>
                                else 
                                  (
                                      if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/SerialNumber) then
                                        <ns17:imei>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/SerialNumber)}</ns17:imei>
                                      else 
                                      (
                                          if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                            <ns17:imei>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:imei>
                                          else ()
                                      )
                                   )
                               )
                              }
                              {
                                if ($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                  <ns17:serialNumber>{fn:data($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:serialNumber>
                                else 
                                (
                                  if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                    <ns17:serialNumber>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:serialNumber>
                                  else 
                                  (
                                    if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue) then
                                      <ns17:serialNumber>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='ICCID']/ns30:LineItemPropertyValue)}</ns17:serialNumber>
                                    else ()
                                  )
                                )
                              }
                               { 
                                    if(($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)or 
                                        ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue) or 
                                        ($LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)) then
                               <ns2:Asset>
                                    <ns2:MSISDN>
                                        {
                                        if ($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue) then
                                        <ns18:SN>{fn:data($LineItem/PreviousCustomerOrder/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)}</ns18:SN>
                                        else 
                                        (
                                          if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue) then
                                            <ns18:SN>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)}</ns18:SN>
                                          else 
                                          (
                                          if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue) then
                                            <ns18:SN>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ns30:LineItemProperty[ns30:LineItemPropertyCode='MSISDN']/ns30:LineItemPropertyValue)}</ns18:SN>
                                          else ()
                                          )
                                        )
                                        }
                                    </ns2:MSISDN>
                                </ns2:Asset>
                                else()
                                }
                                <ns2:PhysicalResourceSpec>
                                    {
                                    if ($LineItem/PreviousCustomerOrder/Description) then
                                    <ns19:description>{fn:data($LineItem/PreviousCustomerOrder/Description)}</ns19:description>
                                    else 
                                    (
                                      if ($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Description) then
                                        <ns19:description>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/Description)}</ns19:description>
                                      else 
                                      (
                                      if ($LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/Description) then
                                        <ns19:description>{fn:data($LineItem/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/Description)}</ns19:description>
                                      else ()
                                      )
                                    )
                                    }
                                    {
                                    if ($LineItem/PreviousCustomerOrder/ItemID) then
                                      <ns19:skuNumber>{fn:data($LineItem/PreviousCustomerOrder/ItemID)}</ns19:skuNumber>
                                    else 
                                    (
                                      if ($LineItem/Sale[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/ItemID) then
                                        <ns19:skuNumber>{fn:data($LineItem/Sale[@ItemType='Stock'or @ItemType='dtv:NonMerchandise']/ItemID)}</ns19:skuNumber>
                                      else 
                                      (
                                      if ($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ItemID) then
                                        <ns19:skuNumber>{fn:data($LineItem/Return[@ItemType='Stock' or  @ItemType='dtv:NonMerchandise']/ItemID)}</ns19:skuNumber>
                                      else ()
                                      )
                                    )
                                    }
                                </ns2:PhysicalResourceSpec>
                            </ns2:PhysicalResource>
                        </ns2:AppliedCustomerBillingProductCharge>
                        else ()        
                    else()
                    }
                    <ns2:AppliedCustomerBillingRate>
                         <ns2:AppliedCustomerBillingProductAlteration>
                            {
                            if (fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)='NOTACREDITO') then
                              <ns20:amount>{fn:abs(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount']))}</ns20:amount>
                            else
                              ()
                            }
                             {
                            if (fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)='NOTACREDITO') then
                              <ns20:compensatedAmount>{fn:abs(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount']))}</ns20:compensatedAmount>
                            else
                              ()
                            }
                            {
                            if (($items/item[@code='FOLIO_ORIGINAL']/@value) and (fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)='NOTACREDITO')) then
                              <ns20:compensatedDocumentNumber>{fn:data($items/item[@code='FOLIO_ORIGINAL']/@value)}</ns20:compensatedDocumentNumber>
                            else
                                <ns20:compensatedDocumentNumber/>
                            }
                            {
                            if (fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)='NOTACREDITO') then
                              <ns20:compensatedDocumentStatus>1</ns20:compensatedDocumentStatus>
                            else
                               ()
                            }
                            
                            {
                            if (($items/item[@code='DOC_TYPE_ORIGINAL']/@value) and (fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)='NOTACREDITO')) then
                              <ns20:compensatedDocumentType>{fn:data($items/item[@code='DOC_TYPE_ORIGINAL']/@value)}</ns20:compensatedDocumentType>
                            else
                               <ns20:compensatedDocumentType/>
                            }
                            
                            {
                            if (($items/item[@code='TRANSACTION_DOC_TYPE']/@value) and (fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)='NOTACREDITO')) then
                              <ns20:documentType>{fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)}</ns20:documentType>
                            else 
                              <ns20:documentType/>
                            }
                            {
                            if (($items/item[@code='FOLIO']/@value) and (fn:data($items/item[@code='TRANSACTION_DOC_TYPE']/@value)='NOTACREDITO')) then
                              <ns20:referenceNumberForCompensation>{fn:data($items/item[@code='FOLIO']/@value)}</ns20:referenceNumberForCompensation>
                            else 
                              <ns20:referenceNumberForCompensation/>
                            }
                        </ns2:AppliedCustomerBillingProductAlteration>
                    </ns2:AppliedCustomerBillingRate>
                    <ns2:IssuingCompany>
                        <ns2:individualIdentification>
                            {
                            if ($items/item[@code='STORE_RUT']/@value) then 
                              <ns13:number>{fn:data($items/item[@code='STORE_RUT']/@value)}</ns13:number>
                            else ()
                            }
                        </ns2:individualIdentification>
                        <ns2:organizationName>
                          {
                            if ($items/item[@code='SOCIETY']/@value) then
                              <ns21:shortName>{fn:data($items/item[@code='SOCIETY']/@value)}</ns21:shortName>
                            else ()
                          }
                          {
                            if ($items/item[@code='SOCIETY']/@value) then
                              <ns21:tradingName>{fn:data($items/item[@code='SOCIETY']/@value)}</ns21:tradingName>
                            else ()
                          }
                        </ns2:organizationName>
                        <ns2:thirdPartyCollectionPM>
                            <ns22:thirdPartyID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailStoreID)}</ns22:thirdPartyID>
                        </ns2:thirdPartyCollectionPM>
                    </ns2:IssuingCompany>
                    <ns2:LoyaltyTransaction>
                    {
                    if ($items/item[@code='LOYALTY_TRN_AMOUNT']/@value) then
                      <ns23:amount>{fn:data($items/item[@code='LOYALTY_TRN_AMOUNT']/@value)}</ns23:amount>
                    else ()
                    }
                    </ns2:LoyaltyTransaction>
                    <ns2:ReceiverCompany>
                        {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/eMail) then
                        <ns2:emailContact>
                            <ns8:eMailAddress>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/eMail)}</ns8:eMailAddress>
                        </ns2:emailContact>
                        else ()
                        }
                        <ns2:geographicArea>
                            <ns10:city>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/City)}</ns10:city>
                            
                            <ns10:neighborhood>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/Neighborhood)}</ns10:neighborhood>
                        </ns2:geographicArea>                       
                        {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine) then
                          <ns2:streetName>
                            <ns12:name>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine)}</ns12:name>
                          </ns2:streetName>
                        else ()
                        }
                    </ns2:ReceiverCompany>
                    <ns2:appliedAmount>
                        {
                            if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount'])
                            then <ns24:amount>{fn:abs(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Total[@TotalType='TransactionGrandAmount']))}</ns24:amount>
                            else ()
                        }
                        
                    </ns2:appliedAmount>
                </ns2:CustomerBill>
                <ns2:CustomerOrder>
                    {
                    if ($items/item[@code='EOC_ORDER_ID']/@value) then
                      <ns25:ID>{fn:data($items/item[@code='EOC_ORDER_ID']/@value)}</ns25:ID>
                    else ()
                    }
                    {
                    if(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/@TransactionStatus)!='')then
                      <ns25:state>{fn:upper-case(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/@TransactionStatus))}</ns25:state>
                   else()
                    }
                </ns2:CustomerOrder>
                <ns2:Money>
                  {
                     if ( $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Tender[TenderID="ACCOUNT_CREDIT"]) then
                         <ns24:amount>{fn:abs(fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Tender[TenderID="ACCOUNT_CREDIT"]/Amount))}</ns24:amount>
                      else()
                    }
                     <ns24:units>{fn:data($CollectionPostTransaction_IE_REQ/transaction/CurrencyCode)}</ns24:units>
                </ns2:Money>
            {
            for $LineItem in $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem
                  return
                
                if (($LineItem/Tender) and (fn:data($LineItem/Tender/TenderID)!="ACCOUNT_CREDIT") and (fn:data($LineItem/Tender/TenderID)!="POINTS")) then
            
                 <ns2:PaymentMethod>
                    
                    {
                    if ($LineItem/Tender/Check/AccountNumber) then
                      <ns26:bankAccountNumber>{fn:data($LineItem/Tender/Check/AccountNumber)}</ns26:bankAccountNumber>
                     else ()
                    }
                    {if(fn:data($LineItem/Tender/Check/BankID)!='')then
                    <ns26:bankCode>{$LineItem/Tender/Check/BankID}</ns26:bankCode>
                    else()
                    }
                    {
                    if ($items/item[@code='FOLIO']/@value and $CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem/Return/@ItemType = 'Stock') then 
                      <ns26:billNoNC>{fn:data($items/item[@code='FOLIO']/@value)}</ns26:billNoNC>
                    else ()
                    }
                    <!-- La validacion por FECHA_VENCIMIENTO queda para despues de Change de schema-->
                    {
                      if ($items/item[@code='FECHA_CONTABLE']/@value) then
                        <ns26:date>{fn:data($items/item[@code='FECHA_CONTABLE']/@value)}</ns26:date>
                      else if (fn:data($LineItem/BeginDateTime)) then
                          <ns26:date>{fn:data($LineItem/BeginDateTime)}</ns26:date>
                      else ()
                    }
                    {
                    
                    if ( (fn:data($items/item[@code='ENTEL_STORE_IND']/@value) = "N") and (fn:data($LineItem/Tender/TenderID)="CHECK_AL_DIA")) then
                       <ns26:paymentMethodType>CHECK_AL_DIA_FRANQUICIA</ns26:paymentMethodType>   
                    else 
                    if ( (fn:data($items/item[@code='ENTEL_STORE_IND']/@value) = "N") and (fn:data($LineItem/Tender/TenderID)="CLP_CURRENCY")) then
                       <ns26:paymentMethodType>CLP_CURRENCY_FRANQUICIA</ns26:paymentMethodType>   
                    else if ($LineItem/Tender/TenderID) then
			<ns26:paymentMethodType>{fn:data($LineItem/Tender/TenderID)}</ns26:paymentMethodType>
                    else ()
                    }
		    <ns2:Money>
                    {	 if (fn:data($LineItem/Tender/Amount/@Currency)) then
                                    <ns24:units>{fn:data($LineItem/Tender/Amount/@Currency)}</ns24:units>
                            else()
                            }
                    </ns2:Money>
		      <ns2:amount> 
                        {
                            if (fn:data($LineItem/Tender/Amount))
                            then
        <ns24:amount>{fn:abs(fn:data($LineItem/Tender/Amount))}</ns24:amount>
                            else ()
                        }
                    </ns2:amount>
                    
                </ns2:PaymentMethod>
               else ()
                }
          
                <ns2:Store>
                    <ns27:ID>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailStoreID)}</ns27:ID>
                    {
                    if ($items/item[@code='STORE_DESCRIPTION']/@value) then
                      <ns27:name>{fn:data($items/item[@code='STORE_DESCRIPTION']/@value)}</ns27:name>
                      else ()
                    }
                    {
                    if ($items/item[@code='ENTEL_STORE_IND']/@value) then
                      <ns27:storeInd>{fn:data($items/item[@code='ENTEL_STORE_IND']/@value)}</ns27:storeInd>
                    else ()
                    }
                    {
                    if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine) then
                      <ns2:GeographicAddress>
                        <ns2:Address>
                            <ns2:streetNameFull>
                                <ns12:name>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/Customer/Address/AddressLine)}</ns12:name>
                            </ns2:streetNameFull>
                        </ns2:Address>
                      </ns2:GeographicAddress>
                    else ()
                    }
                    <ns2:StockTransaction>
                        {
                        if ($items/item[@code='DELIVERY_IND']/@value) then
                          <ns28:deliveryInd>{fn:data($items/item[@code='DELIVERY_IND']/@value)}</ns28:deliveryInd>
                        else ()
                        }
                        {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/Reason) then
                         <ns28:reason>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']/Reason)}</ns28:reason>
                         else ()
                         }
                        <ns28:returnDate>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time()),())}</ns28:returnDate>
                        <ns28:transactionDate>{adjust-dateTime-to-timezone(fn:dateTime(fn:data($CollectionPostTransaction_IE_REQ/transaction/BusinessDayDate), fn:current-time()),())}</ns28:transactionDate>
                        {
                        if ($CollectionPostTransaction_IE_REQ/transaction/RetailTransaction/LineItem[1]/Return[@ItemType='Stock' or @ItemType='dtv:NonMerchandise']) then
                          <ns28:transactionType>DVT</ns28:transactionType>
                        else 
                        (
                          <ns28:transactionType>VTA</ns28:transactionType>
                        )
                        }
                    </ns2:StockTransaction>
                </ns2:Store>               
                <ns2:ThirdPartyPayeeAgency>
                    <ns29:branch>{fn:data($CollectionPostTransaction_IE_REQ/transaction/RetailStoreID)}</ns29:branch>
                    {
                    if ($items/item[@code='STORE_DESCRIPTION']/@value) then
                      <ns29:branchName>{fn:data($items/item[@code='STORE_DESCRIPTION']/@value)}</ns29:branchName>
                    else ()
                    }
                    <ns29:terminal>{fn:data($CollectionPostTransaction_IE_REQ/transaction/WorkstationID)}</ns29:terminal>
                </ns2:ThirdPartyPayeeAgency>
            </ns2:CustomerPayment>
        </ns2:Body>
    </ns2:Publish_CollectionPostTransaction_REQ>
};

local:get_ColecctionPostTransaction_REQ($CollectionPostTransaction_IE_REQ, $RequestHeader)