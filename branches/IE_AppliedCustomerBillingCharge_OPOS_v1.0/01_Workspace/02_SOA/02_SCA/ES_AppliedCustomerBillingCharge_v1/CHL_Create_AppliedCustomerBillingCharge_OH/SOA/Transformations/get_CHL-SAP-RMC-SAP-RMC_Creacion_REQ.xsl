<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:ns0="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tns="http://www.entel.cl/CSM/RA/CHL-SAP-RMC-SAP-RMC/Creacion/v1" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xml:id="id_1" oraxsl:ignorexmlids="true"
                xmlns:ns4="http://www.entel.cl/EBO/CustomerOrder/v1"
                xmlns:ns6="http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1"
                xmlns:ns10="http://www.entel.cl/EBO/Quantity/v1" xmlns:ns12="http://www.entel.cl/EBO/EmailContact/v1"
                xmlns:ns14="http://www.entel.cl/EBO/Money/v1"
                xmlns:ns15="http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1"
                xmlns:ns17="http://www.entel.cl/EBO/PostalDeliveryAddress/v1"
                xmlns:ns2="http://www.entel.cl/RA/CHL-SAP-RMC-SAP-RMC/Creacion/v1"
                xmlns:ns19="http://www.entel.cl/EBO/CustomerBill/v1" xmlns:ns29="http://www.entel.cl/EBO/Rate/v1"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:ns31="http://www.entel.cl/EBO/GeographicArea/v1"
                xmlns:ns32="http://www.entel.cl/EBO/Individual/v1"
                xmlns:ns36="http://www.entel.cl/EBO/AuthenticationEntity/v1"
                xmlns:ns1="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/publish/v1"
                xmlns:ns38="http://www.entel.cl/EDD/Dictionary/v1" xmlns:ns39="http://www.entel.cl/ESO/Error/v1"
                xmlns:client="http://xmlns.oracle.com/ES/OH/BPELProcess"
                xmlns:ns40="http://www.entel.cl/EBO/AppliedCustomerBillingRate/v1"
                xmlns:ns41="http://www.entel.cl/EBO/IndividualIdentification/v1"
                xmlns:ns42="http://www.entel.cl/EBO/BillingAccount/v1"
                xmlns:ns43="http://www.entel.cl/EBO/SalesChannel/v1"
                xmlns:ns3="http://www.entel.cl/EBO/CustomerAccount/v1"
                xmlns:ns5="http://www.entel.cl/EBO/PaymentItem/v1" xmlns:ns7="http://www.entel.cl/EBO/IssuingCompany/v1"
                xmlns:ns8="http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1"
                xmlns:ns9="http://www.entel.cl/EBO/CustomerAccountBalance/v1"
                xmlns:ns11="http://www.entel.cl/EBO/TelephoneNumber/v1"
                xmlns:ns13="http://www.entel.cl/EBO/PartyRole/v1" xmlns:ns16="http://www.entel.cl/EBO/Customer/v1"
                xmlns:ns18="http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1"
                xmlns:ns20="http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1"
                xmlns:ns21="http://www.entel.cl/EBO/OrganizationName/v1"
                xmlns:ns22="http://www.entel.cl/EBO/CustomerAddress/v1"
                xmlns:ns23="http://www.entel.cl/EBO/CustomerComercialAddress/v1"
                xmlns:ns24="http://www.entel.cl/EBO/PartyIdentification/v1"
                xmlns:ns25="http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1"
                xmlns:ns26="http://www.entel.cl/EBO/IndividualName/v1" xmlns:ns27="http://www.entel.cl/ESO/Result/v2"
                xmlns:ns28="http://www.entel.cl/EBO/Skill/v1" xmlns:ns30="http://www.entel.cl/ESO/MessageHeader/v1"
                xmlns:ns33="http://www.entel.cl/EBO/Organization/v1" xmlns:ns34="http://www.entel.cl/EBO/StreetName/v1"
                xmlns:ns35="http://www.entel.cl/EBO/TimePeriod/v1" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ns37="http://www.entel.cl/EBO/FinancialChargeSpec/v1"
                xmlns:ns44="http://www.entel.cl/EBO/Country/v1"
                xmlns:ns45="http://www.entel.cl/EBO/LoyaltyTransaction/v1"
                xmlns:ns46="http://www.entel.cl/EBO/MSISDN/v1" xmlns:ns="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/">
   <oracle-xsl-mapper:schema xml:id="id_2">
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources xml:id="id_3">
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_4">
            <oracle-xsl-mapper:schema location="../WSDLs/BPELProcess.wsdl" xml:id="id_5"/>
            <oracle-xsl-mapper:rootElement name="Create_AppliedCustomerBillingCharge_REQ" namespace="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Create/v1" xml:id="id_6"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets xml:id="id_7">
         <oracle-xsl-mapper:target type="WSDL" xml:id="id_8">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_RA_CHL-SAP-RMC_v1/ResourceAdapters/CHL-SAP-RMC-SAP-RMC_Creacion_RA_v1/CSC/CHL-SAP-RMC-SAP-RMC_Creacion_v1_CSC.wsdl" xml:id="id_9"/>
            <oracle-xsl-mapper:rootElement name="CHL-SAP-RMC-SAP-RMC_Creacion_REQ" namespace="http://www.entel.cl/CSM/RA/CHL-SAP-RMC-SAP-RMC/Creacion/v1" xml:id="id_10"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [WED OCT 12 15:31:03 ART 2016].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/" xml:id="id_11">
      <tns:CHL-SAP-RMC-SAP-RMC_Creacion_REQ xml:id="id_12">
         <ns30:RequestHeader xml:id="id_13">
            <ns30:Consumer sysCode="{/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Consumer/@sysCode}"
                           enterpriseCode="{/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Consumer/@enterpriseCode}"
                           countryCode="{/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Consumer/@countryCode}"
                           xml:id="id_14">
               <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Consumer"
                             xml:id="id_15"/>
            </ns30:Consumer>
            <ns30:Trace clientReqTimestamp="{/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@clientReqTimestamp}"
                        eventID="{/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@eventID}"
                        xml:id="id_16">
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@reqTimestamp"
                       xml:id="id_17">
                  <xsl:attribute name="reqTimestamp" xml:id="id_18">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@reqTimestamp"
                                   xml:id="id_19"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@rspTimestamp"
                       xml:id="id_20">
                  <xsl:attribute name="rspTimestamp" xml:id="id_21">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@rspTimestamp"
                                   xml:id="id_22"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@processID"
                       xml:id="id_23">
                  <xsl:attribute name="processID" xml:id="id_24">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@processID"
                                   xml:id="id_25"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@sourceID"
                       xml:id="id_26">
                  <xsl:attribute name="sourceID" xml:id="id_27">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@sourceID"
                                   xml:id="id_28"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@correlationEventID"
                       xml:id="id_29">
                  <xsl:attribute name="correlationEventID" xml:id="id_30">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@correlationEventID"
                                   xml:id="id_31"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@conversationID"
                       xml:id="id_32">
                  <xsl:attribute name="conversationID" xml:id="id_33">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@conversationID"
                                   xml:id="id_34"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@correlationID"
                       xml:id="id_35">
                  <xsl:attribute name="correlationID" xml:id="id_36">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/@correlationID"
                                   xml:id="id_37"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service"
                       xml:id="id_38">
                  <ns30:Service xml:id="id_39">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service/@code"
                             xml:id="id_40">
                        <xsl:attribute name="code" xml:id="id_41">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service/@code"
                                         xml:id="id_42"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service/@name"
                             xml:id="id_43">
                        <xsl:attribute name="name" xml:id="id_44">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service/@name"
                                         xml:id="id_45"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service/@operation"
                             xml:id="id_46">
                        <xsl:attribute name="operation" xml:id="id_47">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service/@operation"
                                         xml:id="id_48"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Trace/ns30:Service"
                                   xml:id="id_49"/>
                  </ns30:Service>
               </xsl:if>
            </ns30:Trace>
            <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Channel" xml:id="id_50">
               <ns30:Channel xml:id="id_51">
                  <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Channel/@name"
                          xml:id="id_52">
                     <xsl:attribute name="name" xml:id="id_53">
                        <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Channel/@name"
                                      xml:id="id_54"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Channel/@mode"
                          xml:id="id_55">
                     <xsl:attribute name="mode" xml:id="id_56">
                        <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Channel/@mode"
                                      xml:id="id_57"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns30:Channel"
                                xml:id="id_58"/>
               </ns30:Channel>
            </xsl:if>
            <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result" xml:id="id_59">
               <ns27:Result status="{/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/@status}"
                            xml:id="id_60">
                  <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/@description"
                          xml:id="id_61">
                     <xsl:attribute name="description" xml:id="id_62">
                        <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/@description"
                                      xml:id="id_63"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError"
                          xml:id="id_64">
                     <ns39:CanonicalError xml:id="id_65">
                        <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError/@type"
                                xml:id="id_66">
                           <xsl:attribute name="type" xml:id="id_67">
                              <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError/@type"
                                            xml:id="id_68"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError/@code"
                                xml:id="id_69">
                           <xsl:attribute name="code" xml:id="id_70">
                              <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError/@code"
                                            xml:id="id_71"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError/@description"
                                xml:id="id_72">
                           <xsl:attribute name="description" xml:id="id_73">
                              <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError/@description"
                                            xml:id="id_74"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:CanonicalError"
                                      xml:id="id_75"/>
                     </ns39:CanonicalError>
                  </xsl:if>
                  <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError"
                          xml:id="id_76">
                     <ns39:SourceError xml:id="id_77">
                        <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/@code"
                                xml:id="id_78">
                           <xsl:attribute name="code" xml:id="id_79">
                              <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/@code"
                                            xml:id="id_80"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/@description"
                                xml:id="id_81">
                           <xsl:attribute name="description" xml:id="id_82">
                              <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/@description"
                                            xml:id="id_83"/>
                           </xsl:attribute>
                        </xsl:if>
                        <ns39:ErrorSourceDetails xml:id="id_84">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/ns39:ErrorSourceDetails/@source"
                                   xml:id="id_85">
                              <xsl:attribute name="source" xml:id="id_86">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/ns39:ErrorSourceDetails/@source"
                                               xml:id="id_87"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/ns39:ErrorSourceDetails/@details"
                                   xml:id="id_88">
                              <xsl:attribute name="details" xml:id="id_89">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/ns39:ErrorSourceDetails/@details"
                                               xml:id="id_90"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns39:SourceError/ns39:ErrorSourceDetails"
                                         xml:id="id_91"/>
                        </ns39:ErrorSourceDetails>
                     </ns39:SourceError>
                  </xsl:if>
                  <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns27:CorrelativeErrors"
                          xml:id="id_92">
                     <ns27:CorrelativeErrors xml:id="id_93">
                        <xsl:for-each select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns30:RequestHeader/ns27:Result/ns27:CorrelativeErrors/ns39:SourceError"
                                      xml:id="id_94">
                           <ns39:SourceError xml:id="id_95">
                              <xsl:if test="@code" xml:id="id_96">
                                 <xsl:attribute name="code" xml:id="id_97">
                                    <xsl:value-of select="@code" xml:id="id_98"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <xsl:if test="@description" xml:id="id_99">
                                 <xsl:attribute name="description" xml:id="id_100">
                                    <xsl:value-of select="@description" xml:id="id_101"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <ns39:ErrorSourceDetails xml:id="id_102">
                                 <xsl:if test="ns39:ErrorSourceDetails/@source" xml:id="id_103">
                                    <xsl:attribute name="source" xml:id="id_104">
                                       <xsl:value-of select="ns39:ErrorSourceDetails/@source" xml:id="id_105"/>
                                    </xsl:attribute>
                                 </xsl:if>
                                 <xsl:if test="ns39:ErrorSourceDetails/@details" xml:id="id_106">
                                    <xsl:attribute name="details" xml:id="id_107">
                                       <xsl:value-of select="ns39:ErrorSourceDetails/@details" xml:id="id_108"/>
                                    </xsl:attribute>
                                 </xsl:if>
                                 <xsl:value-of select="ns39:ErrorSourceDetails" xml:id="id_109"/>
                              </ns39:ErrorSourceDetails>
                           </ns39:SourceError>
                        </xsl:for-each>
                     </ns27:CorrelativeErrors>
                  </xsl:if>
               </ns27:Result>
            </xsl:if>
         </ns30:RequestHeader>
         <tns:Body xml:id="id_110">
            <tns:CustomerBill xml:id="id_111">
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billComments1"
                       xml:id="id_112">
                  <ns19:billComments1 xml:id="id_113">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billComments1"
                                   xml:id="id_114"/>
                  </ns19:billComments1>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billComments2"
                       xml:id="id_115">
                  <ns19:billComments2 xml:id="id_116">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billComments2"
                                   xml:id="id_117"/>
                  </ns19:billComments2>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billDate"
                       xml:id="id_118">
                  <ns19:billDate xml:id="id_119">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billDate"
                                   xml:id="id_120"/>
                  </ns19:billDate>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billNo"
                       xml:id="id_121">
                  <ns19:billNo xml:id="id_122">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billNo"
                                   xml:id="id_123"/>
                  </ns19:billNo>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billNoOriginal"
                       xml:id="id_124">
                  <ns19:billNoOriginal xml:id="id_125">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billNoOriginal"
                                   xml:id="id_126"/>
                  </ns19:billNoOriginal>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billType"
                       xml:id="id_127">
                  <ns19:billType xml:id="id_128">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billType"
                                   xml:id="id_129"/>
                  </ns19:billType>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billTypeAssociated"
                       xml:id="id_130">
                  <ns19:billTypeAssociated xml:id="id_131">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:billTypeAssociated"
                                   xml:id="id_132"/>
                  </ns19:billTypeAssociated>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:causalCode"
                       xml:id="id_133">
                  <ns19:causalCode xml:id="id_134">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:causalCode"
                                   xml:id="id_135"/>
                  </ns19:causalCode>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:debtType"
                       xml:id="id_136">
                  <ns19:debtType xml:id="id_137">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:debtType"
                                   xml:id="id_138"/>
                  </ns19:debtType>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:discountAmount"
                       xml:id="id_139">
                  <ns19:discountAmount xml:id="id_140">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:discountAmount"
                                   xml:id="id_141"/>
                  </ns19:discountAmount>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:discountPercentage"
                       xml:id="id_142">
                  <ns19:discountPercentage xml:id="id_143">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:discountPercentage"
                                   xml:id="id_144"/>
                  </ns19:discountPercentage>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:exemptAmount"
                       xml:id="id_145">
                  <ns19:exemptAmount xml:id="id_146">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:exemptAmount"
                                   xml:id="id_147"/>
                  </ns19:exemptAmount>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:generationFormat"
                       xml:id="id_148">
                  <ns19:generationFormat xml:id="id_149">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:generationFormat"
                                   xml:id="id_150"/>
                  </ns19:generationFormat>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:header"
                       xml:id="id_151">
                  <ns19:header xml:id="id_152">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:header"
                                   xml:id="id_153"/>
                  </ns19:header>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:idTransaction"
                       xml:id="id_154">
                  <ns19:idTransaction xml:id="id_155">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:idTransaction"
                                   xml:id="id_156"/>
                  </ns19:idTransaction>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:netAmount"
                       xml:id="id_157">
                  <ns19:netAmount xml:id="id_158">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:netAmount"
                                   xml:id="id_159"/>
                  </ns19:netAmount>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:observation"
                       xml:id="id_160">
                  <ns19:observation xml:id="id_161">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:observation"
                                   xml:id="id_162"/>
                  </ns19:observation>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:operation"
                       xml:id="id_163">
                  <ns19:operation xml:id="id_164">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:operation"
                                   xml:id="id_165"/>
                  </ns19:operation>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:paymentDueDate"
                       xml:id="id_166">
                  <ns19:paymentDueDate xml:id="id_167">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:paymentDueDate"
                                   xml:id="id_168"/>
                  </ns19:paymentDueDate>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:presentIndicator"
                       xml:id="id_169">
                  <ns19:presentIndicator xml:id="id_170">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:presentIndicator"
                                   xml:id="id_171"/>
                  </ns19:presentIndicator>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:rechargeAmount"
                       xml:id="id_172">
                  <ns19:rechargeAmount xml:id="id_173">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:rechargeAmount"
                                   xml:id="id_174"/>
                  </ns19:rechargeAmount>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:rechargePercentage"
                       xml:id="id_175">
                  <ns19:rechargePercentage xml:id="id_176">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:rechargePercentage"
                                   xml:id="id_177"/>
                  </ns19:rechargePercentage>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:totalAmount"
                       xml:id="id_178">
                  <ns19:totalAmount xml:id="id_179">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:totalAmount"
                                   xml:id="id_180"/>
                  </ns19:totalAmount>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:totalTaxValue"
                       xml:id="id_181">
                  <ns19:totalTaxValue xml:id="id_182">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:totalTaxValue"
                                   xml:id="id_183"/>
                  </ns19:totalTaxValue>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:vendorId"
                       xml:id="id_184">
                  <ns19:vendorId xml:id="id_185">
                     <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns19:vendorId"
                                   xml:id="id_186"/>
                  </ns19:vendorId>
               </xsl:if>
               <xsl:for-each select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:AppliedCustomerBillingChargeDetail"
                             xml:id="id_187">
                  <tns:AppliedCustomerBillingChargeDetail xml:id="id_188">
                     <xsl:if test="ns25:contract" xml:id="id_189">
                        <ns25:contract xml:id="id_190">
                           <xsl:value-of select="ns25:contract" xml:id="id_191"/>
                        </ns25:contract>
                     </xsl:if>
                     <xsl:if test="ns25:purchaseOrder" xml:id="id_192">
                        <ns25:purchaseOrder xml:id="id_193">
                           <xsl:value-of select="ns25:purchaseOrder" xml:id="id_194"/>
                        </ns25:purchaseOrder>
                     </xsl:if>
                     <xsl:if test="ns0:appliedCustomerBillingProductCharge" xml:id="id_195">
                        <tns:appliedCustomerBillingProductCharge xml:id="id_196">
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:conceptCode" xml:id="id_197">
                              <ns6:conceptCode xml:id="id_198">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:conceptCode"
                                               xml:id="id_199"/>
                              </ns6:conceptCode>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:costCenter" xml:id="id_200">
                              <ns6:costCenter xml:id="id_201">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:costCenter"
                                               xml:id="id_202"/>
                              </ns6:costCenter>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:discountAmount" xml:id="id_203">
                              <ns6:discountAmount xml:id="id_204">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:discountAmount"
                                               xml:id="id_205"/>
                              </ns6:discountAmount>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:discountPercentage"
                                   xml:id="id_206">
                              <ns6:discountPercentage xml:id="id_207">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:discountPercentage"
                                               xml:id="id_208"/>
                              </ns6:discountPercentage>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:itemCode" xml:id="id_209">
                              <ns6:itemCode xml:id="id_210">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:itemCode"
                                               xml:id="id_211"/>
                              </ns6:itemCode>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:itemDescription" xml:id="id_212">
                              <ns6:itemDescription xml:id="id_213">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:itemDescription"
                                               xml:id="id_214"/>
                              </ns6:itemDescription>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:itemNumber" xml:id="id_215">
                              <ns6:itemNumber xml:id="id_216">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:itemNumber"
                                               xml:id="id_217"/>
                              </ns6:itemNumber>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:itemPrice" xml:id="id_218">
                              <ns6:itemPrice xml:id="id_219">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:itemPrice"
                                               xml:id="id_220"/>
                              </ns6:itemPrice>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:itemType" xml:id="id_221">
                              <ns6:itemType xml:id="id_222">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:itemType"
                                               xml:id="id_223"/>
                              </ns6:itemType>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:minutes" xml:id="id_224">
                              <ns6:minutes xml:id="id_225">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:minutes"
                                               xml:id="id_226"/>
                              </ns6:minutes>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:netAmount" xml:id="id_227">
                              <ns6:netAmount xml:id="id_228">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:netAmount"
                                               xml:id="id_229"/>
                              </ns6:netAmount>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:quantityItem" xml:id="id_230">
                              <ns6:quantityItem xml:id="id_231">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:quantityItem"
                                               xml:id="id_232"/>
                              </ns6:quantityItem>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:rechargeAmount" xml:id="id_233">
                              <ns6:rechargeAmount xml:id="id_234">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:rechargeAmount"
                                               xml:id="id_235"/>
                              </ns6:rechargeAmount>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:rechargePercentage"
                                   xml:id="id_236">
                              <ns6:rechargePercentage xml:id="id_237">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:rechargePercentage"
                                               xml:id="id_238"/>
                              </ns6:rechargePercentage>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:serialNumber" xml:id="id_239">
                              <ns6:serialNumber xml:id="id_240">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:serialNumber"
                                               xml:id="id_241"/>
                              </ns6:serialNumber>
                           </xsl:if>
                           <xsl:if test="ns0:appliedCustomerBillingProductCharge/ns6:unit" xml:id="id_242">
                              <ns6:unit xml:id="id_243">
                                 <xsl:value-of select="ns0:appliedCustomerBillingProductCharge/ns6:unit"
                                               xml:id="id_244"/>
                              </ns6:unit>
                           </xsl:if>
                        </tns:appliedCustomerBillingProductCharge>
                     </xsl:if>
                     <xsl:if test="ns0:appliedCustomerBillingTaxRate" xml:id="id_245">
                        <tns:appliedCustomerBillingTaxRate xml:id="id_246">
                           <xsl:if test="ns0:appliedCustomerBillingTaxRate/ns8:taxIndicator" xml:id="id_247">
                              <ns8:taxIndicator xml:id="id_248">
                                 <xsl:value-of select="ns0:appliedCustomerBillingTaxRate/ns8:taxIndicator"
                                               xml:id="id_249"/>
                              </ns8:taxIndicator>
                           </xsl:if>
                        </tns:appliedCustomerBillingTaxRate>
                     </xsl:if>
                     <xsl:if test="ns0:paymentItem" xml:id="id_250">
                        <tns:paymentItem xml:id="id_251">
                           <xsl:if test="ns0:paymentItem/ns0:appliedAmount" xml:id="id_252">
                              <tns:appliedAmount xml:id="id_253">
                                 <xsl:if test="ns0:paymentItem/ns0:appliedAmount/ns14:amount" xml:id="id_254">
                                    <ns14:amount xml:id="id_255">
                                       <xsl:value-of select="ns0:paymentItem/ns0:appliedAmount/ns14:amount"
                                                     xml:id="id_256"/>
                                    </ns14:amount>
                                 </xsl:if>
                                 <xsl:if test="ns0:paymentItem/ns0:appliedAmount/ns14:units" xml:id="id_257">
                                    <ns14:units xml:id="id_258">
                                       <xsl:value-of select="ns0:paymentItem/ns0:appliedAmount/ns14:units"
                                                     xml:id="id_259"/>
                                    </ns14:units>
                                 </xsl:if>
                              </tns:appliedAmount>
                           </xsl:if>
                        </tns:paymentItem>
                     </xsl:if>
                  </tns:AppliedCustomerBillingChargeDetail>
               </xsl:for-each>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:AppliedCustomerBillingRate"
                       xml:id="id_260">
                  <tns:AppliedCustomerBillingRate xml:id="id_261">
                     <xsl:for-each select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:AppliedCustomerBillingRate/ns0:AppliedCustomerBillingProductAlteration"
                                   xml:id="id_262">
                        <tns:AppliedCustomerBillingProductAlteration xml:id="id_263">
                           <xsl:if test="ns20:amount" xml:id="id_264">
                              <ns20:amount xml:id="id_265">
                                 <xsl:value-of select="ns20:amount" xml:id="id_266"/>
                              </ns20:amount>
                           </xsl:if>
                           <xsl:if test="ns20:compensatedAmount" xml:id="id_267">
                              <ns20:compensatedAmount xml:id="id_268">
                                 <xsl:value-of select="ns20:compensatedAmount" xml:id="id_269"/>
                              </ns20:compensatedAmount>
                           </xsl:if>
                           <xsl:if test="ns20:compensatedDocumentNumber" xml:id="id_270">
                              <ns20:compensatedDocumentNumber xml:id="id_271">
                                 <xsl:value-of select="ns20:compensatedDocumentNumber" xml:id="id_272"/>
                              </ns20:compensatedDocumentNumber>
                           </xsl:if>
                           <xsl:if test="ns20:compensatedDocumentStatus" xml:id="id_273">
                              <ns20:compensatedDocumentStatus xml:id="id_274">
                                 <xsl:value-of select="ns20:compensatedDocumentStatus" xml:id="id_275"/>
                              </ns20:compensatedDocumentStatus>
                           </xsl:if>
                           <xsl:if test="ns20:compensatedDocumentType" xml:id="id_276">
                              <ns20:compensatedDocumentType xml:id="id_277">
                                 <xsl:value-of select="ns20:compensatedDocumentType" xml:id="id_278"/>
                              </ns20:compensatedDocumentType>
                           </xsl:if>
                           <xsl:if test="ns20:documentStatus" xml:id="id_279">
                              <ns20:documentStatus xml:id="id_280">
                                 <xsl:value-of select="ns20:documentStatus" xml:id="id_281"/>
                              </ns20:documentStatus>
                           </xsl:if>
                           <xsl:if test="ns20:documentType" xml:id="id_282">
                              <ns20:documentType xml:id="id_283">
                                 <xsl:value-of select="ns20:documentType" xml:id="id_284"/>
                              </ns20:documentType>
                           </xsl:if>
                           <xsl:if test="ns20:referenceNumberForCompensation" xml:id="id_285">
                              <ns20:referenceNumberForCompensation xml:id="id_286">
                                 <xsl:value-of select="ns20:referenceNumberForCompensation" xml:id="id_287"/>
                              </ns20:referenceNumberForCompensation>
                           </xsl:if>
                        </tns:AppliedCustomerBillingProductAlteration>
                     </xsl:for-each>
                  </tns:AppliedCustomerBillingRate>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:AuthenticationEntity"
                       xml:id="id_288">
                  <tns:AuthenticationEntity xml:id="id_289">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:AuthenticationEntity/ns36:userName"
                             xml:id="id_290">
                        <ns36:userName xml:id="id_291">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:AuthenticationEntity/ns36:userName"
                                         xml:id="id_292"/>
                        </ns36:userName>
                     </xsl:if>
                  </tns:AuthenticationEntity>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount"
                       xml:id="id_293">
                  <tns:CustomerAccount xml:id="id_294">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns3:ID"
                             xml:id="id_295">
                        <ns3:ID xml:id="id_296">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns3:ID"
                                         xml:id="id_297"/>
                        </ns3:ID>
                     </xsl:if>
                     <ns3:customerType xml:id="id_298">
                        <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns3:customerType"
                                      xml:id="id_299"/>
                     </ns3:customerType>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:BillingAccount"
                             xml:id="id_300">
                        <tns:BillingAccount xml:id="id_301">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:BillingAccount/ns42:externalID"
                                   xml:id="id_302">
                              <ns42:externalID xml:id="id_303">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:BillingAccount/ns42:externalID"
                                               xml:id="id_304"/>
                              </ns42:externalID>
                           </xsl:if>
                        </tns:BillingAccount>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer"
                             xml:id="id_305">
                        <tns:Customer xml:id="id_306">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:EmailContact"
                                   xml:id="id_307">
                              <tns:EmailContact xml:id="id_308">
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:EmailContact/ns12:eMailAddress"
                                         xml:id="id_309">
                                    <ns12:eMailAddress xml:id="id_310">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:EmailContact/ns12:eMailAddress"
                                                     xml:id="id_311"/>
                                    </ns12:eMailAddress>
                                 </xsl:if>
                              </tns:EmailContact>
                           </xsl:if>
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification"
                                   xml:id="id_312">
                              <tns:IndividualIdentification xml:id="id_313">
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns41:number"
                                         xml:id="id_314">
                                    <ns41:number xml:id="id_315">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns41:number"
                                                     xml:id="id_316"/>
                                    </ns41:number>
                                 </xsl:if>
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns41:type"
                                         xml:id="id_317">
                                    <ns41:type xml:id="id_318">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns41:type"
                                                     xml:id="id_319"/>
                                    </ns41:type>
                                 </xsl:if>
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual"
                                         xml:id="id_320">
                                    <tns:Individual xml:id="id_321">
                                       <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual/ns0:IndividualName"
                                               xml:id="id_322">
                                          <tns:IndividualName xml:id="id_323">
                                             <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual/ns0:IndividualName/ns26:alias"
                                                     xml:id="id_324">
                                                <ns26:alias xml:id="id_325">
                                                   <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual/ns0:IndividualName/ns26:alias"
                                                                 xml:id="id_326"/>
                                                </ns26:alias>
                                             </xsl:if>
                                             <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual/ns0:IndividualName/ns26:firstName"
                                                     xml:id="id_327">
                                                <ns26:firstName xml:id="id_328">
                                                   <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual/ns0:IndividualName/ns26:firstName"
                                                                 xml:id="id_329"/>
                                                </ns26:firstName>
                                             </xsl:if>
                                             <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual/ns0:IndividualName/ns26:lastName"
                                                     xml:id="id_330">
                                                <ns26:lastName xml:id="id_331">
                                                   <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:IndividualIdentification/ns0:Individual/ns0:IndividualName/ns26:lastName"
                                                                 xml:id="id_332"/>
                                                </ns26:lastName>
                                             </xsl:if>
                                          </tns:IndividualName>
                                       </xsl:if>
                                    </tns:Individual>
                                 </xsl:if>
                              </tns:IndividualIdentification>
                           </xsl:if>
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:Skill"
                                   xml:id="id_333">
                              <tns:Skill xml:id="id_334">
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:Skill/ns28:skillSpecification"
                                         xml:id="id_335">
                                    <ns28:skillSpecification xml:id="id_336">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:Customer/ns0:Skill/ns28:skillSpecification"
                                                     xml:id="id_337"/>
                                    </ns28:skillSpecification>
                                 </xsl:if>
                              </tns:Skill>
                           </xsl:if>
                        </tns:Customer>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAccountBalance"
                             xml:id="id_338">
                        <tns:CustomerAccountBalance xml:id="id_339">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAccountBalance/ns9:contractAccount"
                                   xml:id="id_340">
                              <ns9:contractAccount xml:id="id_341">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAccountBalance/ns9:contractAccount"
                                               xml:id="id_342"/>
                              </ns9:contractAccount>
                           </xsl:if>
                        </tns:CustomerAccountBalance>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress"
                             xml:id="id_343">
                        <tns:CustomerAddress xml:id="id_344">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress"
                                   xml:id="id_345">
                              <tns:postalDeliveryAddress xml:id="id_346">
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns17:address"
                                         xml:id="id_347">
                                    <ns17:address xml:id="id_348">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns17:address"
                                                     xml:id="id_349"/>
                                    </ns17:address>
                                 </xsl:if>
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns17:city"
                                         xml:id="id_350">
                                    <ns17:city xml:id="id_351">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns17:city"
                                                     xml:id="id_352"/>
                                    </ns17:city>
                                 </xsl:if>
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns17:commune"
                                         xml:id="id_353">
                                    <ns17:commune xml:id="id_354">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerAddress/ns0:postalDeliveryAddress/ns17:commune"
                                                     xml:id="id_355"/>
                                    </ns17:commune>
                                 </xsl:if>
                              </tns:postalDeliveryAddress>
                           </xsl:if>
                        </tns:CustomerAddress>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress"
                             xml:id="id_356">
                        <tns:CustomerComercialAddress xml:id="id_357">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:geographicArea"
                                   xml:id="id_358">
                              <tns:geographicArea xml:id="id_359">
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:geographicArea/ns31:city"
                                         xml:id="id_360">
                                    <ns31:city xml:id="id_361">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:geographicArea/ns31:city"
                                                     xml:id="id_362"/>
                                    </ns31:city>
                                 </xsl:if>
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:geographicArea/ns31:commune"
                                         xml:id="id_363">
                                    <ns31:commune xml:id="id_364">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:geographicArea/ns31:commune"
                                                     xml:id="id_365"/>
                                    </ns31:commune>
                                 </xsl:if>
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:geographicArea/ns31:neighborhood"
                                         xml:id="id_366">
                                    <ns31:neighborhood xml:id="id_367">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:geographicArea/ns31:neighborhood"
                                                     xml:id="id_368"/>
                                    </ns31:neighborhood>
                                 </xsl:if>
                              </tns:geographicArea>
                           </xsl:if>
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:streetName"
                                   xml:id="id_369">
                              <tns:streetName xml:id="id_370">
                                 <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:streetName/ns34:name"
                                         xml:id="id_371">
                                    <ns34:name xml:id="id_372">
                                       <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:CustomerComercialAddress/ns0:streetName/ns34:name"
                                                     xml:id="id_373"/>
                                    </ns34:name>
                                 </xsl:if>
                              </tns:streetName>
                           </xsl:if>
                        </tns:CustomerComercialAddress>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:FinancialChargeSpec"
                             xml:id="id_374">
                        <tns:FinancialChargeSpec xml:id="id_375">
                           <ns37:ID xml:id="id_376">
                              <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerAccount/ns0:FinancialChargeSpec/ns37:ID"
                                            xml:id="id_377"/>
                           </ns37:ID>
                        </tns:FinancialChargeSpec>
                     </xsl:if>
                  </tns:CustomerAccount>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerBillingCycleSpecification"
                       xml:id="id_378">
                  <tns:CustomerBillingCycleSpecification xml:id="id_379">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerBillingCycleSpecification/ns18:name"
                             xml:id="id_380">
                        <ns18:name xml:id="id_381">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:CustomerBillingCycleSpecification/ns18:name"
                                         xml:id="id_382"/>
                        </ns18:name>
                     </xsl:if>
                  </tns:CustomerBillingCycleSpecification>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany"
                       xml:id="id_383">
                  <tns:IssuingCompany xml:id="id_384">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:CustomerOrder"
                             xml:id="id_385">
                        <tns:CustomerOrder xml:id="id_386">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:CustomerOrder/ns4:ID"
                                   xml:id="id_387">
                              <ns4:ID xml:id="id_388">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:CustomerOrder/ns4:ID"
                                               xml:id="id_389"/>
                              </ns4:ID>
                           </xsl:if>
                        </tns:CustomerOrder>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName"
                             xml:id="id_390">
                        <tns:organizationName xml:id="id_391">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName/ns21:shortName"
                                   xml:id="id_392">
                              <ns21:shortName xml:id="id_393">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName/ns21:shortName"
                                               xml:id="id_394"/>
                              </ns21:shortName>
                           </xsl:if>
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName/ns21:tradingName"
                                   xml:id="id_395">
                              <ns21:tradingName xml:id="id_396">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:organizationName/ns21:tradingName"
                                               xml:id="id_397"/>
                              </ns21:tradingName>
                           </xsl:if>
                        </tns:organizationName>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:partyIdentification"
                             xml:id="id_398">
                        <tns:partyIdentification xml:id="id_399">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:partyIdentification/ns24:number"
                                   xml:id="id_400">
                              <ns24:number xml:id="id_401">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:partyIdentification/ns24:number"
                                               xml:id="id_402"/>
                              </ns24:number>
                           </xsl:if>
                        </tns:partyIdentification>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:thirdPartyCollectionPM"
                             xml:id="id_403">
                        <tns:thirdPartyCollectionPM xml:id="id_404">
                           <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:thirdPartyCollectionPM/ns15:thirdPartyID"
                                   xml:id="id_405">
                              <ns15:thirdPartyID xml:id="id_406">
                                 <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:IssuingCompany/ns0:thirdPartyCollectionPM/ns15:thirdPartyID"
                                               xml:id="id_407"/>
                              </ns15:thirdPartyID>
                           </xsl:if>
                        </tns:thirdPartyCollectionPM>
                     </xsl:if>
                  </tns:IssuingCompany>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:LoyaltyTransaction"
                       xml:id="id_408">
                  <tns:LoyaltyTransaction xml:id="id_409">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:LoyaltyTransaction/ns45:amount"
                             xml:id="id_410">
                        <ns45:amount xml:id="id_411">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:LoyaltyTransaction/ns45:amount"
                                         xml:id="id_412"/>
                        </ns45:amount>
                     </xsl:if>
                  </tns:LoyaltyTransaction>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:Rate"
                       xml:id="id_413">
                  <tns:Rate xml:id="id_414">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:Rate/ns29:date"
                             xml:id="id_415">
                        <ns29:date xml:id="id_416">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:Rate/ns29:date"
                                         xml:id="id_417"/>
                        </ns29:date>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:Rate/ns29:rateValue"
                             xml:id="id_418">
                        <ns29:rateValue xml:id="id_419">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:Rate/ns29:rateValue"
                                         xml:id="id_420"/>
                        </ns29:rateValue>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:Rate/ns29:type"
                             xml:id="id_421">
                        <ns29:type xml:id="id_422">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:Rate/ns29:type"
                                         xml:id="id_423"/>
                        </ns29:type>
                     </xsl:if>
                  </tns:Rate>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:billAmount"
                       xml:id="id_424">
                  <tns:billAmount xml:id="id_425">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:billAmount/ns14:amount"
                             xml:id="id_426">
                        <ns14:amount xml:id="id_427">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:billAmount/ns14:amount"
                                         xml:id="id_428"/>
                        </ns14:amount>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:billAmount/ns14:units"
                             xml:id="id_429">
                        <ns14:units xml:id="id_430">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:billAmount/ns14:units"
                                         xml:id="id_431"/>
                        </ns14:units>
                     </xsl:if>
                  </tns:billAmount>
               </xsl:if>
               <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:unit"
                       xml:id="id_432">
                  <tns:unit xml:id="id_433">
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:unit/ns14:amount"
                             xml:id="id_434">
                        <ns14:amount xml:id="id_435">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:unit/ns14:amount"
                                         xml:id="id_436"/>
                        </ns14:amount>
                     </xsl:if>
                     <xsl:if test="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:unit/ns14:units"
                             xml:id="id_437">
                        <ns14:units xml:id="id_438">
                           <xsl:value-of select="/ns0:Create_AppliedCustomerBillingCharge_REQ/ns0:Body/ns0:CustomerBill/ns0:unit/ns14:units"
                                         xml:id="id_439"/>
                        </ns14:units>
                     </xsl:if>
                  </tns:unit>
               </xsl:if>
            </tns:CustomerBill>
         </tns:Body>
      </tns:CHL-SAP-RMC-SAP-RMC_Creacion_REQ>
   </xsl:template>
</xsl:stylesheet>
