xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/Error/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/Error_v1_ESO.xsd" ::)
declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace tran="http://www.bea.com/wli/sb/transports";

declare variable $LegacyResponse as element() external;
declare variable $LegacyResult as element()? external;
 
declare function local:PER-EBS-ARR-EBS-ARR_JDBC_PR_CREA_PAGO_SEB($LegacyResponse as element(), $LegacyResult as element()?) as element()  (:: schema-element(ns1:SourceError) ::) {

    let $SourceErrorCode	          := local:getSourceErrorCode()
    let $SourceErrorDescription 	  := local:getSourceErrorDescription()
    let $ErrorSourceCode                  := "PER-EBS-ARR"
    let $ErrorSourceDetails               := "PER-EBS-ARR-EBS-ARR_JDBC_PR_CREA_PAGO_TW"
    
    return 
        <ns1:SourceError code="{$SourceErrorCode}" description="{$SourceErrorDescription}">
            <ns1:ErrorSourceDetails source="{$ErrorSourceCode}" details="{$ErrorSourceDetails}"/>
            <ns1:SourceFault>{$LegacyResponse}</ns1:SourceFault>
        </ns1:SourceError>
};

declare function local:getSourceErrorCode() as xs:string
{
	
	let $FaultErrorCode := 
            if (exists( $LegacyResponse/*:XV_COD_RPTA )) 
            then data($LegacyResponse/*:XV_COD_RPTA)
            else ('FRW-Default')
        (: if JCA not return data of code error status, then use FRW-Default :)

	return $FaultErrorCode
};

declare function local:getSourceErrorDescription() as xs:string
{
          let $FaultErrorDescription := 
            if(exists($LegacyResponse/*:XV_MSG_RPTA)) 
            then data($LegacyResponse/*:XV_MSG_RPTA)
            else ('No Legacy Result Information was found. This is a default Legacy Result.')
	
	return $FaultErrorDescription
};

local:PER-EBS-ARR-EBS-ARR_JDBC_PR_CREA_PAGO_SEB( $LegacyResponse, $LegacyResult)
