xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/CSM/RA/PER-EBS-ARR-EBS-ARR/pr_consulta_deuda/v1";
(:: import schema at "../../../../../../DC_RA_PER-EBS-ARR_v1/ResourceAdapters/PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RA_v1/CSC/PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_v1_CSM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Get/v1";
(:: import schema at "../../../../../ESC/Primary/Get_AppliedCustomerBillingCharge_v1_EBM.xsd" ::)

declare namespace ns6 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/MSISDN/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Get_AppliedCustomerBillingCharge_REQ as element() (:: schema-element(ns1:Get_AppliedCustomerBillingCharge_REQ) ::) external;

declare function local:get_PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_REQ($Get_AppliedCustomerBillingCharge_REQ as element() (:: schema-element(ns1:Get_AppliedCustomerBillingCharge_REQ) ::)) as element() (:: schema-element(ns2:PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_REQ) ::) {
    <ns2:PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_REQ>
        {$Get_AppliedCustomerBillingCharge_REQ/*[1]}
        <ns2:Body>
            <ns2:BillDocument>
                {
                    if ($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount)
                    then 
                        <ns2:customerAccount>
                            {
                                if (fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns6:ID)!='')
                                then <ns6:ID>{fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns6:ID)}</ns6:ID>
                                else ()
                            }
                            {
                                if (fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns6:multipleAccounts)!='')
                                then <ns6:multipleAccounts>{fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns6:multipleAccounts)}</ns6:multipleAccounts>
                                else ()
                            }
                            {
                                if ($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns1:Asset)
                                then <ns2:Asset>
                                    {
                                        if ($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns1:Asset/ns1:MSISDN)
                                        then 
                                            <ns2:MSISDN>
                                                {
                                                    if (fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns1:Asset/ns1:MSISDN/ns7:SN)!='')
                                                    then <ns7:SN>{fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerAccount/ns1:Asset/ns1:MSISDN/ns7:SN)}</ns7:SN>
                                                    else ()
                                                }
                                            </ns2:MSISDN>
                                        else ()
                                    }</ns2:Asset>
                                else ()
                            }
                        </ns2:customerAccount>
                    else ()
                }
                {
                    if ($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill)
                    then 
                        <ns2:customerBill>
                            {
                                if (fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:billNo)!='')
                                then <ns8:billNo>{fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:billNo)}</ns8:billNo>
                                else ()
                            }
                            {
                                if (fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:debtType)!='')
                                then <ns8:debtType>{fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:debtType)}</ns8:debtType>
                                else ()
                            }
                            {
                                if (fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:documentType)!='')
                                then <ns8:documentType>{fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:documentType)}</ns8:documentType>
                                else ()
                            }
                            {
                                if (fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:searchType)!='')
                                then <ns8:searchType>{fn:data($Get_AppliedCustomerBillingCharge_REQ/ns1:Body/ns1:BillDocument/ns1:customerBill/ns8:searchType)}</ns8:searchType>
                                else ()
                            }
                        </ns2:customerBill>
                    else ()
                }
            </ns2:BillDocument>
        </ns2:Body>
    </ns2:PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_REQ>
};

local:get_PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_REQ($Get_AppliedCustomerBillingCharge_REQ)
