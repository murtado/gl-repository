xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Get/v1";
(:: import schema at "../../../../../ESC/Primary/Get_AppliedCustomerBillingCharge_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $ResponseHeader as element() (:: schema-element(ns1:ResponseHeader) ::) external;

declare function local:get_Get_AppliedCustomerBillingCreditRequest_FRSP($ResponseHeader as element() (:: schema-element(ns1:ResponseHeader) ::)) as element() (:: schema-element(ns2:Get_AppliedCustomerBillingCharge_FRSP) ::) {
    <ns2:Get_AppliedCustomerBillingCharge_FRSP>
        {$ResponseHeader}
        <ns2:Body></ns2:Body>
    </ns2:Get_AppliedCustomerBillingCharge_FRSP>
};

local:get_Get_AppliedCustomerBillingCreditRequest_FRSP($ResponseHeader)