xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/CSM/RA/PER-EBS-ARR-EBS-ARR/pr_consulta_deuda/v1";
(:: import schema at "../../../../../../DC_RA_PER-EBS-ARR_v1/ResourceAdapters/PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RA_v1/CSC/PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_v1_CSM.xsd" ::)
declare namespace ns3="http://www.entel.cl/EBM/AppliedCustomerBillingCharge/Get/v1";
(:: import schema at "../../../../../ESC/Primary/Get_AppliedCustomerBillingCharge_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns6 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/MSISDN/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/PaymentMethod/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1";

declare namespace ns9 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns12 = "http://www.entel.cl/EBO/PaymentPlan/v1";

declare namespace ns11 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns10 = "http://www.entel.cl/EBO/OrganizationName/v1";

declare namespace ns4 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns13 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns14 = "http://www.entel.cl/ESO/Error/v1";

declare variable $RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::) external;
declare variable $PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP as element() (:: schema-element(ns2:PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP) ::) external;

declare function local:get_Get_AppliedCustomerBillingCharge_RSP($RequestHeader as element() (:: schema-element(ns1:RequestHeader) ::), 
                                                                $PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP as element() (:: schema-element(ns2:PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP) ::)) 
                                                                as element() (:: schema-element(ns3:Get_AppliedCustomerBillingCharge_RSP) ::) {
    <ns3:Get_AppliedCustomerBillingCharge_RSP>
      <ns1:ResponseHeader>
      {$RequestHeader/*}
      {$PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/*[1]}
      </ns1:ResponseHeader>
        <ns3:Body>
            <ns3:CustomerAccount>
                {
                    if (fn:data($PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/ns2:Body/ns2:CustomerAccount/ns4:ID)!='')
                    then <ns4:ID>{fn:data($PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/ns2:Body/ns2:CustomerAccount/ns4:ID)}</ns4:ID>
                    else ()
                }                
                {
                    if ($PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/ns2:Body/ns2:CustomerAccount/ns2:Asset)
                    then <ns3:Asset>
                        {
                            if ($PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/ns2:Body/ns2:CustomerAccount/ns2:Asset/ns2:MSISDN)
                            then 
                                <ns3:MSISDN>
                                    {
                                        if (fn:data($PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/ns2:Body/ns2:CustomerAccount/ns2:Asset/ns2:MSISDN/ns5:SN)!='')
                                        then <ns5:SN>{fn:data($PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/ns2:Body/ns2:CustomerAccount/ns2:Asset/ns2:MSISDN/ns5:SN)}</ns5:SN>
                                        else ()
                                    }
                                </ns3:MSISDN>
                            else ()
                        }</ns3:Asset>
                    else ()
                }
                {
                    for $CustomerBill in $PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP/ns2:Body/ns2:CustomerAccount/ns2:CustomerBill
                    return 
                    <ns3:CustomerBill>
                        {
                            if (xs:string(fn:data($CustomerBill/ns6:assentInterest))!='')
                            then <ns6:assentInterest>{fn:data($CustomerBill/ns6:assentInterest)}</ns6:assentInterest>
                            else ()
                        }
                        {
                            if (xs:string(fn:data($CustomerBill/ns6:balanceDue))!='')
                            then <ns6:balanceDue>{fn:data($CustomerBill/ns6:balanceDue)}</ns6:balanceDue>
                            else ()
                        }
                        {
                            if (xs:string(fn:data($CustomerBill/ns6:billDate))!='')
                            then <ns6:billDate>{fn:data($CustomerBill/ns6:billDate)}</ns6:billDate>
                            else ()
                        }
                        {
                            if (fn:data($CustomerBill/ns6:billNo)!='')
                            then <ns6:billNo>{fn:data($CustomerBill/ns6:billNo)}</ns6:billNo>
                            else ()
                        }
                        {
                            if (fn:data($CustomerBill/ns6:documentType)!='')
                            then <ns6:documentType>{fn:data($CustomerBill/ns6:documentType)}</ns6:documentType>
                            else ()
                        }
                        {
                            if (xs:string(fn:data($CustomerBill/ns6:paymentDueDate))!='')
                            then <ns6:paymentDueDate>{fn:data($CustomerBill/ns6:paymentDueDate)}</ns6:paymentDueDate>
                            else ()
                        }
                        {
                            if (xs:string(fn:data($CustomerBill/ns6:totalAmount))!='')
                            then <ns6:totalAmount>{fn:data($CustomerBill/ns6:totalAmount)}</ns6:totalAmount>
                            else ()
                        }
                        {
                            if ($CustomerBill/ns2:Money)
                            then 
                                <ns3:Money>
                                    {
                                        if (fn:data($CustomerBill/ns2:Money/ns9:units)!='')
                                        then <ns9:units>{fn:data($CustomerBill/ns2:Money/ns9:units)}</ns9:units>
                                        else ()
                                    }
                                </ns3:Money>
                            else ()
                        }
                    </ns3:CustomerBill>
                }
            </ns3:CustomerAccount>
        </ns3:Body>
    </ns3:Get_AppliedCustomerBillingCharge_RSP>
};

local:get_Get_AppliedCustomerBillingCharge_RSP($RequestHeader, $PER-EBS-ARR-EBS-ARR_pr_consulta_deuda_RSP)
