xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-SAP-RMC/Pago/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_Pago_v1_CSM.xsd" ::)
declare namespace ns2="urn:eusb:B2C_pos_B2P:Recaudacion";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SAP-RMC-SAP-RMC/HTTPSOAP11/WSDL/Recaudacion.wsdl" ::)

declare namespace ns3 = "http://www.entel.cl/EBO/BillDocument/v1";

declare namespace ns4 = "http://www.entel.cl/EBO/CustomerPayment/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns6 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns9 = "http://www.entel.cl/EBO/OrganizationName/v1";

declare namespace ns10 = "http://www.entel.cl/EBO/PaymentMethod/v1";

declare namespace ns11 = "http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1";

declare variable $CHL-SAP-RMC-SAP-RMC_Pago_REQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Pago_REQ) ::) external;

declare function local:get_CHL-SAP-RMC-SAP-RMC_Pago_LegacyREQ($CHL-SAP-RMC-SAP-RMC_Pago_REQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Pago_REQ) ::)) as element() (:: schema-element(ns2:MT_PagoRecaudacion_Req) ::) {
    <ns2:MT_PagoRecaudacion_Req>
        <ZST_RECAUDACION_HEADER>
            <BUKRS>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:IssuingCompany/ns1:organizationName/ns9:shortName)}</BUKRS>
            <OPERACION>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns3:operation)}</OPERACION>
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:paymentDate)
                then <FECHA>{fn-bea:dateTime-to-string-with-format("yyy-MM-dd",$CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:paymentDate)}</FECHA>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:operatorID)
                then <USUARIO>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:operatorID)}</USUARIO>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:ID)
                then <TRANSACCION>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:ID)}</TRANSACCION>
                else ()
            }          
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns11:branch)
                then <SUCURSAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns11:branch)}</SUCURSAL>
                else ()
            }           
        </ZST_RECAUDACION_HEADER>
        {
          for $CustomerBill in $CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerBill
          return 
           <ZTT_DOCUMENTO_RECAUDACION>
            {
                if ($CustomerBill/ns7:documentType)
                then <BLART>{fn:data($CustomerBill/ns7:documentType)}</BLART>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns1:IndividualIdentification/ns6:number)
                then <RUT>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns1:IndividualIdentification/ns6:number)}</RUT>
                else ()
            }
            {
                if ($CustomerBill/ns7:billNo)
                then <XBLNR>{fn:data($CustomerBill/ns7:billNo)}</XBLNR>
                else ()
            }
            {
                if ($CustomerBill/ns7:totalAmount)
                then <BETRH>{xs:string(fn:data($CustomerBill/ns7:totalAmount))}</BETRH>
                else ()
            }
             </ZTT_DOCUMENTO_RECAUDACION>
           }            
       {
       let $l := count($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod)
       for $i in 1 to $l
       return       
        <ZTT_PAGO_RECAUDACION>            
            {
              if($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerBill[$i]/ns7:billDate) then
                <BLDAT>{fn-bea:dateTime-to-string-with-format("yyy-MM-dd",$CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerBill[$i]/ns7:billDate)}</BLDAT>
              else()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:bankCode)
                then <BANKL>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:bankCode)}</BANKL>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:bankAccountNumber)
                then <BANKN>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:bankAccountNumber)}</BANKN>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:date)
                then <FAEDN>{fn-bea:dateTime-to-string-with-format("yyy-MM-dd",$CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:date)}</FAEDN>
                else ()
            }           
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:operatorID)
                then <USER_F2K>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:operatorID)}</USER_F2K>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:ID)
                then <NRO_F2K>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:ID)}</NRO_F2K>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:paymentDate)
                then <FECHA_F2K>{fn-bea:dateTime-to-string-with-format("yyy-MM-dd",$CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns4:paymentDate)}</FECHA_F2K>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns11:branch)
                then <SUCURSAL_F2K>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns11:branch)}</SUCURSAL_F2K>
                else ()
            }
           
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns1:IndividualIdentification/ns6:number)
                then <RUT>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns1:IndividualIdentification/ns6:number)}</RUT>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns5:ID)
                then <VKONT>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns5:ID)}</VKONT>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerBill/ns1:Money/ns8:units)
                then <WAERS>{xs:string(fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerBill[$i]/ns1:Money/ns8:units))}</WAERS>
                else ()
            }
            {
                if ($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:ID)
                then <PAGO>{fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:PaymentMethod[$i]/ns10:ID)}</PAGO>
                else ()
            }          
            {
                if (fn:data($CHL-SAP-RMC-SAP-RMC_Pago_REQ/ns1:Body/ns1:BillDocument/ns3:operation)='0') then
                    <IND_PAGO_ABONO>{'P'}</IND_PAGO_ABONO>
                else
                    <IND_PAGO_ABONO>{'A'}</IND_PAGO_ABONO>
            }
        </ZTT_PAGO_RECAUDACION>
        }
    </ns2:MT_PagoRecaudacion_Req>
};

local:get_CHL-SAP-RMC-SAP-RMC_Pago_LegacyREQ($CHL-SAP-RMC-SAP-RMC_Pago_REQ)
