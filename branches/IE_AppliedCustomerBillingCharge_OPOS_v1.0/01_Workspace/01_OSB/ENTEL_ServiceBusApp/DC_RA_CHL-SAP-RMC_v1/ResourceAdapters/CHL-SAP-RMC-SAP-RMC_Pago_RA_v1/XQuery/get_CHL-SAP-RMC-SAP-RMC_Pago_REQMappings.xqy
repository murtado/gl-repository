xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-SAP-RMC/Pago/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_Pago_v1_CSM.xsd" ::)

declare namespace ns6 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/OrganizationName/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns9 = "http://www.entel.cl/EBO/PaymentMethod/v1";

declare namespace ns10 = "http://www.entel.cl/EBO/ThirdPartyPayeeAgency/v1";

declare namespace ns2 = "http://www.entel.cl/EBO/BillDocument/v1";

declare namespace ns4 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns3 = "http://www.entel.cl/EBO/CustomerPayment/v1";

declare namespace ns13 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns12 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns11 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $RaREQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Pago_REQ) ::) external;
declare variable $GetMappingRSP as element() external;
declare variable $PaymentMethodList as element() external;
declare variable $CustomerBillList as element() external;

declare function local:get_CHL-SAP-RMC-SAP-RMC_Pago_REQMappings($RaREQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Pago_REQ) ::), 
                                                                $GetMappingRSP as element(), 
                                                                $PaymentMethodList as element(), 
                                                                $CustomerBillList as element()) 
                                                                as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Pago_REQ) ::) {
    <ns1:CHL-SAP-RMC-SAP-RMC_Pago_REQ>      
        <ns1:Body>
            <ns1:BillDocument>
                {
                    if ($RaREQ/ns1:Body/ns1:BillDocument/ns2:operation)
                    then <ns2:operation>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns2:operation)}</ns2:operation>
                    else ()
                }
                <ns1:customerPayment>
                    {
                        if ($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns3:ID)
                        then <ns3:ID>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns3:ID)}</ns3:ID>
                        else ()
                    }
                    {
                        if ($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns3:operatorID)
                        then <ns3:operatorID>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns3:operatorID)}</ns3:operatorID>
                        else ()
                    }
                    {
                        if ($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns3:paymentDate)
                        then <ns3:paymentDate>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns3:paymentDate)}</ns3:paymentDate>
                        else ()
                    }
                    <ns1:CustomerAccount>
                        {
                            if ($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns4:ID)
                            then <ns4:ID>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns4:ID)}</ns4:ID>
                            else ()
                        }
                        <ns1:IndividualIdentification>
                            {
                                if ($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns1:IndividualIdentification/ns5:number)
                                then <ns5:number>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:CustomerAccount/ns1:IndividualIdentification/ns5:number)}</ns5:number>
                                else ()
                            }
                        </ns1:IndividualIdentification>
                    </ns1:CustomerAccount>
                    {$CustomerBillList/*}                    
                    <ns1:IssuingCompany>
                        <ns1:organizationName>
                        {if (fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:IssuingCompany/ns1:organizationName/ns8:shortName!=''))
                        then
                            <ns8:shortName>{data($GetMappingRSP/*[1]/*[@entity="OrganizationName" and @field="shortName"]/@dCode)}</ns8:shortName>
                          else()
                          }
                        </ns1:organizationName>
                    </ns1:IssuingCompany>
                    {$PaymentMethodList/*}
                    <ns1:ThirdPartyPayeeAgency>
                        {
                            if ($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns10:branch)
                            then <ns10:branch>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns10:branch)}</ns10:branch>
                            else ()
                        }
                        {
                            if ($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns10:branchName)
                            then <ns10:branchName>{fn:data($RaREQ/ns1:Body/ns1:BillDocument/ns1:customerPayment/ns1:ThirdPartyPayeeAgency/ns10:branchName)}</ns10:branchName>
                            else ()
                        }
                    </ns1:ThirdPartyPayeeAgency>
                </ns1:customerPayment>
            </ns1:BillDocument>
        </ns1:Body>
    </ns1:CHL-SAP-RMC-SAP-RMC_Pago_REQ>
};

local:get_CHL-SAP-RMC-SAP-RMC_Pago_REQMappings($RaREQ, $GetMappingRSP, $PaymentMethodList, $CustomerBillList)
