xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/CSM/RA/CHL-SAP-RMC/Pago/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_Pago_v1_CSM.xsd" ::)
declare namespace ns1="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)
declare namespace ns2="urn:eusb:B2C_pos_B2P:Recaudacion";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SAP-RMC-SAP-RMC/HTTPSOAP11/WSDL/Recaudacion.wsdl" ::)

declare variable $Result as element() (:: schema-element(ns1:Result) ::) external;
declare variable $MT_PagoRecaudacion_Res as element() (:: schema-element(ns2:MT_PagoRecaudacion_Res) ::) external;

declare function local:get_CHL-SAP-RMC-SAP-RMC_Pago_AdapterRSP_OK.xqy($Result as element() (:: schema-element(ns1:Result) ::), 
                                                                      $MT_PagoRecaudacion_Res as element() (:: schema-element(ns2:MT_PagoRecaudacion_Res) ::)) 
                                                                      as element() (:: schema-element(ns3:CHL-SAP-RMC-SAP-RMC_Pago_RSP) ::) {
    <ns3:CHL-SAP-RMC-SAP-RMC_Pago_RSP>
        {$Result}
        <ns3:Body></ns3:Body>
    </ns3:CHL-SAP-RMC-SAP-RMC_Pago_RSP>
};

local:get_CHL-SAP-RMC-SAP-RMC_Pago_AdapterRSP_OK.xqy($Result, $MT_PagoRecaudacion_Res)
