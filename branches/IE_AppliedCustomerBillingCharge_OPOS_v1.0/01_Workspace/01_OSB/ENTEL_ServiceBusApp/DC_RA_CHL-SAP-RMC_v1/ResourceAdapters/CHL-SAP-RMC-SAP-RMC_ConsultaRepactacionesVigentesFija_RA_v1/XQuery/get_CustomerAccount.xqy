xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace ns3="http://www.entel.cl/CSM/RA/CHL-SAP-RMC-SAP-RMC/ConsultaRepactacionesVigentesFija/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_ConsultaRepactacionesVigentesFija_v1_CSM.xsd" ::)
declare namespace ns6 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/PaymentMethod/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/PaymentPlan/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns4 = "http://www.entel.cl/EBO/CustomerBill/v1";
declare variable $GetMappingRSP as element() external;
declare variable $CustomerAccount as element() external;

declare function local:get_CustomerAccount($GetMappingRSP as element(), 
                                                              $CustomerAccount as element()) 
                                                              as element() {
    
      <ns3:CustomerAccount>
                    <ns3:PaymentPlan>
                        <ns8:type>{fn:data($GetMappingRSP/*[1]/*[@entity="PaymentPlan" and @field="type"]/@dCode)}</ns8:type>
                        <ns3:CustomerBill>
                            <ns4:balanceDue>{fn:data($CustomerAccount/ns3:PaymentPlan/ns3:CustomerBill/ns4:balanceDue)}</ns4:balanceDue>
                            <ns4:billNo>{fn:data($CustomerAccount/ns3:PaymentPlan/ns3:CustomerBill/ns4:billNo)}</ns4:billNo>
                            <ns4:documentType>{fn:data($GetMappingRSP/*[1]/*[@entity="CustomerBill" and @field="documentType"]/@dCode)}</ns4:documentType>
                            <ns4:paymentDueDate>{fn:data($CustomerAccount/ns3:PaymentPlan/ns3:CustomerBill/ns4:paymentDueDate)}</ns4:paymentDueDate>
                            {$CustomerAccount/ns3:PaymentPlan/ns3:CustomerBill/ns3:CustomerPayment}
                        </ns3:CustomerBill>
                    </ns3:PaymentPlan>
      </ns3:CustomerAccount>
};

local:get_CustomerAccount($GetMappingRSP, $CustomerAccount)
