xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-SAP-RMC-SAP-RMC/Creacion/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_Creacion_v1_CSM.xsd" ::)
declare namespace ns6 = "http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1";
declare namespace ns7 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1";
declare namespace ns8 = "http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1";
declare namespace ns9 = "http://www.entel.cl/EBO/Money/v1";

declare variable $GetMappingRSP as element() external;
declare variable $BillingChargeDetail as element() external;

declare function local:get_AppliedCustomerBillingChargeDetail($GetMappingRSP as element(), 
                                                              $BillingChargeDetail as element()) 
                                                              as element() {
    <ns1:AppliedCustomerBillingChargeDetail>
                    {
                        if ($BillingChargeDetail/ns6:contract)
                        then <ns6:contract>{fn:data($BillingChargeDetail/ns6:contract)}</ns6:contract>
                        else ()
                    }
                    {
                        if ($BillingChargeDetail/ns6:purchaseOrder)
                        then <ns6:purchaseOrder>{fn:data($BillingChargeDetail/ns6:purchaseOrder)}</ns6:purchaseOrder>
                        else ()
                    }
                    <ns1:appliedCustomerBillingProductCharge>
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:conceptCode)
                            then <ns7:conceptCode>{fn:data($GetMappingRSP/*[1]/*[@entity="AppliedCustomerBillingProductCharge" and @field="conceptCode"]/@dCode)}</ns7:conceptCode>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:costCenter)
                            then <ns7:costCenter>{fn:data($GetMappingRSP/*[1]/*[@entity="AppliedCustomerBillingProductCharge" and @field="costCenter"]/@dCode)}</ns7:costCenter>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:discountAmount)
                            then <ns7:discountAmount>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:discountAmount)}</ns7:discountAmount>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:discountPercentage)
                            then <ns7:discountPercentage>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:discountPercentage)}</ns7:discountPercentage>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemCode)
                            then <ns7:itemCode>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemCode)}</ns7:itemCode>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemDescription)
                            then <ns7:itemDescription>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemDescription)}</ns7:itemDescription>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemNumber)
                            then <ns7:itemNumber>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemNumber)}</ns7:itemNumber>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemPrice)
                            then <ns7:itemPrice>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemPrice)}</ns7:itemPrice>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:itemType)
                            then <ns7:itemType>{fn:data($GetMappingRSP/*[1]/*[@entity="AppliedCustomerBillingProductCharge" and @field="itemType"]/@dCode)}</ns7:itemType>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:minutes)
                            then <ns7:minutes>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:minutes)}</ns7:minutes>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:netAmount)
                            then <ns7:netAmount>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:netAmount)}</ns7:netAmount>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:quantityItem)
                            then <ns7:quantityItem>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:quantityItem)}</ns7:quantityItem>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:rechargeAmount)
                            then <ns7:rechargeAmount>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:rechargeAmount)}</ns7:rechargeAmount>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:rechargePercentage)
                            then <ns7:rechargePercentage>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:rechargePercentage)}</ns7:rechargePercentage>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:serialNumber)
                            then <ns7:serialNumber>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:serialNumber)}</ns7:serialNumber>
                            else ()
                        }
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:unit)
                            then <ns7:unit>{fn:data($BillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns7:unit)}</ns7:unit>
                            else ()
                        }
                    </ns1:appliedCustomerBillingProductCharge>
                    <ns1:appliedCustomerBillingTaxRate>
                        {
                            if ($BillingChargeDetail/ns1:appliedCustomerBillingTaxRate/ns8:taxIndicator)
                            then <ns8:taxIndicator>{fn:data($GetMappingRSP/*[1]/*[@entity="AppliedCustomerBillingTaxRate" and @field="taxIndicator"]/@dCode)}</ns8:taxIndicator>
                            else ()
                        }
                    </ns1:appliedCustomerBillingTaxRate>
                    <ns1:paymentItem>
                        <ns1:appliedAmount>
                            {
                                if ($BillingChargeDetail/ns1:paymentItem/ns1:appliedAmount/ns9:amount)
                                then <ns9:amount>{fn:data($BillingChargeDetail/ns1:paymentItem/ns1:appliedAmount/ns9:amount)}</ns9:amount>
                                else ()
                            }
                            {
                                if ($BillingChargeDetail/ns1:paymentItem/ns1:appliedAmount/ns9:units)
                                then <ns9:units>{fn:data($BillingChargeDetail/ns1:paymentItem/ns1:appliedAmount/ns9:units)}</ns9:units>
                                else ()
                            }
                        </ns1:appliedAmount>
                    </ns1:paymentItem>
                </ns1:AppliedCustomerBillingChargeDetail>
                    
};

local:get_AppliedCustomerBillingChargeDetail($GetMappingRSP, $BillingChargeDetail)
