xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-SAP-RMC-SAP-RMC/Creacion/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_Creacion_v1_CSM.xsd" ::)
declare namespace ns10 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1";

declare variable $BillingProductAlteration as element() external;
declare variable $GetMappingRSP as element() external;

declare function local:get_AppliedCustomerBillingProductAlteration($BillingProductAlteration as element(), 
                                                                   $GetMappingRSP as element()) 
                                                                   as element() {
     <ns1:AppliedCustomerBillingProductAlteration>
                        {
                            if ($BillingProductAlteration/ns10:amount)
                            then <ns10:amount>{fn:data($BillingProductAlteration/ns10:amount)}</ns10:amount>
                            else ()
                        }
                        {
                            if ($BillingProductAlteration/ns10:compensatedAmount)
                            then <ns10:compensatedAmount>{fn:data($BillingProductAlteration/ns10:compensatedAmount)}</ns10:compensatedAmount>
                            else ()
                        }
                        {
                            if ($BillingProductAlteration/ns10:compensatedDocumentNumber)
                            then <ns10:compensatedDocumentNumber>{fn:data($BillingProductAlteration/ns10:compensatedDocumentNumber)}</ns10:compensatedDocumentNumber>
                            else ()
                        }
                        {
                            if ($BillingProductAlteration/ns10:compensatedDocumentStatus)
                            then <ns10:compensatedDocumentStatus>{fn:data($BillingProductAlteration/ns10:compensatedDocumentStatus)}</ns10:compensatedDocumentStatus>
                            else ()
                        }
                        {
                            if ($BillingProductAlteration/ns10:compensatedDocumentType)
                            then <ns10:compensatedDocumentType>{fn:data($GetMappingRSP/*[1]/*[@entity="AppliedCustomerBillingProductAlteration" and @field="compensatedDocumentType"]/@dCode)}</ns10:compensatedDocumentType>
                            else ()
                        }
                        {
                            if ($BillingProductAlteration/ns10:documentStatus)
                            then <ns10:documentStatus>{fn:data($BillingProductAlteration/ns10:documentStatus)}</ns10:documentStatus>
                            else ()
                        }
                        {
                            if ($BillingProductAlteration/ns10:documentType)
                            then <ns10:documentType>{fn:data($GetMappingRSP/*[1]/*[@entity="AppliedCustomerBillingProductAlteration" and @field="documentType"]/@dCode)}</ns10:documentType>
                            else ()
                        }
                        {
                            if ($BillingProductAlteration/ns10:referenceNumberForCompensation)
                            then <ns10:referenceNumberForCompensation>{fn:data($BillingProductAlteration/ns10:referenceNumberForCompensation)}</ns10:referenceNumberForCompensation>
                            else ()
                        }
         </ns1:AppliedCustomerBillingProductAlteration>
};

local:get_AppliedCustomerBillingProductAlteration($BillingProductAlteration, $GetMappingRSP)
