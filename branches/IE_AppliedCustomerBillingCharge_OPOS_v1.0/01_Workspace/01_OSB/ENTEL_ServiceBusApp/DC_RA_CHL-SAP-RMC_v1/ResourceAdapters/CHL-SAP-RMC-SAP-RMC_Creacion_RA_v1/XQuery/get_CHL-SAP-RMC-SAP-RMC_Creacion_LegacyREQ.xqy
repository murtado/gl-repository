xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-SAP-RMC-SAP-RMC/Creacion/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_Creacion_v1_CSM.xsd" ::)
declare namespace ns2="urn:eusb:B2C_pos_B2P:Facturacion";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SAP-RMC-SAP-RMC/HTTPSOAP11/WSDL/Facturacion.wsdl" ::)

declare namespace ns3 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns4 = "http://www.entel.cl/EBO/LoyaltyTransaction/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1";

declare namespace ns6 = "http://www.entel.cl/EBO/OrganizationName/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/PartyIdentification/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1";

declare namespace ns9 = "http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1";

declare namespace ns10 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns11 = "http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1";

declare namespace ns12 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1";

declare namespace ns13 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1";

declare namespace ns14 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns15 = "http://www.entel.cl/EBO/BillingAccount/v1";

declare namespace ns16 = "http://www.entel.cl/EBO/CustomerAccountBalance/v1";

declare namespace ns17 = "http://www.entel.cl/EBO/Skill/v1";

declare namespace ns18 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns19 = "http://www.entel.cl/EBO/GeographicArea/v1";

declare namespace ns20 = "http://www.entel.cl/EBO/StreetName/v1";

declare namespace ns21 = "http://www.entel.cl/EBO/PostalDeliveryAddress/v1";

declare namespace ns22 = "http://www.entel.cl/EBO/AuthenticationEntity/v1";

declare variable $CHL-SAP-RMC-SAP-RMC_Creacion_REQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Creacion_REQ) ::) external;

declare function local:get_CHL-SAP-RMC-SAP-RMC_Creacion_LegacyREQ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Creacion_REQ) ::)) as element() (:: schema-element(ns2:MT_Facturacion_Req) ::) {
    <ns2:MT_Facturacion_Req>
        <ZPCS_ESTRUCT_DOC>
            <ZPCS_ESTRUCT_ENCDOC>
                <COD_EMPRESA>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:organizationName/ns6:shortName)}</COD_EMPRESA>
                <COD_DCTO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billType)}</COD_DCTO>
                <CODIGO_CAUSAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:causalCode)}</CODIGO_CAUSAL>
                <COD_SUCURSAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:thirdPartyCollectionPM/ns8:thirdPartyID)}</COD_SUCURSAL>
                <COD_VENDEDOR>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:vendorId)}</COD_VENDEDOR>
                <TIPO_CLIENTE>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns14:customerType)}</TIPO_CLIENTE>
                <RUT>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns18:number)}</RUT>
                <CUENTA_BSCS>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:BillingAccount/ns15:externalID)}</CUENTA_BSCS>
                <RAZON_SOC>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:organizationName/ns6:tradingName)}</RAZON_SOC>
                <VAL_NETO>{                
                if (fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:netAmount))
                then (fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:netAmount))
                else (0)
                }</VAL_NETO>
                
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:discountAmount)
                    then <VAL_DECTO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:discountAmount)}</VAL_DECTO>
                    else (<VAL_DECTO>0</VAL_DECTO>)
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:discountPercentage)
                    then <POR_DESCTO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:discountPercentage)}</POR_DESCTO>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:rechargeAmount)
                    then <VAL_RECARGO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:rechargeAmount)}</VAL_RECARGO>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:rechargePercentage)
                    then <POR_RECARGO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:rechargePercentage)}</POR_RECARGO>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:exemptAmount)
                    then <VAL_EXENTO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:exemptAmount)}</VAL_EXENTO>
                    else (<VAL_EXENTO>0</VAL_EXENTO>)
                }
                <VALOR_IVA>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:totalTaxValue)}</VALOR_IVA>
                <TOT_FACTURA>{
                if(fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:totalAmount))
                then (fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:totalAmount))
                else (0)
                }</TOT_FACTURA>
                
                
                {
                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:paymentDueDate)
                then  <FEC_EMISION>{  
                fn-bea:dateTime-to-string-with-format("yyyyMMdd",$CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:paymentDueDate)
                 }</FEC_EMISION>
                else()
                
                }
                {
                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:paymentDueDate)
                then  <FEC_CVTO>{ 
                fn-bea:dateTime-to-string-with-format("yyyyMMdd",$CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:paymentDueDate)
                }</FEC_CVTO>
                else()
                }
              
                <ESTADO/>
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:generationFormat)
                    then <FORMA_GEN>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:generationFormat)}</FORMA_GEN>
                    else ()
                }
               
                <EST_CTA_CTE/>
                {
                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billDate)
                then  <FEC_SOLICIT>{  
                fn-bea:dateTime-to-string-with-format("yyyyMMdd",$CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billDate)
                }</FEC_SOLICIT>
                else()
                }
                <SUC_SOLICIT>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:thirdPartyCollectionPM/ns8:thirdPartyID)}</SUC_SOLICIT>
                <USR_SOL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:AuthenticationEntity/ns22:userName)}</USR_SOL>
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:vendorId)
                    then <COD_USR>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:vendorId)}</COD_USR>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billComments1)
                    then <OBSERVACION1>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billComments1)}</OBSERVACION1>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billComments2)
                    then <OBSERVACION2>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billComments2)}</OBSERVACION2>
                    else ()
                }
                <NUM_DCTO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billNo)}</NUM_DCTO>
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:operation)
                    then <FORMATO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:operation)}</FORMATO>
                    else ()
                }
                {
                   if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billDate)
                then  <PERIODO>{
                  fn-bea:dateTime-to-string-with-format("yyyyMMdd",$CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billDate)
                 }</PERIODO>
                else()
                }              
                {
                     if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billDate)
                then  <F_EMISION>{ 
                fn-bea:dateTime-to-string-with-format("yyyyMMdd",$CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billDate)
                 }</F_EMISION>
                else()
                }
                <DIR_POSTAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns21:address)}</DIR_POSTAL>
                <COMUNA_POSTAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns21:commune)}</COMUNA_POSTAL>
                <CIUDAD_POSTAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns21:city)}</CIUDAD_POSTAL>
                <DIR_COMERCIAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:streetName/ns20:name)}</DIR_COMERCIAL>
                <COMUNA_COMERCIAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns19:commune)}</COMUNA_COMERCIAL>
                <CIUDAD_COMERCIAL>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns19:city)}</CIUDAD_COMERCIAL>
                <GIRO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:Skill/ns17:skillSpecification)}</GIRO>
                <SECTOR>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns19:neighborhood)}</SECTOR>
                <RUT_EMISOR>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:partyIdentification/ns7:number)}</RUT_EMISOR>
                <NUMERO_NEGOCIO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:idTransaction)}</NUMERO_NEGOCIO>
                <NRO_TX_OT></NRO_TX_OT>
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:presentIndicator)
                    then <INDICADOR_REGALO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:presentIndicator)}</INDICADOR_REGALO>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerBillingCycleSpecification/ns5:name)
                    then <CICLO_FACTURA>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerBillingCycleSpecification/ns5:name)}</CICLO_FACTURA>
                    else ()
                }
                <CUENTA_CTO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAccountBalance/ns16:contractAccount)}</CUENTA_CTO>
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:idTransaction)
                    then <ID_TRANSACCION>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:idTransaction)}</ID_TRANSACCION>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:LoyaltyTransaction/ns4:amount)
                    then <PUNTOS>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:LoyaltyTransaction/ns4:amount)}</PUNTOS>
                    else ()
                }
        
                <NUEVO_TOTAL/>
            </ZPCS_ESTRUCT_ENCDOC>
            { 
            for $AppliedCustomerBillingChargeDetail in $CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:AppliedCustomerBillingChargeDetail
            return
            <ZPCS_ESTRUCT_DETDOC>
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:organizationName/ns6:shortName)
                    then <COD_EMPRESA>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:organizationName/ns6:shortName)}</COD_EMPRESA>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billType)
                    then <COD_DCTO>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns3:billType)}</COD_DCTO>
                    else ()
                }
                <NUM_LINEA>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:itemNumber)}</NUM_LINEA>
                <TIPO_PRODUCTO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:itemType)}</TIPO_PRODUCTO>
                <COD_PRODUCTO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:itemCode)}</COD_PRODUCTO>
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:itemPrice)
                    then <PRECIO_UNI>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:itemPrice)}</PRECIO_UNI>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:quantityItem)
                    then <CANTIDAD>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:quantityItem)}</CANTIDAD>
                    else ()
                }
                <VAL_NETO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:netAmount)}</VAL_NETO>
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:discountAmount)
                    then <VAL_DESCTO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:discountAmount)}</VAL_DESCTO>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:discountPercentage)
                    then <POR_DESCTO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:discountPercentage)}</POR_DESCTO>
                    else (<POR_DESCTO>0</POR_DESCTO>)
                }
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:rechargeAmount)
                    then <VAL_RECARGO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:rechargeAmount)}</VAL_RECARGO>
                    else (<VAL_RECARGO>0</VAL_RECARGO>)
                }
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:rechargePercentage)
                    then <POR_RECARGO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:rechargePercentage)}</POR_RECARGO>
                    else (<POR_RECARGO>0</POR_RECARGO>)
                }
                <TOTAL_ITEM>{fn:data($AppliedCustomerBillingChargeDetail/ns1:paymentItem/ns1:appliedAmount/ns10:units)}</TOTAL_ITEM>
                {
                    if ($AppliedCustomerBillingChargeDetail/ns9:contract)
                    then <CONTRATO>{fn:data($AppliedCustomerBillingChargeDetail/ns9:contract)}</CONTRATO>
                    else ()
                }
                <DETALLE>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:itemDescription)}</DETALLE>
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:minutes)
                    then <MINUTOS>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:minutes)}</MINUTOS>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:unit)
                    then <UNIDAD>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:unit)}</UNIDAD>
                    else ()
                }
                <CODIGO_SGI></CODIGO_SGI>
                {
                    if ($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:serialNumber)
                    then <SERIE_EQUIPO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:serialNumber)}</SERIE_EQUIPO>
                    else ()
                }
                <ORDEN_CO>{fn:data($AppliedCustomerBillingChargeDetail/ns9:purchaseOrder)}</ORDEN_CO>
                <INDICADOR_IVA>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingTaxRate/ns11:taxIndicator)}</INDICADOR_IVA>
                <CODIGO_CONCEPTO>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:conceptCode)}</CODIGO_CONCEPTO>
                <CENTRO_COST>{fn:data($AppliedCustomerBillingChargeDetail/ns1:appliedCustomerBillingProductCharge/ns12:costCenter)}</CENTRO_COST>
            </ZPCS_ESTRUCT_DETDOC>              
            }
            
            {
            for $AppliedCustomerBillingProductAlteration in $CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:AppliedCustomerBillingRate/ns1:AppliedCustomerBillingProductAlteration
            return
             <ZPCS_ESTRUCT_COMPNC>
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:documentType)
                    then <TPO_DOC_COMPENSA>{fn:data($AppliedCustomerBillingProductAlteration/ns13:documentType)}</TPO_DOC_COMPENSA>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:referenceNumberForCompensation)
                    then <NRO_DOC_COMPENSA>{fn:data($AppliedCustomerBillingProductAlteration/ns13:referenceNumberForCompensation)}</NRO_DOC_COMPENSA>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:documentStatus)
                    then <POS_DOC_COMPENSA>{fn:data($AppliedCustomerBillingProductAlteration/ns13:documentStatus)}</POS_DOC_COMPENSA>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:amount)
                    then <MTO_DOC_COMPENSA>{fn:data($AppliedCustomerBillingProductAlteration/ns13:amount)}</MTO_DOC_COMPENSA>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:compensatedDocumentType)
                    then <TPO_DOC_COMPENSADO>{fn:data($AppliedCustomerBillingProductAlteration/ns13:compensatedDocumentType)}</TPO_DOC_COMPENSADO>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:compensatedDocumentNumber)
                    then <NRO_DOC_COMPENSADO>{fn:data($AppliedCustomerBillingProductAlteration/ns13:compensatedDocumentNumber)}</NRO_DOC_COMPENSADO>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:compensatedDocumentStatus)
                    then <POS_DOC_COMPENSADO>{fn:data($AppliedCustomerBillingProductAlteration/ns13:compensatedDocumentStatus)}</POS_DOC_COMPENSADO>
                    else ()
                }
                {
                    if ($AppliedCustomerBillingProductAlteration/ns13:compensatedAmount)
                    then <MTO_DOC_COMPENSADO>{fn:data($AppliedCustomerBillingProductAlteration/ns13:compensatedAmount)}</MTO_DOC_COMPENSADO>
                    else ()
                }
            </ZPCS_ESTRUCT_COMPNC>
            }
           
           
        </ZPCS_ESTRUCT_DOC>
    </ns2:MT_Facturacion_Req>
};

local:get_CHL-SAP-RMC-SAP-RMC_Creacion_LegacyREQ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ)
