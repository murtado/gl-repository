xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL-SAP-RMC-SAP-RMC/Creacion/v1";
(:: import schema at "../CSC/CHL-SAP-RMC-SAP-RMC_Creacion_v1_CSM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns6 = "http://www.entel.cl/EBO/AppliedCustomerBillingChargeDetail/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/CustomerBill/v1";

declare namespace ns8 = "http://www.entel.cl/EBO/AppliedCustomerBillingTaxRate/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductCharge/v1";

declare namespace ns13 = "http://www.entel.cl/EBO/BillingAccount/v1";

declare namespace ns9 = "http://www.entel.cl/EBO/Money/v1";

declare namespace ns12 = "http://www.entel.cl/EBO/CustomerAccount/v1";

declare namespace ns11 = "http://www.entel.cl/EBO/AuthenticationEntity/v1";

declare namespace ns10 = "http://www.entel.cl/EBO/AppliedCustomerBillingProductAlteration/v1";

declare namespace ns17 = "http://www.entel.cl/EBO/Skill/v1";

declare namespace ns16 = "http://www.entel.cl/EBO/IndividualName/v1";

declare namespace ns15 = "http://www.entel.cl/EBO/IndividualIdentification/v1";

declare namespace ns14 = "http://www.entel.cl/EBO/EmailContact/v1";

declare namespace ns19 = "http://www.entel.cl/EBO/PostalDeliveryAddress/v1";

declare namespace ns18 = "http://www.entel.cl/EBO/CustomerAccountBalance/v1";

declare namespace ns20 = "http://www.entel.cl/EBO/GeographicArea/v1";

declare namespace ns24 = "http://www.entel.cl/EBO/CustomerOrder/v1";

declare namespace ns23 = "http://www.entel.cl/EBO/CustomerBillingCycleSpecification/v1";

declare namespace ns22 = "http://www.entel.cl/EBO/FinancialChargeSpec/v1";

declare namespace ns21 = "http://www.entel.cl/EBO/StreetName/v1";

declare namespace ns28 = "http://www.entel.cl/EBO/LoyaltyTransaction/v1";

declare namespace ns27 = "http://www.entel.cl/EBO/ThirdPartyCollectionPM/v1";

declare namespace ns26 = "http://www.entel.cl/EBO/PartyIdentification/v1";

declare namespace ns25 = "http://www.entel.cl/EBO/OrganizationName/v1";

declare namespace ns29 = "http://www.entel.cl/EBO/Rate/v1";

declare variable $CHL-SAP-RMC-SAP-RMC_Creacion_REQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Creacion_REQ) ::) external;
declare variable $GetMappingRSP as element() external;
declare variable $AppliedCustomerBillingChargeDetail as element() external;
declare variable $AppliedCustomerBillingProductAlteration as element() external;

declare function local:func($CHL-SAP-RMC-SAP-RMC_Creacion_REQ as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Creacion_REQ) ::), 
                            $GetMappingRSP as element(),
                            $AppliedCustomerBillingChargeDetail as element(),
                            $AppliedCustomerBillingProductAlteration as element()) as element() (:: schema-element(ns1:CHL-SAP-RMC-SAP-RMC_Creacion_REQ) ::) {
    <ns1:CHL-SAP-RMC-SAP-RMC_Creacion_REQ>
      (: 
          The Request Header is not required at this point, as it is assumed to be saved on a local variable within the 
          RA Pipeline. This reduces the size of the $body variable, and avoids further XPATH queries to be performed.
          As it is a good practice to use positional XPATH, the element is maintained to avoid possible mistakes when applying
          that type of XPATH over the RA Request, after it was modified by this XQ
        :)
        <ns2:RequestHeader></ns2:RequestHeader>
        <ns1:Body>
            <ns1:CustomerBill>
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billComments1)
                    then <ns5:billComments1>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billComments1)}</ns5:billComments1>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billComments2)
                    then <ns5:billComments2>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billComments2)}</ns5:billComments2>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billDate)
                    then <ns5:billDate>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billDate)}</ns5:billDate>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billNo)
                    then <ns5:billNo>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billNo)}</ns5:billNo>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billNoOriginal)
                    then <ns5:billNoOriginal>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billNoOriginal)}</ns5:billNoOriginal>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billType)
                    then <ns5:billType>{fn:data($GetMappingRSP/*[1]/*[@entity="CustomerBill" and @field="billType"]/@dCode)}</ns5:billType>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billTypeAssociated)
                    then <ns5:billTypeAssociated>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:billTypeAssociated)}</ns5:billTypeAssociated>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:causalCode)
                    then <ns5:causalCode>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:causalCode)}</ns5:causalCode>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:debtType)
                    then <ns5:debtType>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:debtType)}</ns5:debtType>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:discountAmount)
                    then <ns5:discountAmount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:discountAmount)}</ns5:discountAmount>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:discountPercentage)
                    then <ns5:discountPercentage>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:discountPercentage)}</ns5:discountPercentage>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:exemptAmount)
                    then <ns5:exemptAmount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:exemptAmount)}</ns5:exemptAmount>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:generationFormat)
                    then <ns5:generationFormat>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:generationFormat)}</ns5:generationFormat>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:header)
                    then <ns5:header>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:header)}</ns5:header>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:idTransaction)
                    then <ns5:idTransaction>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:idTransaction)}</ns5:idTransaction>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:netAmount)
                    then <ns5:netAmount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:netAmount)}</ns5:netAmount>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:observation)
                    then <ns5:observation>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:observation)}</ns5:observation>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:operation)
                    then <ns5:operation>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:operation)}</ns5:operation>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:paymentDueDate)
                    then <ns5:paymentDueDate>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:paymentDueDate)}</ns5:paymentDueDate>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:presentIndicator)
                    then <ns5:presentIndicator>{fn:data($GetMappingRSP/*[1]/*[@entity="CustomerBill" and @field="presentIndicator"]/@dCode)}</ns5:presentIndicator>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:rechargeAmount)
                    then <ns5:rechargeAmount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:rechargeAmount)}</ns5:rechargeAmount>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:rechargePercentage)
                    then <ns5:rechargePercentage>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:rechargePercentage)}</ns5:rechargePercentage>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:totalAmount)
                    then <ns5:totalAmount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:totalAmount)}</ns5:totalAmount>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:totalTaxValue)
                    then <ns5:totalTaxValue>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:totalTaxValue)}</ns5:totalTaxValue>
                    else ()
                }
                {
                    if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:vendorId)
                    then <ns5:vendorId>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns5:vendorId)}</ns5:vendorId>
                    else ()
                }
                {$AppliedCustomerBillingChargeDetail/*}
                    
                <ns1:AppliedCustomerBillingRate>
                   {$AppliedCustomerBillingProductAlteration/*}
                </ns1:AppliedCustomerBillingRate>
                <ns1:AuthenticationEntity>
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:AuthenticationEntity/ns11:userName)
                        then <ns11:userName>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:AuthenticationEntity/ns11:userName)}</ns11:userName>
                        else ()
                    }
                </ns1:AuthenticationEntity>
                <ns1:CustomerAccount>
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns12:ID)
                        then <ns12:ID>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns12:ID)}</ns12:ID>
                        else ()
                    }
                    <ns12:customerType>{fn:data($GetMappingRSP/*[1]/*[@entity="CustomerAccount" and @field="customerType"]/@dCode)}</ns12:customerType>
                    <ns1:BillingAccount>
                        {
                            if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:BillingAccount/ns13:externalID)
                            then <ns13:externalID>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:BillingAccount/ns13:externalID)}</ns13:externalID>
                            else ()
                        }
                    </ns1:BillingAccount>
                    <ns1:Customer>
                        <ns1:EmailContact>
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:EmailContact/ns14:eMailAddress)
                                then <ns14:eMailAddress>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:EmailContact/ns14:eMailAddress)}</ns14:eMailAddress>
                                else ()
                            }
                        </ns1:EmailContact>
                        <ns1:IndividualIdentification>
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns15:number)
                                then <ns15:number>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns15:number)}</ns15:number>
                                else ()
                            }
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns15:type)
                                then <ns15:type>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns15:type)}</ns15:type>
                                else ()
                            }
                            <ns1:Individual>
                                <ns1:IndividualName>
                                    {
                                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns1:Individual/ns1:IndividualName/ns16:alias)
                                        then <ns16:alias>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns1:Individual/ns1:IndividualName/ns16:alias)}</ns16:alias>
                                        else ()
                                    }
                                    {
                                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns1:Individual/ns1:IndividualName/ns16:firstName)
                                        then <ns16:firstName>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns1:Individual/ns1:IndividualName/ns16:firstName)}</ns16:firstName>
                                        else ()
                                    }
                                    {
                                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns1:Individual/ns1:IndividualName/ns16:lastName)
                                        then <ns16:lastName>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:IndividualIdentification/ns1:Individual/ns1:IndividualName/ns16:lastName)}</ns16:lastName>
                                        else ()
                                    }
                                </ns1:IndividualName>
                            </ns1:Individual>
                        </ns1:IndividualIdentification>
                        <ns1:Skill>
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:Skill/ns17:skillSpecification)
                                then <ns17:skillSpecification>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:Customer/ns1:Skill/ns17:skillSpecification)}</ns17:skillSpecification>
                                else ()
                            }
                        </ns1:Skill>
                    </ns1:Customer>
                    <ns1:CustomerAccountBalance>
                        {
                            if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAccountBalance/ns18:contractAccount)
                            then <ns18:contractAccount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAccountBalance/ns18:contractAccount)}</ns18:contractAccount>
                            else ()
                        }
                    </ns1:CustomerAccountBalance>
                    <ns1:CustomerAddress>
                        <ns1:postalDeliveryAddress>
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns19:address)
                                then <ns19:address>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns19:address)}</ns19:address>
                                else ()
                            }
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns19:city)
                                then <ns19:city>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns19:city)}</ns19:city>
                                else ()
                            }
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns19:commune)
                                then <ns19:commune>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerAddress/ns1:postalDeliveryAddress/ns19:commune)}</ns19:commune>
                                else ()
                            }
                        </ns1:postalDeliveryAddress>
                    </ns1:CustomerAddress>
                    <ns1:CustomerComercialAddress>
                        <ns1:geographicArea>
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns20:city)
                                then <ns20:city>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns20:city)}</ns20:city>
                                else ()
                            }
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns20:commune)
                                then <ns20:commune>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns20:commune)}</ns20:commune>
                                else ()
                            }
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns20:neighborhood)
                                then <ns20:neighborhood>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:geographicArea/ns20:neighborhood)}</ns20:neighborhood>
                                else ()
                            }
                        </ns1:geographicArea>
                        <ns1:streetName>
                            {
                                if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:streetName/ns21:name)
                                then <ns21:name>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:CustomerComercialAddress/ns1:streetName/ns21:name)}</ns21:name>
                                else ()
                            }
                        </ns1:streetName>
                    </ns1:CustomerComercialAddress>
                    <ns1:FinancialChargeSpec>
                        <ns22:ID>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerAccount/ns1:FinancialChargeSpec/ns22:ID)}</ns22:ID>
                    </ns1:FinancialChargeSpec>
                </ns1:CustomerAccount>
                <ns1:CustomerBillingCycleSpecification>
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerBillingCycleSpecification/ns23:name)
                        then <ns23:name>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:CustomerBillingCycleSpecification/ns23:name)}</ns23:name>
                        else ()
                    }
                </ns1:CustomerBillingCycleSpecification>
                <ns1:IssuingCompany>
                    <ns1:CustomerOrder>
                        {
                            if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:CustomerOrder/ns24:ID)
                            then <ns24:ID>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:CustomerOrder/ns24:ID)}</ns24:ID>
                            else ()
                        }
                    </ns1:CustomerOrder>
                    <ns1:organizationName>
                        {
                            if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:organizationName/ns25:shortName)
                            then <ns25:shortName>{fn:data($GetMappingRSP/*[1]/*[@entity="OrganizationName" and @field="shortName"]/@dCode)}</ns25:shortName>
                            else ()
                        }
                        {
                            if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:organizationName/ns25:tradingName)
                            then <ns25:tradingName>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:organizationName/ns25:tradingName)}</ns25:tradingName>
                            else ()
                        }
                    </ns1:organizationName>
                    <ns1:partyIdentification>
                        {
                            if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:partyIdentification/ns26:number)
                            then <ns26:number>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:partyIdentification/ns26:number)}</ns26:number>
                            else ()
                        }
                    </ns1:partyIdentification>
                    <ns1:thirdPartyCollectionPM>
                        {
                            if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:thirdPartyCollectionPM/ns27:thirdPartyID)
                            then <ns27:thirdPartyID>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:IssuingCompany/ns1:thirdPartyCollectionPM/ns27:thirdPartyID)}</ns27:thirdPartyID>
                            else ()
                        }
                    </ns1:thirdPartyCollectionPM>
                </ns1:IssuingCompany>
                <ns1:LoyaltyTransaction>
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:LoyaltyTransaction/ns28:amount)
                        then <ns28:amount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:LoyaltyTransaction/ns28:amount)}</ns28:amount>
                        else ()
                    }
                </ns1:LoyaltyTransaction>
                <ns1:Rate>
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:Rate/ns29:date)
                        then <ns29:date>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:Rate/ns29:date)}</ns29:date>
                        else ()
                    }
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:Rate/ns29:rateValue)
                        then <ns29:rateValue>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:Rate/ns29:rateValue)}</ns29:rateValue>
                        else ()
                    }
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:Rate/ns29:type)
                        then <ns29:type>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:Rate/ns29:type)}</ns29:type>
                        else ()
                    }
                </ns1:Rate>
                <ns1:billAmount>
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:billAmount/ns9:amount)
                        then <ns9:amount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:billAmount/ns9:amount)}</ns9:amount>
                        else ()
                    }
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:billAmount/ns9:units)
                        then <ns9:units>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:billAmount/ns9:units)}</ns9:units>
                        else ()
                    }
                </ns1:billAmount>
                <ns1:unit>
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:unit/ns9:amount)
                        then <ns9:amount>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:unit/ns9:amount)}</ns9:amount>
                        else ()
                    }
                    {
                        if ($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:unit/ns9:units)
                        then <ns9:units>{fn:data($CHL-SAP-RMC-SAP-RMC_Creacion_REQ/ns1:Body/ns1:CustomerBill/ns1:unit/ns9:units)}</ns9:units>
                        else ()
                    }
                </ns1:unit>
            </ns1:CustomerBill>
        </ns1:Body>
    </ns1:CHL-SAP-RMC-SAP-RMC_Creacion_REQ>
};

local:func($CHL-SAP-RMC-SAP-RMC_Creacion_REQ, $GetMappingRSP,$AppliedCustomerBillingChargeDetail,$AppliedCustomerBillingProductAlteration)
