SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN	
		INSERT INTO ESB_ERROR_MAPPING(ID,ERROR_SOURCE,MODULE,SUB_MODULE,RAW_CODE,RAW_DESCRIPTION,CAN_ERR_ID,STATUS_ID,RCD_STATUS) values ( ESB_ERROR_MAPPING_SEQ.NEXTVAL,
																													( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),
																													'PIF', 
																													'UnsupportedOperation',
																													'99',
																													'La operacion solicitada no se encuentra disponible para este servicio, segun el Consumer informado en el Header',
																													( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10007' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE') AND DESCRIPTION = 'La operacion solicitada no se encuentra disponible para este servicio, segun el Consumer informado en el Header'),
																													( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR'),
																													'1');
	END;
	
END;
/

COMMIT;
