--------------------------------------------------------
--  DDL for Table ESB_MAPPING
--------------------------------------------------------
  
  
  ALTER TABLE "ESB_MAPPING" MODIFY
   (
      CONTEXT VARCHAR2(100 BYTE),
	  SOURCE_CODE VARCHAR2(250 BYTE), 
	  DESTINATION_CODE VARCHAR2(250 BYTE)	
   );
