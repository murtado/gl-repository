--------------------------------------------------------
--  DDL for Table ESB_MAPPING
--------------------------------------------------------
  
  
  ALTER TABLE "ESB_MAPPING" MODIFY
   (
      CONTEXT VARCHAR2(50 BYTE),
	  SOURCE_CODE VARCHAR2(50 BYTE), 
	  DESTINATION_CODE VARCHAR2(50 BYTE)	
   );
