xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SALESFORCENotification";
(:: import schema at "../XSD/SALESFORCE_Notification.xsd" ::)

declare variable $Notification_REQ as element() (:: schema-element(ns1:SALESFORCE_Notification_REQ) ::) external;

declare function local:func($Notification_REQ as element() (:: schema-element(ns1:SALESFORCE_Notification_REQ) ::)) as element() (:: schema-element(ns1:SALESFORCE_Notification_RSP) ::) {
    
    if(fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)='1') then
    (
      <ns1:SALESFORCE_Notification_RSP>
        <ns1:Body>
          <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
          <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
          <ns1:Notification>
            <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
            <ns1:NotifyCode>0</ns1:NotifyCode>
            <ns1:NotifyDescription>Notification Success</ns1:NotifyDescription>
          </ns1:Notification>
        </ns1:Body>
      </ns1:SALESFORCE_Notification_RSP>
    )
    else if (fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)='2') then
    (
          if(fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)='EMAIL') then
          (
            <ns1:SALESFORCE_Notification_RSP>
              <ns1:Body>
                <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
                <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
                <ns1:Notification>
                  <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
                  <ns1:NotifyCode>1</ns1:NotifyCode>
                  <ns1:NotifyDescription>Error EMAIL Delivery Notification</ns1:NotifyDescription>
                </ns1:Notification>
              </ns1:Body>
            </ns1:SALESFORCE_Notification_RSP>
          )
          
          else if(fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)='SMS') then
          (
            <ns1:SALESFORCE_Notification_RSP>
              <ns1:Body>
                <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
                <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
                <ns1:Notification>
                  <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
                  <ns1:NotifyCode>2</ns1:NotifyCode>
                  <ns1:NotifyDescription>Error SMS Sender</ns1:NotifyDescription>
                </ns1:Notification>
              </ns1:Body>
            </ns1:SALESFORCE_Notification_RSP>
          )
          
          else if(fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)='HOMEBANKING') then
          (
            <ns1:SALESFORCE_Notification_RSP>
              <ns1:Body>
                <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
                <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
                <ns1:Notification>
                  <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
                  <ns1:NotifyCode>3</ns1:NotifyCode>
                  <ns1:NotifyDescription>Error Delivery HOMEBANKING message</ns1:NotifyDescription>
                </ns1:Notification>
              </ns1:Body>
            </ns1:SALESFORCE_Notification_RSP>
          )
          else()
    )
    
    else if((fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)='3') and (fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)='EMAIL')) then
    (
      <ns1:SALESFORCE_Notification_RSP>
        <ns1:Body>
          <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
          <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
          <ns1:Notification>
            <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
            <ns1:NotifyCode>0</ns1:NotifyCode>
            <ns1:NotifyDescription>Notification Success</ns1:NotifyDescription>
          </ns1:Notification>
        </ns1:Body>
      </ns1:SALESFORCE_Notification_RSP>
    )
    
    else if((fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)='3') and (fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)='SMS')) then
    (
      <ns1:SALESFORCE_Notification_RSP>
        <ns1:Body>
          <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
          <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
          <ns1:Notification>
            <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
            <ns1:NotifyCode>2</ns1:NotifyCode>
            <ns1:NotifyDescription>Error SMS Sender</ns1:NotifyDescription>
          </ns1:Notification>
        </ns1:Body>
      </ns1:SALESFORCE_Notification_RSP>
    )
    
    else if((fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)='3') and (fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)='HOMEBANKING')) then
    (
      <ns1:SALESFORCE_Notification_RSP>
        <ns1:Body>
          <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
          <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
          <ns1:Notification>
            <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
            <ns1:NotifyCode>3</ns1:NotifyCode>
            <ns1:NotifyDescription>Error Delivery HOMEBANKING message</ns1:NotifyDescription>
          </ns1:Notification>
        </ns1:Body>
      </ns1:SALESFORCE_Notification_RSP>
    )
    else
    (
      <ns1:SALESFORCE_Notification_RSP>
        <ns1:Body>
          <ns1:TransferID>{fn:data($Notification_REQ/ns1:Body/ns1:TransferID)}</ns1:TransferID>
          <ns1:ClientID>{fn:data($Notification_REQ/ns1:Body/ns1:Client/ns1:ClientID)}</ns1:ClientID>
          <ns1:Notification>
            <ns1:NotifyChannel>{fn:data($Notification_REQ/ns1:Body/ns1:NotifyChannel)}</ns1:NotifyChannel>
            <ns1:NotifyCode>1</ns1:NotifyCode>
            <ns1:NotifyDescription>Error Delivery Notification</ns1:NotifyDescription>
          </ns1:Notification>
        </ns1:Body>
      </ns1:SALESFORCE_Notification_RSP>
    )
};

local:func($Notification_REQ)