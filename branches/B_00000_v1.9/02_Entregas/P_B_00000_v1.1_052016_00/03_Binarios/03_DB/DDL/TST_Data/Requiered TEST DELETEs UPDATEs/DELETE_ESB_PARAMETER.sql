SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN
	
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.error.IsEnabled.100.Consumer.del');
	END;
	
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.error.IsEnabled.100.Consumer.get');
	END;
	
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.error.IsEnabled.100.Consumer.put');
	END;
	
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.error.IsEnabled.100.Consumer.upd');
	END;
		
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.log.IsEnabled.100.Consumer.del');
	END;
	
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.log.IsEnabled.100.Consumer.get');
	END;
	
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.log.IsEnabled.100.Consumer.put');
	END;
	
	BEGIN
		DELETE FROM ESB_PARAMETER  WHERE KEY = ('Logger.log.IsEnabled.100.Consumer.upd');
	END;
	
	
		
END;
/

COMMIT;
