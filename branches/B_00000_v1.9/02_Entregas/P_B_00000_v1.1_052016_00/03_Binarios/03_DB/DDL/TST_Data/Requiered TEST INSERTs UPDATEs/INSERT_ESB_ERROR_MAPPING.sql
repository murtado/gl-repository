SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN	
		Insert into ESB_ERROR_MAPPING (ID,ERROR_SOURCE,MODULE,SUB_MODULE,RAW_CODE,RAW_DESCRIPTION,CAN_ERR_ID,STATUS_ID,RCD_STATUS) values (ESB_ERROR_MAPPING_SEQ.NEXTVAL,( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'CHL-SALESFORCE'),'CHL-SALESFORCE-CRM','ClientObtain','0','Error ok',( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '0'),( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'OK'),'1');
	END;
		
END;
/

COMMIT;
