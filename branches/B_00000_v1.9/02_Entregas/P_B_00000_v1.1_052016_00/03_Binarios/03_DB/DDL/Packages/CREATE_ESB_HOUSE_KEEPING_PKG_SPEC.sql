--------------------------------------------------------
--  DDL for Package ESB_HOUSE_KEEPING_PKG
--------------------------------------------------------
CREATE OR REPLACE PACKAGE "ESB_HOUSE_KEEPING_PKG" AS
  PROCEDURE truncateCorrelationManager(p_Days IN NUMBER, p_BKP IN NUMBER, p_Path IN VARCHAR2);
END ESB_HOUSE_KEEPING_PKG;

/