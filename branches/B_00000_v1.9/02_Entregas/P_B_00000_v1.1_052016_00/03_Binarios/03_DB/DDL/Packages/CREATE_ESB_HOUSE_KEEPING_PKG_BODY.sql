--------------------------------------------------------
--  DDL for Package Body ESB_HOUSE_KEEPING_PKG
--------------------------------------------------------
create or replace PACKAGE BODY ESB_HOUSE_KEEPING_PKG AS
  PROCEDURE truncateCorrelationManager(p_Days IN NUMBER, p_BKP IN NUMBER, p_Path IN VARCHAR2)
  AS
    CURSOR c_data_correlation IS
      SELECT * FROM ESB_CORRELATION ec
      WHERE ec.CREATIONDATE <= (trunc(sysdate) - p_Days);
      
    CURSOR c_data_correlation_group IS
      SELECT * FROM ESB_CORRELATION_GROUP ecg
      WHERE ecg.ID IN (SELECT ec.GROUP_INSTANCE FROM ESB_CORRELATION ec
                          WHERE ec.CREATIONDATE <= (trunc(sysdate) - p_Days));
    
    CURSOR c_data_correlation_instance IS
      SELECT * FROM ESB_CORRELATION_INSTANCE eci
      WHERE eci.ID IN (SELECT ec.MEMBER_INSTANCE FROM ESB_CORRELATION ec
                          WHERE ec.CREATIONDATE <= (trunc(sysdate) - p_Days));
    
    v_file  UTL_FILE.FILE_TYPE;
  BEGIN
    
    IF (p_BKP = 1) THEN
    
      EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY ESB_LOG_KEEPING_DIR AS '''||p_Path||'''';
    
      --Backup data from ESB_CORRELATION
      BEGIN
        v_file := UTL_FILE.FOPEN(location     => 'ESB_LOG_KEEPING_DIR',
                                 filename     => 'log_ESB_CORRELATION_'||to_char(sysdate,'YYYY-MM-DD')||'.txt',
                                 open_mode    => 'w',
                                 max_linesize => 32767);
        FOR cur_rec IN c_data_correlation LOOP
          UTL_FILE.PUT_LINE(v_file,
                            cur_rec.ID                || ',' ||
                            cur_rec.GROUP_INSTANCE    || ',' ||
                            cur_rec.MEMBER_INSTANCE   || ',' ||
                            cur_rec.TYPE_MEMBER       || ',' ||
                            cur_rec.CREATIONDATE      || ',' ||
                            cur_rec.RCD_STATUS);
        END LOOP;
        UTL_FILE.FCLOSE(v_file);
  
        EXCEPTION
          WHEN OTHERS THEN
            UTL_FILE.FCLOSE(v_file);
            RAISE;
      END;
    
      --Backup data from ESB_CORRELATION_GROUP
      BEGIN
        v_file := UTL_FILE.FOPEN(location     => 'ESB_LOG_KEEPING_DIR',
                                 filename     => 'log_ESB_CORRELATION_GROUP_'||to_char(sysdate,'YYYY-MM-DD')||'.txt',
                                 open_mode    => 'w',
                                 max_linesize => 32767);
        FOR cur_rec IN c_data_correlation_group LOOP
          UTL_FILE.PUT_LINE(v_file,
                            cur_rec.ID                || ',' ||
                            cur_rec.NAME              || ',' ||
                            cur_rec.REQUESTHEADER     || ',' ||
                            cur_rec.DESCRIPTION       || ',' ||
                            cur_rec.CREATIONDATE      || ',' ||
                            cur_rec.RCD_STATUS);
        END LOOP;
        UTL_FILE.FCLOSE(v_file);
  
        EXCEPTION
          WHEN OTHERS THEN
            UTL_FILE.FCLOSE(v_file);
            RAISE;
      END;
      
      --Backup data from ESB_CORRELATION_INSTANCE
      BEGIN
        v_file := UTL_FILE.FOPEN(location     => 'ESB_LOG_KEEPING_DIR',
                                 filename     => 'log_ESB_CORRELATION_INSTANCE_'||to_char(sysdate,'YYYY-MM-DD')||'.txt',
                                 open_mode    => 'w',
                                 max_linesize => 32767);
        FOR cur_rec IN c_data_correlation_instance LOOP
          UTL_FILE.PUT_LINE(v_file,
                            cur_rec.ID                || ',' ||
                            cur_rec.CORRELATION_ID    || ',' ||
                            cur_rec.MEMBER_NAME       || ',' ||
                            cur_rec.MEMBER_ADDRESS    || ',' ||
                            cur_rec.CREATIONDATE      || ',' ||
                            cur_rec.RCD_STATUS);
        END LOOP;
        UTL_FILE.FCLOSE(v_file);
  
        EXCEPTION
          WHEN OTHERS THEN
            UTL_FILE.FCLOSE(v_file);
            RAISE;
      END;
    END IF;
    
    --Truncate tables
    EXECUTE IMMEDIATE 'alter table ESB_CORRELATION disable constraint GROUP_INSTANCE_FK';
    EXECUTE IMMEDIATE 'alter table ESB_CORRELATION disable constraint MEMBER_INSTANCE_FK';
    
    DELETE FROM ESB_CORRELATION_INSTANCE eci
      WHERE eci.ID IN (SELECT ec.MEMBER_INSTANCE FROM ESB_CORRELATION ec 
                        WHERE ec.CREATIONDATE <= (trunc(sysdate) - p_Days));
                        
    DELETE FROM ESB_CORRELATION_GROUP ecg
      WHERE ecg.ID IN (SELECT ec.GROUP_INSTANCE FROM ESB_CORRELATION ec 
                        WHERE ec.CREATIONDATE <= (trunc(sysdate) - p_Days));
    
    DELETE FROM ESB_CORRELATION ec
      WHERE ec.CREATIONDATE <= (trunc(sysdate) - p_Days);
    
    EXECUTE IMMEDIATE 'alter table ESB_CORRELATION enable constraint GROUP_INSTANCE_FK';
    EXECUTE IMMEDIATE 'alter table ESB_CORRELATION enable constraint MEMBER_INSTANCE_FK';
    
    COMMIT;

  END truncateCorrelationManager;
END ESB_HOUSE_KEEPING_PKG;

/