SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN
	
	BEGIN	
		INSERT INTO ESB_MAPPING (SOURCE_SYSTEM,SOURCE_CODE,DESTINATION_SYSTEM,CONTEXT,DESTINATION_CODE,RCD_STATUS,ID,FIELD_ID) VALUES (( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),'National',( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'INT-AS400'),'','NAT','1',ESB_MAPPING_SEQ.NEXTVAL,(SELECT F.ID FROM ESB_CDM_FIELD F INNER JOIN ESB_CDM_ENTITY E ON F.ENTITY_ID = E.ID WHERE E.NAME = 'Client' AND F.NAME = 'Type'));
	END;	
	
END;
/

COMMIT;

