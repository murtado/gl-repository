SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		DELETE FROM  ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'CHL-SALESFORCE-CRM_ClientObtain_HTTPSOAP12') AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url') AND RCD_STATUS ='1';
	END;
			
	BEGIN
		DELETE FROM  ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'CHL-SALESFORCE-CRM_ClientObtain_HTTPSOAP12') AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction') AND RCD_STATUS ='1';
	END;	

	BEGIN
		DELETE FROM  ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'CHL-SALESFORCE-CRM_ClientDelete_HTTPSOAP12') AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url') AND RCD_STATUS ='1';
	END;
			
	BEGIN
		DELETE FROM  ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'CHL-SALESFORCE-CRM_ClientDelete_HTTPSOAP12') AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction') AND RCD_STATUS ='1';
	END;	
	
	BEGIN
		DELETE FROM  ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'INT-AS400-CLI_ClientDelete_HTTPSOAP11') AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url') AND RCD_STATUS ='1';
	END;	

	BEGIN
		DELETE FROM  ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = 'INT-AS400-CLI_ClientDelete_HTTPSOAP11') AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction') AND RCD_STATUS = '1';
	END;		
	
END;
/

COMMIT;