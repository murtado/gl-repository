import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000_v1.9'
wlstFileDomain='SOA'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")


scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+SOA_AdminServerListenAddress+":"+SOA_AdminServerListenPort


def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')


""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """
""" ------------------------ """
""" RetryManagerAdmissionQueue """
""" ------------------------ """
def rollbackJMSQueue__RetryManagerAdmissionQueue():
	global beanName
	
	fName = 'rollbackJMSQueue__RetryManagerAdmissionQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	
	debugFile.write('\n')
	debugFile.write('\nPaso0 : ' + fName)
	
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionQueue'))
	debugFile.write('\n')
	debugFile.write('\nPaso2 : ' + fName)
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """


""" ------------------------ """
""" RetryManagerQueue """
""" ------------------------ """
def rollbackJMSQueue__RetryManagerQueue():
	global beanName
	
	fName = 'rollbackJMSQueue__RetryManagerQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	
	debugFile.write('\n')
	debugFile.write('\nPaso0 : ' + fName)
	
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerQueue'))
	debugFile.write('\n')
	debugFile.write('\nPaso2 : ' + fName)
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """



""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	# JMSAdapter Connection Factories
	rollbackJMSQueue__RetryManagerAdmissionQueue()
	rollbackJMSQueue__RetryManagerQueue()
	
	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')