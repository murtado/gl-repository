import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000_v1.9'
wlstFileDomain='SOA'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")


scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+SOA_AdminServerListenAddress+":"+SOA_AdminServerListenPort

#Local JMS Artifacts
SOA_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+SOA_ManagedServerListenURLs+';java.naming.security.principal='+SOA_ConUsr+';java.naming.security.credentials='+SOA_ConPsw

#------------------------------------------
#Shared JMS Artifacts (with OSB Domain)
#------------------------------------------
if (isTwoDomains=='true'):
	sharedUsr=OSB_SOA_crossDomainUsr
	sharedPsw=OSB_SOA_crossDomainPsw
else:
	sharedUsr=OSB_ConUsr
	sharedPsw=OSB_ConPsw
	
OSB_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+OSB_ManagedServerListenURLs+';java.naming.security.principal='+sharedUsr+';java.naming.security.credentials='+sharedPsw
#------------------------------------------


def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

 		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ------------------------ """
""" RetryManagerAdmissionQueue """
""" ------------------------ """
def createJMSQueue__RetryManagerAdmissionQueue():
	global fName
	
	fName = 'createJMSQueue__RetryManagerAdmissionQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RetryManagerAdmissionQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/RetryManagerAdmissionQueue')
	cmo.setDefaultTargetingEnabled(true)
		
	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RetryManagerAdmissionErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionErrorQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/RetryManagerAdmissionErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	
	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionQueue/DeliveryFailureParams/RetryManagerAdmissionQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionQueue/DeliveryParamsOverrides/RetryManagerAdmissionQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionQueue/DeliveryFailureParams/RetryManagerAdmissionQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerAdmissionErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')


""" ------------------ """



""" ------------------------ """
""" RetryManagerQueue """
""" ------------------------ """
def createJMSQueue__RetryManagerQueue():
	global fName
	
	fName = 'createJMSQueue__RetryManagerQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RetryManagerQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/RetryManagerQueue')
	cmo.setDefaultTargetingEnabled(true)
		
	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('RetryManagerErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerErrorQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/RetryManagerErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	
	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerQueue/DeliveryFailureParams/RetryManagerQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerQueue/DeliveryParamsOverrides/RetryManagerQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerQueue/DeliveryFailureParams/RetryManagerQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/RetryManagerErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')

""" ------------------ """


""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """
	
	
	# JMS Queues
	createJMSQueue__RetryManagerAdmissionQueue()
	createJMSQueue__RetryManagerQueue()



	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')
