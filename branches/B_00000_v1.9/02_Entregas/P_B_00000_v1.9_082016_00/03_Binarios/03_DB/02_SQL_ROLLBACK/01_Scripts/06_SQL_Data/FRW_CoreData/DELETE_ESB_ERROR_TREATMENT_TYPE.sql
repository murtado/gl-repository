SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	--Inserciones de tratamientos, NotificationTraetment, y RetryTreatment
	
	BEGIN
		DELETE FROM ESB_ERROR_TREATMENT_TYPE WHERE NAME = 'Retry' AND ID = 2;
	END;
	
END;
/

COMMIT;
