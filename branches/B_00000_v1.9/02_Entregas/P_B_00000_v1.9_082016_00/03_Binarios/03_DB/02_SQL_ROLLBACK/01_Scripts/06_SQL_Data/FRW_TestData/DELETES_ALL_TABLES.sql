SPOOL salida_inserts_all_tables.txt
@DELETE_ESB_CAPABILITY_DETAILS.sql
@DELETE_ESB_SERVICE_DETAILS.sql
@DELETE_ESB_RETRYMANAGER_CONFIG.sql
@DELETE_ESB_ERROR_DIAGNOSIS.sql
@DELETE_ESB_ERROR_TREATMENT_PLAN.sql
@DELETE_ESB_ERROR_THERAPY.sql
@DELETE_ESB_MAPPING.sql
@DELETE_ESB_PARAMETER.sql
@DELETE_ESB_CONSUMER.sql
@DELETE_ESB_SYSTEM.sql
SPOOL OFF;