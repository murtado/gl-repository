create or replace 
PACKAGE BODY ESB_CONVERSATION_MANAGER_PKG
AS

---------------------------------------------------------------------------------------------------
	PROCEDURE getConversationStatus(

		p_CONVERSATION_ID 		IN VARCHAR2,
		p_STATUS				OUT VARCHAR2,
		p_RSP_MSG 			    OUT CLOB,
		p_CAN_ERROR_ID 			OUT NUMBER) IS

			BEGIN
      
				p_STATUS := '';
				p_RSP_MSG := null;
				p_CAN_ERROR_ID := '';
     		
				SELECT CS.STATUS, CS.RSP_MSG, CS.CAN_ERR_ID INTO p_STATUS, p_RSP_MSG, p_CAN_ERROR_ID FROM ESB_CONVERSATION_STATUS CS
				WHERE CONVERSATION_ID = p_CONVERSATION_ID;
			
			EXCEPTION
				WHEN NO_DATA_FOUND THEN 
					p_STATUS := 'NO SE ENCONTRARON DATOS. '||SQLERRM;
				WHEN OTHERS THEN 
					p_STATUS := 'ERROR AL CONSULTAR EN TABLA ESB_CONVERSATION_STATUS. '||SQLERRM;						
				
	END getConversationStatus;

---------------------------------------------------------------------------------------------------
	PROCEDURE updateConversationStatus(

		p_CONVERSATION_ID 	  IN VARCHAR2, 
		p_STATUS 			      	IN VARCHAR2,
		p_RSP_MSG 			    	IN CLOB,
		p_CAN_ERROR_CODE  	 	IN NUMBER,
		p_CAN_ERROR_TYPE     	IN VARCHAR2,
		p_UPDATE_COMPONENT		IN VARCHAR2,
		p_UPDATE_OPERATION		IN VARCHAR2,
    
		p_RESULT_CODE			OUT VARCHAR2,
		p_RESULT_DESCRIPTION 	OUT VARCHAR2) IS
    
    status VARCHAR2(30);
    
		BEGIN
       
      SELECT CS.STATUS INTO status FROM ESB_CONVERSATION_STATUS CS
      WHERE CONVERSATION_ID = p_CONVERSATION_ID;
      
      IF NOT(status IS NULL) THEN 
          UPDATE esb_conversation_status CS SET cs.status = p_status, 
                              cs.rsp_msg = p_rsp_msg, 
                              cs.can_err_id = (select CE.ID from esb_canonical_error CE  INNER JOIN esb_canonical_error_type CET ON CET.ID = CE.TYPE_ID
                                       where CE.CODE = p_CAN_ERROR_CODE AND CET.TYPE = p_CAN_ERROR_TYPE),
                              cs.update_component = p_update_component, 
                              cs.update_operation = p_update_operation, 
                              cs.update_timestamp = TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF') 
                             
                              WHERE cs.conversation_id = p_conversation_id
    
          RETURNING cs.conversation_id INTO p_RESULT_CODE;
          		
     END IF;
     
		EXCEPTION    
    
      WHEN NO_DATA_FOUND then
      
       insert into esb_conversation_status (CONVERSATION_ID, STATUS, RSP_MSG, CAN_ERR_ID, CREATION_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_COMPONENT, UPDATE_OPERATION)
       values (p_CONVERSATION_ID, p_STATUS, p_RSP_MSG, (select CE.ID from esb_canonical_error CE  INNER JOIN esb_canonical_error_type CET ON CET.ID = CE.TYPE_ID
                                       where CE.CODE = p_CAN_ERROR_CODE AND CET.TYPE = p_CAN_ERROR_TYPE), TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF') ,
                                       TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF') , p_UPDATE_COMPONENT, p_UPDATE_OPERATION );
                          
	  WHEN OTHERS THEN 
			p_RESULT_CODE := '-99';
			p_RESULT_DESCRIPTION := 'ERROR AL INSERTAR EN TABLA ESB_CONVERSATION_STATUS. '||SQLERRM;	
    
    END updateConversationStatus;
---------------------------------------------------------------------------------------------------    
PROCEDURE putConversationStatus(P_CONVERSATION_ID IN VARCHAR2, p_RESULT_CODE OUT VARCHAR2, p_RESULT_DESCRIPTION OUT VARCHAR2 )
IS

status VARCHAR2(30);
duplicate_transaction EXCEPTION;

BEGIN

   SELECT CS.STATUS INTO status FROM ESB_CONVERSATION_STATUS CS
   WHERE CONVERSATION_ID = p_CONVERSATION_ID;
   
   IF NOT(status IS NULL) THEN RAISE duplicate_transaction;
   END IF;

    
EXCEPTION 
  WHEN NO_DATA_FOUND THEN 
      INSERT INTO ESB_CONVERSATION_STATUS ( CONVERSATION_id, STATUS, CAN_ERR_ID, CREATION_TIMESTAMP,UPDATE_TIMESTAMP )
      VALUES ( P_CONVERSATION_ID,'PENDING',0,TO_TIMESTAMP (SYSDATE, 'DD-Mon-RR HH24:MI:SS.FF'),null) ;
      COMMIT;
	
  WHEN duplicate_transaction THEN
      p_RESULT_CODE := '-99';
      p_RESULT_DESCRIPTION := 'ERROR AL INSERTAR EN TABLA ESB_CONVERSATION_STATUS. ' ||SQLERRM; 
  WHEN OTHERS THEN 
			p_RESULT_CODE := '-99';
			p_RESULT_DESCRIPTION := 'ERROR AL INSERTAR EN TABLA ESB_CONVERSATION_STATUS. '||SQLERRM;		
	
END putConversationStatus;
---------------------------------------------------------------------------------------------------  
PROCEDURE getConsumerCallbackURL(
    p_SYSTEM_CODE 		    IN VARCHAR2, 
    p_COUNTRY_CODE        IN VARCHAR2,
    p_ENTERPRISE_CODE     IN VARCHAR2,
    p_SERVICE_CODE     IN VARCHAR2,
	p_CAPABILITY_NAME     IN VARCHAR2,
    p_TRANSPORT           OUT VARCHAR2,
    p_CALLBACK_URL        OUT VARCHAR2,
    p_SOAP_ACTION         OUT VARCHAR2,
		p_RESULT_CODE 			  OUT VARCHAR2,
		p_RESULT_DESCRIPTION 	OUT VARCHAR2) AS
       
    p_CONSUMER_ID NUMBER;
	p_CAPABILITY_CODE VARCHAR2(30);
    
    BEGIN
    
    
    existsConsumer(p_SYSTEM_CODE, p_COUNTRY_CODE, p_ENTERPRISE_CODE, p_CONSUMER_ID);
    
    IF (p_CONSUMER_ID != -1) THEN
    
      getCapabilityCode(p_SERVICE_CODE, p_CAPABILITY_NAME, p_CAPABILITY_CODE);
	  
	  IF (p_CAPABILITY_CODE != '-1') THEN 
        getConsumerCallbackURLWithCap(p_CONSUMER_ID, p_CAPABILITY_CODE, p_TRANSPORT, p_CALLBACK_URL, p_SOAP_ACTION, p_RESULT_CODE, p_RESULT_DESCRIPTION);
      ELSE
        getConsumerCallbackURLLessCap(p_CONSUMER_ID, p_TRANSPORT, p_CALLBACK_URL, p_RESULT_CODE, p_RESULT_DESCRIPTION);
      END IF;
    
    ELSE 
      
        p_RESULT_CODE := '-1';
        p_RESULT_DESCRIPTION := 'CONSUMIDOR NO REGISTRADO. '  || SQLERRM;
    
    END IF;
    
    END;
    
    
    
---------------------------------------------------------------------------------------------------

PROCEDURE getCapabilityCode(
	p_SERVICE_CODE		IN VARCHAR2, 
	p_CAPABILITY_NAME	IN VARCHAR2, 
	p_CAPABILITY_CODE	OUT VARCHAR2) AS
	
	BEGIN
	
		SELECT EC.CODE INTO p_CAPABILITY_CODE FROM ESB_CAPABILITY EC
		WHERE
			EC.SERVICE_ID = (SELECT ID FROM esb_service WHERE CODE = p_SERVICE_CODE)
		AND
			EC.NAME = p_CAPABILITY_NAME;
			
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			p_CAPABILITY_CODE := '-1';
	END;

---------------------------------------------------------------------------------------------------
  
PROCEDURE getConsumerCallbackURLWithCap(
    p_CONSUMER_ID         IN NUMBER,
    p_CAPABILITY_CODE     IN VARCHAR2,
    p_TRANSPORT           OUT VARCHAR2,
    p_CALLBACK_URL        OUT VARCHAR2,
    p_SOAP_ACTION         OUT VARCHAR2,
		p_RESULT_CODE 			  OUT VARCHAR2,
		p_RESULT_DESCRIPTION 	OUT VARCHAR2) AS
    
    BEGIN
    
        getSOAPAction(p_CAPABILITY_CODE, p_SOAP_ACTION);
    
        SELECT ECCD.DETAIL_CONTENT INTO p_TRANSPORT FROM ESB_CONSUMER_CAP_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (SELECT ID FROM esb_consumer_cap_details_type WHERE NAME = 'TRANSPORT' )
        AND
          ECCD.consumer_id = p_CONSUMER_ID
        AND 
        ECCD.capability_id = (SELECT ID FROM esb_capability WHERE CODE = p_CAPABILITY_CODE );

        
        SELECT ECCD.DETAIL_CONTENT INTO p_CALLBACK_URL FROM ESB_CONSUMER_CAP_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (SELECT ID FROM esb_consumer_cap_details_type WHERE NAME = 'CALLBACK_URL' )
        AND
          ECCD.consumer_id = p_CONSUMER_ID
        AND 
          ECCD.capability_id = (SELECT ID FROM esb_capability WHERE CODE = p_CAPABILITY_CODE);
        
      p_RESULT_CODE := '0';
      p_RESULT_DESCRIPTION := 'Ejecucion exitosa.';
      
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        getConsumerCallbackURLLessCap(p_CONSUMER_ID, p_TRANSPORT, p_CALLBACK_URL, p_RESULT_CODE, p_RESULT_DESCRIPTION);
        
       WHEN OTHERS THEN
        p_RESULT_CODE := '-98';
        p_RESULT_DESCRIPTION := 'SE HA PRODUCIDO UN ERROR EN LA BUSQUEDA DE LA URI. ' || SQLERRM;
    
    END;

---------------------------------------------------------------------------------------------------  
PROCEDURE getSOAPAction(
    p_CAPABILITY_CODE     IN VARCHAR2,
    p_SOAP_ACTION         OUT VARCHAR2) AS
    
    BEGIN
    
      SELECT DETAIL_CONTENT INTO p_SOAP_ACTION FROM ESB_CAPABILITY_DETAILS 
      WHERE
          CAPABILITY_ID = (SELECT ID FROM esb_capability WHERE CODE = p_CAPABILITY_CODE)
      AND
          DETAIL_TYPE_ID = (SELECT ID FROM ESB_CAPABILITY_DETAILS_TYPE WHERE DESCRIPTION = 'SOAP Action');
  
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_SOAP_ACTION := '-1';
        
    END;

    
---------------------------------------------------------------------------------------------------  
PROCEDURE getConsumerCallbackURLLessCap(
    p_CONSUMER_ID         IN NUMBER,
    p_TRANSPORT           OUT VARCHAR2,
    p_CALLBACK_URL        OUT VARCHAR2,
		p_RESULT_CODE 			  OUT VARCHAR2,
		p_RESULT_DESCRIPTION 	OUT VARCHAR2) AS
    
    BEGIN
    
        SELECT ECCD.DETAIL_CONTENT INTO p_TRANSPORT FROM ESB_CONSUMER_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (SELECT ID FROM esb_consumer_details_type WHERE NAME = 'TRANSPORT' )
        AND
          ECCD.consumer_id = p_CONSUMER_ID;

        
        SELECT ECCD.DETAIL_CONTENT INTO p_CALLBACK_URL FROM ESB_CONSUMER_DETAILS ECCD
        WHERE
          ECCD.DETAIL_TYPE_ID = (SELECT ID FROM esb_consumer_details_type WHERE NAME = 'URL' )
        AND
          ECCD.consumer_id = p_CONSUMER_ID;
        
      p_RESULT_CODE := '0';
      p_RESULT_DESCRIPTION := 'Ejecucion exitosa.';
      
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_RESULT_CODE := '-99';
        p_RESULT_DESCRIPTION := 'NO SE HAN ENCONTRADO DATOS. '  || SQLERRM;
        
       WHEN OTHERS THEN
        p_RESULT_CODE := '-98';
        p_RESULT_DESCRIPTION := 'SE HA PRODUCIDO UN ERROR EN LA BUSQUEDA DE LA URI. ' || SQLERRM;
    
    END;

---------------------------------------------------------------------------------------------------

  PROCEDURE existsConsumer(
    p_SYSTEM_CODE 		    IN VARCHAR2, 
    p_COUNTRY_CODE        IN VARCHAR2,
    p_ENTERPRISE_CODE     IN VARCHAR2,
    p_CONSUMER_ID OUT NUMBER
  )AS
  
  BEGIN
          SELECT ID INTO p_CONSUMER_ID FROM esb_consumer WHERE ESB_CONSUMER.syscode = (SELECT ID FROM esb_system WHERE CODE = p_SYSTEM_CODE) 
                                        AND ESB_CONSUMER.country_id = (SELECT ID FROM esb_country WHERE CODE = p_COUNTRY_CODE)
                                        AND ESB_CONSUMER.ent_code = (SELECT ID FROM esb_enterprise WHERE CODE = p_ENTERPRISE_CODE);
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
          p_CONSUMER_ID := -1;
          
  END;

---------------------------------------------------------------------------------------------------

    PROCEDURE getSequenceStatusConversation(
    p_CONVERSATION_ID 		 IN VARCHAR2,
    p_CONVERSATION_STATUS	 OUT VARCHAR2,
		p_SEQ_STATUS 				   OUT NUMBER)IS
    
	P_RSP_MSG CLOB;
	P_CAN_ERROR_ID NUMBER;
    
    BEGIN
      
      getConversationStatus(p_CONVERSATION_ID, p_CONVERSATION_STATUS, P_RSP_MSG, P_CAN_ERROR_ID);
      getSequenceStatus(p_CONVERSATION_ID, p_SEQ_STATUS);
        
    END;

---------------------------------------------------------------------------------------------------

PROCEDURE getSequenceStatus(
    p_CONVERSATION_ID 		 IN VARCHAR2,
    p_SEQ_STATUS	 OUT NUMBER) IS
    
    BEGIN
      
      SELECT SEQUENCE INTO p_SEQ_STATUS FROM ESB_CONVERSATION WHERE CONVERSATION_ID = p_CONVERSATION_ID;
      
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_SEQ_STATUS := -1;

    END;


END ESB_CONVERSATION_MANAGER_PKG;
/