SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN	

		DELETE FROM ESB_ERROR_MAPPING
		WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
		MODULE = 'MessageManager' AND 
		SUB_MODULE = 'checkIN' AND
		RAW_CODE = '100' AND
		RAW_DESCRIPTION = 'Mensaje no soportado' AND
		CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10013' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE') AND DESCRIPTION = 'Unsupported Message') AND
		STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR');
		
	END;
	
	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'ConversationManager' AND
																													SUB_MODULE = 'getInfo' AND
																													RAW_CODE = '-1' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10014' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE') AND DESCRIPTION = 'No se encontraron datos para la Conversacion enviada por parametro') AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1';
	END;

	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'ConversationManager' AND
																													SUB_MODULE = 'getInfo' AND
																													RAW_CODE = '-2' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '50007' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWCF') AND DESCRIPTION = 'El tipo de a Conversacion no se corresponde con ningun tipo de Transaction Owner definido') AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1';
	END;

	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'ConversationManager' AND
																													SUB_MODULE = 'getInfo' AND
																													RAW_CODE = '-3' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '50008' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWCF') AND DESCRIPTION = 'La Transacción asociada no se corresponde con ningun tipo de Transaction Instance definido') AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1';
	END;
	
END;
/

COMMIT;