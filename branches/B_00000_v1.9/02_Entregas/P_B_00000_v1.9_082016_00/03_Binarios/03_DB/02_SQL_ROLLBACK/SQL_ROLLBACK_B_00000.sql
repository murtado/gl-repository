SPOOL salida_DELETE_ALL_DB.txt

--------------------------- DATA ------------------------
/* 
--> Requiered INSERTS for the Service Templates to work.
	The service templates are used to test in the framework
	within a real functional context. 
*/

@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_CAPABILITY_DETAILS.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_SERVICE_DETAILS.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_RETRYMANAGER_CONFIG.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_ERROR_DIAGNOSIS.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_ERROR_TREATMENT_PLAN.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_ERROR_THERAPY.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_MAPPING.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_PARAMETER.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_CONSUMER.sql
@.\01_Scripts\06_SQL_Data\FRW_TestData\DELETE_ESB_SYSTEM.sql


--> Requiered INSERTS for the Framework Components to work.

@.\01_Scripts\06_SQL_Data\FRW_CoreData\DELETE_ESB_ERROR_TREATMENT_TYPE.sql
@.\01_Scripts\06_SQL_Data\FRW_CoreData\DELETE_ESB_CAPABILITY_DETAILS_TYPE.sql
@.\01_Scripts\06_SQL_Data\FRW_CoreData\DELETE_ESB_SERVICE_DETAILS_TYPE.sql
@.\01_Scripts\06_SQL_Data\FRW_CoreData\DELETE_ESB_ERROR_MAPPING.sql
@.\01_Scripts\06_SQL_Data\FRW_CoreData\DELETE_ESB_CANONICAL_ERROR.sql

---------------------------------------------------------

------------------------- TRIGGERS ----------------------
@.\01_Scripts\05_SQL_Triggers\ALTER_ESB_ERROR_CONV_STATUS.sql
---------------------------------------------------------

------------------------- PACKAGES ----------------------

@.\01_Scripts\04_SQL_Packages\ROLLBACK_ESB_LOGGERMANAGER_PKG_BODY.sql
@.\01_Scripts\04_SQL_Packages\ROLLBACK_ESB_MESSAGEMANAGER_PKG_SPEC.sql
@.\01_Scripts\04_SQL_Packages\ROLLBACK_ESB_MESSAGEMANAGER_PKG_BODY.sql
@.\01_Scripts\04_SQL_Packages\ROLLBACK_ESB_RETRY_MANAGER_PKG.sql
@.\01_Scripts\04_SQL_Packages\ROLLBACK_ESB_CONVERSATION_MANAGER_PKG_SPEC.sql
@.\01_Scripts\04_SQL_Packages\ROLLBACK_ESB_CONVERSATION_MANAGER_PKG_BODY.sql

---------------------------------------------------------

------------------------- SEQUENCES ----------------------
@.\01_Scripts\03_SQL_Sequences\DROP_ESB_RETRY_NOTIFICATION_SEQ.sql
----------------------------------------------------------

------------------------- TABLES -------------------------
@.\01_Scripts\01_SQL_Tables\DROP_ESB_RETRY_NOTIFICATION.sql
@.\01_Scripts\01_SQL_Tables\ALTER_ESB_MESSAGE_TRANSACTION.sql
@.\01_Scripts\01_SQL_Tables\ALTER_ESB_TRACE.sql
----------------------------------------------------------

SPOOL OFF;

