SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		INSERT INTO ESB_MAPPING (SOURCE_SYSTEM,SOURCE_CODE,DESTINATION_SYSTEM,CONTEXT,DESTINATION_CODE,RCD_STATUS,ID,FIELD_ID) 
		VALUES (( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),'0',( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'CR0002'),'CR0002@del','00','1', ESB_MAPPING_SEQ.NEXTVAL,(SELECT F.ID FROM ESB_CDM_FIELD F INNER JOIN ESB_CDM_ENTITY E ON F.ENTITY_ID = E.ID WHERE E.NAME = 'CanonicalError' AND F.NAME = 'code'));
	END;
	BEGIN
	
		INSERT INTO ESB_MAPPING (SOURCE_SYSTEM,SOURCE_CODE,DESTINATION_SYSTEM,CONTEXT,DESTINATION_CODE,RCD_STATUS,ID,FIELD_ID) 
		VALUES (( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),'-1',( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'CR0002'),'CR0002@del','999','1', ESB_MAPPING_SEQ.NEXTVAL,(SELECT F.ID FROM ESB_CDM_FIELD F INNER JOIN ESB_CDM_ENTITY E ON F.ENTITY_ID = E.ID WHERE E.NAME = 'CanonicalError' AND F.NAME = 'code'));
	END;
	BEGIN	
		INSERT INTO ESB_MAPPING (SOURCE_SYSTEM,SOURCE_CODE,DESTINATION_SYSTEM,CONTEXT,DESTINATION_CODE,RCD_STATUS,ID,FIELD_ID) 
		VALUES (( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),'161',( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'CR0002'),'CR0002@del','9161','1', ESB_MAPPING_SEQ.NEXTVAL,(SELECT F.ID FROM ESB_CDM_FIELD F INNER JOIN ESB_CDM_ENTITY E ON F.ENTITY_ID = E.ID WHERE E.NAME = 'CanonicalError' AND F.NAME = 'code'));
	END;
	
END;
/

COMMIT;

