DROP TABLE ESB_AUDIT CASCADE CONSTRAINTS;
DROP TABLE ESB_AUDIT_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_CANONICAL_ERROR CASCADE CONSTRAINTS;
DROP TABLE ESB_CANONICAL_ERROR_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_CAPABILITY CASCADE CONSTRAINTS;
DROP TABLE ESB_CAPABILITY_CHECK CASCADE CONSTRAINTS;
DROP TABLE ESB_CDM_ENTITY CASCADE CONSTRAINTS;
DROP TABLE ESB_CDM_FIELD CASCADE CONSTRAINTS;
DROP TABLE ESB_CHANNEL CASCADE CONSTRAINTS;
DROP TABLE ESB_CHECK_CONFIG CASCADE CONSTRAINTS;
DROP TABLE ESB_CHECK_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_CONFIG CASCADE CONSTRAINTS;
DROP TABLE ESB_CONFIG_LIST CASCADE CONSTRAINTS;
DROP TABLE ESB_CONFIG_PROPERTY CASCADE CONSTRAINTS;
DROP TABLE ESB_CONSUMER_DETAILS CASCADE CONSTRAINTS;
DROP TABLE ESB_CONSUMER_CAP_DETAILS CASCADE CONSTRAINTS;
DROP TABLE ESB_CONSUMER CASCADE CONSTRAINTS;
DROP TABLE ESB_CONSUMER_DETAILS_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_CONSUMER_CAP_DETAILS_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_CONVERSATION CASCADE CONSTRAINTS;
DROP TABLE ESB_CONVERSATION_STATUS CASCADE CONSTRAINTS;
DROP TABLE ESB_CORRELATION CASCADE CONSTRAINTS;
DROP TABLE ESB_CORRELATION_GROUP CASCADE CONSTRAINTS;
DROP TABLE ESB_CORRELATION_INSTANCE CASCADE CONSTRAINTS;
DROP TABLE ESB_CORRELATION_MEMBER_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_COUNTRY CASCADE CONSTRAINTS;
DROP TABLE ESB_DEFAULT_CHECK CASCADE CONSTRAINTS;
DROP TABLE ESB_ENDPOINT CASCADE CONSTRAINTS;
DROP TABLE ESB_ENDPOINT_TRANSPORT CASCADE CONSTRAINTS;
DROP TABLE ESB_ENTERPRISE CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_DIAGNOSIS CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_DISPATCHER CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_HOSPITAL CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_THERAPY CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_TREATMENT_PLAN CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_TREATMENT_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_CONVERSATION CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_DERIVATION_CONF CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_MAPPING CASCADE CONSTRAINTS;
DROP TABLE ESB_ERROR_STATUS_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_LOG CASCADE CONSTRAINTS;
DROP TABLE ESB_LOG_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_MAPPING CASCADE CONSTRAINTS;
DROP TABLE ESB_MESSAGE_TRANSACTION CASCADE CONSTRAINTS;
DROP TABLE ESB_PARAMETER CASCADE CONSTRAINTS;
DROP TABLE ESB_SERVICE CASCADE CONSTRAINTS;
DROP TABLE ESB_SEVERITY CASCADE CONSTRAINTS;
DROP TABLE ESB_SYSTEM CASCADE CONSTRAINTS;
DROP TABLE ESB_SYSTEM_API CASCADE CONSTRAINTS;
DROP TABLE ESB_SYSTEM_API_OPERATION CASCADE CONSTRAINTS;
DROP TABLE ESB_TRACE CASCADE CONSTRAINTS;
DROP TABLE ESB_TRANSACTION_CHECK CASCADE CONSTRAINTS;

--------------------------------------------------------
--  RETRYMANAGER
--------------------------------------------------------

DROP TABLE ESB_RETRYMANAGER_HISTORY CASCADE CONSTRAINTS;
DROP TABLE ESB_RETRYMANAGER CASCADE CONSTRAINTS;
DROP TABLE ESB_RETRYMANAGER_CONFIG CASCADE CONSTRAINTS;
DROP TABLE ESB_RETRYMANAGER_HISTORY CASCADE CONSTRAINTS;
DROP TABLE ESB_RETRYMANAGER CASCADE CONSTRAINTS;
DROP TABLE ESB_RETRY_NOTIFICATION CASCADE CONSTRAINTS;
DROP TABLE ESB_SERVICE_DETAILS CASCADE CONSTRAINTS;
DROP TABLE ESB_CAPABILITY_DETAILS CASCADE CONSTRAINTS;
DROP TABLE ESB_SERVICE_DETAILS_TYPE CASCADE CONSTRAINTS;
DROP TABLE ESB_CAPABILITY_DETAILS_TYPE CASCADE CONSTRAINTS;
