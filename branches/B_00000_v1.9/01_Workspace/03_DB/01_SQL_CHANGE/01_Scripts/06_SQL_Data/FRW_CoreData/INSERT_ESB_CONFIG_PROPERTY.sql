SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'soapAction','SOAPAction, para 1.1 o 1.2','1');
	END;
	
	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'url','URL for service','1');
	END;
	
	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'path','path','1');
	END;
	
	BEGIN
		Insert into ESB_CONFIG_PROPERTY (ID,NAME,DESCRIPTION,RCD_STATUS) values (ESB_CONFIG_PROPERTY_SEQ.NEXTVAL,'legalType','legalType','1');
	END;

END;
/

COMMIT;
