import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000'
wlstFileDomain='OSB'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

frwDBConURL_nXA=frwDBUrlDriverCName_nXA+"@"+frwDBListerAddress+":"+frwDBListerPort+":"+frwDBSchemaName
frwDBConURL_XA=frwDBUrlDriverCName_XA+"@"+frwDBListerAddress+":"+frwDBListerPort+":"+frwDBSchemaName

#Local JMS Artifacts
OSB_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+SOA_ManagedServerListenURLs+';java.naming.security.principal='+SOA_ConUsr+';java.naming.security.credentials='+SOA_ConPsw

#------------------------------------------
#Shared JMS Artifacts (with SOA Domain)
#------------------------------------------
if (isTwoDomains=='true'):
	sharedUsr=OSB_SOA_crossDomainUsr
	sharedPsw=OSB_SOA_crossDomainPsw
else:
	sharedUsr=SOA_ConUsr
	sharedPsw=SOA_ConPsw
	
SOA_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+SOA_ManagedServerListenURLs+';java.naming.security.principal='+sharedUsr+';java.naming.security.credentials='+sharedPsw
#------------------------------------------

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')
	
	disconnect()
	exit()
		
def makeDeploymentPlanVariable(wlstPlan, adapterAppname, name, value, xpath, origin='planbased'):
	variableAssignment = wlstPlan.createVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	variableAssignment.setXpath(xpath)
	variableAssignment.setOrigin(origin)
	wlstPlan.createVariable(name, value)
 		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------- """
""" FRW_OSB_FileStore """
""" ----------------- """
def createFileStore__FRW_OSB_FileStore():
	global fName
	
	fName = 'createFileStore__FRW_OSB_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createFileStore('FRW_OSB_FileStore')

	cd('/FileStores/FRW_OSB_FileStore')

	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_OSB_JMSServer """
""" ----------------- """
def createJMSServer__FRW_OSB_JMSServer():
	global fName

	fName = 'createJMSServer__FRW_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJMSServer('FRW_OSB_JMSServer')

	cd('/JMSServers/FRW_OSB_JMSServer')
	cmo.setPersistentStore(getMBean('/FileStores/FRW_OSB_FileStore'))
	
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------ """
""" SALESFORCE_OSB_JMSServer """
""" ------------------------ """
def createJMSServer__SALESFORCE_OSB_JMSServer():
	global fName

	fName = 'createJMSServer__SALESFORCE_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJMSServer('SALESFORCE_OSB_JMSServer')

	cd('/JMSServers/SALESFORCE_OSB_JMSServer')
	cmo.setPersistentStore(getMBean('/FileStores/FRW_OSB_FileStore'))
	
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" ------------------- """
""" DUMMY_OSB_JMSServer """
""" ------------------- """
def createJMSServer__DUMMY_OSB_JMSServer():
	global fName

	fName = 'createJMSServer__DUMMY_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJMSServer('DUMMY_OSB_JMSServer')

	cd('/JMSServers/DUMMY_OSB_JMSServer')
	cmo.setPersistentStore(getMBean('/FileStores/FRW_OSB_FileStore'))
	
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ----------------- """
""" FRW_OSB_JMSModule """
""" ----------------- """
def createJMSModule__FRW_OSB_JMSModule():
	global fName
	
	fName = 'createJMSModule__FRW_OSB_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJMSSystemResource('FRW_OSB_JMSModule')

	cd('/JMSSystemResources/FRW_OSB_JMSModule')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))

	cmo.createSubDeployment('FRW_OSB_SubDeploy')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/SubDeployments/FRW_OSB_SubDeploy')
	set('Targets',jarray.array([ObjectName('com.bea:Name=FRW_OSB_JMSServer,Type=JMSServer')], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ----------------------- """
""" SALESFORCE_RA_JMSModule """
""" ----------------------- """
def createJMSModule__SALESFORCE_RA_JMSModule():
	global fName
	
	fName = 'createJMSModule__SALESFORCE_RA_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJMSSystemResource('SALESFORCE_RA_JMSModule')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))

	cmo.createSubDeployment('SALESFORCE_RA_SubDeploy')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/SubDeployments/SALESFORCE_RA_SubDeploy')
	set('Targets',jarray.array([ObjectName('com.bea:Name=FRW_OSB_JMSServer,Type=JMSServer')], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" ------------------- """
""" DUMMY_OSB_JMSModule """
""" ------------------- """
def createJMSModule__DUMMY_OSB_JMSModule():
	global fName
	
	fName = 'createJMSModule__DUMMY_OSB_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJMSSystemResource('DUMMY_OSB_JMSModule')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))

	cmo.createSubDeployment('DUMMY_OSB_SubDeploy')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/SubDeployments/DUMMY_OSB_SubDeploy')
	set('Targets',jarray.array([ObjectName('com.bea:Name=FRW_OSB_JMSServer,Type=JMSServer')], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" ------------------------ """
""" ConversationManagerQueue """
""" ------------------------ """
def createJMSQueue__ConversationManagerQueue():
	global fName
	
	fName = 'createJMSQueue__ConversationManagerQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue('ConversationManagerQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerQueue')
	cmo.setJNDIName('/jms/FRW/ConversationManager/ConversationManagerQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue(' ConversationManagerErrorQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerErrorQueue')
	cmo.setJNDIName('/jms/FRW/ConversationManager/ConversationManagerErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerQueue/DeliveryFailureParams/ConversationManagerQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerQueue/DeliveryParamsOverrides/ConversationManagerQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerQueue/DeliveryFailureParams/ConversationManagerQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerErrorQueue'))
	
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------------- """

""" -------------------- """
""" LoggerDiscardedQueue """
""" -------------------- """
def createJMSQueue__LoggerDiscardedQueue():
	global fName
	
	fName = 'createJMSQueue__LoggerDiscardedQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue('LoggerDiscardedQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedQueue')
	cmo.setJNDIName('/jms/FRW/LoggerManager/LoggerDiscardedQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue('LoggerDiscardedErrorQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedErrorQueue')
	cmo.setJNDIName('/jms/FRW/LoggerManager/LoggerDiscardedErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedQueue/DeliveryFailureParams/LoggerDiscardedQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedQueue/DeliveryParamsOverrides/LoggerDiscardedQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedQueue/DeliveryFailureParams/LoggerDiscardedQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" --------------------- """
""" LoggerEnterpriseQueue """
""" --------------------- """
def createJMSQueue__LoggerEnterpriseQueue():
	global fName
	
	fName = 'createJMSQueue__LoggerEnterpriseQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue('LoggerEnterpriseQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseQueue')
	cmo.setJNDIName('/jms/FRW/LoggerManager/LoggerEnterpriseQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue('LoggerEnterpriseErrorQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseErrorQueue')
	cmo.setJNDIName('/jms/FRW/LoggerManager/LoggerEnterpriseErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseQueue/DeliveryFailureParams/LoggerEnterpriseQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseQueue/DeliveryParamsOverrides/LoggerEnterpriseQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseQueue/DeliveryFailureParams/LoggerEnterpriseQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------- """

""" ------------------ """
""" RegisterGroupQueue """
""" ------------------ """
def createJMSQueue__RegisterGroupQueue():
	global fName
	
	fName = 'createJMSQueue__RegisterGroupQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue('RegisterGroupQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupQueue')
	cmo.setJNDIName('/jms/FRW/CorrelationManager/RegisterGroupQueue	')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createUniformDistributedQueue('RegisterGroupErrorQueue')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupErrorQueue')
	cmo.setJNDIName('/jms/FRW/CorrelationManager/RegisterGroupErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupQueue/DeliveryFailureParams/RegisterGroupQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupQueue/DeliveryParamsOverrides/RegisterGroupQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupQueue/DeliveryFailureParams/RegisterGroupQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ---------------------- """
""" UPD-CLI-DATA_REQ_Queue """
""" ---------------------- """
def createJMSQueue__UPDCLIDATA_REQ_Queue():
	global fName
	
	fName = 'createJMSQueue__UPDCLIDATA_REQ_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')
	cmo.createUniformDistributedQueue('UPD-CLI-DATA_REQ_Queue')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_Queue')
	cmo.setJNDIName('/jms/Dummy/SALESFORCE/UPD-CLI-DATA_REQ_Queue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('DUMMY_OSB_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')
	cmo.createUniformDistributedQueue('UPD-CLI-DATA_REQ_ErrorQueue')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_ErrorQueue')
	cmo.setJNDIName('/jms/Dummy/SALESFORCE/UPD-CLI-DATA_REQ_ErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('DUMMY_OSB_SubDeploy')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_Queue/DeliveryFailureParams/UPD-CLI-DATA_REQ_Queue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_Queue/DeliveryParamsOverrides/UPD-CLI-DATA_REQ_Queue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_Queue/DeliveryFailureParams/UPD-CLI-DATA_REQ_Queue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_ErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ---------------------- """
""" UPD-CLI-DATA_RSP_Queue """
""" ---------------------- """
def createJMSQueue__UPDCLIDATA_RSP_Queue():
	global fName
	
	fName = 'createJMSQueue__UPDCLIDATA_RSP_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')
	cmo.createUniformDistributedQueue('UPD-CLI-DATA_RSP_Queue')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_Queue')
	cmo.setJNDIName('/jms/Dummy/SALESFORCE/UPD-CLI-DATA_RSP_Queue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('DUMMY_OSB_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')
	cmo.createUniformDistributedQueue('UPD-CLI-DATA_RSP_ErrorQueue')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_ErrorQueue')
	cmo.setJNDIName('/jms/Dummy/SALESFORCE/UPD-CLI-DATA_RSP_ErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('DUMMY_OSB_SubDeploy')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_Queue/DeliveryFailureParams/UPD-CLI-DATA_RSP_Queue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_Queue/DeliveryParamsOverrides/UPD-CLI-DATA_RSP_Queue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_Queue/DeliveryFailureParams/UPD-CLI-DATA_RSP_Queue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_ErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """

""" --------------------------------------------- """
""" SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue """
""" --------------------------------------------- """
def createJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_REQ_Queue():
	global fName
	
	fName = 'createJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_REQ_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')
	cmo.createUniformDistributedQueue('SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue')
	cmo.setJNDIName('/jms/RA/SALESFORCE/CHL-SALESFORCE-CRM_UPD-CLI-DATA_RA_REQ_Queue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('SALESFORCE_RA_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')
	cmo.createUniformDistributedQueue('SALESFORCE_CRM_UPD-CLI-DATA_RA_REQ_ErrorQueue')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_REQ_ErrorQueue')
	cmo.setJNDIName('/jms/RA/SALESFORCE/CHL-SALESFORCE-CRM_UPD-CLI-DATA_RA_REQ_ErrorQueue ')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('SALESFORCE_RA_SubDeploy')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue/DeliveryFailureParams/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue/DeliveryParamsOverrides/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue/DeliveryFailureParams/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_REQ_ErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """

""" --------------------------------------------- """
""" SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue """
""" --------------------------------------------- """
def createJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_RSP_Queue():
	global fName
	
	fName = 'createJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_RSP_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')
	cmo.createUniformDistributedQueue('SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue')
	cmo.setJNDIName('/jms/RA/SALESFORCE/CHL-SALESFORCE-CRM_UPD-CLI-DATA_RA_RSP_Queue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('SALESFORCE_RA_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')
	cmo.createUniformDistributedQueue('SALESFORCE_CRM_UPD-CLI-DATA_RA_RSP_ErrorQueue')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_RSP_ErrorQueue')
	cmo.setJNDIName('/jms/RA/SALESFORCE/CHL-SALESFORCE-CRM_UPD-CLI-DATA_RA_RSP_ErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('SALESFORCE_RA_SubDeploy')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue/DeliveryFailureParams/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue/DeliveryParamsOverrides/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue/DeliveryFailureParams/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_RSP_ErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """
""" ------------------------- """
""" ConversationManagerXA_DCF """
""" ------------------------- """
def createJMSConnFactory__ConversationManagerXA_DCF():
	global fName
	
	fName = 'createJMSConnFactory__ConversationManagerXA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createConnectionFactory('ConversationManagerXA_DCF')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/ConversationManagerXA_DCF')
	cmo.setJNDIName('/jms/FRW/ConversationManager/DefaultXAConnFactory')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/ConversationManagerXA_DCF/SecurityParams/ConversationManagerXA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/ConversationManagerXA_DCF/ClientParams/ConversationManagerXA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/ConversationManagerXA_DCF/TransactionParams/ConversationManagerXA_DCF')
	cmo.setXAConnectionFactoryEnabled(true)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/ConversationManagerXA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------- """

""" --------------------- """
""" LoggerManager_NXA_DCF """
""" --------------------- """
def createJMSConnFactory__LoggerManager_NXA_DCF():
	global fName
	
	fName = 'createJMSConnFactory__LoggerManager_NXA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createConnectionFactory('LoggerManager_NXA_DCF')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_NXA_DCF')
	cmo.setJNDIName('/jms/FRW/LoggerManager/DefaultNXAConnFactory')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_NXA_DCF/SecurityParams/LoggerManager_NXA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_NXA_DCF/ClientParams/LoggerManager_NXA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_NXA_DCF/TransactionParams/LoggerManager_NXA_DCF')
	cmo.setXAConnectionFactoryEnabled(false)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_NXA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------- """

""" -------------------- """
""" LoggerManager_XA_DCF """
""" -------------------- """
def createJMSConnFactory__LoggerManager_XA_DCF():
	global fName
	
	fName = 'createJMSConnFactory__LoggerManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createConnectionFactory('LoggerManager_XA_DCF')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_XA_DCF')
	cmo.setJNDIName('/jms/FRW/LoggerManager/DefaultXAConnFactory')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_XA_DCF/SecurityParams/LoggerManager_XA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_XA_DCF/ClientParams/LoggerManager_XA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_XA_DCF/TransactionParams/LoggerManager_XA_DCF')
	cmo.setXAConnectionFactoryEnabled(true)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_XA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" ------------------------- """
""" CorrelationManager_XA_DCF """
""" ------------------------- """
def createJMSConnFactory__CorrelationManager_XA_DCF():
	global fName
	
	fName = 'createJMSConnFactory__CorrelationManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.createConnectionFactory('CorrelationManager_XA_DCF')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF')
	cmo.setJNDIName('/jms/FRW/CorrelationManager/DefaultXAConnFactory')

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF/SecurityParams/CorrelationManager_XA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF/ClientParams/CorrelationManager_XA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF/TransactionParams/CorrelationManager_XA_DCF')
	cmo.setXAConnectionFactoryEnabled(true)

	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('FRW_OSB_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------- """


""" -------------------- """
""" SALESFORCE_RA_XA_DCF """
""" -------------------- """
def createJMSConnFactory__SALESFORCE_RA_XA_DCF():
	global fName
	
	fName = 'createJMSConnFactory__SALESFORCE_RA_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')
	cmo.createConnectionFactory('SALESFORCE_RA_XA_DCF')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/ConnectionFactories/SALESFORCE_RA_XA_DCF')
	cmo.setJNDIName('/jms/RA/SALESFORCE/DefaultXAConnFactory')

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/ConnectionFactories/SALESFORCE_RA_XA_DCF/SecurityParams/SALESFORCE_RA_XA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/ConnectionFactories/SALESFORCE_RA_XA_DCF/ClientParams/SALESFORCE_RA_XA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/ConnectionFactories/SALESFORCE_RA_XA_DCF/TransactionParams/SALESFORCE_RA_XA_DCF')
	cmo.setXAConnectionFactoryEnabled(true)

	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/ConnectionFactories/SALESFORCE_RA_XA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('SALESFORCE_RA_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" -------------------- """
""" SALESFORCE_DUMMY_XA_DCF """
""" -------------------- """
def createJMSConnFactory__SALESFORCE_DUMMY_XA_DCF():
	global fName
	
	fName = 'createJMSConnFactory__SALESFORCE_DUMMY_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')
	cmo.createConnectionFactory('SALESFORCE_DUMMY_XA_DCF')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/ConnectionFactories/SALESFORCE_DUMMY_XA_DCF')
	cmo.setJNDIName('/jms/DUMMY/SALESFORCE/DefaultXAConnFactory')

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/ConnectionFactories/SALESFORCE_DUMMY_XA_DCF/SecurityParams/SALESFORCE_DUMMY_XA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/ConnectionFactories/SALESFORCE_DUMMY_XA_DCF/ClientParams/SALESFORCE_DUMMY_XA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/ConnectionFactories/SALESFORCE_DUMMY_XA_DCF/TransactionParams/SALESFORCE_DUMMY_XA_DCF')
	cmo.setXAConnectionFactoryEnabled(true)

	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/ConnectionFactories/SALESFORCE_DUMMY_XA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('DUMMY_OSB_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" ------------------ """
""" SupportComponentDS """
""" ------------------ """
def createDatasource__SupportComponentDS():
	global fName
	
	fName = 'createDatasource__SupportComponentDS'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJDBCSystemResource('SupportComponentDS')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS')
	cmo.setName('SupportComponentDS')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDataSourceParams/SupportComponentDS')
	set('JNDINames',jarray.array([String('/jdbc/FRW/SupportComponentsDSTx')], String))

	#cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS')
	#cmo.setDatasourceType('Generic')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS')
	
	cmo.setUrl(frwDBConURL_XA)
	cmo.setDriverName(frwDBDriverName_XA)
	cmo.setPassword(frwDBPass)

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS')
	cmo.createProperty('user')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS/Properties/user')
	cmo.setValue(frwDBUser)
	
	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS')
	cmo.createProperty('Oracle.net.CONNECT_TIMEOUT')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS/Properties/Oracle.net.CONNECT_TIMEOUT')
	cmo.setValue('10000')
	
	
	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS')
	cmo.createProperty('Oracle.jdbc.ReadTimeout')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS/Properties/Oracle.jdbc.ReadTimeout')
	cmo.setValue('30000')
			
	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDataSourceParams/SupportComponentDS')
	cmo.setGlobalTransactionsProtocol('TwoPhaseCommit')

	cd('/JDBCSystemResources/SupportComponentDS')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ---------------------- """
""" SupportComponentDSNoTx """
""" ---------------------- """
def createDatasource__SupportComponentDSNoTx():
	global fName
	
	fName = 'createDatasource__SupportComponentDSNoTx'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJDBCSystemResource('SupportComponentDSNoTx')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx')
	cmo.setName('SupportComponentDSNoTx')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDataSourceParams/SupportComponentDSNoTx')
	set('JNDINames',jarray.array([String('/jdbc/FRW/SupportComponentsDSNoTx')], String))

	#cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx')
	#cmo.setDatasourceType('Generic')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx')
	
	cmo.setUrl(frwDBConURL_nXA)
	cmo.setDriverName(frwDBDriverName_nXA)
	cmo.setPassword(frwDBPass)	

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx')
	cmo.createProperty('user')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx/Properties/user')
	cmo.setValue(frwDBUser)
	
	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx')
	cmo.createProperty('Oracle.net.CONNECT_TIMEOUT')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx/Properties/Oracle.net.CONNECT_TIMEOUT')
	cmo.setValue('10000')
	
	
	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx')
	cmo.createProperty('Oracle.jdbc.ReadTimeout')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx/Properties/Oracle.jdbc.ReadTimeout')
	cmo.setValue('30000')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDataSourceParams/SupportComponentDSNoTx')
	cmo.setGlobalTransactionsProtocol('None')

	cd('/JDBCSystemResources/SupportComponentDSNoTx')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ConversationManager """
""" ------------------------------ """
def createDBAdapterCFactory__ConversationManager():
	global fName
	global wlstVersion
	
	fName = 'createDBAdapterCFactory__ConversationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSNoTx'
	dsJNDI='/jdbc/FRW/SupportComponentsDSNoTx'
	
	cfName='ConversationManager'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="DataSourceName"]/value')
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ErrorManager """
""" ------------------------------ """
def createDBAdapterCFactory__ErrorManager():
	global fName
	
	fName = 'createDBAdapterCFactory__ErrorManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSNoTx'
	dsJNDI='/jdbc/FRW/SupportComponentsDSNoTx'
	
	cfName='ErrorManager'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="DataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/LoggerManager """
""" ------------------------------ """
def createDBAdapterCFactory__LoggerManager():
	global fName
	
	fName = 'createDBAdapterCFactory__LoggerManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSTx'
	dsJNDI='/jdbc/FRW/SupportComponentsDSTx'
	
	cfName='LoggerManager'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="xADataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/MessageManager """
""" ------------------------------ """
def createDBAdapterCFactory__MessageManager():
	global fName
	
	fName = 'createDBAdapterCFactory__MessageManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSNoTx'
	dsJNDI='/jdbc/FRW/SupportComponentsDSNoTx'
	
	cfName='MessageManager'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="DataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ParameterManager """
""" ------------------------------ """
def createDBAdapterCFactory__ParameterManager():
	global fName
	
	fName = 'createDBAdapterCFactory__ParameterManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSNoTx'
	dsJNDI='/jdbc/FRW/SupportComponentsDSNoTx'
	
	cfName='ParameterManager'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="DataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------------------- """
""" createDBAdapterCFactory__CorrelationManager """
""" ------------------------------------------- """
def createDBAdapterCFactory__CorrelationManager():
	global fName
	
	fName = 'createDBAdapterCFactory__CorrelationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSTx'
	dsJNDI='/jdbc/FRW/SupportComponentsDSTx'
	
	cfName='CorrelationManager'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="xADataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------------------- """

""" ------------------------------ """
""" eis/DB/FRW/ErrorHospital """
""" ------------------------------ """
# From 1.1
def createDBAdapterCFactory__ErrorHospital():
	global fName
	
	fName = 'createDBAdapterCFactory__ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSTx'
	dsJNDI='jdbc/FRW/SupportComponentsDSTx'
	
	cfName='ErrorHospital'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="xADataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" -------------------- """
""" eis/Coherence/Entel  """
""" -------------------- """
def createCOHAdapterCFactory__Entel ():
	global fName
	
	fName = 'createCOHAdapterCFactory__Entel'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + coherenceAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + coherenceAdapterPlanName
	
	dsName='Entel'
	
	cfName='Entel'
	eisName='eis/Coherence/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConnectionInstance_eis/Coherence/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConfigProperty_CacheConfigLocation_Value_' + cfName, frwCoherenceConfigFileDir, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="CacheConfigLocation"]/value')
	makeDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConfigProperty_ServiceName_Value_' + cfName, frwCoherenceServiceName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ServiceName"]/value')

	plan.save();

	cd('/AppDeployments/'+coherenceAdapterAppName+'/Targets');
	redeploy(coherenceAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """


""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """
	
	#-------------------------------------------------------------------------------
	# Framework Configurations --->
	#-------------------------------------------------------------------------------
	
	# DB Datasources
	createDatasource__SupportComponentDSNoTx()
	createDatasource__SupportComponentDS()
	
	# Persistent Stores
	createFileStore__FRW_OSB_FileStore()
	
	# JMS Servers
	createJMSServer__FRW_OSB_JMSServer()

	# JMS Modules
	createJMSModule__FRW_OSB_JMSModule()

	# JMS Queues
	createJMSQueue__ConversationManagerQueue()
	createJMSQueue__LoggerDiscardedQueue()
	createJMSQueue__LoggerEnterpriseQueue()
	createJMSQueue__RegisterGroupQueue()
	
	# JMS Connection Factories
	createJMSConnFactory__ConversationManagerXA_DCF()
	createJMSConnFactory__LoggerManager_NXA_DCF()
	createJMSConnFactory__LoggerManager_XA_DCF()
	createJMSConnFactory__CorrelationManager_XA_DCF()
	
	#-------------------------------------------------------------------------------
	# Service Template Configurations --->
	#-------------------------------------------------------------------------------
	
	# JMS Servers
	createJMSServer__SALESFORCE_OSB_JMSServer()
	createJMSServer__DUMMY_OSB_JMSServer()
	
	# JMS Modules
	createJMSModule__SALESFORCE_RA_JMSModule()
	createJMSModule__DUMMY_OSB_JMSModule()
	
	# JMS Queues
	createJMSQueue__UPDCLIDATA_REQ_Queue()
	createJMSQueue__UPDCLIDATA_RSP_Queue()
	createJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_REQ_Queue()
	createJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_RSP_Queue()
		
	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	#-------------------------------------------------------------------------------
	# Framework Applications --->
	#-------------------------------------------------------------------------------
	
	# Coherence Adapter
	createCOHAdapterCFactory__Entel()
	
	# DBAdapter Connection Factories
	createDBAdapterCFactory__ConversationManager()
	createDBAdapterCFactory__ErrorManager()
	createDBAdapterCFactory__LoggerManager()
	createDBAdapterCFactory__MessageManager()
	createDBAdapterCFactory__ParameterManager()
	createDBAdapterCFactory__CorrelationManager()
	createDBAdapterCFactory__ErrorHospital()
	
	#-------------------------------------------------------------------------------
	# Service Template Applications --->
	#-------------------------------------------------------------------------------
	
	# JMS Connection Factories
	createJMSConnFactory__SALESFORCE_RA_XA_DCF()
	createJMSConnFactory__SALESFORCE_DUMMY_XA_DCF()

	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')
