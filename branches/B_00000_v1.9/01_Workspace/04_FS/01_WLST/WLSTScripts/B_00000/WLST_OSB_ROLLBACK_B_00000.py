import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000'
wlstFileDomain='OSB'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')
	
	disconnect()
	exit()
	
def deleteDeploymentPlanVariable(wlstPlan, adapterAppname, name):
	wlstPlan.destroyVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	wlstPlan.destroyVariable(name)
		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------- """
""" FRW_OSB_FileStore """
""" ------------------- """
def rollbackFileStore__FRW_OSB_FileStore():
	global fName
	
	fName = 'rollbackFileStore__FRW_OSB_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cmo.destroyFileStore(getMBean('/FileStores/FRW_OSB_FileStore'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_OSB_JMSServer """
""" ----------------- """
def rollbackJMSServer__FRW_OSB_JMSServer():
	global fName
	fName = 'rollbackJMSServer__FRW_OSB_JMSServer'

	cmo.destroyJMSServer(getMBean('/JMSServers/FRW_OSB_JMSServer'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ------------------------ """
""" SALESFORCE_OSB_JMSServer """
""" ------------------------ """
def rollbackJMSServer__SALESFORCE_OSB_JMSServer():
	global fName
	
	fName = 'rollbackJMSServer__SALESFORCE_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyJMSServer(getMBean('/JMSServers/SALESFORCE_OSB_JMSServer'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" ------------------- """
""" DUMMY_OSB_JMSServer """
""" ------------------- """
def rollbackJMSServer__DUMMY_OSB_JMSServer():
	global fName
	
	fName = 'rollbackJMSServer__DUMMY_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyJMSServer(getMBean('/JMSServers/DUMMY_OSB_JMSServer'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ----------------- """
""" FRW_OSB_JMSModule """
""" ----------------- """
def rollbackJMSModule__FRW_OSB_JMSModule():
	global beanName
	
	fName = 'rollbackJMSModule__FRW_OSB_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/FRW_OSB_JMSModule'))
	
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------- """

""" ----------------------- """
""" SALESFORCE_RA_JMSModule """
""" ----------------------- """
def rollbackJMSModule__SALESFORCE_RA_JMSModule():
	global beanName
	
	fName = 'rollbackJMSModule__SALESFORCE_RA_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule'))
	
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ------------------- """
""" DUMMY_OSB_JMSModule """
""" ------------------- """
def rollbackJMSModule__DUMMY_OSB_JMSModule():
	global beanName
	
	fName = 'rollbackJMSModule__DUMMY_OSB_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule'))
	
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ------------------------ """
""" ConversationManagerQueue """
""" ------------------------ """
def rollbackJMSQueue__ConversationManagerQueue():
	global beanName
	
	fName = 'rollbackJMSQueue__ConversationManagerQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/ConversationManagerErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """

""" ------------------------ """
""" LoggerDiscardedQueue """
""" ------------------------ """
def rollbackJMSQueue__LoggerDiscardedQueue():
	global beanName
	
	fName = 'rollbackJMSQueue__LoggerDiscardedQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerDiscardedErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """

""" --------------------- """
""" LoggerEnterpriseQueue """
""" --------------------- """
def rollbackJMSQueue__LoggerEnterpriseQueue():
	global beanName
	
	fName = 'rollbackJMSQueue__LoggerEnterpriseQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/LoggerEnterpriseErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ------------------ """
""" RegisterGroupQueue """
""" ------------------ """
def rollbackJMSQueue__RegisterGroupQueue():
	global beanName
	
	fName = 'rollbackJMSQueue__RegisterGroupQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupQueue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/UniformDistributedQueues/RegisterGroupErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------- """

""" ---------------------- """
""" UPD-CLI-DATA_REQ_Queue """
""" ---------------------- """
def rollbackJMSQueue__UPDCLIDATA_REQ_Queue():
	global beanName
	
	fName = 'rollbackJMSQueue__UPDCLIDATA_REQ_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_REQ_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/ConversationManagerErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" ---------------------- """
""" UPD-CLI-DATA_RSP_Queue """
""" ---------------------- """
def rollbackJMSQueue__UPDCLIDATA_RSP_Queue():
	global beanName
	
	fName = 'rollbackJMSQueue__UPDCLIDATA_RSP_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/DUMMY_OSB_JMSModule/JMSResource/DUMMY_OSB_JMSModule/UniformDistributedQueues/UPD-CLI-DATA_RSP_ErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """

""" --------------------------------------------- """
""" SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue """
""" --------------------------------------------- """
def rollbackJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_REQ_Queue():
	global beanName
	
	fName = 'rollbackJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_REQ_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_REQ_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_REQ_ErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------------------------------- """

""" --------------------------------------------- """
""" SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue """
""" --------------------------------------------- """
def rollbackJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_RSP_Queue():
	global beanName
	
	fName = 'rollbackJMSQueue__SALESFORCE_CRM_UPDCLIDATA_Sync_RA_RSP_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule')

	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_Sync_RA_RSP_Queue'))
	cmo.destroyUniformDistributedQueue(getMBean('/JMSSystemResources/SALESFORCE_RA_JMSModule/JMSResource/SALESFORCE_RA_JMSModule/UniformDistributedQueues/SALESFORCE_CRM_UPD-CLI-DATA_RA_RSP_ErrorQueue'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" --------------------------------------------- """

""" ------------------------- """
""" ConversationManagerXA_DCF """
""" ------------------------- """
def rollbackJMSConnFactory__ConversationManagerXA_DCF():
	global beanName
	
	fName = 'rollbackJMSConnFactory__ConversationManagerXA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/ConversationManagerXA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" --------------------- """
""" LoggerManager_NXA_DCF """
""" --------------------- """
def rollbackJMSConnFactory__LoggerManager_NXA_DCF():
	global beanName
	
	fName = 'rollbackJMSConnFactory__LoggerManager_NXA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_NXA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" -------------------- """
""" LoggerManager_XA_DCF """
""" -------------------- """
def rollbackJMSConnFactory__LoggerManager_XA_DCF():
	global beanName
	
	fName = 'rollbackJMSConnFactory__LoggerManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/LoggerManager_XA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------- """
""" CorrelationManager_XA_DCF """
""" ------------------------- """
def rollbackJMSConnFactory__CorrelationManager_XA_DCF():
	global beanName
	
	fName = 'rollbackJMSConnFactory__CorrelationManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ------------------------- """
""" CorrelationManager_XA_DCF """
""" ------------------------- """
def rollbackJMSConnFactory__CorrelationManager_XA_DCF():
	global beanName
	
	fName = 'rollbackJMSConnFactory__CorrelationManager_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule')
	cmo.destroyConnectionFactory(getMBean('/JMSSystemResources/FRW_OSB_JMSModule/JMSResource/FRW_OSB_JMSModule/ConnectionFactories/CorrelationManager_XA_DCF'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """

""" ------------------ """
""" SupportComponentDS """
""" ------------------ """
def rollbackDatasource__SupportComponentDS():
	global beanName
	
	fName = 'rollbackDatasource__SupportComponentDS'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.destroyJDBCSystemResource(getMBean('/JDBCSystemResources/SupportComponentDS'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------- """

""" ------------------------------- """
""" SupportComponentsDSTx """
""" ------------------------------- """
def rollbackDatasource__SupportComponentDSNoTx():
	global beanName
	
	fName = 'rollbackDatasource__SupportComponentDSNoTx'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.destroyJDBCSystemResource(getMBean('/JDBCSystemResources/SupportComponentDSNoTx'))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ConversationManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory__ConversationManager():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__ConversationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName='ConversationManager'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)

	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ErrorManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory__ErrorManager():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__ErrorManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName='ErrorManager'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/LoggerManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory__LoggerManager():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__LoggerManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName='LoggerManager'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/MessageManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory__MessageManager():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__MessageManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName='MessageManager'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ParameterManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory__ParameterManager():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__ParameterManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName='ParameterManager'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/CorrelationManager """
""" ------------------------------ """
def rollbackDBAdapterCFactory__CorrelationManager():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__CorrelationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName='CorrelationManager'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ErrorHospital """
""" ------------------------------ """
def rollbackDBAdapterCFactory__ErrorHospital():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName='ErrorHospital'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName)
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/Coherence/Entel  """
""" ------------------------------ """
def rollbackCOHAdapterCFactory__Entel():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__CorrelationManager'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + coherenceAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + coherenceAdapterPlanName
	
	cfName='Entel'
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConnectionInstance_eis/Coherence/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConfigProperty_CacheConfigLocation_Value_' + cfName)
	deleteDeploymentPlanVariable(plan, coherenceAdapterSourceName, 'ConfigProperty_ServiceName_Value_' + cfName)
	
	plan.save();

	cd('/AppDeployments/'+coherenceAdapterAppName+'/Targets');
	redeploy(coherenceAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	#-------------------------------------------------------------------------------
	# Service Template Resources --->
	#-------------------------------------------------------------------------------
	
	# JMS Modules
	rollbackJMSModule__SALESFORCE_RA_JMSModule()
	rollbackJMSModule__DUMMY_OSB_JMSModule()
	
	# JMS Servers
	rollbackJMSServer__SALESFORCE_OSB_JMSServer()
	rollbackJMSServer__DUMMY_OSB_JMSServer()
	
	#-------------------------------------------------------------------------------
	# Framework Resources --->
	#-------------------------------------------------------------------------------
	
	# JMS Modules
	rollbackJMSModule__FRW_OSB_JMSModule()

	# JMS Servers
	rollbackJMSServer__FRW_OSB_JMSServer()

	# Persistent Stores
	rollbackFileStore__FRW_OSB_FileStore()
		
	# DB Datasources
	rollbackDatasource__SupportComponentDS()
	rollbackDatasource__SupportComponentDSNoTx()
	
	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	#-------------------------------------------------------------------------------
	# Service Template Applications --->
	#-------------------------------------------------------------------------------
	
	#-------------------------------------------------------------------------------
	# Framework Applications --->
	#-------------------------------------------------------------------------------
	
	# DBAdapter Connection Factories
	rollbackDBAdapterCFactory__ConversationManager()
	rollbackDBAdapterCFactory__ErrorManager()
	rollbackDBAdapterCFactory__LoggerManager()
	rollbackDBAdapterCFactory__MessageManager()
	rollbackDBAdapterCFactory__ParameterManager()
	rollbackDBAdapterCFactory__CorrelationManager()
	rollbackDBAdapterCFactory__ErrorHospital()
	
	# Coherence Adapter
	rollbackCOHAdapterCFactory__Entel()

	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')