import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_00000'
wlstFileDomain='SOA'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+SOA_AdminServerListenAddress+":"+SOA_AdminServerListenPort

frwDBConURL_nXA=frwDBUrlDriverCName_nXA+"@"+frwDBListerAddress+":"+frwDBListerPort+":"+frwDBSchemaName
frwDBConURL_XA=frwDBUrlDriverCName_XA+"@"+frwDBListerAddress+":"+frwDBListerPort+":"+frwDBSchemaName

#Local JMS Artifacts
SOA_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+SOA_ManagedServerListenURLs+';java.naming.security.principal='+SOA_ConUsr+';java.naming.security.credentials='+SOA_ConPsw

#------------------------------------------
#Shared JMS Artifacts (with OSB Domain)
#------------------------------------------
if (isTwoDomains=='true'):
	sharedUsr=OSB_SOA_crossDomainUsr
	sharedPsw=OSB_SOA_crossDomainPsw
else:
	sharedUsr=OSB_ConUsr
	sharedPsw=OSB_ConPsw
	
OSB_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+OSB_ManagedServerListenURLs+';java.naming.security.principal='+sharedUsr+';java.naming.security.credentials='+sharedPsw
#------------------------------------------

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')
	
	disconnect()
	exit()		

def makeDeploymentPlanVariable(wlstPlan, adapterAppname, name, value, xpath, origin='planbased'):
	variableAssignment = wlstPlan.createVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	variableAssignment.setXpath(xpath)
	variableAssignment.setOrigin(origin)
	wlstPlan.createVariable(name, value)
		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------- """
""" FRW_SOA_FileStore """
""" ----------------- """
def createFileStore__FRW_SOA_FileStore():
	global fName
	
	fName = 'createFileStore__FRW_SOA_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createFileStore('FRW_SOA_FileStore')

	cd('/FileStores/FRW_SOA_FileStore')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_SOA_JMSServer """
""" ----------------- """
def createJMSServer__FRW_SOA_JMSServer():
	global fName
	
	fName = 'createJMSServer__FRW_SOA_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cd('/')
	cmo.createJMSServer('FRW_SOA_JMSServer')

	cd('/JMSServers/FRW_SOA_JMSServer')
	cmo.setPersistentStore(getMBean('/FileStores/FRW_SOA_FileStore'))
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """

""" ----------------- """
""" FRW_SOA_JMSModule """
""" ----------------- """
def createJMSModule__FRW_SOA_JMSModule():
	global fName
	
	fName = 'createJMSModule__FRW_SOA_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJMSSystemResource('FRW_SOA_JMSModule')

	cd('/JMSSystemResources/FRW_SOA_JMSModule')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))

	cmo.createSubDeployment('FRW_SOA_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/SubDeployments/FRW_SOA_SubDeploy')
	set('Targets',jarray.array([ObjectName('com.bea:Name=FRW_SOA_JMSServer,Type=JMSServer')], ObjectName))
	
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------- """

""" ------------------------ """
""" MessageDispatcherQueue """
""" ------------------------ """
def createJMSQueue__MessageDispatcherQueue():
	global fName
	
	fName = 'createJMSQueue__MessageDispatcherQueue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('MessageDispatcherQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/MessageDispatcherQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createUniformDistributedQueue('MessageDispatcherErrorQueue')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherErrorQueue')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/MessageDispatcherErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue/DeliveryFailureParams/MessageDispatcherQueue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue/DeliveryParamsOverrides/MessageDispatcherQueue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherQueue/DeliveryFailureParams/MessageDispatcherQueue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/UniformDistributedQueues/MessageDispatcherErrorQueue'))

	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" -------------------- """
""" ErrorHospital_XA_DCF """
""" -------------------- """
def createJMSConnFactory__ErrorHospital_XA_DCF():
	global fName
	
	fName = 'createJMSConnFactory__ErrorHospital_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule')
	cmo.createConnectionFactory('ErrorHospital_XA_DCF')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF')
	cmo.setJNDIName('/jms/FRW/ErrorManager/ErrorHospital/DefaultXAConnFactory')

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF/SecurityParams/ErrorHospital_XA_DCF')
	cmo.setAttachJMSXUserId(false)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF/ClientParams/ErrorHospital_XA_DCF')
	cmo.setClientIdPolicy('Restricted')
	cmo.setSubscriptionSharingPolicy('Exclusive')
	cmo.setMessagesMaximum(10)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF/TransactionParams/ErrorHospital_XA_DCF')
	cmo.setXAConnectionFactoryEnabled(true)

	cd('/JMSSystemResources/FRW_SOA_JMSModule/JMSResource/FRW_SOA_JMSModule/ConnectionFactories/ErrorHospital_XA_DCF')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setSubDeploymentName('FRW_SOA_SubDeploy')

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------- """

""" ------------------ """
""" SupportComponentDS """
""" ------------------ """
def createDatasource__SupportComponentDS():
	global fName
	
	fName = 'createDatasource__SupportComponentDS'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJDBCSystemResource('SupportComponentDS')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS')
	cmo.setName('SupportComponentDS')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDataSourceParams/SupportComponentDS')
	set('JNDINames',jarray.array([String('/jdbc/FRW/SupportComponentsDSTx')], String))

	#cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS')
	#cmo.setDatasourceType('Generic')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS')
	
	cmo.setUrl(frwDBConURL_XA)
	cmo.setDriverName(frwDBDriverName_XA)
	cmo.setPassword(frwDBPass)

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS')
	cmo.createProperty('user')

	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDriverParams/SupportComponentDS/Properties/SupportComponentDS/Properties/user')
	cmo.setValue(frwDBUser)

	
	cd('/JDBCSystemResources/SupportComponentDS/JDBCResource/SupportComponentDS/JDBCDataSourceParams/SupportComponentDS')
	cmo.setGlobalTransactionsProtocol('TwoPhaseCommit')

	cd('/JDBCSystemResources/SupportComponentDS')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ---------------------- """
""" SupportComponentDSNoTx """
""" ---------------------- """
def createDatasource__SupportComponentDSNoTx():
	global fName
	
	fName = 'createDatasource__SupportComponentDSNoTx'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJDBCSystemResource('SupportComponentDSNoTx')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx')
	cmo.setName('SupportComponentDSNoTx')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDataSourceParams/SupportComponentDSNoTx')
	set('JNDINames',jarray.array([String('/jdbc/FRW/SupportComponentsDSNoTx')], String))

	#cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx')
	#cmo.setDatasourceType('Generic')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx')
	
	cmo.setUrl(frwDBConURL_nXA)
	cmo.setDriverName(frwDBDriverName_nXA)
	cmo.setPassword(frwDBPass)	

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx')
	cmo.createProperty('user')

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDriverParams/SupportComponentDSNoTx/Properties/SupportComponentDSNoTx/Properties/user')
	cmo.setValue(frwDBUser)

	cd('/JDBCSystemResources/SupportComponentDSNoTx/JDBCResource/SupportComponentDSNoTx/JDBCDataSourceParams/SupportComponentDSNoTx')
	cmo.setGlobalTransactionsProtocol('None')

	cd('/JDBCSystemResources/SupportComponentDSNoTx')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+SOA_TargetName.strip()+',Type='+SOA_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/DB/FRW/ErrorHospital """
""" ------------------------------ """
def createDBAdapterCFactory__ErrorHospital():
	global fName
	
	fName = 'createDBAdapterCFactory__ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName='SupportComponentsDSTx'
	dsJNDI='jdbc/FRW/SupportComponentsDSTx'
	
	cfName='ErrorHospital'
	eisName='eis/DB/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_xADataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="xADataSourceName"]/value')

	plan.save();

	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/JMS/FRW/ErrorHospital """
""" ------------------------------ """
def createJMSAdapterCFactory__ErrorHospital():
	global fName
	
	fName = 'createJMSAdapterCFactory__ErrorHospital'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName
	
	connFactoryJNDI='/jms/FRW/ErrorManager/ErrorHospital/DefaultXAConnFactory'
	
	cfName='ErrorHospital'
	eisName='eis/JMS/FRW/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName, connFactoryJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ConnectionFactoryLocation"]/value')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName, SOA_JmsAdapterFactoryProperties, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="FactoryProperties"]/value')

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" eis/JMS/SALESFORCE """
""" ------------------------------ """
def createJMSAdapterCFactory__SALESFORCE():
	global fName
	
	fName = 'createJMSAdapterCFactory__SALESFORCE'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName
	
	connFactoryJNDI='/jms/RA/SALESFORCE/DefaultXAConnFactory'
	
	cfName='SALESFORCE'
	eisName='eis/JMS/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName, connFactoryJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ConnectionFactoryLocation"]/value')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName, OSB_JmsAdapterFactoryProperties, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="FactoryProperties"]/value')

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """
	
	#-------------------------------------------------------------------------------
	# Framework Resources --->
	#-------------------------------------------------------------------------------
	
	# DB Datasources
	createDatasource__SupportComponentDSNoTx()
	createDatasource__SupportComponentDS()
		
	# Persistent Stores
	createFileStore__FRW_SOA_FileStore()
	
	# JMS Servers
	createJMSServer__FRW_SOA_JMSServer()
	
	# JMS Modules
	createJMSModule__FRW_SOA_JMSModule()

	# JMS Queues
	createJMSQueue__MessageDispatcherQueue()
	
	# JMS Connection Factories
	createJMSConnFactory__ErrorHospital_XA_DCF()
	
	#-------------------------------------------------------------------------------
	# Service Template Resources --->
	#-------------------------------------------------------------------------------

	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	#-------------------------------------------------------------------------------
	# Framework Applications --->
	#-------------------------------------------------------------------------------
	
	# DBAdapter Connection Factories
	createDBAdapterCFactory__ErrorHospital()
	
	# JMSAdapter Connection Factories
	createJMSAdapterCFactory__ErrorHospital()

	#-------------------------------------------------------------------------------
	# Service Template Applications --->
	#-------------------------------------------------------------------------------
	
	# JMSAdapter Connection Factories
	createJMSAdapterCFactory__SALESFORCE()

	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')