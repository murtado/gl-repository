<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:ns0="http://www.entel.cl/ESO/MessageHeader/v1" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://www.entel.cl/SC/ConversationManager/updStatus/v1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 tns mhdr oraext xp20 xref socket dvm oraxsl" xml:id="id_1" oraxsl:ignorexmlids="true"
                xmlns:ns2="http://www.entel.cl/ESO/Tracer/v1"
                xmlns:ns3="http://www.entel.cl/SC/ConversationManager/Aux/ConversationStatus"
                xmlns:publishResult="http://www.entel.cl/SC/ConversationManager/publishResult/v1"
                xmlns:ns1="http://www.entel.cl/SC/ConversationManager/v1" xmlns:ns6="http://www.entel.cl/ESO/Result/v2"
                xmlns:getNewConversation="http://www.entel.cl/SC/ConversationManager/getNewConversation/v1"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ns4="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:rcPubRes="http://www.entel.cl/SC/ConversationManager/RefreshCache_PublishResult/v1"
                xmlns:ns5="http://www.entel.cl/ESO/Error/v1"
                xmlns:getStatus="http://www.entel.cl/SC/ConversationManager/getStatus/v1"
                xmlns:getInfo="http://www.entel.cl/SC/ConversationManager/getInfo/v1"
                xmlns:ns7="http://www.entel.cl/SC/ConversationManager/Aux/Conversation"
                xmlns:subscribe="http://www.entel.cl/SC/ConversationManager/subscribe/v1"
                xmlns:rcIsTxOw="http://www.entel.cl/SC/ConversationManager/RefreshCache_getInfo/v1">
   <oracle-xsl-mapper:schema xml:id="id_2">
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources xml:id="id_3">
         <oracle-xsl-mapper:source type="WSDL" xml:id="id_4">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_ConversationManager/SupportAPI/WSDL/ConversationManager.wsdl" xml:id="id_5"/>
            <oracle-xsl-mapper:rootElement name="ResponseHeader" namespace="http://www.entel.cl/ESO/MessageHeader/v1" xml:id="id_6"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets xml:id="id_7">
         <oracle-xsl-mapper:target type="WSDL" xml:id="id_8">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_ConversationManager/SupportAPI/WSDL/ConversationManager.wsdl" xml:id="id_9"/>
            <oracle-xsl-mapper:rootElement name="UpdStatusREQ" namespace="http://www.entel.cl/SC/ConversationManager/updStatus/v1" xml:id="id_10"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [FRI AUG 12 17:07:45 ART 2016].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/" xml:id="id_11">
      <tns:UpdStatusREQ xml:id="id_12">
         <tns:ConversationID xml:id="id_13">
            <xsl:value-of select="/ns0:ResponseHeader/ns0:Trace/@conversationID" xml:id="id_14"/>
         </tns:ConversationID>
         <tns:ConversationStatus xml:id="id_15" status="">
            <ns3:ReponseMessage xml:id="id_16"/>
            <ns6:Result status="{/ns0:ResponseHeader/ns6:Result/@status}" xml:id="id_17">
               <xsl:if test="/ns0:ResponseHeader/ns6:Result/@description" xml:id="id_18">
                  <xsl:attribute name="description" xml:id="id_19">
                     <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/@description" xml:id="id_20"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError" xml:id="id_21">
                  <ns5:CanonicalError xml:id="id_22">
                     <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError/@type" xml:id="id_23">
                        <xsl:attribute name="type" xml:id="id_24">
                           <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError/@type"
                                         xml:id="id_25"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError/@code" xml:id="id_26">
                        <xsl:attribute name="code" xml:id="id_27">
                           <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError/@code"
                                         xml:id="id_28"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError/@description" xml:id="id_29">
                        <xsl:attribute name="description" xml:id="id_30">
                           <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError/@description"
                                         xml:id="id_31"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:CanonicalError" xml:id="id_32"/>
                  </ns5:CanonicalError>
               </xsl:if>
               <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:SourceError" xml:id="id_33">
                  <ns5:SourceError xml:id="id_34">
                     <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/@code" xml:id="id_35">
                        <xsl:attribute name="code" xml:id="id_36">
                           <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/@code" xml:id="id_37"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/@description" xml:id="id_38">
                        <xsl:attribute name="description" xml:id="id_39">
                           <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/@description"
                                         xml:id="id_40"/>
                        </xsl:attribute>
                     </xsl:if>
                     <ns5:ErrorSourceDetails xml:id="id_41">
                        <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source"
                                xml:id="id_42">
                           <xsl:attribute name="source" xml:id="id_43">
                              <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source"
                                            xml:id="id_44"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details"
                                xml:id="id_45">
                           <xsl:attribute name="details" xml:id="id_46">
                              <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details"
                                            xml:id="id_47"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="/ns0:ResponseHeader/ns6:Result/ns5:SourceError/ns5:ErrorSourceDetails"
                                      xml:id="id_48"/>
                     </ns5:ErrorSourceDetails>
                  </ns5:SourceError>
               </xsl:if>
               <xsl:if test="/ns0:ResponseHeader/ns6:Result/ns6:CorrelativeErrors" xml:id="id_49">
                  <ns6:CorrelativeErrors xml:id="id_50">
                     <xsl:for-each select="/ns0:ResponseHeader/ns6:Result/ns6:CorrelativeErrors/ns5:SourceError"
                                   xml:id="id_51">
                        <ns5:SourceError xml:id="id_52">
                           <xsl:if test="@code" xml:id="id_53">
                              <xsl:attribute name="code" xml:id="id_54">
                                 <xsl:value-of select="@code" xml:id="id_55"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="@description" xml:id="id_56">
                              <xsl:attribute name="description" xml:id="id_57">
                                 <xsl:value-of select="@description" xml:id="id_58"/>
                              </xsl:attribute>
                           </xsl:if>
                           <ns5:ErrorSourceDetails xml:id="id_59">
                              <xsl:if test="ns5:ErrorSourceDetails/@source" xml:id="id_60">
                                 <xsl:attribute name="source" xml:id="id_61">
                                    <xsl:value-of select="ns5:ErrorSourceDetails/@source" xml:id="id_62"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <xsl:if test="ns5:ErrorSourceDetails/@details" xml:id="id_63">
                                 <xsl:attribute name="details" xml:id="id_64">
                                    <xsl:value-of select="ns5:ErrorSourceDetails/@details" xml:id="id_65"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <xsl:value-of select="ns5:ErrorSourceDetails" xml:id="id_66"/>
                           </ns5:ErrorSourceDetails>
                        </ns5:SourceError>
                     </xsl:for-each>
                  </ns6:CorrelativeErrors>
               </xsl:if>
            </ns6:Result>
         </tns:ConversationStatus>
         <tns:SourceComponent xml:id="id_67" operation="{/ns0:ResponseHeader/ns0:Trace/ns0:Service/@operation}"
                              component="{/ns0:ResponseHeader/ns0:Trace/ns0:Service/@name}"/>
      </tns:UpdStatusREQ>
   </xsl:template>
</xsl:stylesheet>
