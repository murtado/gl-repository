xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

import module  namespace nssif_header="htt://www.entel.cl/sif/MessageHeader/v1" at "../../../../../../SR_Commons/XQuery/ServiceComponent/SIF/module_MessageHeader_v1.xqy" ;

declare namespace ns1="http://www.entel.cl/EBM/Consumer/get/v1";
(:: import schema at "../../../../../ESC/Primary/get_Consumer_v1_EBM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/EBO/Consumer/v1";

declare namespace ns6 = "http://www.entel.cl/EBM/Consumer/get/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/get_Consumer_JSON_v1_EBM.xsd" ::)

declare variable $getConsumerReq as element() (:: schema-element(ns6:Get_Consumer_REQ) ::) external;

declare function local:func($getConsumerReq as element() (:: schema-element(ns6:Get_Consumer_REQ) ::)
                            ) 
                            as element() (:: schema-element(ns1:Get_Consumer_REQ) ::) {
    <ns1:Get_Consumer_REQ>
       
      {nssif_header:get_RequestHeader($getConsumerReq/*:RequestHeader)}
        <ns1:Body>
            <ns2:ConsumerID>{fn:data($getConsumerReq/ns6:Body/ns6:ConsumerID)}</ns2:ConsumerID>
        </ns1:Body>
    </ns1:Get_Consumer_REQ>
};

local:func($getConsumerReq)
