xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/EnterpriseService/v1";

declare function local:get_ESArchitecture() as element(){
    
    (: 
    This function holds the information require to describe this Service's Architecture (ESAS), at the Solution level.
    
    It should be kept updated over each modification made to the service, on which its Solution Architecture is modified.
    :)
    
    <ns1:EnterpriseService name="CollectionPostTransaction" code="BIM_0023" majorVersion="1" minorVersion="0">
	<ns1:ESC>
		<ns1:EBSC>
			<ns1:PrimaryContract name="CollectionPostTransaction_v1_ESC"/>
		</ns1:EBSC>
	</ns1:ESC>
	<ns1:MPL>
		<ns1:PIF name="CollectionPostTransaction_PIF"/>
        <ns1:SIF name="CollectionPostTransaction_SIF"/>
	</ns1:MPL>
	<ns1:CSL>
		<ns1:Country code="CHL">
			<ns1:OH opName="Publish" opCode="OPER_00330" name="CHL_Publish_CollectionPostTransaction"></ns1:OH>
		</ns1:Country>	
                <ns1:Country code="PER">
			<ns1:OH opName="Publish" opCode="OPER_00330" name="PER_Publish_CollectionPostTransaction"></ns1:OH>
		</ns1:Country>
	</ns1:CSL>
  </ns1:EnterpriseService>
};

local:get_ESArchitecture()