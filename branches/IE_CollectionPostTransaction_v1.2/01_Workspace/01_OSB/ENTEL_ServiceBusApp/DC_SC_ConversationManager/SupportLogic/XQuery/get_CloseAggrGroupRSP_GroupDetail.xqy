xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/ConversationManager/closeAggrGroup/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/closeAggrGroup_ConversationManager_v1_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getClosedCBGroups";
(:: import schema at "../JCA/getClosedCBGroups/getClosedCBGroups_sp.xsd" ::)

declare namespace ns5 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace cal = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare variable $ClosedGroup as element() (:: element(*, ns1:ENTEL_DEV.CB_GRP_T) ::) external;

declare variable $Result as element() (:: schema-element(ns3:Result) ::) external;

declare function local:get_CloseAggrGroupRSP_GroupDetail($ClosedGroup as element() (:: element(*, ns1:ENTEL_DEV.CB_GRP_T) ::),$Result as element() (:: schema-element(ns3:Result) ::)) as element() (:: schema-element(ns2:CloseAggrGroupRSP) ::) {
    <ns2:CloseAggrGroupRSP>
        {$Result}
        <cal:CbAggrGroupDetail>
            {fn-bea:inlinedXML($ClosedGroup/ns1:CB_GRP_HEADER_)}
            <cal:CbGroup>
                {
                    if ($ClosedGroup/ns1:CB_GRP_SUB_ID_)
                    then attribute subID {fn:data($ClosedGroup/ns1:CB_GRP_SUB_ID_)}
                    else ()
                }
                {
                    if ($ClosedGroup/ns1:CB_GRP_TAG_)
                    then attribute cbAggregationGroup {fn:data($ClosedGroup/ns1:CB_GRP_TAG_)}
                    else ()
                }
            </cal:CbGroup>
        </cal:CbAggrGroupDetail>
        <cal:CbGroupMembers>
            {
                for $CB_GRP_MEMS_ITEM in $ClosedGroup/ns1:CB_GRP_MEMS/ns1:CB_GRP_MEMS_ITEM
                return 
                <cal:CbGroupMember cbID="{fn:data($CB_GRP_MEMS_ITEM/ns1:MEM_CB_ID)}" srvOper="{fn:data($CB_GRP_MEMS_ITEM/ns1:MEM_SRV_OP_)}"
                    srvName="{fn:data($CB_GRP_MEMS_ITEM/ns1:MEM_SRV_NAME_)}"
                    cnvStatus="{fn:data($CB_GRP_MEMS_ITEM/ns1:MEM_CNV_STATUS_)}"
                    cnvID="{fn:data($CB_GRP_MEMS_ITEM/ns1:MEM_CNV_ID_)}">
                    {
                        if ($CB_GRP_MEMS_ITEM/ns1:MEM_PROV_ID_)
                        then attribute provID {fn:data($CB_GRP_MEMS_ITEM/ns1:MEM_PROV_ID_)}
                        else ()
                    }</cal:CbGroupMember>
            }
        </cal:CbGroupMembers>
    </ns2:CloseAggrGroupRSP>
};

local:get_CloseAggrGroupRSP_GroupDetail($ClosedGroup,$Result)
