xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://v2.ws.poslog.xcenter.dtv/";
(:: import schema at "../XSD/PoslogObjReceiverApiService_schema1.xsd" ::)

declare variable $RSP as element() external;

declare function local:func($RSP as element()) as element() (:: schema-element(ns1:PoslogObjReceiverException) ::) {
    <ns1:PoslogObjReceiverException>
        <errorCode>{fn:data($RSP/*[1]/*[1]/*:Result/*[1]/@code)}</errorCode>
        <errorDetails>{fn:data($RSP/*[1]/*[1]/*:Result/*[1]/@description)}</errorDetails>
    </ns1:PoslogObjReceiverException>
};

local:func($RSP)
