----------------------------------------------------------  DDL for Table ESB_ENDPOINT--------------------------------------------------------    CREATE TABLE "ESB_ENDPOINT"    (	"OPERATION_ID" NUMBER NOT NULL, 	"TRANSPORT_ID" NUMBER NOT NULL, 	"INSTANCE" VARCHAR2 (50 BYTE), 	"CONFIG_ID" NUMBER NOT NULL, 	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 	"CREATIONDATE" DATE DEFAULT CURRENT_DATE,  	 CONSTRAINT "ESB_ENDPOINT_FK1" FOREIGN KEY ("OPERATION_ID")	  REFERENCES "ESB_SYSTEM_API_OPERATION" ("ID"), 	 CONSTRAINT "ESB_ENDPOINT_FK2" FOREIGN KEY ("CONFIG_ID")	  REFERENCES "ESB_CONFIG" ("ID"), 	 CONSTRAINT "ESB_ENDPOINT_FK3" FOREIGN KEY ("TRANSPORT_ID")	  REFERENCES "ESB_ENDPOINT_TRANSPORT" ("ID"),	 CONSTRAINT "ESB_ENDPOINT_STATUS_CK" CHECK ("RCD_STATUS" IN(0,1))   );   COMMENT ON COLUMN "ESB_ENDPOINT"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';