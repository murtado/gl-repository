SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		INSERT INTO ESB_ENDPOINT (OPERATION_ID,TRANSPORT_ID,INSTANCE,CONFIG_ID,RCD_STATUS) VALUES (( SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = 'ClientDelete'),( SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'HTTPSOAP12'),null,( SELECT ID FROM ESB_CONFIG WHERE NAME = 'SALESFORCE_CRM_HTTPSOAP12'),'1');
	END;
	
	BEGIN
		INSERT INTO ESB_ENDPOINT (OPERATION_ID,TRANSPORT_ID,INSTANCE,CONFIG_ID,RCD_STATUS) VALUES (( SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = 'ClientObtain'),( SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'HTTPSOAP12'),null,( SELECT ID FROM ESB_CONFIG WHERE NAME = 'SALESFORCE_CRM_HTTPSOAP12'),'1');
	END;
	
	BEGIN
		INSERT INTO ESB_ENDPOINT (OPERATION_ID,TRANSPORT_ID,INSTANCE,CONFIG_ID,RCD_STATUS) VALUES (( SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = 'UPD-CLI-DATA'),( SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'JMS'),null,( SELECT ID FROM ESB_CONFIG WHERE NAME = 'SALESFORCE_CRM_JMS'),'1');
	END;
	
END;
/

COMMIT;

