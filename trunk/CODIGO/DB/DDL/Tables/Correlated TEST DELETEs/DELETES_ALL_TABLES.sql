SPOOL salida_deletes_all_tables.txt
@DELETE_ESB_MAPPING.sql
@DELETE_ESB_ERROR_MAPPING.sql
@DELETE_ESB_ERROR_DIAGNOSIS.sql
@DELETE_ESB_ERROR_TREATMENT_PLAN.sql
@DELETE_ESB_ERROR_THERAPY.sql
@DELETE_ESB_CONSUMER_DETAILS.sql
@DELETE_ESB_CONSUMER.sql
@DELETE_ESB_ENDPOINT.sql
@DELETE_ESB_SYSTEM_API_OPERATION.sql
@DELETE_ESB_SYSTEM_API.sql
@DELETE_ESB_SYSTEM.sql
@DELETE_ESB_PARAMETER.sql
@DELETE_ESB_CONFIG_LIST.sql
@DELETE_ESB_CONFIG.sql
@DELETE_ESB_CHECK_CONFIG.sql
@DELETE_ESB_CHANNEL.sql
@DELETE_ESB_CDM_FIELD.sql
@DELETE_ESB_CDM_ENTITY.sql
@DELETE_ESB_CAPABILITY_CHECK.sql
@DELETE_ESB_CAPABILITY.sql
@DELETE_ESB_SERVICE.sql
@DELETE_ESB_CANONICAL_ERROR.sql
SPOOL OFF;


