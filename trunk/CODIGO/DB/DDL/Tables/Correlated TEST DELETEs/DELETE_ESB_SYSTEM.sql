SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		DELETE FROM  ESB_SYSTEM WHERE ID = (SELECT ID FROM ESB_SYSTEM WHERE CODE = 'SALESFORCE' AND NAME = 'SALESFORCE' AND DESCRIPTION = '....' AND RCD_STATUS = '1');
	END;
	
END;
/

COMMIT;