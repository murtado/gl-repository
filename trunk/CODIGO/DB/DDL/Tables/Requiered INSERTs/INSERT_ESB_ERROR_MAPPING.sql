SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		INSERT INTO ESB_ERROR_MAPPING(ID,ERROR_SOURCE,MODULE,SUB_MODULE,RAW_CODE,RAW_DESCRIPTION,CAN_ERR_ID,STATUS_ID,RCD_STATUS) values ( ESB_ERROR_MAPPING_SEQ.NEXTVAL,
																													( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),
																													'MessageManager', 
																													'checkIN',
																													'10',
																													'Transaction ONGOING',
																													( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10001' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')),
																													( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR'),
																													'1');
	END;
		
	BEGIN
		INSERT INTO ESB_ERROR_MAPPING(ID,ERROR_SOURCE,MODULE,SUB_MODULE,RAW_CODE,RAW_DESCRIPTION,CAN_ERR_ID,STATUS_ID,RCD_STATUS) values ( ESB_ERROR_MAPPING_SEQ.NEXTVAL,
																													( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),
																													'MessageManager', 
																													'checkIN',
																													'25',
																													'Duplicate Message',
																													( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10005' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')),
																													( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR'),
																													'1');
	END;	
		
	BEGIN	
		INSERT INTO ESB_ERROR_MAPPING(ID,ERROR_SOURCE,MODULE,SUB_MODULE,RAW_CODE,RAW_DESCRIPTION,CAN_ERR_ID,STATUS_ID,RCD_STATUS) values ( ESB_ERROR_MAPPING_SEQ.NEXTVAL,
																													( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),
																													'MessageManager', 
																													'checkIN',
																													'15',
																													'Transaction IDEMPOTENT',
																													( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10002' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')),
																													( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR'),
																													'1');
	END;	
		
	BEGIN
		INSERT INTO ESB_ERROR_MAPPING(ID,ERROR_SOURCE,MODULE,SUB_MODULE,RAW_CODE,RAW_DESCRIPTION,CAN_ERR_ID,STATUS_ID,RCD_STATUS) values ( ESB_ERROR_MAPPING_SEQ.NEXTVAL,
																													( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),
																													'MessageManager', 
																													'checkIN',
																													'21',
																													'Duplicate Message - Result is an OK',
																													( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10004' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')),
																													( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR'),
																													'1');
	END;	
		
	BEGIN	
		INSERT INTO ESB_ERROR_MAPPING(ID,ERROR_SOURCE,MODULE,SUB_MODULE,RAW_CODE,RAW_DESCRIPTION,CAN_ERR_ID,STATUS_ID,RCD_STATUS) values ( ESB_ERROR_MAPPING_SEQ.NEXTVAL,
																													( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW'),
																													'MessageManager', 
																													'checkIN',
																													'20',
																													'Duplicate Message - Result is an ERROR',
																													( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10003' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')),
																													( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR'),
																													'1');
	END;

END;
/

COMMIT;
