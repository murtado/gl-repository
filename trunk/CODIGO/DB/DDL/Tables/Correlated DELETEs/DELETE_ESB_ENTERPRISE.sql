SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		DELETE FROM ESB_ENTERPRISE WHERE ID = (SELECT ID FROM ESB_ENTERPRISE WHERE CODE = 'ENTEL' AND NAME = 'ENTEL' AND RCD_STATUS = '0');
	END;

END;

/

COMMIT;
