SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		DELETE FROM ESB_ENDPOINT_TRANSPORT WHERE ID = (SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'MQ' AND RCD_STATUS = '1');
	END;
	
	BEGIN
		DELETE FROM ESB_ENDPOINT_TRANSPORT WHERE ID = (SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'OSB' AND RCD_STATUS = '1'); 
	END;
	
	BEGIN
		DELETE FROM ESB_ENDPOINT_TRANSPORT WHERE ID = (SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'JNDI' AND RCD_STATUS = '1');
	END;
	
	BEGIN
		DELETE FROM ESB_ENDPOINT_TRANSPORT WHERE ID = (SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'JMS' AND RCD_STATUS = '1');
	END;
	
	BEGIN
		DELETE FROM ESB_ENDPOINT_TRANSPORT WHERE ID = (SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'HTTP' AND RCD_STATUS = '1');
	END;
		
	BEGIN	
		DELETE FROM ESB_ENDPOINT_TRANSPORT WHERE ID = (SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = 'HTTPSOAP12' AND RCD_STATUS = '1');
	END;

END;
/

COMMIT;

