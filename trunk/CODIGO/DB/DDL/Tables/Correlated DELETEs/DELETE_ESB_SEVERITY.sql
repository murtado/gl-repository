SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		DELETE FROM ESB_SEVERITY WHERE ID = (SELECT ID FROM ESB_SEVERITY WHERE "LEVEL" = 'INFO' AND DESCRIPTION = 'Log Info level' AND "ORDER" = '100' AND RCD_STATUS = '0');
	END;

	BEGIN
		DELETE FROM ESB_SEVERITY WHERE ID = (SELECT ID FROM ESB_SEVERITY WHERE "LEVEL" = 'WARNING' AND DESCRIPTION = 'Log Warning level' AND "ORDER" = '200' AND RCD_STATUS = '0');
	END;

	BEGIN
		DELETE FROM ESB_SEVERITY WHERE ID = (SELECT ID FROM ESB_SEVERITY WHERE "LEVEL" = 'DEBUG' AND DESCRIPTION = 'Log Debug level' AND "ORDER" = '0' AND RCD_STATUS = '0');
	END;
	
END;
/

COMMIT;

