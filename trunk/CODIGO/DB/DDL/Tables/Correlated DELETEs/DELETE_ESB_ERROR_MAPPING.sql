SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

BEGIN

	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ID = (SELECT ID FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'MessageManager' AND
																													SUB_MODULE = 'checkIN' AND
																													RAW_CODE = '10' AND
																													RAW_DESCRIPTION = 'Transaction ONGOING' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10001' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')) AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1');
	END;
		
	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ID = (SELECT ID FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'MessageManager' AND
																													SUB_MODULE = 'checkIN' AND
																													RAW_CODE = '25' AND
																													RAW_DESCRIPTION = 'Duplicate Message' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10005' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')) AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1');
	END;	
		
	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ID = (SELECT ID FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'MessageManager' AND
																													SUB_MODULE = 'checkIN' AND
																													RAW_CODE = '15' AND
																													RAW_DESCRIPTION = 'Transaction IDEMPOTENT' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10002' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')) AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1');
	END;	
		
	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ID = (SELECT ID FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'MessageManager' AND
																													SUB_MODULE = 'checkIN' AND
																													RAW_CODE = '21' AND
																													RAW_DESCRIPTION = 'Duplicate Message - Result is an OK' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10004' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')) AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1');
	END;	
		
	BEGIN
		DELETE FROM ESB_ERROR_MAPPING WHERE ID = (SELECT ID FROM ESB_ERROR_MAPPING WHERE ERROR_SOURCE = ( SELECT ID FROM ESB_SYSTEM WHERE CODE = 'FRW') AND
																													MODULE = 'MessageManager' AND
																													SUB_MODULE = 'checkIN' AND
																													RAW_CODE = '20' AND
																													RAW_DESCRIPTION = 'Duplicate Message - Result is an ERROR' AND
																													CAN_ERR_ID = ( SELECT ID FROM ESB_CANONICAL_ERROR WHERE CODE = '10003' AND TYPE_ID = (SELECT ID FROM ESB_CANONICAL_ERROR_TYPE WHERE TYPE = 'FWNE')) AND
																													STATUS_ID = ( SELECT ID FROM ESB_ERROR_STATUS_TYPE WHERE NAME = 'ERROR') AND
																													RCD_STATUS = '1');
	END;

END;
/

COMMIT;
