xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/RetryManager/Dispatcher";
(:: import schema at "../Schemas/DynamicRoutingDispatcher.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_NEW_RETRY";
(:: import schema at "../Schemas/SP_NEW_RETRY_sp.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/ESB_RETRYMANAGER_Pooling";
(:: import schema at "../Schemas/ESB_RETRYMANAGER_Pooling_table.xsd" ::)

declare variable $EsbRetrymanager as element() (:: schema-element(ns1:EsbRetrymanager) ::) external;
declare variable $OutputSpNewRetry as element() (:: schema-element(ns2:OutputParameters) ::) external;

declare function local:setRequestDynamicRetry($EsbRetrymanager as element() (:: schema-element(ns1:EsbRetrymanager) ::), 
                                              $OutputSpNewRetry as element() (:: schema-element(ns2:OutputParameters) ::)) 
                                              as element() (:: schema-element(ns3:RetryDispatcher_REQ) ::) {
    <ns3:RetryDispatcher_REQ>
        <ns3:EndpointURL>{fn:data($OutputSpNewRetry/ns2:P_URL)}</ns3:EndpointURL>
        <ns3:SOAPAction>{fn:data($OutputSpNewRetry/ns2:P_ACTION)}</ns3:SOAPAction>
        <ns3:Operation>{fn:data($OutputSpNewRetry/ns2:P_CAPATILITY)}</ns3:Operation>
        <ns3:Payload>{ ($EsbRetrymanager/ns1:requestBody)}</ns3:Payload>
    </ns3:RetryDispatcher_REQ>
};

local:setRequestDynamicRetry($EsbRetrymanager, $OutputSpNewRetry)
