xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $out as element() external;

declare function local:func($out as element()) {
    
    fn:serialize();
};

local:func($out)
