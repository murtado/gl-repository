<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns0="http://www.entel.cl/EBM/Consumer/upd/v1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tns="http://www.entel.cl/CSM/RA/SALESFORCE/CRM/UPD-CLI-DATA/v1" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 tns mhdr oraext xp20 xref socket dvm oraxsl"
                xmlns:ns3="http://www.entel.cl/ESO/Result/v2"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:ns1="http://www.entel.cl/ESO/MessageHeader/v1" xmlns:ns2="http://www.entel.cl/EBO/Consumer/v1"
                xmlns:vprop="http://docs.oasis-open.org/wsbpel/2.0/varprop"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ns4="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:ns5="http://www.entel.cl/ESO/Error/v1"
                xmlns:ns="http://xmlns.oracle.com/pcbpel/adapter/jms/ES_Consumer_v1/Update_Consumer_OH/jmsReference_Upd_RA_SALESFORCE"
                xmlns:cor="http://xmlns.oracle.com/ES_Consumer_v1/Update_Consumer_OH/Update_Consumer_OH_BPELProcess/correlationset"
                xmlns:client="http://xmlns.oracle.com/ES_Consumer_v1/Update_Consumer_OH/Update_Consumer_OH_BPELProcess"
                xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
                xmlns:plt="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:jca="http://xmlns.oracle.com/pcbpel/wsdl/jca/"
                xmlns:ns6="http://schemas.oracle.com/bpel/extension">
   <oracle-xsl-mapper:schema>
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/Update_Consumer_OH_BPELProcess.wsdl"/>
            <oracle-xsl-mapper:rootElement name="Upd_Consumer_REQ" namespace="http://www.entel.cl/EBM/Consumer/upd/v1"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/jmsReference_Upd_RA_SALESFORCE.wsdl"/>
            <oracle-xsl-mapper:rootElement name="SALESFORCE_CRM_UPD-CLI-DATA_REQ" namespace="http://www.entel.cl/CSM/RA/SALESFORCE/CRM/UPD-CLI-DATA/v1"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [WED FEB 24 08:20:12 ART 2016].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/">
      <tns:SALESFORCE_CRM_UPD-CLI-DATA_REQ>
         <ns1:RequestHeader>
            <ns1:Consumer sysCode="{/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Consumer/@sysCode}"
                          enterpriseCode="{/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Consumer/@enterpriseCode}"
                          countryCode="{/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Consumer/@countryCode}">
               <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Consumer"/>
            </ns1:Consumer>
            <ns1:Trace clientReqTimestamp="{/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@clientReqTimestamp}"
                       eventID="{/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@eventID}">
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@reqTimestamp">
                  <xsl:attribute name="reqTimestamp">
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@reqTimestamp"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@rspTimestamp">
                  <xsl:attribute name="rspTimestamp">
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@rspTimestamp"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@processID">
                  <xsl:attribute name="processID">
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@processID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@sourceID">
                  <xsl:attribute name="sourceID">
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@sourceID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@correlationEventID">
                  <xsl:attribute name="correlationEventID">
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@correlationEventID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@conversationID">
                  <xsl:attribute name="conversationID">
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@conversationID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@correlationID">
                  <xsl:attribute name="correlationID">
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/@correlationID"/>
                  </xsl:attribute>
               </xsl:if>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service">
                  <ns1:Service>
                     <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service/@code">
                        <xsl:attribute name="code">
                           <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service/@code"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service/@name">
                        <xsl:attribute name="name">
                           <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service/@name"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service/@operation">
                        <xsl:attribute name="operation">
                           <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service/@operation"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Trace/ns1:Service"/>
                  </ns1:Service>
               </xsl:if>
            </ns1:Trace>
            <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Channel">
               <ns1:Channel>
                  <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Channel/@name">
                     <xsl:attribute name="name">
                        <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Channel/@name"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Channel/@mode">
                     <xsl:attribute name="mode">
                        <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Channel/@mode"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns1:Channel"/>
               </ns1:Channel>
            </xsl:if>
            <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result">
               <ns3:Result status="{/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/@status}">
                  <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/@description">
                     <xsl:attribute name="description">
                        <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/@description"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError">
                     <ns5:CanonicalError>
                        <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError/@type">
                           <xsl:attribute name="type">
                              <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError/@type"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError/@code">
                           <xsl:attribute name="code">
                              <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError/@code"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError/@description">
                           <xsl:attribute name="description">
                              <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError/@description"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:CanonicalError"/>
                     </ns5:CanonicalError>
                  </xsl:if>
                  <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError">
                     <ns5:SourceError>
                        <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/@code">
                           <xsl:attribute name="code">
                              <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/@code"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/@description">
                           <xsl:attribute name="description">
                              <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/@description"/>
                           </xsl:attribute>
                        </xsl:if>
                        <ns5:ErrorSourceDetails>
                           <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source">
                              <xsl:attribute name="source">
                                 <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/ns5:ErrorSourceDetails/@source"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details">
                              <xsl:attribute name="details">
                                 <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/ns5:ErrorSourceDetails/@details"/>
                              </xsl:attribute>
                           </xsl:if>
                           <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns5:SourceError/ns5:ErrorSourceDetails"/>
                        </ns5:ErrorSourceDetails>
                     </ns5:SourceError>
                  </xsl:if>
                  <xsl:if test="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns3:CorrelativeErrors">
                     <ns3:CorrelativeErrors>
                        <xsl:for-each select="/ns0:Upd_Consumer_REQ/ns1:RequestHeader/ns3:Result/ns3:CorrelativeErrors/ns5:SourceError">
                           <ns5:SourceError>
                              <xsl:if test="@code">
                                 <xsl:attribute name="code">
                                    <xsl:value-of select="@code"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <xsl:if test="@description">
                                 <xsl:attribute name="description">
                                    <xsl:value-of select="@description"/>
                                 </xsl:attribute>
                              </xsl:if>
                              <ns5:ErrorSourceDetails>
                                 <xsl:if test="ns5:ErrorSourceDetails/@source">
                                    <xsl:attribute name="source">
                                       <xsl:value-of select="ns5:ErrorSourceDetails/@source"/>
                                    </xsl:attribute>
                                 </xsl:if>
                                 <xsl:if test="ns5:ErrorSourceDetails/@details">
                                    <xsl:attribute name="details">
                                       <xsl:value-of select="ns5:ErrorSourceDetails/@details"/>
                                    </xsl:attribute>
                                 </xsl:if>
                                 <xsl:value-of select="ns5:ErrorSourceDetails"/>
                              </ns5:ErrorSourceDetails>
                           </ns5:SourceError>
                        </xsl:for-each>
                     </ns3:CorrelativeErrors>
                  </xsl:if>
               </ns3:Result>
            </xsl:if>
         </ns1:RequestHeader>
         <tns:Body>
            <ns2:ConsumerID>
               <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns0:Body/ns2:ConsumerID"/>
            </ns2:ConsumerID>
            <ns2:Consumer>
               <ns2:ConsumerName>
                  <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns0:Body/ns2:Consumer/ns2:ConsumerName"/>
               </ns2:ConsumerName>
               <ns2:ConsumerSurename>
                  <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns0:Body/ns2:Consumer/ns2:ConsumerSurename"/>
               </ns2:ConsumerSurename>
               <xsl:if test="/ns0:Upd_Consumer_REQ/ns0:Body/ns2:Consumer/ns2:ConsumerAge">
                  <ns2:ConsumerAge>
                     <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns0:Body/ns2:Consumer/ns2:ConsumerAge"/>
                  </ns2:ConsumerAge>
               </xsl:if>
               <ns2:ConsumerStatus>
                  <xsl:value-of select="/ns0:Upd_Consumer_REQ/ns0:Body/ns2:Consumer/ns2:ConsumerStatus"/>
               </ns2:ConsumerStatus>
            </ns2:Consumer>
         </tns:Body>
      </tns:SALESFORCE_CRM_UPD-CLI-DATA_REQ>
   </xsl:template>
</xsl:stylesheet>
