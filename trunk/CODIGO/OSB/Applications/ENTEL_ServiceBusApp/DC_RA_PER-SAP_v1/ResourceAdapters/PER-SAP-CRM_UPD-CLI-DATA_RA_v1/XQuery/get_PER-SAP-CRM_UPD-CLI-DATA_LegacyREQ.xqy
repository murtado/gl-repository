xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/SAP_Update.xsd" ::)
declare namespace ns1="http://www.entel.cl/CSM/RA/PER/SAP/CRM/UPD-CLI-DATA/v1";
(:: import schema at "../XSD/PER-SAP-CRM_UPD-CLI-DATA_v1_CSM.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/EBO/Consumer/v1";

declare variable $Req as element() (:: schema-element(ns1:PER-SAP-CRM_UPD-CLI-DATA_REQ) ::) external;

declare function local:get_PER-SAP-CRM_UPD-CLI-DATA_LegacyREQ($Req as element() (:: schema-element(ns1:PER-SAP-CRM_UPD-CLI-DATA_REQ) ::)) as element() (:: schema-element(SAP_REQ_UPDATE) ::) {
    <SAP_REQ_UPDATE>
        <Cliente_ID>{xs:string(fn:data($Req/ns1:Body/ns2:ConsumerID))}</Cliente_ID>
        <Cliente_Nombre>{xs:string(fn:data($Req/ns1:Body/ns2:Consumer/ns2:ConsumerName))}</Cliente_Nombre>
        <Cliente_Apellido>{xs:string(fn:data($Req/ns1:Body/ns2:Consumer/ns2:ConsumerSurename))}</Cliente_Apellido>
        <Cliente_Edad>{xs:integer(fn:data($Req/ns1:Body/ns2:Consumer/ns2:ConsumerAge))}</Cliente_Edad>
        <Cliente_Estado>{if(fn:data($Req/ns1:Body/ns2:Consumer/ns2:ConsumerStatus)='Active') then (1)else(0)  }</Cliente_Estado>
        <Codigo_Servicio>{fn:data($Req/ns1:Body/ns1:ServiceCode)}</Codigo_Servicio>
    </SAP_REQ_UPDATE>
};

local:get_PER-SAP-CRM_UPD-CLI-DATA_LegacyREQ($Req)
