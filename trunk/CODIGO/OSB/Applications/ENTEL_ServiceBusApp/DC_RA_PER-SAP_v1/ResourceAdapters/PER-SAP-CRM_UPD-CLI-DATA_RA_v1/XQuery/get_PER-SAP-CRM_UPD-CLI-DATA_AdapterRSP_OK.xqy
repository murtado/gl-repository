xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/SAP_Update.xsd" ::)
declare namespace ns1="http://www.entel.cl/CSM/RA/PER/SAP/CRM/UPD-CLI-DATA/v1";
(:: import schema at "../XSD/PER-SAP-CRM_UPD-CLI-DATA_v1_CSM.xsd" ::)

declare namespace ns6="http://www.entel.cl/ESO/MessageHeader/v1";

(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns4 = "http://www.entel.cl/EBO/Consumer/v1";

declare variable $SAP_RSP_UPDATE as element() (:: schema-element(SAP_RSP_UPDATE) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns6:RequestHeader) ::) external;
declare function local:get_PER-SAP-CRM_UPD-CLI-DATA_AdapterRSP_OK($SAP_RSP_UPDATE as element() (:: schema-element(SAP_RSP_UPDATE) ::)
                                                ,$RequestHeader as element() (:: schema-element(ns6:RequestHeader) ::)) 
                                                                  as element() (:: schema-element(ns1:PER-SAP-CRM_UPD-CLI-DATA_RSP) ::) {
    <ns1:PER-SAP-CRM_UPD-CLI-DATA_RSP>
        {$RequestHeader}	
        <ns1:Body>
            <ns4:Consumer>
                <ns4:ConsumerName>{fn:data($SAP_RSP_UPDATE/Cliente_Nombre)}</ns4:ConsumerName>
                <ns4:ConsumerSurename>{fn:data($SAP_RSP_UPDATE/Cliente_Apellido)}</ns4:ConsumerSurename>
                 <ns4:ConsumerAge>{fn:data($SAP_RSP_UPDATE/Cliente_Edad)}</ns4:ConsumerAge>
                 <ns4:ConsumerStatus>{if(xs:int(fn:data($SAP_RSP_UPDATE/Cliente_Estado))=1) then ('Active') else('Inactive')}</ns4:ConsumerStatus>           
            </ns4:Consumer>
        </ns1:Body>
    </ns1:PER-SAP-CRM_UPD-CLI-DATA_RSP>
};

local:get_PER-SAP-CRM_UPD-CLI-DATA_AdapterRSP_OK($SAP_RSP_UPDATE,$RequestHeader )
