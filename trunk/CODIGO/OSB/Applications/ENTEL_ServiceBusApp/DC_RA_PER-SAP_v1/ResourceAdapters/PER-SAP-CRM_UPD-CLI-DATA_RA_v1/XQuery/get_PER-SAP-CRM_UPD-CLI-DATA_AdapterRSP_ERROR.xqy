xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/CSM/RA/PER/SAP/CRM/UPD-CLI-DATA/v1";
(:: import schema at "../XSD/PER-SAP-CRM_UPD-CLI-DATA_v1_CSM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns1="http://www.entel.sap.per/";
(:: import schema at "../../../CommonResources/LegacyResources/PER-SAP-CRM/HTTPSOAP12/XSD/SAP_CommonAPI_v13.1.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns5 = "http://www.entel.cl/EBO/Consumer/v1";

declare namespace ns6="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $LegacyRSP as element()? external;
declare variable $RequestHeader as element() (:: schema-element(ns6:RequestHeader) ::) external;

declare function local:get_PER-SAP-CRM_UPD-CLI-DATA_AdapterRSP_ERROR($LegacyRSP as element()?, 
                                                                  $RequestHeader as element() (:: schema-element(ns6:RequestHeader) ::)) 
                                                                  as element() (:: schema-element(ns3:PER-SAP-CRM_UPD-CLI-DATA_RSP) ::) {
    <ns3:PER-SAP-CRM_UPD-CLI-DATA_RSP>
       {$RequestHeader}
    </ns3:PER-SAP-CRM_UPD-CLI-DATA_RSP>
};

local:get_PER-SAP-CRM_UPD-CLI-DATA_AdapterRSP_ERROR($LegacyRSP, $RequestHeader)