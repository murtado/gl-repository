xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/Error/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/Error_v1_ESO.xsd" ::)

declare variable $LegacyResponse as element() external;

declare function local:PER-SAP-CRM_JMS_FV($LegacyResponse as element())
    as xs:boolean {

      (:False = Error Response, True = OK Response:)
      if(data($LegacyResponse/*:StatusCode)='0') then
        true()
      else
        false()
};

local:PER-SAP-CRM_JMS_FV($LegacyResponse)