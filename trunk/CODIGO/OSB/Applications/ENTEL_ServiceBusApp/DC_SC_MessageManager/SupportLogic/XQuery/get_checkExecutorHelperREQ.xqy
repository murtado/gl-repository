xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace ns1="http://www.entel.cl/SC/MessageManager/CheckExecutorHelper/v1";
(:: import schema at "../XSD/CheckExecutorHelper.xsd" ::)

declare variable $name as xs:string external;
declare variable $type as xs:int external;
declare variable $grade as xs:int external;
declare variable $elapsedTime as xs:string external;
declare variable $capability as xs:string external;
declare variable $message as element() external;

declare variable $transaction_id as xs:int external;
declare variable $transaction_status as xs:int external;
declare variable $transaction_msgTimestamp as xs:dateTime external;
declare variable $transaction_sequence as xs:int external;

declare function local:checkExecutorHelperREQ($name as xs:string, 
                                              $type as xs:int, 
                                              $grade as xs:int, 
                                              $elapsedTime as xs:string,
                                              $capability as xs:string,
                                              $message as element(),
                                              $transaction_id as xs:int?,
                                              $transaction_status as xs:int?,
                                              $transaction_msgTimestamp as xs:dateTime?,
                                              $transaction_sequence as xs:int?) as element() (:: schema-element(ns1:checkExecutorHelperREQ) ::) {
    <ns1:checkExecutorHelperREQ name="{fn:data($name)}" type="{fn:data($type)}" grade="{fn:data($grade)}" elapsedTime="{fn:data($elapsedTime)}" capability="{fn:data($capability)}">
        <ns1:Message>{$message}</ns1:Message>
        {if($transaction_id) then
          <ns1:TransactionInfo id="{$transaction_id}" status="{$transaction_status}" message_timestamp="{$transaction_msgTimestamp}" sequence="{$transaction_sequence}"/>
         else ()
        }
    </ns1:checkExecutorHelperREQ>
};

local:checkExecutorHelperREQ($name, $type, $grade, $elapsedTime, $capability, $message, $transaction_id, $transaction_status, $transaction_msgTimestamp, $transaction_sequence)