xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL/SALESFORCE/CRM/ChangeLegalClientStatus/v1";
(:: import schema at "../XSD/CHL-SALESFORCE-CRM_ChangeLegalClientStatus_v1_CSM.xsd" ::)
declare namespace ns2="http://www.entel.salesforce.cl/";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SALESFORCE-CRM/HTTPSOAP12/XSD/SALESFORCE_CommonAPI_v13.1.xsd"  ::)

declare namespace ns3 = "http://www.entel.cl/EBO/Consumer/v1";

declare variable $GetMappingRSP as element() external;
declare variable $ChangeLegalClientStatus_REQ as element() (:: schema-element(ns1:CHL-SALESFORCE-CRM_ChangeLegalClientStatus_REQ) ::) external;

declare function local:get_CHL-SALESFORCE-CRM_ChangeLegalClientStatus_LegacyREQ.xqy($GetMappingRSP as element(), 
                                                                                $ChangeLegalClientStatus_REQ as element() (:: schema-element(ns1:CHL-SALESFORCE-CRM_ChangeLegalClientStatus_REQ) ::)) 
                                                                                as element() (:: schema-element(ns2:ChangeLegalClientStatus_REQUEST) ::) {
    <ns2:ChangeLegalClientStatus_REQUEST>
        <ns2:CLI_ID>{fn:data($ChangeLegalClientStatus_REQ/ns1:Body/ns3:ConsumerID)}</ns2:CLI_ID>
        <ns2:CLI_TYPE>{fn:data($GetMappingRSP)}</ns2:CLI_TYPE>
        <ns2:CLI_STAT>{fn:data($ChangeLegalClientStatus_REQ/ns1:Body/ns3:ConsumerStatus)}</ns2:CLI_STAT>
    </ns2:ChangeLegalClientStatus_REQUEST>
};

local:get_CHL-SALESFORCE-CRM_ChangeLegalClientStatus_LegacyREQ.xqy($GetMappingRSP, $ChangeLegalClientStatus_REQ)
