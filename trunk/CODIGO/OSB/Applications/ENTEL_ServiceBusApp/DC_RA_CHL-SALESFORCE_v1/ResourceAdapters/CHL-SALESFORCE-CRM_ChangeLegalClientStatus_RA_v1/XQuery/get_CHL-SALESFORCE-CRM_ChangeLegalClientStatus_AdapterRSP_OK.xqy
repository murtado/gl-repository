xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/CSM/RA/CHL/SALESFORCE/CRM/ChangeLegalClientStatus/v1";
(:: import schema at "../XSD/CHL-SALESFORCE-CRM_ChangeLegalClientStatus_v1_CSM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)
declare namespace ns1="http://www.entel.salesforce.cl/";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SALESFORCE-CRM/HTTPSOAP12/XSD/SALESFORCE_CommonAPI_v13.1.xsd" ::)
declare namespace ns5 = "http://www.entel.cl/EBO/Consumer/v1";
(:: import schema at "../../../../SR_Commons/XSD/EBO/Consumer_v1_EBO.xsd" ::)

declare variable $ChangeLegalClientStatus_RESPONSE as element() (:: schema-element(ns1:ChangeLegalClientStatus_RESPONSE) ::) external;
declare variable $Result as element() (:: schema-element(ns2:Result) ::) external;

declare function local:get_CHL-SALESFORCE-CRM_ChangeLegalClientStatus_AdapterRSP_OK($ChangeLegalClientStatus_RESPONSE as element() (:: schema-element(ns1:ChangeLegalClientStatus_RESPONSE) ::), 
                                                                                $Result as element() (:: schema-element(ns2:Result) ::)) 
                                                                                as element() (:: schema-element(ns3:CHL-SALESFORCE-CRM_ChangeLegalClientStatus_RSP) ::) {
    <ns3:CHL-SALESFORCE-CRM_ChangeLegalClientStatus_RSP>
           {$Result}
     {if ($ChangeLegalClientStatus_RESPONSE) then(
         <ns3:Body>
                <ns5:ConsumerStatus>{fn:data($ChangeLegalClientStatus_RESPONSE/ns1:CLI_STAT)}</ns5:ConsumerStatus>
         </ns3:Body>
         )else
          ()
     }
     </ns3:CHL-SALESFORCE-CRM_ChangeLegalClientStatus_RSP>
};

local:get_CHL-SALESFORCE-CRM_ChangeLegalClientStatus_AdapterRSP_OK($ChangeLegalClientStatus_RESPONSE, $Result)
