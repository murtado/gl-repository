xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SALESFORCENotification";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SALESFORCE-CRM/JMS_Billing/XSD/SALESFORCE_Notification.xsd" ::)

declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns3="http://www.entel.cl/CSM/RA/CHL/SALESFORCE/CRM/NOTIFY-TRANSFER-DATA/v1";
(:: import schema at "../XSD/CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_v1_CSM.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/EBO/Billing/v1";

declare variable $LegacyRSP as element()? (:: schema-element(ns1:SALESFORCE_Notification_RSP) ::) external;
declare variable $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::) external;

declare function local:get_CHL-SALESFORCE-CRM_UPD-CLI-DATA_AdapterRSP_OK($LegacyRSP as element()? (:: schema-element(ns1:SALESFORCE_Notification_RSP) ::), 
                                                                  $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::)) 
                                                                  as element() (:: schema-element(ns3:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_RSP) ::) {
    <ns3:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_RSP>
       {$RequestHeader}
        <ns3:Body>
            <ns4:TransferID>{fn:data($LegacyRSP/ns1:Body/ns1:TransferID)}</ns4:TransferID>
            {
                if ($LegacyRSP/ns1:Body/ns1:ClientID)
                then <ns4:ClientID>{fn:data($LegacyRSP/ns1:Body/ns1:ClientID)}</ns4:ClientID>
                else ()
            }
            <ns4:Notification>
                <ns4:NotifyChannel>{fn:data($LegacyRSP/ns1:Body/ns1:Notification/ns1:NotifyChannel)}</ns4:NotifyChannel>
                <ns4:NotifyCode>{fn:data($LegacyRSP/ns1:Body/ns1:Notification/ns1:NotifyCode)}</ns4:NotifyCode>
                <ns4:NotifyDescription>{fn:data($LegacyRSP/ns1:Body/ns1:Notification/ns1:NotifyDescription)}</ns4:NotifyDescription>
            </ns4:Notification>
        </ns3:Body>
    </ns3:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_RSP>
};

local:get_CHL-SALESFORCE-CRM_UPD-CLI-DATA_AdapterRSP_OK($LegacyRSP, $RequestHeader)