xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL/SALESFORCE/CRM/NOTIFY-TRANSFER-DATA/v1";
(:: import schema at "../XSD/CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_v1_CSM.xsd" ::)

declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $LegacyRSP as element()? external;
declare variable $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::) external;

declare function local:get_CHL-SALESFORCE-CRM_UPD-CLI-DATA_AdapterRSP_ERROR($LegacyRSP as element()?, 
                                                                  $RequestHeader as element() (:: schema-element(ns2:RequestHeader) ::)) 
                                                                  as element() (:: schema-element(ns1:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_RSP) ::) {
    <ns1:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_RSP>
       {$RequestHeader}
    </ns1:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_RSP>
};

local:get_CHL-SALESFORCE-CRM_UPD-CLI-DATA_AdapterRSP_ERROR($LegacyRSP, $RequestHeader)