xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/CHL/SALESFORCE/CRM/NOTIFY-TRANSFER-DATA/v1";
(:: import schema at "../XSD/CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_v1_CSM.xsd" ::)
declare namespace ns2="http://www.entel.cl/SALESFORCENotification";
(:: import schema at "../../../CommonResources/LegacyResources/CHL-SALESFORCE-CRM/JMS_Billing/XSD/SALESFORCE_Notification.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/EBO/Billing/v1";

declare variable $RaREQ as element() (:: schema-element(ns1:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_REQ) ::) external;

declare function local:get_CHL-SALESFORCE-CRM_NotifyTransfer_LegacyREQ($RaREQ as element() (:: schema-element(ns1:CHL-SALESFORCE-CRM_NOTIFY-TRANSFER-DATA_REQ) ::)) 
                                                                 as element() (:: schema-element(ns2:SALESFORCE_Notification_REQ) ::) {
    <ns2:SALESFORCE_Notification_REQ>
      <ns2:Body>
        <ns2:TransferID>{fn:data($RaREQ/ns1:Body/ns3:TransferID)}</ns2:TransferID>
         <ns2:Client>
            <ns2:ClientID>{fn:data($RaREQ/ns1:Body/ns3:Client/ns3:ClientID)}</ns2:ClientID>
             {
                 if ($RaREQ/ns1:Body/ns3:Client/ns3:ClientSurname)
                 then <ns2:ClientSurname>{fn:data($RaREQ/ns1:Body/ns3:Client/ns3:ClientSurname)}</ns2:ClientSurname>
                 else ()
             }
             {
                 if ($RaREQ/ns1:Body/ns3:Client/ns3:ClientName)
                 then <ns2:ClientName>{fn:data($RaREQ/ns1:Body/ns3:Client/ns3:ClientName)}</ns2:ClientName>
                 else ()
             }
             {
                 if ($RaREQ/ns1:Body/ns3:Client/ns3:NotificationEmail)
                 then <ns2:NotificationEmail>{fn:data($RaREQ/ns1:Body/ns3:Client/ns3:NotificationEmail)}</ns2:NotificationEmail>
                 else ()
             }
             {
                 if ($RaREQ/ns1:Body/ns3:Client/ns3:NotificationSMS)
                 then <ns2:NotificationSMS>{fn:data($RaREQ/ns1:Body/ns3:Client/ns3:NotificationSMS)}</ns2:NotificationSMS>
                 else ()
             }
             {
                 if ($RaREQ/ns1:Body/ns3:Client/ns3:NotificationHomeBanking)
                 then <ns2:NotificationHomeBanking>{fn:data($RaREQ/ns1:Body/ns3:Client/ns3:NotificationHomeBanking)}</ns2:NotificationHomeBanking>
                 else ()
             }
         </ns2:Client>
          <ns2:NotifyChannel>{fn:data($RaREQ/ns1:Body/ns1:NotifyChannel)}</ns2:NotifyChannel>
      </ns2:Body>
    </ns2:SALESFORCE_Notification_REQ>
};

local:get_CHL-SALESFORCE-CRM_NotifyTransfer_LegacyREQ($RaREQ)