xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $GetEndpoint as element() external;
declare function local:get_routeContext($GetEndpoint as element()) as element() {

    <ctx:route xmlns:ctx='http://www.bea.com/wli/sb/context'>
      <ctx:service isProxy="true">{data($GetEndpoint/*[1]/*[1]/OSB/@path)}</ctx:service>
    </ctx:route>
};

local:get_routeContext($GetEndpoint)