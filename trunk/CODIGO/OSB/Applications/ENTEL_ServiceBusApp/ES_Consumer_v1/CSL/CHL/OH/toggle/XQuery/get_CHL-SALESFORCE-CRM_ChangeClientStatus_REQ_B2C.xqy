xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/CSM/RA/CHL/SALESFORCE/CRM/ChangePhysicalClientStatus/v1";
(:: import schema at "../../../../../../DC_RA_CHL-SALESFORCE_v1/ResourceAdapters/CHL-SALESFORCE-CRM_ChangePhysicalClientStatus_RA_v1/XSD/CHL-SALESFORCE-CRM_ChangePhysicalClientStatus_CSM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/toggle/v1";
(:: import schema at "../../../../../ESC/Primary/toggle_Consumer_v1_EBM.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns7 = "http://www.entel.cl/EBO/Consumer/v1";

declare variable $Toggle_Consumer_REQ as element() (:: schema-element(ns1:Toggle_Consumer_REQ) ::) external;

declare function local:get_CHL-SALESFORCE-CRM_ChangePhysicalClientStatus_REQ($Toggle_Consumer_REQ as element() (:: schema-element(ns1:Toggle_Consumer_REQ) ::)) 
                                                                         as element() (:: schema-element(ns3:CHL-SALESFORCE-CRM_ChangePhysicalClientStatus_REQ) ::) {
    <ns3:CHL-SALESFORCE-CRM_ChangePhysicalClientStatus_REQ>
        <ns4:RequestHeader>{$Toggle_Consumer_REQ/*[1]/*}</ns4:RequestHeader>
        <ns3:Body>
            <ns7:ConsumerID>{fn:data($Toggle_Consumer_REQ/*[2]/*[1])}</ns7:ConsumerID>
            <ns7:ConsumerType>National</ns7:ConsumerType>
            <ns7:ConsumerStatus>{fn:data($Toggle_Consumer_REQ/*[2]/*[2])}</ns7:ConsumerStatus>
        </ns3:Body>   
    </ns3:CHL-SALESFORCE-CRM_ChangePhysicalClientStatus_REQ>
};

local:get_CHL-SALESFORCE-CRM_ChangePhysicalClientStatus_REQ($Toggle_Consumer_REQ)
