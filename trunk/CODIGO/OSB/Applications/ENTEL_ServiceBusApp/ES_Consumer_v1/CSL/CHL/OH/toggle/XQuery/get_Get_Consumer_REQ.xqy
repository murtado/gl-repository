xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/Consumer/get/v1";
(:: import schema at "../../../../../ESC/Primary/get_Consumer_v1_EBM.xsd" ::)
declare namespace ns1="http://www.entel.cl/EBM/Consumer/toggle/v1";
(:: import schema at "../../../../../ESC/Primary/toggle_Consumer_v1_EBM.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns6 = "http://www.entel.cl/EBO/Consumer/v1";

declare variable $Toggle_Consumer_REQ as element() (:: schema-element(ns1:Toggle_Consumer_REQ) ::) external;

declare function local:get_Get_Consumer_REQ($Toggle_Consumer_REQ as element() (:: schema-element(ns1:Toggle_Consumer_REQ) ::)) as element() (:: schema-element(ns2:Get_Consumer_REQ) ::) {
    <ns2:Get_Consumer_REQ>      
        <ns3:RequestHeader>            
        {$Toggle_Consumer_REQ/*[1]/*[1]}
        <ns3:Trace clientReqTimestamp="{fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@clientReqTimestamp)}" eventID="{fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@eventID)}">
                { 
                attribute reqTimestamp {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@reqTimestamp)}
                }
                {
                attribute processID {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@processID)}
                }
                {
                attribute sourceID {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@sourceID)}                   
                }
                {
                attribute correlationEventID {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@correlationEventID)}
                }
                {
                attribute conversationID {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@conversationID)}
                }
                {
                attribute correlationID {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/@correlationID)}
                }
                <ns3:Service>
                    {
                     attribute code {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/*[1]/@code)}
                    }
                    {
                    attribute name {fn:data($Toggle_Consumer_REQ/*[1]/*[2]/*[1]/@name)}                            
                    }
                    {
                    attribute operation {'get'}
                    }
                  </ns3:Service>                 
            </ns3:Trace>
        {$Toggle_Consumer_REQ/*[1]/*[3]}
        </ns3:RequestHeader>
        <ns2:Body>
            <ns6:ConsumerID>{fn:data($Toggle_Consumer_REQ/*[2]/*[1])}</ns6:ConsumerID>
        </ns2:Body>       
    </ns2:Get_Consumer_REQ>
};

local:get_Get_Consumer_REQ($Toggle_Consumer_REQ)
