xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/EBM/Consumer/toggle/v1";
(:: import schema at "../../../../../ESC/Primary/toggle_Consumer_v1_EBM.xsd" ::)
declare namespace ns7="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns1 = "http://www.entel.cl/EBO/Consumer/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";


declare variable $ConsumerStatus as xs:string external;
declare variable $RequestHeader as element() (:: schema-element(ns7:RequestHeader) ::) external;
declare variable $ResponseHeader as element() (:: schema-element(ns7:ResponseHeader) ::) external;
declare variable $Result as element() (:: schema-element(ns4:Result) ::) external;

declare function local:get_Toggle_Consumer_RSP($ConsumerStatus as xs:string, 
                                               $RequestHeader as element() (:: schema-element(ns7:RequestHeader) ::),
                                               $ResponseHeader as element() (:: schema-element(ns7:ResponseHeader) ::),
                                               $Result as element() (:: schema-element(ns4:Result) ::)) 
                                               as element() (:: schema-element(ns2:Toggle_Consumer_RSP) ::) {
    <ns2:Toggle_Consumer_RSP>
    <ns7:ResponseHeader>       
        {$RequestHeader/*}
        {$ResponseHeader/*[1]}
        {$Result}
        </ns7:ResponseHeader>
    <ns2:Body>
        <ns1:ConsumerStatus>{$ConsumerStatus}</ns1:ConsumerStatus>
    </ns2:Body>
   
    
    </ns2:Toggle_Consumer_RSP>
};

local:get_Toggle_Consumer_RSP($ConsumerStatus, $RequestHeader,$ResponseHeader,$Result)
