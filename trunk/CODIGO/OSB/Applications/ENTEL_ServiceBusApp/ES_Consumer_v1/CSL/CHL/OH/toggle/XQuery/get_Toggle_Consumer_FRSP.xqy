xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/Consumer/toggle/v1";
(:: import schema at "../../../../../ESC/Primary/toggle_Consumer_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare variable $ResponseHeader as element() (:: schema-element(ns2:ResponseHeader) ::) external;

declare function local:get_Toggle_Consumer_FRSP($ResponseHeader as element() (:: schema-element(ns2:ResponseHeader) ::)) as element() (:: schema-element(ns1:Toggle_Consumer_FRSP) ::) {
    <ns1:Toggle_Consumer_FRSP>
     {$ResponseHeader}
        <ns2:Body></ns2:Body>
    </ns1:Toggle_Consumer_FRSP>
};

local:get_Toggle_Consumer_FRSP($ResponseHeader)
