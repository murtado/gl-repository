xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EBM/Consumer/put/JSON/v1";
(:: import schema at "../../../../../ESC/Secondary/put_Consumer_JSON_v1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/EBM/Consumer/put/v1";
(:: import schema at "../../../../../ESC/Primary/put_Consumer_v1_EBM.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/EBO/Consumer/v1";

declare namespace ns6 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace ns7 = "http://www.entel.cl/ESO/MessageHeaderJSON/v1";

declare variable $PutConsumerREQ as element() (:: schema-element(ns1:Put_Consumer_REQ) ::) external;

declare function local:func($PutConsumerREQ as element() (:: schema-element(ns1:Put_Consumer_REQ) ::)) as element() (:: schema-element(ns2:Put_Consumer_REQ) ::) {
    <ns2:Put_Consumer_REQ>
        <ns4:RequestHeader>
            <ns4:Consumer
                sysCode="{fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Consumer/ns7:sysCode)}" enterpriseCode="{fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Consumer/ns7:enterpriseCode)}" countryCode="{fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Consumer/ns7:countryCode)}">
            </ns4:Consumer>
            <ns4:Trace clientReqTimestamp="{fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:clientReqTimestamp)}" eventID="{fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:eventID)}">
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:reqTimestamp)
                    then attribute reqTimestamp {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:reqTimestamp)}
                    else ()
                }
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:rspTimestamp)
                    then attribute rspTimestamp {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:rspTimestamp)}
                    else ()
                }
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:processID)
                    then attribute processID {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:processID)}
                    else ()
                }
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:sourceID)
                    then attribute sourceID {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:sourceID)}
                    else ()
                }
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:correlationEventID)
                    then attribute correlationEventID {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:correlationEventID)}
                    else ()
                }
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:conversationID)
                    then attribute conversationID {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:conversationID)}
                    else ()
                }
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:correlationID)
                    then attribute correlationID {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:correlationID)}
                    else ()
                }
                <ns4:Service>
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:Service/ns7:code)
                        then attribute code {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:Service/ns7:code)}
                        else ()
                    }
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:Service/ns7:name)
                        then attribute name {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:Service/ns7:name)}
                        else ()
                    }
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:Service/ns7:operation)
                        then attribute operation {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Trace/ns7:Service/ns7:operation)}
                        else ()
                    }
                </ns4:Service>
            </ns4:Trace>
            <ns4:Channel>
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Channel/ns7:name)
                    then attribute name {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Channel/ns7:name)}
                    else ()
                }
                {
                    if ($PutConsumerREQ/ns7:RequestHeader/ns7:Channel/ns7:mode)
                    then attribute mode {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Channel/ns7:mode)}
                    else ()
                }
            </ns4:Channel>
            <ns5:Result status="{fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:status)}">
                <ns6:CanonicalError>
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:CanonicalError/ns7:code)
                        then attribute code {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:CanonicalError/ns7:code)}
                        else ()
                    }
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:CanonicalError/ns7:description)
                        then attribute description {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:CanonicalError/ns7:description)}
                        else ()
                    }
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:CanonicalError/ns7:type)
                        then attribute type {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:CanonicalError/ns7:type)}
                        else ()
                    }
                </ns6:CanonicalError>
                <ns6:SourceError>
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:code)
                        then attribute code {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:code)}
                        else ()
                    }
                    {
                        if ($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:description)
                        then attribute description {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:description)}
                        else ()
                    }
                    <ns6:ErrorSourceDetails>
                        {
                            if ($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:ErrorSourceDetails/ns7:source)
                            then attribute source {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:ErrorSourceDetails/ns7:source)}
                            else ()
                        }
                        {
                            if ($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:ErrorSourceDetails/ns7:details)
                            then attribute details {fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:ErrorSourceDetails/ns7:details)}
                            else ()
                        }
                    </ns6:ErrorSourceDetails>
                    <ns6:SourceFault>{fn:data($PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:SourceError/ns7:SourceFault)}</ns6:SourceFault>
                </ns6:SourceError>
                <ns5:CorrelativeErrors>
                    {
                        for $SourceError in $PutConsumerREQ/ns7:RequestHeader/ns7:Result/ns7:CorrelativeErrors/ns7:SourceError
                        return 
                        <ns6:SourceError>
                            {
                                if ($SourceError/ns7:code)
                                then attribute code {fn:data($SourceError/ns7:code)}
                                else ()
                            }
                            {
                                if ($SourceError/ns7:description)
                                then attribute description {fn:data($SourceError/ns7:description)}
                                else ()
                            }
                            <ns6:ErrorSourceDetails>
                                {
                                    if ($SourceError/ns7:ErrorSourceDetails/ns7:source)
                                    then attribute source {fn:data($SourceError/ns7:ErrorSourceDetails/ns7:source)}
                                    else ()
                                }
                                {
                                    if ($SourceError/ns7:ErrorSourceDetails/ns7:details)
                                    then attribute details {fn:data($SourceError/ns7:ErrorSourceDetails/ns7:details)}
                                    else ()
                                }
                            </ns6:ErrorSourceDetails>
                            <ns6:SourceFault>{fn:data($SourceError/ns7:SourceFault)}</ns6:SourceFault></ns6:SourceError>
                    }
                </ns5:CorrelativeErrors>
            </ns5:Result>
        </ns4:RequestHeader>
        <ns2:Body>
            <ns3:Consumer>
                <ns3:ConsumerName>{fn:data($PutConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerName)}</ns3:ConsumerName>
                <ns3:ConsumerSurename>{fn:data($PutConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerSurename)}</ns3:ConsumerSurename>
                <ns3:ConsumerAge>{fn:data($PutConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerAge)}</ns3:ConsumerAge>
                <ns3:ConsumerStatus>{fn:data($PutConsumerREQ/ns1:Body/ns1:Consumer/ns1:ConsumerStatus)}</ns3:ConsumerStatus>
            </ns3:Consumer>
        </ns2:Body>
    </ns2:Put_Consumer_REQ>
};

local:func($PutConsumerREQ)
