import shutil
import util
import os
import ConfigParser
import xml.etree.ElementTree as ET

root_path=os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')

def chdir_force(_dir):
	force = False
	if not(os.path.isdir(_dir)):
		force = True
		os.makedirs(_dir)
	os.chdir(_dir)
	return force

def get_System(_target):
	sysApi=_target['country']+	'-'+	_target['system']
	return sysApi

def get_SystemApi(_target):
	sysApi = get_System(_target)+'-'+_target['api']
	return sysApi

def get_TransportInstance(_target):
	transportInstanceName = _target['transport']
	if len(_target['instance']) > 0:
		transportInstanceName = transportInstanceName + '_' + _target['instance']
	return transportInstanceName

def get_TransportWrapperName(_target):
	transportWrapperName = get_SystemApi(_target) + '_' + get_TransportInstance(_target)
	return transportWrapperName

def getProjectName(_target):
	projectName = 'DC_LW_' + get_System(_target)+'_v'+_target['version']
	return projectName	

def build_project(_target):

	projectName = getProjectName(_target)

	forceMkdir = chdir_force(projectName)

	#project.jpr
	project_file=projectName+'.jpr'
	if not os.path.isfile(project_file):
		with open(project_file, 'w') as new_file:
			with open((root_path+'/TEMPLATES/LW/TEMPLATE.jpr'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%PROJECT_NAME%', projectName).replace('%DEFUALT_PACKAGE_PROJECT_NAME%', projectName.lower().replace('-',''))
					new_file.write(new_line)
		print project_file + ' creado'
	else:
		print 'El archivo ' + project_file + ' ya existe'

	#pom.xml
	pom_file='pom.xml'
	if not os.path.isfile(pom_file):
		with open(pom_file, 'w') as new_file:
			with open((root_path+'/TEMPLATES/LW/POM_TEMPLATE.xml'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%PROJECT_NAME%', projectName)
					new_file.write(new_line)
		print pom_file + ' creado'
	else:
		print 'El archivo ' + pom_file + ' ya existe'
	return forceMkdir

def replaceComponentSystemApiOperation(_to, _from):
	with open(_to, 'w') as new_file:
		with open(_from, 'r') as template_file:
			for line in template_file:
				new_line=line.replace('%SYSTEM_API%', country_system_api).replace('%OPERATION%',_target['operation'])
				new_file.write(new_line)

def build(_target, workspace):
	print '\n---------------------------------------------\n'
	
	print 'Armado projecto LW: ' + getProjectName(_target)

	country_system=get_System(_target)
	project_name=getProjectName(_target)

	print 'Creaction de carpeta del proyecto: ' + project_name
	isBuilder = build_project(_target)

	country_system_api=get_SystemApi(_target)

	print 'Creacion de Wrappers'
	#Creacion de CSC, sus xsd y wsdl
	chdir_force('Wrapper')

	#Create proxy y pipeline de RA
			
	lw_pipeline=country_system+'_LW.pipeline'
	if not os.path.isfile(lw_pipeline):
		with open(lw_pipeline, 'w') as new_file:
			with open((root_path+'/TEMPLATES/LW/Wrapper/LW.pipeline'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system )
					new_file.write(new_line)
		print lw_pipeline + ' creado'
	else:
		print 'El pipeline ' + lw_pipeline+ ' ya existe'
	lw_proxy=country_system+'_LW.proxy'
	if not os.path.isfile(lw_proxy):
		with open(lw_proxy, 'w') as new_file:
			with open((root_path+'/TEMPLATES/LW/Wrapper/LW.proxy'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system )
					new_file.write(new_line)
		print lw_proxy + ' creado'
	else:
		print 'El proxy ' + lw_proxy+ ' ya existe'

	lw_ac_pipeline=country_system+'_LW_AC.pipeline'
	if not os.path.isfile(lw_ac_pipeline):
		with open(lw_ac_pipeline, 'w') as new_file:
			with open((root_path+'/TEMPLATES/LW/Wrapper/LW_AC.pipeline'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system )
					new_file.write(new_line)
		print lw_ac_pipeline + ' creado'
	else:
		print 'El pipeline ' + lw_ac_pipeline+ ' ya existe'

	os.chdir('../')

	chdir_force('APIWs')

	chdir_force(country_system_api)
	
	apiw_pipeline = country_system_api + '_APIW.pipeline'

	if not os.path.isfile(apiw_pipeline):
		with open(apiw_pipeline, 'w') as new_file:
			with open((root_path+'/TEMPLATES/LW/APIWs/API/APIW.pipeline'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system ).replace('%SYSTEM_API%', country_system_api )
					new_file.write(new_line)
		print 'Creacion de APIW: ' + apiw_pipeline + ' terminado'
	else:
		print 'El API Wrapper pipeline ' + apiw_pipeline + ' ya existe'

	apiw_ac_pipeline = country_system_api + '_APIW_AC.pipeline'
	if not os.path.isfile(apiw_ac_pipeline):
		with open(apiw_ac_pipeline, 'w') as new_file:
			with open((root_path+'/TEMPLATES/LW/APIWs/API/APIW_AC.pipeline'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system ).replace('%SYSTEM_API%', country_system_api )
					new_file.write(new_line)
		print 'Creacion de APIW: ' + apiw_ac_pipeline + ' terminado'
	else:
		print 'El API Wrapper pipeline ' + apiw_ac_pipeline + ' ya existe'

	chdir_force('TW')

	transportInstanceName = get_TransportInstance(_target)

	chdir_force(transportInstanceName)

	transportWrapperName = get_TransportWrapperName(_target)

	tw_pipeline = transportWrapperName + '_TW.pipeline'

	template_transport_path = root_path+ '/TEMPLATES/LW/APIWs/API/TW/'+ _target['type'] +'/'+ _target['transport']

	if not os.path.isfile(tw_pipeline):
		with open(tw_pipeline, 'w') as new_file:
			with open((template_transport_path +'/TW.pipeline'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system ).replace('%SYSTEM_API%', country_system_api ).replace('%TW_NAME%',transportWrapperName ).replace('%TRANSPORT_INSTANCE%', transportInstanceName)
					new_file.write(new_line)
		print 'Creacion de TransportWrapper Pipeline: ' + tw_pipeline + ' terminado'
	else:
		print 'El pipeline de Transport Wrapper ' + tw_pipeline + ' ya existe'

	tw_bix = transportWrapperName + '_TW.bix'

	if not os.path.isfile(tw_bix):
		with open(tw_bix, 'w') as new_file:
			with open((template_transport_path +'/TW.bix'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system ).replace('%SYSTEM_API%', country_system_api ).replace('%TW_NAME%',transportWrapperName ).replace('%TRANSPORT_INSTANCE%', transportInstanceName)
					new_file.write(new_line)
		print 'Creacion de TransportWrapper Pipeline: ' + tw_bix
	else:
		print 'El business service ' + tw_bix + ' ya existe'

	chdir_force('XQuery')
	
	tw_seb_xqy = transportWrapperName + '_SEB.xqy'
	if not os.path.isfile(tw_seb_xqy):
		with open(tw_seb_xqy, 'w') as new_file:
			with open((template_transport_path +'/XQuery/SEB.xqy'), 'r') as template_file:
				for line in template_file:
					new_line=line.replace('%SYSTEM%', country_system ).replace('%SYSTEM_API%', country_system_api ).replace('%TW_NAME%',transportWrapperName ).replace('%TRANSPORT_INSTANCE%', transportInstanceName)
					new_file.write(new_line)
		os.chdir('../')
	else:
		print 'La xquery ' + tw_seb_xqy + ' ya existe'
	
	if  _target['type'] == 'ASYNC':
		template_transport_path = root_path+ '/TEMPLATES/LW/APIWs/API/TW/'+ _target['type'] +'/'+ _target['transportAsync']

		tw_pipeline = transportWrapperName + '_TW_AC.pipeline'
		if not os.path.isfile(tw_pipeline):
			with open(tw_pipeline, 'w') as new_file:
				with open((template_transport_path +'/TW_AC.pipeline'), 'r') as template_file:
					for line in template_file:
						new_line=line.replace('%SYSTEM%', country_system ).replace('%SYSTEM_API%', country_system_api ).replace('%TW_NAME%',transportWrapperName ).replace('%TRANSPORT_INSTANCE%', transportInstanceName)
						new_file.write(new_line)
			print 'Creacion de TransportWrapper Pipeline: ' + tw_pipeline + ' terminado'
		else:
			print 'El pipeline de Transport Wrapper ' + tw_pipeline + ' ya existe'

		tw_ac_proxy = transportWrapperName + '_TW_AC.proxy'

		if not os.path.isfile(tw_ac_proxy):
			with open(tw_ac_proxy, 'w') as new_file:
				with open((template_transport_path +'/TW_AC.proxy'), 'r') as template_file:
					for line in template_file:
						new_line=line.replace('%SYSTEM%', country_system ).replace('%SYSTEM_API%', country_system_api ).replace('%TW_NAME%',transportWrapperName ).replace('%TRANSPORT_INSTANCE%', transportInstanceName)
						new_file.write(new_line)
			print 'Creacion de TransportWrapper Pipeline: ' + tw_ac_proxy
		else:
			print 'El business service ' + tw_ac_proxy + ' ya existe'

	#Return to Application ServiceBuss
	os.chdir(workspace)

	if isBuilder:
		try:
			newElement = ET.fromstring(('<hash><url n="URL" path="'+project_name+'/'+project_name+'.jpr"/></hash>'))
			applicationName=os.path.basename(os.path.normpath(workspace))+'.jws'
			applicationName = os.path.join(workspace, applicationName)
			tree = ET.parse(applicationName)
			root = tree.getroot()
			for ele in root.findall(".//*[@n='listOfChildren']"):
				ele.append(newElement)
				break
			tree.write(applicationName,  encoding="UTF-8")
			print 'JWS Actualizado exitosamente.'

		except Exception as e:
			print('Ocurrio un error al actualizar el archivo de aplicacion .jws')
	print '\n---------------------------------------------\n'

def getDictionary(config, section, workspace):
	dictionary = dict()
	dictionary['country'] = config.get(section, 'country')
	dictionary['system'] = config.get(section, 'system')
	dictionary['api'] = config.get(section, 'api')
	dictionary['version'] = config.get(section, 'version')
	dictionary['transport'] = config.get(section, 'transport')
	dictionary['instance'] = config.get(section, 'instance')
	if (config.get(section, 'type') == 'ASYNC'):
		dictionary['type'] = 'ASYNC'
	else:
		dictionary['type'] = 'SYNC'

	dictionary['transportAsync'] = config.get(section, 'transportAsync')
	return dictionary

def main():
	config = ConfigParser.ConfigParser()
	config.readfp(open('init.cfg'))

	print 'Creacion de contexto'
	workspace = config.get('WORKSPACE', 'path')
	config.remove_section('WORKSPACE')

	os.chdir(workspace)

	for target in config.sections():
		dictionary = getDictionary(config,target, workspace)
		build(dictionary, workspace)

main()