xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/CSM/RA/%SYSTEM_API%/%OPERATION%/v1";
(:: import schema at "../CSC/%SYSTEM_API%_%OPERATION%_v1_CSM.xsd" ::)


declare namespace ns2="http://REPLACE_WITH_NAMESPACE";
(: import schema at "../../../CommonResources/LegacyResources/%SYSTEM_API%/**.wsdl/xsd" :)

declare variable $%SYSTEM_API%_%OPERATION%_REQ as element() (:: schema-element(ns1:%SYSTEM_API%_%OPERATION%_REQ) ::) external;

declare function local:get_%SYSTEM_API%_%OPERATION%_LegacyREQ($%SYSTEM_API%_%OPERATION%_REQ as element() (:: schema-element(ns1:%SYSTEM_API%_%OPERATION%_REQ) ::)) as element()  {
    <STUB>{$%SYSTEM_API%_%OPERATION%_REQ}</STUB>
};

local:get_%SYSTEM_API%_%OPERATION%_LegacyREQ($%SYSTEM_API%_%OPERATION%_REQ)
