import shutil
import util
import os
import ConfigParser
import xml.etree.ElementTree as ET

root_path=os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
workspace=''

def chdir_force(_dir):
	force = False
	if not(os.path.isdir(_dir)):
		force = True
		os.makedirs(_dir)
	os.chdir(_dir)
	return force

def getPath(_target):
	path = '02_Entregas/'+_target['type']+'_'+_target['branch']+'_'+_target['branch_date_version']+'/'
	return path

def get_MDS_Path_Binary(_target):
	result = getPath(_target)+'03_Binarios/02_SOA/01_MDS/'
	print result
	return result

def get_MDS_Path_SRC(_target):
	result = getPath(_target)+'02_Fuentes/02_SOA/01_MDS/'
	print result
	return result

def get_SCA_Path_SRC(_target):
	result = getPath(_target)+'02_Fuentes/02_SOA/02_SCA/'
	print result
	return result

def get_SCA_Path_Binary(_target):
	result = getPath(_target)+'03_Binarios/02_SOA/02_SCA/'
	print result
	return result

def get_MDS(_target):
	result = get_MDS_Path_Binary(_target)+'/'+_target['country']+'_'+_target['branch'] + '_SOA_' + _target['release'] + '_MDS.zip'
	return result

def get_MDS_SRC(_target):
	result = get_MDS_Path_SRC(_target)+'/SRC_'+_target['country']+'_'+_target['branch'] + '_SOA_' + _target['release'] + '_MDS.rar'
	return result

def get_SCA_SRC(_target):
	result = get_SCA_Path_SRC(_target)+'/SRC_sca_'+_target['country']+'_SOA_'+_target['service'] + '_OH_' +_target['country']+'_'+ _target['release'] + '.rar'
	return result

def get_SCA_Binary(_target):
	result = get_SCA_Path_Binary(_target)+'/sca_'+_target['country']+'_SOA_'+_target['service'] + '_OH_' +_target['country']+'_'+ _target['release'] + '.jar'
	return result

def get_SCA_Binary_Config_Plan(_target):
	result = get_SCA_Path_Binary(_target)+'/sca_'+_target['country']+'_SOA_'+_target['service'] + '_OH_' +_target['country']+'_'+ _target['release'] + '_cfgplan.xml'
	return result

def get_OSB_SRC(_target):
	result = getPath(_target)+'02_Fuentes/01_OSB/SRC_OSB_'+_target['country']+'_OSB_' + _target['branch'] + '_' + _target['release'] + '.rar'
	return result

def get_OSB_Binary(_target):
	result = getPath(_target)+'03_Binarios/01_OSB/OSB_'+_target['country']+'_OSB_' + _target['branch'] + '_' + _target['release'] + '.jar'
	return result

def get_OSB_Custom_file(_target):
	result = getPath(_target)+'03_Binarios/01_OSB/OSB_'+_target['country']+'_OSB_' + _target['branch'] + '_' + _target['release'] + '_CustomizationFile.xml'
	return result

def get_CHANGE(_target):
	result = getPath(_target)+'03_Binarios/03_DB/SQL_CHANGE_'+_target['country']+'_FWR_' + _target['branch'] + '_' + _target['release'] + '.zip'
	return result

def get_ROLLBACK(_target):
	result = getPath(_target)+'03_Binarios/03_DB/SQL_ROLLBACK_'+_target['country']+'_FWR_' + _target['branch'] + '_' + _target['release'] + '.zip'
	return result

def get_ADP(_target):
	result = getPath(_target)+'03_Binarios/03_DB/SQL_ADP_'+_target['country']+'_ADP_' + _target['branch'] + '_' + _target['release'] + '.zip'
	return result


def createfile(_name):
	if not os.path.isfile(_name):
		with open(_name, 'w') as new_file:
			new_file.write('example file')
	else:
		print 'El archivo ' + _name + ' ya existe'

def chdir_force(_dir):
	force = False
	if not(os.path.isdir(_dir)):
		force = True
		os.makedirs(_dir)
	os.chdir(_dir)
	return force


def build(_target, workspace):
	print '\n---------------------------------------------\n'
	
	createfile(get_OSB_Binary(_target))
	createfile(get_OSB_Custom_file(_target))
	createfile(get_OSB_SRC(_target))
	createfile(get_CHANGE(_target))
	createfile(get_ROLLBACK(_target))
	createfile(get_ADP(_target))
	chdir_force(get_MDS_Path_Binary(_target))
	os.chdir(workspace)
	createfile(get_MDS(_target))
	chdir_force(get_SCA_Path_Binary(_target))
	os.chdir(workspace)
	createfile(get_SCA_Binary(_target))
	createfile(get_SCA_Binary_Config_Plan(_target))
	chdir_force(get_MDS_Path_SRC(_target))
	os.chdir(workspace)
	createfile(get_MDS_SRC(_target))
	chdir_force(get_SCA_Path_SRC(_target))
	os.chdir(workspace)
	createfile(get_SCA_SRC(_target))
	




def getDictionary(config, section, workspace):
	dictionary = dict()
	dictionary['country'] = config.get(section, 'country')
	dictionary['release'] = config.get(section, 'release')
	dictionary['branch'] = config.get(section, 'branch')
	dictionary['branch_date_version'] = config.get(section, 'branch_date_version')
	dictionary['type'] = config.get(section, 'type')
	dictionary['capability'] = config.get(section, 'capability')
	dictionary['service'] = config.get(section, 'service')

	return dictionary

def main():
	config = ConfigParser.ConfigParser()
	config.readfp(open('init.cfg'))
	print 'Creacion de entrega'
	workspace = config.get('WORKSPACE', 'path')
	config.remove_section('WORKSPACE')
	for target in config.sections():
		os.chdir(workspace)
		dictionary = getDictionary(config,target, workspace)
		build(dictionary, workspace)
	
main()