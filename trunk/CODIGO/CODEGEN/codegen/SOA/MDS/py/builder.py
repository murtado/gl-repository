import os, sys

root_path=os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')

application = sys.argv[1]
branchName = sys.argv[2]

os.chdir(application)

with open(('BRANCHNAME.jws'), 'r') as old_file:
	with open((branchName+'_MDS.jws'), 'w') as new_file:
		for line in old_file:
			new_line=line.replace('%BRANCHNAME%', branchName).replace('%BRANCHNAME_LOWER%', branchName.lower())
			new_file.write(new_line)

os.remove('BRANCHNAME.jws')

os.chdir('.adf')
os.chdir('META-INF')

with open(('adf-config.xml'), 'r') as old_file:
	with open(('tmp.xml'), 'w') as new_file:
		for line in old_file:
			new_line=line.replace('%BRANCHNAME%', branchName)
			new_file.write(new_line)

os.remove('adf-config.xml')
os.rename('tmp.xml', 'adf-config.xml')
os.chdir('../../')

os.chdir('Commons')



with open(('Commons.jpr'), 'r') as old_file:
	with open(('tmp.xml'), 'w') as new_file:
		for line in old_file:
			new_line=line.replace('%BRANCHNAME%', branchName).replace('%BRANCHNAME_LOWER%', branchName.lower())
			new_file.write(new_line)
os.remove('Commons.jpr')
os.rename('tmp.xml', 'Commons.jpr')

os.chdir('../')