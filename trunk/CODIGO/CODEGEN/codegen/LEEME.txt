04102016- FIX en creacion de Proxy de LW. Correccion en checkeo de existencia de componente
05102016- Agregado de generador de MDS
06102016- Borrado de codigo innecesario
07102016- FIX en Endpoint de PIF
11102016- Cambio en template de ES, Proxy de la PIF utiliza PortType, y no Binding
16102016- LW- Diferenciacion entre HTTPSOAP11 y HTTPSOAP12 agregada, mas template sin JCA de Transport JDBC y BS dummy
16102016- Agregado de de generador de mapping a partir del RD, exportado como .csv.
26102016- Agregado un nuevo atributo al init.cfg. el cual indica si este es Sync o Async, dependiendo de esto,crea en base a un template u otro