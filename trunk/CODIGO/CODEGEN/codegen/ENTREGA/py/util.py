import sys
import os
import xml.etree.ElementTree as ET

filename = sys.argv[1]
dest = sys.argv[2]

tree = ET.parse(filename)
root = tree.getroot()
name = root.get('name')
file = open(dest, 'w')
file.write(name)
