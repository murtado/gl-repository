import sys
import os
import xml.etree.ElementTree as ET

filename = sys.argv[1]
dest = sys.argv[2]
tree = ET.parse(filename)
root = tree.getroot()
composite= root.find('{http://schemas.oracle.com/soa/configplan}composite')
name = composite.get('name')

file = open(dest, 'w')
file.write(name)
