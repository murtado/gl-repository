import os
import sys
import ConfigParser

def getDestinationCode( _code, _hash ):
	destination = ''
	if (_hash.lower() == 'duplicate'):
		destination = _code + _code
	elif (_hash.lower() == 'upper'):
		destination = _code.upper()
	elif (_hash.lower() == 'lower'):
		destination = _code.lower()
	elif (_hash.lower() == 'first'):
		destination = _code[1]
	else:
		destination = _code
	return destination

def generateConfig(_path):
	_config = ConfigParser.ConfigParser()
	_config.readfp(open(_path))
	return _config

context = sys.argv[1]
source_system = sys.argv[2]
destination_system = sys.argv[3]

try:
	digest = sys.argv[4]
except Exception as e:
	digest = 'default'
	pass

print( context+ ' - '+ source_system + '' +destination_system)

path = 'rd.csv'
sqlPath = 'file.sql'

config = generateConfig('./template.cfg')

with open(path,'r') as file:

	with open(sqlPath, 'w') as newSQL:

		newSQL.write('DECLARE\n\tV_SOURCE_SYSTEM ESB_SYSTEM.ID%TYPE;\n\tV_DESTINATION_SYSTEM ESB_SYSTEM.ID%TYPE;\n\tV_ENTITY_ID ESB_CDM_ENTITY.ID%TYPE;\n\tV_FIELD_ID ESB_CDM_FIELD.ID%TYPE;\n\tBEGIN\n')

		newLine = config.get('QUERIES', 'source_system').replace('%SOURCE_SYSTEM_CODE%', source_system)
		newSQL.write(newLine+'\n')

		newLine = config.get('QUERIES', 'destination_system').replace('%DESTINATION_SYSTEM_CODE%', destination_system)
		newSQL.write(newLine+'\n')	

		entity = ''
		field = ''

		for line in file:

			print(line)
			entityField = line.split(';')[0]
			print('------------' + entityField)
			newEntity = entityField.split('.')[0]
			newField = entityField.split('.')[1]
			sourceCode = line.split(';')[1]
			destinationCode = getDestinationCode(sourceCode,digest)
			if entity != newEntity:
				entity = newEntity
				newLine = config.get('QUERIES', 'entity').replace('%ENTITY_NAME%',entity ).replace('\\t', '\t').replace('\\n', '\n')
				newSQL.write(newLine)
				
			if field != newField:
				
				field = newField
				newLine = config.get('QUERIES', 'field').replace('%FIELD_NAME%',field ).replace('\\t', '\t').replace('\\n', '\n')
				newSQL.write(newLine)

			newLine = config.get('QUERIES', 'mapping').replace('%CONTEXT%', context).replace('%SOURCE_CODE%', sourceCode).replace('%DESTINATION_CODE%', destinationCode).replace('\\t', '\t').replace('\\n', '\n')
			newSQL.write(newLine)

			newLine = config.get('QUERIES', 'mappingReturn').replace('%CONTEXT%', context).replace('%SOURCE_CODE%', sourceCode).replace('%DESTINATION_CODE%', destinationCode).replace('\\t', '\t').replace('\\n', '\n')
			newSQL.write(newLine)

		newSQL.write('\nEND;')