import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='IE_EventPooling_SIM_v1.0'
legacyName='EventPooling_SIM'
typeComponent='IE'
wlstFileDomain='OSB'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

def makeDeploymentPlanVariable(wlstPlan, adapterAppname, name, value, xpath, origin='planbased'):
	variableAssignment = wlstPlan.createVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	variableAssignment.setXpath(xpath)
	variableAssignment.setOrigin(origin)
	wlstPlan.createVariable(name, value)
 		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ---------------------- """
""" ---------------------- """
def createDatasource():
	global fName
	
	fName = 'createDatasource__'+legacyName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.createJDBCSystemResource(legacyName)

	cd('/JDBCSystemResources/'+legacyName+'/JDBCResource/'+legacyName+'')
	cmo.setName(legacyName)

	cd('/JDBCSystemResources/'+legacyName+'/JDBCResource/'+legacyName+'/JDBCDataSourceParams/'+legacyName)
	set('JNDINames',jarray.array([String('jdbc/'+typeComponent+'/'+legacyName)], String))

	cd('/JDBCSystemResources/'+legacyName+'/JDBCResource/'+legacyName+'/JDBCDriverParams/'+legacyName+'')
	
	cmo.setUrl(UrlDriverCName_nXA+"@"+ListenAddress+":"+ListenPort+":"+SchemaName)
	cmo.setDriverName(DriverName_nXA)
	cmo.setPassword(Pass)	

	cd('/JDBCSystemResources/'+legacyName+'/JDBCResource/'+legacyName+'/JDBCDriverParams/'+legacyName+'/Properties/'+legacyName)
	cmo.createProperty('user')

	cd('/JDBCSystemResources/'+legacyName+'/JDBCResource/'+legacyName+'/JDBCDriverParams/'+legacyName+'/Properties/'+legacyName+'/Properties/user')
	cmo.setValue(User)

	cd('/JDBCSystemResources/'+legacyName+'/JDBCResource/'+legacyName+'/JDBCDataSourceParams/'+legacyName)
	cmo.setGlobalTransactionsProtocol('None')

	cd('/JDBCSystemResources/'+legacyName+'')
	set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ------------------------------ """
""" ------------------------------ """
def createDBAdapterCFactory():
	global fName
	global wlstVersion
	
	fName = 'createDBAdapterCFactory__'+legacyName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	dsName=legacyName
	dsJNDI='jdbc/'+typeComponent+'/'+legacyName
	
	cfName=legacyName
	eisName='eis/DB/'+typeComponent+'/' + cfName

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName, dsJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="DataSourceName"]/value')
	
	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """
	
	# DB Datasources
	createDatasource()

	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	# DBAdapter Connection Factories
	createDBAdapterCFactory()


	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')
