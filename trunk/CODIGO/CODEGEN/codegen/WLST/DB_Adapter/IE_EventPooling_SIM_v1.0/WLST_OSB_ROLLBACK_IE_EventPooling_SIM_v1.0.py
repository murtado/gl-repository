import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='IE_EventPooling_SIM_v1.0'
legacyName='EventPooling_SIM'
wlstFileDomain='OSB'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
wlstFileInstanceDir=wlstFileDir  + wlstVersion + "/"

loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")
loadProperties(wlstFileInstanceDir + wlstVersion + ".properties")

scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')

def deleteDeploymentPlanVariable(wlstPlan, adapterAppname, name):
	wlstPlan.destroyVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	wlstPlan.destroyVariable(name)
		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ------------------ """
""" PER_EBS_ARR_EBS_ """
""" ------------------ """
def rollbackDatasource():
	global beanName
	
	fName = 'rollbackDatasource__'+legacyName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.destroyJDBCSystemResource(getMBean('/JDBCSystemResources/'+legacyName))

	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------- """

""" ------------------------------ """
""" eis/DB/LW/PER-EBS-ARR-EBS-ARR """
""" ------------------------------ """
def rollbackDBAdapterCFactory():
	global fName
	
	fName = 'rollbackDBAdapterCFactory__'+legacyName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + dbAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + dbAdapterPlanName
	
	cfName=legacyName
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConnectionInstance_eis/DB/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, dbAdapterSourceName, 'ConfigProperty_DataSourceName_Value_' + cfName)

	plan.save();
	
	cd('/AppDeployments/'+dbAdapterAppName+'/Targets');
	redeploy(dbAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """



try:

	startEditSession()

	""" @@@ """
	
	""" ********************************************************************************** """
											# APP DEPLOYMENT
	""" ********************************************************************************** """

	# DBAdapter Connection Factories
	rollbackDBAdapterCFactory()
	
	""" ********************************************************************************** """
										# WLS CONFIGURATIONS
	""" ********************************************************************************** """
	
	# DB Datasources
	rollbackDatasource()
	
	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')