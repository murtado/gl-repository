import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_xxxx1'
legacyName='CHL-TEST'


wlstFileDomain='SOA'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion


loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")


scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+SOA_AdminServerListenAddress+":"+SOA_AdminServerListenPort

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')
		

	
def deleteDeploymentPlanVariable(wlstPlan, adapterAppname, name):
	wlstPlan.destroyVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	wlstPlan.destroyVariable(name)
		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """
""" ------------------ """

""" ------------------------------ """
""" rollbackJMSAdapterCFactory__LG """
""" ------------------------------ """
def rollbackJMSAdapterCFactory__LG():
	global fName
	
	fName = 'rollbackJMSAdapterCFactory__LG'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName
	
	cfName=legacyName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'
	
	plan = loadApplication(appPath, planPath)

	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName)
	deleteDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName)
	
	plan.save();
	
	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """
	
""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """


	
	# JMSAdapter Connection Factories
	rollbackJMSAdapterCFactory__LG()
	
	

	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')