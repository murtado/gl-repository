import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_xxxx1'
legacyName='CHL-TEST'


wlstFileDomain='SOA'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion


loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")


scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+SOA_AdminServerListenAddress+":"+SOA_AdminServerListenPort



#Local JMS Artifacts
SOA_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+SOA_ManagedServerListenURLs+';java.naming.security.principal='+SOA_ConUsr+';java.naming.security.credentials='+SOA_ConPsw

#------------------------------------------
#Shared JMS Artifacts (with OSB Domain)
#------------------------------------------
if (isTwoDomains=='true'):
	sharedUsr=OSB_SOA_crossDomainUsr
	sharedPsw=OSB_SOA_crossDomainPsw
else:
	sharedUsr=OSB_ConUsr
	sharedPsw=OSB_ConPsw
	
OSB_JmsAdapterFactoryProperties='java.naming.factory.initial=weblogic.jndi.WLInitialContextFactory;java.naming.provider.url=t3://'+OSB_ManagedServerListenURLs+';java.naming.security.principal='+sharedUsr+';java.naming.security.credentials='+sharedPsw
#------------------------------------------

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')
	
	

def makeDeploymentPlanVariable(wlstPlan, adapterAppname, name, value, xpath, origin='planbased'):
	variableAssignment = wlstPlan.createVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	variableAssignment.setXpath(xpath)
	variableAssignment.setOrigin(origin)
	wlstPlan.createVariable(name, value)
		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """


""" ------------------------------ """
""" createJMSAdapterCFactory__LG """
""" ------------------------------ """
def createJMSAdapterCFactory__LG():
	global fName
	
	fName = 'createJMSAdapterCFactory__LG'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	appPath = soaConnectorsAppDir + '/' + jmsAdapterSourceName
	planPath = soaConnectorsPlanDir + '/' + jmsAdapterPlanName
	
	connFactoryJNDI='/jms/RA/'+legacyName+'/DefaultXAConnFactory'
	
	cfName=legacyName
	eisName='eis/JMS/' + cfName
	
	print '\n ------------------------------------------------------------------------ \n'
	print '\n @@@ Executing	- ' + fName + ' @@@ \n'
	print '\n Plan Path ----> ' + planPath
	print '\n App Path ----> ' + appPath
	print '\n ------------------------------------------------------------------------ \n'

	plan = loadApplication(appPath, planPath)

	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConnectionInstance_eis/JMS/' + cfName + '_JNDIName_' + cfName, eisName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/jndi-name')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_ConnectionFactoryLocation_Value_' + cfName, connFactoryJNDI, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="ConnectionFactoryLocation"]/value')
	makeDeploymentPlanVariable(plan, jmsAdapterSourceName, 'ConfigProperty_FactoryProperties_Value_' + cfName, OSB_JmsAdapterFactoryProperties, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="oracle.tip.adapter.jms.IJmsConnectionFactory"]/connection-instance/[jndi-name="' + eisName + '"]/connection-properties/properties/property/[name="FactoryProperties"]/value')

	plan.save();

	cd('/AppDeployments/'+jmsAdapterAppName+'/Targets');
	redeploy(jmsAdapterAppName, planPath, targets=cmo.getTargets());
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------ """

""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """

	
	# JMSAdapter Connection Factories
	createJMSAdapterCFactory__LG()

	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')