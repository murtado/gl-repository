import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_xxxx1'
legacyName='CHL-TEST'
raName='CHL-TEST-API-Get_RA'


wlstFileDomain='OSB'
wlstFileType='ROLLBACK'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")


scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')
	
	
	
def deleteDeploymentPlanVariable(wlstPlan, adapterAppname, name):
	wlstPlan.destroyVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	wlstPlan.destroyVariable(name)
		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------- """
""" LG_OSB_FileStore """
""" ------------------- """
def rollbackFileStore__LG_OSB_FileStore():
	global fName
	
	fName = 'rollbackFileStore__LG_'+legacyName+'_OSB_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cmo.destroyFileStore(getMBean('/FileStores/LG_'+legacyName+'_OSB_FileStore'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------- """


""" ------------------------ """
""" LG_OSB_JMSServer """
""" ------------------------ """
def rollbackJMSServer__LG_OSB_JMSServer():
	global fName
	
	fName = 'rollbackJMSServer__LG_'+legacyName+'_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)

	cmo.destroyJMSServer(getMBean('/JMSServers/'+legacyName+'_OSB_JMSServer'))

	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """


""" ----------------------- """
""" LG_JMSModule """
""" ----------------------- """
def rollbackJMSModule__LG_JMSModule():

	
	fName = 'rollbackJMSModule__LG_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	cmo.destroyJMSSystemResource(getMBean('/JMSSystemResources/'+legacyName+'_JMSModule'))
	
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """




""" ----------------------- """
""" rollbackJMSModule_UniformDistributedQueues_JMS """
""" ----------------------- """
def rollbackJMSModule_UniformDistributedQueues_JMS():

	
	fName = 'removeee '+raName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	queueMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/')
	
	qInstanceMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_Queue')	
	if qInstanceMBean != None:
		queueMBean.destroyUniformDistributedQueue(qInstanceMBean)
		debugFile.write('\n    JMS Queue Deleted: '+raName+'_REQ_Queue')
	else:
		debugFile.write('\n    No JMS Queue to delete.... Skipping: ' +raName+'_REQ_Queue')

	qInstanceMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_ErrorQueue')
	if qInstanceMBean != None:
		queueMBean.destroyUniformDistributedQueue(qInstanceMBean)
		debugFile.write('\n    JMS Queue Deleted: '+raName+'_REQ_ErrorQueue')
	else:
		debugFile.write('\n    No JMS Queue to delete.... Skipping: ' +raName+'_REQ_ErrorQueue')


	qInstanceMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_Queue')
	if qInstanceMBean != None:
		queueMBean.destroyUniformDistributedQueue(qInstanceMBean)
		debugFile.write('\n    JMS Queue Deleted: '+raName+'_RSP_Queue')
	else:
		debugFile.write('\n    No JMS Queue to delete.... Skipping: ' +raName+'_RSP_Queue')
		
	qInstanceMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_ErrorQueue')
	if qInstanceMBean != None:
		queueMBean.destroyUniformDistributedQueue(qInstanceMBean)
		debugFile.write('\n    JMS Queue Deleted: '+raName+'_RSP_ErrorQueue')
	else:
		debugFile.write('\n    No JMS Queue to delete.... Skipping: ' +raName+'_RSP_ErrorQueue')
	

	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """
def list_JMS():
	fName = 'listJMS_Of_ServerJMS '+legacyName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	global notEmptyJMS
	notEmptyJMS	= False
	
	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues')
	aux=ls(returnMap='true')
	if aux:
		notEmptyJMS=True

	debugFile.write('\nOUTCOME of ' + legacyName + ' : SUCCESS! ')
""" ----------------------- """

""" ----------------------- """
def list_CF():
	fName = 'list_CF_Of_ServerJMS '+legacyName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	global notEmptyCF
	notEmptyCF	= False

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories')
	aux=ls(returnMap='true')
	if aux:
		notEmptyCF=True

	debugFile.write('\nOUTCOME of ' + legacyName + ' : SUCCESS! ')
""" ----------------------- """




""" ----------------------- """
""" rollbackJMSModule_UniformDistributedQueues_JMS """
""" ----------------------- """
def rollbackConnectionFactory():

	
	fName = 'removeee '+raName
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	cfMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/')	
	colasMBean=  getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues')	
	concetfMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories')	
	cfMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/')		
		
	cfInstanceMBean=getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/'+legacyName+'_XA_DCF')
	if cfInstanceMBean != None:
		cfMBean.destroyConnectionFactory(cfInstanceMBean)  
		debugFile.write('\n    ConnectionFactory Queue Deleted: '+legacyName+'_RSP_ErrorQueue')
	else:
		debugFile.write('\n    No ConnectionFactory Queue to delete.... Skipping: ' +legacyName+'_RSP_ErrorQueue')
	

	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ----------------------- """










"""                                                                                  * """
									# WLS CONFIGURATIONS
"""                                                                                  * """
try:

	startEditSession()

	""" @@@ """

	#-------------------------------------------------------------------------------
	# Service Template Resources --->
	#-------------------------------------------------------------------------------
	


	
	rollbackJMSModule_UniformDistributedQueues_JMS()
	list_JMS()

	if notEmptyJMS==False:
		rollbackConnectionFactory()

	list_CF()

	if notEmptyCF==False and notEmptyJMS==False:
		rollbackJMSModule__LG_JMSModule()
		rollbackJMSServer__LG_OSB_JMSServer()
		rollbackFileStore__LG_OSB_FileStore()

	

	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')