import time

wlstFileDir=os.path.abspath(sys.argv[0])+"/WLSTScripts/"

fName=''

wlstVersion='B_xxxx1'
legacyName='CHL-TEST'
raName='CHL-TEST-API-Get_RA'


wlstFileDomain='OSB'
wlstFileType='CREATE'
wlstFileName='WLST_' + wlstFileDomain + '_' + wlstFileType + '_' + wlstVersion
loadProperties(wlstFileDir + wlstFileDomain + "_Domain.properties")


scriptInstance = time.strftime("%d%m%Y%H%M%S")
debugFile = open(wlstFileDir + "_DebugLogs/" + wlstFileName + '_Debug_' + scriptInstance, "a")

conURL="t3://"+OSB_AdminServerListenAddress+":"+OSB_AdminServerListenPort




#------------------------------------------
#Shared JMS Artifacts (with SOA Domain)
#------------------------------------------
if (isTwoDomains=='true'):
	sharedUsr=OSB_SOA_crossDomainUsr
	sharedPsw=OSB_SOA_crossDomainPsw
else:
	sharedUsr=SOA_ConUsr
	sharedPsw=SOA_ConPsw
	
#------------------------------------------

def startEditSession():
	connect(OSB_ConUsr, OSB_ConPsw, conURL)
	edit()
	startEdit(-1, -1, 'false')
	debugFile.write('\n----------------------------------------------------------------------------------')
	debugFile.write('\n--- Starting the Activate Session ---')
	debugFile.write('\n----------------------------------------------------------------------------------')

def endEditSession(outcome):
	if(outcome=='OK'):
		cd('/')
		save()
		activate()
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (SUCCESS) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n All the tasks were completed Sucessfully')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
	else:
		debugFile.write('\n')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n--- Ending the Activate Session (ERROR) ---')
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.write('\n There was an error excecution the following function : ' + fName)
		debugFile.write('\n----------------------------------------------------------------------------------')
		debugFile.close()
		cd('/')
		undo('true','true')
		stopEdit('y')
	

		
def makeDeploymentPlanVariable(wlstPlan, adapterAppname, name, value, xpath, origin='planbased'):
	variableAssignment = wlstPlan.createVariableAssignment(name, adapterAppname, 'META-INF/weblogic-ra.xml')
	variableAssignment.setXpath(xpath)
	variableAssignment.setOrigin(origin)
	wlstPlan.createVariable(name, value)
 		
""" ---------------------------------------------------------------------- """
""" CREATION FUNCTIONS """
""" ---------------------------------------------------------------------- """

""" ----------------- """
""" LG_OSB_FileStore """
""" ----------------- """
def createFileStore__LG_OSB_FileStore():
	global fName
	
	fName = 'createFileStore__LG_'+legacyName+'_OSB_FileStore'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)	
	cd('/')
	
	try: 
		cd('/FileStores/LG_'+legacyName+'_OSB_FileStore')
		debugFile.write('\n     FileStore: LG_'+legacyName+'_OSB_FileStore'+'  already exists')
	except Exception:
		debugFile.write('\n     FileStore: LG_'+legacyName+'_OSB_FileStore'+'  creation')
		cmo.createFileStore('LG_'+legacyName+'_OSB_FileStore')
		cd('/FileStores/LG_'+legacyName+'_OSB_FileStore')
		set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
		debugFile.write('\n     FileStore: LG_'+legacyName+'_OSB_FileStore'+'  creation finish')


	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')

""" ------------------------ """
""" LG_OSB_JMSServer """
""" ------------------------ """
def createJMSServer__LG_OSB_JMSServer():
	global fName

	fName = 'createJMSServer__'+legacyName+'_OSB_JMSServer'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	cd('/')
	
	try: 
		cd('/JMSServers/'+legacyName+'_OSB_JMSServer')
		debugFile.write('\n     JMSServers: LG_'+legacyName+'_OSB_JMSServer'+'  already exists')
	except Exception:
		debugFile.write('\n     JMSServers: LG_'+legacyName+'_OSB_JMSServer'+'  creation')
		cmo.createJMSServer(legacyName+'_OSB_JMSServer')
		cd('/JMSServers/'+legacyName+'_OSB_JMSServer')
		cmo.setPersistentStore(getMBean('/FileStores/LG_'+legacyName+'_OSB_FileStore'))
		set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
		debugFile.write('\n     JMSServers: LG_'+legacyName+'_OSB_JMSServer'+'  creation finish')
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """


""" ----------------------- """
""" Legacy_JMSModule """
""" ----------------------- """
def createJMSModule__LG_JMSModule():
	global fName
	
	fName = 'createJMSModule__'+legacyName+'_JMSModule'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/')
	
	
	try: 
		cd('/JMSSystemResources/'+legacyName+'_JMSModule')
		debugFile.write('\n     JMSModule: LG_'+legacyName+'_JMSModule'+'  already exists')
	except Exception:
		debugFile.write('\n     JMSModule: LG_'+legacyName+'_JMSModule'+'  creation')
		cmo.createJMSSystemResource(legacyName+'_JMSModule')
		cd('/JMSSystemResources/'+legacyName+'_JMSModule')
		set('Targets',jarray.array([ObjectName('com.bea:Name='+OSB_TargetName.strip()+',Type='+OSB_TargetType.strip())], ObjectName))
		cmo.createSubDeployment(legacyName+'_SubDeploy')
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/SubDeployments/'+legacyName+'_SubDeploy')
		set('Targets',jarray.array([ObjectName('com.bea:Name=FRW_OSB_JMSServer,Type=JMSServer')], ObjectName))
		debugFile.write('\n     JMSModule: LG_'+legacyName+'_JMSModule'+'  creation finish')
	
	
	


	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ------------------------ """

""" --------------------------------------------- """
""" createJMSQueue__LG_REQ_Queue """
""" --------------------------------------------- """
def createJMSQueue__LG_REQ_Queue():
	global fName
	
	fName = 'createJMSQueue__'+raName+'_REQ_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	
	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule')
	cmo.createUniformDistributedQueue(raName+'_REQ_Queue')
	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_Queue')
	cmo.setJNDIName('/jms/RA/'+legacyName+'/'+raName+'_REQ_Queue')
	cmo.setDefaultTargetingEnabled(true)
	cmo.setSubDeploymentName(legacyName+'_SubDeploy')
	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule')

	cmo.createUniformDistributedQueue(raName+'_REQ_ErrorQueue')

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_ErrorQueue')
	cmo.setJNDIName('/jms/RA/'+legacyName+'/'+raName+'_REQ_ErrorQueue ')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName(legacyName+'_SubDeploy')

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_Queue/DeliveryFailureParams/'+raName+'_REQ_Queue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_Queue/DeliveryParamsOverrides/'+raName+'_REQ_Queue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_Queue/DeliveryFailureParams/'+raName+'_REQ_Queue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_REQ_ErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" ---------------------- """

""" --------------------------------------------- """
""" createJMSQueue__LG_RSP_Queue """
""" --------------------------------------------- """
def createJMSQueue__LG_RSP_Queue():
	global fName
	
	fName = 'createJMSQueue__LG_RSP_Queue'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)
	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule')

	cmo.createUniformDistributedQueue(raName+'_RSP_Queue')

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_Queue')
	cmo.setJNDIName('/jms/RA/'+legacyName+'/'+raName+'_RSP_Queue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.setSubDeploymentName(legacyName+'_SubDeploy')

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')


	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule')
	cmo.createUniformDistributedQueue(raName+'_RSP_ErrorQueue')

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_ErrorQueue')
	cmo.setJNDIName('/jms/RA/'+legacyName+'/'+raName+'_RSP_ErrorQueue')
	cmo.setDefaultTargetingEnabled(true)

	cmo.unSet('Template')
	cmo.setDefaultTargetingEnabled(false)
	cmo.setForwardDelay(-1)
	cmo.setResetDeliveryCountOnForward(true)
	cmo.setLoadBalancingPolicy('Round-Robin')

	cmo.setSubDeploymentName(legacyName+'_SubDeploy')

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_Queue/DeliveryFailureParams/'+raName+'_RSP_Queue')
	cmo.setExpirationPolicy('Redirect')
	cmo.setRedeliveryLimit(0)

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_Queue/DeliveryParamsOverrides/'+raName+'_RSP_Queue')
	cmo.setRedeliveryDelay(-1)

	cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_Queue/DeliveryFailureParams/'+raName+'_RSP_Queue')
	cmo.setErrorDestination(getMBean('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/UniformDistributedQueues/'+raName+'_RSP_ErrorQueue'))
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')

""" -------------------- """






""" -------------------- """
""" createJMSConnFactory__LG_XA_DCF """
""" -------------------- """
def createJMSConnFactory__LG_XA_DCF():
	global fName
	fName = 'createJMSConnFactory__'+legacyName+'_XA_DCF'
	debugFile.write('\n')
	debugFile.write('\nStarting the Execution of Function : ' + fName)	
		
	try: 
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/'+legacyName+'_XA_DCF')
		debugFile.write('\n     ConnectionFactories:'+legacyName+'  already exists')
	except Exception:
		debugFile.write('\n      ConnectionFactories:'+legacyName+'  creation')
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule')
		cmo.createConnectionFactory(legacyName+'_XA_DCF')
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/'+legacyName+'_XA_DCF')
		cmo.setJNDIName('/jms/RA/'+legacyName+'/DefaultXAConnFactory')
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/'+legacyName+'_XA_DCF/SecurityParams/'+legacyName+'_XA_DCF')
		cmo.setAttachJMSXUserId(false)
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/'+legacyName+'_XA_DCF/ClientParams/'+legacyName+'_XA_DCF')
		cmo.setClientIdPolicy('Restricted')
		cmo.setSubscriptionSharingPolicy('Exclusive')
		cmo.setMessagesMaximum(10)
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/'+legacyName+'_XA_DCF/TransactionParams/'+legacyName+'_XA_DCF')
		cmo.setXAConnectionFactoryEnabled(true)
		cd('/JMSSystemResources/'+legacyName+'_JMSModule/JMSResource/'+legacyName+'_JMSModule/ConnectionFactories/'+legacyName+'_XA_DCF')
		cmo.setDefaultTargetingEnabled(false)
		cmo.setSubDeploymentName(legacyName+'_SubDeploy')
		debugFile.write('\n      ConnectionFactories:'+legacyName+'  creation finish')
	
	
	debugFile.write('\nOUTCOME of ' + fName + ' : SUCCESS! ')
""" -------------------- """


""" ********************************************************************************** """
									# WLS CONFIGURATIONS
""" ********************************************************************************** """
try:

	startEditSession()

	""" @@@ """
	# Persistent Stores
	createFileStore__LG_OSB_FileStore()
	
	# JMS Servers
	createJMSServer__LG_OSB_JMSServer()

	
	# JMS Modules
	createJMSModule__LG_JMSModule()

	
	# JMS Queues
	createJMSQueue__LG_REQ_Queue()
	createJMSQueue__LG_RSP_Queue()
		

	#-------------------------------------------------------------------------------
	
	# JMS Connection Factories
	createJMSConnFactory__LG_XA_DCF()


	""" @@@ """

	validate()
	endEditSession('OK')
	
except:
	dumpStack()
	endEditSession('ERROR')
