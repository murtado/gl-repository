SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK
-----------------------------------------------------------------------------------
-- The following commments can be deleted for each Release
------------------------------------------------------------------------------------
-- V_SYSTEM_CODE @@ Legacy System CODE

-- One per-API --->

	-- V_API_CODE @@ Legacy System API CODE
	
	-- One per-Operation --->
		-- V_API_OP_NAME @@ Legacy System API Operation NAME
		-- V_API_OP_VER @@ Legacy System API Operation VERSION
		-- V_ESB_CONFIG @@ Legacy System API Operation Configuration Name
		-- V_ESB_ENDPOINT_TRANSPORT @@ Legacy System API Operation Transport Name
------------------------------------------------------------------------------------

DECLARE
	ELEMENT_CONT number;

	V_SYSTEM_CODE ESB_SYSTEM.CODE%TYPE;
	V_API_CODE ESB_SYSTEM_API.CODE%TYPE;
	V_API_OP_VER ESB_SYSTEM_API_OPERATION.VERSION%TYPE;
	V_API_OP_NAME ESB_SYSTEM_API_OPERATION.NAME%TYPE;
	V_ESB_ENDPOINT_TRANSPORT ESB_ENDPOINT_TRANSPORT.NAME%TYPE;
	V_ESB_CONFIG ESB_CONFIG.NAME%TYPE;

	PROCEDURE DELETE_OPER_API IS
	BEGIN
		V_ESB_CONFIG := V_API_CODE || '_' || V_API_OP_NAME || '_' || V_ESB_ENDPOINT_TRANSPORT;
	-- ESB_CONFIG_LIST ---------------------------------------------------------------------------------------------------------------------------------------

		DELETE FROM ESB_CONFIG_LIST WHERE CONFIG_ID  = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG);

	-- ESB_ENDPOINT ------------------------------------------------------------------------------------------------------------------------------------------
	
		DELETE FROM  ESB_ENDPOINT WHERE OPERATION_ID = (SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = V_API_OP_NAME AND SYSTEM_API_ID = (SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE))
									AND TRANSPORT_ID = (SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = V_ESB_ENDPOINT_TRANSPORT)
									AND CONFIG_ID    = (SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG);

	-- ESB_CONFIG ---------------------------------------------------------------------------------------------------------------------------------------
			
		DELETE FROM  ESB_CONFIG WHERE NAME     =  V_ESB_CONFIG
								  AND RCD_STATUS  = '1';

	-- ESB_SYSTEM_API_OPERATION ------------------------------------------------------------------------------------------------------------------------------
	
		DELETE FROM  ESB_SYSTEM_API_OPERATION WHERE SYSTEM_API_ID = (SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE)
										  		AND VERSION       = V_API_OP_VER
												AND NAME          = V_API_OP_NAME;
    
	END DELETE_OPER_API;

	PROCEDURE DELETE_SYSTEM_API IS
  BEGIN
    DELETE FROM  ESB_SYSTEM_API WHERE SYSTEM_ID = (SELECT ID FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE) AND CODE = V_API_CODE AND RCD_STATUS = '1';
     EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE(' ESB_SYSTEM_API ' || V_API_CODE || ' no pudo ser borrado.');

  END DELETE_SYSTEM_API;

  PROCEDURE DELETE_SYSTEM IS
  BEGIN
    DELETE FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE AND RCD_STATUS = '1';
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE(' ESB_SYSTEM ' || V_SYSTEM_CODE || ' no pudo ser borrado.');
  END DELETE_SYSTEM;

BEGIN

-- *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
-- ======================================================================================================================================================
-- == INCIO DE ZONA A MODIFICAR =========================================================================================================================
-- ======================================================================================================================================================

	V_SYSTEM_CODE :='SystemCode';

-- APIs  ------------------------------------------------------------------------------------------------------------------------------------------
-- Repetir el bloqueAPI por API del legado a crear.
---------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------   INICIO BLOQUE-API   ---------------------------------------------------------------------------------------------------
	V_API_CODE    :='ApiCode';

-- CAPACIDADES  ------------------------------------------------------------------------------------------------------------------------------------------
-- Repetir el bloque por operacion del legado a borrar.
----------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------   INICIO BLOQUE-CAP    --------------------------------------------------------------------------------------------------------------
		
	V_API_OP_VER  := '1.0';
	V_API_OP_NAME := 'OperationName';
	V_ESB_ENDPOINT_TRANSPORT := 'HTTPSOAP11';	
	DELETE_OPER_API;
--------------------------  INICIO BLOQUE-CAP   -----------------------------------------------------------------------------------------------------------------
--
-- ESB_SYSTEM_API ----------------------------------------------------------------------------------------------------------------------------------------
 
	DELETE_SYSTEM_API;
--------------------------  FIN BLOQUE-API -----------------------------------------------------------------------------------------------------------
--
-- ESB_SYSTEM --------------------------------------------------------------------------------------------------------------------------------------------

	DELETE_SYSTEM;

-- ======================================================================================================================================================
-- == FIN DE ZONA A MODIFICAR ===========================================================================================================================
-- ======================================================================================================================================================
-- *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

END;
/
COMMIT;