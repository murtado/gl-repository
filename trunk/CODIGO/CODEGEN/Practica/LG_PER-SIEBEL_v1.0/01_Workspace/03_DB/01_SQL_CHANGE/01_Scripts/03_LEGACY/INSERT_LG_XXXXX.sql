SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

-----------------------------------------------------------------------------------
-- The following commments can be deleted for each Release
------------------------------------------------------------------------------------
-- V_SYSTEM_CODE @@ Legacy System CODE
-- V_SYSTEM_NAME @@ Legacy System NAME
-- V_SYSTEM_DESC @@ Legacy System DESCRIPTION

-- One per-API --->

	-- V_API_CODE @@ Legacy System API CODE
	-- V_API_NAME @@ Legacy System API NAME
	-- V_API_DESC @@ Legacy System API DESCRIPTION
	
	-- One per-Operation --->
		-- V_API_OP_NAME @@ Legacy System API Operation NAME
		-- V_API_OP_VER @@ Legacy System API Operation VERSION
		-- V_API_OP_DESC @@ Legacy System API Operation DESCRIPTION

		-- V_ESB_CONFIG @@ Legacy System API Operation Configuration Name
		-- V_ESB_ENDPOINT_TRANSPORT @@ Legacy System API Operation Transport Name
		-- V_INSTANCE @ Legacy System API Operation Transport Instance 
		-- V_URL_VALUE @ Legacy System API Operation Transport endpoint
		-- V_soapAction_VALUE @ Legacy System API Operation Transport soapAction
------------------------------------------------------------------------------------
DECLARE
	ELEMENT_CONT number;

	V_SYSTEM_CODE ESB_SYSTEM.CODE%TYPE;
	V_SYSTEM_NAME ESB_SYSTEM.NAME%TYPE;
	V_SYSTEM_DESC ESB_SYSTEM.DESCRIPTION%TYPE;

	V_API_CODE ESB_SYSTEM_API.CODE%TYPE;
	V_API_NAME ESB_SYSTEM_API.NAME%TYPE;
	V_API_DESC ESB_SYSTEM_API.DESCRIPTION%TYPE;

	V_API_OP_VER ESB_SYSTEM_API_OPERATION.VERSION%TYPE;
	V_API_OP_NAME ESB_SYSTEM_API_OPERATION.NAME%TYPE;
	V_API_OP_DESC ESB_SYSTEM_API_OPERATION.DESCRIPTION%TYPE;

	V_ESB_ENDPOINT_TRANSPORT ESB_ENDPOINT_TRANSPORT.NAME%TYPE;
	V_ESB_CONFIG ESB_CONFIG.NAME%TYPE;
	V_INSTANCE ESB_ENDPOINT.INSTANCE%TYPE;
	V_URL_VALUE	ESB_CONFIG_LIST.VALUE%TYPE;
	V_soapAction_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
	

	PROCEDURE CREATE_OPER_API IS
	BEGIN
	    
	-- ESB_SYSTEM_API_OPERATION ------------------------------------------------------------------------------------------------------------------------------
		
		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_SYSTEM_API_OPERATION WHERE SYSTEM_API_ID= ( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE) AND VERSION=V_API_OP_VER AND NAME=V_API_OP_NAME;
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_SYSTEM_API_OPERATION (ID,SYSTEM_API_ID,VERSION,NAME,DESCRIPTION,RCD_STATUS) 
			VALUES (ESB_SYSTEM_API_OPERATION_SEQ.NEXTVAL,( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE),V_API_OP_VER,V_API_OP_NAME,V_API_OP_DESC,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_SYSTEM_API_OPERATION '|| V_API_OP_NAME || ' de API ' ||  V_SYSTEM_CODE ||' ya existe');
		END IF;

	-- ESB_CONFIG -------------------------------------------------------------------------------------------------------------------------------------------
		V_ESB_CONFIG := V_API_CODE || '_' || V_API_OP_NAME || '_' || V_ESB_ENDPOINT_TRANSPORT;
		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG;
		IF ELEMENT_CONT=0 THEN

			INSERT INTO ESB_CONFIG (ID,NAME,RCD_STATUS) VALUES (ESB_CONFIG_SEQ.NEXTVAL,V_ESB_CONFIG,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_CONFIG '|| V_ESB_CONFIG ||' ya existe ');
		END IF;

	-- ESB_ENDPOINT -----------------------------------------------------------------------------------------------------------------------------------------
		

		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_ENDPOINT WHERE  OPERATION_ID = ( SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = V_API_OP_NAME AND SYSTEM_API_ID = ( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE))
															AND    TRANSPORT_ID = ( SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = V_ESB_ENDPOINT_TRANSPORT);

	    IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_ENDPOINT (OPERATION_ID,TRANSPORT_ID,INSTANCE,CONFIG_ID,RCD_STATUS) 
				VALUES (
					( SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = V_API_OP_NAME AND SYSTEM_API_ID = ( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE)),
					( SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = V_ESB_ENDPOINT_TRANSPORT),	
					V_INSTANCE,
					( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),
					'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_ENDPOINT para operation '|| V_API_OP_NAME || ' de API ' || V_API_CODE  ||' ya existe configuracion');
		END IF;	

	-- ESB_CONFIG_LIST ---------------------------------------------------------------------------------------------------------------------------------------

		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
																AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url');
		IF ELEMENT_CONT=0 THEN												
			INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS) 
				VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url'),V_URL_VALUE,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST URL para '|| V_ESB_CONFIG || ' ya existe');
		END IF;	
		

		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
																AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction');	
		IF ( ELEMENT_CONT=0 AND V_soapAction_VALUE IS NOT NULL) THEN
			INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS) 
				VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction'),V_soapAction_VALUE,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST soapAction para '|| V_ESB_CONFIG  ||' ya existe o no es requerido');
		END IF;	

	END CREATE_OPER_API; 

	PROCEDURE CREATE_SYSTEM IS
	BEGIN
	-- ESB_SYSTEM ------------------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_SYSTEM WHERE CODE=V_SYSTEM_CODE;
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_SYSTEM (ID,CODE,NAME,DESCRIPTION,RCD_STATUS) VALUES (ESB_SYSTEM_SEQ.NEXTVAL,V_SYSTEM_CODE,V_SYSTEM_NAME,V_SYSTEM_DESC,'1');
		ELSE
			UPDATE ESB_SYSTEM SET RCD_STATUS = '1'
			WHERE CODE=V_SYSTEM_CODE;
			DBMS_OUTPUT.PUT_LINE('ESB_SYSTEM '||V_SYSTEM_CODE||' ya existe');
		END IF;
	END CREATE_SYSTEM;

	PROCEDURE CREATE_API IS
	BEGIN
	-- ESB_SYSTEM_API ------------------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_SYSTEM_API WHERE CODE=V_API_CODE AND SYSTEM_ID = (SELECT ID FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE);
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_SYSTEM_API (ID,SYSTEM_ID,CODE,NAME,DESCRIPTION,RCD_STATUS) 	VALUES (ESB_SYSTEM_API_SEQ.NEXTVAL,( SELECT ID FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE),V_API_CODE,V_API_NAME,V_API_DESC ,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_SYSTEM_API '||V_SYSTEM_CODE||' ya existe');
		END IF;
	END CREATE_API;


BEGIN

-- *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
-- ======================================================================================================================================================
-- == INCIO DE ZONA A MODIFICAR =========================================================================================================================
-- ======================================================================================================================================================

	V_SYSTEM_CODE := 'SystemCode';
	V_SYSTEM_NAME := 'System Name'; 
	V_SYSTEM_DESC := 'System Description';

	CREATE_SYSTEM;

-- APIs  ------------------------------------------------------------------------------------------------------------------------------------------
-- Repetir el bloqueAPI por API del legado a crear.
---------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------   INICIO BLOQUE-API   ---------------------------------------------------------------------------------------------------
	
	V_API_CODE    := 'ApiCode';
	V_API_NAME    := 'Api Name';
	V_API_DESC    := 'Api of System :: System Name';

	CREATE_API;

------------------ CAPACIDADES  --------------------------------------------------------------------------------------------------------------------
------------------ Repetir el bloqueCAP por operacion del legado a crear.
-----------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------   INICIO BLOQUE-CAP  ---------------------------------------------------------------------------------------
				
			V_API_OP_VER  := '1.0';
			V_API_OP_NAME := 'OperationName';
			V_API_OP_DESC := 'Operation of Api :: Api Name';
			V_ESB_ENDPOINT_TRANSPORT := 'HTTPSOAP11';
			-- Solo completar en caso de usar instacia de transporte  en caso de no requerir asignar con := '';
			V_INSTANCE := '';
			V_URL_VALUE := 'http://DynamicUri';
			-- Solo completar si el protocolo requeriere soapAction en caso de no requerir asignar con := '';
			V_soapAction_VALUE := '"http://soapAction"';

			CREATE_OPER_API;	

		---   Para casos particulares de nececidad de insertar informacion adicional como por ejemplo en la tabla 
		--- ESB_PARAMETER, colocar dichos insert en este espacio reservado.

-----------------------------------------  FIN BLOQUE-CAP --------------------------------------------------------------------------------------------
--
--------------------------  FIN BLOQUE-API -----------------------------------------------------------------------------------------------------------
--
-- ======================================================================================================================================================
-- == FIN DE ZONA A MODIFICAR ===========================================================================================================================
-- ======================================================================================================================================================
-- *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

END;
/
COMMIT;