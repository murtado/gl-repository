SPOOL salida_CREATE_ALL_DB.txt

--Creacion de Types
@.\00_Script\00_Types\CREATE_SYSTEM_QT.sql
@.\00_Script\00_Types\CREATE_FIELD_QT.sql
@.\00_Script\00_Types\CREATE_PAGING_QT.sql

@.\00_Script\01_Package\FRW_SYSTEM_PKG_SPEC.sql
@.\00_Script\01_Package\FRW_SYSTEM_PKG_BODY.sql

SPOOL OFF;

