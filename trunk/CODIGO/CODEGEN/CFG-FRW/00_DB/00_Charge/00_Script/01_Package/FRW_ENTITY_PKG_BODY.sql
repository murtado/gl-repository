CREATE OR REPLACE PACKAGE BODY FRW_ENTITY_PKG AS

    PROCEDURE GET_ENTITIES_BYID(
        P_QUERY IN ENTITY_QT, 
        P_PAGING IN PAGING_T, 
        P_ENTITY OUT SYS_REFCURSOR, 
        P_ERROR_CODE OUT VARCHAR2, 
        P_ERROR_DESC OUT VARCHAR2
    ) AS
    
    BEGIN
        OPEN P_ENTITY FOR
        Select *
        From (
            Select e.id, e.name, e.maj_version, e.description, e.rcd_status, e.creationdate, row_number() OVER (order by e.id)rn
            From ESB_CDM_ENTITY e
            Where e.id = COALESCE(P_QUERY.ID, E.ID)
            and e.name LIKE CONCAT(COALESCE(P_QUERY.NAME, E.NAME), '%')
            and e.maj_version LIKE CONCAT(COALESCE(P_QUERY.MAJ_VERSION, E.MAJ_VERSION), '%')
            order by e.id) data
        Where rn BETWEEN(P_PAGING.PAGE_RESULTS * (P_PAGING.PAGE_NUMBER -1)) +1 
                 and (P_PAGING.PAGE_NUMBER * P_PAGING.PAGE_RESULTS);
        P_ERROR_CODE:='0';
        P_ERROR_DESC:= 'Succesful';
    EXCEPTION
      WHEN OTHERS THEN
        BEGIN 
          P_ERROR_CODE:='99';
          P_ERROR_DESC:='Unexpected Error';
        END;
  END GET_ENTITIES_BYID;


    PROCEDURE GET_FIELDS_FROM_ENTITIES(
        P_QUERY IN ENTITY_QT,
        P_QUERY_FIELD IN FIELD_QT,
        P_PAGING IN PAGING_T,
        P_ENTITY OUT SYS_REFCURSOR,
        P_ERROR_CODE OUT VARCHAR2,
        P_ERROR_DESC OUT VARCHAR2) AS
    BEGIN
      OPEN P_ENTITY FOR
      SELECT *
      FROM (SELECT F.ID, F.NAME, F.RCD_STATUS, F.CREATIONDATE, row_number() OVER (order by F.ID)rn
            FROM ESB_CDM_ENTITY E INNER JOIN ESB_CDM_FIELD F ON (F.ENTITY_ID = E.ID)
            WHERE 
              F.ID = COALESCE(P_QUERY_FIELD.ID, F.ID) AND
              F.NAME LIKE CONCAT(COALESCE(P_QUERY_FIELD.NAME,F.NAME), '%')
              
              AND
              
              E.MAJ_VERSION = P_QUERY.MAJ_VERSION AND E.NAME = P_QUERY.NAME
              
              ORDER BY F.ID) DATA
            
            WHERE rn BETWEEN(P_PAGING.PAGE_RESULTS * (P_PAGING.PAGE_NUMBER - 1)) + 1
            AND (P_PAGING.PAGE_NUMBER * P_PAGING.PAGE_RESULTS);
            P_ERROR_CODE:='0';
            P_ERROR_DESC:='Successful';
            EXCEPTION
              WHEN OTHERS THEN
              BEGIN
                P_ERROR_CODE:= '99';
                P_ERROR_DESC:= 'Unexpected Error';
              END;
    END GET_FIELDS_FROM_ENTITIES;

    PROCEDURE GET_FIELDS_FROM_ENTITIES_NAME(P_QUERY IN ENTITY_QT, 
                                     P_QUERY_FIELD IN FIELD_QT, 
                                     P_PAGING IN PAGING_T, 
                                     P_ENTITY OUT SYS_REFCURSOR,
                                     P_ERROR_CODE OUT VARCHAR2,
                                     P_ERROR_DESC OUT VARCHAR2) AS
    BEGIN
        OPEN P_ENTITY FOR
        Select *
        From(
            Select cf.id, cf.entity_id, cf.name, cf.description, cf.rcd_status, cf.creationdate, row_number() OVER (order by cf.name) rn
            From esb_cdm_entity e INNER JOIN esb_cdm_field cf ON (cf.entity_id = e.id)
            Where cf.id = COALESCE(P_QUERY_FIELD.id, cf.id)
            and cf.name LIKE CONCAT(COALESCE(P_QUERY_FIELD.NAME, cf.name), '%')
            and e.maj_version = P_QUERY.maj_version 
            and e.name = P_QUERY.name
            order by cf.id) DATA
      
        Where rn BETWEEN (P_PAGING.PAGE_RESULTS * (P_PAGING.PAGE_NUMBER - 1)) + 1
                AND (P_PAGING.PAGE_NUMBER * P_PAGING.PAGE_RESULTS);
        P_ERROR_CODE:='0';
        P_ERROR_DESC:='Successful';
        EXCEPTION
            WHEN OTHERS THEN
            BEGIN
                P_ERROR_CODE:= '99';
                P_ERROR_DESC:= 'Unexpected Error';
            END;
    END GET_FIELDS_FROM_ENTITIES_NAME;


END FRW_ENTITY_PKG;