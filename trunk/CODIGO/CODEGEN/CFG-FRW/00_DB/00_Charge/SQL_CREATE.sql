SPOOL salida_CREATE_ALL_DB.txt

--Creacion de Types
@.\00_Script\00_Types\CREATE_ENTITY_QT.sql
@.\00_Script\00_Types\CREATE_FIELD_QT.sql
@.\00_Script\00_Types\CREATE_PAGING_QT.sql

@.\00_Script\01_Package\FRW_ENTITY_PKG_SPEC.sql
@.\00_Script\01_Package\FRW_ENTITY_PKG_BODY.sql

SPOOL OFF;

