create or replace PACKAGE BODY FRW_TRANSACTION_PKG AS

  PROCEDURE GET_TRANSACTION_BYID(P_QUERY IN TRANSACTION_QT, 
                                 P_PAGING IN PAGING_T,
                                 P_TRANSACTION OUT SYS_REFCURSOR, 
                                 P_ERROR_CODE OUT VARCHAR2, 
                                 P_ERROR_DESC OUT VARCHAR2) AS
  BEGIN
      OPEN P_TRANSACTION FOR
      SELECT *
      FROM (SELECT mt.id, mt.proc_id, mt.event_id, mt.status, mt.rcd_status, mt.creationdate, mt.client_req_timestamp, mt.msg_timestamp, mt.sequence, mt.correlation_id,
            row_number() OVER (order by mt.id)rn
            FROM ESB_MESSAGE_TRANSACTION mt
            WHERE mt.id = COALESCE(P_QUERY.ID, MT.ID)
            AND mt.proc_id LIKE CONCAT(COALESCE(P_QUERY.PROC_ID, MT.PROC_ID), '%')
            AND mt.event_id LIKE CONCAT(COALESCE(P_QUERY.EVENT_ID, MT.EVENT_ID), '%')
            ORDER BY mt.id) data
      WHERE rn BETWEEN(P_PAGING.PAGE_RESULTS * (P_PAGING.PAGE_NUMBER -1)) +1
      AND (P_PAGING.PAGE_NUMBER * P_PAGING.PAGE_RESULTS);
        P_ERROR_CODE:='0';
        P_ERROR_DESC:= 'Succesful';
      EXCEPTION
        WHEN OTHERS THEN
          BEGIN 
            P_ERROR_CODE:='99';
            P_ERROR_DESC:='Unexpected Error';
          END;
  END GET_TRANSACTION_BYID;
END FRW_TRANSACTION_PKG;