create or replace PACKAGE FRW_TRANSACTION_PKG AS 

  PROCEDURE GET_TRANSACTION_BYID(P_QUERY IN TRANSACTION_QT, 
                                 P_PAGING IN PAGING_T,
                                 P_TRANSACTION OUT SYS_REFCURSOR, 
                                 P_ERROR_CODE OUT VARCHAR2, 
                                 P_ERROR_DESC OUT VARCHAR2);
END FRW_TRANSACTION_PKG;