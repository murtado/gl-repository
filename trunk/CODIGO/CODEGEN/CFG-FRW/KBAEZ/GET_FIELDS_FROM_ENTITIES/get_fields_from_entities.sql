  PROCEDURE GET_FIELDS_FROM_ENTITIES(P_QUERY IN ENTITY_QT,
                                       P_QUERY_FIELD IN FIELD_QT,
                                       P_PAGING IN PAGING_T,
                                       P_ENTITY OUT SYS_REFCURSOR,
                                       P_ERROR_CODE OUT VARCHAR2,
                                       P_ERROR_DESC OUT VARCHAR2);
									   
 //-----------------------------------------------------------------------------
 
   PROCEDURE GET_FIELDS_FROM_ENTITIES(P_QUERY IN ENTITY_QT,
                                       P_QUERY_FIELD IN FIELD_QT,
                                       P_PAGING IN PAGING_T,
                                       P_ENTITY OUT SYS_REFCURSOR,
                                       P_ERROR_CODE OUT VARCHAR2,
                                       P_ERROR_DESC OUT VARCHAR2) AS
    BEGIN
      OPEN P_ENTITY FOR
      SELECT *
      FROM (SELECT F.ID, F.NAME, F.RCD_STATUS, F.CREATIONDATE, row_number() OVER (order by F.ID)rn
            FROM ESB_CDM_ENTITY E INNER JOIN ESB_CDM_FIELD F ON (F.ENTITY_ID = E.ID)
            WHERE 
              F.ID = COALESCE(P_QUERY_FIELD.ID, F.ID) AND
              F.NAME LIKE CONCAT(COALESCE(P_QUERY_FIELD.NAME,F.NAME), '%')
              
              AND
              
              E.MAJ_VERSION = P_QUERY.MAJ_VERSION AND E.NAME = P_QUERY.NAME
              
              ORDER BY F.ID) DATA
            
            WHERE rn BETWEEN(P_PAGING.PAGE_RESULTS * (P_PAGING.PAGE_NUMBER - 1)) + 1
            AND (P_PAGING.PAGE_NUMBER * P_PAGING.PAGE_RESULTS);
            P_ERROR_CODE:='0';
            P_ERROR_DESC:='Successful';
            EXCEPTION
              WHEN OTHERS THEN
              BEGIN
                P_ERROR_CODE:= '99';
                P_ERROR_DESC:= 'Unexpected Error';
              END;
    END GET_FIELDS_FROM_ENTITIES;