--------------------------------------------------------
--  File created - Monday-October-05-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PARAMETERMANAGERCONFIG
--------------------------------------------------------

  CREATE TABLE "GLOBALLOGIC"."PARAMETERMANAGERCONFIG" 
   (	"NAME" VARCHAR2(250 BYTE), 
	"CONFIG" VARCHAR2(1000 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table PARAMETERMANAGERCONFIG
--------------------------------------------------------

  ALTER TABLE "GLOBALLOGIC"."PARAMETERMANAGERCONFIG" MODIFY ("NAME" NOT NULL ENABLE);
