--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_COUNTRY--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_COUNTRY" 
   (
	"ID" NUMBER NOT NULL, 
	"CODE" NUMBER(3,0) NOT NULL, 
	"NAME" VARCHAR2(3 BYTE) NOT NULL, 
	"DESCRIPTION" VARCHAR2(30 BYTE), 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	CONSTRAINT "ESB_COUNTRY_PK" PRIMARY KEY ("ID")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_COUNTRY"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
