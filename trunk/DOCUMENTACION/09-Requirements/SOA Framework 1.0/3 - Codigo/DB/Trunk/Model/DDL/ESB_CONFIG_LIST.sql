--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_CONFIG_LIST--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_CONFIG_LIST" 
   (
	"CONFIG_NAME" VARCHAR2(20 BYTE) NOT NULL, 
	"PROPERTY_ID" NUMBER NOT NULL, 
	"VALUE" VARCHAR2(50 BYTE) NOT NULL, 
	"RCD" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	 CONSTRAINT "ESB_CONFIG_LIST_PK" PRIMARY KEY ("CONFIG_NAME", "PROPERTY_ID"), 
	 CONSTRAINT "CONFIG_LIST_PROPERTY_FK" FOREIGN KEY ("PROPERTY_ID")
	   REFERENCES "ENTEL01"."ESB_CONFIG_PROPERTY" ("ID"), 
	 CONSTRAINT "CONFIG_LIST_CONFIG_FK" FOREIGN KEY ("CONFIG_NAME")
	   REFERENCES "ENTEL01"."ESB_CONFIG" ("NAME")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_CONFIG_LIST"."RCD" IS '1=ACTIVE, 0=INACTIVE';
