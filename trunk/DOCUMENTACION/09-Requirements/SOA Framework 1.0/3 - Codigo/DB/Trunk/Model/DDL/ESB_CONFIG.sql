--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_CONFIG--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_CONFIG" 
   (
	"ID" NUMBER NOT NULL, 
	"NAME" VARCHAR2(20 BYTE) NOT NULL, 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	 CONSTRAINT "ESB_CONFIG_PK" PRIMARY KEY ("ID"), 
	 CONSTRAINT "ESB_CONFIG_UK1" UNIQUE ("NAME")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_CONFIG"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
