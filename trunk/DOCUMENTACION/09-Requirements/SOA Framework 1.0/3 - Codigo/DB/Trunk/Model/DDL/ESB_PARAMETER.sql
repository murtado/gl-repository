--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_PARAMETER--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_PARAMETER" 
   (
	"KEY" VARCHAR2(20 BYTE) NOT NULL, 
	"VALUE" VARCHAR2(50 BYTE) NOT NULL, 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	CONSTRAINT "TABLE1_PK" PRIMARY KEY ("KEY")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_PARAMETER"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
