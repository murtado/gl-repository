--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_TRANSACTION--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_TRANSACTION" 
   (
	"MESSAGE_ID" NUMBER NOT NULL,
	"CORRELATION_ID" NUMBER,
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	 CONSTRAINT "ESB_TRANSACTION_PK" PRIMARY KEY ("MESSAGE_ID")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_TRANSACTION"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
