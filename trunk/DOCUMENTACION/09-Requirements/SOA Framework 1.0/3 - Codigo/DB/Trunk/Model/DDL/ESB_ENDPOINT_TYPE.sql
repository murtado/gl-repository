--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_CAPABILITY--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_ENDPOINT_TYPE" 
   (	"ID" NUMBER NOT NULL, 
	"NAME" NUMBER NOT NULL, 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	CONSTRAINT "ESB_CAPABILITY_PK" PRIMARY KEY ("ID"),
	CONSTRAINT "ESB_CAPABILITY_CODE_UK" UNIQUE ("NAME")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_ENDPOINT_TYPE"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
