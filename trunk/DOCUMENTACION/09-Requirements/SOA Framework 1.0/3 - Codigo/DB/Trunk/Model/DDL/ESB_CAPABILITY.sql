--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_CAPABILITY--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_CAPABILITY" 
   (	"ID" NUMBER NOT NULL, 
	"SERVICE_ID" NUMBER NOT NULL, 
	"CODE" NUMBER NOT NULL, 
	"NAME" VARCHAR2(20 BYTE) NOT NULL, 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	CONSTRAINT "ESB_CAPABILITY_PK" PRIMARY KEY ("ID"),
	CONSTRAINT "ESB_CAPABILITY_CODE_UK" UNIQUE ("SERVICE_ID", "CODE"),
	CONSTRAINT "ESB_CAPABILITY_NAME_UK" UNIQUE ("SERVICE_ID", "NAME"), 
	CONSTRAINT "ESB_CAPABILITY_SERVICE_FK" FOREIGN KEY ("SERVICE_ID")
	  REFERENCES "ENTEL01"."ESB_SERVICE" ("ID")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_CAPABILITY"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
