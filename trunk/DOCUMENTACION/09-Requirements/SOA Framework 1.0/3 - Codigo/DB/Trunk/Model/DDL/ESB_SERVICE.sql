--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_SERVICE--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_SERVICE" 
   (	"ID" NUMBER NOT NULL, 
	"CODE" NUMBER(3,0) NOT NULL, 
	"NAME" VARCHAR2(20 BYTE) NOT NULL, 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	 CONSTRAINT "ESB_SERVICE_PK" PRIMARY KEY ("ID"), 
	 CONSTRAINT "ESB_SERVICE_CODE_UK" UNIQUE ("CODE"), 
	 CONSTRAINT "ESB_SERVICE_NAME_UK" UNIQUE ("NAME")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_SERVICE"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';