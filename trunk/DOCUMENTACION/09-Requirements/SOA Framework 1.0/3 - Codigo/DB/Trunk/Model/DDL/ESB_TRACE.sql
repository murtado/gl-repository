--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_TRACE--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_TRACE" 
   (
	"ID" NUMBER NOT NULL, 
	"CONV_ID" NUMBER NOT NULL, 
	"SEQUENCE" NUMBER DEFAULT 0, 
	"COMPONENT" VARCHAR2(100 BYTE), 
	"OPERATION" VARCHAR2(100 BYTE), 
	"MSG_TIMESTAMP" TIMESTAMP (6) NOT NULL, 
	"FAMILY" VARCHAR2(100 BYTE), 
	"CLASS" VARCHAR2(100 BYTE), 
	"MEMBER" VARCHAR2(100 BYTE), 
	"MODULE" VARCHAR2(100 BYTE), 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	CONSTRAINT "ESB_TRACE_PK" PRIMARY KEY ("ID"), 
	CONSTRAINT "ESB_TRACE_CONVERSATION_FK" FOREIGN KEY ("CONV_ID")
	  REFERENCES "ENTEL01"."ESB_CONVERSATION" ("ID")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_TRACE"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
