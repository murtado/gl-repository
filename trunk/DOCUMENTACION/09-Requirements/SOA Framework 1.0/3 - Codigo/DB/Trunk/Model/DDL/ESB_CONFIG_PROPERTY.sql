--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_CONFIG_PROPERTY--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_CONFIG_PROPERTY" 
   (
	"ID" NUMBER NOT NULL, 
	"NAME" VARCHAR2(20 BYTE) NOT NULL, 
	"DESCRIPTION" VARCHAR2(50 BYTE), 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	 CONSTRAINT "ESB_CONFIG_PROPERTY_PK" PRIMARY KEY ("ID")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_CONFIG_PROPERTY"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
