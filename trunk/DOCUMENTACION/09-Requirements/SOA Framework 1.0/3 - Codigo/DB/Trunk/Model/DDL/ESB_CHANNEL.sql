--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_CHANNEL--------------------------------------------------------

  CREATE TABLE "ENTEL01"."ESB_CHANNEL" 
   (
	"ID" NUMBER NOT NULL, 
	"NAME" VARCHAR2(20 BYTE), 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	 CONSTRAINT "ESB_CHANNEL_PK" PRIMARY KEY ("ID")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_CHANNEL"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';

