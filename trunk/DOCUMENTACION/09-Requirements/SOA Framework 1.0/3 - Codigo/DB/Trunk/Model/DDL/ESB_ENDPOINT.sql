--------------------------------------------------------
--  File created - Wednesday-October-21-2015   
--------------------------------------------------------
----------------------------------------------------------  DDL for Table ESB_ENDPOINT--------------------------------------------------------
  
  CREATE TABLE "ENTEL01"."ESB_ENDPOINT" 
   (
	"OPERATION" VARCHAR2(20 BYTE) NOT NULL, 
	"LEGACY" VARCHAR2(20 BYTE) NOT NULL, 
	"VERSION" NUMBER NOT NULL, 
	"TYPE_ID" NUMBER NOT NULL,
	"CONFIG_ID" NUMBER NOT NULL, 
	"RCD_STATUS" NUMBER(1,0) DEFAULT 1 NOT NULL, 
	CONSTRAINT "ESB_ENDPOINT_PK" PRIMARY KEY ("OPERATION", "LEGACY", "VERSION"), 
	CONSTRAINT "ESB_ENDPOINT_FK1" FOREIGN KEY ("CONFIG_ID")
	  REFERENCES "ENTEL01"."ESB_CONFIG" ("ID"),
	CONSTRAINT "ESB_ENDPOINT_FK2" FOREIGN KEY ("TYPE_ID")
	  REFERENCES "ENTEL01"."ESB_ENDPOINT_TYPE" ("ID")
   );
   COMMENT ON COLUMN "ENTEL01"."ESB_ENDPOINT"."RCD_STATUS" IS '1=ACTIVE, 0=INACTIVE';
