xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

module namespace hash="http://www.entel.cl/XQuery/Hash/v1";

declare function hash:timeHash-to-hexString() as xs:string {
  let $hex := ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F')
  let $a := year-from-date(fn:current-date())
  let $b := month-from-date(fn:current-date())
  let $c := day-from-date(fn:current-date())
  let $d := hours-from-time(fn:current-time())
  let $e := minutes-from-time(fn:current-time())
  let $f := seconds-from-time(fn:current-time())

  let $g := ((($a mod 1000)+fn:floor($f))*($b+$e)*($c+$d) div 10 + ($f - fn:floor($f)*20))

  return fn:concat($hex[position() eq fn:round((($g div 65536)-fn:floor($g idiv 65536))*16)],
                   $hex[position() eq fn:round((($g div 4096)-fn:floor($g idiv 4096))*16)],
                   $hex[position() eq fn:round((($g div 256)-fn:floor($g idiv 256))*16)],
                   $hex[position() eq fn:round((($g div 16)-fn:floor($g idiv 16))*16)])
};
