xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
(:: pragma  parameter="$inbound" type="anyType" ::)

declare namespace ns0 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)
declare namespace ns1 = "http://www.entel.cl/ESO/Result/v1";
(:: import schema at "../XSD/ESO/Result_v1_ESO.xsd" ::)

declare namespace xf="http://www.entel.cl/XQuery/getResponseHeader_Stamp/";
declare namespace con="http://www.bea.com/wli/sb/context";
declare variable $requestHeader as element() (:: schema-element(ns0:RequestHeader) ::) external;
declare variable $result as element() (:: schema-element(ns1:Result) ::) external;

declare function xf:getResponseHeader_Stamp($requestHeader as element(ns0:RequestHeader),
                                            $result as element() (:: schema-element(ns1:Result) ::))
                                            as element(ns0:ResponseHeader) {
    <ns0:ResponseHeader>
        <ns0:Consumer>{ $requestHeader/ns0:Consumer/@*, $requestHeader/ns0:Consumer/node() }</ns0:Consumer>
        <ns0:Trace reqTimestamp = "{ data($requestHeader/ns0:Trace/@reqTimestamp) }"
                   rspTimestamp = "{ fn:current-dateTime() }"
                   messageID = "{ data($requestHeader/ns0:Trace/@messageID) }"
                   conversationID = "{ data($requestHeader/ns0:Trace/@conversationID) }"
                   correlationID = "{ data($requestHeader/ns0:Trace/@correlationID) }">
          <ns0:Service>{ $requestHeader/ns0:Trace/ns0:Service/@*, $requestHeader/ns0:Trace/ns0:Service/node() }</ns0:Service>
        </ns0:Trace>
        <ns0:Country>{ $requestHeader/ns0:Country/@* , $requestHeader/ns0:Country/node() }</ns0:Country>
        <ns0:Channel>{ $requestHeader/ns0:Channel/@* , $requestHeader/ns0:Channel/node() }</ns0:Channel>
        <ns0:QoS>{ $requestHeader/ns0:QoS/@* , $requestHeader/ns0:QoS/node() }</ns0:QoS>
        <ns1:Result>{ $result/@*, $result/node() }</ns1:Result>
    </ns0:ResponseHeader>
};

xf:getResponseHeader_Stamp($requestHeader, $result)