xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns1="http://www.entel.cl/XQuery/getOperationHandlerRoute";
declare namespace ctx="http://www.bea.com/wli/sb/context";

declare variable $requestheader as element() (:: element(*, ns2:MessageHeader_Type) ::) external;

declare function ns1:getOperationHandler($requestheader as element() (:: element(*, ns2:MessageHeader_Type) ::)) as element() {
    <ctx:route>
      <ctx:pipeline>{ fn:concat("ES_",data($requestheader/*:Trace/*:Service/@name),
                      "/CSL/OperationHandler/",data($requestheader/*:Trace/*:Service/@operation),"/Pipeline/",
                      data($requestheader/*:Trace/*:Service/@operation),"_",
                      data($requestheader/*:Trace/*:Service/@name),"_OH") }</ctx:pipeline>
    </ctx:route>
};

ns1:getOperationHandler($requestheader)
