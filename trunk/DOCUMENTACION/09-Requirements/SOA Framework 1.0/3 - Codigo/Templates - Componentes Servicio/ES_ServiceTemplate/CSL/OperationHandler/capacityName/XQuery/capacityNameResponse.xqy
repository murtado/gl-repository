xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/EBM/ServiceName/capacityName/v1";
(:: import schema at "../../../../ESC/Primary/MessageStructure/Template_V1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns1="http://www.entel.cl/XQuery/capacityNameResponse";

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare variable $responseHeader as element() (:: schema-element(ns2:ResponseHeader) ::) external;
declare variable $request as xs:string external;
declare function ns1:capacityNameResponse($responseHeader as element(ns2:ResponseHeader),
                                          $request as xs:string) as element() (:: schema-element(ns3:capacityName_ServiceName_RSP) ::) {
    <ns3:capacityName_ServiceName_RSP>
        <ns2:ResponseHeader>
            <ns2:Consumer code="{fn:data($responseHeader/ns2:Consumer/@code)}">
                {
                    if ($responseHeader/ns2:Consumer/@name)
                    then attribute name {fn:data($responseHeader/ns2:Consumer/@name)}
                    else ()
                }
                {
                    if ($responseHeader/ns2:Consumer/ns2:Credentials)
                    then 
                        <ns2:Credentials>
                            {
                                if ($responseHeader/ns2:Consumer/ns2:Credentials/@user)
                                then attribute user {fn:data($responseHeader/ns2:Consumer/ns2:Credentials/@user)}
                                else ()
                            }
                            {
                                if ($responseHeader/ns2:Consumer/ns2:Credentials/@password)
                                then attribute password {fn:data($responseHeader/ns2:Consumer/ns2:Credentials/@password)}
                                else ()
                            }
                        </ns2:Credentials>
                    else ()
                }
            </ns2:Consumer>
            <ns2:Trace>
                {
                    if ($responseHeader/ns2:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($responseHeader/ns2:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($responseHeader/ns2:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($responseHeader/ns2:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($responseHeader/ns2:Trace/@messageID)
                    then attribute messageID {fn:data($responseHeader/ns2:Trace/@messageID)}
                    else ()
                }
                {
                    if ($responseHeader/ns2:Trace/@correlationID)
                    then attribute correlationID {fn:data($responseHeader/ns2:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($responseHeader/ns2:Trace/@conversationID)
                    then attribute conversationID {fn:data($responseHeader/ns2:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($responseHeader/ns2:Trace/ns2:Service)
                    then 
                        <ns2:Service>
                            {
                                if ($responseHeader/ns2:Trace/ns2:Service/@code)
                                then attribute code {fn:data($responseHeader/ns2:Trace/ns2:Service/@code)}
                                else ()
                            }
                            {
                                if ($responseHeader/ns2:Trace/ns2:Service/@name)
                                then attribute name {fn:data($responseHeader/ns2:Trace/ns2:Service/@name)}
                                else ()
                            }
                            {
                                if ($responseHeader/ns2:Trace/ns2:Service/@operation)
                                then attribute operation {fn:data($responseHeader/ns2:Trace/ns2:Service/@operation)}
                                else ()
                            }
                        </ns2:Service>
                    else ()
                }
            </ns2:Trace>
            <ns2:Country>{ $responseHeader/ns2:Country/@*, $responseHeader/ns2:Country/node() }</ns2:Country>
            <ns2:Channel code="{fn:data($responseHeader/ns2:Channel/@code)}" mode="{fn:data($responseHeader/ns2:Channel/@mode)}"></ns2:Channel>
            {
                if ($responseHeader/ns2:QoS)
                then 
                    <ns2:QoS>
                        {
                            if ($responseHeader/ns2:QoS/@ttl)
                            then attribute ttl {fn:data($responseHeader/ns2:QoS/@ttl)}
                            else ()
                        }
                        {
                            if ($responseHeader/ns2:QoS/@priority)
                            then attribute priority {fn:data($responseHeader/ns2:QoS/@priority)}
                            else ()
                        }
                        {
                            if ($responseHeader/ns2:QoS/@retryCount)
                            then attribute retryCount {fn:data($responseHeader/ns2:QoS/@retryCount)}
                            else ()
                        }
                        {
                            if ($responseHeader/ns2:QoS/@maxRetryCount)
                            then attribute maxRetryCount {fn:data($responseHeader/ns2:QoS/@maxRetryCount)}
                            else ()
                        }
                    </ns2:QoS>
                else ()
            }
            <ns4:Result>
                <ns4:Status>{fn:data($responseHeader/ns4:Result/ns4:Status)}</ns4:Status>
                {
                    if ($responseHeader/ns4:Result/ns4:Description)
                    then <ns4:Description>{fn:data($responseHeader/ns4:Result/ns4:Description)}</ns4:Description>
                    else ()
                }
                {
                    if ($responseHeader/ns4:Result/ns5:Error)
                    then 
                        <ns5:Error>
                            <ns5:Code>{fn:data($responseHeader/ns4:Result/ns5:Error/ns5:Code)}</ns5:Code>
                            {
                                if ($responseHeader/ns4:Result/ns5:Error/ns5:Description)
                                then <ns5:Description>{fn:data($responseHeader/ns4:Result/ns5:Error/ns5:Description)}</ns5:Description>
                                else ()
                            }
                            <ns5:Type>{fn:data($responseHeader/ns4:Result/ns5:Error/ns5:Type)}</ns5:Type>
                            {
                                if ($responseHeader/ns4:Result/ns5:Error/ns5:Origin)
                                then <ns5:Origin>{fn:data($responseHeader/ns4:Result/ns5:Error/ns5:Origin)}</ns5:Origin>
                                else ()
                            }
                            {
                                if ($responseHeader/ns4:Result/ns5:Error/ns5:Details)
                                then <ns5:Details>{fn:data($responseHeader/ns4:Result/ns5:Error/ns5:Details)}</ns5:Details>
                                else ()
                            }
                        </ns5:Error>
                    else ()
                }
            </ns4:Result>
        </ns2:ResponseHeader>
        <ns3:ResponseBody>
            <ns3:elementName>{fn:data($request)}</ns3:elementName>
        </ns3:ResponseBody>
    </ns3:capacityName_ServiceName_RSP>
};

ns1:capacityNameResponse($responseHeader, $request)