package cl.entel.logger.simplepublisher.message;

public class LogMessageImpl implements IMessage {
    
    String messageId, nivel, componente, operacion, conversationId, correlationId;
    Object xmlDatos;
    String type, description;

    public LogMessageImpl(String messageId, String nivel, String componente, String operacion,
                          String conversationId, String correlationId, Object xmlDatos, String type, String description) {
        this.messageId = messageId;
        this.nivel = nivel;
        this.componente = componente;
        this.type = type;
        this.description = description;
        this.operacion = operacion;
        this.conversationId = conversationId;
        this.correlationId = correlationId;
        this.xmlDatos = xmlDatos;
    }

    @Override
    public String getLogMessage() {
        StringBuffer sb = new StringBuffer();
        sb.append(" Log[").append(nivel).append("] ")
            .append("Message[").append(messageId).append("-").append(conversationId).append((correlationId != "" ? "-" : "")).append(correlationId).append("] ")
            .append("Service[").append(componente).append("-").append(operacion).append("] ")
            .append("Type[").append(type).append("] ")
            .append("Description[").append(description).append("] ")
            .append("Payload[").append(this.xmlDatos.toString().trim()).append("]");
        return sb.toString();
    }
}
