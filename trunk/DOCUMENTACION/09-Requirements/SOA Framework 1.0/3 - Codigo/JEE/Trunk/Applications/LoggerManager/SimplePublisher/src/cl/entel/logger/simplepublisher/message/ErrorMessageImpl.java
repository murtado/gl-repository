package cl.entel.logger.simplepublisher.message;

public class ErrorMessageImpl implements IMessage {
    
    String statusResult, severityError;
    String messageId, componente, operacion, conversationId, correlationId;
    Object xmlDatos;
    
    public ErrorMessageImpl(String messageId, String componente, String operacion,
                          String conversationId, String correlationId, String statusResult, String severityError, Object xmlDatos) {
        this.messageId = messageId;
        this.componente = componente;
        this.operacion = operacion;
        this.conversationId = conversationId;
        this.correlationId = correlationId;
        this.xmlDatos = xmlDatos;
        this.severityError = severityError;
        this.statusResult = statusResult;
    }

    @Override
    public String getLogMessage() {
        StringBuffer sb = new StringBuffer();
        sb.append(" Message[").append(messageId).append("-").append(conversationId).append((correlationId != "" ? "-" : "")).append(correlationId).append("] ")
            .append("Service[").append(componente).append("-").append(operacion).append("] ")
            .append("Error [").append(severityError).append(", ").append(statusResult).append("] ")
            .append("Fault[").append(this.xmlDatos.toString().trim()).append("]");
        return sb.toString();
    }
}
