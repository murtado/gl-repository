package cl.entel.logger.simplepublisher.message;

import java.util.Date;

public class AuditMessageImpl implements IMessage {
    
    String messageId, description, componente, type, operacion, conversationId, correlationId;
    String codeConsumer, nameConsumer;
    Object xmlDatos;
    Date requestTime;
    
    public AuditMessageImpl(String messageId, String componente, String operacion,
                                 String conversationId, String correlationId, String type, Object xmlDatos, String description, String codeConsumer,
                            String nameConsumer, Date requestTime) {
        this.messageId = messageId;
        this.description = description;
        this.componente = componente;
        this.type = type;
        this.operacion = operacion;
        this.conversationId = conversationId;
        this.correlationId = correlationId;
        this.codeConsumer = codeConsumer;
        this.nameConsumer = nameConsumer;
        this.requestTime = requestTime;
        this.xmlDatos = xmlDatos;
    }

    @Override
    public String getLogMessage() {
        StringBuffer sb = new StringBuffer();
        sb.append(" Audit[").append(type).append("]");
        if(requestTime != null)
            sb.append(" Time[").append(requestTime.toString()).append("] ");

        sb.append(" Message[").append(messageId).append("-").append(conversationId).append((correlationId != "" ? "-" : "")).append(correlationId).append("] ")
            .append(" Consumer[").append(codeConsumer).append("-").append(nameConsumer).append("] ")
            .append(" Service[").append(componente).append("-").append(operacion).append("] ")
            .append(" Description[").append(description).append("]")
            .append(" Payload[").append(this.xmlDatos.toString().trim()).append("]");
        return sb.toString();
    }
}
