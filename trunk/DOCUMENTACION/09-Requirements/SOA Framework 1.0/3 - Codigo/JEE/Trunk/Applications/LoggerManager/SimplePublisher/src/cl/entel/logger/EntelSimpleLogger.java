package cl.entel.logger;

import cl.entel.logger.simplepublisher.message.AuditMessageImpl;
import cl.entel.logger.simplepublisher.message.ErrorMessageImpl;
import cl.entel.logger.simplepublisher.message.IMessage;

import cl.entel.logger.simplepublisher.message.LogMessageImpl;

import java.io.IOException;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import org.w3c.dom.Node;

public class EntelSimpleLogger implements Serializable{

    @SuppressWarnings("compatibility:-1965423272180788171")
    private static final long serialVersionUID = 3293156707759047640L;

    private static final Map<String, Level> LEVELS = new HashMap<String, Level>();
    private String name;

    public EntelSimpleLogger(String name) {
        this.name = name;
    }

    public EntelSimpleLogger(Class clazz) {
        this(clazz.getName());
    }

    public static EntelSimpleLogger getLogger(String name) {
        return new EntelSimpleLogger(name);
    }

    public void audit(String messageId, String componente, String operacion,
                                 String conversationId, String correlationId, String type, Object xmlDatos, String description,
                                 String codeConsumer, String nameConsumer, Date requestTime) {
        IMessage mensaje =
            new AuditMessageImpl(messageId, componente, operacion, conversationId, correlationId, type, xmlDatos, description, codeConsumer,
                            nameConsumer, requestTime);

        log(Level.INFO, mensaje);
    }

    public void log(String messageId, String nivel, String componente, String operacion,
                          String conversationId, String correlationId, Object xmlDatos, String type, String description) throws IOException {
        IMessage mensaje =
            new LogMessageImpl(messageId, nivel, componente, operacion, conversationId, correlationId, xmlDatos, type, description);

        Level level = (Level) LEVELS.get(nivel.toUpperCase());

        if (level != null) {
            log(level, mensaje);
            return;
        }

        log(Level.INFO, mensaje);
    }

    public void log(String request) throws Exception {
        log(Level.ERROR, request);
    }
    public void audit(String request) throws Exception {
        log(Level.ERROR, request);
    }
    public void error(String request) throws Exception {
        log(Level.ERROR, request);
    }

    public void error(String messageId, String componente, String operacion,
                          String conversationId, String correlationId, String statusResult, String severityError, Object xmlDatos) {
        IMessage mensaje = new ErrorMessageImpl(messageId, componente, operacion, conversationId, correlationId, statusResult, severityError, xmlDatos);

        Logger.getLogger(this.name).log(Level.ERROR, mensaje.getLogMessage());
    }


    private void log(Level level, IMessage mensaje) {
        Logger.getLogger(this.name).log(level, mensaje.getLogMessage());
    }

    private void log(Level level, String mensaje) {
        Logger.getLogger(this.name).log(level, mensaje);
    }

    static {
        LEVELS.put("INFO", Level.INFO);
        LEVELS.put("DEBUG", Level.DEBUG);
        LEVELS.put("WARN", Level.WARN);
        LEVELS.put("ERROR", Level.ERROR);
        LEVELS.put("FATAL", Level.FATAL);
        try {
            DOMConfigurator.configureAndWatch(System.getProperty("Logger.Config"));
        } catch (Exception e) {
            System.err.println("Error al obtener el archivo de configuracion del logger");
        }
    }
}
