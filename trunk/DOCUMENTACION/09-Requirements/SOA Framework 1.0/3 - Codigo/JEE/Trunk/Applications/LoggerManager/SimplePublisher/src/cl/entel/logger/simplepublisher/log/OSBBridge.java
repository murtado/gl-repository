package cl.entel.logger.simplepublisher.log;

import cl.entel.logger.EntelSimpleLogger;

import java.util.Date;

import org.w3c.dom.Node;

public class OSBBridge {

    public OSBBridge() {
        super();
    }

    public static void callLog(String messageId, String nivel, String componente, String operacion,
                               String conversationId, String correlationId, String xmlDatos,
                               String type, String description) throws Exception {
        EntelSimpleLogger logger = new EntelSimpleLogger("EntelSimpleLogger.log");
        logger.log(messageId, nivel, componente, operacion, conversationId, correlationId, xmlDatos, type, description);
    }

    public static void callError(String messageId, String componente, String operacion,
                                 String conversationId, String correlationId, String statusResult, String severityError,
                                 String xmlDatos) {
        EntelSimpleLogger logger = new EntelSimpleLogger("EntelSimpleLogger.error");
        logger.error(messageId, componente, operacion, conversationId, correlationId, statusResult,
                     severityError, xmlDatos);
    }

    public static void callAudit(String messageId, String componente, String operacion,
                                 String conversationId, String correlationId, String type, String xmlDatos, String description,
                                 String codeConsumer, String nameConsumer, Date requestTime) {
        EntelSimpleLogger logger = new EntelSimpleLogger("EntelSimpleLogger.audit");
        logger.audit(messageId, componente, operacion, conversationId, correlationId, type,
                     xmlDatos, description, codeConsumer,
                            nameConsumer, requestTime);
    }
    
    public static void callLogDirect(String request) throws Exception {
        EntelSimpleLogger logger = new EntelSimpleLogger("EntelSimpleLogger.Exception.log");
        logger.log(request);
    }
    public static void callAuditDirect(String request) throws Exception {
        EntelSimpleLogger logger = new EntelSimpleLogger("EntelSimpleLogger.Exception.audit");
        logger.audit(request);
    }
    public static void callErrorDirect(String request) throws Exception {
        EntelSimpleLogger logger = new EntelSimpleLogger("EntelSimpleLogger.Exception.error");
        logger.error(request);
    }
}
