package cl.entel.logger.simplepublisher;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import java.io.StringReader;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;

import org.w3c.dom.ls.LSSerializer;

import org.xml.sax.InputSource;

@MessageDriven(activationConfig = {
               @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
               @ActivationConfigProperty(propertyName = "connectionFactoryJndiName",
                                         propertyValue = "entel.jms.LoggerManager.XA.CF"),
               @ActivationConfigProperty(propertyName = "destinationJndiName", propertyValue = "LoggerErrorQueue")
    }, mappedName = "ENTELErrorLoggerMDB")
public class ErrorPublisherMDBBean extends PublisherBaseBean implements MessageListener {
    /**
     * Message to log file.
     * @param message
     */
    public void onMessage(Message message) {
        try {
            Document xml = getDocument(((TextMessage) message).getText());
            if (xml.getFirstChild().getNodeName().contains("LogREQ"))
                callLogDirect(xml);
            else if (xml.getFirstChild().getNodeName().contains("AuditREQ"))
                callAuditDirect(xml);
            else
                callErrorDirect(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static Document getDocument(String xmlSample) throws Exception {
      DocumentBuilderFactory docBuilderFactory=DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder=null;
      docBuilder=docBuilderFactory.newDocumentBuilder();
      StringReader ir=new StringReader(xmlSample);
      InputSource is=new InputSource(ir);
      Document doc=docBuilder.parse(is);
      doc.normalize();
      return doc;
    }
}
