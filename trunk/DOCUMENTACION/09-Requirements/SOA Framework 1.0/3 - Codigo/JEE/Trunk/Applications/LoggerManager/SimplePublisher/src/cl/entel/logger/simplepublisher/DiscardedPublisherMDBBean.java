package cl.entel.logger.simplepublisher;

import cl.entel.logger.simplepublisher.log.OSBBridge;

import java.io.StringReader;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;

import javax.jms.Message;
import javax.jms.MessageListener;

import javax.jms.TextMessage;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

@MessageDriven(activationConfig = {
               @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
               @ActivationConfigProperty(propertyName = "connectionFactoryJndiName",
                                         propertyValue = "entel.jms.LoggerManager.XA.CF"),
               @ActivationConfigProperty(propertyName = "destinationJndiName", propertyValue = "LoggerDiscardedQueue")
    }, mappedName = "ENTELDiscardedLoggerMDB")
public class DiscardedPublisherMDBBean extends PublisherBaseBean implements MessageListener {

    /**
     * Message to log file
     * @param message
     */
    public void onMessage(Message message) {
        try {
            DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dBF.newDocumentBuilder();
            Document body = builder.parse(new InputSource(new StringReader(((TextMessage) message).getText())));

            Node nodeREQ = body.getChildNodes().item(0);
            if (nodeREQ.getNodeName().contains("LogREQ"))
                callLog(body);
            else if (nodeREQ.getNodeName().contains("AuditREQ"))
                callAudit(body);
            else
                callError(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
