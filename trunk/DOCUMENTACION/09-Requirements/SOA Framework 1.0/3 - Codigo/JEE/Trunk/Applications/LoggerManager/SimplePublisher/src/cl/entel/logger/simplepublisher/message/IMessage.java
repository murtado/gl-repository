package cl.entel.logger.simplepublisher.message;

public interface IMessage {
    
    public abstract String getLogMessage();
}
