package cl.entel.logger.simplepublisher;

import cl.entel.logger.simplepublisher.log.OSBBridge;

import java.io.StringWriter;

import java.sql.Timestamp;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

public class PublisherBaseBean {
    protected void callAudit(Document body) throws Exception {
        String messageId = "", componente = "", operacion = "", conversationId =
            "", correlationId = "";
        String consumerName = "", consumerCode = "";
        String msg = "", type = "", description = "";
        Timestamp reqTime = null;
        
        NodeList nodeList = body.getChildNodes().item(0).getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            if(item.getNodeName().contains("Trace")){
                for(int k = 0; k < item.getChildNodes().getLength(); k++){
                    Node node =  item.getChildNodes().item(k);
                    if(node.getNodeName().contains("Header")){
                        for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                            Node child =  node.getChildNodes().item(j);
                            if(child.getNodeName().contains("Trace")){
                                messageId = child.getAttributes().getNamedItem("messageID").getTextContent();
                                conversationId = child.getAttributes().getNamedItem("conversationID").getTextContent();
                                correlationId = child.getAttributes().getNamedItem("correlationID").getTextContent();
                                if(child.getAttributes().getNamedItem("reqTimestamp").getTextContent() != null)
                                    reqTime = Timestamp.valueOf(child.getAttributes().getNamedItem("reqTimestamp").getTextContent());
                                break;
                            }
                            if(child.getNodeName().contains("Consumer")){
                                consumerCode = child.getAttributes().getNamedItem("code").getTextContent();
                                consumerName = child.getAttributes().getNamedItem("name").getTextContent();
                            }
                        }
                    }
                    if(node.getNodeName().contains("Logger")){
                        componente = node.getAttributes().getNamedItem("Component").getTextContent();
                        operacion = node.getAttributes().getNamedItem("Operation").getTextContent();
                        break;
                    }
                }

            }
            if(item.getNodeName().contains("Message")){
                msg = innerXml(item);
            }
            if(item.getNodeName().contains("Type")){
                type = item.getTextContent();
            }
            if(item.getNodeName().contains("Description")){
                description = item.getTextContent();
            }
        }

        OSBBridge.callAudit(messageId, componente, operacion, conversationId, correlationId,
                            type, msg, description, consumerCode, consumerName, reqTime);
    }

    protected void callError(Document body) throws Exception {
        String messageId = "", componente = "", operacion = "", conversationId =
            "", correlationId = "";
        String msg = "";
        String statusResult = "", severityError = "";

        NodeList nodeList = body.getChildNodes().item(0).getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            if(item.getNodeName().contains("Trace")){
                for(int k = 0; k < item.getChildNodes().getLength(); k++){
                    Node node =  item.getChildNodes().item(k);
                    if(node.getNodeName().contains("Header")){
                        for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                            Node child =  node.getChildNodes().item(j);
                            if(child.getNodeName().contains("Trace")){
                                messageId = child.getAttributes().getNamedItem("messageID").getTextContent();
                                conversationId = child.getAttributes().getNamedItem("conversationID").getTextContent();
                                correlationId = child.getAttributes().getNamedItem("correlationID").getTextContent();
                                break;
                             }
                        }
                    }
                    if(node.getNodeName().contains("Logger")){
                        componente = node.getAttributes().getNamedItem("Component").getTextContent();
                        operacion = node.getAttributes().getNamedItem("Operation").getTextContent();
                        break;
                    }
                }
                continue;
            }
            if(item.getNodeName().contains("FaultDetails")){
                msg = innerXml(item);
                continue;
            }
            if(item.getNodeName().contains("ErrorResult")){
                for(int k = 0; k < item.getChildNodes().getLength(); k++){
                    Node node = item.getChildNodes().item(k);
                    if(node.getNodeName().contains("Status")){
                        statusResult = node.getTextContent();
                        continue;
                    }
                    if(node.getNodeName().contains("Error")){
                        for(int j = 0; j < node.getChildNodes().getLength(); j++){
                            Node c = node.getChildNodes().item(j);
                            if(c.getNodeName().contains("Type"))
                                severityError.concat(c.getTextContent());
                            else if(c.getNodeName().contains("Code"))
                                severityError.concat(c.getTextContent());
                        }
                    }
                }
            }
        }

        OSBBridge.callError(messageId, componente, operacion, conversationId, correlationId,
                            statusResult, severityError, msg.replaceAll("&lt;", "<"));
    }

    protected void callLog(Document body) throws Exception {
        String messageId = "", nivel = "", componente = "", operacion = "", conversationId =
            "", correlationId = "";
        String msg = "", type = "", description = "";
        String consumerName = "", consumerCode = "";
        Timestamp reqTime = null;

        NodeList nodeList = body.getChildNodes().item(0).getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            if(item.getNodeName().contains("Trace")){
                for(int k = 0; k < item.getChildNodes().getLength(); k++){
                    Node node =  item.getChildNodes().item(k);
                    if(node.getNodeName().contains("Header")){
                        for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                            Node child =  node.getChildNodes().item(j);
                            if(child.getNodeName().contains("Trace")){
                                messageId = child.getAttributes().getNamedItem("messageID").getTextContent();
                                conversationId = child.getAttributes().getNamedItem("conversationID").getTextContent();
                                correlationId = child.getAttributes().getNamedItem("correlationID").getTextContent();
                                if(child.getAttributes().getNamedItem("reqTimestamp").getTextContent() != null)
                                    reqTime = Timestamp.valueOf(child.getAttributes().getNamedItem("reqTimestamp").getTextContent());
                                break;
                            }
                            if(child.getNodeName().contains("Consumer")){
                                consumerCode = child.getAttributes().getNamedItem("code").getTextContent();
                                consumerName = child.getAttributes().getNamedItem("name").getTextContent();
                            }
                        }
                    }
                    if(node.getNodeName().contains("Logger")){
                        componente = node.getAttributes().getNamedItem("Component").getTextContent();
                        operacion = node.getAttributes().getNamedItem("Operation").getTextContent();
                        break;
                    }
                }
                continue;
            }
            if(item.getNodeName().contains("Message")){
                msg = innerXml(item);
                continue;
            }
            if(item.getNodeName().contains("LogType")){
                type = item.getTextContent();
                continue;
            }
            if(item.getNodeName().contains("Severity")){
                nivel = item.getTextContent();
                continue;
            }
            if(item.getNodeName().contains("Description")){
                description = item.getTextContent();
                continue;
            }
        }

        OSBBridge.callLog(messageId, nivel, componente, operacion, conversationId, correlationId,
                          msg.replaceAll("&lt;", "<"), type, description);
    }
    
    public void callLogDirect(Document request) throws Exception {
       OSBBridge.callLogDirect(convertDocumentToString(request));
    }
    public void callAuditDirect(Document request) throws Exception {
       OSBBridge.callAuditDirect(convertDocumentToString(request));
    }
    public void callErrorDirect(Document request) throws Exception {
       OSBBridge.callErrorDirect(convertDocumentToString(request));
    }
    
    private String innerXml(Node node) {
        DOMImplementationLS lsImpl = (DOMImplementationLS)node.getOwnerDocument().getImplementation().getFeature("LS", "3.0");
        LSSerializer lsSerializer = lsImpl.createLSSerializer();
        NodeList childNodes = node.getChildNodes();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < childNodes.getLength(); i++) {
           sb.append(lsSerializer.writeToString(childNodes.item(i)));
        }
        return sb.toString(); 
    }
    
    private static String convertDocumentToString(Document doc) {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer;
            try {
                transformer = tf.newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                StringWriter writer = new StringWriter();
                transformer.transform(new DOMSource(doc), new StreamResult(writer));
                String output = writer.getBuffer().toString();
                return output;
            } catch (TransformerException e) {
                e.printStackTrace();
            }
    
            return null;
    }    
}
