xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/EDD/Dictionary/v1";
(:: import schema at "../XSD/EDD/Dictionary_v1_EDD.xsd" ::)
declare namespace ns2="http://www.entel.cl/SC/ParameterManager/get/v1";
(:: import schema at "../../DC_SC_ParameterManager_Refactor/SupportAPI/XSD/CSM/get_ParameterManager_v1_CSM.xsd" ::)

declare namespace ns3="http://www.entel.cl/XQuery/getParameterManager_GetReq";

declare variable $Keys as xs:string+ (:: ns1:genericStringValue_SType+ ::)  external;

declare function ns3:func($Keys as xs:string+ (:: ns1:genericStringValue_SType+ ::) ) as element() (:: schema-element(ns2:GetREQ) ::) {
    <ns2:GetREQ>
        <ns2:Keys>
            "{for $Key in ($Keys) return
                <ns2:Key>{fn:data($Key)}</ns2:Key>}"
        </ns2:Keys>
    </ns2:GetREQ>
};

ns3:func($Keys)
