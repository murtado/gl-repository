xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/LoggerManager/log/v1";
(:: import schema at "../../../../../DC_SC_LoggerManager/SupportAPI/XSD/ESM/log_LoggerManager_v1_ESM.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns0="http://www.entel.cl/XQuery/ServiceTemplate_LoggerManagerRequest";

declare namespace ns4 = "http://www.entel.cl/ESO/TraceProfile/v1";


declare variable $request as element() external;

declare function ns0:ServiceTemplate_LoggerManagerRequest($request as element()) as element() (:: schema-element(ns2:LogREQ) ::) {
    <ns2:LogREQ>
      <ns2:Trace>
          <ns4:Header>
              <ns3:Consumer code="{fn:data($request/ns3:RequestHeader/ns3:Consumer/@code)}">
                  {
                      if ($request/ns3:RequestHeader/ns3:Consumer/@name)
                      then attribute name {fn:data($request/ns3:RequestHeader/ns3:Consumer/@name)}
                      else ()
                  }
                  {
                      if ($request/ns3:RequestHeader/ns3:Consumer/ns3:Credentials)
                      then 
                          <ns3:Credentials>
                              {
                                  if ($request/ns3:RequestHeader/ns3:Consumer/ns3:Credentials/@user)
                                  then attribute user {fn:data($request/ns3:RequestHeader/ns3:Consumer/ns3:Credentials/@user)}
                                  else ()
                              }
                              {
                                  if ($request/ns3:RequestHeader/ns3:Consumer/ns3:Credentials/@password)
                                  then attribute password {fn:data($request/ns3:RequestHeader/ns3:Consumer/ns3:Credentials/@password)}
                                  else ()
                              }
                          </ns3:Credentials>
                      else ()
                  }
              </ns3:Consumer>
              <ns3:Trace>
                  {
                      if ($request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)
                      then attribute reqTimestamp {fn:data($request/ns3:RequestHeader/ns3:Trace/@reqTimestamp)}
                      else ()
                  }
                  {
                      if ($request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)
                      then attribute rspTimestamp {fn:data($request/ns3:RequestHeader/ns3:Trace/@rspTimestamp)}
                      else ()
                  }
                  {
                      if ($request/ns3:RequestHeader/ns3:Trace/@messageID)
                      then attribute messageID {fn:data($request/ns3:RequestHeader/ns3:Trace/@messageID)}
                      else ()
                  }
                  {
                      if ($request/ns3:RequestHeader/ns3:Trace/@correlationID)
                      then attribute correlationID {fn:data($request/ns3:RequestHeader/ns3:Trace/@correlationID)}
                      else ()
                  }
                  {
                      if ($request/ns3:RequestHeader/ns3:Trace/@conversationID)
                      then attribute conversationID {fn:data($request/ns3:RequestHeader/ns3:Trace/@conversationID)}
                      else ()
                  }
                  {
                      if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service)
                      then 
                          <ns3:Service>
                              {
                                  if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)
                                  then attribute code {fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@code)}
                                  else ()
                              }
                              {
                                  if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)
                                  then attribute name {fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@name)}
                                  else ()
                              }
                              {
                                  if ($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)
                                  then attribute operation {fn:data($request/ns3:RequestHeader/ns3:Trace/ns3:Service/@operation)}
                                  else ()
                              }
                          </ns3:Service>
                      else ()
                  }
              </ns3:Trace>
              <ns3:Country code="{fn:data($request/ns3:RequestHeader/ns3:Country/@code)}">
                  {
                      if ($request/ns3:RequestHeader/ns3:Country/@name)
                      then attribute name {fn:data($request/ns3:RequestHeader/ns3:Country/@name)}
                      else ()
                  }
              </ns3:Country>
              <ns3:Channel name="{fn:data($request/ns3:RequestHeader/ns3:Channel/@name)}" mode="{fn:data($request/ns3:RequestHeader/ns3:Channel/@mode)}"> code="{fn:data($request/ns3:RequestHeader/ns3:Channel/@code)}" mode="{fn:data($request/ns3:RequestHeader/ns3:Channel/@mode)}"</ns3:Channel>
              {
                  if ($request/ns3:RequestHeader/ns3:QoS)
                  then 
                      <ns3:QoS>
                          {
                              if ($request/ns3:RequestHeader/ns3:QoS/@ttl)
                              then attribute ttl {fn:data($request/ns3:RequestHeader/ns3:QoS/@ttl)}
                              else ()
                          }
                          {
                              if ($request/ns3:RequestHeader/ns3:QoS/@priority)
                              then attribute priority {fn:data($request/ns3:RequestHeader/ns3:QoS/@priority)}
                              else ()
                          }
                          {
                              if ($request/ns3:RequestHeader/ns3:QoS/@retryCount)
                              then attribute retryCount {fn:data($request/ns3:RequestHeader/ns3:QoS/@retryCount)}
                              else ()
                          }
                          {
                              if ($request/ns3:RequestHeader/ns3:QoS/@maxRetryCount)
                              then attribute maxRetryCount {fn:data($request/ns3:RequestHeader/ns3:QoS/@maxRetryCount)}
                              else ()
                          }
                      </ns3:QoS>
                  else ()
              }
          </ns4:Header>
          <ns4:Logger></ns4:Logger>
      </ns2:Trace>
        <ns2:Message>{fn:data($request/*[1])}</ns2:Message>
        <ns2:MessageType>LOG</ns2:MessageType>
        <ns2:Severity>INFO</ns2:Severity>
    </ns2:LogREQ>
};

ns0:ServiceTemplate_LoggerManagerRequest($request)
