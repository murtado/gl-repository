xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.anyuri.com";
(:: import schema at "../XSD/Legacy/_SchemaName_.xsd" ::)
declare namespace ns1="http://www.entel.cl/RA/_InstanceName_/v1";
(:: import schema at "../XSD/CSM/_InstanceName_v1_CSM.xsd" ::)

declare variable $AdapterREQ as element() (:: schema-element(ns1:_InstanceName_REQ) ::) external;

declare function local:func($AdapterREQ as element() (:: schema-element(ns1:_InstanceName_REQ) ::)) as element() (:: schema-element(ns2:REQ) ::) {
    <ns2:REQ>
        <ns2:REQ>{fn:data($AdapterREQ/ns1:RequestBody/ns1:dummyREQ)}</ns2:REQ>
        <ns2:REQ_Aux>Aux Value</ns2:REQ_Aux>
    </ns2:REQ>
};

local:func($AdapterREQ)