xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.entel.cl/EBM/ServiceName/_InstanceName_/v1";
(:: import schema at "../../../../../ESC/Primary/MessageStructure/Template_V1_EBM.xsd" ::)
declare namespace ns2="http://www.entel.cl/RA/_InstanceName_/v1";
(:: import schema at "../../../../../../DC_RA_LegacyName/ResourceAdapters/_InstanceName_/XSD/CSM/_InstanceName_v1_CSM.xsd" ::)

declare namespace ns1="http://www.entel.cl/XQuery/getRequest_InstanceName_RA";

declare namespace ns4 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $Request as element() (:: schema-element(ns3:_InstanceName__ServiceName_REQ) ::) external;

declare function ns1:getRequest_InstanceName_RA($Request as element() (:: schema-element(ns3:_InstanceName__ServiceName_REQ) ::)) as element() (:: schema-element(ns2:_InstanceName_REQ) ::) {
    <ns2:_InstanceName_REQ>
        <ns4:RequestHeader>
            <ns4:Consumer code="{fn:data($Request/ns4:RequestHeader/ns4:Consumer/@code)}">
                {
                    if ($Request/ns4:RequestHeader/ns4:Consumer/@name)
                    then attribute name {fn:data($Request/ns4:RequestHeader/ns4:Consumer/@name)}
                    else ()
                }
                {
                    if ($Request/ns4:RequestHeader/ns4:Consumer/ns4:Credentials)
                    then 
                        <ns4:Credentials>
                            {
                                if ($Request/ns4:RequestHeader/ns4:Consumer/ns4:Credentials/@user)
                                then attribute user {fn:data($Request/ns4:RequestHeader/ns4:Consumer/ns4:Credentials/@user)}
                                else ()
                            }
                            {
                                if ($Request/ns4:RequestHeader/ns4:Consumer/ns4:Credentials/@password)
                                then attribute password {fn:data($Request/ns4:RequestHeader/ns4:Consumer/ns4:Credentials/@password)}
                                else ()
                            }
                        </ns4:Credentials>
                    else ()
                }
            </ns4:Consumer>
            <ns4:Trace>
                {
                    if ($Request/ns4:RequestHeader/ns4:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($Request/ns4:RequestHeader/ns4:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns4:RequestHeader/ns4:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($Request/ns4:RequestHeader/ns4:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($Request/ns4:RequestHeader/ns4:Trace/@messageID)
                    then attribute messageID {fn:data($Request/ns4:RequestHeader/ns4:Trace/@messageID)}
                    else ()
                }
                {
                    if ($Request/ns4:RequestHeader/ns4:Trace/@correlationID)
                    then attribute correlationID {fn:data($Request/ns4:RequestHeader/ns4:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($Request/ns4:RequestHeader/ns4:Trace/@conversationID)
                    then attribute conversationID {fn:data($Request/ns4:RequestHeader/ns4:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($Request/ns4:RequestHeader/ns4:Trace/ns4:Service)
                    then 
                        <ns4:Service>
                            {
                                if ($Request/ns4:RequestHeader/ns4:Trace/ns4:Service/@code)
                                then attribute code {fn:data($Request/ns4:RequestHeader/ns4:Trace/ns4:Service/@code)}
                                else ()
                            }
                            {
                                if ($Request/ns4:RequestHeader/ns4:Trace/ns4:Service/@name)
                                then attribute name {fn:data($Request/ns4:RequestHeader/ns4:Trace/ns4:Service/@name)}
                                else ()
                            }
                            {
                                if ($Request/ns4:RequestHeader/ns4:Trace/ns4:Service/@operation)
                                then attribute operation {fn:data($Request/ns4:RequestHeader/ns4:Trace/ns4:Service/@operation)}
                                else ()
                            }
                        </ns4:Service>
                    else ()
                }
            </ns4:Trace>
            <ns4:Country code="{fn:data($Request/ns4:RequestHeader/ns4:Country/@code)}">
                {
                    if ($Request/ns4:RequestHeader/ns4:Country/@name)
                    then attribute name {fn:data($Request/ns4:RequestHeader/ns4:Country/@name)}
                    else ()
                }
            </ns4:Country>
            <ns4:Channel name="{fn:data($Request/ns4:RequestHeader/ns4:Channel/@name)}" mode="{fn:data($Request/ns4:RequestHeader/ns4:Channel/@mode)}"></ns4:Channel>
            {
                if ($Request/ns4:RequestHeader/ns4:QoS)
                then 
                    <ns4:QoS>
                        {
                            if ($Request/ns4:RequestHeader/ns4:QoS/@ttl)
                            then attribute ttl {fn:data($Request/ns4:RequestHeader/ns4:QoS/@ttl)}
                            else ()
                        }
                        {
                            if ($Request/ns4:RequestHeader/ns4:QoS/@priority)
                            then attribute priority {fn:data($Request/ns4:RequestHeader/ns4:QoS/@priority)}
                            else ()
                        }
                        {
                            if ($Request/ns4:RequestHeader/ns4:QoS/@retryCount)
                            then attribute retryCount {fn:data($Request/ns4:RequestHeader/ns4:QoS/@retryCount)}
                            else ()
                        }
                        {
                            if ($Request/ns4:RequestHeader/ns4:QoS/@maxRetryCount)
                            then attribute maxRetryCount {fn:data($Request/ns4:RequestHeader/ns4:QoS/@maxRetryCount)}
                            else ()
                        }
                    </ns4:QoS>
                else ()
            }
        </ns4:RequestHeader>
        <ns2:Body>
            <ns2:dummyREQ>{fn:data($Request/ns3:Body/ns3:elementName[1])}</ns2:dummyREQ></ns2:Body>
    </ns2:_InstanceName_REQ>
};

ns1:getRequest_InstanceName_RA($Request)
