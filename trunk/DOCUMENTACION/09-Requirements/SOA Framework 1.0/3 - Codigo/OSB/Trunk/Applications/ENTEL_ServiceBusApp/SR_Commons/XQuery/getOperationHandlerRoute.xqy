xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns1="http://www.entel.cl/XQuery/getOperationHandlerRoute";
declare namespace ctx="http://www.bea.com/wli/sb/context";

declare variable $requestheader as element() (:: element(*, ns2:MessageHeader_Type) ::) external;

(:~
 : This function creates the routing XML tags required to dynamically route a message from
 : the PIF to the desired Operation Handler. It uses the indormation in the Request Header
 : as a parameter to do so.
 :)

declare function ns1:getOperationHandler($requestheader as element() (:: element(*, ns2:MessageHeader_Type) ::)) as element() {
    <ctx:route>
      <ctx:pipeline>{ fn:concat("ES_",data($requestheader/*:Trace/*:Service/@name),
                      "/CSL/",data($requestheader/*:Consumer/@countryCode),
                      "/OperationHandler/",data($requestheader/*:Trace/*:Service/@operation),"/",
                      data($requestheader/*:Trace/*:Service/@operation),"_",
                      data($requestheader/*:Trace/*:Service/@name),"_OH") }</ctx:pipeline>
    </ctx:route>
};

ns1:getOperationHandler($requestheader)
