xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns12="http://www.entel.cl/SC/ErrorManager/handle/v1";
(:: import schema at "../../DC_SC_ErrorManager/SupportAPI/XSD/CSM/handle_ErrorManager_v1_CSM.xsd" ::)

declare namespace ns10="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns1="http://www.entel.cl/EDD/Dictionary/v1";
(:: import schema at "../XSD/EDD/Dictionary_v1_EDD.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/Tracer/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";
(:: import schema at "../XSD/ESO/Error_v1_ESO.xsd" ::)

declare variable $componentName as xs:string external;
declare variable $componentOperation as xs:string? external;
declare variable $errorSourceID as xs:int (:: ns1:sysCode_SType ::)  external;
declare variable $errorSourceDetails as xs:string? external;
declare variable $sourceFault as element()? external;
declare variable $messageHeader as element() (:: element(*, ns10:MessageHeader_Type) ::) external;
declare variable $sourceErrorCode as xs:string? external;
declare variable $sourceErrorDesc as xs:string? external;
declare variable $canonicalError as element()? (:: schema-element(ns3:CanonicalError) ::) external;
declare variable $errorIndexModule as xs:string? external;
declare variable $errorIndexSubModule as xs:string? external;
declare variable $errorPlaceHolder as xs:string? external;

declare function local:func($componentName as xs:string, 
                            $componentOperation as xs:string?, 
                            $errorSourceID as xs:int (:: ns1:sysCode_SType ::) , 
                            $errorSourceDetails as xs:string?, 
                            $sourceFault as element()?,
                            $messageHeader as element() (:: element(*, ns10:MessageHeader_Type) ::), 
                            $sourceErrorCode as xs:string?, 
                            $sourceErrorDesc as xs:string?,
                            $canonicalError as element()? (:: schema-element(ns3:CanonicalError) ::),
                            $errorIndexModule as xs:string?,
                            $errorIndexSubModule as xs:string?,
                            $errorPlaceHolder as xs:string?) as element() (:: schema-element(ns12:handleREQ) ::)  {
    <ns12:handleREQ>
        <ns2:ErrorTracer component="{fn:data($componentName)}" operation="{fn:data($componentOperation)}">
            <ns2:Header>
                <ns10:Consumer sysCode="{fn:data($messageHeader/ns10:Consumer/@sysCode)}" enterpriseCode="{fn:data($messageHeader/ns10:Consumer/@enterpriseCode)}" countryCode="{fn:data($messageHeader/ns10:Consumer/@countryCode)}"></ns10:Consumer>
                <ns10:Trace>
                    {
                        if ($messageHeader/ns10:Trace/@clientReqTimestamp)
                        then attribute clientReqTimestamp {fn:data($messageHeader/ns10:Trace/@clientReqTimestamp)}
                        else ()
                    }
                    {
                        if ($messageHeader/ns10:Trace/@reqTimestamp)
                        then attribute reqTimestamp {fn:data($messageHeader/ns10:Trace/@reqTimestamp)}
                        else ()
                    }
                    {
                        if ($messageHeader/ns10:Trace/@rspTimestamp)
                        then attribute rspTimestamp {fn:data($messageHeader/ns10:Trace/@rspTimestamp)}
                        else ()
                    }
                    {
                        if ($messageHeader/ns10:Trace/@processID)
                        then attribute processID {fn:data($messageHeader/ns10:Trace/@processID)}
                        else ()
                    }
                    {
                        if ($messageHeader/ns10:Trace/@eventID)
                        then attribute eventID {fn:data($messageHeader/ns10:Trace/@eventID)}
                        else ()
                    }
                    {
                        if ($messageHeader/ns10:Trace/@conversationID)
                        then attribute conversationID {fn:data($messageHeader/ns10:Trace/@conversationID)}
                        else ()
                    }
                    {
                        if ($messageHeader/ns10:Trace/@correlationID)
                        then attribute correlationID {fn:data($messageHeader/ns10:Trace/@correlationID)}
                        else ()
                    }
                    {
                        if ($messageHeader/ns10:Trace/ns10:Service)
                        then 
                            <ns10:Service>
                                {
                                    if ($messageHeader/ns10:Trace/ns10:Service/@code)
                                    then attribute code {fn:data($messageHeader/ns10:Trace/ns10:Service/@code)}
                                    else ()
                                }
                                {
                                    if ($messageHeader/ns10:Trace/ns10:Service/@name)
                                    then attribute name {fn:data($messageHeader/ns10:Trace/ns10:Service/@name)}
                                    else ()
                                }
                                {
                                    if ($messageHeader/ns10:Trace/ns10:Service/@operation)
                                    then attribute operation {fn:data($messageHeader/ns10:Trace/ns10:Service/@operation)}
                                    else ()
                                }
                            </ns10:Service>
                        else ()
                    }
                </ns10:Trace>
                {
                    if ($messageHeader/ns10:Channel)
                    then 
                        <ns10:Channel>
                            {
                                if ($messageHeader/ns10:Channel/@name)
                                then attribute name {fn:data($messageHeader/ns10:Channel/@name)}
                                else ()
                            }
                            {
                                if ($messageHeader/ns10:Channel/@mode)
                                then attribute mode {fn:data($messageHeader/ns10:Channel/@mode)}
                                else ()
                            }
                        </ns10:Channel>
                    else ()
                }
            </ns2:Header>
            {
                if ($sourceErrorCode) then 
                    <ns3:SourceError code="{fn:data($sourceErrorCode)}" description="{fn:data($sourceErrorDesc)}">
                        <ns3:ErrorSourceDetails source="{fn:data($errorSourceID)}" details="{fn:data($errorSourceDetails)}">
                        </ns3:ErrorSourceDetails>
                        {
                            if ($sourceFault)
                            then <ns3:SourceFault>{fn:data($sourceFault)}</ns3:SourceFault>
                            else ()
                        }
                    </ns3:SourceError>
                else
                    
                    <ns3:CanonicalError>
                        {
                            if ($canonicalError/@code)
                            then attribute code {fn:data($canonicalError/@code)}
                            else ()
                        }
                        {
                            if ($canonicalError/@description)
                            then attribute description {fn:data($canonicalError/@description)}
                            else ()
                        }
                        {
                            if ($canonicalError/@type)
                            then attribute type {fn:data($canonicalError/@type)}
                            else ()
                        }
                    </ns3:CanonicalError>
            }
        </ns2:ErrorTracer>
        <ns12:ErrorIndex module="{fn:data($errorIndexModule)}" subModule="{fn:data($errorIndexSubModule)}">
        </ns12:ErrorIndex>
        <ns12:ErrorPlaceholder>{fn:data($errorPlaceHolder)}</ns12:ErrorPlaceholder>
    </ns12:handleREQ>
};

local:func( $componentName,
            $componentOperation,
            $errorSourceID,
            $errorSourceDetails,
            $sourceFault,
            $messageHeader,
            $sourceErrorCode,
            $sourceErrorDesc,
            $canonicalError,
            $errorIndexModule,
            $errorIndexSubModule,
            $errorPlaceHolder)