xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace ns1="http://www.entel.cl/SC/MessageManager/CheckExecutorHelper/v1";
(:: import schema at "../XSD/ESO/CheckExecutorHelper_ESO.xsd" ::)

declare variable $name as xs:string external;
declare variable $type as xs:int external;
declare variable $grade as xs:int external;
declare variable $elapsedTime as xs:string external;
declare variable $message as element() external;

declare function local:checkExecutorHelperREQ($name as xs:string, 
                                              $type as xs:int, 
                                              $grade as xs:int, 
                                              $elapsedTime as xs:string, 
                                              $message as element()) as element() (:: schema-element(ns1:checkExecutorHelperREQ) ::) {
    <ns1:checkExecutorHelperREQ name="{fn:data($name)}" type="{fn:data($type)}" grade="{fn:data($grade)}" elapsedTime="{fn:data($elapsedTime)}">
        <ns1:message>{$message/*[1]}</ns1:message>
    </ns1:checkExecutorHelperREQ>
};

local:checkExecutorHelperREQ($name, $type, $grade, $elapsedTime, $message)
