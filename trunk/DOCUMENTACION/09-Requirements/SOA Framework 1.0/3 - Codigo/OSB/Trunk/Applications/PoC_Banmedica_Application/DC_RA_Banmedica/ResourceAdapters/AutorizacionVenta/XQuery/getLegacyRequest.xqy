xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://www.banmedica.cl/";
(:: import schema at "../../../../DC_LW_Banmedica/LegacyAPI/WSDL/BanmedicaFCV.wsdl" ::)
declare namespace ns2="http://www.entel.cl/RA/AutorizacionVenta/v1";
(:: import schema at "../XSD/CSM/AutorizacionVenta_v1_CSM.xsd" ::)
declare namespace ns1="http://www.entel.cl/SC/ParameterManager/getMapping/v1";
(:: import schema at "../../../../DC_SC_ParameterManager_Refactor/SupportAPI/XSD/CSM/getMapping_ParameterManager_v1_CSM.xsd" ::)

declare variable $getMappingRSP as element() (:: schema-element(ns1:GetMappingsRSP) ::) external;
declare variable $request as element() (:: schema-element(ns2:AutorizacionVentaREQ) ::) external;

declare function local:autorizacionVentaREQ_Banmedica($getMappingRSP as element(ns1:GetMappingsRSP), 
                                                      $request as element() (:: schema-element(ns2:AutorizacionVentaREQ) ::)) 
                                                      as element() (:: schema-element(ns3:AutorizacionVenta) ::) {
    <ns3:AutorizacionVenta>
        <ns3:Empresa>{ if(fn:data($getMappingRSP/ns1:Maps/ns1:Value[@field="compania.codigo"]))
                       then fn:data($getMappingRSP/ns1:Maps/ns1:Value[@field="compania.codigo"]/@dCode)
                       else (data($request/ns2:Body/ns2:Compania))
        }</ns3:Empresa>
        <ns3:Cadena>{fn:data($request/ns2:Body/ns2:Cadena)}</ns3:Cadena>
        <ns3:Local>{ if (fn:data($getMappingRSP/ns1:Maps/ns1:Value[@field="farmacia.codigo"]/@dCode))
                     then fn:data($getMappingRSP/ns1:Maps/ns1:Value[@field="farmacia.codigo"]/@dCode)
                     else ($request/ns2:Body/ns2:Farmacia)
        }</ns3:Local>
        <ns3:NumeroPos>{fn:data($request/ns2:Body/ns2:NumeroPos)}</ns3:NumeroPos>
        {
            if ($request/ns2:Body/ns2:CodigoTx)
            then <ns3:CodigoTx>{fn:data($request/ns2:Body/ns2:CodigoTx)}</ns3:CodigoTx>
            else ()
        }
        {
            if ($request/ns2:Body/ns2:FechaHora)
            then <ns3:FechaHora>{fn:data($request/ns2:Body/ns2:FechaHora)}</ns3:FechaHora>
            else ()
        }
        <ns3:TTL>{fn:data($request/ns2:Body/ns2:TTL)}</ns3:TTL>
        {
            if ($request/ns2:Body/ns2:VersionPos)
            then <ns3:VersionPos>{fn:data($request/ns2:Body/ns2:VersionPos)}</ns3:VersionPos>
            else ()
        }
        {
            if ($request/ns2:Body/ns2:Requerimiento)
            then <ns3:Requerimiento>{fn:data($request/ns2:Body/ns2:Requerimiento)}</ns3:Requerimiento>
            else ()
        }
    </ns3:AutorizacionVenta>
};

local:autorizacionVentaREQ_Banmedica($getMappingRSP, $request)