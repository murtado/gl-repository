xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/ParameterManager/getMapping/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getMapping_ParameterManager_v1_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getMappingParameterManagerAdapter";
(:: import schema at "../JCA/getMappingParameterManagerAdapter/getMappingParameterManagerAdapter_sp.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/Result/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;
declare variable $Result as element()? (:: schema-element(ns3:Result) ::) external;

declare function local:getMappingRSP_Adapter($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::),
                                             $Result as element()? (:: schema-element(ns3:Result) ::)) as element() (:: schema-element(ns2:GetMappingsRSP) ::) {
    <ns2:GetMappingsRSP>
        <ns2:Maps>
            {
                for $P_MAPOUTPUT_Row in $OutputParameters/ns1:P_MAPOUTPUT/ns1:P_MAPOUTPUT_Row
                return 
                <ns2:Value 
                    dCode="{fn:data($P_MAPOUTPUT_Row/ns1:DCODE)}" 
                    sCode="{fn:data($P_MAPOUTPUT_Row/ns1:SCODE)}">
                {
                    if($P_MAPOUTPUT_Row/ns1:FIELD)
                    then attribute field {fn:data($P_MAPOUTPUT_Row/ns1:FIELD)}
                    else ()
                }
                {
                    if($P_MAPOUTPUT_Row/ns1:ENTITY)
                    then attribute entity {fn:data($P_MAPOUTPUT_Row/ns1:ENTITY)}
                    else ()
                }
                </ns2:Value>
            }
        </ns2:Maps>
        {
            if ($Result)
            then 
                <ns3:Result>
                    {
                        if ($Result/@status)
                        then attribute status {fn:data($Result/@status)}
                        else ()
                    }
                    {
                        if ($Result/@description)
                        then attribute description {fn:data($Result/@description)}
                        else ()
                    }
                    {
                        if ($Result/ns4:CanonicalError)
                        then 
                            <ns4:CanonicalError>
                                {
                                    if ($Result/ns4:CanonicalError/@code)
                                    then attribute code {fn:data($Result/ns4:CanonicalError/@code)}
                                    else ()
                                }
                                {
                                    if ($Result/ns4:CanonicalError/@description)
                                    then attribute description {fn:data($Result/ns4:CanonicalError/@description)}
                                    else ()
                                }
                                {
                                    if ($Result/ns4:CanonicalError/@type)
                                    then attribute type {fn:data($Result/ns4:CanonicalError/@type)}
                                    else ()
                                }
                            </ns4:CanonicalError>
                        else ()
                    }
                    {
                        if ($Result/ns3:SourceErrors)
                        then 
                            <ns3:SourceErrors>
                                {
                                    for $SourceError in $Result/ns3:SourceErrors/ns4:SourceError
                                    return 
                                    <ns4:SourceError>
                                        {
                                            if ($SourceError/@code)
                                            then attribute code {fn:data($SourceError/@code)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/@description)
                                            then attribute description {fn:data($SourceError/@description)}
                                            else ()
                                        }
                                        <ns4:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns4:ErrorSourceDetails/@source)
                                                then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/ns4:ErrorSourceDetails/@details)
                                                then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                                else ()
                                            }
                                        </ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:SourceFault)
                                            then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                            else ()
                                        }
                                    </ns4:SourceError>
                                }
                            </ns3:SourceErrors>
                        else ()
                    }
                </ns3:Result>
            else ()
        }
    </ns2:GetMappingsRSP>
};

local:getMappingRSP_Adapter($OutputParameters, $Result)