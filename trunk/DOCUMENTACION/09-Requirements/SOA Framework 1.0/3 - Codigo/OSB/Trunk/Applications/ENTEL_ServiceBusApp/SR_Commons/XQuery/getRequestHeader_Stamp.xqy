xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
(:: pragma  parameter="$inbound" type="anyType" ::)

declare namespace ns0 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace xf = "http://www.entel.cl/XQuery/getRequestHeader_Stamp/";
declare namespace con="http://www.bea.com/wli/sb/context";
import module namespace hash="http://www.entel.cl/XQuery/Hash/v1" at "Modules/Hash.xqy";

declare variable $requestHeader as element() (:: schema-element(ns0:RequestHeader) ::) external;
declare variable $inbound as element() external;

(:~
 : This function creates the Request Header Stamp, which contains all the traceability and
 : transactional information, as well as the client's information. It uses the header
 : provided by the client as an input, and also uses information about the context of the
 : execution.
 :)

declare function xf:getRequestHeader_Stamp($requestHeader as element(ns0:RequestHeader),
                                           $inbound as element(*))
                                           as element(ns0:RequestHeader) {
        <ns0:RequestHeader>
            <ns0:Consumer>{ $requestHeader/ns0:Consumer/@* , $requestHeader/ns0:Consumer/node() }</ns0:Consumer>
            <ns0:Trace reqTimestamp = "{ if (data($requestHeader/ns0:Trace/@reqTimestamp) != '') then
                                             (data($requestHeader/ns0:Trace/@reqTimestamp))
                                         else 
                                             fn:current-dateTime() }"
                       messageID = "{ fn:data($requestHeader/ns0:Trace/@messageID) }"
                       correlationID = "{ if (fn:data($requestHeader/ns0:Trace/@correlationID)) then
                                            fn:data($requestHeader/ns0:Trace/@correlationID)
                                          else "" }"
                       conversationID = "{ fn:data($requestHeader/ns0:Trace/@conversationID ) }">
                  <ns0:Service code = "{ '' }"
                                 name = "{ fn:replace(fn:tokenize($inbound/@name,'\$')[2],"ES_","") }"
                                 operation = "{ $inbound/con:service/con:operation }"/>
                </ns0:Trace>
            <ns0:Country>{ $requestHeader/ns0:Country/@* , $requestHeader/ns0:Country/node() }</ns0:Country>
            <ns0:Channel>{ $requestHeader/ns0:Channel/@* , $requestHeader/ns0:Channel/node() }</ns0:Channel>
            <ns0:QoS>{ $requestHeader/ns0:QoS/@* , $requestHeader/ns0:QoS/node() }</ns0:QoS>
            </ns0:RequestHeader>
};

xf:getRequestHeader_Stamp($requestHeader, $inbound)