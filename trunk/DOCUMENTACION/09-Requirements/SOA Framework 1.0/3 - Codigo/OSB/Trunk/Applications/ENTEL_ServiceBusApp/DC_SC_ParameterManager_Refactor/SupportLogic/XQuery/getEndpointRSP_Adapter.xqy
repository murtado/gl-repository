xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/ParameterManager/getEndpoint/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getEndpoint_ParameterManager_v1_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getEndpointParameterManagerAdapter";
(:: import schema at "../JCA/getEndpointParameterManagerAdapter/getEndpointParameterManagerAdapter_sp.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/Result/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;
declare variable $Result as element()? (:: schema-element(ns3:Result) ::) external;

declare function local:getEndpointResp_Adapter($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::),
                                               $Result as element()? (:: schema-element(ns3:Result) ::)) as element() (:: schema-element(ns2:GetEndpointRSP) ::) {
    <ns2:GetEndpointRSP>
      { if(fn:data($OutputParameters/ns1:P_ENDPTYPE)) then
        <ns2:Endpoint>
            {
            let $strOption := fn:data($OutputParameters/ns1:P_ENDPTYPE)
            let $option:= element {$strOption} {}
            return typeswitch($option)
                 case element(OSB) return
                    <ns2:OSB 
                        uri="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='uri']/ns1:VALUE)}"
                        isProxy="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='isProxy']/ns1:VALUE)}" 
                        isPipeline="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='isPipeline']/ns1:VALUE)}">
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']))
                            then attribute timeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']))
                            then attribute readtimeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='operation']))
                            then attribute operation
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='operation']/ns1:VALUE)}
                            else ()
                          }
                            <ns2:Credentials>
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']))
                              then attribute user
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']/ns1:VALUE)}
                              else ()
                            }
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']))
                              then attribute password
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']/ns1:VALUE)}
                              else ()
                            }
                            </ns2:Credentials>
                    </ns2:OSB>
                 case element(JMS) return
                    <ns2:JMS 
                        queueJNDI="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='queueJNDI']/ns1:VALUE)}" 
                        connFactoryJNDI="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='connFactoryJNDI']/ns1:VALUE)}" 
                        host="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='host']/ns1:VALUE)}" 
                        port="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='port']/ns1:VALUE)}">
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']))
                            then attribute timeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']))
                            then attribute readtimeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='protocol']))
                            then attribute protocol
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='protocol']/ns1:VALUE)}
                            else ()
                          }
                            <ns2:Credentials>
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']))
                              then attribute user
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']/ns1:VALUE)}
                              else ()
                            }
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']))
                              then attribute password
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']/ns1:VALUE)}
                              else ()
                            }
                            </ns2:Credentials>
                    </ns2:JMS>
                 case element(HTTP) return
                    <ns2:HTTP 
                        URI="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='URI']/ns1:VALUE)}" 
                        host="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='host']/ns1:VALUE)}" 
                        port="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='port']/ns1:VALUE)}">
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']))
                            then attribute timeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']))
                            then attribute readtimeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='SSL']))
                            then attribute SSL
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='SSL']/ns1:VALUE)}
                            else ()
                          }
                            <ns2:Credentials>
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']))
                              then attribute user
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']/ns1:VALUE)}
                              else ()
                            }
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']))
                              then attribute password
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']/ns1:VALUE)}
                              else ()
                            }
                            </ns2:Credentials>
                    </ns2:HTTP>
                 case element(JNDI) return
                    <ns2:JNDI>
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']))
                            then attribute timeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']))
                            then attribute readtimeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='initialContextFactory']))
                            then attribute initialContextFactory
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='initialContextFactory']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='jndiLookup']))
                            then attribute jndiLookup
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='jndiLookup']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='providerURL']))
                            then attribute providerURL
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='providerURL']/ns1:VALUE)}
                            else ()
                          }
                            <ns2:Credentials>
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']))
                              then attribute user
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']/ns1:VALUE)}
                              else ()
                            }
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']))
                              then attribute password
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']/ns1:VALUE)}
                              else ()
                            }
                            </ns2:Credentials>
                    </ns2:JNDI>
                 case element(MQ) return
                    <ns2:MQ 
                        requestURI="{fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='requestURI']/ns1:VALUE)}">
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']))
                            then attribute timeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='timeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']))
                            then attribute readtimeout
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='readtimeout']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='pollInterval']))
                            then attribute pollInterval
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='pollInterval']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='isResponseRequired']))
                            then attribute isResponseRequired
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='isResponseRequired']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='responseURI']))
                            then attribute responseURI
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='responseURI']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='responseMessageType']))
                            then attribute responseMessageType
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='responseMessageType']/ns1:VALUE)}
                            else ()
                          }
                          {
                            if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='connResourceRef']))
                            then attribute connResourceRef
                            {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='connResourceRef']/ns1:VALUE)}
                            else ()
                          }
                            <ns2:Credentials>
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']))
                              then attribute user
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='user']/ns1:VALUE)}
                              else ()
                            }
                            {
                              if(fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']))
                              then attribute password
                              {fn:data($OutputParameters/ns1:P_ENDPOINT/ns1:P_ENDPOINT_Row[ns1:KEY='password']/ns1:VALUE)}
                              else ()
                            }
                            </ns2:Credentials>
                    </ns2:MQ>
                 default return
                    <unknown>
                        <error>unknown type</error>
                    </unknown>
            }
            <ns2:Target 
            operation="{fn:data($OutputParameters/ns1:P_TARGET/ns1:OPERATION_)}" 
            provider="{fn:data($OutputParameters/ns1:P_TARGET/ns1:LEGACY_)}">
            {
                if(fn:data($OutputParameters/ns1:P_TARGET/ns1:VERSION_))
                then attribute version
                {fn:data($OutputParameters/ns1:P_TARGET/ns1:VERSION_)}
                else ()
            }
            </ns2:Target>
        </ns2:Endpoint>
        else <ns2:Endpoint/>
        }
        {
            if ($Result)
            then 
                <ns3:Result>
                    {
                        if ($Result/@status)
                        then attribute status {fn:data($Result/@status)}
                        else ()
                    }
                    {
                        if ($Result/@description)
                        then attribute description {fn:data($Result/@description)}
                        else ()
                    }
                    {
                        if ($Result/ns4:CanonicalError)
                        then 
                            <ns4:CanonicalError>
                                {
                                    if ($Result/ns4:CanonicalError/@code)
                                    then attribute code {fn:data($Result/ns4:CanonicalError/@code)}
                                    else ()
                                }
                                {
                                    if ($Result/ns4:CanonicalError/@description)
                                    then attribute description {fn:data($Result/ns4:CanonicalError/@description)}
                                    else ()
                                }
                                {
                                    if ($Result/ns4:CanonicalError/@type)
                                    then attribute type {fn:data($Result/ns4:CanonicalError/@type)}
                                    else ()
                                }
                            </ns4:CanonicalError>
                        else ()
                    }
                    {
                        if ($Result/ns3:SourceErrors)
                        then 
                            <ns3:SourceErrors>
                                {
                                    for $SourceError in $Result/ns3:SourceErrors/ns4:SourceError
                                    return 
                                    <ns4:SourceError>
                                        {
                                            if ($SourceError/@code)
                                            then attribute code {fn:data($SourceError/@code)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/@description)
                                            then attribute description {fn:data($SourceError/@description)}
                                            else ()
                                        }
                                        <ns4:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns4:ErrorSourceDetails/@source)
                                                then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/ns4:ErrorSourceDetails/@details)
                                                then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                                else ()
                                            }
                                        </ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:SourceFault)
                                            then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                            else ()
                                        }
                                    </ns4:SourceError>
                                }
                            </ns3:SourceErrors>
                        else ()
                    }
                </ns3:Result>
            else ()
        }
    </ns2:GetEndpointRSP>
};

local:getEndpointResp_Adapter($OutputParameters, $Result)
