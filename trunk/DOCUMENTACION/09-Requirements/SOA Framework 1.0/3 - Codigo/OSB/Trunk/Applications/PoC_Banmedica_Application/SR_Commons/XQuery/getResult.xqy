xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/Result/v1";
(:: import schema at "../XSD/ESO/Result_v1_ESO.xsd" ::)

declare namespace ns2 = "http://www.entel.cl/ESO/Error/v1";
declare namespace ns3 = "http://www.entel.cl/XQuery/getResult";

(:~
 : This function returns a Result element from the Result schema
 : based on its parameters. Each parameter represents each atomic
 : element within the schema.
 :)

declare variable $resStatus as xs:string external;
declare variable $resDescritpion as xs:string? external;
declare variable $errCode as xs:string? external;
declare variable $errDescription as xs:string? external;
declare variable $errType as xs:string? external;
declare variable $errOrigin as xs:string? external;
declare variable $errDetails as element()? external;

declare function ns3:getResult($resStatus as xs:string,
                               $resDescritpion as xs:string?,
                               $errCode as xs:string?,
                               $errDescription as xs:string?,
                               $errType as xs:string?,
                               $errOrigin as xs:string?,
                               $errDetails as element()?)
                               as element() (:: schema-element(ns1:Result) ::) {
    <ns1:Result>
        <ns1:Status>{fn:data($resStatus)}</ns1:Status>
        {
          if($resDescritpion)
          then <ns1:Description>{fn:data($resDescritpion)}</ns1:Description>
          else ()
        }
        {
          if($errCode) then
            <ns2:Error>
                <ns2:Code>{fn:data($errCode)}</ns2:Code>
                {
                    if($errDescription)
                    then <ns2:Description>{fn:data($errDescription)}</ns2:Description>
                    else ()
                }
                <ns2:Type>{fn:data($errType)}</ns2:Type>
                {
                    if($errOrigin)
                    then <ns2:Origin>{fn:data($errOrigin)}</ns2:Origin>
                    else ()
                }
                {
                    if ($errDetails)
                    then <ns2:Details>{$errDetails/@*, $errDetails/node()}</ns2:Details>
                    else ()
                }
            </ns2:Error>
          else ()
        }
    </ns1:Result>
};

ns3:getResult($resStatus, $resDescritpion, $errCode, $errDescription, $errType, $errOrigin, $errDetails)