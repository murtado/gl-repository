xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.anyuri.com";
(:: import schema at "../XSD/Legacy/_SchemaName_.xsd" ::)
declare namespace ns2="http://www.entel.cl/RA/_InstanceName_/v1";
(:: import schema at "../XSD/CSM/_InstanceName_v1_CSM.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Result/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/Error/v1";

declare variable $LegacyRSP as element() (:: schema-element(ns1:RSP) ::) external;
declare variable $ResponseHeader as element(ns3:ResponseHeader) external;

declare function local:getAdapterResponse($LegacyRSP as element() (:: schema-element(ns1:RSP) ::),
                                          $ResponseHeader as element(ns3:ResponseHeader)) as element() (:: schema-element(ns2:_InstanceName_RSP) ::) {
    <ns2:_InstanceName_RSP>
        <ns3:ResponseHeader>
            <ns3:Consumer code="{fn:data($ResponseHeader/ns3:Consumer/@code)}">
                {
                    if ($ResponseHeader/ns3:Consumer/@name)
                    then attribute name {fn:data($ResponseHeader/ns3:Consumer/@name)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns3:Consumer/ns3:Credentials)
                    then 
                        <ns3:Credentials>
                            {
                                if ($ResponseHeader/ns3:Consumer/ns3:Credentials/@user)
                                then attribute user {fn:data($ResponseHeader/ns3:Consumer/ns3:Credentials/@user)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns3:Consumer/ns3:Credentials/@password)
                                then attribute password {fn:data($ResponseHeader/ns3:Consumer/ns3:Credentials/@password)}
                                else ()
                            }
                        </ns3:Credentials>
                    else ()
                }
            </ns3:Consumer>
            <ns3:Trace>
                {
                    if ($ResponseHeader/ns3:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($ResponseHeader/ns3:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns3:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($ResponseHeader/ns3:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns3:Trace/@messageID)
                    then attribute messageID {fn:data($ResponseHeader/ns3:Trace/@messageID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns3:Trace/@correlationID)
                    then attribute correlationID {fn:data($ResponseHeader/ns3:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns3:Trace/@conversationID)
                    then attribute conversationID {fn:data($ResponseHeader/ns3:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns3:Trace/ns3:Service)
                    then 
                        <ns3:Service>
                            {
                                if ($ResponseHeader/ns3:Trace/ns3:Service/@code)
                                then attribute code {fn:data($ResponseHeader/ns3:Trace/ns3:Service/@code)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns3:Trace/ns3:Service/@name)
                                then attribute name {fn:data($ResponseHeader/ns3:Trace/ns3:Service/@name)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns3:Trace/ns3:Service/@operation)
                                then attribute operation {fn:data($ResponseHeader/ns3:Trace/ns3:Service/@operation)}
                                else ()
                            }
                        </ns3:Service>
                    else ()
                }
            </ns3:Trace>
            <ns3:Country code="{fn:data($ResponseHeader/ns3:Country/@code)}">
                {
                    if ($ResponseHeader/ns3:Country/@name)
                    then attribute name {fn:data($ResponseHeader/ns3:Country/@name)}
                    else ()
                }
            </ns3:Country>
            <ns3:Channel name="{fn:data($ResponseHeader/ns3:Channel/@name)}" mode="{fn:data($ResponseHeader/ns3:Channel/@mode)}"></ns3:Channel>
            {
                if ($ResponseHeader/ns3:QoS)
                then 
                    <ns3:QoS>
                        {
                            if ($ResponseHeader/ns3:QoS/@ttl)
                            then attribute ttl {fn:data($ResponseHeader/ns3:QoS/@ttl)}
                            else ()
                        }
                        {
                            if ($ResponseHeader/ns3:QoS/@priority)
                            then attribute priority {fn:data($ResponseHeader/ns3:QoS/@priority)}
                            else ()
                        }
                        {
                            if ($ResponseHeader/ns3:QoS/@retryCount)
                            then attribute retryCount {fn:data($ResponseHeader/ns3:QoS/@retryCount)}
                            else ()
                        }
                        {
                            if ($ResponseHeader/ns3:QoS/@maxRetryCount)
                            then attribute maxRetryCount {fn:data($ResponseHeader/ns3:QoS/@maxRetryCount)}
                            else ()
                        }
                    </ns3:QoS>
                else ()
            }
            <ns4:Result>
                <ns4:Status>{fn:data($ResponseHeader/ns4:Result/ns4:Status)}</ns4:Status>
                {
                    if ($ResponseHeader/ns4:Result/ns4:Description)
                    then <ns4:Description>{fn:data($ResponseHeader/ns4:Result/ns4:Description)}</ns4:Description>
                    else ()
                }
                {
                    if ($ResponseHeader/ns4:Result/ns5:Error)
                    then 
                        <ns5:Error>
                            <ns5:Code>{fn:data($ResponseHeader/ns4:Result/ns5:Error/ns5:Code)}</ns5:Code>
                            {
                                if ($ResponseHeader/ns4:Result/ns5:Error/ns5:Description)
                                then <ns5:Description>{fn:data($ResponseHeader/ns4:Result/ns5:Error/ns5:Description)}</ns5:Description>
                                else ()
                            }
                            <ns5:Type>{fn:data($ResponseHeader/ns4:Result/ns5:Error/ns5:Type)}</ns5:Type>
                            {
                                if ($ResponseHeader/ns4:Result/ns5:Error/ns5:Origin)
                                then <ns5:Origin>{fn:data($ResponseHeader/ns4:Result/ns5:Error/ns5:Origin)}</ns5:Origin>
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns4:Result/ns5:Error/ns5:Details)
                                then <ns5:Details>{fn:data($ResponseHeader/ns4:Result/ns5:Error/ns5:Details)}</ns5:Details>
                                else ()
                            }
                        </ns5:Error>
                    else ()
                }
            </ns4:Result>
        </ns3:ResponseHeader>
        <ns2:Body>
            <ns2:dummyRSP>{fn:data($LegacyRSP/ns1:RSP)}</ns2:dummyRSP>
        </ns2:Body>
    </ns2:_InstanceName_RSP>
};

local:getAdapterResponse($LegacyRSP, $ResponseHeader)