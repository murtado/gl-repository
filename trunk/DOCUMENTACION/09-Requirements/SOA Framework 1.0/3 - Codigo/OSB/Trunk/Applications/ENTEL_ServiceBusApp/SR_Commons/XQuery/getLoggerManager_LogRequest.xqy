xquery version "1.0" encoding "utf-8";
(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../XSD/ESO/MessageHeader_v1_ESO.xsd" ::)
declare namespace ns1="http://www.entel.cl/SC/LoggerManager/log/v1";
(:: import schema at "../../DC_SC_LoggerManager/SupportAPI/XSD/ESM/log_LoggerManager_v1_ESM.xsd" ::)
declare namespace ns3="http://www.entel.cl/XQuery/getLoggerManager_LogRequest";

declare namespace ns4 = "http://www.entel.cl/ESO/TraceProfile/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/ContextMetadataProfile/v1";

declare variable $MessageHeader as element()? (:: element(*,ns2:MessageHeader_Type)? ::) external;
declare variable $Message as element(*) external;
declare variable $MessageType as xs:string (:: ns1:MessageType_SType ::) external;
declare variable $Severity as xs:string (:: ns1:Severity_SType ::) external;
declare variable $Description as xs:string? (:: ns1:LogDescription_SType? ::) external;
declare variable $Details as xs:string? (:: ns1:Severity_SType? ::) external;
declare variable $HeaderType as xs:string external;

(:~
 : This function returns the request for the Logger Manager support component.
 : It may take any of both the Request Header or the Response Header as an input,
 : given the existance of one or the other is the Message Header taken into
 : account, if both are sent, then the Request Header is used.
 :
 : It also accepts a Message, MessageType {SREQ|SRSP|LOG}, Description, Details,
 : and Severity {INFO|WARNING|ERROR}.
 :
 : @author GlobalLogic
 : @version 1.0
 :)

declare function ns3:getLoggerManager_LogRequest($MessageHeader as element()? (:: element(*,ns2:MessageHeader_Type)? ::),
                                                 $Message as element(*), 
                                                 $MessageType as xs:string (:: ns1:MessageType_SType ::), 
                                                 $Severity as xs:string (:: ns1:Severity_SType ::), 
                                                 $Description as xs:string? (:: ns1:LogDescription_SType? ::), 
                                                 $Details as xs:string? (:: ns1:Severity_SType? ::),
                                                 $HeaderType as xs:string)
                                                 as element(ns1:LogREQ) {
        <ns1:LogREQ>
            <ns1:Trace>
                <ns4:Header>
                    <ns2:Consumer code="{fn:data($MessageHeader/ns2:Consumer/@code)}">
                        {
                            if ($MessageHeader/ns2:Consumer/@name)
                            then attribute name {fn:data($MessageHeader/ns2:Consumer/@name)}
                            else ()
                        }
                        {
                            if ($MessageHeader/ns2:Consumer/ns2:Credentials)
                            then 
                                <ns2:Credentials>
                                    {
                                        if ($MessageHeader/ns2:Consumer/ns2:Credentials/@user)
                                        then attribute user {fn:data($MessageHeader/ns2:Consumer/ns2:Credentials/@user)}
                                        else ()
                                    }
                                    {
                                        if ($MessageHeader/ns2:Consumer/ns2:Credentials/@password)
                                        then attribute password {fn:data($MessageHeader/ns2:Consumer/ns2:Credentials/@password)}
                                        else ()
                                    }
                                </ns2:Credentials>
                            else ()
                        }
                    </ns2:Consumer>
                    <ns2:Trace>
                        {
                            if ($MessageHeader/ns2:Trace/@reqTimestamp)
                            then attribute reqTimestamp {fn:data($MessageHeader/ns2:Trace/@reqTimestamp)}
                            else ()
                        }
                        {
                            if ($MessageHeader/ns2:Trace/@rspTimestamp)
                            then attribute rspTimestamp {fn:data($MessageHeader/ns2:Trace/@rspTimestamp)}
                            else ()
                        }
                        {
                            if ($MessageHeader/ns2:Trace/@messageID)
                            then attribute messageID {fn:data($MessageHeader/ns2:Trace/@messageID)}
                            else ()
                        }
                        {
                            if ($MessageHeader/ns2:Trace/@correlationID)
                            then attribute correlationID {fn:data($MessageHeader/ns2:Trace/@correlationID)}
                            else ()
                        }
                        {
                            if ($MessageHeader/ns2:Trace/@conversationID)
                            then attribute conversationID {fn:data($MessageHeader/ns2:Trace/@conversationID)}
                            else ()
                        }
                        {
                            if ($MessageHeader/ns2:Trace/ns2:Service)
                            then 
                                <ns2:Service>
                                    {
                                        if ($MessageHeader/ns2:Trace/ns2:Service/@code)
                                        then attribute code {fn:data($MessageHeader/ns2:Trace/ns2:Service/@code)}
                                        else ()
                                    }
                                    {
                                        if ($MessageHeader/ns2:Trace/ns2:Service/@name)
                                        then attribute name {fn:data($MessageHeader/ns2:Trace/ns2:Service/@name)}
                                        else ()
                                    }
                                    {
                                        if ($MessageHeader/ns2:Trace/ns2:Service/@operation)
                                        then attribute operation {fn:data($MessageHeader/ns2:Trace/ns2:Service/@operation)}
                                        else ()
                                    }
                                </ns2:Service>
                            else ()
                        }
                    </ns2:Trace>
                    <ns2:Country code="{fn:data($MessageHeader/ns2:Country/@code)}">
                        {
                            if ($MessageHeader/ns2:Country/@name)
                            then attribute name {fn:data($MessageHeader/ns2:Country/@name)}
                            else ()
                        }
                    </ns2:Country>
                    <ns2:Channel name="{fn:data($MessageHeader/ns2:Channel/@name)}" mode="{fn:data($MessageHeader/ns2:Channel/@mode)}"></ns2:Channel>
                    {
                        if ($MessageHeader/ns2:QoS)
                        then 
                            <ns2:QoS>
                                {
                                    if ($MessageHeader/ns2:QoS/@ttl)
                                    then attribute ttl {fn:data($MessageHeader/ns2:QoS/@ttl)}
                                    else ()
                                }
                                {
                                    if ($MessageHeader/ns2:QoS/@priority)
                                    then attribute priority {fn:data($MessageHeader/ns2:QoS/@priority)}
                                    else ()
                                }
                                {
                                    if ($MessageHeader/ns2:QoS/@retryCount)
                                    then attribute retryCount {fn:data($MessageHeader/ns2:QoS/@retryCount)}
                                    else ()
                                }
                                {
                                    if ($MessageHeader/ns2:QoS/@maxRetryCount)
                                    then attribute maxRetryCount {fn:data($MessageHeader/ns2:QoS/@maxRetryCount)}
                                    else ()
                                }
                            </ns2:QoS>
                        else ()
                    }
                </ns4:Header>
                <ns4:Logger>
                    {
                        if ($MessageHeader/ns2:Trace/ns2:Service/@name)
                        then attribute Service {fn:data($MessageHeader/ns2:Trace/ns2:Service/@name)}
                        else ()
                    }
                    {
                        if ($MessageHeader/ns2:Trace/ns2:Service/@operation)
                        then attribute Operation {fn:data($MessageHeader/ns2:Trace/ns2:Service/@operation)}
                        else ()
                    }
                </ns4:Logger>
            </ns1:Trace>
            <ns1:Message>{ $Message/* }</ns1:Message>
            <ns1:MessageType>{fn:data($MessageType)}</ns1:MessageType>
            <ns1:Severity>{fn:data($Severity)}</ns1:Severity>
            <ns1:Description>{fn:data($Description)}</ns1:Description>
            <ns1:Detail>{fn:data($Details)}</ns1:Detail>
        </ns1:LogREQ>
    
};

ns3:getLoggerManager_LogRequest($MessageHeader, $Message, $MessageType, $Severity, $Description, $Details,$HeaderType)
