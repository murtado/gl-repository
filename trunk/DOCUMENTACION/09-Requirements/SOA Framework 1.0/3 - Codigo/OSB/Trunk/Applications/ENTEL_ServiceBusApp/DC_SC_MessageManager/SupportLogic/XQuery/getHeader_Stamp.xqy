xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns0 = "http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)
declare namespace ns1 = "http://www.entel.cl/ESO/Result/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v1_ESO.xsd" ::)

declare variable $message as element() external;
declare variable $conversationID as xs:string? external;
declare variable $serviceCode as xs:string? external;
declare variable $serviceName as xs:string? external;
declare variable $serviceOperation as xs:string? external;

declare function local:getHeaderStamp($message as element(), 
                                      $conversationID as xs:string?,
                                      $serviceCode as xs:string?,
                                      $serviceName as xs:string?,
                                      $serviceOperation as xs:string?) 
                                      as element() {
    let $header := $message/*[1]
    return
    if (fn:local-name($header) = "RequestHeader") then (
        <ns0:RequestHeader>
            <ns0:Consumer>{ $header/ns0:Consumer/@* , $header/ns0:Consumer/node() }</ns0:Consumer>
            <ns0:Trace clientReqTimestamp = "{ fn:data($header/ns0:Trace/@clientReqTimestamp) }"
                       reqTimestamp = "{ if (data($header/ns0:Trace/@reqTimestamp) != '') then
                                             (data($header/ns0:Trace/@reqTimestamp))
                                         else 
                                             fn:current-dateTime() }"
                       processID = "{ if (fn:data($header/ns0:Trace/@processID)) then
                                            fn:data($header/ns0:Trace/@processID)
                                          else "" }"
                       eventID = "{ fn:data($header/ns0:Trace/@eventID) }"
                       conversationID = "{ if ($conversationID != '') then
                                            $conversationID
                                          else fn:data($header/ns0:Trace/@conversationID)   }"
                       correlationID = "{ if (fn:data($header/ns0:Trace/@correlationID)) then
                                            fn:data($header/ns0:Trace/@correlationID)
                                          else "" }">
                  <ns0:Service code = "{ $serviceCode }" name = "{ $serviceName }"
                                 operation = "{ $serviceOperation }"/>
                </ns0:Trace>
            <ns0:Channel>{ $header/ns0:Channel/@* , $header/ns0:Channel/node() }</ns0:Channel>
        </ns0:RequestHeader>
    )
    else (
      <ns0:ResponseHeader>
          <ns0:Consumer>{ $header/ns0:Consumer/@*, $header/ns0:Consumer/node() }</ns0:Consumer>
          <ns0:Trace reqTimestamp = "{ data($header/ns0:Trace/@reqTimestamp) }"
                     rspTimestamp = "{ fn:current-dateTime() }"
                     processID = "{ data($header/ns0:Trace/@processID) }"
                     eventID = "{ data($header/ns0:Trace/@eventID) }"
                     conversationID = "{ data($header/ns0:Trace/@conversationID) }"
                     correlationID = "{ data($header/ns0:Trace/@correlationID) }">
            <ns0:Service>{ $header/ns0:Trace/ns0:Service/@*, $header/ns0:Trace/ns0:Service/node() }</ns0:Service>
          </ns0:Trace>
          <ns0:Channel>{ $header/ns0:Channel/@* , $header/ns0:Channel/node() }</ns0:Channel>
          <ns1:Result>{ $header/ns1:Channel/@*, $header/ns1:Channel/node() }</ns1:Result>
      </ns0:ResponseHeader>
    )
};

local:getHeaderStamp($message, $conversationID, $serviceCode, $serviceName, $serviceOperation)
