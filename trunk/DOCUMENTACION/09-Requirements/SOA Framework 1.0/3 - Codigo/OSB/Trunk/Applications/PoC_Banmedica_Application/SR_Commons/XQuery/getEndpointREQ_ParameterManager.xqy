xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/ParameterManager/getEndpoint/v1";
(:: import schema at "../../DC_SC_ParameterManager_Refactor/SupportAPI/XSD/CSM/getEndpoint_ParameterManager_v1_CSM.xsd" ::)

declare namespace ns1="http://www.entel.cl/XQuery/getLW_ParameterManager_EndpointREQ";

declare variable $operation as xs:string external;
declare variable $version as xs:int? external;
declare variable $legacyName as xs:string external;

declare function ns1:getParameterManager_EndpointREQ($operation as xs:string, 
                                                        $version as xs:int?, 
                                                        $legacyName as xs:string) 
                                                        as element() (:: schema-element(ns2:GetEndpointREQ) ::) {
    <ns2:GetEndpointREQ>
        <ns2:Target operation="{fn:data($operation)}"
                    provider="{fn:data($legacyName)}"
                    version="{fn:data($version)}"/>
    </ns2:GetEndpointREQ>
};

ns1:getParameterManager_EndpointREQ($operation, $version, $legacyName)