xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/LW/LegacyWrapperAPI/v1";
(:: import schema at "../XSD/CSM/LegacyWrapper_v1_CSM.xsd" ::)

declare namespace ns1="http://www.entel.cl/XQuery/getLegacyWrapper_Request";

declare variable $RA_REQ as element() external;
declare variable $TrgtOp as xs:string external;
declare variable $TrgtVer as xs:int external;

declare function ns1:func($RA_REQ as element(), 
                          $TrgtOp as xs:string,
                          $TrgtVer as xs:int) 
                          as element() (:: schema-element(ns2:LW_REQ) ::) {
    <ns2:LW_REQ>
        <ns2:LegacyREQ>{$RA_REQ/*}</ns2:LegacyREQ>
        <ns2:Target operation="{$TrgtOp}" version="{$TrgtVer}"/>
    </ns2:LW_REQ>
};

ns1:func($RA_REQ, $TrgtOp, $TrgtVer)
