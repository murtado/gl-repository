xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $isProxy as xs:boolean external;
declare variable $isPipeline as xs:boolean? external;
declare variable $operation as xs:string? external;
declare variable $URI as xs:string external;

declare function local:routeTo($isProxy as xs:boolean, 
                               $isPipeline as xs:boolean?, 
                               $operation as xs:string?, 
                               $URI as xs:string) 
                               as element() {
    <ctx:route xmlns:ctx='http://www.bea.com/wli/sb/context'>
    {if($isPipeline) then
          <ctx:pipeline isProxy='{$isProxy}'>
              {$URI}
          </ctx:pipeline>
     else  <ctx:service isProxy='{$isProxy}'>{$URI}</ctx:service>   
     } 
         <ctx:operation>{$operation}</ctx:operation>
    </ctx:route>
};

local:routeTo($isProxy, $isPipeline, $operation, $URI)
