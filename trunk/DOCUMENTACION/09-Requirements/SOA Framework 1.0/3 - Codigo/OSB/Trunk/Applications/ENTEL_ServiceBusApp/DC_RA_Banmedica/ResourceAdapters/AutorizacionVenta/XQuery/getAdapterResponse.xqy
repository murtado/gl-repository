xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.banmedica.cl/";
(:: import schema at "../../../../DC_LW_Banmedica/LegacyAPI/WSDL/BanmedicaFCV.wsdl" ::)
declare namespace ns3="http://www.entel.cl/RA/AutorizacionVenta/v1";
(:: import schema at "../XSD/CSM/AutorizacionVenta_v1_CSM.xsd" ::)
declare namespace ns4="http://www.entel.cl/ESO/MessageHeader/v1";
(:: import schema at "../../../../SR_Commons/XSD/ESO/MessageHeader_v1_ESO.xsd" ::)


declare namespace ns1="http://www.entel.cl/SC/getAdapterResponse";

declare namespace ns5 = "http://www.entel.cl/ESO/Result/v1";

declare namespace ns6 = "http://www.entel.cl/ESO/Error/v1";

declare variable $LegacyResponse as element() (:: schema-element(ns2:AutorizacionVentaResponse) ::) external;
declare variable $ResponseHeader as element(ns4:ResponseHeader) external;

declare function ns1:getAdapterResponse($LegacyResponse as element() (:: schema-element(ns2:AutorizacionVentaResponse) ::),
                                        $ResponseHeader as element(ns4:ResponseHeader)) as element() (:: schema-element(ns3:AutorizacionVentaRSP) ::) {
    <ns3:AutorizacionVentaRSP>
        <ns4:ResponseHeader>
            <ns4:Consumer code="{fn:data($ResponseHeader/ns4:Consumer/@code)}">
                {
                    if ($ResponseHeader/ns4:Consumer/@name)
                    then attribute name {fn:data($ResponseHeader/ns4:Consumer/@name)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns4:Consumer/ns4:Credentials)
                    then 
                        <ns4:Credentials>
                            {
                                if ($ResponseHeader/ns4:Consumer/ns4:Credentials/@user)
                                then attribute user {fn:data($ResponseHeader/ns4:Consumer/ns4:Credentials/@user)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns4:Consumer/ns4:Credentials/@password)
                                then attribute password {fn:data($ResponseHeader/ns4:Consumer/ns4:Credentials/@password)}
                                else ()
                            }
                        </ns4:Credentials>
                    else ()
                }
            </ns4:Consumer>
            <ns4:Trace>
                {
                    if ($ResponseHeader/ns4:Trace/@reqTimestamp)
                    then attribute reqTimestamp {fn:data($ResponseHeader/ns4:Trace/@reqTimestamp)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns4:Trace/@rspTimestamp)
                    then attribute rspTimestamp {fn:data($ResponseHeader/ns4:Trace/@rspTimestamp)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns4:Trace/@messageID)
                    then attribute messageID {fn:data($ResponseHeader/ns4:Trace/@messageID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns4:Trace/@correlationID)
                    then attribute correlationID {fn:data($ResponseHeader/ns4:Trace/@correlationID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns4:Trace/@conversationID)
                    then attribute conversationID {fn:data($ResponseHeader/ns4:Trace/@conversationID)}
                    else ()
                }
                {
                    if ($ResponseHeader/ns4:Trace/ns4:Service)
                    then 
                        <ns4:Service>
                            {
                                if ($ResponseHeader/ns4:Trace/ns4:Service/@code)
                                then attribute code {fn:data($ResponseHeader/ns4:Trace/ns4:Service/@code)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns4:Trace/ns4:Service/@name)
                                then attribute name {fn:data($ResponseHeader/ns4:Trace/ns4:Service/@name)}
                                else ()
                            }
                            {
                                if ($ResponseHeader/ns4:Trace/ns4:Service/@operation)
                                then attribute operation {fn:data($ResponseHeader/ns4:Trace/ns4:Service/@operation)}
                                else ()
                            }
                        </ns4:Service>
                    else ()
                }
            </ns4:Trace>
            <ns4:Country code="{fn:data($ResponseHeader/ns4:Country/@code)}">
                {
                    if ($ResponseHeader/ns4:Country/@name)
                    then attribute name {fn:data($ResponseHeader/ns4:Country/@name)}
                    else ()
                }
            </ns4:Country>
            <ns4:Channel name="{fn:data($ResponseHeader/ns4:Channel/@name)}" mode="{fn:data($ResponseHeader/ns4:Channel/@mode)}"/>
            {
                if ($ResponseHeader/ns4:QoS)
                then 
                    <ns4:QoS>
                        {
                            if ($ResponseHeader/ns4:QoS/@ttl)
                            then attribute ttl {fn:data($ResponseHeader/ns4:QoS/@ttl)}
                            else ()
                        }
                        {
                            if ($ResponseHeader/ns4:QoS/@priority)
                            then attribute priority {fn:data($ResponseHeader/ns4:QoS/@priority)}
                            else ()
                        }
                        {
                            if ($ResponseHeader/ns4:QoS/@retryCount)
                            then attribute retryCount {fn:data($ResponseHeader/ns4:QoS/@retryCount)}
                            else ()
                        }
                        {
                            if ($ResponseHeader/ns4:QoS/@maxRetryCount)
                            then attribute maxRetryCount {fn:data($ResponseHeader/ns4:QoS/@maxRetryCount)}
                            else ()
                        }
                    </ns4:QoS>
                else ()
            }
            <ns5:Result>
                <ns5:Status>{fn:data($ResponseHeader/ns5:Result/ns5:Status)}</ns5:Status>
                {
                    if ($ResponseHeader/ns5:Result/ns5:Description)
                    then <ns5:Description>{fn:data($ResponseHeader/ns5:Result/ns5:Description)}</ns5:Description>
                    else ()
                }
                {
                    if ($ResponseHeader/ns5:Result/ns6:Error)
                    then 
                        <ns6:Error>
                            <ns6:Code>{fn:data($ResponseHeader/ns5:Result/ns6:Error/ns6:Code)}</ns6:Code>
                            {
                                if ($ResponseHeader/ns5:Result/ns6:Error/ns6:Description)
                                then <ns6:Description>{fn:data($ResponseHeader/ns5:Result/ns6:Error/ns6:Description)}</ns6:Description>
                                else ()
                            }
                            <ns6:Type>{fn:data($ResponseHeader/ns5:Result/ns6:Error/ns6:Type)}</ns6:Type>
                        </ns6:Error>
                    else ()
                }
            </ns5:Result>
        </ns4:ResponseHeader>
        <ns3:Body>
            <ns3:Status>{fn:data($LegacyResponse/ns2:AutorizacionVentaResult)}</ns3:Status>
        </ns3:Body>
    </ns3:AutorizacionVentaRSP>
};

ns1:getAdapterResponse($LegacyResponse, $ResponseHeader)
