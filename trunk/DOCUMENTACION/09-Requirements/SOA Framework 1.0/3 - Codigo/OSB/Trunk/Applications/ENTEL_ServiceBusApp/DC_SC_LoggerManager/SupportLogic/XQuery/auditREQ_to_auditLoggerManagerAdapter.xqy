xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/LoggerManager/audit/v1";
(:: import schema at "../../SupportAPI/XSD/ESM/audit_LoggerManager_v1_ESM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/auditLoggerManagerAdapter";
(:: import schema at "../JCA/auditLoggerManagerAdapter/auditLoggerManagerAdapter_sp.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/TraceProfile/v1";

declare namespace ns4 = "http://www.entel.cl/ESO/ContextMetadataProfile/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare variable $auditREQ as element() (:: schema-element(ns1:AuditREQ) ::) external;

declare function local:auditLoggerManagerAdapter($auditREQ as element() (:: schema-element(ns1:AuditREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:AUDITREQ>
            <ns2:TRACE_>
                <ns2:HEADER_>
                    <ns2:CONSUMER_>
                        <ns2:CODE_>{fn:data($auditREQ/ns1:Trace/ns3:Header/ns5:Consumer/@code)}</ns2:CODE_>
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:Consumer/@name)
                            then <ns2:NAME_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Consumer/@name)}</ns2:NAME_>
                            else ()
                        }
                        <ns2:CREDENTIAL_>
                            {
                                if ($auditREQ/ns1:Trace/ns3:Header/ns5:Consumer/ns5:Credentials/@user)
                                then <ns2:USER_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Consumer/ns5:Credentials/@user)}</ns2:USER_>
                                else ()
                            }
                            {
                                if ($auditREQ/ns1:Trace/ns3:Header/ns5:Consumer/ns5:Credentials/@password)
                                then <ns2:PASSWORD_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Consumer/ns5:Credentials/@password)}</ns2:PASSWORD_>
                                else ()
                            }
                        </ns2:CREDENTIAL_>
                    </ns2:CONSUMER_>
                    <ns2:TRACE_>
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@reqTimestamp)
                            then <ns2:REQTIMESTAMP_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@reqTimestamp)}</ns2:REQTIMESTAMP_>
                            else ()
                        }
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@rspTimestamp)
                            then <ns2:RSPTIMESTAMP_>{fn:data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@rspTimestamp)}</ns2:RSPTIMESTAMP_>
                            else ()
                        }
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@messageID)
                            then <ns2:MESSAGEID_>{fn:data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@messageID)}</ns2:MESSAGEID_>
                            else ()
                        }
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@correlationID)
                            then <ns2:CORRELATIONID_>{fn:data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@correlationID)}</ns2:CORRELATIONID_>
                            else ()
                        }
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@conversationID)
                            then <ns2:CONVERSATIONID_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/@conversationID)}</ns2:CONVERSATIONID_>
                            else ()
                        }
                        <ns2:SERVICE_>
                            {
                                if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/ns5:Service/@code)
                                then <ns2:CODE_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/ns5:Service/@code)}</ns2:CODE_>
                                else ()
                            }
                            {
                                if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/ns5:Service/@name)
                                then <ns2:NAME_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/ns5:Service/@name)}</ns2:NAME_>
                                else ()
                            }
                            {
                                if ($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/ns5:Service/@operation)
                                then <ns2:OPERATION_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Trace/ns5:Service/@operation)}</ns2:OPERATION_>
                                else ()
                            }
                        </ns2:SERVICE_>
                    </ns2:TRACE_>
                    <ns2:COUNTRY_>
                        <ns2:CODE_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Country/@code)}</ns2:CODE_>
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:Country/@name)
                            then <ns2:NAME_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Country/@name)}</ns2:NAME_>
                            else ()
                        }
                    </ns2:COUNTRY_>
                    <ns2:CHANNEL_>
                        <ns2:NAME_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Channel/@name)}</ns2:NAME_>
                        <ns2:MODE_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:Channel/@mode)}</ns2:MODE_>
                    </ns2:CHANNEL_>
                    <ns2:QOS_>
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@ttl)
                            then <ns2:TTL_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@ttl)}</ns2:TTL_>
                            else ()
                        }
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@priority)
                            then <ns2:PRIORITY_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@priority)}</ns2:PRIORITY_>
                            else ()
                        }
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@retryCount)
                            then <ns2:RETRYCOUNTER_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@retryCount)}</ns2:RETRYCOUNTER_>
                            else ()
                        }
                        {
                            if ($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@maxRetryCount)
                            then <ns2:MAXRETRYCOUNT_>{data($auditREQ/ns1:Trace/ns3:Header/ns5:QoS/@maxRetryCount)}</ns2:MAXRETRYCOUNT_>
                            else ()
                        }
                    </ns2:QOS_>
                </ns2:HEADER_>
                <ns2:LOGGER_>
                    {
                        if ($auditREQ/ns1:Trace/ns3:Logger/@Component)
                        then <ns2:COMPONENT_>{data($auditREQ/ns1:Trace/ns3:Logger/@Component)}</ns2:COMPONENT_>
                        else ()
                    }
                    {
                        if ($auditREQ/ns1:Trace/ns3:Logger/@Operation)
                        then <ns2:OPERATION_>{data($auditREQ/ns1:Trace/ns3:Logger/@Operation)}</ns2:OPERATION_>
                        else ()
                    }
                </ns2:LOGGER_>
                <ns2:CONTEXTMETADATA_>
                    <ns2:FAMILY_>{data($auditREQ/ns1:Trace/ns3:ContextMetadata/@Family)}</ns2:FAMILY_>
                    <ns2:CLASS_>{data($auditREQ/ns1:Trace/ns3:ContextMetadata/@Class)}</ns2:CLASS_>
                    {
                        if ($auditREQ/ns1:Trace/ns3:ContextMetadata/@Member)
                        then <ns2:MEMBER_>{data($auditREQ/ns1:Trace/ns3:ContextMetadata/@Member)}</ns2:MEMBER_>
                        else ()
                    }
                    {
                        if ($auditREQ/ns1:Trace/ns3:ContextMetadata/@Module)
                        then <ns2:MODULE_>{data($auditREQ/ns1:Trace/ns3:ContextMetadata/@Module)}</ns2:MODULE_>
                        else ()
                    }
                </ns2:CONTEXTMETADATA_>
            </ns2:TRACE_>
            <ns2:MESSAGE_>{$auditREQ/ns1:Message/node()}</ns2:MESSAGE_>
            <ns2:TYPE_>{fn:data($auditREQ/ns1:Type)}</ns2:TYPE_>
            {
                if ($auditREQ/ns1:Description)
                then <ns2:DESCRIPTION_>{fn:data($auditREQ/ns1:Description)}</ns2:DESCRIPTION_>
                else ()
            }
        </ns2:AUDITREQ>
    </ns2:InputParameters>
};

local:auditLoggerManagerAdapter($auditREQ)
