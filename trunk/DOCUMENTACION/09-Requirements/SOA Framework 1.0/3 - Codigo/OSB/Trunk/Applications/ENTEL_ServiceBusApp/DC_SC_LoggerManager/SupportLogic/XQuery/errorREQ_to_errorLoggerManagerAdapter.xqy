xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/LoggerManager/error/v1";
(:: import schema at "../../SupportAPI/XSD/ESM/error_LoggerManager_v1_ESM.xsd" ::)

declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/errorLoggerManagerAdapter";
(:: import schema at "../JCA/errorLoggerManagerAdapter/errorLoggerManagerAdapter_sp.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/TraceProfile/v1";
declare namespace ns4 = "http://www.entel.cl/ESO/ContextMetadataProfile/v1";
declare namespace ns5 = "http://www.entel.cl/ESO/MessageHeader/v1";
declare namespace ns6 = "http://www.entel.cl/ESO/Result/v1";
declare namespace ns7 = "http://www.entel.cl/ESO/Error/v1";

declare variable $errorREQ as element() (:: schema-element(ns1:ErrorREQ) ::) external;

declare function local:errorLoggerManagerAdapter($errorREQ as element() (:: schema-element(ns1:ErrorREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
     <ns2:InputParameters>

    </ns2:InputParameters>
   
};

local:errorLoggerManagerAdapter($errorREQ)
