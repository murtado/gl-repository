xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ParameterManager/getEndpoint/v1";
(:: import schema at "../../DC_SC_ParameterManager_Refactor/SupportAPI/XSD/CSM/getEndpoint_ParameterManager_v1_CSM.xsd" ::)

declare variable $getEndpointRSP as element() (:: schema-element(ns1:GetEndpointRSP) ::) external;

declare function local:getRouteTo($getEndpointRSP as element() (:: schema-element(ns1:GetEndpointRSP) ::)) as element() {
    if(exists($getEndpointRSP/ns1:Endpoint/ns1:OSB)) then (
      <ctx:route xmlns:ctx='http://www.bea.com/wli/sb/context'>{
        if(fn:boolean(fn:data($getEndpointRSP/ns1:Endpoint/ns1:OSB/@isPipeline))) then
          <ctx:pipeline isProxy='{fn:data($getEndpointRSP/ns1:Endpoint/ns1:OSB/@isProxy)}'>
              {fn:data($getEndpointRSP/ns1:Endpoint/ns1:OSB/@uri)}
          </ctx:pipeline>
        else  <ctx:service isProxy='{fn:boolean(fn:data($getEndpointRSP/ns1:Endpoint/ns1:OSB/@isPipeline))}'>{fn:data($getEndpointRSP/ns1:Endpoint/ns1:OSB/@uri)}</ctx:service>   
        } 
         <ctx:operation>{fn:data($getEndpointRSP/ns1:Endpoint/ns1:OSB/@operation)}</ctx:operation>
      </ctx:route>
    )
    else (<ctx:route xmlns:ctx='http://www.bea.com/wli/sb/context'/>)
};

local:getRouteTo($getEndpointRSP)
