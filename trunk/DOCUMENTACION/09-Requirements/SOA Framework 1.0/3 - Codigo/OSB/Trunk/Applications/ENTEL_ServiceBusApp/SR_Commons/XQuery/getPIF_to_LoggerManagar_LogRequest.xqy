xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/LoggerManager/log/v1";
(:: import schema at "../../DC_SC_LoggerManager/SupportAPI/XSD/ESM/log_LoggerManager_v1_ESM.xsd" ::)

declare namespace ns3="http://www.entel.cl/XQuery/getPIF_to_LoggerManagar_LogRequest";
declare namespace ns2 = "http://www.entel.cl/ESO/MessageHeader/v1";
declare namespace ns4 = "http://www.entel.cl/ESO/TraceProfile/v1";
declare namespace ns5 = "http://www.entel.cl/ESO/ContextMetadataProfile/v1";

declare variable $body as element() external;

(:~
 : This function intends to map the data in the message body of the¨Primary Interface to 
 : the one needed to call the Log operation of the Logger Manager.
 :)

declare function ns2:getPIF_to_LoggerManagar_LogRequest($body as element())
                                                              as element() (:: schema-element(ns1:LogREQ) ::) {
    let $Header := $body/*[1]/*[1] return
    <ns1:LogREQ>
        <ns1:Trace>
            <ns4:Header>
                <ns2:Consumer code="{fn:data($Header/ns2:Consumer/@code)}">
                    {
                        if ($Header/ns2:Consumer/@name)
                        then attribute name {fn:data($Header/ns2:Consumer/@name)}
                        else ()
                    }
                    {
                        if ($Header/ns2:Consumer/ns2:Credentials)
                        then 
                            <ns2:Credentials>
                                {
                                    if ($Header/ns2:Consumer/ns2:Credentials/@user)
                                    then attribute user {fn:data($Header/ns2:Consumer/ns2:Credentials/@user)}
                                    else ()
                                }
                                {
                                    if ($Header/ns2:Consumer/ns2:Credentials/@password)
                                    then attribute password {fn:data($Header/ns2:Consumer/ns2:Credentials/@password)}
                                    else ()
                                }
                            </ns2:Credentials>
                        else ()
                    }
                </ns2:Consumer>
                <ns2:Trace>
                    {
                        if ($Header/ns2:Trace/@reqTimestamp)
                        then attribute reqTimestamp {fn:data($Header/ns2:Trace/@reqTimestamp)}
                        else ()
                    }
                    {
                        if ($Header/ns2:Trace/@rspTimestamp)
                        then attribute rspTimestamp {fn:data($Header/ns2:Trace/@rspTimestamp)}
                        else ()
                    }
                    {
                        if ($Header/ns2:Trace/@messageID)
                        then attribute messageID {fn:data($Header/ns2:Trace/@messageID)}
                        else ()
                    }
                    {
                        if ($Header/ns2:Trace/@correlationID)
                        then attribute correlationID {fn:data($Header/ns2:Trace/@correlationID)}
                        else ()
                    }
                    {
                        if ($Header/ns2:Trace/@conversationID)
                        then attribute conversationID {fn:data($Header/ns2:Trace/@conversationID)}
                        else ()
                    }
                    {
                        if ($Header/ns2:Trace/ns2:Service)
                        then 
                            <ns2:Service>
                                {
                                    if ($Header/ns2:Trace/ns2:Service/@code)
                                    then attribute code {fn:data($Header/ns2:Trace/ns2:Service/@code)}
                                    else ()
                                }
                                {
                                    if ($Header/ns2:Trace/ns2:Service/@name)
                                    then attribute name {fn:data($Header/ns2:Trace/ns2:Service/@name)}
                                    else ()
                                }
                                {
                                    if ($Header/ns2:Trace/ns2:Service/@operation)
                                    then attribute operation {fn:data($Header/ns2:Trace/ns2:Service/@operation)}
                                    else ()
                                }
                            </ns2:Service>
                        else ()
                    }
                </ns2:Trace>
                <ns2:Country code="{fn:data($Header/ns2:Country/@code)}">
                    {
                        if ($Header/ns2:Country/@name)
                        then attribute name {fn:data($Header/ns2:Country/@name)}
                        else ()
                    }
                </ns2:Country>
                <ns2:Channel code="{fn:data($Header/ns2:Channel/@code)}" mode="{fn:data($Header/ns2:Channel/@mode)}"/>
                {
                    if ($Header/ns2:QoS)
                    then 
                        <ns2:QoS>
                            {
                                if ($Header/ns2:QoS/@ttl)
                                then attribute ttl {fn:data($Header/ns2:QoS/@ttl)}
                                else ()
                            }
                            {
                                if ($Header/ns2:QoS/@priority)
                                then attribute priority {fn:data($Header/ns2:QoS/@priority)}
                                else ()
                            }
                            {
                                if ($Header/ns2:QoS/@retryCount)
                                then attribute retryCount {fn:data($Header/ns2:QoS/@retryCount)}
                                else ()
                            }
                            {
                                if ($Header/ns2:QoS/@maxRetryCount)
                                then attribute maxRetryCount {fn:data($Header/ns2:QoS/@maxRetryCount)}
                                else ()
                            }
                        </ns2:QoS>
                    else ()
                }
            </ns4:Header>
            <ns4:Logger>
                {
                    if ($Header/ns2:Trace/ns2:Service/@name)
                    then attribute Service {fn:data($Header/ns2:Trace/ns2:Service/@name)}
                    else ()
                }
                {
                    if ($Header/ns2:Trace/ns2:Service/@operation)
                    then attribute Operation {fn:data($Header/ns2:Trace/ns2:Service/@operation)}
                    else ()
                }
            </ns4:Logger>
        </ns1:Trace>
        <ns1:Message>{ $body/*[1]/*[2]/* }</ns1:Message>
        <ns1:MessageType>{if (fn:local-name($Header)="RequestHeader") then "SREQ"
                           else if (fn:local-name($Header)="ResponseHeader") then "SRSP"
                           else "LOG"}</ns1:MessageType>
        <ns1:Severity>INFO</ns1:Severity>
        <ns1:Description/>
        <ns1:Detail/>
    </ns1:LogREQ>
};

ns2:getPIF_to_LoggerManagar_LogRequest($body)
