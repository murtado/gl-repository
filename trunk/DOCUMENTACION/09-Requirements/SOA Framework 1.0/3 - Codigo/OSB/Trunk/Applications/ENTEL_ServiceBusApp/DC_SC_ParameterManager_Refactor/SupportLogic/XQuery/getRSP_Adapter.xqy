xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/ParameterManager/get/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/get_ParameterManager_v1_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getParameterManagerAdapter";
(:: import schema at "../JCA/getParameterManagerAdapter/getParameterManagerAdapter_sp.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/Result/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/Result/v1";



declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;
declare variable $Result as element()? (:: schema-element(ns3:Result) ::) external;

declare function local:getRSP_Adapter($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::),
                                      $Result as element()? (:: schema-element(ns3:Result) ::)) as element() (:: schema-element(ns2:GetRSP) ::) {
    <ns2:GetRSP xmlns:ns3="http://www.entel.cl/EDD/Dictionary/v1">
        <ns2:Values>
            {
                for $P_KEYVALUES_Row in $OutputParameters/ns1:P_KEYVALUES/ns1:P_KEYVALUES_Row
                return 
                <ns3:Item>
                {
                    attribute key {fn:data($P_KEYVALUES_Row/ns1:KEY)}
                }
                {
                    attribute value {fn:data($P_KEYVALUES_Row/ns1:VALUE)}
                }
                </ns3:Item>
            }
        </ns2:Values>
        { if($Result) then
            <ns5:Result>
                {
                    if ($Result/@status)
                    then attribute status {fn:data($Result/@status)}
                    else ()
                }
                {
                    if ($Result/@description)
                    then attribute description {fn:data($Result/@description)}
                    else ()
                }
                {
                    if ($Result/ns4:CanonicalError)
                    then 
                        <ns4:CanonicalError>
                            {
                                if ($Result/ns4:CanonicalError/@code)
                                then attribute code {fn:data($Result/ns4:CanonicalError/@code)}
                                else ()
                            }
                            {
                                if ($Result/ns4:CanonicalError/@description)
                                then attribute description {fn:data($Result/ns4:CanonicalError/@description)}
                                else ()
                            }
                            {
                                if ($Result/ns4:CanonicalError/@type)
                                then attribute type {fn:data($Result/ns4:CanonicalError/@type)}
                                else ()
                            }
                        </ns4:CanonicalError>
                    else ()
                }
                {
                    if ($Result/ns5:SourceErrors)
                    then 
                        <ns5:SourceErrors>
                            {
                                for $SourceError in $Result/ns5:SourceErrors/ns4:SourceError
                                return 
                                <ns4:SourceError>
                                    {
                                        if ($SourceError/@code)
                                        then attribute code {fn:data($SourceError/@code)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/@description)
                                        then attribute description {fn:data($SourceError/@description)}
                                        else ()
                                    }
                                    <ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@source)
                                            then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/ns4:ErrorSourceDetails/@details)
                                            then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                            else ()
                                        }
                                    </ns4:ErrorSourceDetails>
                                    {
                                        if ($SourceError/ns4:SourceFault)
                                        then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                        else ()
                                    }
                                </ns4:SourceError>
                            }
                        </ns5:SourceErrors>
                    else ()
                }
            </ns5:Result>
        else () }
    </ns2:GetRSP>
};

local:getRSP_Adapter($OutputParameters, $Result)
