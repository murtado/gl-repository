xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ParameterManager/getMapping/v1";
(:: import schema at "../../DC_SC_ParameterManager_Refactor/SupportAPI/XSD/CSM/getMapping_ParameterManager_v1_CSM.xsd" ::)

declare variable $Maps as element() external;

declare function local:getMappingREQ_ParameterManager($Maps as element()) as element() (:: schema-element(ns1:GetMappingsREQ) ::) {
    <ns1:GetMappingsREQ>
        <ns1:Maps>
            { for $Map in $Maps/* return
            <ns1:Map sCode="{$Map/@sCode}" group="{$Map/@group}" field="{$Map/@field}" dSys="{$Map/@dSys}" sSys="{$Map/@sSys}">
            </ns1:Map>
            }
        </ns1:Maps>
    </ns1:GetMappingsREQ>
};

local:getMappingREQ_ParameterManager($Maps)
