xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/ParameterManager/getConfig/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/getConfig_ParameterManager_v1_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getConfigParameterManagerAdapter";
(:: import schema at "../JCA/getConfigParameterManagerAdapter/getConfigParameterManagerAdapter_sp.xsd" ::)
declare namespace ns3="http://www.entel.cl/ESO/Result/v1";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v1_ESO.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns5 = "http://www.entel.cl/ESO/Result/v1";

declare variable $outputParametersDB as element() (:: schema-element(ns1:OutputParameters) ::) external;
declare variable $Result as element()? (:: schema-element(ns3:Result) ::) external;

declare function local:getConfigResp_Adapter($outputParametersDB as element() (:: schema-element(ns1:OutputParameters) ::),
                                             $Result as element()? (:: schema-element(ns3:Result) ::)) as element() (:: schema-element(ns2:GetConfigRSP) ::) {
    <ns2:GetConfigRSP xmlns:ns3 = "http://www.entel.cl/EDD/Dictionary/v1">
        <ns2:Config>
            {
                for $P_CONFIG_Row in $outputParametersDB/ns1:P_CONFIG/ns1:P_CONFIG_Row
                return 
                <ns3:Item key="{fn:data($P_CONFIG_Row/ns1:KEY)}">
                      <ns3:Value>{fn:data($P_CONFIG_Row/ns1:VALUE)}</ns3:Value>
                </ns3:Item>
            }
        </ns2:Config>
        {
            if ($Result)
            then 
                <ns5:Result>
                    {
                        if ($Result/@status)
                        then attribute status {fn:data($Result/@status)}
                        else ()
                    }
                    {
                        if ($Result/@description)
                        then attribute description {fn:data($Result/@description)}
                        else ()
                    }
                    {
                        if ($Result/ns4:CanonicalError)
                        then 
                            <ns4:CanonicalError>
                                {
                                    if ($Result/ns4:CanonicalError/@code)
                                    then attribute code {fn:data($Result/ns4:CanonicalError/@code)}
                                    else ()
                                }
                                {
                                    if ($Result/ns4:CanonicalError/@description)
                                    then attribute description {fn:data($Result/ns4:CanonicalError/@description)}
                                    else ()
                                }
                                {
                                    if ($Result/ns4:CanonicalError/@type)
                                    then attribute type {fn:data($Result/ns4:CanonicalError/@type)}
                                    else ()
                                }
                            </ns4:CanonicalError>
                        else ()
                    }
                    {
                        if ($Result/ns5:SourceErrors)
                        then 
                            <ns5:SourceErrors>
                                {
                                    for $SourceError in $Result/ns5:SourceErrors/ns4:SourceError
                                    return 
                                    <ns4:SourceError>
                                        {
                                            if ($SourceError/@code)
                                            then attribute code {fn:data($SourceError/@code)}
                                            else ()
                                        }
                                        {
                                            if ($SourceError/@description)
                                            then attribute description {fn:data($SourceError/@description)}
                                            else ()
                                        }
                                        <ns4:ErrorSourceDetails>
                                            {
                                                if ($SourceError/ns4:ErrorSourceDetails/@source)
                                                then attribute source {fn:data($SourceError/ns4:ErrorSourceDetails/@source)}
                                                else ()
                                            }
                                            {
                                                if ($SourceError/ns4:ErrorSourceDetails/@details)
                                                then attribute details {fn:data($SourceError/ns4:ErrorSourceDetails/@details)}
                                                else ()
                                            }
                                        </ns4:ErrorSourceDetails>
                                        {
                                            if ($SourceError/ns4:SourceFault)
                                            then <ns4:SourceFault>{fn:data($SourceError/ns4:SourceFault)}</ns4:SourceFault>
                                            else ()
                                        }
                                    </ns4:SourceError>
                                }
                            </ns5:SourceErrors>
                        else ()
                    }
                </ns5:Result>
            else ()
        }
    </ns2:GetConfigRSP>
};

local:getConfigResp_Adapter($outputParametersDB, $Result)
