SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE

	v_ERR_CMP_AUX ERR_CMP_T;
	v_ERR_CMP_CFG_AUX ERR_CMP_CFG_T;
	v_LOCAL_LOCATOR_INDEX_AUX NUMBER;
	v_REMOTE_LOCATOR_AUX VARCHAR(255);
	v_SOURCE_ERROR_CODE VARCHAR(255);
	v_SOURCE_ERROR_DESC VARCHAR(255);
	v_CMP_ID_AUX NUMBER;
	v_CMP_CFG_ID_AUX NUMBER;

BEGIN

	v_LOCAL_LOCATOR_INDEX_AUX 	:= 0;
	v_REMOTE_LOCATOR_AUX := 'INIT - ADP_FWK_ERR_007';

	v_ERR_CMP_AUX 			:= NULL;
	v_ERR_CMP_CFG_AUX 		:= NULL;
	v_CMP_ID_AUX 			:= NULL;
	v_CMP_CFG_ID_AUX 		:= NULL;

	v_SOURCE_ERROR_CODE		:= '0';
	v_SOURCE_ERROR_DESC 	:= '';

	/*
	------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------
		This script creates any number of Components with any number of Steps each, to work with within the
		diagnosis process.
	------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------
	*/

		/*
			########################################################################################################
				REPEAT FOR EACH COMPONENT
			########################################################################################################
		*/

		-- The 'XXX' values should be replaced either by NULL (when possible) or their intended value.
			v_ERR_CMP_AUX := NEW ERR_CMP_T
			(
				/*Component Name*/'XXX',-- Mandatory
				/*Component Description*/'XXX'-- Optional
			);

		-- ### DO NOT MODIFY THESE STATEMENTS ###
			v_CMP_ID_AUX:=ESB_ERROR_MANAGER_PKG.CREATE_COMPONENT(v_ERR_CMP_AUX,v_REMOTE_LOCATOR_AUX,v_SOURCE_ERROR_CODE,v_SOURCE_ERROR_DESC);
			
			IF(v_SOURCE_ERROR_CODE!=0) THEN
				DBMS_OUTPUT.PUT_LINE('Error Found on {'|| v_LOCAL_LOCATOR_INDEX_AUX||'}. Details :'||v_SOURCE_ERROR_CODE||'] - ['||v_SOURCE_ERROR_DESC||'].');
				ROLLBACK;
				DBMS_OUTPUT.PUT_LINE('All changes were Rollbacked.');
				RETURN;
			ELSE
				DBMS_OUTPUT.PUT_LINE('Execution of {'|| v_LOCAL_LOCATOR_INDEX_AUX||'} was successful. A Component with ID :['|| v_CMP_ID_AUX||'] was created.');
				v_LOCAL_LOCATOR_INDEX_AUX := ++v_LOCAL_LOCATOR_INDEX_AUX;
			END IF;

					/*
						########################################################################################################
							REPEAT FOR EACH STEP
						########################################################################################################
					*/
						-- The 'XXX' values should be replaced either by NULL (when possible) or their intended value.
						v_ERR_CMP_CFG_AUX := NEW ERR_CMP_CFG_T
						(
							/*Component*/v_ERR_CMP_AUX,-- Mandatory
							/*Component Step Name*/'XXX',-- Mandatory
							/*Component Step Description*/'XXX'-- Optional
						);

					-- ### DO NOT MODIFY THESE STATEMENTS ###
						v_CMP_CFG_ID_AUX:=ESB_ERROR_MANAGER_PKG.CREATE_COMPONENT_CFG(v_ERR_CMP_CFG_AUX,v_CMP_ID_AUX,v_REMOTE_LOCATOR_AUX,v_SOURCE_ERROR_CODE,v_SOURCE_ERROR_DESC);
						
						IF(v_SOURCE_ERROR_CODE!=0) THEN
							DBMS_OUTPUT.PUT_LINE('Error Found on {'|| v_LOCAL_LOCATOR_INDEX_AUX||'}. Details :'||v_SOURCE_ERROR_CODE||'] - ['||v_SOURCE_ERROR_DESC||'].');
							ROLLBACK;
							DBMS_OUTPUT.PUT_LINE('All changes were Rollbacked.');
							RETURN;
						ELSE
							DBMS_OUTPUT.PUT_LINE('Execution of {'|| v_LOCAL_LOCATOR_INDEX_AUX||'} was successful. A Component CFG with ID :['|| v_CMP_CFG_ID_AUX||'] was created.');
							v_LOCAL_LOCATOR_INDEX_AUX := ++v_LOCAL_LOCATOR_INDEX_AUX;
						END IF;

						v_ERR_CMP_CFG_AUX := NULL;
						v_CMP_CFG_ID_AUX := NULL;
						/*
							########################################################################################################
						*/

		/*
			########################################################################################################
		*/

	-- ### DO NOT MODIFY THESE STATEMENTS ###

	COMMIT;

	EXCEPTION WHEN OTHERS THEN

		v_SOURCE_ERROR_CODE := '99';
		v_SOURCE_ERROR_DESC :=
				v_SOURCE_ERROR_DESC
				||' @ ['
				||'Local ['|| v_LOCAL_LOCATOR_INDEX_AUX||'] - Remote ['|| v_REMOTE_LOCATOR_AUX||'].'
				||'] --- Error ['
				|| SQLERRM
				||']';

		DBMS_OUTPUT.PUT_LINE('Error Found on {'||v_LOCAL_LOCATOR_INDEX_AUX||'}. Details :'||v_SOURCE_ERROR_CODE||'] - ['||v_SOURCE_ERROR_DESC||'].');
		ROLLBACK;
		DBMS_OUTPUT.PUT_LINE('All changes were Rollbacked.');
END;
/