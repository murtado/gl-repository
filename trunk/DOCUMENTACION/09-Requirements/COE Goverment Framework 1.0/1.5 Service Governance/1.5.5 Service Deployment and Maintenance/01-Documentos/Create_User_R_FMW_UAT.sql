-- USER SQL
CREATE USER TDEFMW_USERU01 IDENTIFIED BY tdeuser 
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";

-- QUOTAS

-- ROLES
GRANT "CONNECT" TO TDEFMW_USERU01 ;

-- SYSTEM PRIVILEGES
GRANT SELECT ANY TABLE TO TDEFMW_USERU01 ;
